#ifndef IPLUS_LOG_SINK_H
#define IPLUS_LOG_SINK_H
#include <iostream>
#include <string>
#include <cstring>
#include <glog/logging.h>
#include <glog/log_severity.h>
#include <iomanip>

static const char *const LogSeverityNames2[4] = {
    "INFO", "WARNING", "ERROR", "FATAL"};

static const char *GetBasename(const char *filepath)
{
  const char *base = std::strrchr(filepath, '/');
  return base ? (base + 1) : filepath;
}
class IplusLogSink : public google::LogSink
{
  virtual void send(google::LogSeverity severity, const char *full_filename,
                    const char *base_filename, int line,
                    const struct ::tm *tm_time,
                    const char *message, size_t message_len)
  {
    int usecs = 0;
    std::cout << "[" << 1900 + tm_time->tm_year << "-"
              << std::setfill('0')
              << std::setw(2) << 1 + tm_time->tm_mon << "-"
              << std::setw(2) << tm_time->tm_mday << " "
              << std::setw(2) << tm_time->tm_hour << ":"
              << std::setw(2) << tm_time->tm_min << ":"
              << std::setw(2) << tm_time->tm_sec << "."
              << std::setw(3) << usecs << std::setfill('0') << "] ["
              << LogSeverityNames2[severity] << "] ("
              << GetBasename(full_filename) << ":" << line << ") "
              << std::string(message, message_len) << std::endl;
    ;
  }
};
#endif // IPLUS_LOG_SINK_H
