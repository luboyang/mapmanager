#ifndef ROS_TOOL
#define ROS_TOOL
#include <chrono>
#include <thread>
#include "ros/ros.h"
#include <tf/tf.h>
#include <geometry_msgs/TransformStamped.h>
namespace rostools
{
  bool GetTfPose(const std::string& target_frame, const std::string& source_frame,const tf2_ros::Buffer &tfBuffer,
  const std_msgs::Header &header,geometry_msgs::TransformStamped &base2odom_tf)
  {
      std::chrono::steady_clock::time_point t1 =
            std::chrono::steady_clock::now();

        while (ros::ok()) {
          try {
            base2odom_tf = tfBuffer.lookupTransform(
                target_frame, source_frame,
                header.stamp);
            break;
          } catch (tf::TransformException ex) {
            std::chrono::steady_clock::time_point t2 =
                std::chrono::steady_clock::now();
            auto duration =
                std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
            if (duration.count() > 0.5) {
              printf("Can't get transform between odom and base_footprint!");
              return false;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
          }
        }

        return true;
  }
};

#endif