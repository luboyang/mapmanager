#ifndef UTILS_SYSTEM
#define UTILS_SYSTEM

#ifdef _WIN32 //Windows
#include <direct.h>
#include <io.h>
#include <windows.h>
#else  // Linux
#include <sys/io.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#endif

#include <cstring>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
namespace systemutils
{
	// File System
	inline bool JudgeFileExist(const std::string& filePath)
	{
		return std::filesystem::exists(filePath);
	}

	inline bool JudgeFolderExist(const std::string& filePath)
	{
		return std::filesystem::exists(filePath);
	}

	/*inline std::string GetCurrentPath()
	{
		auto currentPath = std::filesystem::current_path();
		return currentPath.string();
	}*/

	inline void RemoveDirectory(const std::filesystem::path& path, std::string& errorMsgs)
	{
		if (!std::filesystem::exists(path))
		{
			errorMsgs = "Path is not existed";
			return;
		}

		std::filesystem::remove_all(path);
	}

	inline bool CreatePath(const std::string& path, std::string& errorMsg)
	{
		if (JudgeFolderExist(path))
		{
			errorMsg = "This directory already exists.";
			return true;
		}

		if (!std::filesystem::create_directories(path))
		{
			errorMsg = "Create directory failed";
			return false;
		}

		return true;
	}

#ifndef _WIN32
inline void GetPath(const std::string &mapName, std::string &fullPath)
{
  std::string homePath;

  if(const char *envPath = std::getenv("HOME"))
  {
    std::string temp = "/root";
    homePath = envPath;
    if (homePath.compare(0, 4, temp, 0, 4) == 0)
    {
      homePath = "/home/jz";
    }
  }
  else
  {
    homePath = "/home/jz";
  }
  fullPath = homePath + "/map/" + mapName + "/";
}

inline bool CheckSystemStatus(const std::string &cmdString, std::string &errorMsgs)
{
  int status = system(cmdString.c_str());
  if (status < 0)
  {
    // 这里务必要把errno信息输出或记入Log
    errorMsgs = "cmd:" + cmdString + "\nerror:" + strerror(errno);
    return false;
  }

  if(WIFEXITED(status))
  {
    // 取得cmdString执行结果
    errorMsgs = "normal termination, exit status = " + WEXITSTATUS(status);
  }
  else if(WIFSIGNALED(status))
  {
    // 如果cmdString被信号中断，取得信号值
    errorMsgs = "abnormal termination,signal number = " + WTERMSIG(status);
  }
  else if(WIFSTOPPED(status))
  {
    // 如果cmdString被信号暂停执行，取得信号值
    errorMsgs = "process stopped, signal number = " + WSTOPSIG(status);
  }
  return true;
}
#endif
}; // namespace systemutils
#endif