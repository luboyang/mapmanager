#ifndef UTILS_STRING
#define UTILS_STRING
#ifndef _WIN32
#include <geometry_msgs/Pose.h>
#endif
#include <string>
#include <vector>
#include "common/datatype/transform.h"
namespace stringutils
{
typedef std::uint64_t hash_t;
constexpr hash_t prime = 0x100000001B3ull;
constexpr hash_t basis = 0xCBF29CE484222325ull;
#ifndef _WIN32
inline void PoseXmlToString(const geometry_msgs::Pose &pose, std::string &poseStr)
{
  poseStr = std::to_string(pose.position.x) + " " + std::to_string(pose.position.y) + " " +
            std::to_string(pose.position.z) + " " + std::to_string(pose.orientation.w) + " " +
            std::to_string(pose.orientation.x) + " " + std::to_string(pose.orientation.y) + " " +
            std::to_string(pose.orientation.z);
}

inline void PoseStringToXml(const std::string &poseStr, geometry_msgs::Pose &pose)
{
  std::sscanf(poseStr.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose.position.x, &pose.position.y,
              &pose.position.z, &pose.orientation.x, &pose.orientation.y, &pose.orientation.z,
              &pose.orientation.w);
}
#endif

inline void PoseXmlToString(const Rigid3d& pose, std::string& poseStr, const bool flag = false)
{
	if (flag)
		poseStr = std::to_string(pose.translation().x()) + " " + std::to_string(pose.translation().y()) + " " +
		std::to_string(pose.translation().z()) + " " + std::to_string(pose.rotation().w()) + " " +
		std::to_string(pose.rotation().x()) + " " + std::to_string(pose.rotation().y()) + " " +
		std::to_string(pose.rotation().z());
	else
		poseStr = std::to_string(pose.translation().x()) + " " + std::to_string(pose.translation().y()) + " " +
		std::to_string(pose.translation().z()) + " " +
		std::to_string(pose.rotation().x()) + " " + std::to_string(pose.rotation().y()) + " " +
		std::to_string(pose.rotation().z()) + " " + std::to_string(pose.rotation().w());
}

inline void PoseStringToXml(const std::string& poseStr, Rigid3d& pose, const bool flag = false)
{
	double x, y, z, qx, qy, qz, qw;
	if (flag)
		std::sscanf(poseStr.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &x, &y, &z, &qw, &qx, &qy, &qz);
	else
		std::sscanf(poseStr.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &x, &y, &z, &qx, &qy, &qz, &qw);
	pose = Rigid3d({ x,y,z }, { qw,qx,qy,qz });
}

inline void SplitString(const std::string& str, std::vector<std::string>& results, const std::string delim = " ")
{
	results.clear();

	auto start = str.find_first_not_of(delim, 0);       // 分割到的字符串的第一个字符
	auto position = str.find_first_of(delim, start);    // 分隔符的位置
	while (position != std::string::npos || start != std::string::npos)
	{
		// [start, position) 为分割下来的字符串
		results.emplace_back(std::move(str.substr(start, position - start)));
		start = str.find_first_not_of(delim, position);
		position = str.find_first_of(delim, start);
	}
}

inline hash_t hash_(char const *str)
{
  hash_t ret{basis};

  while (*str)
  {
    ret ^= *str;
    ret *= prime;
    str++;
  }

  return ret;
}
inline constexpr hash_t hash_compile_time(char const *str, hash_t last_value = basis)
{
  return *str ? hash_compile_time(str + 1, (*str ^ last_value) * prime) : last_value;
}
// switch string
inline constexpr unsigned long long operator"" _hash(char const *p, size_t)
{
  return hash_compile_time(p);
}

static const std::string base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";


static inline bool is_base64(unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}

inline std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
	std::string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; (i < 4); i++)
				ret += base64_chars[char_array_4[i]];
			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[char_array_4[j]];

		while ((i++ < 3))
			ret += '=';

	}

	return ret;

}

inline std::string base64_decode(std::string const& encoded_string) {
	size_t in_len = encoded_string.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	std::string ret;

	while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i == 4) {
			for (i = 0; i < 4; i++)
				char_array_4[i] = base64_chars.find(char_array_4[i]) & 0xff;

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				ret += char_array_3[i];
			i = 0;
		}
	}

	if (i) {
		for (j = 0; j < i; j++)
			char_array_4[j] = base64_chars.find(char_array_4[j]) & 0xff;

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

		for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
	}

	return ret;
}

inline void PointsToString(const std::vector<float>& points, std::string& pointsStr)
{

	for (size_t i = 0; i < points.size() - 1; i++)
	{
		pointsStr = pointsStr + std::to_string(points[i]) + " ";
	}
	pointsStr += std::to_string(points.back());
}

inline void PointsFromString(const std::string& pointsStr, std::vector<float>& points)
{
	std::istringstream iss(pointsStr);
	float f;
	while (iss >> f)
	{
		points.push_back(f);
	}
}

} // namespace stringutils
#endif