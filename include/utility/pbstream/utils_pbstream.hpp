#ifndef UTILS_PBSTREAM
#define UTILS_PBSTREAM

#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/sensor/imu_data.h>
#include <cartographer/sensor/rangefinder_point.h>
#include <cartographer_ros/time_conversion.h>
#include <cartographer_ros_msgs/SaveMapServerRequest.h>
#include <cartographer_ros_msgs/StatusCode.h>
#include <cartographer_ros_msgs/SubmapQuery.h>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <common/data.hpp>
#include <utility/cv/utils_cv.hpp>
#include <utility/eigen/eigen_utils.hpp>
#include <utility/xml/landmark_xml.hpp>
#include <utility/xml/laser_data_xml.hpp>
#include <utility/xml/map_xml.hpp>
#include <utility/xml/mapdata_xml.hpp>
namespace pbstreamutils
{
inline void ReadPosesFromPbstream(const std::string &path, data::PbstreamInfos &infos)
{
  cartographer::io::ProtoStreamReader stream(path + "/map.pbstream");
  cartographer::io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  // LOG(INFO) << "Insert node poses..";
  std::map<cartographer::mapping::NodeId, cartographer::transform::Rigid3d> origin_node_poses;
  for (const auto &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const auto &node_proto : trajectory_proto.node())
    {
      infos.node_poses.Insert(
          cartographer::mapping::NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
          cartographer::transform::ToRigid3(node_proto.pose()));
    }

    for (const auto &submap_proto : trajectory_proto.submap())
    {
      infos.submap_poses.Insert(cartographer::mapping::SubmapId{trajectory_proto.trajectory_id(),
                                                                submap_proto.submap_index()},
                                cartographer::transform::ToRigid3(submap_proto.pose()));
    }
  }

  for (const auto &landmark : pose_graph_proto.landmark_poses())
  {
    infos.landmark_poses[landmark.landmark_id()] =
        cartographer::transform::ToRigid3(landmark.global_pose());
  }

  cartographer::mapping::proto::SerializedData proto;
  // LOG(INFO) << "deserializer pbstream..";
  std::vector<cartographer::sensor::ImuData> imu_datas;
  std::vector<cartographer::sensor::OdometryData> odom_datas;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kPoseGraph:
      // LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
      //             "stream likely corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kAllTrajectoryBuilderOptions:
      // LOG(ERROR) << "Found multiple serialized "
      //             "`AllTrajectoryBuilderOptions`. Serialized stream likely "
      //             "corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kSubmap:
    {
      infos.submap_id_to_submap.Insert(
          cartographer::mapping::SubmapId{proto.submap().submap_id().trajectory_id(),
                                          proto.submap().submap_id().submap_index()},
          proto.submap());
      break;
    }
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      const cartographer::mapping::NodeId node_id(proto.node().node_id().trajectory_id(),
                                                  proto.node().node_id().node_index());
      infos.node_datas.Insert(node_id, cartographer::mapping::FromProto(proto.node().node_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kImuData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kOdometryData:
    {
      infos.odom_datas.push_back(
          cartographer::sensor::FromProto(proto.odometry_data().odometry_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kFixedFramePoseData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kLandmarkData:
    {
      infos.landmark_datas.push_back(
          cartographer::sensor::FromProto(proto.landmark_data().landmark_data()));
      break;
    }
    default:
      break;
    }
  }

  infos.constraints = cartographer::mapping::FromProto(pose_graph_proto.constraint());
}

}; // namespace pbstreamutils
#endif