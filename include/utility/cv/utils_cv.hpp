#ifndef UTILS_CV
#define UTILS_CV
#ifdef _WIN32
#include <opencv2/opencv.hpp>
#else
#include <opencv3/opencv2/opencv.hpp>
#endif
#include <vector>
#include <common/error_info.hpp>

namespace cvutils
{
enum MAP_FORMART
{
  SHOW_MAP,
  SAVE_MAP,
  PUB_MAP
};

inline cv::Mat Grey16ToRgba(const cv::Mat &image)
{
  cv::Mat mapAdjusted = cv::Mat::zeros(image.rows, image.cols, CV_8UC4);
  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uint16_t>(i, j);
      if (value == 0)
      {
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value / 128;
          
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = static_cast<uchar>(value / 128.);
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = static_cast<uchar>(value / 128.);
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = static_cast<uchar>(value / 128.);

        const int delta = 128 - value / 128;
        // 				const int delta = 128 - int(value / 256);
        if (delta < 0)
        {
          mapAdjusted.at<cv::Vec4b>(i, j)[0] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[1] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta : 0;
        // 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = static_cast<uchar>(255 * sqrt(sqrt(alpha / 128.)));
      }
    }
  }
  return mapAdjusted;
}

inline cv::Mat Grey8ToRgba(const cv::Mat &image)
{
  cv::Mat mapAdjusted = cv::Mat::zeros(image.rows, image.cols, CV_8UC4);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uchar>(i, j);
      if (value == 128)
      {
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value / 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = value;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = value;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = value;

        const int delta = 128 - value;
        // 				const int delta = 128 - int(value / 256);
        if (delta < 0)
        {
          mapAdjusted.at<cv::Vec4b>(i, j)[0] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[1] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta : 0;
        // 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = static_cast<uchar>(255 * sqrt(sqrt(alpha / 128.)));
      }
    }
  }
  return mapAdjusted;
}

inline cv::Mat Image16ToImage8(const cv::Mat &image16)
{
  cv::Mat image8(image16.rows, image16.cols, CV_8UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      int value = image16.at<uint16_t>(i, j);
      if (value == 0)
        image8.at<uchar>(i, j) = 128;
      else
        image8.at<uchar>(i, j) = value / 128;
    }

  return image8;
}

inline cv::Mat Image8ToImage16(const cv::Mat &image8)
{
  cv::Mat image16(image8.rows, image8.cols, CV_16UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      const unsigned char color = image8.at<uchar>(i, j);
      if (color == 128)
        image16.at<uint16_t>(i, j) = 0;
      else
        image16.at<uint16_t>(i, j) = color * 128;
    }
  return image16;
}

inline void AutoSetIgnoreArea(const std::string& path, const int& distance_threshold = 3)
{
	// auto set ignore area
	cv::Mat img = cv::imread(path + "/map.png", CV_LOAD_IMAGE_UNCHANGED);
	cv::Mat gray_img;
	if (img.type() != CV_8UC1)
	{
		cvtColor(img, gray_img, CV_BGR2GRAY);
	}
	else
	{
		gray_img = img;
	}
	cv::Mat new_bool_image(gray_img.rows, gray_img.cols, CV_8UC1, cv::Scalar(255));
	cv::Mat binary_img;
	threshold(gray_img, binary_img, 100, 255, CV_THRESH_BINARY);
	cv::Mat dstImage;
	cv::distanceTransform(binary_img, dstImage, CV_DIST_L2, CV_DIST_MASK_PRECISE);
	//   Mat element1 = getStructuringElement(cv::MorphShapes::MORPH_RECT, Size(5, 5));
	//   erode(binary_img, dstImage, element1);
	cv::Mat output(dstImage.rows, dstImage.cols, CV_8UC1, cv::Scalar(0));
	for (int i = 0; i < dstImage.rows; i++)
	{
		for (int j = 0; j < dstImage.cols; j++)
		{
			// if(dstImage.at<uchar>(i,j) != 0)
			if (dstImage.at<float>(i, j) > distance_threshold)
			{
				new_bool_image.at<uchar>(i, j) = 0;
			}
		}
	}
	imwrite(path + "/bool_image_auto.png", new_bool_image);
}

inline void UpdateModifyArea(const cv::Mat& modify_area_img, const std::string& path)
{
	std::string img_full_path = path + "/frames/0/probability_grid_origin.png";
	std::string img_update_path = path + "/frames/0/probability_grid.png";
	cv::Mat org = cv::imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
	for (int i = 0; i < modify_area_img.rows; i++)
		for (int j = 0; j < modify_area_img.cols; j++)
		{
			if (modify_area_img.at<uint8_t>(i, j) == 127 || modify_area_img.at<uint8_t>(i, j) == 128)
			{
				continue;
			}
			else if (modify_area_img.at<uint8_t>(i, j) == 255)
				org.at<uint16_t>(i, j) = 32765;
			else
			{
				if (modify_area_img.at<uint8_t>(i, j) == 0)
					org.at<uint16_t>(i, j) = 1;
				else
					org.at<uint16_t>(i, j) = 128 * modify_area_img.at<uint8_t>(i, j);
			}
		}
	imwrite(path + "/modify_area.png", modify_area_img);
	// LOG(INFO) << "imwrite modify area.png done!";
	imwrite(img_update_path, org);
	// LOG(INFO) << "update probability_grid.png done!";
	cv::Mat img_8 = Image16ToImage8(org);
	imwrite(path + "/map.png", img_8);
	// LOG(INFO) << "update map.png done!";
}

#ifndef _WIN32
inline const cv::Mat GetMatMap(const cartographer::io::PaintSubmapSlicesResult &painted_slices,
                               MAP_FORMART format)
{
  const int width = cairo_image_surface_get_width(painted_slices.surface.get());
  const int height = cairo_image_surface_get_height(painted_slices.surface.get());
  const uint32_t *pixel_data =
      reinterpret_cast<uint32_t *>(cairo_image_surface_get_data(painted_slices.surface.get()));
  cv::Mat pub_img(height, width, CV_8UC4);
  cv::Mat show_img(height, width, CV_8UC4);
  cv::Mat save_img(height, width, CV_16UC1);
  for (int y = height - 1; y >= 0; --y)
  {
    for (int x = 0; x < width; ++x)
    {
      const uint32_t packed = pixel_data[y * width + x];
      const unsigned char color = packed >> 16;
      const unsigned char observed = packed >> 8;
      const int delta = 128 - int(color);
      // 			const unsigned char alpha = delta > 0 ? delta : -delta;
      // 			LOG(INFO) << int(alpha) <<"  ";
      // 			LOG(INFO) << int(color) ;
      const int value =
          observed == 0 ? -1 : cartographer::common::RoundToInt((1. - color / 255.) * 100.);
      // ���޸�
      if (!(-1 <= value && 100 >= value))
      {
        exit(0);
      }
      // CHECK_LE(-1, value);
      // CHECK_GE(100, value);
      //  			if(observed!=0)
      //  			if (color == 128)
      //  				img.at<uint16_t>(y, x) = 0;
      //  			else
      if (format == PUB_MAP)
      {
        if (color == 128)
        {
          pub_img.at<cv::Vec4b>(y, x)[0] = 128;
          pub_img.at<cv::Vec4b>(y, x)[1] = 128;
          pub_img.at<cv::Vec4b>(y, x)[2] = 128;
          pub_img.at<cv::Vec4b>(y, x)[3] = 0;
        }
        else
        {
          pub_img.at<cv::Vec4b>(y, x)[0] = color;
          pub_img.at<cv::Vec4b>(y, x)[1] = color;
          pub_img.at<cv::Vec4b>(y, x)[2] = color;
          // 					pub_img.at<Vec4b>(y, x)[3] = alpha;
          // 				const int delta = 128 - int(value / 256);
          if (delta < 0)
          {
            pub_img.at<cv::Vec4b>(y, x)[0] = 255;
            pub_img.at<cv::Vec4b>(y, x)[1] = 255;
            pub_img.at<cv::Vec4b>(y, x)[2] = 255;
          }
          // 					const unsigned char alpha1 = delta > 0 ? delta : -delta;
          // 				const unsigned char value1 = delta > 0 ? delta : 0;
          const double alpha1 = delta > 0 ? delta : -delta;
          pub_img.at<cv::Vec4b>(y, x)[3] = 255 * sqrt(sqrt(alpha1 / 128.));
          // 					pub_img.at<Vec4b>(y, x)[3] = -255* log(1-alpha/128)
        }
      }
      else if (format == SAVE_MAP)
      {
        if (color == 128)
          save_img.at<uint16_t>(y, x) = 0;
        else
          save_img.at<uint16_t>(y, x) = color * 128;
      }
      else
      {
        show_img.at<cv::Vec4b>(y, x)[0] = color;
        show_img.at<cv::Vec4b>(y, x)[1] = color;
        show_img.at<cv::Vec4b>(y, x)[2] = color;
        show_img.at<cv::Vec4b>(y, x)[3] = 255;
        // 				show_img.at<uint8_t>(y, x) = color;
      }
    }
  }
  if (format == PUB_MAP)
  {
    cv::Mat pub_img_flip;
    transpose(pub_img, pub_img_flip);
    cv::flip(pub_img_flip, pub_img_flip, 0);
    return pub_img_flip.clone();
  }
  else if (format == SAVE_MAP)
  {
    cv::Mat save_img_flip;
    transpose(save_img, save_img_flip);
    cv::flip(save_img_flip, save_img_flip, 0);
    return save_img_flip.clone();
  }
  else
  {
    cv::Mat show_img_flip;
    transpose(show_img, show_img_flip);
    cv::flip(show_img_flip, show_img_flip, 0);
    return show_img_flip.clone();
  }
}

inline int MergeTransparentImgs(cv::Mat &dst, cv::Mat &scr, double scale)
{
  if (dst.channels() != 3 || scr.channels() != 4)
  {
    return true;
  }
  if (scale < 0.01)
  {
    return false;
  }
  std::vector<cv::Mat> scr_channels;
  std::vector<cv::Mat> dstt_channels;
  split(scr, scr_channels);
  split(dst, dstt_channels);
  CV_Assert(scr_channels.size() == 4 && dstt_channels.size() == 3);

  if (scale < 1)
  {
    scr_channels[3] *= scale;
    scale = 1;
  }
  for (int i = 0; i < 3; i++)
  {
    dstt_channels[i] = dstt_channels[i].mul(255.0 / scale - scr_channels[3], scale / 255.0);
    dstt_channels[i] += scr_channels[i].mul(scr_channels[3], scale / 255.0);
  }
  merge(dstt_channels, dst);
  return true;
}




#endif

inline cv::Mat Base64StringToCvMat(const std::string&imgStr )
{
    std::vector<char> base64Img(imgStr.begin(), imgStr.end());
	return cv::imdecode(base64Img, cv::IMREAD_UNCHANGED);;
}

inline void SetPngFromCvMat(const std::string& pngPath,const cv::Mat &dataMat)
{
    cv::imwrite(pngPath, dataMat);
}

inline void GetImageData(const std::string& path, std::vector<uint8_t>& mapData)
{
    cv::Mat submapThres = cv::imread(path, cv::IMREAD_GRAYSCALE);
    mapData = std::move(submapThres.reshape(1, 1));
}

inline void GetImageData(const std::string& path, std::string& imgData)
{
	cv::Mat submapThres = cv::imread(path, cv::IMREAD_GRAYSCALE);
	std::vector<uint8_t> mapData = std::move(submapThres.reshape(1, 1));
    imgData.clear();
    imgData.assign(mapData.begin(), mapData.end());
}

inline common::ErrorInfo SetPngToThresPng(const std::string& pngPath, const std::string& thresPngPath)
{
    cv::Mat pngImg = cv::imread(pngPath);
    if (pngImg.empty())
    {
        return common::ErrorInfo(-1, "Cant read png, png path is " + pngPath, common::ErrorType::kError);
    }
	cv::Mat thresPngImg;
	threshold(pngImg, thresPngImg, 100, 255, cv::THRESH_BINARY);
	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(9);
    if (!imwrite(thresPngPath, thresPngImg, compression_params))
    {
        return common::ErrorInfo(-1, "Cant write threspng, threspng path is " + thresPngPath, common::ErrorType::kError);
    }
    return common::ErrorInfo();
}

inline common::ErrorInfo SetPngToJpg(const std::string& pngPath, const std::string& jpgPath)
{
    cv::Mat pngImg = cv::imread(pngPath, cv::IMREAD_GRAYSCALE);
	if (pngImg.empty())
	{
		return common::ErrorInfo(-1, "Cant read png, png path is " + pngPath, common::ErrorType::kError);
	}

	int scaleWidth = pngImg.cols;
	int scaleHeight = pngImg.rows;

	cv::Mat resizedImg;
	resize(pngImg, resizedImg, cv::Size(scaleWidth, scaleHeight));

	cv::Mat jpgImg(scaleHeight, scaleWidth, CV_8UC3, cv::Scalar(129, 127, 129));
	resizedImg.copyTo(jpgImg(cv::Rect(0, 0, scaleWidth, scaleHeight)));

	std::vector<int> compression_params;
	compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
	compression_params.push_back(30);
	compression_params.push_back(cv::IMWRITE_JPEG_CHROMA_QUALITY);
	compression_params.push_back(444);

    if (cv::imwrite(jpgPath, jpgImg, compression_params))
    {
        return common::ErrorInfo(-1, "Cant write jpg, jpg path is " + jpgPath, common::ErrorType::kError);
    }
    return common::ErrorInfo();
}

} // namespace cvutils
#endif