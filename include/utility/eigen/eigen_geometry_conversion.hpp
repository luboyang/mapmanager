#ifndef EIGEN_GEOMETRY_CONVERSION
#define EIGEN_GEOMETRY_CONVERSION
#include "cartographer/transform/rigid_transform.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Vector3.h>

namespace EigenGeometryConversion
{

/**
 * @brief 将Eigen::Affine3d转换为geometry_msgs::Pose
 *
 * @param transform Eigen::Affine3d类型的变换矩阵
 * @return geometry_msgs::Pose类型的位姿
 */
inline geometry_msgs::Pose EigenToGeometryMsgsPose(const Eigen::Affine3d &transform)
{
  geometry_msgs::Pose pose;
  pose.position.x = transform.translation().x();
  pose.position.y = transform.translation().y();
  pose.position.z = transform.translation().z();
  Eigen::Quaterniond q(transform.rotation());
  pose.orientation.x = q.x();
  pose.orientation.y = q.y();
  pose.orientation.z = q.z();
  pose.orientation.w = q.w();
  return pose;
}

/**
 * @brief 将geometry_msgs::Pose转换为Eigen::Affine3d
 *
 * @param pose geometry_msgs::Pose类型的位姿
 * @return Eigen::Affine3d类型的变换矩阵
 */
inline Eigen::Affine3d GeometryMsgsToEigenAffine3d(const geometry_msgs::Pose &pose)
{
  Eigen::Translation3d translation(pose.position.x, pose.position.y, pose.position.z);
  Eigen::Quaterniond rotation(pose.orientation.w, pose.orientation.x, pose.orientation.y,
                              pose.orientation.z);
  Eigen::Affine3d transform = translation * rotation;
  return transform;
}

/**
 * @brief 将Eigen::Affine3d转换为geometry_msgs::Transform
 *
 * @param transform Eigen::Affine3d类型的变换矩阵
 * @return geometry_msgs::Transform类型的变换
 */
inline geometry_msgs::Transform EigenToGeometryMsgsTransform(const Eigen::Affine3d &transform)
{
  geometry_msgs::Transform msg;
  msg.translation.x = transform.translation().x();
  msg.translation.y = transform.translation().y();
  msg.translation.z = transform.translation().z();
  Eigen::Quaterniond q(transform.rotation());
  msg.rotation.x = q.x();
  msg.rotation.y = q.y();
  msg.rotation.z = q.z();
  msg.rotation.w = q.w();
  return msg;
}

/**
 * @brief 将geometry_msgs::Transform转换为Eigen::Affine3d
 *
 * @param transform geometry_msgs::Transform类型的变换
 * @return Eigen::Affine3d类型的变换矩阵
 */
inline Eigen::Affine3d GeometryMsgsToEigenAffine3d(const geometry_msgs::Transform &transform)
{
  Eigen::Translation3d translation(transform.translation.x, transform.translation.y,
                                   transform.translation.z);
  Eigen::Quaterniond rotation(transform.rotation.w, transform.rotation.x, transform.rotation.y,
                              transform.rotation.z);
  Eigen::Affine3d affine3d = translation * rotation;
  return affine3d;
}

/**
 * @brief 将geometry_msgs::Vector3转换为Eigen::Vector3d
 *
 * @param vector geometry_msgs::Vector3类型的向量
 * @return Eigen::Vector3d类型的向量
 */
inline Eigen::Vector3d GeometryMsgsToEigenVector3d(const geometry_msgs::Vector3 &vector)
{
  return Eigen::Vector3d(vector.x, vector.y, vector.z);
}

/**
 * @brief 将Eigen::Vector3d转换为geometry_msgs::Vector3
 *
 * @param vector Eigen::Vector3d类型的向量
 * @return geometry_msgs::Vector3类型的向量
 */
inline geometry_msgs::Vector3 EigenToGeometryMsgsVector3(const Eigen::Vector3d &vector)
{
  geometry_msgs::Vector3 msg;
  msg.x = vector.x();
  msg.y = vector.y();
  msg.z = vector.z();
  return msg;
}

/**
 * @brief 将geometry_msgs::Point转换为Eigen::Vector3d
 *
 * @param point geometry_msgs::Point类型的点
 * @return Eigen::Vector3d类型的向量
 */
inline Eigen::Vector3d GeometryMsgsToEigenVector3d(const geometry_msgs::Point &point)
{
  return Eigen::Vector3d(point.x, point.y, point.z);
}

/**
 * @brief 将Eigen::Vector3d转换为geometry_msgs::Point
 *
 * @param vector Eigen::Vector3d类型的向量
 * @return geometry_msgs::Point类型的点
 */
inline geometry_msgs::Point EigenToGeometryMsgsPoint(const Eigen::Vector3d &vector)
{
  geometry_msgs::Point msg;
  msg.x = vector.x();
  msg.y = vector.y();
  msg.z = vector.z();
  return msg;
}

/**
 * @brief 将Eigen::Isometry3d转换为geometry_msgs::Pose
 *
 * @param transform Eigen::Isometry3d类型的变换矩阵
 * @return geometry_msgs::Pose类型的位姿
 */
inline geometry_msgs::Pose EigenToGeometryMsgsPose(const Eigen::Isometry3d &transform)
{
  geometry_msgs::Pose pose;
  pose.position.x = transform.translation().x();
  pose.position.y = transform.translation().y();
  pose.position.z = transform.translation().z();
  Eigen::Quaterniond q(transform.rotation());
  pose.orientation.x = q.x();
  pose.orientation.y = q.y();
  pose.orientation.z = q.z();
  pose.orientation.w = q.w();
  return pose;
}

/**
 * @brief 将geometry_msgs::Pose转换为Eigen::Isometry3d
 *
 * @param pose geometry_msgs::Pose类型的位姿
 * @return Eigen::Isometry3d类型的变换矩阵
 */
inline Eigen::Isometry3d GeometryMsgsToEigenIsometry3d(const geometry_msgs::Pose &pose)
{
  Eigen::Translation3d translation(pose.position.x, pose.position.y, pose.position.z);
  Eigen::Quaterniond rotation(pose.orientation.w, pose.orientation.x, pose.orientation.y,
                              pose.orientation.z);
  Eigen::Isometry3d transform = translation * rotation;
  return transform;
}

/**
 * @brief 将geometry_msgs::Point32转换为Eigen::Vector3d
 *
 * @param point geometry_msgs::Point32类型的点
 * @return Eigen::Vector3d类型的向量
 */
inline Eigen::Vector3d GeometryMsgsToEigenVector3d(const geometry_msgs::Point32 &point)
{
  return Eigen::Vector3d(point.x, point.y, point.z);
}

/**
 * @brief 将Eigen::Vector3d转换为geometry_msgs::Point32
 *
 * @param vector Eigen::Vector3d类型的向量
 * @return geometry_msgs::Point32类型的点
 */
inline geometry_msgs::Point32 EigenToGeometryMsgsPoint32(const Eigen::Vector3d &vector)
{
  geometry_msgs::Point32 msg;
  msg.x = vector.x();
  msg.y = vector.y();
  msg.z = vector.z();
  return msg;
}

/**
 * @brief 将geometry_msgs::Quaternion转换为Eigen::Quaterniond
 *
 * @param quaternion geometry_msgs::Quaternion类型的四元数
 * @return Eigen::Quaterniond类型的四元数
 */
inline Eigen::Quaterniond
GeometryMsgsToEigenQuaterniond(const geometry_msgs::Quaternion &quaternion)
{
  return Eigen::Quaterniond(quaternion.w, quaternion.x, quaternion.y, quaternion.z);
}

/**
 * @brief 将Eigen::Quaterniond转换为geometry_msgs::Quaternion
 *
 * @param quaternion Eigen::Quaterniond类型的四元数
 * @return geometry_msgs::Quaternion类型的四元数
 */
inline geometry_msgs::Quaternion EigenToGeometryMsgsQuaternion(const Eigen::Quaterniond &quaternion)
{
  geometry_msgs::Quaternion msg;
  msg.w = quaternion.w();
  msg.x = quaternion.x();
  msg.y = quaternion.y();
  msg.z = quaternion.z();
  return msg;
}

/**
 * @brief 将geometry_msgs::Pose转换为Eigen::Vector3d
 *
 * @param pose geometry_msgs::Pose类型的位姿
 * @return Eigen::Vector3d类型的向量
 */
inline Eigen::Vector3d GeometryMsgsToEigenVector3d(const geometry_msgs::Pose &pose)
{
  return Eigen::Vector3d(pose.position.x, pose.position.y, pose.position.z);
}

/**
 * @brief 将Eigen::Vector3d转换为geometry_msgs::Pose
 *
 * @param vector Eigen::Vector3d类型的向量
 * @return geometry_msgs::Pose类型的位姿
 */
inline geometry_msgs::Pose EigenToGeometryMsgsPose(const Eigen::Vector3d &vector)
{
  geometry_msgs::Pose pose;
  pose.position.x = vector.x();
  pose.position.y = vector.y();
  pose.position.z = vector.z();
  pose.orientation.x = 0;
  pose.orientation.y = 0;
  pose.orientation.z = 0;
  pose.orientation.w = 1;
  return pose;
}

/**
 * @brief 将geometry_msgs::Point转换为Eigen::Vector3f
 *
 * @param point geometry_msgs::Point类型的点
 * @return Eigen::Vector3f类型的向量
 */
inline Eigen::Vector3f GeometryMsgsToEigenVector3f(const geometry_msgs::Point &point)
{
  return Eigen::Vector3f(point.x, point.y, point.z);
}

/**
 * @brief 将Eigen::Vector3f转换为geometry_msgs::Point
 *
 * @param vector Eigen::Vector3f类型的向量
 * @return geometry_msgs::Point类型的点
 */
inline geometry_msgs::Point EigenToGeometryMsgsPoint(const Eigen::Vector3f &vector)
{
  geometry_msgs::Point msg;
  msg.x = vector.x();
  msg.y = vector.y();
  msg.z = vector.z();
  return msg;
}

/**
 * @brief 将geometry_msgs::Quaternion转换为Eigen::Quaternionf
 *
 * @param quaternion geometry_msgs::Quaternion类型的四元数
 * @return Eigen::Quaternionf类型的四元数
 */
inline Eigen::Quaternionf
GeometryMsgsToEigenQuaternionf(const geometry_msgs::Quaternion &quaternion)
{
  return Eigen::Quaternionf(quaternion.w, quaternion.x, quaternion.y, quaternion.z);
}

/**
 * @brief 将Eigen::Quaternionf转换为geometry_msgs::Quaternion
 *
 * @param quaternion Eigen::Quaternionf类型的四元数
 * @return geometry_msgs::Quaternion类型的四元数
 */
inline geometry_msgs::Quaternion EigenToGeometryMsgsQuaternion(const Eigen::Quaternionf &quaternion)
{
  geometry_msgs::Quaternion msg;
  msg.w = quaternion.w();
  msg.x = quaternion.x();
  msg.y = quaternion.y();
  msg.z = quaternion.z();
  return msg;
}

/**
 * @brief 将geometry_msgs::Point32转换为Eigen::Vector3f
 *
 * @param point geometry_msgs::Point32类型的点
 * @return Eigen::Vector3f类型的向量
 */
inline Eigen::Vector3f GeometryMsgsToEigenVector3f(const geometry_msgs::Point32 &point)
{
  return Eigen::Vector3f(point.x, point.y, point.z);
}

/**
 * @brief 将Eigen::Vector3f转换为geometry_msgs::Point32
 *
 * @param vector Eigen::Vector3f类型的向量
 * @return geometry_msgs::Point32类型的点
 */
inline geometry_msgs::Point32 EigenToGeometryMsgsPoint32(const Eigen::Vector3f &vector)
{
  geometry_msgs::Point32 msg;
  msg.x = vector.x();
  msg.y = vector.y();
  msg.z = vector.z();
  return msg;
}

// 将Rigid3d类型转换为Eigen::Isometry3d类型
inline Eigen::Isometry3d Rigid3dToEigenIsometry3d(const cartographer::transform::Rigid3d &rigid)
{
  return Eigen::Translation3d(rigid.translation()) * rigid.rotation();
}

// 将Rigid3d类型转换为Eigen::Quaterniond类型
inline Eigen::Quaterniond Rigid3dToEigenQuaternion(const cartographer::transform::Rigid3d &rigid)
{
  return Eigen::Quaterniond(rigid.rotation());
}

// 将geometry_msgs::Pose转换为Rigid3d类型
inline cartographer::transform::Rigid3d GeometryMsgsPoseToRigid3d(const geometry_msgs::Pose &pose)
{
  Eigen::Quaterniond quaternion(pose.orientation.w, pose.orientation.x, pose.orientation.y,
                                pose.orientation.z);
  Eigen::Vector3d translation(pose.position.x, pose.position.y, pose.position.z);
  return cartographer::transform::Rigid3d(translation, quaternion);
}

// 将Rigid3d类型转换为geometry_msgs::Pose类型
inline geometry_msgs::Pose Rigid3dToGeometryMsgsPose(const cartographer::transform::Rigid3d &rigid)
{
  geometry_msgs::Pose pose;
  pose.position.x = rigid.translation().x();
  pose.position.y = rigid.translation().y();
  pose.position.z = rigid.translation().z();
  Eigen::Quaterniond quaternion(rigid.rotation());
  pose.orientation.x = quaternion.x();
  pose.orientation.y = quaternion.y();
  pose.orientation.z = quaternion.z();
  pose.orientation.w = quaternion.w();
  return pose;
}

// 将Rigid3d类型转换为Eigen::Affine3d类型
inline Eigen::Affine3d Rigid3dToEigenAffine3d(const cartographer::transform::Rigid3d &rigid)
{
  return Eigen::Translation3d(rigid.translation()) * rigid.rotation();
}

/**
 * @brief 将位姿转换为变换矩阵
 *
 * @param pose 位姿
 * @return geometry_msgs::Transform 变换矩阵
 */
inline geometry_msgs::Transform GeometryMsgsPoseToTransform(const geometry_msgs::Pose &pose)
{
  geometry_msgs::Transform transform;
  transform.translation.x = pose.position.x;
  transform.translation.y = pose.position.y;
  transform.translation.z = pose.position.z;
  transform.rotation.x = pose.orientation.x;
  transform.rotation.y = pose.orientation.y;
  transform.rotation.z = pose.orientation.z;
  transform.rotation.w = pose.orientation.w;
  return transform;
}

/**
 * @brief 将变换矩阵转换为位姿
 *
 * @param transform 变换矩阵
 * * @return geometry_msgs::Pose 位姿
 */
inline geometry_msgs::Pose GeometryMsgsTransformToPose(const geometry_msgs::Transform &transform)
{
  geometry_msgs::Pose pose;
  pose.position.x = transform.translation.x;
  pose.position.y = transform.translation.y;
  pose.position.z = transform.translation.z;
  pose.orientation.x = transform.rotation.x;
  pose.orientation.y = transform.rotation.y;
  pose.orientation.z = transform.rotation.z;
  pose.orientation.w = transform.rotation.w;
  return pose;
}

} // namespace EigenGeometryConversion
#endif
