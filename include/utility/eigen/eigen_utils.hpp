#ifndef EIGEN_UTILS
#define EIGEN_UTILS
#include <map>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <cairo/cairo.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/id.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Point32.h>
#include "utility/eigen/eigen_geometry_conversion.hpp"

namespace EigenUtils
{
const int kPaddingPixel = 0;
using UniqueCairoSurfacePtr = std::unique_ptr<cairo_surface_t, void (*)(cairo_surface_t *)>;
using UniqueCairoPtr = std::unique_ptr<cairo_t, void (*)(cairo_t *)>;
constexpr cairo_format_t kCairoFormat = CAIRO_FORMAT_ARGB32;

enum EXTREME_VALUE
{
  MIN,
  MAX
};

inline double GetExtremeValue(const std::vector<double> &values, const EXTREME_VALUE &type)
{
  if (type == MIN)
  {
    double min_value = values[0];
    for (auto &it : values)
    {
      if (min_value > it)
      {
        min_value = it;
      }
    }
    return min_value;
  }
  else
  {
    double max_value = values[0];
    for (auto &it : values)
    {
      if (max_value < it)
      {
        max_value = it;
      }
    }
    return max_value;
  }
}

inline UniqueCairoSurfacePtr MakeUniqueCairoSurfacePtr(cairo_surface_t *surface)
{
  return UniqueCairoSurfacePtr(surface, cairo_surface_destroy);
}

inline UniqueCairoPtr MakeUniqueCairoPtr(cairo_t *surface)
{
  return UniqueCairoPtr(surface, cairo_destroy);
}

inline void CairoPaintSubmapSlice(const double scale, const cartographer::io::SubmapSlice &submap,
                           cairo_t *cr,
                           std::function<void(const cartographer::io::SubmapSlice &)> draw_callback)
{
  cairo_scale(cr, scale, scale);
  const auto &submap_slice = submap;
  if (submap_slice.surface == nullptr)
  {
    return;
  }
  const Eigen::Matrix4d homo = EigenGeometryConversion::Rigid3dToEigenAffine3d(submap_slice.slice_pose).matrix();

  cairo_save(cr);
  cairo_matrix_t matrix;
  cairo_matrix_init(&matrix, homo(1, 0), homo(0, 0), -homo(1, 1), -homo(0, 1), homo(0, 3),
                    -homo(1, 3));
  cairo_transform(cr, &matrix);

  const double submap_resolution = submap_slice.resolution;
  cairo_scale(cr, submap_resolution, submap_resolution);

  // Invokes caller's callback to utilize slice data in global cooridnate
  // frame. e.g. finds bounding box, paints slices.
  draw_callback(submap_slice);
  cairo_restore(cr);
}

inline void CairoPaintSubmapsSlices(
    const double scale,
    const std::map<::cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> &submaps,
    cairo_t *cr, std::function<void(const cartographer::io::SubmapSlice &)> draw_callback)
{
  cairo_pattern_set_filter(cairo_get_source(cr), CAIRO_FILTER_NEAREST);
  cairo_scale(cr, scale, scale);
  for (auto &pair : submaps)
  {
    const auto &submap_slice = pair.second;
    if (submap_slice.surface == nullptr)
    {
      return;
    }
    const Eigen::Matrix4d homo =
        EigenGeometryConversion::Rigid3dToEigenAffine3d(submap_slice.pose * submap_slice.slice_pose).matrix();

    cairo_save(cr);
    cairo_matrix_t matrix;
    cairo_matrix_init(&matrix, homo(1, 0), homo(0, 0), -homo(1, 1), -homo(0, 1), homo(0, 3),
                      -homo(1, 3));
    cairo_transform(cr, &matrix);

    const double submap_resolution = submap_slice.resolution;
    cairo_scale(cr, submap_resolution, submap_resolution);

    // Invokes caller's callback to utilize slice data in global cooridnate
    // frame. e.g. finds bounding box, paints slices.
    draw_callback(submap_slice);
    cairo_restore(cr);
  }
}

inline cartographer::io::PaintSubmapSlicesResult PaintSubmapSlice(cartographer::io::SubmapSlice &submap,
                                                           const double resolution)
{
  Eigen::AlignedBox2f bounding_box;
  {
    auto surface =
        MakeUniqueCairoSurfacePtr(cairo_image_surface_create(cartographer::io::kCairoFormat, 1, 1));
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    const auto update_bounding_box = [&bounding_box, &cr](double x, double y)
    {
      cairo_user_to_device(cr.get(), &x, &y);
      bounding_box.extend(Eigen::Vector2f(x, y));
    };

    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&update_bounding_box](const cartographer::io::SubmapSlice &submap_slice)
                          {
                            update_bounding_box(0, 0);
                            update_bounding_box(submap_slice.width, 0);
                            update_bounding_box(0, submap_slice.height);
                            update_bounding_box(submap_slice.width, submap_slice.height);
                          });
  }

  const Eigen::Array2i size(std::ceil(bounding_box.sizes().x()) + 2 * kPaddingPixel,
                            std::ceil(bounding_box.sizes().y()) + 2 * kPaddingPixel);
  const Eigen::Array2f origin(-bounding_box.min().x() + kPaddingPixel,
                              -bounding_box.min().y() + kPaddingPixel);

  auto surface = MakeUniqueCairoSurfacePtr(
      cairo_image_surface_create(cartographer::io::kCairoFormat, size.x(), size.y()));
  {
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    cairo_set_source_rgba(cr.get(), 0.5, 0.0, 0.0, 1.);
    cairo_paint(cr.get());
    cairo_translate(cr.get(), origin.x(), origin.y());
    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&cr](const cartographer::io::SubmapSlice &submap_slice)
                          {
                            cairo_set_source_surface(cr.get(), submap_slice.surface.get(), 0., 0.);
                            //                              cairo_set_antialias (cr.get(),
                            //                              CAIRO_ANTIALIAS_NONE);
                            //                              cairo_pattern_set_filter(cairo_get_source(cr.get()),CAIRO_FILTER_NEAREST);
                            cairo_paint(cr.get());
                          });
    cairo_surface_flush(surface.get());
  }
  return cartographer::io::PaintSubmapSlicesResult(std::move(surface), origin);
}

inline cartographer::io::PaintSubmapSlicesResult PaintSubmapsSlices(
    const std::map<::cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> &submaps,
    const double resolution)
{
  Eigen::AlignedBox2f bounding_box;
  {
    auto surface =
        MakeUniqueCairoSurfacePtr(cairo_image_surface_create(cartographer::io::kCairoFormat, 1, 1));
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));

    const auto update_bounding_box = [&bounding_box, &cr](double x, double y)
    {
      cairo_user_to_device(cr.get(), &x, &y);
      bounding_box.extend(Eigen::Vector2f(x, y));
    };

    CairoPaintSubmapsSlices(
        1. / resolution, submaps, cr.get(),
        [&update_bounding_box](const cartographer::io::SubmapSlice &submap_slice)
        {
          update_bounding_box(0, 0);
          update_bounding_box(submap_slice.width, 0);
          update_bounding_box(0, submap_slice.height);
          update_bounding_box(submap_slice.width, submap_slice.height);
        });
  }

  const Eigen::Array2i size(std::ceil(bounding_box.sizes().x()) + 2 * kPaddingPixel,
                            std::ceil(bounding_box.sizes().y()) + 2 * kPaddingPixel);
  const Eigen::Array2f origin(-bounding_box.min().x() + kPaddingPixel,
                              -bounding_box.min().y() + kPaddingPixel);

  auto surface = MakeUniqueCairoSurfacePtr(
      cairo_image_surface_create(cartographer::io::kCairoFormat, size.x(), size.y()));

  {
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    cairo_set_source_rgba(cr.get(), 0.5, 0.0, 0.0, 1.);
    cairo_paint(cr.get());
    cairo_translate(cr.get(), origin.x(), origin.y());
    CairoPaintSubmapsSlices(1. / resolution, submaps, cr.get(),
                            [&cr](const cartographer::io::SubmapSlice &submap_slice)
                            {
                              cairo_set_source_surface(cr.get(), submap_slice.surface.get(), 0.,
                                                       0.);
                              //                              cairo_set_antialias (cr.get(),
                              //                              CAIRO_ANTIALIAS_NONE);
                              //                              cairo_pattern_set_filter(cairo_get_source(cr.get()),CAIRO_FILTER_NEAREST);
                              cairo_paint(cr.get());
                            });
    cairo_surface_flush(surface.get());
  }
  return cartographer::io::PaintSubmapSlicesResult(std::move(surface), origin);
}

inline double GetYaw(const Eigen::Quaterniond &rotation)
{
  const Eigen::Matrix<double, 3, 1> direction = rotation * Eigen::Matrix<double, 3, 1>::UnitX();
  return atan2(direction.y(), direction.x());
}

inline double GetYaw(geometry_msgs::Pose p)
{
  Eigen::Isometry3d pose;
  Eigen::Quaterniond q =
      Eigen::Quaterniond(p.orientation.w, p.orientation.x, p.orientation.y, p.orientation.z);
  double yaw = GetYaw(q);
  return yaw;
}

inline Eigen::AlignedBox2d GetBox(Eigen::Isometry3d pose, Eigen::Vector2d sides, const double &resolution)
{
  Eigen::AlignedBox2d box;
  double width, heigth;
  width = -sides[0] * 1.0;
  heigth = -sides[1] * 1.0;
  double inv_resolution = 1 / resolution;
  pose(0, 3) *= inv_resolution * 1.0;
  ;
  pose(1, 3) *= inv_resolution * 1.0;
  ;

  Eigen::Vector3d point0, point1, point2, point3;
  /*
    0        1
    ---------
    |       |
    |       |
    ---------
    2        3
  */
  point0 = Eigen::Vector3d(0, 0, 0);
  point1 = Eigen::Vector3d(0, width, 0);
  point2 = Eigen::Vector3d(heigth, 0, 0);
  point3 = Eigen::Vector3d(heigth, width, 0);

  point0 = pose * point0;
  point1 = pose * point1;
  point2 = pose * point2;
  point3 = pose * point3;

  std::vector<double> set_x, set_y;
  set_x.push_back(point0(0));
  set_x.push_back(point1(0));
  set_x.push_back(point2(0));
  set_x.push_back(point3(0));
  set_y.push_back(point0(1));
  set_y.push_back(point1(1));
  set_y.push_back(point2(1));
  set_y.push_back(point3(1));
  double min_x, min_y, max_x, max_y;
  min_x = GetExtremeValue(set_y, MIN);
  min_y = GetExtremeValue(set_x, MIN);
  max_x = GetExtremeValue(set_y, MAX);
  max_y = GetExtremeValue(set_x, MAX);

  box = Eigen::AlignedBox2d(Eigen::Vector2d(min_x, min_y), Eigen::Vector2d(max_x, max_y));
  return box;
}

inline const Eigen::Vector2d GetMax(const cartographer::transform::Rigid3d &slice_pose,
                             const cartographer::transform::Rigid3d &local_pose)
{
  Eigen::Vector3d trans = slice_pose.translation();
  Eigen::Vector3d max1 = local_pose * trans;
  Eigen::Vector2d max;
  max(0) = max1(0);
  max(1) = max1(1);
  return max;
}

inline void Image2worldLoc(const Eigen::Vector2d &max, const double &resolution,
                    const Eigen::Vector2i &imageLoc, Eigen::Vector2d &worldLoc)
{
  worldLoc[0] = max(0) - imageLoc[1] * resolution;
  worldLoc[1] = max(1) - imageLoc[0] * resolution;
}

inline void World2imageLoc(const Eigen::Vector2d &max, const double &resolution,
                    const Eigen::Vector2d &worldLoc, Eigen::Vector2i &imageLoc)
{
  imageLoc[0] = (max(1) - worldLoc(1)) / resolution;
  imageLoc[1] = (max(0) - worldLoc(0)) / resolution;
}
}; // namespace EigenGeometryConversion

#endif