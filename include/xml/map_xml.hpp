#ifndef MAP_XML
#define MAP_XML
#include <vector>
#include <string>
#include "common/datatype/file_data_type/file_data.hpp"
#include "utility/system/utils_system.hpp"
#include "utility/string/utils_string.hpp"
#include "tinyxml2.h"

inline void setXmlEDocument(XMLDocument& doc, const std::string& name, const std::string& value)
{
	XMLElement* ele = doc.NewElement(name.c_str());
	ele->InsertFirstChild(doc.NewText(value.c_str()));
	doc.InsertEndChild(ele);
}

inline void setXmlElement(XMLDocument& doc, XMLElement* ele, const std::string& name, const std::string& value)
{
	XMLElement* userNode = doc.NewElement(name.c_str());
	userNode->InsertFirstChild(doc.NewText(value.c_str()));
	//4.3���ý�����ǩ
	ele->InsertEndChild(userNode);
}

class MapXml
{
public:
	MapXml() {};
	MapXml(std::vector<file_data_type::MapData> nodeList)
		: nodeCount_(nodeList.size()), nodeList_(nodeList) {};
	~MapXml() {};

	bool SetXml(const std::string& path)
	{
		std::string systemCommand = "mkdir -p " + path;
		std::string systemCommandErrorMsg;
		if (!systemutils::CheckSystemStatus(systemCommand, systemCommandErrorMsg))
		{
			printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
			return false;
		}
		if (0 == nodeCount_)
		{
			printf("NodeList is empty!");
			return false;
		}
		XMLDocument doc;
		XMLDeclaration* declaration = doc.NewDeclaration();
		doc.InsertFirstChild(declaration);
		XMLElement* pmap = doc.NewElement("map");
		doc.InsertFirstChild(pmap);
		doc.InsertEndChild(pmap);

		XMLElement* pnode_list = doc.NewElement("node_list");
		pmap->InsertFirstChild(pnode_list);
		pmap->InsertEndChild(pnode_list);

		for (const auto& node : nodeList_)
		{
			XMLElement* pnode = doc.NewElement("node");
			pnode_list->InsertFirstChild(pnode);
			pnode_list->InsertEndChild(pnode);
			setXmlElement(doc, pnode, "id", std::to_string(node.id));

			XMLElement* pneighbour_list = doc.NewElement("neighbour_list");
			pnode->InsertFirstChild(pneighbour_list);
			pnode->InsertEndChild(pneighbour_list);
			for (const auto& neighbour : node.neighbourList)
			{
				XMLElement* pneighbour = doc.NewElement("neighbour");
				pneighbour_list->InsertFirstChild(pneighbour);
				pneighbour_list->InsertEndChild(pneighbour);

				setXmlElement(doc, pneighbour, "id", std::to_string(neighbour.id));
				std::string transformStr;
				stringutils::PoseXmlToString(neighbour.transform, transformStr, true);
				setXmlElement(doc, pneighbour, "transform", transformStr);
				setXmlElement(doc, pneighbour, "reachable", neighbour.reachable ? "true" : "false");
			}

		}
		XMLElement* pproperty = doc.NewElement("property");
		pmap->InsertFirstChild(pproperty);
		pmap->InsertEndChild(pproperty);
		setXmlElement(doc, pproperty, "node_count", std::to_string(nodeList_.size()));
		string save_path = path + "/map.xml";
		cout << "save_path: " << save_path << endl;
		return doc.SaveFile(save_path.c_str());
	}

	bool GetXml(const std::string& path)
	{
		XMLDocument doc;
		string file_path = path + "/map.xml";
		cout << "file_path: " << file_path << endl;
		if (XML_SUCCESS != doc.LoadFile(file_path.c_str()))
		{
			cout << "load xml file failed!" << endl;
			return false;
		}
		XMLElement* pmap = doc.RootElement();
		XMLElement* pnode_list = pmap->FirstChildElement("node_list");
		XMLElement* pproperty = pmap->FirstChildElement("property");
		XMLElement* pnode = pnode_list->FirstChildElement();
		file_data_type::MapData tmpNode;
		file_data_type::NeighbourInMapData neighbourInNode;
		nodeList_.clear();
		while (pnode != NULL)
		{
			tmpNode.id = pnode->FirstChildElement("id")->IntText();
			XMLElement* pneighbour_list = pnode->FirstChildElement("neighbour_list");
			XMLElement* pneighbour = pneighbour_list->FirstChildElement();
			while (pneighbour != NULL)
			{
				neighbourInNode.id = pneighbour->FirstChildElement("id")->IntText();
				string tmpPoseStr = pneighbour->FirstChildElement("transform")->GetText();
				stringutils::PoseStringToXml(tmpPoseStr, neighbourInNode.transform, true);
				neighbourInNode.reachable = pneighbour->FirstChildElement("reachable")->BoolText();
				pneighbour = pneighbour->NextSiblingElement();
				tmpNode.neighbourList.emplace_back(std::move(neighbourInNode));
			}
			nodeList_.emplace_back(std::move(tmpNode));
			pnode = pnode->NextSiblingElement();
		}
		this->nodeCount_ = this->nodeList_.size();
		return true;
	}

	int GetNodeCount() { return nodeCount_; };
	std::vector<file_data_type::MapData> GetNodeList() { return nodeList_; };
private:
	int nodeCount_;
	std::vector<file_data_type::MapData> nodeList_;
};
}

#endif