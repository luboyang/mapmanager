#ifndef CORNER_POINTS_XML
#define CORNER_POINTS_XML
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <common/data.hpp>
namespace cornerpointsxml
{
class CornerPointsXml{
    public:
    CornerPointsXml(){};
    CornerPointsXml(const std::vector<data::CornerPointsNode> &CornerPointsNodeList):cornerPointsNodeList_(CornerPointsNodeList){};
    bool SetCornerPointsXml(const std::string &path){
        if(cornerPointsNodeList_.empty())
        {
            printf("CornerPointsNodeList is empty!");
            return false;
        }
        boost::property_tree::ptree p_info;
        boost::property_tree::ptree p_list;
        std::string pointsStr;
        for(const auto&cornerPointsNode:cornerPointsNodeList_)
        {
            p_info.put("points_size", cornerPointsNode.points_size);
            for(const auto &point:cornerPointsNode.point_in_map)
            {
                 pointsStr = std::to_string(point.x())+" "+
                        std::to_string(point.y())+" ";
            }
            p_info.put("points", pointsStr);
            p_list.add_child("corner_points", p_info);
        }
        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(path + "/corner_points.xml", p_list, std::locale(),
                                    setting);
        return true;
    }

    
    bool GetCornerPointsXml(const std::string &path)
    {
        boost::property_tree::ptree cornerPointsXmlPt;
        boost::property_tree::xml_parser::read_xml(path+"/corner_points.xml", cornerPointsXmlPt);
        std::string xml;
        if(cornerPointsXmlPt.empty()){
            printf("corner_points.xml is empty! Path is %s", path.c_str());
            return false;
        }
        cornerPointsNodeList_.clear();
        cornerPointsNodeList_.reserve(cornerPointsXmlPt.size());
        data::CornerPointsNode tmpCornerPointsNode;
        for (const auto &mark : cornerPointsXmlPt)
        {
            if (mark.first == "corner_points")
            {
                tmpCornerPointsNode.points_size=std::stoi(mark.second.get("points_size",""));
                xml = mark.second.get("points"," ");
            // 默认为4，待商榷
                tmpCornerPointsNode.point_in_map.resize(4);
                sscanf(xml.c_str(), "%lf %lf %lf %lf %lf %lf %lf %lf",
                        &tmpCornerPointsNode.point_in_map[0].x(), &tmpCornerPointsNode.point_in_map[0].y(), 
                        &tmpCornerPointsNode.point_in_map[1].x(), &tmpCornerPointsNode.point_in_map[1].y(), 
                        &tmpCornerPointsNode.point_in_map[2].x(), &tmpCornerPointsNode.point_in_map[2].y(), 
                        &tmpCornerPointsNode.point_in_map[3].x(), &tmpCornerPointsNode.point_in_map[3].y());
            }
            cornerPointsNodeList_.emplace_back(std::move(tmpCornerPointsNode));
        }

        return true;
    }

    std::vector<data::CornerPointsNode> GetNodeList(){
        return cornerPointsNodeList_;
    }
    private:
    std::vector<data::CornerPointsNode> cornerPointsNodeList_;
};
};

#endif