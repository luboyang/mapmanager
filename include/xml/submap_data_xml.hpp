#ifndef LASER_DATA_XML
#define LASER_DATA_XML
#include <vector>
#include <string>
#include "common/datatype/file_data_type/file_data.hpp"
#include "utility/system/utils_system.hpp"
#include "utility/string/utils_string.hpp"
#include "tinyxml2.h"

inline void setXmlEDocument(XMLDocument& doc, const std::string& name, const std::string& value)
{
	XMLElement* ele = doc.NewElement(name.c_str());
	ele->InsertFirstChild(doc.NewText(value.c_str()));
	doc.InsertEndChild(ele);
}

inline void setXmlElement(XMLDocument& doc, XMLElement* ele, const std::string& name, const std::string& value)
{
	XMLElement* userNode = doc.NewElement(name.c_str());
	userNode->InsertFirstChild(doc.NewText(value.c_str()));
	//4.3���ý�����ǩ
	ele->InsertEndChild(userNode);
}

class SubmapDataXml
{
public:
	SubmapDataXml() {};
	SubmapDataXml(file_data_type::SubmapData newNode) : node_(newNode) {};
	~SubmapDataXml() {};
	bool SetXml(const std::string& path)
	{
		std::string folderStringSubmap = path + "/frames/" + std::to_string(node_.id);
		std::string systemCommand = "mkdir -p " + folderStringSubmap;
		std::string systemCommandErrorMsg;
		if (!systemutils::CheckSystemStatus(systemCommand, systemCommandErrorMsg))
		{
			printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
			return false;
		}

		XMLDocument doc;
		XMLDeclaration* declaration = doc.NewDeclaration();
		doc.InsertFirstChild(declaration);
		setXmlEDocument(doc, "id", std::to_string(node_.id));
		std::string poseStr;
		stringutils::PoseXmlToString(node_.pose, poseStr);
		setXmlEDocument(doc, "pose", poseStr);
		std::string localPoseStr;
		stringutils::PoseXmlToString(node_.localPose, localPoseStr);
		setXmlEDocument(doc, "local_pose", localPoseStr);
		setXmlEDocument(doc, "num_range_data", std::to_string(node_.numRangeData));
		setXmlEDocument(doc, "finished", node_.finished ? "true" : "false");
		XMLElement* pProbabilityGrid = doc.NewElement("probability_grid");
		doc.InsertFirstChild(pProbabilityGrid);
		doc.InsertEndChild(pProbabilityGrid);
		setXmlElement(doc, pProbabilityGrid, "resolution", std::to_string(node_.resolution));
		std::string maxStr = std::to_string(node_.max[0]) + " " +
			std::to_string(node_.max[1]);
		setXmlElement(doc, pProbabilityGrid, "max", maxStr);
		std::string numCellsStr = std::to_string(node_.numCells[0]) + " " +
			std::to_string(node_.numCells[1]);
		setXmlElement(doc, pProbabilityGrid, "num_cells", numCellsStr);
		std::string knownCellsBoxStr = std::to_string(node_.knownCellsBox[0]) + " " +
			std::to_string(node_.knownCellsBox[1]) + " " +
			std::to_string(node_.knownCellsBox[2]) + " " +
			std::to_string(node_.knownCellsBox[3]);
		setXmlElement(doc, pProbabilityGrid, "known_cells_box", knownCellsBoxStr);
		string save_path = folderStringSubmap + "/data.xml";
		cout << "save_path: " << save_path << endl;
		return doc.SaveFile(save_path.c_str());
	}

	bool GetXml(const std::string& path)
	{
		XMLDocument doc;
		string file_path = path + "/data.xml";
		cout << "file_path: " << file_path << endl;
		if (XML_SUCCESS != doc.LoadFile(file_path.c_str()))
		{
			cout << "load xml file failed!" << endl;
			return false;
		}
		node_.id = std::atoi(doc.FirstChildElement("id")->GetText());
		node_.finished = doc.FirstChildElement("finished")->BoolText();
		node_.numRangeData = std::atoi(doc.FirstChildElement("num_range_data")->GetText());
		std::string poseStr = doc.FirstChildElement("pose")->GetText();
		stringutils::PoseStringToXml(poseStr, node_.pose);
		std::string localPoseStr = doc.FirstChildElement("local_pose")->GetText();
		stringutils::PoseStringToXml(localPoseStr, node_.localPose);
		std::string maxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("max")->GetText();
		sscanf(maxStr.c_str(), "%lf %lf", &node_.max[0], &node_.max[1]);
		std::string numCellsStr = doc.FirstChildElement("probability_grid")->FirstChildElement("num_cells")->GetText();
		sscanf(numCellsStr.c_str(), "%d %d", &node_.numCells[0], &node_.numCells[1]);
		std::string knownCellsBoxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("known_cells_box")->GetText();
		sscanf(knownCellsBoxStr.c_str(), "%d %d %d %d", &node_.knownCellsBox[0], &node_.knownCellsBox[1],
			&node_.knownCellsBox[2], &node_.knownCellsBox[3]);
		node_.resolution = doc.FirstChildElement("probability_grid")->FirstChildElement("known_cells_box")->DoubleText();
		return true;
	}

	file_data_type::SubmapData GetNode() { return node_; }
private:
	file_data_type::SubmapData node_;
};

#endif