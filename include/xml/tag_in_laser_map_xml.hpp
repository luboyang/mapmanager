#ifndef TAG_IN_LASER_MAP_XML
#define TAG_IN_LASER_MAP_XML
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <common/data.hpp>
#include <geometry_msgs/Pose.h>
#include <string>
#include <utility/string/utils_string.hpp>
#include <vector>
namespace taginlasermapxml
{
class TagInLaserMapXml
{
  public:
  TagInLaserMapXml(){};
  TagInLaserMapXml(std::vector<data::TagInLaserMapNode> nodeList) : nodeList_(nodeList){};
  ~TagInLaserMapXml(){};

  bool SetTagInLaserMapXml(const std::string &path)
  {
    if (0 == nodeList_.size())
    {
      printf("NodeList is empty!");
      return false;
    }
    boost::property_tree::ptree pTagInLaserMap;
    boost::property_tree::ptree pTag;
    std::string poseStr;
    for (const auto &tag : nodeList_)
    {
      pTag.put("tag_id", tag.tagId);
      stringutils::PoseXmlToString(tag.tagInLaserMapPose, poseStr);
      pTag.put("tag_in_laser_map_pose", poseStr);
      pTagInLaserMap.add_child("tag", pTag);
    }
    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    boost::property_tree::write_xml(path + "/tag_in_laser_map.xml", pTagInLaserMap, std::locale(),
                                    setting);
    return true;
  };

  bool GetTagInLaserMapXml(const std::string &path)
  {
    boost::property_tree::ptree tagInLaserMapXmlPt;
    boost::property_tree::xml_parser::read_xml(path + "/tag_in_laser_map.xml", tagInLaserMapXmlPt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (tagInLaserMapXmlPt.empty())
    {
      printf("tag_in_laser_map.xml is empty! Path is %s", path.c_str());
      return false;
    }
    data::TagInLaserMapNode tmpNode;
    std::string poseStr;
    nodeList_.clear();
    nodeList_.reserve(tagInLaserMapXmlPt.size());
    for (const auto &tag : tagInLaserMapXmlPt)
    {
      if (tag.first == "tag")
      {
        tmpNode.tagId = std::stoi(tag.second.get("tag_id", "").c_str());
        poseStr = tag.second.get("tag_in_laser_map_pose", "");
        stringutils::PoseStringToXml(poseStr, tmpNode.tagInLaserMapPose);
        nodeList_.emplace_back(std::move(tmpNode));
      }
    }
    return true;
  };

  std::vector<data::TagInLaserMapNode> GetNodeList() { return nodeList_; }

  private:
  std::vector<data::TagInLaserMapNode> nodeList_;
};
}; // namespace taginlasermapxml
#endif