#ifndef MAP_DATA_XML
#define MAP_DATA_XML
#include <string>
#include <geometry_msgs/Pose.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <utility/string/utils_string.hpp>
#include <common/data.hpp>
namespace mapdataxml
{
    class MapDataXml
    {
    public:
        MapDataXml(){};
        MapDataXml(data::MapDataNode newNode) : node_(newNode){};
        ~MapDataXml(){};

        bool SetMapDataXml(const std::string &path)
        {
            boost::property_tree::ptree pMap,pTop;

            pMap.put("width", std::to_string(node_.width));
            pMap.put("height", std::to_string(node_.height));
            pMap.put("resolution", std::to_string(node_.resolution));
            std::string transformStr;
            stringutils::PoseXmlToString(node_.transform, transformStr);
            pMap.put("pose", transformStr);
            pTop.add_child("mapPng", pMap);

            boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
            boost::property_tree::write_xml(path + "/map_data.xml", pTop,
                                        std::locale(), setting);
            return true;
        };

        bool GetMapDataXml(const std::string &path)
        {
            boost::property_tree::ptree mapDataXmlPt;
            boost::property_tree::ptree pMapPng;
            boost::property_tree::read_xml(path + "/map_data.xml", mapDataXmlPt);
            if(mapDataXmlPt.empty())
            {
                printf("map_data.xml is empty! Path is %s", path.c_str());
                return false;
            }
            pMapPng = mapDataXmlPt.get_child("mapPng");

            node_.width = pMapPng.get<int>("width");
            node_.height = pMapPng.get<int>("height");
            node_.resolution = pMapPng.get<double>("resolution");
            std::string tmpPoseStr = pMapPng.get<std::string>("pose");
            stringutils::PoseStringToXml(tmpPoseStr, node_.transform);
            return true;
        };

        data::MapDataNode GetNode()
        {
            return node_;
        }
    private:
        data::MapDataNode node_;
    };
};
#endif