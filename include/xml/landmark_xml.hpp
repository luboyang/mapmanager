#ifndef LANDMARK_XML
#define LANDMARK_XML
#include <vector>
#include <string>
#include <geometry_msgs/Pose.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <utility/string/utils_string.hpp>
#include <common/data.hpp>
namespace landmarkxml
{
    class LandmarkXml
    {
    public:
        LandmarkXml(){};
        LandmarkXml(std::vector<data::LandmarkNode> nodeList) : nodeCount_(nodeList.size()),
                                                          nodeList_(nodeList){};
        ~LandmarkXml(){};

        bool SetLandmarkXml(const std::string &path)
        {
            if(0 == nodeCount_)
            {
                printf("NodeList is empty!");
                return false;
            }
            std::string transformStr;
            std::string translationInImageStr;
            boost::property_tree::ptree pLandmarkInfo;
            boost::property_tree::ptree pNeighbourList;

            for (const auto &landmark : nodeList_)
            {
                pLandmarkInfo.put("ns", landmark.ns);
                pLandmarkInfo.put("id", landmark.id);
                pLandmarkInfo.put("visible", landmark.visible);
                stringutils::PoseXmlToString(landmark.transform, transformStr);
                pLandmarkInfo.put("transform", transformStr);
                translationInImageStr = std::to_string(landmark.translationInImage[0]) + " " +
                                        std::to_string(landmark.translationInImage[1]);
                pLandmarkInfo.put("translation_in_image", translationInImageStr);
                pNeighbourList.add_child("landmark", pLandmarkInfo);
                transformStr.clear();
                translationInImageStr.clear();
            }
            boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
            boost::property_tree::write_xml(path + "/landmark.xml", pNeighbourList, std::locale(),
                                            setting);
            return true;
        };

        bool GetLandmarkXml(const std::string &path)
        {
            boost::property_tree::ptree landmarkXmlPt;
            boost::property_tree::xml_parser::read_xml(path + "/landmark.xml", landmarkXmlPt,
                                               boost::property_tree::xml_parser::trim_whitespace);
            if (landmarkXmlPt.empty())
            {
                printf("Landmark.xml is empty! Path is %s", path.c_str());
                return false;
            }

            data::LandmarkNode tmpNode;
            std::string tmpPoseStr;
            std::string tmpTranslationInImageStr;
            boost::property_tree::ptree neighbourList;

            nodeList_.clear();
            nodeList_.reserve(landmarkXmlPt.size());
            for (auto &mark : landmarkXmlPt)
            {
                if (mark.first == "landmark")
                {
                    //考虑用bool表示，true or false
                    tmpNode.visible = std::stoi(mark.second.get("visible", ""));
                    if (!tmpNode.visible)
                    {
                        continue;
                    }
                    tmpNode.ns = mark.second.get("ns", "");
                    if (tmpNode.ns != "Visualmarks" && tmpNode.ns != "Visualmarks_4x4")
                    {
                        continue;
                    }
                    tmpNode.id = std::stoi(mark.second.get("id", " "));
                    tmpPoseStr = mark.second.get("transform", " ");
                    stringutils::PoseStringToXml(tmpPoseStr, tmpNode.transform);
                    tmpTranslationInImageStr = mark.second.get("translation_in_image", "");
                    std::sscanf(tmpTranslationInImageStr.c_str(), "%lf %lf",
                                &tmpNode.translationInImage[0], &tmpNode.translationInImage[1]);
                    nodeList_.emplace_back(std::move(tmpNode));
                }
            }
            this->nodeCount_=this->nodeList_.size();
            return true;
        };

        //for saveBlankMapServer
        void SetBlankXml(const std::string& blankXmlPath)
        {
            boost::property_tree::ptree p_landmark;
            boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
            boost::property_tree::write_xml(blankXmlPath, p_landmark, std::locale(), setting);
        }

        int GetNodeCount()
        {
            return nodeCount_;
        }

        std::vector<data::LandmarkNode> GetNodeList()
        {
            return nodeList_;
        }
    private:
        int nodeCount_;
        std::vector<data::LandmarkNode> nodeList_;
    };
};
#endif