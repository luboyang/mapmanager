/*
 * Copyright 2023 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef XMLHANDLE_H_
#define XMLHANDLE_H_
#include <vector>
#include <string>
#include "common/datatype/file_data_type/file_data.hpp"
#include "utility/system/utils_system.hpp"
#include "utility/string/utils_string.hpp"
#include "tinyxml2.h"
namespace tiny_xml {
    class XmlHandle
    {
    public:
        XmlHandle();
        ~XmlHandle();


    };

    class SubmapDataXml
    {
    public:
        SubmapDataXml() {};
        SubmapDataXml(file_data_type::SubmapData newNode) : node_(newNode) {};
        ~SubmapDataXml() {};
        bool SetXml(const std::string& path);
        bool GetXml(const std::string& path);
        file_data_type::SubmapData GetNode() { return node_; }
    private:
        file_data_type::SubmapData node_;
    };

    class MapXml
    {
    public:
        MapXml() {};
        MapXml(std::vector<file_data_type::MapData> nodeList)
            : nodeCount_(nodeList.size()), nodeList_(nodeList) {};
        ~MapXml() {};

        bool SetXml(const std::string& path);
        bool GetXml(const std::string& path);
        int GetNodeCount() { return nodeCount_; };
        std::vector<file_data_type::MapData> GetNodeList() { return nodeList_; };
    private:
        int nodeCount_;
        std::vector<file_data_type::MapData> nodeList_;
    };

    class LandmarkXml
    {
    public:
        LandmarkXml() {};
        LandmarkXml(const std::vector<file_data_type::LandmarkInfo>& nodeList) : nodeCount_(nodeList.size()),
            nodeList_(nodeList) {};
        ~LandmarkXml() {};
        bool SetXml(const std::string& path);
        bool GetXml(const std::string& path);
        int GetNodeCount() { return nodeCount_; }

        std::vector<file_data_type::LandmarkInfo> GetNodeList() { return nodeList_; }
    private:
        int nodeCount_;
        std::vector<file_data_type::LandmarkInfo> nodeList_;
    };

	class MapPngXml
	{
	public:
		MapPngXml() {};
		MapPngXml(const file_data_type::MapPngData& nodeList)
			: nodeList_(nodeList) {};
		~MapPngXml() {};

		bool SetXml(const std::string& path);
		bool GetXml(const std::string& path);
		file_data_type::MapPngData GetNode() { return nodeList_; };
	private:
		file_data_type::MapPngData nodeList_;
	};

	class BoolImageCornerPointsXml
	{
	public:
		BoolImageCornerPointsXml() {};
		BoolImageCornerPointsXml(const std::vector<file_data_type::CornerPointsData>& nodeList)
			: nodeList_(nodeList) {};
		~BoolImageCornerPointsXml() {};

		bool SetXml(const std::string& path);
		bool GetXml(const std::string& path);
		std::vector<file_data_type::CornerPointsData> GetNodeList() { return nodeList_; };
	private:
		std::vector<file_data_type::CornerPointsData> nodeList_;
	};
}

#endif // XMLHANDLE_H
