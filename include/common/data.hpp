#ifndef DATA
#define DATA
#include <Eigen/Core>
#include <cartographer/mapping/pose_graph_interface.h>
#include <cartographer/sensor/landmark_data.h>
#include <cartographer/sensor/odometry_data.h>
#include <cartographer/sensor/range_data.h>
#include <cartographer/transform/transform.h>
#include <cartographer_ros_msgs/LandmarkNewEntry.h>
#include <cartographer_ros_msgs/SaveSubmap.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <map_manager_msgs/TagMapInfo.h>
#include <utility/eigen/eigen_utils.hpp>
namespace data
{
// xml
struct LandmarkNode
{
  int visible;
  int id;
  double translationInImage[2];
  std::string ns;
  geometry_msgs::Pose transform;
  LandmarkNode() : visible(0), id(-1)
  {
    memset(translationInImage, 0, sizeof(translationInImage));
    transform.position.x = 0;
    transform.position.y = 0;
    transform.position.z = 0;
    transform.orientation.x = 0;
    transform.orientation.y = 0;
    transform.orientation.z = 0;
    transform.orientation.w = 1;
  }

  LandmarkNode(int newVisible, int newId, double newTranslationInImage[2], const char *newNs,
               geometry_msgs::Pose newTransform)
      : visible{newVisible}, id{newId}, ns{newNs}, transform{newTransform}
  {
    memcpy(translationInImage, newTranslationInImage, sizeof(translationInImage));
  }
};

struct LaserDataNode
{
  bool finished;
  int id;
  int numRangeData;
  double resolution;
  double max[2];
  int numCells[2];
  int knownCellsBox[4];
  geometry_msgs::Pose pose;
  geometry_msgs::Pose localPose;

  LaserDataNode() : finished(true), id(-1), numRangeData(0), resolution(0.05)
  {
    memset(max, 0, sizeof(max));
    memset(numCells, 0, sizeof(numCells));
    memset(knownCellsBox, 0, sizeof(knownCellsBox));
  }

  LaserDataNode(bool newFinished, int newId, int numRangeData, float newResolution, double *newMax,
                int *newNumCells, int *newKnownCellsBox, geometry_msgs::Pose newPose,
                geometry_msgs::Pose newLocalPose)
      : finished(newFinished), id(newId), numRangeData(numRangeData), resolution(newResolution),
        pose(newPose), localPose(newLocalPose)
  {
    memcpy(max, newMax, sizeof(max));
    memcpy(numCells, newNumCells, sizeof(numCells));
    memcpy(knownCellsBox, newKnownCellsBox, sizeof(knownCellsBox));
  }
};

struct NeighbourInMapNode
{
  bool reachable;
  int id;
  geometry_msgs::Pose transform;

  NeighbourInMapNode() : reachable(false), id(-1)
  {
    transform.position.x = 0;
    transform.position.y = 0;
    transform.position.z = 0;
    transform.orientation.x = 0;
    transform.orientation.y = 0;
    transform.orientation.z = 0;
    transform.orientation.w = 1;
  }
  NeighbourInMapNode(bool newReachable, int newId, geometry_msgs::Pose newTransform)
      : reachable(newReachable), id(newId), transform(newTransform){};
};

struct MapNode
{
  int id;
  std::vector<NeighbourInMapNode> neighbourList;

  MapNode() : id(0){};

  MapNode(int newId, std::vector<NeighbourInMapNode> newNeighbourList)
      : id(newId), neighbourList(newNeighbourList){};
};

struct MapDataNode
{
  int width;
  int height;
  float resolution;
  geometry_msgs::Pose transform;

  MapDataNode() : width(0), height(0), resolution(0.05)
  {
    transform.position.x = 0;
    transform.position.y = 0;
    transform.position.z = 0;
    transform.orientation.w = 1;
    transform.orientation.x = 0;
    transform.orientation.y = 0;
    transform.orientation.z = 0;
  }

  MapDataNode(int newWidth, int newHeight, float newResolution, geometry_msgs::Pose newTransform)
      : width(newWidth), height(newHeight), resolution(newResolution), transform(newTransform){};
};

struct TagDataNode
{
  int id;
  int tagId;
  geometry_msgs::Pose pose;
  TagDataNode() : id(-1), tagId(-1)
  {
    pose.position.x = 0;
    pose.position.y = 0;
    pose.position.z = 0;
    pose.orientation.x = 0;
    pose.orientation.y = 0;
    pose.orientation.z = 0;
    pose.orientation.w = 1;
  }

  TagDataNode(int newId, int newTagId, geometry_msgs::Pose newTransform)
      : id(newId), tagId(newTagId), pose(newTransform){};
};

struct TagGlobalPoseNode
{
  int id;
  int tagId;
  int visible;
  std::string ns;
  geometry_msgs::Pose globalPose;
  TagGlobalPoseNode() : id(-1), tagId(-1), visible(0), ns("")
  {
    globalPose.position.x = 0;
    globalPose.position.y = 0;
    globalPose.position.z = 0;
    globalPose.orientation.x = 0;
    globalPose.orientation.y = 0;
    globalPose.orientation.z = 0;
    globalPose.orientation.w = 1;
  }

  TagGlobalPoseNode(int newId, int newTagId, int newVisible, std::string newNs,
                    geometry_msgs::Pose newTransform)
      : id(newId), tagId(newTagId), visible(newVisible), ns(newNs), globalPose(newTransform){};
};

struct TagInLaserMapNode
{
  int tagId;
  geometry_msgs::Pose tagInLaserMapPose;
  TagInLaserMapNode() : tagId(-1)
  {
    tagInLaserMapPose.position.x = 0;
    tagInLaserMapPose.position.y = 0;
    tagInLaserMapPose.position.z = 0;
    tagInLaserMapPose.orientation.x = 0;
    tagInLaserMapPose.orientation.y = 0;
    tagInLaserMapPose.orientation.z = 0;
    tagInLaserMapPose.orientation.w = 1;
  }

  TagInLaserMapNode(const int newTagId, geometry_msgs::Pose newTagInLaserMapPose)
      : tagId(newTagId), tagInLaserMapPose(newTagInLaserMapPose){};
  TagInLaserMapNode(const TagInLaserMapNode &tagInLaserMapNode)
  {
    tagId = tagInLaserMapNode.tagId;
    tagInLaserMapPose = tagInLaserMapNode.tagInLaserMapPose;
  }
  TagInLaserMapNode &operator=(const TagInLaserMapNode &tagInLaserMapNode)
  {
    tagId = tagInLaserMapNode.tagId;
    tagInLaserMapPose = tagInLaserMapNode.tagInLaserMapPose;
    return *this;
  }
};

struct CornerPointsNode
{
  std::string points_size;
  std::vector<Eigen::Vector2d> point_in_map;
};

// mapping
struct NodeData
{
  cartographer::sensor::RangeData range_data;
  Eigen::VectorXf rotational_scan_matcher_histogram_in_gravity;
  Eigen::Quaterniond local_from_gravity_aligned;
  cartographer::transform::Rigid3d global_pose;
};

struct ProbabilityGridInfo
{
  int id;
  cartographer::transform::Rigid3d global_pose;
  cartographer::transform::Rigid3d local_pose;
  int num_range_data;
  bool finished;
  double resolution;
  double max[2];
  int num_cells[2];
};

struct LandmarkInfo
{
  std::string ns;
  std::string id;
  int visible;
  cartographer::transform::Rigid3d pose;
  Eigen::Vector2d translation_in_image;
  float translation_weight;
  float rotation_weight;
  float pole_radius;
};

struct TrajectoryTransform
{
  Eigen::Matrix2d area_R_map;
  Eigen::Vector2d area_t_map;
  Eigen::Vector2d max_area;
};

// pbstream
struct PbstreamInfos
{
  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      node_poses;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::TrajectoryNode::Data>
      node_datas;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId, cartographer::transform::Rigid3d>
      submap_poses;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                 cartographer::mapping::proto::Submap>
      submap_id_to_submap;
  std::map<std::string, cartographer::transform::Rigid3d> landmark_poses;
  std::vector<cartographer::sensor::OdometryData> odom_datas;
  std::vector<cartographer::sensor::LandmarkData> landmark_datas;
  std::vector<cartographer::mapping::PoseGraphInterface::Constraint> constraints;
};

// request
struct SaveMapServerData
{
  std::vector<cartographer_ros_msgs::SaveSubmap> submaps;
  std::vector<cartographer_ros_msgs::LandmarkNewEntry> landmarks;
  std::vector<geometry_msgs::PoseStamped> nodes_poses_with_stamp;
};

struct GetTagMapInfoServerData
{
  int tagId;
  int submapId;
  std::vector<int> neighbourNodeId;
  geometry_msgs::Pose pose;
  geometry_msgs::Pose tagPoseInLaser;
  GetTagMapInfoServerData() : tagId(-1), submapId(-1)
  {
    pose.position.x = 0;
    pose.position.y = 0;
    pose.position.z = 0;
    pose.orientation.x = 0;
    pose.orientation.y = 0;
    pose.orientation.z = 0;
    pose.orientation.w = 1;

    tagPoseInLaser.position.x = 0;
    tagPoseInLaser.position.y = 0;
    tagPoseInLaser.position.z = 0;
    tagPoseInLaser.orientation.x = 0;
    tagPoseInLaser.orientation.y = 0;
    tagPoseInLaser.orientation.z = 0;
    tagPoseInLaser.orientation.w = 1;
  }

  GetTagMapInfoServerData(int newTagId, int newSubmapId, std::vector<int> newNeighbourNodeId,
                          geometry_msgs::Pose newPose, geometry_msgs::Pose newTagPoseInLaser)
      : tagId(newTagId), submapId(newSubmapId), neighbourNodeId(newNeighbourNodeId), pose(newPose),
        tagPoseInLaser(newTagPoseInLaser){};
};

// trans
inline void ProbabilityGridInfoToLaserData(const ProbabilityGridInfo &probabilityGridInfo,
                                           LaserDataNode &laserDataNode)
{
  laserDataNode.finished = probabilityGridInfo.finished;
  laserDataNode.id = probabilityGridInfo.id;
  laserDataNode.numRangeData = probabilityGridInfo.num_range_data;
  laserDataNode.resolution = probabilityGridInfo.resolution;
  memcpy(laserDataNode.max, probabilityGridInfo.max, sizeof(laserDataNode.max));
  memcpy(laserDataNode.numCells, probabilityGridInfo.num_cells, sizeof(laserDataNode.numCells));
  laserDataNode.pose =
      EigenGeometryConversion::Rigid3dToGeometryMsgsPose(probabilityGridInfo.global_pose);
  laserDataNode.localPose =
      EigenGeometryConversion::Rigid3dToGeometryMsgsPose(probabilityGridInfo.local_pose);
  laserDataNode.knownCellsBox[0] = 0;
  laserDataNode.knownCellsBox[1] = 0;
  laserDataNode.knownCellsBox[2] = laserDataNode.numCells[0] - 1;
  laserDataNode.knownCellsBox[3] = laserDataNode.numCells[1] - 1;
}

inline void LaserDataToProbabilityGridInfo(const LaserDataNode &laserDataNode,
                                           ProbabilityGridInfo &probabilityGridInfo)
{
  probabilityGridInfo.finished = laserDataNode.finished;
  probabilityGridInfo.id = laserDataNode.id;
  probabilityGridInfo.num_range_data = laserDataNode.numRangeData;
  probabilityGridInfo.resolution = laserDataNode.resolution;
  memcpy(probabilityGridInfo.max, laserDataNode.max, sizeof(probabilityGridInfo.max));
  memcpy(probabilityGridInfo.num_cells, laserDataNode.numCells,
         sizeof(probabilityGridInfo.num_cells));
  probabilityGridInfo.global_pose =
      EigenGeometryConversion::GeometryMsgsPoseToRigid3d(laserDataNode.pose);
  probabilityGridInfo.local_pose =
      EigenGeometryConversion::GeometryMsgsPoseToRigid3d(laserDataNode.localPose);
}

inline void TagMapInfoToGetTagMapInfoServerData(const map_manager_msgs::TagMapInfo &tagMapInfo,
                                                GetTagMapInfoServerData &getTagMapInfoServerData)
{
  getTagMapInfoServerData =
      GetTagMapInfoServerData(tagMapInfo.tag_id, tagMapInfo.submap_id, tagMapInfo.neighbors_id,
                              tagMapInfo.pose, tagMapInfo.tag_pose_in_laser);
}

inline void
GetTagMapInfoServerDataToTagMapInfo(const GetTagMapInfoServerData &getTagMapInfoServerData,
                                    map_manager_msgs::TagMapInfo &tagMapInfo)
{
  tagMapInfo.tag_id = getTagMapInfoServerData.tagId;
  tagMapInfo.submap_id = getTagMapInfoServerData.submapId;
  tagMapInfo.neighbors_id = std::move(getTagMapInfoServerData.neighbourNodeId);
  tagMapInfo.pose = getTagMapInfoServerData.pose;
  tagMapInfo.tag_pose_in_laser = getTagMapInfoServerData.tagPoseInLaser;
}
}; // namespace data
#endif