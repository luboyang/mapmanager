#ifndef ROS_TIME_H_INCLUDED
#define ROS_TIME_H_INCLUDED

#ifdef _MSC_VER
#pragma warning(disable : 4244)
#pragma warning(disable : 4661)
#endif

#include <cmath>
#include <iostream>

#include "jz_duration.h"

#if defined(_WIN32)
#include <sys/timeb.h>
#else
#include <sys/time.h>
#endif

namespace jzros {

/*********************************************************************
 ** Functions
 *********************************************************************/

void normalizeSecNSec(uint64_t& sec, uint64_t& nsec);
void normalizeSecNSec(uint32_t& sec, uint32_t& nsec);
void normalizeSecNSecUnsigned(int64_t& sec, int64_t& nsec);
void ros_walltime(uint32_t& sec, uint32_t& nsec);
void ros_steadytime(uint32_t& sec, uint32_t& nsec);

/*********************************************************************
 ** Time Classes
 *********************************************************************/

/**
 * \brief Base class for Time implementations.  Provides storage, common
 * functions and operator overloads. This should not need to be used directly.
 */
template <class T, class D>
class TimeBase {
 public:
  uint32_t sec, nsec;

  TimeBase() : sec(0), nsec(0) {}
  TimeBase(uint32_t _sec, uint32_t _nsec) : sec(_sec), nsec(_nsec) {
    normalizeSecNSec(sec, nsec);
  }
  explicit TimeBase(double t) { fromSec(t); }
  D operator-(const T& rhs) const;
  T operator+(const D& rhs) const;
  T operator-(const D& rhs) const;
  T& operator+=(const D& rhs);
  T& operator-=(const D& rhs);
  bool operator==(const T& rhs) const;
  inline bool operator!=(const T& rhs) const {
    return !(*static_cast<const T*>(this) == rhs);
  }
  bool operator>(const T& rhs) const;
  bool operator<(const T& rhs) const;
  bool operator>=(const T& rhs) const;
  bool operator<=(const T& rhs) const;

  double toSec() const {
    return static_cast<double>(sec) + 1e-9 * static_cast<double>(nsec);
  };
  T& fromSec(double t);

  uint64_t toNSec() const {
    return static_cast<uint64_t>(sec) * 1000000000ull +
           static_cast<uint64_t>(nsec);
  }
  T& fromNSec(uint64_t t);

  inline bool isZero() const { return sec == 0 && nsec == 0; }
  inline bool is_zero() const { return isZero(); }
};

/**
 * \brief Time representation.  May either represent wall clock time or ROS
 * clock time.
 *
 * ros::TimeBase provides most of its functionality.
 */
class Time : public TimeBase<Time, Duration> {
 public:
  Time() : TimeBase<Time, Duration>() {}

  Time(uint32_t _sec, uint32_t _nsec) : TimeBase<Time, Duration>(_sec, _nsec) {}

  explicit Time(double t) { fromSec(t); }

  /**
   * \brief Retrieve the current time.  If ROS clock time is in use, this
   * returns the time according to the ROS clock.  Otherwise returns the current
   * wall clock time.
   */
  static Time now();
  /**
   * \brief Sleep until a specific time has been reached.
   * @return True if the desired sleep time was met, false otherwise.
   */
  static bool sleepUntil(const Time& end);

  static void init();
  static void shutdown();
  static void setNow(const Time& new_now);
  static bool useSystemTime();
  static bool isSimTime();
  static bool isSystemTime();

  /**
   * \brief Returns whether or not the current time source is valid.  Simulation
   * time is valid if it is non-zero.
   */
  static bool isValid();
  /**
   * \brief Wait for time source to become valid
   */
  static bool waitForValid();
  /**
   * \brief Wait for time source to become valid, with timeout
   */
  static bool waitForValid(const WallDuration& timeout);
};

extern const Time TIME_MAX;
extern const Time TIME_MIN;

/**
 * \brief Time representation.  Always wall-clock time.
 *
 * ros::TimeBase provides most of its functionality.
 */
class WallTime : public TimeBase<WallTime, WallDuration> {
 public:
  WallTime() : TimeBase<WallTime, WallDuration>() {}

  WallTime(uint32_t _sec, uint32_t _nsec)
      : TimeBase<WallTime, WallDuration>(_sec, _nsec) {}

  explicit WallTime(double t) { fromSec(t); }

  /**
   * \brief Returns the current wall clock time.
   */
  static WallTime now();

  /**
   * \brief Sleep until a specific time has been reached.
   * @return True if the desired sleep time was met, false otherwise.
   */
  static bool sleepUntil(const WallTime& end);

  static bool isSystemTime() { return true; }
};

/**
 * \brief Time representation.  Always steady-clock time.
 *
 * Not affected by ROS time.
 *
 * ros::TimeBase provides most of its functionality.
 */
class SteadyTime : public TimeBase<SteadyTime, WallDuration> {
 public:
  SteadyTime() : TimeBase<SteadyTime, WallDuration>() {}

  SteadyTime(uint32_t _sec, uint32_t _nsec)
      : TimeBase<SteadyTime, WallDuration>(_sec, _nsec) {}

  explicit SteadyTime(double t) { fromSec(t); }

  /**
   * \brief Returns the current steady (monotonic) clock time.
   */
  static SteadyTime now();

  /**
   * \brief Sleep until a specific time has been reached.
   * @return True if the desired sleep time was met, false otherwise.
   */
  static bool sleepUntil(const SteadyTime& end);

  static bool isSystemTime() { return true; }
};

std::ostream& operator<<(std::ostream& os, const Time& rhs);
std::ostream& operator<<(std::ostream& os, const WallTime& rhs);
std::ostream& operator<<(std::ostream& os, const SteadyTime& rhs);
}  // namespace jzros

#endif  // ROS_TIME_H
