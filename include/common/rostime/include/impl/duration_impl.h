#ifndef ROSTIME_IMPL_DURATION_H_INCLUDED
#define ROSTIME_IMPL_DURATION_H_INCLUDED

#include "jz_duration.h"
#include "jz_rate.h"

namespace jzros {
//
// DurationBase template member function implementation
//
template <class T>
DurationBase<T>::DurationBase(int32_t _sec, int32_t _nsec)
    : sec(_sec), nsec(_nsec) {
  normalizeSecNSecSigned(sec, nsec);
}

template <class T>
T &DurationBase<T>::fromSec(double d) {
  int64_t sec64 = static_cast<int64_t>(floor(d));
  if (sec64 < std::numeric_limits<int32_t>::min() ||
      sec64 > std::numeric_limits<int32_t>::max())
    throw std::runtime_error("Duration is out of dual 32-bit range");
  sec = static_cast<int32_t>(sec64);
  nsec = static_cast<int32_t>(std::round((d - sec) * 1e9));
  int32_t rollover = nsec / 1000000000ul;
  sec += rollover;
  nsec %= 1000000000ul;
  return *static_cast<T *>(this);
}

template <class T>
T &DurationBase<T>::fromNSec(int64_t t) {
  int64_t sec64 = t / 1000000000LL;
  if (sec64 < std::numeric_limits<int32_t>::min() ||
      sec64 > std::numeric_limits<int32_t>::max())
    throw std::runtime_error("Duration is out of dual 32-bit range");
  sec = static_cast<int32_t>(sec64);
  nsec = static_cast<int32_t>(t % 1000000000LL);

  normalizeSecNSecSigned(sec, nsec);

  return *static_cast<T *>(this);
}

template <class T>
T DurationBase<T>::operator+(const T &rhs) const {
  T t;
  return t.fromNSec(toNSec() + rhs.toNSec());
}

template <class T>
T DurationBase<T>::operator*(double scale) const {
  return T(toSec() * scale);
}

template <class T>
T DurationBase<T>::operator-(const T &rhs) const {
  T t;
  return t.fromNSec(toNSec() - rhs.toNSec());
}

template <class T>
T DurationBase<T>::operator-() const {
  T t;
  return t.fromNSec(-toNSec());
}

template <class T>
T &DurationBase<T>::operator+=(const T &rhs) {
  *this = *this + rhs;
  return *static_cast<T *>(this);
}

template <class T>
T &DurationBase<T>::operator-=(const T &rhs) {
  *this += (-rhs);
  return *static_cast<T *>(this);
}

template <class T>
T &DurationBase<T>::operator*=(double scale) {
  fromSec(toSec() * scale);
  return *static_cast<T *>(this);
}

template <class T>
bool DurationBase<T>::operator<(const T &rhs) const {
  if (sec < rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec < rhs.nsec)
    return true;
  return false;
}

template <class T>
bool DurationBase<T>::operator>(const T &rhs) const {
  if (sec > rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec > rhs.nsec)
    return true;
  return false;
}

template <class T>
bool DurationBase<T>::operator<=(const T &rhs) const {
  if (sec < rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec <= rhs.nsec)
    return true;
  return false;
}

template <class T>
bool DurationBase<T>::operator>=(const T &rhs) const {
  if (sec > rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec >= rhs.nsec)
    return true;
  return false;
}

template <class T>
bool DurationBase<T>::operator==(const T &rhs) const {
  return sec == rhs.sec && nsec == rhs.nsec;
}

template <class T>
bool DurationBase<T>::isZero() const {
  return sec == 0 && nsec == 0;
}

}  // namespace jzros
#endif
