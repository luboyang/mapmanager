#ifndef ROS_TIME_IMPL_H_INCLUDED
#define ROS_TIME_IMPL_H_INCLUDED

#include <cmath>
#include <iostream>

#include "jz_time.h"

#if defined(_WIN32)
#include <sys/timeb.h>
#else
#include <sys/time.h>
#endif

namespace jzros {

template <class T, class D>
T &TimeBase<T, D>::fromNSec(uint64_t t) {
  uint64_t sec64 = 0;
  uint64_t nsec64 = t;

  normalizeSecNSec(sec64, nsec64);

  sec = static_cast<uint32_t>(sec64);
  nsec = static_cast<uint32_t>(nsec64);

  return *static_cast<T *>(this);
}

template <class T, class D>
T &TimeBase<T, D>::fromSec(double t) {
  int64_t sec64 = static_cast<int64_t>(floor(t));
  if (sec64 < 0 || sec64 > std::numeric_limits<uint32_t>::max())
    throw std::runtime_error("Time is out of dual 32-bit range");
  sec = static_cast<uint32_t>(sec64);
  nsec = static_cast<uint32_t>(std::round((t - sec) * 1e9));
  // avoid rounding errors
  sec += (nsec / 1000000000ul);
  nsec %= 1000000000ul;
  return *static_cast<T *>(this);
}

template <class T, class D>
D TimeBase<T, D>::operator-(const T &rhs) const {
  D d;
  return d.fromNSec(toNSec() - rhs.toNSec());
}

template <class T, class D>
T TimeBase<T, D>::operator-(const D &rhs) const {
  return *static_cast<const T *>(this) + (-rhs);
}

template <class T, class D>
T TimeBase<T, D>::operator+(const D &rhs) const {
  int64_t sec_sum = static_cast<uint64_t>(sec) + static_cast<uint64_t>(rhs.sec);
  int64_t nsec_sum =
      static_cast<uint64_t>(nsec) + static_cast<uint64_t>(rhs.nsec);

  // Throws an exception if we go out of 32-bit range
  normalizeSecNSecUnsigned(sec_sum, nsec_sum);

  // now, it's safe to downcast back to uint32 bits
  return T(static_cast<uint32_t>(sec_sum), static_cast<uint32_t>(nsec_sum));
}

template <class T, class D>
T &TimeBase<T, D>::operator+=(const D &rhs) {
  *this = *this + rhs;
  return *static_cast<T *>(this);
}

template <class T, class D>
T &TimeBase<T, D>::operator-=(const D &rhs) {
  *this += (-rhs);
  return *static_cast<T *>(this);
}

template <class T, class D>
bool TimeBase<T, D>::operator==(const T &rhs) const {
  return sec == rhs.sec && nsec == rhs.nsec;
}

template <class T, class D>
bool TimeBase<T, D>::operator<(const T &rhs) const {
  if (sec < rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec < rhs.nsec)
    return true;
  return false;
}

template <class T, class D>
bool TimeBase<T, D>::operator>(const T &rhs) const {
  if (sec > rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec > rhs.nsec)
    return true;
  return false;
}

template <class T, class D>
bool TimeBase<T, D>::operator<=(const T &rhs) const {
  if (sec < rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec <= rhs.nsec)
    return true;
  return false;
}

template <class T, class D>
bool TimeBase<T, D>::operator>=(const T &rhs) const {
  if (sec > rhs.sec)
    return true;
  else if (sec == rhs.sec && nsec >= rhs.nsec)
    return true;
  return false;
}
}  // namespace jzros

#endif  // ROS_IMPL_TIME_H_INCLUDED
