#ifndef VTR_MSGS_DATA
#define VTR_MSGS_DATA
#include <geometryMsgsData.hpp>
#include <stdMsgsData.hpp>
namespace datatype
{
struct VtrPose
{
  uint8_t poseValid;
  int64_t referenceId;
  std::string localizationType;
  Header header;
  PoseWithCovariance pose;
  VtrPose(uint8_t pPoseValid, int64_t pReferenceId, std::string pLocalizationType, Header pHeader,
          PoseWithCovariance pPose)
      : poseValid(pPoseValid), referenceId(pReferenceId), localizationType(pLocalizationType),
        header(std::move(pHeader)), pose(std::move(pPose)){};
  VtrPose(VtrPose &&vtrPose) noexcept
  {
    poseValid = vtrPose.poseValid;
    referenceId = vtrPose.referenceId;
    localizationType = std::move(vtrPose.localizationType);
    header = std::move(vtrPose.header);
    pose = std::move(vtrPose.pose);
  }

  VtrPose &operator=(VtrPose &&vtrPose) noexcept
  {
    poseValid = vtrPose.poseValid;
    referenceId = vtrPose.referenceId;
    localizationType = std::move(vtrPose.localizationType);
    header = std::move(vtrPose.header);
    pose = std::move(vtrPose.pose);
  }
};

struct GlobalLocalizationPose
{
  uint8_t poseValid;
  int64_t referenceId;
  std::string localizationType;
  Header header;
  Eigen::Isometry3d sensor2odom;
  PoseWithCovariance sensor2reference;
  GlobalLocalizationPose(uint8_t pPoseValid, int64_t pReferenceId, std::string pLocalizationType,
                         Header pHeader, Eigen::Isometry3d pSensor2odom,
                         PoseWithCovariance pSensor2reference)
      : poseValid(pPoseValid), referenceId(pReferenceId), localizationType(pLocalizationType),
        header(std::move(pHeader)), sensor2odom(pSensor2odom),
        sensor2reference(std::move(pSensor2reference)){};
  GlobalLocalizationPose(GlobalLocalizationPose &&globalLocalizationPose)
  {
    poseValid = globalLocalizationPose.poseValid;
    referenceId = globalLocalizationPose.referenceId;
    localizationType = globalLocalizationPose.localizationType;
    header = std::move(globalLocalizationPose.header);
    sensor2odom = globalLocalizationPose.sensor2odom;
    sensor2reference = std::move(globalLocalizationPose.sensor2reference);
  }
  GlobalLocalizationPose &operator=(GlobalLocalizationPose &&globalLocalizationPose)
  {
    poseValid = globalLocalizationPose.poseValid;
    referenceId = globalLocalizationPose.referenceId;
    localizationType = globalLocalizationPose.localizationType;
    header = std::move(globalLocalizationPose.header);
    sensor2odom = globalLocalizationPose.sensor2odom;
    sensor2reference = std::move(globalLocalizationPose.sensor2reference);
  }
};
}; // namespace datatype
#endif
