#ifndef STD_MSGS_DATA
#define STD_MSGS_DATA
#include <string>
#include "common/rostime/include/jz_time.h"
namespace datatype
{
struct Header
{
  uint32_t seq;
  jzros::Time stamp;
  std::string frameId;

  Header() : seq(0){};
  Header(const uint32_t newSeq, jzros::Time&newStamp, const std::string &newFrameId)
      : seq(newSeq), stamp(std::move(newStamp)), frameId(newFrameId){};

  Header(Header &&header) noexcept
  {
    seq = header.seq;
    stamp = std::move(header.stamp);
    frameId = std::move(header.frameId);
  }

  Header(const Header& header) noexcept
  {
	  seq = header.seq;
	  stamp = header.stamp;
	  frameId = header.frameId;
  }

  Header &operator=(Header &&header) noexcept
  {
    seq = header.seq;
    stamp = std::move(header.stamp);
    frameId = std::move(header.frameId);
    return *this;
  }
  Header& operator=(const Header& header) noexcept
  {
	  seq = header.seq;
	  stamp = header.stamp;
	  frameId = header.frameId;
      return *this;
  }
};
}; // namespace datatype
#endif