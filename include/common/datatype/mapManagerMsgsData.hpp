#ifndef MAP_MANAGER_MSGS_DATA
#define MAP_MANAGER_MSGS_DATA
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>
#include <vector>
namespace datatype
{
struct Point2d
{
  double x;
  double y;
  Point2d() : x(0.0), y(0.0){};
  Point2d(double newX, double newY) : x(newX), y(newY){};
  Point2d(Point2d &&point2d)
  {
    x = point2d.x;
    y = point2d.y;
  };
  Point2d &operator=(Point2d &&point2d)
  {
    x = point2d.x;
    y = point2d.y;
  };
};

struct Points2d
{
  std::vector<Point2d> points;
  Points2d(){};
  Points2d(std::vector<Point2d> &points) : points(points){};
  Points2d(Points2d &&points2d) noexcept { points = std::move(points2d.points); }
  Points2d &operator=(Points2d &&points2d) noexcept { points = std::move(points2d.points); }
};

struct IdRemap
{
  int32_t originId;
  int32_t newId;
  std::string ns;

  IdRemap(){};
  IdRemap(int32_t pOriginId, int32_t pNewId, std::string &pNs)
      : originId(pOriginId), newId(pNewId), ns(pNs){};

  IdRemap(IdRemap &&idRemap) noexcept
  {
    ns = std::move(idRemap.ns);
    originId = idRemap.originId;
    newId = idRemap.newId;
  }
  IdRemap &operator=(IdRemap &&idRemap) noexcept
  {
    ns = std::move(idRemap.ns);
    originId = idRemap.originId;
    newId = idRemap.newId;
  }
};

struct TagMapInfo
{
  int32_t tagId;
  int32_t submapId;
  Eigen::Isometry3d pose;
  Eigen::Isometry3d tagPoseInLaser;
  std::vector<int32_t> neighborsId;

  TagMapInfo(int32_t pTagId, int32_t pSubmapId, Eigen::Isometry3d pPose,
             Eigen::Isometry3d pTagPoseInLaser, std::vector<int32_t> pNeighborsId)
      : tagId(pTagId), submapId(pSubmapId), pose(pPose), tagPoseInLaser(pTagPoseInLaser),
        neighborsId(pNeighborsId){};
  TagMapInfo(TagMapInfo &&tagMapInfo) noexcept
  {
    tagId = tagMapInfo.tagId;
    submapId = tagMapInfo.submapId;
    pose = std::move(tagMapInfo.pose);
    tagPoseInLaser = std::move(tagMapInfo.tagPoseInLaser);
    neighborsId = std::move(tagMapInfo.neighborsId);
  }
  TagMapInfo &operator=(TagMapInfo &&tagMapInfo) noexcept
  {
    tagId = tagMapInfo.tagId;
    submapId = tagMapInfo.submapId;
    pose = std::move(tagMapInfo.pose);
    tagPoseInLaser = std::move(tagMapInfo.tagPoseInLaser);
    neighborsId = std::move(tagMapInfo.neighborsId);
  }
};

struct VisualmarksDetectInfo
{
  int32_t tagId;
  int32_t errorCode;
  std::string errorMsg;

  VisualmarksDetectInfo(int32_t pTagId, int32_t pErrorCode, std::string pErrorMsg)
      : tagId(pTagId), errorCode(pErrorCode), errorMsg(pErrorMsg){};
};

}; // namespace datatype
#endif