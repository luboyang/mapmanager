#ifndef GEOMETRY_MSGS_DATA
#define GEOMETRY_MSGS_DATA
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <stdMsgsData.hpp>
#include <geometry_msgs/PoseWithCovariance.h>
namespace datatype
{
struct PoseStamped
{
  Header header;
  Eigen::Isometry3d pose;
  PoseStamped(){};
  PoseStamped(Header &pHeader, Eigen::Isometry3d &pPose)
      : header(std::move(pHeader)), pose(pPose){};
  PoseStamped(PoseStamped &&poseStamped) noexcept
  {
    header = std::move(poseStamped.header);
    pose = std::move(poseStamped.pose);
  };
  PoseStamped &operator=(PoseStamped &&poseStamped) noexcept
  {
    header = std::move(poseStamped.header);
    pose = std::move(poseStamped.pose);
  };
};

struct PoseWithCovariance
{
  Eigen::Isometry3d pose;
  std::array<double,36> covariance;
  PoseWithCovariance(){};
  PoseWithCovariance(Eigen::Isometry3d pPose,
  std::array<double,36> pCovariance):pose(pPose),covariance(pCovariance){}; 
  PoseWithCovariance(PoseWithCovariance &&poseWithCovariance)noexcept
  {
    pose=poseWithCovariance.pose;
    covariance=std::move(poseWithCovariance.covariance);
  }
  PoseWithCovariance &operator=(PoseWithCovariance &&poseWithCovariance)noexcept
  {
    pose=poseWithCovariance.pose;
    covariance=std::move(poseWithCovariance.covariance);
  }
};
}; // namespace datatype
#endif