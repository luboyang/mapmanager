#ifndef CARTOGRAPHER_ROS_MSGS_DATA
#define CARTOGRAPHER_ROS_MSGS_DATA
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <array>
#include <common/datatype/sensorMsgsData.hpp>
#include <vector>
namespace datatype
{
struct SubmapID
{
  int32_t trajectoryId;
  int32_t submapIndex;
  int32_t submapVersion;

  SubmapID(const int32_t trajectory_id, const int32_t submap_index, const int32_t submap_version)
      : trajectoryId(trajectory_id), submapIndex(submap_index), submapVersion(submap_version){};

  SubmapID() : trajectoryId(0), submapIndex(0), submapVersion(0){};

  SubmapID(SubmapID &&old)
      : trajectoryId(old.trajectoryId), submapIndex(old.submapIndex),
        submapVersion(old.submapVersion){};

  SubmapID(const SubmapID& old)
	  : trajectoryId(old.trajectoryId), submapIndex(old.submapIndex),
	  submapVersion(old.submapVersion) {};

  SubmapID &operator=(SubmapID &&old)
  {
    trajectoryId = old.trajectoryId;
    submapIndex = old.submapIndex;
    submapVersion = old.submapVersion;
  }
  SubmapID& operator=(const SubmapID& old)
  {
	  trajectoryId = old.trajectoryId;
	  submapIndex = old.submapIndex;
	  submapVersion = old.submapVersion;
  }
};

struct SubmapTexture
{
  int32_t width;
  int32_t height;
  double resolution;
  std::vector<uint8_t> cells;
  Eigen::Isometry3d slicePose;

  SubmapTexture(const std::vector<uint8_t> &newCells, const int32_t newWidth,
                const int32_t newHeight, const double newResolution, Eigen::Isometry3d slice_pose)
      : cells(newCells), width(newWidth), height(newHeight), resolution(newResolution),
        slicePose(slice_pose){};

  SubmapTexture() : width(0), height(0), resolution(0.0){};

  SubmapTexture(SubmapTexture &&old) noexcept
  {
    width = old.width;
    height = old.height;
    resolution = old.resolution;
    cells = std::move(old.cells);
    slicePose = std::move(old.slicePose);
  }

  SubmapTexture(const SubmapTexture& old) noexcept
  {
	  width = old.width;
	  height = old.height;
	  resolution = old.resolution;
	  cells = old.cells;
	  slicePose = old.slicePose;
  }

  SubmapTexture &operator=(SubmapTexture &&old)
  {
    width = old.width;
    height = old.height;
    resolution = old.resolution;
    cells = std::move(old.cells);
    slicePose = std::move(old.slicePose);
  }

  SubmapTexture& operator=(const SubmapTexture& old)
  {
	  width = old.width;
	  height = old.height;
	  resolution = old.resolution;
	  cells = old.cells;
	  slicePose = old.slicePose;
  }
};

struct SaveSubmap
{
  int32_t submapVersion;
  SubmapID id;
  Eigen::Isometry3d poseInMap;
  Eigen::Isometry3d poseInLocal;
  std::array<float, 2> numCells;
  std::array<float, 2> maxCells;
  std::vector<SubmapID> constraints;
  std::vector<SubmapTexture> textures;

  SaveSubmap(int32_t submap_version, SubmapID &newId, Eigen::Isometry3d &pose_in_map,
             Eigen::Isometry3d &pose_in_local, std::array<float, 2> &num_cells,
             std::array<float, 2> &newMax, std::vector<SubmapID> &newConstraints,
             std::vector<SubmapTexture> &newTextures)
      : submapVersion(submap_version), id(std::move(newId)), poseInMap(pose_in_map),
        poseInLocal(pose_in_local), numCells(num_cells), maxCells(newMax), constraints(newConstraints),
        textures(newTextures){};

  SaveSubmap() : submapVersion(0){};

  SaveSubmap(SaveSubmap &&old) noexcept
  {
    submapVersion = old.submapVersion;
    id = std::move(old.id);
    poseInMap = std::move(old.poseInMap);
    poseInLocal = std::move(old.poseInLocal);
    numCells = std::move(old.numCells);
    maxCells = std::move(old.maxCells);
    constraints = std::move(old.constraints);
    textures = std::move(old.textures);
  }

  SaveSubmap &operator=(SaveSubmap &&old)
  {
    submapVersion = old.submapVersion;
    id = std::move(old.id);
    poseInMap = std::move(old.poseInMap);
    poseInLocal = std::move(old.poseInLocal);
    numCells = std::move(old.numCells);
    maxCells = std::move(old.maxCells);
    constraints = std::move(old.constraints);
    textures = std::move(old.textures);
  }
};

struct SubmapImageEntry
{
  int32_t trajectoryId;
  int32_t submapIndex;
  int32_t submapVersion;
  double resolution;
  Eigen::Isometry3d poseInMap;
  CompressedImage image;
  SubmapImageEntry(){};
  SubmapImageEntry(int32_t newTrajectoryId, int32_t newSubmapIndex, int32_t newSubmapVersion,
                   double newResolution, Eigen::Isometry3d &&newPoseInMap,
                   CompressedImage &&newImage)
      : trajectoryId(newTrajectoryId), submapIndex(newSubmapIndex), submapVersion(newSubmapVersion),
        resolution(newResolution), poseInMap(newPoseInMap), image(std::move(newImage)){};
  SubmapImageEntry(SubmapImageEntry &&submapImageEntry)
  {
    trajectoryId = submapImageEntry.trajectoryId;
    submapIndex = submapImageEntry.submapIndex;
    submapVersion = submapImageEntry.submapVersion;
    resolution = submapImageEntry.resolution;
    poseInMap = std::move(submapImageEntry.poseInMap);
    image = std::move(submapImageEntry.image);
  }

  SubmapImageEntry(const SubmapImageEntry& submapImageEntry)
  {
	  trajectoryId = submapImageEntry.trajectoryId;
	  submapIndex = submapImageEntry.submapIndex;
	  submapVersion = submapImageEntry.submapVersion;
	  resolution = submapImageEntry.resolution;
	  poseInMap = submapImageEntry.poseInMap;
	  image = submapImageEntry.image;
  }

  SubmapImageEntry& operator=(const SubmapImageEntry& submapImageEntry)
  {
	  trajectoryId = submapImageEntry.trajectoryId;
	  submapIndex = submapImageEntry.submapIndex;
	  submapVersion = submapImageEntry.submapVersion;
	  resolution = submapImageEntry.resolution;
	  poseInMap = submapImageEntry.poseInMap;
	  image = submapImageEntry.image;
      return *this;
  }
  SubmapImageEntry &operator=(SubmapImageEntry &&submapImageEntry)noexcept
  {
    trajectoryId = submapImageEntry.trajectoryId;
    submapIndex = submapImageEntry.submapIndex;
    submapVersion = submapImageEntry.submapVersion;
    resolution = submapImageEntry.resolution;
    poseInMap = std::move(submapImageEntry.poseInMap);
    image = std::move(submapImageEntry.image);
    return *this;
  }
};

struct LandmarkNewEntry
{
  std::string ns;
  std::string id;
  int32_t visible;
  Eigen::Isometry3d trackingFromLandmarkTransform;
  double translationWeight;
  double rotationWeight;
  double poleRadius;

  LandmarkNewEntry(std::string &newNs, std::string &newId, int32_t newVisible,
                   Eigen::Isometry3d &&newTrackingFromLandmarkTransform,
                   double newTranslationWeight, double newRotationWeight, double newPoleRadius)
      : ns(newNs), id(newId), visible(newVisible),
        trackingFromLandmarkTransform(newTrackingFromLandmarkTransform),
        translationWeight(newTranslationWeight), rotationWeight(newRotationWeight),
        poleRadius(newPoleRadius){};

  LandmarkNewEntry(LandmarkNewEntry &&landmarkNewEntry) noexcept
  {
    ns = std::move(landmarkNewEntry.ns);
    id = std::move(landmarkNewEntry.id);
    visible = landmarkNewEntry.visible;
    trackingFromLandmarkTransform = std::move(landmarkNewEntry.trackingFromLandmarkTransform);
    translationWeight = landmarkNewEntry.translationWeight;
    rotationWeight = landmarkNewEntry.rotationWeight;
    poleRadius = landmarkNewEntry.poleRadius;
  }

  LandmarkNewEntry &operator=(LandmarkNewEntry &&landmarkNewEntry) noexcept
  {
    ns = std::move(landmarkNewEntry.ns);
    id = std::move(landmarkNewEntry.id);
    visible = landmarkNewEntry.visible;
    trackingFromLandmarkTransform = std::move(landmarkNewEntry.trackingFromLandmarkTransform);
    translationWeight = landmarkNewEntry.translationWeight;
    rotationWeight = landmarkNewEntry.rotationWeight;
    poleRadius = landmarkNewEntry.poleRadius;
  }
};

struct LandmarkEntry
{
  std::string id;
  Eigen::Isometry3d trackingFromLandmarkTransform;
  std::vector<Eigen::Vector3f> observationPointsInBase;
  double translationWeight;
  double rotationWeight;
  double poleRadius;

  LandmarkEntry(std::string &newId, Eigen::Isometry3d &&newTrackingFromLandmarkTransform,
                std::vector<Eigen::Vector3f> &&newObservationPointsInBase,
                double newTranslationWeight, double newRotationWeight, double newPoleRadius)
      : id(newId), trackingFromLandmarkTransform(newTrackingFromLandmarkTransform),
        observationPointsInBase(newObservationPointsInBase),
        translationWeight(newTranslationWeight), rotationWeight(newRotationWeight),
        poleRadius(newPoleRadius){};
  LandmarkEntry(LandmarkEntry &&landmarkEntry) noexcept
  {
    id = std::move(landmarkEntry.id);
    trackingFromLandmarkTransform = std::move(landmarkEntry.trackingFromLandmarkTransform);
    observationPointsInBase = std::move(landmarkEntry.observationPointsInBase);
    translationWeight = landmarkEntry.translationWeight;
    rotationWeight = landmarkEntry.rotationWeight;
    poleRadius = landmarkEntry.poleRadius;
  }
  LandmarkEntry &operator=(LandmarkEntry &&landmarkEntry) noexcept
  {
    id = std::move(landmarkEntry.id);
    trackingFromLandmarkTransform = std::move(landmarkEntry.trackingFromLandmarkTransform);
    observationPointsInBase = std::move(landmarkEntry.observationPointsInBase);
    translationWeight = landmarkEntry.translationWeight;
    rotationWeight = landmarkEntry.rotationWeight;
    poleRadius = landmarkEntry.poleRadius;
  }
};

struct LandmarkList
{
  Header header;
  std::vector<LandmarkEntry> landmarks;

  LandmarkList(){};
  LandmarkList(Header newHeader, std::vector<LandmarkEntry> &newLandmarks)
      : header(std::move(newHeader)), landmarks(std::move(newLandmarks)){};
  LandmarkList(LandmarkList &&landmarkList) noexcept
  {
    header = std::move(landmarkList.header);
    landmarks = std::move(landmarkList.landmarks);
  }
  LandmarkList &operator=(LandmarkList &&landmarkList) noexcept
  {
    header = std::move(landmarkList.header);
    landmarks = std::move(landmarkList.landmarks);
  }
};
} // namespace datatype

#endif