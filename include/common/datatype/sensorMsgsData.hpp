#ifndef SENSOR_MSGS_DATA
#define SENSOR_MSGS_DATA

#include <common/datatype/stdMsgsData.hpp>
#include <string>
#include <vector>
namespace datatype
{
struct CompressedImage
{
  Header header;
  std::string format;
  std::vector<uint8_t> data;

  CompressedImage(){};
  CompressedImage(Header &newHeader, const std::string &newFormat,
                  const std::vector<uint8_t> &newData)
      : header(newHeader), format(newFormat), data(newData){};
  CompressedImage(CompressedImage &&compressedImage) noexcept
  {
    header = std::move(compressedImage.header);
    format = std::move(compressedImage.format);
    data = std::move(compressedImage.data);
  }
  CompressedImage& operator=(const CompressedImage& compressedImage)
  {
	  header = compressedImage.header;
	  format = compressedImage.format;
	  data = compressedImage.data;
      return *this;
  }
  CompressedImage &operator=(CompressedImage &&compressedImage) noexcept
  {
    header = std::move(compressedImage.header);
    format = std::move(compressedImage.format);
    data = std::move(compressedImage.data);
    return *this;
  }
};
}; // namespace datatype
#endif