#ifndef WEBSOCK_DATETYPR
#define WEBSOCK_DATETYPR
#include "transform.h"
struct WebsockSubmap
{
	std::string smidStr;
	int32_t version;
	double resolution;
	Rigid3d poseInMap;

	WebsockSubmap(std::string smidStrNew, int32_t versionNew, double resolutionNew,
		Rigid3d poseInMapNew) :smidStr(smidStrNew), version(versionNew), resolution(resolutionNew), poseInMap(poseInMapNew) {};
};

struct WebsockLandmark
{
	int32_t visible;
	float translation_weight;
	float rotation_weight;
	float pole_radius;
	std::string ns;
	std::string id;
	Rigid3d tracking_from_landmark_transform;

	WebsockLandmark(int32_t visibleNew,
		float translationWeightNew,
		float rotationWeightNew,
		float poleRadiusNew,
		std::string nsNew,
		std::string idNew,
		Rigid3d trackingFromLandmarkTransformNew) :visible(visibleNew), translation_weight(translationWeightNew),
		rotation_weight(rotationWeightNew),
		pole_radius(poleRadiusNew),
		ns(nsNew),
		id(idNew),
		tracking_from_landmark_transform(trackingFromLandmarkTransformNew) {};
};

#endif