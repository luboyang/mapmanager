#ifndef DATA_TRANS
#define DATA_TRANS

#include "common/datatype/cartographerRosMsgscommon/data.hpp"
#include "common/datatype/geometryMsgscommon/data.hpp"
#include "common/datatype/mapManagerMsgscommon/data.hpp"
#include "common/datatype/sensorMsgscommon/data.hpp"
#include "common/datatype/stdMsgscommon/data.hpp"
#include <cartographer_ros_msgs/GetPointsInImgFromGlobal.h>
#include <cartographer_ros_msgs/LandmarkList.h>
#include <cartographer_ros_msgs/LandmarkListServer.h>
#include <cartographer_ros_msgs/MapModify.h>
#include <cartographer_ros_msgs/ReferenceLocationFromMap.h>
#include <cartographer_ros_msgs/SaveSubmap.h>
#include <cartographer_ros_msgs/SubmapID.h>
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <cartographer_ros_msgs/SubmapTexture.h>
#include <cartographer_ros_msgs/UpdateLandmarkServer.h>
#include <map_manager_msgs/DetectVisualmarksService.h>
#include <map_manager_msgs/Get2dPoints.h>
#include <map_manager_msgs/GetImage.h>
#include <map_manager_msgs/IdRemapSrv.h>
#include <map_manager_msgs/Set2dPoints.h>
#include <map_manager_msgs/TagInfo.h>
#include <map_manager_msgs/TagInfos.h>
#include <map_manager_msgs/VisualmarksDetectInfo.h>
#include <sensor_msgs/CompressedImage.h>
#include "utility/eigen/eigen_utils.hpp"
// cartographer_ros_msgs
// LandmarkList
datatype::LandmarkList
CartographerRosMsgsToDataType(cartographer_ros_msgs::LandmarkList &&landmarkList)
{
  datatype::LandmarkList landmarkListData;
  datatype::RosTime rosTime{landmarkList.header.stamp.sec, landmarkList.header.stamp.nsec};
  landmarkListData.header = {landmarkList.header.seq, rosTime, landmarkList.header.frame_id};
  auto func =
      [&](const cartographer_ros_msgs::LandmarkEntry &landmark) -> std::vector<Eigen::Vector3f>
  {
    std::vector<Eigen::Vector3f> tmp;
    tmp.reserve(landmark.observation_points_in_base.size());
    for (const auto &observationPoint : landmark.observation_points_in_base)
    {
      tmp.emplace_back(std::move(EigenGeometryConversion::ToEigenVector3f(observationPoint)));
    }
    return tmp;
  };

  // std::vector<Eigen::Vector3f> obPoints;
  landmarkListData.landmarks.reserve(landmarkList.landmarks.size());
  for (auto &landmark : landmarkList.landmarks)
  {
    // obPoints.clear();
    // obPoints = func(landmark);
    datatype::LandmarkEntry landmarkData{
        landmark.id,
        EigenGeometryConversion::ToEigenIsometry3d(landmark.tracking_from_landmark_transform),
        func(landmark),
        landmark.translation_weight,
        landmark.rotation_weight,
        landmark.pole_radius};
    landmarkListData.landmarks.emplace_back(std::move(landmarkData));
  }
  return landmarkListData;
}

// SubmapID
datatype::SubmapID CartographerRosMsgsToDataType(cartographer_ros_msgs::SubmapID &&submapId)
{
  return datatype::SubmapID{submapId.trajectory_id, submapId.submap_index, submapId.submap_version};
}

cartographer_ros_msgs::SubmapID DataTypeToCartographerRosMsgs(datatype::SubmapID &&submapIdData)
{
  cartographer_ros_msgs::SubmapID submapId;
  submapId.trajectory_id = submapIdData.trajectoryId;
  submapId.submap_index = submapIdData.submapIndex;
  submapId.submap_version = submapIdData.submapVersion;
  return submapId;
}

// SubmapTexture
datatype::SubmapTexture
CartographerRosMsgsToDataType(cartographer_ros_msgs::SubmapTexture &&submapTexture)
{
  return datatype::SubmapTexture{submapTexture.cells, submapTexture.width, submapTexture.height,
                                 submapTexture.resolution,
                                 EigenGeometryConversion::ToEigenIsometry3d(submapTexture.slice_pose)};
}

cartographer_ros_msgs::SubmapTexture
DataTypeToCartographerRosMsgs(datatype::SubmapTexture &&submapTextureData)
{
  cartographer_ros_msgs::SubmapTexture submapTexture;
  submapTexture.cells = std::move(submapTextureData.cells);
  submapTexture.width = submapTextureData.width;
  submapTexture.height = submapTextureData.height;
  submapTexture.resolution = submapTextureData.resolution;
  submapTexture.slice_pose = EigenGeometryConversion::ToGeometryMsgPose(submapTextureData.slicePose);
  return submapTexture;
}

// SaveSubmap
datatype::SaveSubmap CartographerRosMsgsToDataType(cartographer_ros_msgs::SaveSubmap &&saveSubmap)
{
  datatype::SaveSubmap saveSubmapData;
  saveSubmapData.submapVersion = saveSubmap.submap_version;
  saveSubmapData.id = CartographerRosMsgsToDataType(std::move(saveSubmap.id));
  saveSubmapData.poseInMap = EigenGeometryConversion::ToEigenIsometry3d(saveSubmap.pose_in_map);
  saveSubmapData.poseInLocal = EigenGeometryConversion::ToEigenIsometry3d(saveSubmap.pose_in_local);
  saveSubmapData.numCells = {saveSubmap.num_cells[0], saveSubmap.num_cells[1]};
  saveSubmapData.max = {saveSubmap.max[0], saveSubmap.max[1]};
  saveSubmapData.constraints.reserve(saveSubmap.constraints.size());
  for (auto &constraint : saveSubmap.constraints)
  {
    saveSubmapData.constraints.emplace_back(
        std::move(CartographerRosMsgsToDataType(std::move(constraint))));
  }
  saveSubmapData.textures.reserve(saveSubmap.textures.size());
  for (auto &textures : saveSubmap.textures)
  {
    saveSubmapData.textures.emplace_back(
        std::move(CartographerRosMsgsToDataType(std::move(textures))));
  }
  return saveSubmapData;
}

cartographer_ros_msgs::SaveSubmap
DataTypeToCartographerRosMsgs(datatype::SaveSubmap &&saveSubmapData)
{
  cartographer_ros_msgs::SaveSubmap saveSubmap;
  saveSubmap.submap_version = saveSubmapData.submapVersion;
  saveSubmap.id = DataTypeToCartographerRosMsgs(std::move(saveSubmapData.id));
  saveSubmap.pose_in_map = EigenGeometryConversion::ToGeometryMsgPose(saveSubmapData.poseInMap);
  saveSubmap.pose_in_local = EigenGeometryConversion::ToGeometryMsgPose(saveSubmapData.poseInLocal);
  saveSubmap.num_cells = {saveSubmapData.numCells[0], saveSubmapData.numCells[1]};
  saveSubmap.max = {saveSubmapData.max[0], saveSubmapData.max[1]};
  saveSubmap.constraints.reserve(saveSubmapData.constraints.size());
  for (auto &constraint : saveSubmapData.constraints)
  {
    saveSubmap.constraints.emplace_back(
        std::move(DataTypeToCartographerRosMsgs(std::move(constraint))));
  }
  saveSubmap.textures.reserve(saveSubmapData.textures.size());
  for (auto &textures : saveSubmapData.textures)
  {
    saveSubmap.textures.emplace_back(std::move(DataTypeToCartographerRosMsgs(std::move(textures))));
  }
  return saveSubmap;
}

// SubmapImageEntry
datatype::SubmapImageEntry
CartographerRosMsgsToDataType(cartographer_ros_msgs::SubmapImageEntry &&submapImageEntry)
{
  return datatype::SubmapImageEntry{submapImageEntry.trajectory_id,
                                    submapImageEntry.submap_index,
                                    submapImageEntry.submap_version,
                                    submapImageEntry.resolution,
                                    EigenGeometryConversion::ToEigenIsometry3d(submapImageEntry.pose_in_map),
                                    SensorMsgsToDataType(std::move(submapImageEntry.image))};
}

cartographer_ros_msgs::SubmapImageEntry
DataTypeToCartographerRosMsgs(datatype::SubmapImageEntry &&submapImageEntryData)
{
  cartographer_ros_msgs::SubmapImageEntry submapImageEntry;
  submapImageEntry.trajectory_id = submapImageEntryData.trajectoryId;
  submapImageEntry.submap_index = submapImageEntryData.submapIndex;
  submapImageEntry.submap_version = submapImageEntryData.submapIndex;
  submapImageEntry.resolution = submapImageEntryData.resolution;
  submapImageEntry.pose_in_map = EigenGeometryConversion::ToGeometryMsgPose(submapImageEntryData.poseInMap);
  submapImageEntry.image = DataTypeToSensorMsgs(std::move(submapImageEntryData.image));
  return submapImageEntry;
}

// LandmarkNewEntry
datatype::LandmarkNewEntry
CartographerRosMsgsToDataType(cartographer_ros_msgs::LandmarkNewEntry &&landmarkNewEntry)
{
  return datatype::LandmarkNewEntry{
      landmarkNewEntry.ns,
      landmarkNewEntry.id,
      landmarkNewEntry.visible,
      EigenGeometryConversion::ToEigenIsometry3d(landmarkNewEntry.tracking_from_landmark_transform),
      landmarkNewEntry.translation_weight,
      landmarkNewEntry.rotation_weight,
      landmarkNewEntry.pole_radius};
}

cartographer_ros_msgs::LandmarkNewEntry
DataTypeToCartographerRosMsgs(datatype::LandmarkNewEntry &&landmarkNewEntryData)
{
  cartographer_ros_msgs::LandmarkNewEntry landmarkNewEntry;
  landmarkNewEntry.ns = landmarkNewEntryData.ns;
  landmarkNewEntry.id = landmarkNewEntryData.id;
  landmarkNewEntry.visible = landmarkNewEntryData.visible;
  landmarkNewEntry.tracking_from_landmark_transform =
      EigenGeometryConversion::ToGeometryMsgPose(landmarkNewEntryData.trackingFromLandmarkTransform);
  landmarkNewEntry.translation_weight = landmarkNewEntryData.translationWeight;
  landmarkNewEntry.rotation_weight = landmarkNewEntryData.rotationWeight;
  landmarkNewEntry.pole_radius = landmarkNewEntryData.poleRadius;
  return landmarkNewEntry;
}

// LandmarkEntry
datatype::LandmarkEntry
CartographerRosMsgsToDataType(cartographer_ros_msgs::LandmarkEntry &&landmarkEntry)
{
  auto func = [&](const std::vector<geometry_msgs::Point32> &observationPointsInBase)
      -> std::vector<Eigen::Vector3f>
  {
    std::vector<Eigen::Vector3f> tmp;
    tmp.reserve(observationPointsInBase.size());
    for (const auto &observationPoint : observationPointsInBase)
    {
      tmp.emplace_back(std::move(EigenGeometryConversion::ToEigenVector3f(observationPoint)));
    }
    return tmp;
  };
  return datatype::LandmarkEntry{
      landmarkEntry.id,
      EigenGeometryConversion::ToEigenIsometry3d(landmarkEntry.tracking_from_landmark_transform),
      func(landmarkEntry.observation_points_in_base),
      landmarkEntry.translation_weight,
      landmarkEntry.rotation_weight,
      landmarkEntry.pole_radius};
}

cartographer_ros_msgs::LandmarkEntry
DataTypeToCartographerRosMsgs(datatype::LandmarkEntry &&landmarkEntryData)
{
  auto func = [&](const std::vector<Eigen::Vector3f> &observationPointsInBase)
      -> std::vector<geometry_msgs::Point32>
  {
    std::vector<geometry_msgs::Point32> tmp;
    tmp.reserve(observationPointsInBase.size());
    for (const auto &observationPoint : observationPointsInBase)
    {
      tmp.emplace_back(std::move(EigenGeometryConversion::ToGeometryMsgPoint(observationPoint)));
    }
    return tmp;
  };
  cartographer_ros_msgs::LandmarkEntry landmarkEntry;
  landmarkEntry.id = std::move(landmarkEntryData.id);
  landmarkEntry.tracking_from_landmark_transform =
      EigenGeometryConversion::ToGeometryMsgPose(landmarkEntryData.trackingFromLandmarkTransform);
  landmarkEntry.observation_points_in_base = func(landmarkEntryData.observationPointsInBase);
  landmarkEntry.translation_weight = landmarkEntryData.translationWeight;
  landmarkEntry.rotation_weight = landmarkEntryData.rotationWeight;
  landmarkEntry.pole_radius = landmarkEntryData.poleRadius;
  return landmarkEntry;
}

// sensor_msgs
// CompressedImage
datatype::CompressedImage SensorMsgsToDataType(sensor_msgs::CompressedImage &&compressedImage)
{
  datatype::CompressedImage compressedImageData;
  datatype::RosTime rosTime{compressedImage.header.stamp.sec, compressedImage.header.stamp.nsec};
  compressedImageData.header = {compressedImage.header.seq, rosTime,
                                compressedImage.header.frame_id};
  compressedImageData.format = std::move(compressedImage.format);
  compressedImageData.data = std::move(compressedImage.data);
  return compressedImageData;
}

sensor_msgs::CompressedImage DataTypeToSensorMsgs(datatype::CompressedImage &&compressedImageData)
{
  sensor_msgs::CompressedImage compressedImage;
  compressedImage.header.seq = compressedImageData.header.seq;
  compressedImage.header.frame_id = std::move(compressedImageData.header.frameId);
  compressedImage.header.stamp = {compressedImageData.header.stamp.sec,
                                  compressedImageData.header.stamp.nsec};
  compressedImage.format = std::move(compressedImageData.format);
  compressedImage.data = std::move(compressedImageData.data);
  return compressedImage;
}

// map_manager_msgs
// Point2d
datatype::Point2d MapManagerMsgsToDataType(map_manager_msgs::Point2d &&point2d)
{
  return datatype::Point2d{point2d.x, point2d.y};
}

map_manager_msgs::Point2d DataTypeToMapManagerMsgs(datatype::Point2d &&point2dData)
{
  map_manager_msgs::Point2d point2d;
  point2d.x = point2dData.x;
  point2d.y = point2dData.y;
  return point2d;
}

// Points2d
datatype::Points2d MapManagerMsgsToDataType(map_manager_msgs::Points2d &&points2d)
{
  datatype::Points2d point2dData;
  point2dData.points.reserve(points2d.points.size());
  for (auto &point2d : points2d.points)
  {
    point2dData.points.emplace_back(std::move(MapManagerMsgsToDataType(std::move(point2d))));
  }
  return point2dData;
}

map_manager_msgs::Points2d DataTypeToMapManagerMsgs(datatype::Points2d &&points2dData)
{
  map_manager_msgs::Points2d points2d;
  points2d.points.reserve(points2dData.points.size());
  for (auto &point2d : points2dData.points)
  {
    points2d.points.emplace_back(std::move(DataTypeToMapManagerMsgs(std::move(point2d))));
  }
  return points2d;
}

// IdRemap
datatype::IdRemap MapManagerMsgsToDataType(map_manager_msgs::IdRemap &&idRemap)
{
  return datatype::IdRemap{idRemap.origin_id, idRemap.new_id, idRemap.ns};
}

map_manager_msgs::IdRemap DataTypeToMapManagerMsgs(datatype::IdRemap &&idRemapData)
{
  map_manager_msgs::IdRemap idRemap;
  idRemap.new_id = idRemapData.newId;
  idRemap.origin_id = idRemapData.originId;
  idRemap.ns = std::move(idRemapData.ns);
  return idRemap;
}

// TagMapInfo
datatype::TagMapInfo MapManagerMsgsToDataType(map_manager_msgs::TagMapInfo &&tagMapInfo)
{
  return datatype::TagMapInfo{
      tagMapInfo.tag_id, tagMapInfo.submap_id, EigenGeometryConversion::ToEigenIsometry3d(tagMapInfo.pose),
      EigenGeometryConversion::ToEigenIsometry3d(tagMapInfo.tag_pose_in_laser), tagMapInfo.neighbors_id};
}

map_manager_msgs::TagMapInfo DataTypeToMapManagerMsgs(datatype::TagMapInfo &&tagMapInfoData)
{
  map_manager_msgs::TagMapInfo tagMapInfo;
  tagMapInfo.tag_id = tagMapInfoData.tagId;
  tagMapInfo.submap_id = tagMapInfoData.submapId;
  tagMapInfo.pose = EigenGeometryConversion::ToGeometryMsgPose(tagMapInfoData.pose);
  tagMapInfo.tag_pose_in_laser = EigenGeometryConversion::ToGeometryMsgPose(tagMapInfoData.tagPoseInLaser);
  tagMapInfo.neighbors_id = std::move(tagMapInfoData.neighborsId);
  return tagMapInfo;
}

// VisualmarksDetectInfo
datatype::VisualmarksDetectInfo
MapManagerMsgsToDataType(map_manager_msgs::VisualmarksDetectInfo &&visualmarksDetectInfo)
{
  return datatype::VisualmarksDetectInfo{visualmarksDetectInfo.tag_id,
                                         visualmarksDetectInfo.error_code,
                                         visualmarksDetectInfo.error_msg};
}

map_manager_msgs::VisualmarksDetectInfo
DataTypeToMapManagerMsgs(datatype::VisualmarksDetectInfo &&visualmarksDetectInfoData)
{
  map_manager_msgs::VisualmarksDetectInfo visualmarksDetectInfo;
  visualmarksDetectInfo.tag_id = visualmarksDetectInfoData.tagId;
  visualmarksDetectInfo.error_code = visualmarksDetectInfoData.errorCode;
  visualmarksDetectInfo.error_msg = std::move(visualmarksDetectInfoData.errorMsg);
  return visualmarksDetectInfo;
}

// geometry_msgs
// PoseStamped
datatype::PoseStamped GeometryMsgsToDataType(geometry_msgs::PoseStamped &&poseStamped)
{
  datatype::PoseStamped poseStampedData;
  datatype::RosTime rosTime{poseStamped.header.stamp.sec, poseStamped.header.stamp.nsec};
  poseStampedData.header = {poseStamped.header.seq, rosTime, poseStamped.header.frame_id};
  poseStampedData.pose = EigenGeometryConversion::ToEigenIsometry3d(poseStamped.pose);
  return poseStampedData;
}

geometry_msgs::PoseStamped DataTypeToGeometryMsgs(datatype::PoseStamped &&poseStampedData)
{
  geometry_msgs::PoseStamped poseStamped;
  poseStamped.header.seq = poseStampedData.header.seq;
  poseStamped.header.frame_id = std::move(poseStampedData.header.frameId);
  poseStamped.header.stamp = {poseStampedData.header.stamp.sec, poseStampedData.header.stamp.nsec};
  poseStamped.pose = EigenGeometryConversion::ToGeometryMsgPose(poseStampedData.pose);
  return poseStamped;
}

#endif