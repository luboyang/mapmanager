#pragma once
#include <string>
#include <fstream>
#include <common/error_info.hpp>
#include <utility/system/utils_system.hpp>
#include <utility/string/utils_string.hpp>
#include <nlohmann/json.hpp>
#include <filesystem>
using namespace common;
using json = nlohmann::json;
class Map
{
public:
	Map(const std::string& mapPath) :mapPath_(mapPath) {
		std::vector<std::string> tmpStrs;
		stringutils::SplitString(mapPath_, tmpStrs, "\\");
		mapName_ = tmpStrs.back();
	};
	~Map() {};

	ErrorInfo GetVersion(std::string& versionStr)
	{
		std::string versionFilePathStr = mapPath_ + "//version";
		std::ifstream versionFile;
		versionFile.open(versionFilePathStr, std::ios::in);
		if (!versionFile.is_open())
		{
			return ErrorInfo(-1, "Can not open version file", ErrorType::kError);
		}

		std::getline(versionFile, versionStr);

		return ErrorInfo();
	}

	std::string GetMapPath()
	{
		return mapPath_;
	}

	std::string GetMapName()
	{
		return mapName_;
	}

	ErrorInfo CreateSubmapImageCache()
	{
		std::string errorMsgs;
		std::string submapImageCachePath = mapPath_ + "\\.submapImageCache";
		if (!systemutils::CreatePath(submapImageCachePath, errorMsgs))
		{
			return ErrorInfo(-1, errorMsgs, ErrorType::kError);
		}

		return ErrorInfo();
	}

	ErrorInfo GetInfoWorldReferJson(std::string &infoWorldReferJsonStr)
	{
		std::string infoWorldReferJsonPath = mapPath_ + "\\info_WorldRefer.json";
		std::cout << infoWorldReferJsonPath << std::endl;
		std::ifstream jsonFile(infoWorldReferJsonPath.c_str());
		if (!jsonFile.is_open())
		{
			return ErrorInfo(-1, "Can not open info_WorldRefer.json, file path = " + infoWorldReferJsonPath, ErrorType::kError);
		}
		json infoWorldJson;
		jsonFile >> infoWorldJson;
		if (!infoWorldJson.is_object() && !infoWorldJson.is_array())
		{
			return ErrorInfo(-1, "Can not open info_WorldRefer.json, file path = " + infoWorldReferJsonPath, ErrorType::kError);
		}
		jsonFile.close();
		infoWorldReferJsonStr = infoWorldJson.dump();
		return ErrorInfo();
	}

	ErrorInfo SetInfoWorldReferJson(const std::string& infoWorldReferJsonStr)
	{
		std::string infoWorldReferJsonPath = mapPath_ + "/info_WorldRefer.json";
		//json infoWorldJson;
		//infoWorldJson.parse(infoWorldReferJsonStr);
		std::ofstream jsonFile(infoWorldReferJsonPath.c_str(), std::ios::trunc);
		if (!jsonFile.is_open())
		{
			return ErrorInfo(-1, "Can not open info_WorldRefer.json, file path = " + infoWorldReferJsonPath, ErrorType::kError);
		}
		//jsonFile << infoWorldJson.dump(4);
		jsonFile << infoWorldReferJsonStr;
		jsonFile.close();

		return ErrorInfo();
	}

	ErrorInfo GetInfoImageReferJson(const std::string& infoImageReferJsonStr)
	{
		return ErrorInfo();
	}

	ErrorInfo SetInfoImageReferJson(const std::string& infoImageReferJsonStr)
	{
		return ErrorInfo();
	}

	ErrorInfo DeleteImageCache()
	{
		std::string errorMsgs;
		std::string submapImageCachePath = mapPath_ + "\\.submapImageCache";
		systemutils::RemoveDirectory(submapImageCachePath, errorMsgs);
		if (!errorMsgs.empty())
		{
			return ErrorInfo(-1, errorMsgs, ErrorType::kError);
		}

		return ErrorInfo();
	}
private:
	std::string mapPath_;
	std::string mapName_;
};