#pragma once
#include <string>
#include <common/error_info.hpp>
#include <map>
#include <common/datatype/websockDatatype.hpp>
#include "map.hpp"
#include <memory>
#include <queue>
#include "load_submaps_server_service.h"
#include "load_landmarks_server_service.h"
#include "reference_location_service.h"
#include "modify_map_server_service.h"
#include <mutex>
#include <shared_mutex>
#include <tuple>
using namespace common;

class MapEditorCtl
{
public:
	using ptr = std::shared_ptr<MapEditorCtl>;
	MapEditorCtl();

	ErrorInfo LoadMap(const std::string& mapName);
	ErrorInfo GetSubmap(const std::string& idStr, const std::string& versionStr, const bool needPng, std::string& pngData);
	ErrorInfo CloseMap();
	ErrorInfo GetMapInfo(const std::string& mapName, std::string& infoWorldReferJsonStr);
	ErrorInfo SetLocationRegion(const std::string& groundsJsonStr);
	ErrorInfo SetWorldPoseToReferencePose(std::string& nodesJsonStr);
	ErrorInfo SetInfoJson(const std::string& infoWorldReferJsonStr);
	ErrorInfo AdjustedMap(const std::string& adjustedMapJsonStr);
	std::queue<WebsockSubmap> websockSubmapQueue_;
	std::queue<WebsockLandmark> websockLandmarkQueue_;
	std::mutex submapMutex_;
	std::mutex landmarkMutex_;
private:
	ErrorInfo LoadSubmap();
	ErrorInfo LoadLandmarks();
	ErrorInfo ReloadMap();

	std::map<std::string, output_data_type::SubmapImageEntry> submapIdWithSubmap_;
	std::map<std::string, std::string> submapIdWithSubmapIdVersion_;
	std::unique_ptr<Map> mapPtr_;

	std::unique_ptr<LoadSubmapsServerService> loadSubmapPtr_;
	std::unique_ptr<LoadLandmarksService> loadLandmarksPtr_;
	std::unique_ptr<ReferenceLocationService> referenceLocalitionPtr_;
	std::unique_ptr<ModifyMapServerService> modifyMapPtr_;
	//std::string mapDirPath_;

	std::tuple<std::vector<double>, double> maxesAndResolution_;
	std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> posesInGlobal_;
};