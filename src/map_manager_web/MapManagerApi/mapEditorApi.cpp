#include "mapEditorApi.h"
#include <fstream>
#include <common/datatype/cartographerRosMsgsData.hpp>
#include <utility/cv/utils_cv.hpp>
#include <utility/system/utils_system.hpp>
#include <utility/string/utils_string.hpp>
#include <common/datatype/output_data_type/output_data.hpp>
#include <common/datatype/file_data_type/file_data.hpp>
#include <nlohmann/json.hpp>

MapEditorCtl::MapEditorCtl()
{
	loadSubmapPtr_ = std::make_unique<LoadSubmapsServerService>();
	loadLandmarksPtr_ = std::make_unique<LoadLandmarksService>();
	referenceLocalitionPtr_ = std::make_unique<ReferenceLocationService>();
	modifyMapPtr_ = std::make_unique<ModifyMapServerService>();
	//mapDirPath_ = systemutils::GetCurrentPath() + "\\map";
	//std::cout << mapDirPath_ << std::endl;
}

ErrorInfo MapEditorCtl::LoadMap(const std::string& mapName)
{
	if (mapName.empty())
	{
		return ErrorInfo(-1,"mapName invalid",ErrorType::kError);
	}
	//std::string mapPath = mapDirPath_ + "\\" + mapName;
	std::string mapPath = mapName;
	std::cout << mapPath << std::endl;
	if (!systemutils::JudgeFolderExist(mapPath))
	{
		return ErrorInfo(-1, "Target path is not map Dir", ErrorType::kError);
	}

	mapPtr_ = std::make_unique<Map>(mapPath);

	std::string versionStr;
	auto getVersionResult = mapPtr_->GetVersion(versionStr);
	if (!getVersionResult.IsOk())
	{
		return ErrorInfo(-1, "Get version failed, errorMsg is " + getVersionResult.errorMsg, ErrorType::kError);
	}
	// version版本2.3写死的，以后应引出作为配置项读取
	if (versionStr != "2.3")
	{
		return ErrorInfo(-1, "Version is not 2.3, version is " + versionStr, ErrorType::kError);
	}
	
	auto getCreateCacheResult = mapPtr_->CreateSubmapImageCache();
	if (!getCreateCacheResult.IsOk())
	{
		return ErrorInfo(-1, "Create submap image cache failed, errorMsg is " + getCreateCacheResult.errorMsg, ErrorType::kError);
	}

	auto loadSubmapResult = LoadSubmap();
	if (!loadSubmapResult.IsOk())
	{
		return ErrorInfo(-1, "Load submap failed, errorMsg is " + loadSubmapResult.errorMsg, ErrorType::kError);
	}

	auto loadLandmarksResult = LoadLandmarks();
	if (!loadLandmarksResult.IsOk())
	{
		return ErrorInfo(-1, "Load landmarks failed, errorMsg is " + loadLandmarksResult.errorMsg, ErrorType::kError);
	}

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::LoadSubmap()
{
	std::vector<output_data_type::SubmapImageEntry> submapList;
	
	//调用接口获取submaplist
	output_data_type::MapPosesInfo mapPosesInfo;
	
	std::string mapPath = mapPtr_->GetMapPath();
	std::cout << mapPath << std::endl;
	auto result = loadSubmapPtr_->Init(mapPath);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Load submap service init failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	result = loadSubmapPtr_->GetSubmapImages(submapList, mapPosesInfo);
	if(!result.IsOk()) 
	{
		return ErrorInfo(-1, "Get submap images failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}
	std::cout << 11111111111111111111 << std::endl;
	maxesAndResolution_ = std::make_tuple(mapPosesInfo.maxes, 0.05);
	std::swap(posesInGlobal_, mapPosesInfo.poses_in_global);
	std::lock_guard<std::mutex> lock(submapMutex_);
	for (const auto &submap : submapList)
	{
		std::string submapIndexStr = std::to_string(submap.trajectory_id) + "-" + std::to_string(submap.submap_index);
		output_data_type::SubmapImageEntry lastSubmap;
		WebsockSubmap newWebsockSubmap{ submapIndexStr, submap.submap_version,submap.resolution,submap.pose_in_map };
		websockSubmapQueue_.push(newWebsockSubmap);
		std::cout << "websockSubmapQueue_,size = " << websockSubmapQueue_.size() << std::endl;
		std::string submapIdAndVersionStr = std::to_string(submap.trajectory_id) +"-"+ std::to_string(submap.submap_index) + 
			"-" + std::to_string(submap.submap_version);
		if (!!submapIdWithSubmap_.count(submapIdAndVersionStr))
		{
			lastSubmap = submapIdWithSubmap_.at(submapIdAndVersionStr);
		}
		std::string pngPath = mapPath + "\\" + ".submapImageCache" + "\\" + submapIdAndVersionStr + ".png";

		std::cout << pngPath << std::endl;
		cvutils::SetPngFromCvMat(pngPath, submap.image);
		std::cout << 333333333333 << std::endl;
		submapIdWithSubmap_.insert(std::make_pair(submapIdAndVersionStr, lastSubmap));
		
		
		if (!!submapIdWithSubmapIdVersion_.count(submapIndexStr))
		{
			std::string oldIndexStr = submapIdWithSubmapIdVersion_.at(submapIndexStr);
			if (oldIndexStr != submapIdAndVersionStr)
			{
				auto old = submapIdWithSubmap_.at(oldIndexStr);
				//考虑是否加入超时删除
			}
		}

		submapIdWithSubmapIdVersion_.insert(std::make_pair(submapIndexStr, submapIdAndVersionStr));
	}

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::LoadLandmarks()
{
	auto mapPath = mapPtr_->GetMapPath();
	auto result = loadLandmarksPtr_->Init(mapPath);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Load landmarks service init failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}
	std::vector<file_data_type::LandmarkInfo> markerList;
	result = loadLandmarksPtr_->LoadLandmarksData(markerList);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Load landmarks failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	std::lock_guard<std::mutex> lock(landmarkMutex_);
	for (const auto& landmark : markerList)
	{
		WebsockLandmark newWebsockLandmark{ landmark.visible,landmark.translation_weight,landmark.rotation_weight,landmark.pole_radius,landmark.ns,landmark.id,landmark.transform };
		websockLandmarkQueue_.push(newWebsockLandmark);
	}

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::GetSubmap(const std::string& idStr, const std::string& versionStr, const bool needPng, std::string& imgPath)
{
	if (mapPtr_ == nullptr)
	{
		return ErrorInfo(-1, "Have not load map", ErrorType::kError);
	}
	std::string submapIndexStr;
	std::string submapIdAndVersionStr;
	std::vector<std::string> idStrs;
	stringutils::SplitString(idStr, idStrs, "-");
	if (idStrs.size() >= 2)
	{
		submapIndexStr = idStrs[0] + "-" + idStrs[1];
		std::cout << submapIndexStr << std::endl;
		if (!submapIdWithSubmapIdVersion_.count(submapIndexStr))
		{
			return ErrorInfo(-1, "SubmapIdWithVersion string not found", ErrorType::kError);
		}

		if (!versionStr.empty())
		{
			submapIdAndVersionStr = submapIdWithSubmapIdVersion_.at(submapIndexStr);
		}
		else
		{
			submapIdAndVersionStr = submapIndexStr + "-" + versionStr;
		}

		if (!submapIdWithSubmap_.count(submapIdAndVersionStr))
		{
			return ErrorInfo(-1, "Submap not found", ErrorType::kError);
		}
		
		auto submap = submapIdWithSubmap_.at(submapIdAndVersionStr);
		if (mapPtr_ == nullptr)
		{
			return ErrorInfo(-1, "Map has been closed", ErrorType::kError);
		}
		std::cout << mapPtr_->GetMapPath() << std::endl;
		std::string pngPath = mapPtr_->GetMapPath() + "\\.submapImageCache" + "\\" + submapIdAndVersionStr + ".png";
		std::string thresPngPath = mapPtr_->GetMapPath() + "\\.submapImageCache" + "\\" + submapIdAndVersionStr + "_thres.png";
		std::string overlayJpgPath = mapPtr_->GetMapPath() + "\\overlay.jpg";
		std::string jpgPath = mapPtr_->GetMapPath() + "\\.submapImageCache" + "\\" + submapIdAndVersionStr + ".jpg";

		//3D图待添加
		if (needPng)
		{
			if (!systemutils::JudgeFileExist(thresPngPath))
			{
				cvutils::SetPngToThresPng(pngPath, thresPngPath);
			}
			imgPath = thresPngPath;
		}
		else
		{
			if (submapIdAndVersionStr == "0-0-0")
			{
				imgPath = overlayJpgPath;
			}
			else
			{
				if (!systemutils::JudgeFileExist(jpgPath))
				{
					cvutils::SetPngToJpg(pngPath, jpgPath);
				}
				imgPath = jpgPath;
			}
		}
	}
	return ErrorInfo();
}

ErrorInfo MapEditorCtl::CloseMap()
{
	if (mapPtr_ != nullptr)
	{
		mapPtr_.release();
	}
	return ErrorInfo();
}

ErrorInfo MapEditorCtl::GetMapInfo(const std::string& mapName,std::string & infoWorldReferJsonStr)
{
	std::string cachePath;
	if (mapName.empty())
	{
		return ErrorInfo(-1, "mapName invalid", ErrorType::kError);
	}

	auto result = mapPtr_->GetInfoWorldReferJson(infoWorldReferJsonStr);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, result.errorMsg, ErrorType::kError);
	}

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::SetLocationRegion(const std::string & groundsJsonStr)
{
	nlohmann::json groundsJson= nlohmann::json::parse(groundsJsonStr);
	

	// /map_manager_localization/set_location_region
	return ErrorInfo();
}

ErrorInfo MapEditorCtl::SetWorldPoseToReferencePose(std::string& nodesJsonStr)
{
	nlohmann::json nodesJson = nlohmann::json::parse(nodesJsonStr);
	std::vector<Rigid3d> worldPoseList;
	worldPoseList.reserve(nodesJson.size());

	for (const auto& nodeJson : nodesJson)
	{
		Eigen::Vector3d tempPosition{
			static_cast<double>(nodeJson["worldPose"]["position"]["x"]),
			static_cast<double>(nodeJson["worldPose"]["position"]["y"]),
			static_cast<double>(nodeJson["worldPose"]["position"]["z"]) };
		Eigen::Quaterniond tempOrientation{
			static_cast<double>(nodeJson["worldPose"]["orientation"]["w"]),
			static_cast<double>(nodeJson["worldPose"]["orientation"]["x"]),
			static_cast<double>(nodeJson["worldPose"]["orientation"]["y"]),
			static_cast<double>(nodeJson["worldPose"]["orientation"]["z"]) };
		Rigid3d tmpPose{ tempPosition,tempOrientation };
		worldPoseList.emplace_back(tmpPose);
	}
	
	std::vector<output_data_type::ReferenceLocationData> refInfoList;

	auto result = referenceLocalitionPtr_->getReferenceInfos(posesInGlobal_, worldPoseList, refInfoList);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Get reference infos failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}
	// reference_location_server(worldPoseList,referenceIdList,referencePoseList,newWorldPoseList);

	size_t index = 0;
	for (auto& nodeJson : nodesJson)
	{
		nodeJson["atMap"] = refInfoList.at(index).reference_id;
		nodeJson["pose"] = { 
			refInfoList.at(index).reference_pose.translation().x(),
			refInfoList.at(index).reference_pose.translation().y(),
			refInfoList.at(index).reference_pose.translation().z(),
			refInfoList.at(index).reference_pose.rotation().x(),
			refInfoList.at(index).reference_pose.rotation().y(),
			refInfoList.at(index).reference_pose.rotation().z(),
			refInfoList.at(index).reference_pose.rotation().w(), };
		nodeJson["worldPose"]["position"]["x"] = refInfoList.at(index).world_pose.translation().x();
		nodeJson["worldPose"]["position"]["y"] = refInfoList.at(index).world_pose.translation().y();
		nodeJson["worldPose"]["position"]["z"] = refInfoList.at(index).world_pose.translation().z();
		nodeJson["worldPose"]["orientation"]["w"] = refInfoList.at(index).world_pose.rotation().w();
		nodeJson["worldPose"]["orientation"]["x"] = refInfoList.at(index).world_pose.rotation().x();
		nodeJson["worldPose"]["orientation"]["y"] = refInfoList.at(index).world_pose.rotation().y();
		nodeJson["worldPose"]["orientation"]["z"] = refInfoList.at(index).world_pose.rotation().z();
		index++;
	}
	nodesJsonStr = nodesJson.dump();

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::SetInfoJson(const std::string& infoWorldReferJsonStr)
{
	auto result = mapPtr_->SetInfoWorldReferJson(infoWorldReferJsonStr);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Set InfoWorldRefer.Json failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	nlohmann::json nodesJson = nlohmann::json::parse(infoWorldReferJsonStr);


	return ErrorInfo();
}

ErrorInfo MapEditorCtl::AdjustedMap(const std::string& adjustedMapJsonStr)
{
	nlohmann::json adjustedMapJson = nlohmann::json::parse(adjustedMapJsonStr);
	std::vector<std::string> tmpStrs;
	std::vector<output_data_type::SubmapImageEntry> modifyMapList;
	modifyMapList.reserve(adjustedMapJson.size());
	for (const auto& adjustedMap : adjustedMapJson)
	{
		auto id = adjustedMap["id"];
		std::string img = adjustedMap["img"];
		
		stringutils::SplitString(img, tmpStrs, ",");
		if (tmpStrs.size() <= 1)
		{
			return ErrorInfo(-1, "Image data string error", ErrorType::kError);
		}
		auto imgDataBase64Str = tmpStrs[1];
		auto imgDataStr = stringutils::base64_decode(imgDataBase64Str);

		output_data_type::SubmapImageEntry modifyMap;

		modifyMap.submap_index = id;
		modifyMap.image = cvutils::Base64StringToCvMat(imgDataStr);
		modifyMapList.emplace_back(modifyMap);
	}
	auto mapName = mapPtr_->GetMapName();
	auto mapPath = mapPtr_->GetMapPath();
	auto result = modifyMapPtr_->Init(mapPath, std::get<0>(maxesAndResolution_), std::get<1>(maxesAndResolution_));
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Modify map service init failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	result = modifyMapPtr_->ModifyMap(modifyMapList);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Modify map failed, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	//ModifyMapServer(mapName,modifyMapList)

	mapPtr_->DeleteImageCache();
	
	result = ReloadMap();
	if (!result.IsOk())
	{
		return result;
	}

	return ErrorInfo();
}

ErrorInfo MapEditorCtl::ReloadMap()
{
	auto mapPath = mapPtr_->GetMapPath();
	CloseMap();

	auto result = LoadMap(mapPath);
	if (!result.IsOk())
	{
		return ErrorInfo(-1, "Reload map error, errormsgs is " + result.errorMsg, ErrorType::kError);
	}

	return ErrorInfo();
}