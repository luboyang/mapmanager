#include "MapEditor.h"
#include <iostream>
// Add definition of your processing function here
void MapEditor::LoadSubmapFunc(const HttpRequestPtr& req,
	std::function<void(const HttpResponsePtr&)>&& callback,
	const std::string& mapName, const bool& odom, const bool& imu, const bool& laser)
{
	std::cout << mapName << std::endl;
	Json::Value ret;
	auto result = mapEditorPtr_->LoadMap(mapName);
	if (!result.IsOk())
	{
		ret["result"] = result.errorMsg;
		ret["status"] = 500;
	}
	else
	{
		ret["result"] = "ok";
		ret["status"] = 200;
	}
	auto resp = HttpResponse::newHttpJsonResponse(ret);
	callback(resp);
}

void MapEditor::GetSubmapFunc(const HttpRequestPtr& req,
	std::function<void(const HttpResponsePtr&)>&& callback,
	const std::string& idStr,
	const std::string& versionStr,
	const std::string& needPNG)
{
	std::cout << "GetSubmapFunc" << std::endl;
	std::cout << idStr << std::endl;
	std::cout << versionStr << std::endl;
	std::cout << needPNG << std::endl;

	std::string imgPath;
	Json::Value ret;
	bool needPngFlag = true;
	if (needPNG != "true")
	{
		needPngFlag = false;
	}
	auto result = mapEditorPtr_->GetSubmap(idStr, versionStr, needPngFlag, imgPath);
	if (!result.IsOk())
	{
		ret["result"] = result.errorMsg;
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}
	
	auto resFile = HttpResponse::newFileResponse(imgPath);
	callback(resFile);
}

void MapEditor::GetMapInfoFunc(const HttpRequestPtr& req,
	std::function<void(const HttpResponsePtr&)>&& callback, const std::string& mapName)
{
	std::cout << mapName << std::endl;
	std::string infoWorldReferJsonStr;
	Json::Value ret;
	auto result = mapEditorPtr_->GetMapInfo(mapName, infoWorldReferJsonStr);
	if (!result.IsOk())
	{
		ret["result"] = result.errorMsg;
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}
	
	std::istringstream iss(infoWorldReferJsonStr);
	iss >> ret;
	auto resp = HttpResponse::newHttpJsonResponse(ret);
	resp->setStatusCode(k200OK);
	callback(resp);
}

void MapEditor::SetTotalStatusFunc(const HttpRequestPtr& req,
	std::function<void(const HttpResponsePtr&)>&& callback)
{
	auto reqDataStr = req->body();
	Json::Value reqJson;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(reqDataStr.data(), reqDataStr.data() + reqDataStr.size(), reqJson);
	if (!parsingSuccessful)
	{
		std::cout << reqDataStr << std::endl;
		return;
	}
	Json::Value ret;
	Json::FastWriter writer;
	auto mapName = reqJson["mapName"];
	std::cout << mapName << std::endl;
	if (mapName.empty())
	{
		ret["result"] = "MapName is empty";
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}
	
	auto infos = reqJson["infos"];
	//std::cout << infos.asString() << std::endl;
	if (infos.empty())
	{
		ret["result"] = "InfoWorldRefer is empty";
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}

	/*auto grounds = infos["grounds"];
	if (!grounds.empty() && grounds.isArray())
	{
		std::string groundsStr = writer.write(grounds);
		auto result = mapEditorPtr_->SetLocationRegion(groundsStr);
		if (!result.IsOk())
		{
			ret["result"] = result.errorMsg;
			ret["status"] = 500;
			auto resp = HttpResponse::newHttpJsonResponse(ret);
			callback(resp);
			return;
		}
	}*/

	auto nodes = infos["nodes"];
	if (nodes.empty() || !nodes.isArray())
	{
		ret["result"] = "InfoWorldRefer nodes is invalid";
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}

	std::string nodesStr = writer.write(nodes);
	std::cout << nodesStr << std::endl;
	auto result = mapEditorPtr_->SetWorldPoseToReferencePose(nodesStr);
	if (!result.IsOk())
	{
		ret["result"] = result.errorMsg;
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}

	Json::Value newNodes;
	std::istringstream iss(nodesStr);
	iss >> newNodes;
	infos["nodes"] = newNodes;

	std::string infoWorldReferJsonStr = writer.write(infos);
	result = mapEditorPtr_->SetInfoJson(infoWorldReferJsonStr);
	if (!result.IsOk())
	{
		ret["result"] = result.errorMsg;
		ret["status"] = 500;
		auto resp = HttpResponse::newHttpJsonResponse(ret);
		callback(resp);
		return;
	}

	// request中请求的内容，导航相关，暂时无用
	//auto dontInfomNav = reqJson->get("dontInfomNav", "");

	auto adjustedMap = reqJson["adjustedMap"];
	if (!adjustedMap.empty())
	{
		std::string adjustedMapJsonStr= writer.write(adjustedMap);
		auto result = mapEditorPtr_->AdjustedMap(adjustedMapJsonStr);
		if (!result.IsOk())
		{
			ret["result"] = result.errorMsg;
			ret["status"] = 500;
			auto resp = HttpResponse::newHttpJsonResponse(ret);
			callback(resp);
			return;
		}
	}

	ret["result"] = "ok";
	ret["status"] = 200;
	auto resp = HttpResponse::newHttpJsonResponse(ret);
	callback(resp);
	return;
}

void MapEditor::CloseMap(const HttpRequestPtr& req,
	std::function<void(const HttpResponsePtr&)>&& callback)
{
	Json::Value ret;
	mapEditorPtr_->CloseMap();
	ret["result"] = "ok";
	ret["status"] = 200;
	auto resp = HttpResponse::newHttpJsonResponse(ret);
	callback(resp);
	return;
}