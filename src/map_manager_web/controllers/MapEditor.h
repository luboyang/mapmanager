#pragma once

#include <drogon/HttpController.h>
#include "mapEditorApi.h"
using namespace drogon;

class MapEditor : public drogon::HttpController<MapEditor>
{
  public:
	  MapEditor(const std::shared_ptr<MapEditorCtl> &mapPtr):mapEditorPtr_(mapPtr) {
	  }
    METHOD_LIST_BEGIN
    // use METHOD_ADD to add your custom processing function here;
    // METHOD_ADD(MapEditor::get, "/{2}/{1}", Get); // path is /MapEditor/{arg2}/{arg1}
    // METHOD_ADD(MapEditor::your_method_name, "/{1}/{2}/list", Get); // path is /MapEditor/{arg1}/{arg2}/list
    // ADD_METHOD_TO(MapEditor::your_method_name, "/absolute/path/{1}/{2}/list", Get); // path is /absolute/path/{arg1}/{arg2}/list

	ADD_METHOD_TO(MapEditor::LoadSubmapFunc, "/robot/loadMap?mapName={1}&odom={2}&imu={3}&mutilLaser={4}", Get);
	ADD_METHOD_TO(MapEditor::GetSubmapFunc, "/robot/getSubmap?id={1}&version={2}&needPNG={3}", Get);
	ADD_METHOD_TO(MapEditor::CloseMap, "/robot/closeMap", Get);
	ADD_METHOD_TO(MapEditor::GetMapInfoFunc, "/maps/mapInfo?mapName={1}", Get);
	ADD_METHOD_TO(MapEditor::SetTotalStatusFunc, "/maps/save", Post, Options);
	METHOD_LIST_END
		// your declaration of processing function maybe like this:
		// void get(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, int p1, std::string p2);
		// void your_method_name(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, double p1, int p2) const;
	void LoadSubmapFunc(const HttpRequestPtr& req,
		std::function<void(const HttpResponsePtr&)>&& callback,
		const std::string& mapName,
		const bool& odom, 
		const bool& imu, 
		const bool& laser);
	void GetSubmapFunc(const HttpRequestPtr& req,
		std::function<void(const HttpResponsePtr&)>&& callback,
		const std::string& idStr,
		const std::string& versionStr,
		const std::string& needPNG);
	void GetMapInfoFunc(const HttpRequestPtr& req,
		std::function<void(const HttpResponsePtr&)>&& callback, 
		const std::string& mapName);

	void SetTotalStatusFunc(const HttpRequestPtr& req,
		std::function<void(const HttpResponsePtr&)>&& callback);

	void CloseMap(const HttpRequestPtr& req,
		std::function<void(const HttpResponsePtr&)>&& callback);
private:
	MapEditorCtl::ptr mapEditorPtr_;
};
