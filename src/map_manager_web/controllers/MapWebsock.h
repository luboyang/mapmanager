#pragma once

#include <drogon/WebSocketController.h>
#include "mapEditorApi.h"
using namespace drogon;

class MapWebsock : public drogon::WebSocketController<MapWebsock>
{
  public:
     MapWebsock(const std::shared_ptr<MapEditorCtl>& mapPtr) :mapEditorPtr_(mapPtr), isWebsockOpen_(false) {};
     void handleNewMessage(const WebSocketConnectionPtr&,
                                  std::string &&,
                                  const WebSocketMessageType &) override;
    void handleNewConnection(const HttpRequestPtr &,
                                     const WebSocketConnectionPtr&) override;
    void handleConnectionClosed(const WebSocketConnectionPtr&) override;
    WS_PATH_LIST_BEGIN
        // list path definitions here;
        // WS_PATH_ADD("/path", "filter1", "filter2", ...);
        WS_PATH_ADD("/mapManager",Get);
    WS_PATH_LIST_END

private:
    void sendSubmapMsgs(const WebSocketConnectionPtr& conn);
    void sendLandmarkMsgs(const WebSocketConnectionPtr& conn);
    MapEditorCtl::ptr mapEditorPtr_;
    bool isWebsockOpen_;
};
