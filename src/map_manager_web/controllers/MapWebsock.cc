#include "MapWebsock.h"

inline std::string WebsockSubmapToBuffer(std::queue<WebsockSubmap> &websockDataQueue)
{
	Json::Value subsmapIndexesList;
	while (!websockDataQueue.empty())
	{
		auto websockData = websockDataQueue.front();
		Json::Value root;
		root["version"] = websockData.version;
		root["resolution"] = websockData.resolution;
		Json::Value pose;
		Json::Value position;
		position["x"] = websockData.poseInMap.translation().x();
		position["y"] = websockData.poseInMap.translation().y();
		position["z"] = websockData.poseInMap.translation().z();
		pose["position"] = position;
		std::cout << websockData.poseInMap.translation().x() << std::endl;
		std::cout << websockData.poseInMap.translation().y() << std::endl;
		std::cout << websockData.poseInMap.translation().z() << std::endl;
		Json::Value orientation;
		orientation["x"] = websockData.poseInMap.rotation().x();
		orientation["y"] = websockData.poseInMap.rotation().y();
		orientation["z"] = websockData.poseInMap.rotation().z();
		orientation["w"] = websockData.poseInMap.rotation().w();
		pose["orientation"] = orientation;
		std::cout << websockData.poseInMap.rotation().x() << std::endl;
		std::cout << websockData.poseInMap.rotation().y() << std::endl;
		std::cout << websockData.poseInMap.rotation().z() << std::endl;
		std::cout << websockData.poseInMap.rotation().w() << std::endl;
		root["pose"] = pose;
		subsmapIndexesList["subsmapIndexes"].append(websockData.smidStr);
		subsmapIndexesList["subsmapIndexes"].append(root);
		websockDataQueue.pop();
	}
	Json::FastWriter writer; 
	std::string strContent = writer.write(subsmapIndexesList);
	std::cout << strContent << std::endl;

	return strContent;
}

inline std::string WebsockLandmarkToBuffer(std::queue<WebsockLandmark>& websockDataQueue)
{
	Json::Value landmarksList;
	while (!websockDataQueue.empty())
	{

		auto websockData = websockDataQueue.front();
		Json::Value root;
		root["ns"] = websockData.ns;
		root["id"] = websockData.id;
		root["visible"] = websockData.visible;
		root["translation_weight"] = websockData.translation_weight;
		root["rotation_weight"] = websockData.rotation_weight;
		root["pole_radius"] = websockData.pole_radius;

		Json::Value pose;
		Json::Value position;
		position["x"] = websockData.tracking_from_landmark_transform.translation().x();
		position["y"] = websockData.tracking_from_landmark_transform.translation().y();
		position["z"] = websockData.tracking_from_landmark_transform.translation().z();
		pose["position"] = position;
		
		Json::Value orientation;
		orientation["x"] = websockData.tracking_from_landmark_transform.rotation().x();
		orientation["y"] = websockData.tracking_from_landmark_transform.rotation().y();
		orientation["z"] = websockData.tracking_from_landmark_transform.rotation().z();
		orientation["w"] = websockData.tracking_from_landmark_transform.rotation().w();
		pose["orientation"] = orientation;
		
		root["pose"] = pose;
		landmarksList["landmarks"].append(root);
		websockDataQueue.pop();
	}
	Json::FastWriter writer;
	std::string strContent = writer.write(landmarksList);
	std::cout << strContent << std::endl;

	return strContent;
}

void MapWebsock::handleNewMessage(const WebSocketConnectionPtr& wsConnPtr, std::string &&message, const WebSocketMessageType &type)
{
    // write your application logic here
}

void MapWebsock::handleNewConnection(const HttpRequestPtr &req, const WebSocketConnectionPtr& wsConnPtr)
{
    // write your application logic here
	isWebsockOpen_ = true;
	std::thread sendSubmap(&MapWebsock::sendSubmapMsgs, this, wsConnPtr);
	sendSubmap.detach();
}

void MapWebsock::handleConnectionClosed(const WebSocketConnectionPtr& wsConnPtr)
{
    // write your application logic here
	isWebsockOpen_ = false;
}

void MapWebsock::sendSubmapMsgs(const WebSocketConnectionPtr& conn)
{
	std::cout << "Start send submap msgs" << std::endl;
	while (isWebsockOpen_)
	{
		if (!mapEditorPtr_->websockSubmapQueue_.empty())
		{
			std::string buffer;
			//conn->send(std::to_string(arr.front()));
			{
				std::lock_guard<std::mutex> lock(mapEditorPtr_->submapMutex_);
				buffer = WebsockSubmapToBuffer(mapEditorPtr_->websockSubmapQueue_);
			}
			conn->send(buffer);
			//mapEditorPtr_->websockSubmapQueue_.pop();
			Sleep(500);
		}

		if (!mapEditorPtr_->websockLandmarkQueue_.empty())
		{
			std::string buffer;
			//conn->send(std::to_string(arr.front()));
			{
				std::lock_guard<std::mutex> lock(mapEditorPtr_->landmarkMutex_);
				buffer = WebsockLandmarkToBuffer(mapEditorPtr_->websockLandmarkQueue_);
			}
			conn->send(buffer);
			//mapEditorPtr_->websockSubmapQueue_.pop();
			Sleep(500);
		}
		else
		{
			Sleep(1000);
			continue;
		}
	}
}

void MapWebsock::sendLandmarkMsgs(const WebSocketConnectionPtr& conn)
{
	std::cout << "Start send landmark msgs" << std::endl;
	while (isWebsockOpen_)
	{
		if (!mapEditorPtr_->websockLandmarkQueue_.empty())
		{
			std::string buffer;
			//conn->send(std::to_string(arr.front()));
			{
				std::lock_guard<std::mutex> lock(mapEditorPtr_->landmarkMutex_);
				buffer = WebsockLandmarkToBuffer(mapEditorPtr_->websockLandmarkQueue_);
			}
			conn->send(buffer);
			//mapEditorPtr_->websockSubmapQueue_.pop();
			Sleep(500);
		}
		else
		{
			Sleep(1000);
			continue;
		}
	}
}
