#include <drogon/drogon.h>
#include "MapEditor.h"
#include "MapWebsock.h"
#include "Windows.h"
#include "DbgHelp.h"
int GenerateMiniDump(HANDLE hFile, PEXCEPTION_POINTERS pExceptionPointers, PWCHAR pwAppName)
{
	BOOL bOwnDumpFile = FALSE;
	HANDLE hDumpFile = hFile;
	MINIDUMP_EXCEPTION_INFORMATION ExpParam;

	typedef BOOL(WINAPI* MiniDumpWriteDumpT)(
		HANDLE,
		DWORD,
		HANDLE,
		MINIDUMP_TYPE,
		PMINIDUMP_EXCEPTION_INFORMATION,
		PMINIDUMP_USER_STREAM_INFORMATION,
		PMINIDUMP_CALLBACK_INFORMATION
		);

	MiniDumpWriteDumpT pfnMiniDumpWriteDump = NULL;
	HMODULE hDbgHelp = LoadLibrary("DbgHelp.dll");
	if (hDbgHelp)
		pfnMiniDumpWriteDump = (MiniDumpWriteDumpT)GetProcAddress(hDbgHelp, "MiniDumpWriteDump");

	if (pfnMiniDumpWriteDump)
	{
		if (hDumpFile == NULL || hDumpFile == INVALID_HANDLE_VALUE)
		{
			//TCHAR szPath[MAX_PATH] = { 0 };
			TCHAR szFileName[MAX_PATH] = { 0 };
			//TCHAR* szAppName = pwAppName;
			TCHAR* szVersion = "v1.0";
			TCHAR dwBufferSize = MAX_PATH;
			SYSTEMTIME stLocalTime;

			GetLocalTime(&stLocalTime);
			//GetTempPath(dwBufferSize, szPath);

			//wsprintf(szFileName, L"%s%s", szPath, szAppName);
			CreateDirectory(szFileName, NULL);

			wsprintf(szFileName, "%s-%04d%02d%02d-%02d%02d%02d-%ld-%ld.dmp",
				//szPath, szAppName, szVersion,
				szVersion,
				stLocalTime.wYear, stLocalTime.wMonth, stLocalTime.wDay,
				stLocalTime.wHour, stLocalTime.wMinute, stLocalTime.wSecond,
				GetCurrentProcessId(), GetCurrentThreadId());
			hDumpFile = CreateFile(szFileName, GENERIC_READ | GENERIC_WRITE,
				FILE_SHARE_WRITE | FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);

			bOwnDumpFile = TRUE;
			OutputDebugString(szFileName);
		}

		if (hDumpFile != INVALID_HANDLE_VALUE)
		{
			ExpParam.ThreadId = GetCurrentThreadId();
			ExpParam.ExceptionPointers = pExceptionPointers;
			ExpParam.ClientPointers = FALSE;

			pfnMiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(),
				hDumpFile, MiniDumpWithDataSegs, (pExceptionPointers ? &ExpParam : NULL), NULL, NULL);

			if (bOwnDumpFile)
				CloseHandle(hDumpFile);
		}
	}

	if (hDbgHelp != NULL)
		FreeLibrary(hDbgHelp);

	return EXCEPTION_EXECUTE_HANDLER;
}

LONG WINAPI ExceptionFilter(LPEXCEPTION_POINTERS lpExceptionInfo)
{
	if (IsDebuggerPresent())
	{
		return EXCEPTION_CONTINUE_SEARCH;
	}

	return GenerateMiniDump(NULL, lpExceptionInfo, L"test");
}

int main() {
	SetUnhandledExceptionFilter(ExceptionFilter);
	auto mapPtr = std::make_shared<MapEditorCtl>();
	drogon::app().registerController(std::make_shared<MapEditor>(mapPtr));
	drogon::app().registerController(std::make_shared<MapWebsock>(mapPtr));
	
	/*std::string certStr = systemutils::GetCurrentPath() + "\\server.crt";
	std::string keyStr = systemutils::GetCurrentPath() + "\\server.key";*/
	
	std::vector<std::pair<std::string, std::string>> sslCfg = {
		{"Options", "SessionTicket"},
		{"Options", "Compression"} ,
		{"VerifyClientCertifcate","none"},
		{"Protocol","ALL"}};
	/*drogon::app().setSSLFiles(certStr, keyStr);
	drogon::app().setSSLConfigCommands(sslCfg);*/
	drogon::app().registerPostHandlingAdvice(
		[](const drogon::HttpRequestPtr& req, const drogon::HttpResponsePtr& resp) {
		resp->addHeader("Access-Control-Allow-Origin", "*");
		//resp->addHeader("Access-Control-Allow-Origin", "ws://172.18.119.243:6688");
		resp->addHeader("Access-Control-Allow-Methed", "GET, POST, PUT, DELETE, OPTIONS");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		if (req->method() == drogon::HttpMethod::Options)
		{
			std::cout << "options drogon" << std::endl;
			/*resp->addHeader("Content-Type", "text/html;charset=utf-8");
			resp->addHeader("Access-Control-Allow-Headers", "*");
			resp->setStatusCode(drogon::k200OK);*/
		}
		if (req->method() == drogon::HttpMethod::Post)
		{
			std::cout << "post" << std::endl;
			resp->addHeader("Content-Type", "text/html;charset=utf-8");
		}

		});
	//drogon::app().setIdleConnectionTimeout(3600);
	//drogon::app().addListener("0.0.0.0", 6688, true, certStr, keyStr, false, sslCfg);
	drogon::app().addListener("0.0.0.0", 6688);
	drogon::app().addListener("0.0.0.0", 6689);
    // drogon::app().loadConfigFile("config.json");
    drogon::app().run();

	//RunServer(6688);
    return 0;
}