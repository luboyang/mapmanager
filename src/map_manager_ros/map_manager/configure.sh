#!/bin/bash

#---------------------------------------------
# To install dependencies
# author: yichao (sunyichao@iplusbot.com)
#---------------------------------------------

set -e

pkg_arch=$(dpkg --print-architecture)
echo "pkg_arch=${pkg_arch}"

script=$(readlink -f "$0")
script_path=$(dirname "$script")

depend_debs_path_on_ftp=(
    "/jzsoft/deploy/iplus-algo-msgs/iplus-algo-msgs_1.0.10_${pkg_arch}.deb"
    "/jzsoft/deploy/cartographer-iplus-mod/cartographer-iplus-mod_1.10.9-1_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/protobuf_3.0.0-GA-1_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/libnabo_0.0.1_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/imreg-fmt_1.1-2_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/libabsl_0.0.1_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/csm-eigen_3.0.4_${pkg_arch}.deb"
    "/jzsoft/deploy/iplus-perception/iplus-perception_0.8.3-1_${pkg_arch}.deb"
    "/jzsoft/deploy/line-detector/line-detector_0.0.3_${pkg_arch}.deb"
    "/jzsoft/deploy/jrosmm/jrosmm_2.3.1_${pkg_arch}.deb"
    "/jzsoft/third_parties/all_debs/libdraco_1.5.6_${pkg_arch}.deb"
)

sudo apt-get update
sudo apt-get install -y python-wstool python-rosdep ninja-build

# ros dependencies
sudo apt-get install -y ros-${ROS_DISTRO}-libg2o libgoogle-glog-dev liblua5.2-dev libatlas-base-dev libceres-dev
sudo apt-get install -y ros-${ROS_DISTRO}-tf2-sensor-msgs
sudo apt-get install -y ros-${ROS_DISTRO}-gps-umd ros-${ROS_DISTRO}-gps-common
# get dependencies packages form iplus git repo
# to catkin workspace root
cd $script_path/../..
wstool init src
wstool merge -t src $script_path/map_manager.rosinstall
wstool update -t src

# download the dependencies
cd $script_path
mkdir -p depend_debs
cd depend_debs

for f in ${depend_debs_path_on_ftp[@]}; do
    echo "downloading $f..."
    pftp -n << !
    open 172.19.118.28
    user ftp-jack !23456u8
    cd $(dirname $f)
    get $(basename $f)
    close
    bye
!
    if [ $? -gt 0 ]; then
        echo "we got error"
        exit -1
    fi
    echo "done."
done

for f in ${depend_debs_path_on_ftp[@]}; do
    echo "installing $(basename $f)..."
    sudo dpkg -i $(basename $f)
done
