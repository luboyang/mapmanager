#!/bin/bash

set -e
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m'
BOLD='\033[1m'

script=$(readlink -f "$0")
script_path=$(dirname "$script")

sudo apt-get install -y ros-kinetic-cv-bridge || echo ""

( cat ~/.bashrc | grep "source /opt/jz/map-manager/setup.bash --extend"  > /dev/null || echo "source /opt/jz/map-manager/setup.bash --extend" >> ~/.bashrc) || echo ""
