#!/bin/bash

# upload the target dep to ftp server

package_name=map-manager

if [ $# -lt 1 ]; then
    # echo help 
    echo "Illegal command format:"
    echo "    {script} DEP_FILE_NAME [deploy|temp|rc|release]"
    exit 0
fi

deb_file_name=$1
is_deploy=0
is_rc=0
is_release=0
is_deb_file=0
is_file_exist=0

echo $(basename $1) | grep ".deb" && is_deb_file=1
if [ -f $1 ];  then
    is_file_exist=1
fi

if [[ ${is_deb_file} == 0 || ${is_file_exist} == 0 ]]; then
    echo "file path or file name not correct"
    exit 1
fi

if [[ $2 == "deploy" ]]; then
    is_deploy=1
elif [[ $2 == "rc" ]]; then
    is_rc=1
    # cp $deb_file_name ${deb_file_name%.*}_RC.deb
    # deb_file_name=${deb_file_name%.*}_RC.deb
elif [[ $2 == "release" ]]; then
    is_release=1
fi

upload_sub_folder=${package_name}
upload_path=/jzsoft/temp/
if [ $is_deploy -gt 0 ]; then
    upload_path=/jzsoft/deploy/
elif [ $is_rc -gt 0 ]; then
    upload_path=/jzsoft/release_candidate/
elif [ $is_release -gt 0 ]; then
    upload_path=/jzsoft/deploy/
else
    exit 0
fi

echo "uploading to ftp path:" ${upload_path}${upload_sub_folder}
echo "uploading file name: " $1
pftp -n << !
open 172.19.118.28
user ftp-jack !23456u8
cd $upload_path
mkdir $upload_sub_folder
cd $upload_sub_folder
lcd $(dirname $deb_file_name)
put $(basename $deb_file_name)
put $(basename $deb_file_name .deb).md5
close
bye
!
