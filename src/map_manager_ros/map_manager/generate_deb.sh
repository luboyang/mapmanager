#!/bin/bash

# usage 
# $0 {tgt_version} {tgt_install_prefix}
# e.g., ./generate_deb.sh 0.0.1 /opt/jz

pkg_name=map-manager
working_dir=${pkg_name}

pkg_arch=$(dpkg --print-architecture)
echo "pkg_arch=${pkg_arch}"

tgt_version=$1
tgt_install_prefix=$2
tgt_files=()

if [ "${tgt_install_prefix}" == "" ]; then
    tgt_install_prefix=/opt/jz
fi

echo "0: $0"
script=$(readlink -f "$0")
script_path=$(dirname "$script")
tgt_file_path=${tgt_install_prefix}/${pkg_name}
dist_path=${script_path}/../../dist
deb_resource_path=${script_path}/deb_res
depend_pkgs=""


echo "script_path: ${script_path}"
echo "dist_path: ${dist_path}"
echo "tgt_file_path: ${tgt_file_path}"

echo "deb install prefix is ${tgt_install_prefix}"

echo " *********************************  "
echo "cleaning up previous files if existing.."
sudo dpkg -r ${pkg_name}

### backup if necessary the working dir
echo "removing ${tgt_file_path}.."
bk_idx=0
if [ -e ${tgt_file_path} ]; then
    while [ -e ${tgt_file_path}.${bk_idx} ]; do
        let bk_idx++
    done
    sudo mv ${tgt_file_path} ${tgt_file_path}.${bk_idx}
fi

echo " change directory to ${script_path}/../.."
cd ${script_path}/../..

echo " compiling target code"
catkin_make install -DCMAKE_INSTALL_PREFIX=${tgt_file_path}
if [ $? -gt 0 ]; then
    exit 1
fi

echo " change directory to ${script_path}"
cd ${script_path}
if [ "${tgt_version}" == "" ]; then
    tgt_version=$(git describe --abbrev=6 --dirty | cat)
    if [ "${tgt_version}" == "" ]; then
        tgt_version=$(cat ${script_path}/version)
        echo "No version number is provided, ,so we use value in '{proj_root}/version' as the target version: ${tgt_version}"
    fi
fi

### make the working dir
bk_idx=0
if [ -e ${dist_path}/${working_dir} ]; then
    while [ -e ${dist_path}/${working_dir}.${bk_idx} ]; do
        let bk_idx++
    done
    mv ${dist_path}/${working_dir} ${dist_path}/${working_dir}.${bk_idx}
fi

mkdir -p ${dist_path}/${working_dir}
mkdir -p ${dist_path}/${working_dir}/DEBIAN
mkdir -p ${dist_path}/${working_dir}${tgt_install_prefix}/${pkg_name}

### copy targets to deb ready dir
echo "make ${tgt_file_path} as package content"
cp -rfT ${tgt_file_path} ${dist_path}/${working_dir}${tgt_install_prefix}/${pkg_name} || ( echo "copy target files meets error" && exit 7 )

### generate resource files into deb ready dir 
escaped_tgt_install_prefix=$( echo ${tgt_install_prefix} | sed 's/\//\\\//g' )

sed "s/{PACKAGE_NAME}/${pkg_name}/" ${script_path}/deb_res/control |  \
    sed "s/{PACKAGE_ARCH}/${pkg_arch}/" | \
    sed "s/{VERSION}/${tgt_version}/" > ${dist_path}/${working_dir}/DEBIAN/control || (echo "sed error" && exit 6 )

sed "" ${script_path}/deb_res/postrm > ${dist_path}/${working_dir}/DEBIAN/postrm || ( echo "sed error" && exit 4 )

sed "s/{TGT_FILES}/${tgt_files[*]}/" ${script_path}/deb_res/postinst | \
    sed "s/{INSTALL_PREFIX}/${escaped_tgt_install_prefix}/" | \
    sed "s/{PKG_NAME}/${pkg_name}/" > ${dist_path}/${working_dir}/DEBIAN/postinst || ( echo "sed error"  && exit 3: )

sed "s/{TGT_FILES}/${tgt_files[*]}/" ${script_path}/deb_res/prerm | \
    sed "s/{INSTALL_PREFIX}/${escaped_tgt_install_prefix}/" | \
    sed "s/{PKG_NAME}/${pkg_name}/" > ${dist_path}/${working_dir}/DEBIAN/prerm || ( echo "sed error"  && exit 2 )

cp ${script_path}/tools/gen_my_jz_deps ${dist_path}/${working_dir}${tgt_install_prefix}/${pkg_name}/.
cp ${script_path}/tools/runtime_env_install.sh ${dist_path}/${working_dir}${tgt_install_prefix}/${pkg_name}/.
cp ${script_path}/changelog ${dist_path}/${working_dir}${tgt_install_prefix}/${pkg_name}/.

cd ${dist_path}/${working_dir}/DEBIAN
chmod +x postinst postrm preinst prerm || ( echo "chmode error" && exit 4 )

### start to make .deb package
cd ${dist_path}
dpkg -b ${working_dir} ${pkg_name}_${tgt_version}_${pkg_arch}.deb ||  (echo "dpkg packing error" && exit 1)
#rm -rf ${working_dir}

echo "pack deb finished"

## write the version to file
echo ${tgt_version} > ${script_path}/version
echo "Version is ${tgt_version} and it has been written into '${script_path}/version'"

## write md5
echo "writing md5..."
cd ${dist_path}
md5sum ${pkg_name}_${tgt_version}_${pkg_arch}.deb | awk '{print $1}' > ${pkg_name}_${tgt_version}_${pkg_arch}.md5

exit 0
