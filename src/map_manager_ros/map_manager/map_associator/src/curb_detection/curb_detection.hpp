#include <math.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <iostream>
#include <vector>

#include "ros/ros.h"
// #include "../../velodyne_pcl/include/velodyne_pcl/point_types.h"
#include <eigen_conversions/eigen_msg.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <tf2_ros/transform_listener.h>

#include <cmath>
#include <memory>

// using namespace pcl;
// using namespace std;
// #define CURB_INTERVAL 1
namespace velodyne_pcl
{
struct PointXYZIRR
{
  PCL_ADD_POINT4D; // quad-word XYZ
  float intensity; ///< laser intensity reading
  float rgb;
  int32_t ring;                   ///< laser ring number
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW // ensure proper alignment
} EIGEN_ALIGN16;
} // namespace velodyne_pcl

POINT_CLOUD_REGISTER_POINT_STRUCT(
    velodyne_pcl::PointXYZIRR,
    (float, x, x)(float, y, y)(float, z, z)(float, intensity, intensity)(float, rgb,
                                                                         rgb)(int32_t, ring, ring))

// 对点云升序排列

namespace curb
{
using VPoint = velodyne_pcl::PointXYZIRR;
class curbDetector
// 此类用于检测curb。输入点云，返回检测到的curb点组成的点云。主执行函数为detector。
{
  public:
  curbDetector()
  {
    int angle_limit_front_[21] = {65, 45, 47, 50, 52, 55, 60, 65, 70, 75, 100,
                                  35, 30, 33, 35, 37, 40, 42, 45, 47, 75};
    memcpy(angle_limit_front, angle_limit_front_, sizeof(angle_limit_front_));
  }

  bool comp_up_dist(const pcl::PointXYZ &a, const pcl::PointXYZ &b)
  {
    return (a.x * a.x + a.y * a.y + a.z * a.z) < (b.x * b.x + b.y * b.y + b.z * b.z);
  }

  std::vector<pcl::PointCloud<pcl::PointXYZ>> cleanPoints(pcl::PointCloud<VPoint> pc,
                                                          const ros::Time &stamp,
                                                          Eigen::Isometry3d base_to_lidar0,
                                                          Eigen::Isometry3d base_to_lidar2)
  {
    // 函数作用：
    // 1. 去除Nan点
    // 2. 根据设定的感兴趣区域取点。（包括矩形区域和环数。）
    // 3. 将取得的不同环数的点分别储存，返还。
    float z_range_down = 0.2;
    int ring_idx[22];
    int ring_idx_[22] = {1028, 1027, 1026, 1025, 1024, 1023, 1022, 1021, 1020, 1019, 1018,
                         28,   27,   26,   25,   24,   23,   22,   21,   20,   19,   18};
    memcpy(ring_idx, ring_idx_, sizeof(ring_idx_));

    size_t cloudSize = pc.size();

    pcl::PointXYZ point;
    int scanID_;

    std::vector<pcl::PointCloud<pcl::PointXYZ>> laserCloudScans(22);

    for (int i = 0; i < cloudSize; i++)
    {
      if ((pc[i].z > z_range_down))
      {
        continue;
      }

      Eigen::Vector4d pt;
      pt(0) = pc[i].x;
      pt(1) = pc[i].y;
      pt(2) = pc[i].z;
      pt(3) = 1;
      scanID_ = pc[i].ring;

      if (scanID_ >= 1000)
      {
        Eigen::Vector4d pt_in_lidar2 = base_to_lidar2 * pt;
        // if (pt_in_lidar2(1) > 0) continue;
        point.x = pt_in_lidar2(0);
        point.y = pt_in_lidar2(1);
        point.z = pt_in_lidar2(2);
      }
      else
      {
        Eigen::Vector4d pt_in_lidar0 = base_to_lidar0 * pt;
        // if (pt_in_lidar0(1) > 0) continue;
        point.x = pt_in_lidar0(0);
        point.y = pt_in_lidar0(1);
        point.z = pt_in_lidar0(2);
      }

      for (int ring_num = 0; ring_num < 22; ring_num++)
      {
        if (scanID_ == ring_idx[ring_num])
        {
          laserCloudScans[ring_num].push_back(point);
        }
      }
    }
    return laserCloudScans; // 二维vector
  }
  // 提取front_ring中17-27
  pcl::PointCloud<pcl::PointXYZ>
  ptr_front_11(const std::vector<pcl::PointCloud<pcl::PointXYZ>> input)
  {
    pc_in = input;
    pcl::PointCloud<pcl::PointXYZ> front_11;
    for (int i = 11; i < 21; i++)
    { // i < 22;
      front_11 += pc_in[i];
    }
    return front_11;
  }

  pcl::PointCloud<pcl::PointXYZ>
  ptr_back_10(const std::vector<pcl::PointCloud<pcl::PointXYZ>> input)
  {
    pc_in = input;
    pcl::PointCloud<pcl::PointXYZ> back_10;
    for (int i = 0; i < 10; i++)
    {
      back_10 += pc_in[i];
    }
    return back_10;
  }

  pcl::PointCloud<pcl::PointXYZ>
  detector_front(const std::vector<pcl::PointCloud<pcl::PointXYZ>> input)
  {
    pc_in = input;
    pcl::PointCloud<pcl::PointXYZ> piao_rou_ground;
    pcl::PointCloud<pcl::PointXYZ> piao_rou_pc_ring;

    for (
        int i = 11; i < 21;
        i++) // i < 22
             // for (int i = 0; i < 1; i++)
             // 对于每一环进行处理、检测。由于之前我们这里取了32线lidar的10线，所以这里循环次数为10.
    {
      pcl::PointCloud<pcl::PointXYZ> pointsInTheRing = pc_in[i]; // 储存此线上的点。
      // cout << i << endl;
      piao_rou_pc_ring = piao_rou(pointsInTheRing, angle_limit_front[i]);
      piao_rou_ground += piao_rou_pc_ring;
    }
    return piao_rou_ground;
  }

  pcl::PointCloud<pcl::PointXYZ>
  detector_back(const std::vector<pcl::PointCloud<pcl::PointXYZ>> input)
  {
    pc_in = input;
    pcl::PointCloud<pcl::PointXYZ> piao_rou_ground;
    pcl::PointCloud<pcl::PointXYZ> piao_rou_pc_ring;

    for (int i = 0; i < 10; i++)
    // 对于每一环进行处理、检测。由于之前我们这里取了32线lidar的10线，所以这里循环次数为10.
    {
      pcl::PointCloud<pcl::PointXYZ> pointsInTheRing = pc_in[i]; // 储存此线上的点。
      piao_rou_pc_ring = piao_rou(pointsInTheRing, angle_limit_front[i]);
      // piao_rou_pc_ring = pointsInTheRing;
      // piaorou后数据结果
      piao_rou_ground += piao_rou_pc_ring;
    }
    return piao_rou_ground;
  }

  // piaorou+自适应参数局部圆弧特征
  pcl::PointCloud<pcl::PointXYZ> piao_rou(const pcl::PointCloud<pcl::PointXYZ> hairs, int ang_lim)
  {
    const float xy_treth = 0.3; // 0.2
                                //     const float z_treth = 0.25;   // 0.2
    const float z_limit = -0.4; // -0.7
    const float k_limit = 0.06;
    const int n_N = 8; // 6
    int Theta = ang_lim;
    float angle_diff;
    float angle_diff_space;
    const int space = 6; //

    pcl::PointXYZ p0;
    pcl::PointXYZ p1;
    pcl::PointXYZ p2;
    bool jump_flag = true;

    pcl::PointCloud<pcl::PointXYZ> cleanedHairs;
    pcl::PointCloud<pcl::PointXYZ> mayDropedHair;
    //去掉后100个点
    size_t numOfTheHairs = hairs.size();
    // cout << endl;
    std::vector<float> ang_l;
    std::vector<float> ang_r;
    std::vector<float> angle_diff_v;

    for (int i = 1; i < numOfTheHairs; i++)
    {
      p0 = hairs[i - 1];
      p1 = hairs[i];

      if (p0.y == p1.y)
      {
        continue;
      }
      // cout << p1.y << " ";
      float p_dist;
      float z_high;
      float x_diff;
      float y_diff;
      float k;

      // v1_v2_angle left angle
      float v1_x;
      float v1_y;
      float v2_x;
      float v2_y;
      float v1_abs;
      float v2_abs;
      float v1_v2_dot;
      float v1_v2_cos;
      float v1_v2_angle;

      // v3_v4_angle right angle
      float v3_x;
      float v3_y;
      float v4_x;
      float v4_y;
      float v3_abs;
      float v4_abs;
      float v3_v4_dot;
      float v3_v4_cos;
      float v3_v4_angle;

      v1_x = 0 - p0.x;
      v1_y = 0 - p0.y;
      v2_x = p1.x - p0.x;
      v2_y = p1.y - p0.y;
      v1_abs = sqrt(pow((v1_x), 2) + pow((v1_y), 2));
      v2_abs = sqrt(pow((v2_x), 2) + pow((v2_y), 2));
      v1_v2_dot = v1_x * v2_x + v1_y * v2_y;
      v1_v2_cos = v1_v2_dot / (v1_abs * v2_abs);
      v1_v2_angle = fabs(acos(v1_v2_cos) * 180 / M_PI);

      v3_x = 0 - p1.x;
      v3_y = 0 - p1.y;
      v4_x = p0.x - p1.x;
      v4_y = p0.y - p1.y;
      v3_abs = sqrt(pow((v3_x), 2) + pow((v3_y), 2));
      v4_abs = sqrt(pow((v4_x), 2) + pow((v4_y), 2));
      v3_v4_dot = v3_x * v4_x + v3_y * v4_y;
      v3_v4_cos = v3_v4_dot / (v3_abs * v4_abs);
      v3_v4_angle = fabs(acos(v3_v4_cos) * 180 / M_PI);

      angle_diff = fabs(v1_v2_angle - v3_v4_angle);

      p_dist = sqrt(((p0.x - p1.x) * (p0.x - p1.x)) + ((p0.y - p1.y) * (p0.y - p1.y)));
      z_high = fabs(p0.z - p1.z);
      x_diff = fabs(p0.x - p1.x);
      y_diff = fabs(p0.y - p1.y);
      k = z_high / p_dist;

      if (i >= space)
      {
        p2 = hairs[i - space];

        // v5_v6_angle left angle
        float v5_x;
        float v5_y;
        float v6_x;
        float v6_y;
        float v5_abs;
        float v6_abs;
        float v5_v6_dot;
        float v5_v6_cos;
        float v5_v6_angle;

        // v7_v8_angle right angle
        float v7_x;
        float v7_y;
        float v8_x;
        float v8_y;
        float v7_abs;
        float v8_abs;
        float v7_v8_dot;
        float v7_v8_cos;
        float v7_v8_angle;

        v5_x = 0 - p2.x;
        v5_y = 0 - p2.y;
        v6_x = p1.x - p2.x;
        v6_y = p1.y - p2.y;
        v5_abs = sqrt(pow((v5_x), 2) + pow((v5_y), 2));
        v6_abs = sqrt(pow((v6_x), 2) + pow((v6_y), 2));
        v5_v6_dot = v5_x * v6_x + v5_y * v6_y;
        v5_v6_cos = v5_v6_dot / (v5_abs * v6_abs);
        v5_v6_angle = fabs(acos(v5_v6_cos) * 180 / M_PI);

        v7_x = 0 - p1.x;
        v7_y = 0 - p1.y;
        v8_x = p2.x - p1.x;
        v8_y = p2.y - p1.y;
        v7_abs = sqrt(pow((v7_x), 2) + pow((v7_y), 2));
        v8_abs = sqrt(pow((v8_x), 2) + pow((v8_y), 2));
        v7_v8_dot = v7_x * v8_x + v7_y * v8_y;
        v7_v8_cos = v7_v8_dot / (v7_abs * v8_abs);
        v7_v8_angle = fabs(acos(v7_v8_cos) * 180 / M_PI);

        angle_diff_space = fabs(v5_v6_angle - v7_v8_angle);
      }

      if ((p_dist <= xy_treth) && (k >= k_limit) && (0 < z_high) && (p1.z < z_limit) &&
          (angle_diff_space > Theta) && (angle_diff > Theta))
      {
        if (jump_flag)
        {
          mayDropedHair.push_back(p1);
          if (mayDropedHair.size() >= n_N)
          {
            cleanedHairs = cleanedHairs + mayDropedHair;
            jump_flag = false;
            mayDropedHair.clear();
          }
        }
        else
        {
          cleanedHairs.push_back(p1);
        }
      }
      else
      {
        jump_flag = true;
        mayDropedHair.clear();
        mayDropedHair.push_back(p1);
      }
    }
    return cleanedHairs;
  }

  pcl::PointCloud<pcl::PointXYZ> curb_detector(pcl::PointCloud<VPoint> pc, const ros::Time &stamp,
                                               Eigen::Isometry3d base_to_lidar0,
                                               Eigen::Isometry3d base_to_lidar2)
  {
    std::vector<pcl::PointCloud<pcl::PointXYZ>> cloud_cleaned;
    cloud_cleaned = cleanPoints(pc, stamp, base_to_lidar0, base_to_lidar2);

    // 前11个环 lidar0
    pcl::PointCloud<pcl::PointXYZ> front_11;
    front_11 = ptr_front_11(cloud_cleaned);

    // 后11个还 lidar2
    pcl::PointCloud<pcl::PointXYZ> back_10;
    back_10 = ptr_back_10(cloud_cleaned);

    pcl::PointCloud<pcl::PointXYZ> cloud_out_front;
    pcl::PointCloud<pcl::PointXYZ> cloud_out_back;
    pcl::PointCloud<pcl::PointXYZ> cloud_out_base_l;
    pcl::PointCloud<pcl::PointXYZ> cloud_out_base_r;
    pcl::PointCloud<pcl::PointXYZ> cloud_out;

    pcl::PointXYZ point;
    // 执行检测。
    cloud_out_front = detector_front(cloud_cleaned);
    for (int i = 0; i < cloud_out_front.size(); i++)
    {
      Eigen::Vector4d pt;
      pt(0) = cloud_out_front[i].x;
      pt(1) = cloud_out_front[i].y;
      pt(2) = cloud_out_front[i].z;
      pt(3) = 1;
      Eigen::Vector4d pt_in_base = base_to_lidar0.inverse() * pt;
      point.x = pt_in_base(0);
      point.y = pt_in_base(1);
      point.z = pt_in_base(2);
      if (pt_in_base(1) < 0)
      {
        cloud_out_base_r.push_back(point);
      }
      else
      {
        cloud_out_base_l.push_back(point);
      }
    }

    cloud_out_back = detector_back(cloud_cleaned);
    for (int i = 0; i < cloud_out_back.size(); i++)
    {
      Eigen::Vector4d pt;
      pt(0) = cloud_out_back[i].x;
      pt(1) = cloud_out_back[i].y;
      pt(2) = cloud_out_back[i].z;
      pt(3) = 1;
      Eigen::Vector4d pt_in_base = base_to_lidar2.inverse() * pt;
      point.x = pt_in_base(0);
      point.y = pt_in_base(1);
      point.z = pt_in_base(2);
      if (pt_in_base(1) < 0)
      {
        cloud_out_base_r.push_back(point);
      }
      else
      {
        cloud_out_base_l.push_back(point);
      }
    }
    cloud_out = cloud_out_base_r + cloud_out_base_l;

    return cloud_out;
  }

  private:
  std::vector<pcl::PointCloud<pcl::PointXYZ>> pc_in;
  pcl::PointCloud<pcl::PointXYZ> front_11;
  int angle_limit_front[22];
};
} // namespace curb

/*
class rosTransPoints
{
public:
  bool lookupTransform(const std::string& target_frame,
                       const std::string& source_frame,
                       const ros::Time& time,
                       const ros::Duration timeout,
                       Eigen::Isometry3d& transform)
  {
    while (1)
    {
      geometry_msgs::TransformStamped transform_stamped;
      try
      {
        transform_stamped =
        tfBuffer_.lookupTransform(target_frame, source_frame, time);
      }
      catch (tf2::TransformException& ex)
      {
        if (ros::Time::now() - time > timeout)
        {
          std::cerr << "Look TF failed: " << ex.what();
          return false;
        }

        usleep(1000);
        continue;
      }
      tf::transformMsgToEigen(transform_stamped.transform, transform);
      break;
    }
    return true;
  }

  rosTransPoints() : tfBuffer_(ros::Duration(60.)), tfListener_(tfBuffer_)
  {
    // 定义接受和发布。
    pub_front =
nh.advertise<sensor_msgs::PointCloud2>("/curb_detection_result_front", 1);
    sub_front =
nh.subscribe("/cartographer_ros/merge_point_cloud",3,&rosTransPoints::callback,this);
  }

  void callback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& input)
  {
    // 回调函数。每次获得点云都会自动执行的函数。
    clock_t startTime,endTime;
    startTime = ros::Time::now().toNSec();
    curbDetector cd;

    pcl::fromROSMsg(*input, cloud_vpoint);

    // 坐标变换
    if (!lookupTransform(lidar1_frame_, base_frame_, input->header.stamp,
ros::Duration(1), base_to_lidar0))
    {
      cerr << "Look TF failed, base_footprint_to_lidar_link, RETURN!" << endl;
      return;
    }

    if (!lookupTransform(lidar2_frame_, base_frame_, input->header.stamp,
ros::Duration(1), base_to_lidar2))
    {
      cerr << "Look TF failed, base_footprint_to_lidar_link, RETURN!" << endl;
      return;
    }

    // 路沿检测
    pcl::PointCloud<pcl::PointXYZ> cloud_out;
    cloud_out = cd.curb_detector(cloud_vpoint, input->header.stamp,
base_to_lidar0, base_to_lidar2);

    pcl::toROSMsg(cloud_out, cloud_final);
    cloud_final.header.stamp = input->header.stamp;
    cloud_final.header.frame_id = input->header.frame_id;
    pub_front.publish(cloud_final);

    endTime = ros::Time::now().toNSec();
    cout << "The run time is:" << (double)(endTime - startTime) / 10e6 << "ms"
<< endl;

  }


private:
  ros::NodeHandle nh;
  ros::Publisher pub_front;
  ros::Subscriber sub_front;
  pcl::PointCloud<VPoint> cloud_vpoint;
  sensor_msgs::PointCloud2 cloud_final;
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  std::string lidar1_frame_ = "lidar_link";
  std::string lidar2_frame_ = "lidar_link2";
  std::string base_frame_ = "base_footprint";

  Eigen::Isometry3d base_to_lidar2 = Eigen::Isometry3d::Identity();
  Eigen::Isometry3d base_to_lidar0 = Eigen::Isometry3d::Identity();
};*/

/*
int main(int argc, char **argv)
{
  ros::init(argc, argv, "curb_detector");
  rosTransPoints start_detec;
  ros::spin();
}*/
