/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mark_localization.h"

#include "std_srvs/SetBool.h"
using namespace std;

namespace map_associator
{
MarkLocalization::MarkLocalization() : MarkInterface(1), is_cam_subscribed_(false)
{
  string map_name;
  if (!::ros::param::get("/vtr/path", map_name))
  {
    LOG(ERROR) << ("Please set map name! ");
    map_name = "test";
    //  		exit(0);
  }
  string map_folder = map_name + "/landmark.xml";
  LOG(INFO) << "path : " << map_folder;

  laser_.reset(new map_associator::lasermark::LasermarkLocalization(nh_));
  // TODO: distingush uwb with other marks
  getPoseInMap(map_folder, "Uwb", laser_->landmark_pose_list_);
  if (laser_->landmark_pose_list_.size() > 0) laser_->setHasUwb(true);
  getPoseInMap(map_folder, "Lasermarks", laser_->landmark_pose_list_);

  bool is_3d = false;
  if (!ros::param::get("~is_3d", is_3d))
  {
    LOG(WARNING) << ("Can not get param of ~is_3d!");
    is_3d = false;
  }

  // visual mark
  bool use_tag = false;
  int towards = 0;
  ros::param::get("/cartographer_node/cam_towards", towards);
  string topic_name_param = "/algo/property/cam_1_bottom_docking_topic";
  if (!ros::param::get("/cartographer_node/use_visualmarks", use_tag))
  {
    LOG(WARNING) << ("LaserMarkNode: can not get param of useTag!");
  }

  if (use_tag)
  {
    is_cam_subscribed_ = true;
    LOG(INFO) << "lasermark_node/start using Visualmarks!";
  }

  // laser mark
  is_lasermark_subscribed_ = false;
  if (!ros::param::get("/cartographer_node/use_lasermarks", is_lasermark_subscribed_))
  {
    LOG(WARNING) << ("LaserMarkNode: can not get param of useLasermark!");
  }
  if (is_lasermark_subscribed_)
  {
    laser_->setParam();
    laser_->setUseLasermark(true);
    //     string laser_scan_topic = "/scan_matched_points2";
    //     if (is_3d)
    //       laser_scan_topic = "/cartographer_ros/lidar_mark_point_cloud";
    string laser_scan_topic = "/cartographer_ros/lidar_mark_point_cloud";
    if (is_3d && laser_->GetUsePoleMarks())
      laser_scan_topic = "/cartographer_ros/merge_point_cloud";
    ::ros::param::set("/mapping_associator/lasermark_points_topic", laser_scan_topic);
    scan_sub_ = nh_.subscribe("/sensor_detector/lasermarks", 3,
                              &MarkLocalization::HandleLasermarksPoints, this);

    LOG(INFO) << "laser_scan_topic: " << laser_scan_topic
              << "\n lasermark_node/start using Lasermarks!";
  }

  // gps
  bool use_gps = false;
  if (!ros::param::get("/cartographer_node/use_gps", use_gps))
  {
    LOG(WARNING) << ("LaserMarkNode: can not get param of use_gps!");
  }
  gps_.reset(new map_associator::gps::GpsLocalization(nh_, map_name));
  gps_topic_ = "/jzhw/gps/fix";
  if (!ros::param::get("/jzhw/gps/default/property/gps_fix", gps_topic_))
  {
    LOG(WARNING) << "LaserMarkNode: can not get param of gps_topic,use default:" << gps_topic_;
  }
  is_gps_subscribed_ = false;
  gps_->setParam();
  if (use_gps && is_3d)
  {
    is_gps_subscribed_ = true;
    gps_sub_ = nh_.subscribe<gps_common::GPSFix>(gps_topic_, 3, &MarkLocalization::HandleGps, this);
    LOG(INFO) << "lasermark_node/start using gps localization!";
  }
  laser_->setBoolGpsInfo(gps_->hasGpsInfo());

  // uwb mark
  bool use_uwb = false;
  ::ros::param::get("/cartographer_node/use_uwb", use_uwb);
  if (use_uwb)
  {
    laser_->setUseUwb(use_uwb);
  }

  bool use_curb = false;
  if (is_3d)
  {
    ::ros::param::get("/cartographer_node/use_curb", use_curb);
    string cloud_topic = "/cartographer_ros/merge_point_cloud";
    string pose_topic = "/cartographer_ros/scan_pose";
    cloud1_sub_.subscribe(nh_, cloud_topic, 3);
    pose2_sub_.subscribe(nh_, pose_topic, 3);
    cloudPoseSync policy_cloud_pose(15);
    cloud_pose_sync_.reset(new message_filters::Synchronizer<cloudPoseSync>(
        cloudPoseSync(policy_cloud_pose), cloud1_sub_, pose2_sub_));
    cloud_pose_sync_->registerCallback(
        boost::bind(&MarkLocalization::handleMergeCloudPoints, this, _1, _2));
    cloud1_sub_.unsubscribe();
    pose2_sub_.unsubscribe();
    is_curb_subscribed_ = false;
  }
  if (use_curb)
  {
    laser_->setUseCurb(use_curb);
    cloud1_sub_.subscribe();
    pose2_sub_.subscribe();
    is_curb_subscribed_ = true;
    LOG(INFO) << "lasermark_node/start using Curb!";
  }

  visual_points_sub_ = nh_.subscribe("/vertical_line_landmark", 3,
                                     &MarkLocalization::handleVerticalLineLandmark, this);

  //   LandMark_pub_ =
  //   nh_.advertise<cartographer_ros_msgs::LandmarkList>("/visual_landmarks",
  //   3);
  servers_.push_back(nh_.advertiseService("/mark_localization/set_mark_switch",
                                          &MarkLocalization::setMarkSwitch, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/reload_free_space_image",
                                          &MarkLocalization::reloadFreeSpaceImageSrv, this));
  servers_.push_back(nh_.advertiseService("/mark_localization/set_location_lasermarks",
                                          &MarkLocalization::setLocationLasermarksID, this));
  marker_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/mapping_markers", 1);
  gps_pose_pub_ = nh_.advertise<vtr_msgs::GlobalLocalizationPose>("/vtr/global_localization", 1);
  landmark_detect_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("/landmark_poses_list", 1);
  precise_localization_client_ =
      nh_.serviceClient<std_srvs::SetBool>("/cartographer_node/precise_localization");
  believe_in_lidar_localization_client_ =
      nh_.serviceClient<std_srvs::SetBool>("/vtr/believe_in_lidar_localiation_server");
  last_trackings_reset_client_ =
      nh_.serviceClient<std_srvs::SetBool>("/vtr/last_trackings_reset_server");
  precise_localization_num_range_data_ = 1;
  if (!ros::param::get("~precise_localization_num_range_data",
                       precise_localization_num_range_data_))
  {
    LOG(WARNING) << ("LaserMarkNode: can not get param of useLasermark!");
  }
  use_lidar_ = true;
  gps_origin_flag_ = false;
  last_gps_quality_ = false;
  is_strip_detect_subscribed_ = false;
  is_uwb_subscribed_ = false;
  receive_tags_ = true;
  strip_detect_.reset(new StripDetect(nh_));
}

MarkLocalization::~MarkLocalization() {}

void MarkLocalization::HandleImage(const sensor_msgs::ImageConstPtr &img_msg)
{
  //   cartographer_ros_msgs::LandmarkList landmark_list;
  //   cam_->imgCb(img_msg, landmark_list);
  //   mutex_.lock();
  //   if(landmark_list.landmarks.size()!=0)
  //     LandMark_pub_.publish(landmark_list);
  //
  //   mutex_.unlock();
}

void MarkLocalization::HandleLasermarksPoints(
    const cartographer_ros_msgs::LandmarkListConstPtr &msg)
{
  // 	LOG(ERROR) << "here coming a pointcloud2";
  cartographer_ros_msgs::LandmarkList landmark_list;
  laser_->handleScanMatchedPoints(msg, landmark_list);
  std::unique_lock<std::mutex> lk(mutex_);
  LandMark_pub_.publish(landmark_list);
  visualization_msgs::MarkerArray landmark_find;
  for(const cartographer_ros_msgs::LandmarkEntry mark:landmark_list.landmarks)
  {
    visualization_msgs::Marker landmark;
    landmark.id = std::stoi(mark.id);
    if (landmark.id >= 5000000)
      continue;
    else if (landmark.id >= 1000000)
    {
      landmark.id = landmark.id - 1000000;
      landmark.ns = "Lasermarks";
    }
    else if (landmark.id >= 44032)
      landmark.ns = "Visualmarks_4x4";
    else
      landmark.ns = "Visualmarks";
    landmark_find.markers.push_back(landmark);
  }
  landmark_detect_pub_.publish(landmark_find);
  marker_pub_.publish(markers_);
}

void MarkLocalization::handleMergeCloudPoints(const sensor_msgs::PointCloud2ConstPtr &cloud_msg,
                                              const geometry_msgs::PoseStampedConstPtr &pose_msg)
{
  cartographer_ros_msgs::LandmarkList landmark_list;
  laser_->handleMergeCloudPoints(cloud_msg, pose_msg, landmark_list);
  std::unique_lock<std::mutex> lk(mutex_);
  LandMark_pub_.publish(landmark_list);
  marker_pub_.publish(markers_);
}

void MarkLocalization::HandleGps(const gps_common::GPSFix::ConstPtr &gps_msg)
{
  vtr_msgs::GlobalLocalizationPose pose_temp;
  std::unique_lock<std::mutex> lk(mutex_);
  // 处理gps消息判定是否可用，并确定置信度信息
  if (gps_->handleGps(gps_msg, pose_temp))
  {
    bool cur_gps_quality;
    // 若uwb 有效，则不适用普通gps定位
    if (laser_->useUwb() && !gps_->useGpsManual() && laser_->unValidUwbSize() < 5)
    {
      return;
    }
    if (pose_temp.sensor2reference.covariance[0] < 11 &&
        pose_temp.sensor2reference.covariance[35] < 11)
      cur_gps_quality = true;
    else
      cur_gps_quality = false;
    // 若当前gps置信度与上次置信度不一致，则call 定位节点服务更改gps置信度
    if (cur_gps_quality != last_gps_quality_)
    {
      callBelieveInLidarLocalizationSrv(!cur_gps_quality);
    }
    if (pose_temp.localization_type != "rtk_IGNORE")
    {
      if (gps_->useGpsManual())
        pose_temp.localization_type = "rtk_MANUAL";
      else
      {
        if (cur_gps_quality)
          pose_temp.localization_type = "rtk_NORMAL";
        else
          pose_temp.localization_type = "rtk_WEAK";
      }
    }
    gps_pose_pub_.publish(pose_temp);
  }
}
void MarkLocalization::handleVerticalLineLandmark(
    const cartographer_ros_msgs::LandmarkInDifferentCoordinates::ConstPtr &msg)
{
  cartographer_ros_msgs::LandmarkList landmark_list;
  laser_->handleVerticalLineLandmark(msg, landmark_list);
  std::unique_lock<std::mutex> lk(mutex_);
  if (landmark_list.landmarks.size() != 0) LandMark_pub_.publish(landmark_list);
  marker_pub_.publish(markers_);
}

void MarkLocalization::HandleStripImage(const sensor_msgs::ImageConstPtr &msg)
{
  try
  {
    ros::Duration tmp = msg->header.stamp - last_strip_image_time_;
    if (tmp.toSec() < intervel_between_strip_images_) return;
  }
  catch (const std::exception &e)
  {
    LOG(WARNING) << e.what();
  }
  LOG(INFO) << "Handle strip image!";
  sensor_msgs::PointCloud points;
  try
  {
    // 从图像中获取条带点云信息
    strip_detect_->HandleImage(msg, points);
    // 加入到用于定位的消息中
    if (points.points.size() > 0) laser_->handleStripDetectPoints(points);
  }
  catch (const std::exception &e)
  {
    LOG(WARNING) << e.what();
  }
  last_strip_image_time_ = msg->header.stamp;
}

bool MarkLocalization::setMarkSwitch(::cartographer_ros_msgs::SetSwitch::Request &request,
                                     ::cartographer_ros_msgs::SetSwitch::Response &response)
{
  std::unique_lock<std::mutex> lk(mutex_);
  LOG(WARNING) << "Mark switch type:" << request.type;
  cartographer_ros_msgs::SetSwitch srv;
  srv.request = request;
  detect_client_.call(srv);
  try
  {
    if (request.type == "Lasermarks")
    {
      // 设置是否使用反射板定位
      laser_->setUseLasermark(request.flag);
      if (request.flag)
      {
        if (!is_lasermark_subscribed_)
        {
          is_lasermark_subscribed_ = true;
          laser_->setParam();

          bool is_3d = false;
          if (!ros::param::get("~is_3d", is_3d))
          {
            LOG(WARNING) << ("Can not get param of ~is_3d!");
            is_3d = false;
          }
          string laser_scan_topic = "/cartographer_ros/lidar_mark_point_cloud";
          // not use now
          if (is_3d && laser_->GetUsePoleMarks())
            laser_scan_topic = "/cartographer_ros/merge_point_cloud";

          ::ros::param::set("/mapping_associator/lasermark_points_topic", laser_scan_topic);
          scan_sub_ = nh_.subscribe("/sensor_detector/lasermarks", 3,
                                    &MarkLocalization::HandleLasermarksPoints, this);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "laser_scan_topic: " << laser_scan_topic
                    << "\n lasermark_node: start using Lasermarks!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Lasermarks!";
      }
      else
      {
        if (is_lasermark_subscribed_)
        {
          is_lasermark_subscribed_ = false;
          scan_sub_.shutdown();
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close Lasermarks!";
          if (!is_lasermark_subscribed_ && !use_lidar_)
          {
            std_srvs::SetBool srv;
            srv.request.data = true;
            if (last_trackings_reset_client_.call(srv))
            {
              LOG(INFO) << "Call  last_trackings_reset_server success!";
            }
            else
              LOG(WARNING) << "Call last_trackings_reset_server failed!";
          }
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Lasermarks!";
      }
    }
    else if (request.type == "Visualmarks")
    {
      if (request.flag)
      {
        if (!is_cam_subscribed_)
        {
          is_cam_subscribed_ = true;
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: start using Visualmarks!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Visualmarks!";
      }
      else
      {
        if (is_cam_subscribed_)
        {
          is_cam_subscribed_ = false;
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close Visualmarks!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Visualmarks!";
      }
    }
    else if (request.type == "LaserScanOdom")
    {
      laser_->setUseScanLocaliser(request.flag);
      use_lidar_ = request.flag;
      if (is_gps_subscribed_) gps_->setUseLidar(use_lidar_);
      std::string switch_msg = request.flag ? "open" : "close";

      if (!is_lasermark_subscribed_ && !use_lidar_)
      {
        std_srvs::SetBool srv;
        srv.request.data = true;
        if (last_trackings_reset_client_.call(srv))
        {
          LOG(INFO) << "Call last_trackings_reset_server success!";
        }
        else
          LOG(WARNING) << "Call last_trackings_reset_server failed!";
      }

      LOG(INFO) << "[" << ros::Time::now() << "] "
                << "lasermark_node/" << switch_msg << " LaserScanLocaliser!";
    }
    else if (request.type == "gps")
    {
      if (request.flag)
      {
        if (!is_gps_subscribed_)
        {
          is_gps_subscribed_ = true;
          gps_sub_ = nh_.subscribe(gps_topic_, 3, &MarkLocalization::HandleGps, this);
          gps_->setUseLidar(use_lidar_);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: start using gps localization!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call GPS!";
      }
      else
      {
        if (is_gps_subscribed_)
        {
          is_gps_subscribed_ = false;
          gps_sub_.shutdown();
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close gps localization!";
          callBelieveInLidarLocalizationSrv(true);
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call GPS!";
      }
    }
    else if (request.type == "gps_manual")
    {
      if (!is_gps_subscribed_)
      {
        if (request.flag)
        {
          gps_sub_ = nh_.subscribe(gps_topic_, 3, &MarkLocalization::HandleGps, this);
          gps_->setUseLidar(use_lidar_);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: start using gps localization!";
        }
        else
        {
          gps_sub_.shutdown();
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close gps localization!";
          callBelieveInLidarLocalizationSrv(true);
        }
      }
      gps_->setUseGpsManual(request.flag);
    }
    else if (request.type == "PreciseLocalization")
    {
      laser_->setUsePreciseLocalization(request.flag);
      std_srvs::SetBool srv_temp;
      if (request.flag)
        srv_temp.request.data = precise_localization_num_range_data_;
      else
        srv_temp.request.data = 0;
      if (!precise_localization_client_.call(srv_temp))
        LOG(WARNING) << "Call /cartographer_node/precise_localization failed!";

      std::string switch_msg = request.flag ? "open" : "close";
      LOG(INFO) << "[" << ros::Time::now() << "] "
                << "lasermark_node/" << switch_msg << " PreciseLocalization!";
    }
    else if (request.type == "StripLocalization")
    {
      laser_->setUseStripDetect(request.flag);
      if (request.flag)
      {
        if (!is_strip_detect_subscribed_)
        {
          is_strip_detect_subscribed_ = true;
          string strip_camera_topic = "/front/color/lower_frq_image_raw";
          ::ros::param::get("~front_color_image_topic", strip_camera_topic);
          strip_detect_sub_ =
              nh_.subscribe(strip_camera_topic, 1, &MarkLocalization::HandleStripImage, this);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: start using strip localization!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call StripDetect!";
      }
      else
      {
        if (is_strip_detect_subscribed_)
        {
          //         strip_detect_->StripSwitch(false, false);
          is_strip_detect_subscribed_ = false;
          laser_->clearStripPoints();
          LOG(INFO) << "strip start shutdown!";
          strip_detect_sub_.shutdown();
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close strip localization!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call StripDetect!";
      }
    }
    else if (request.type == "Uwb")
    {
      if (request.flag)
      {
        if (!is_uwb_subscribed_)
        {
          laser_->setUseUwb(true);
          is_uwb_subscribed_ = true;
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: start using Uwb!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Uwb!";
      }
      else
      {
        if (is_uwb_subscribed_)
        {
          is_uwb_subscribed_ = false;
          laser_->setUseUwb(false);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close Uwb!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Uwb!";
      }
    }
    else if (request.type == "Curb")
    {
      if (request.flag)
      {
        if (!is_curb_subscribed_)
        {
          is_curb_subscribed_ = true;
          laser_->setUseCurb(true);
          cloud1_sub_.subscribe();
          pose2_sub_.subscribe();
          //         merge_cloud_sub_ = nh_.subscribe(laser_scan_topic, 1,
          //         &MarkLocalization::handleMergeCloudPoints, this);
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "\n lasermark_node: start using Curb!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Curb!";
      }
      else
      {
        if (is_curb_subscribed_)
        {
          laser_->setUseCurb(false);
          is_curb_subscribed_ = false;
          cloud1_sub_.unsubscribe();
          pose2_sub_.unsubscribe();
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "lasermark_node: close Curb!";
        }
        else
          LOG(INFO) << "[" << ros::Time::now() << "] "
                    << "Repeat call Curb!";
      }
    }
    else
    {
      response.error_message = "request.type is wrong! ";
      return false;
    }
    response.error_message = "";
  }
  catch (const std::exception &e)
  {
    LOG(WARNING) << e.what();
  }
  return true;
}

void MarkLocalization::callBelieveInLidarLocalizationSrv(
    const bool &believe_in_lidar_localization_flag)
{
  last_gps_quality_ = !believe_in_lidar_localization_flag;
  std_srvs::SetBool srv_temp;
  srv_temp.request.data = believe_in_lidar_localization_flag;
  believe_in_lidar_localization_client_.call(srv_temp);
  std::string switch_msg = believe_in_lidar_localization_flag ? "believe" : "not believe";
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "lasermark_node:" << switch_msg << " lidar localization!";
}

bool MarkLocalization::reloadFreeSpaceImageSrv(std_srvs::SetBool::Request &request,
                                               std_srvs::SetBool::Response &response)
{
  laser_->reloadFreeSpaceImage();
  return true;
}

bool MarkLocalization::setLocationLasermarksID(emma_tools_msgs::SetIDs::Request &request,
                                               emma_tools_msgs::SetIDs::Response &response)
{
  std::map<int, int> lasermark_ids;
  for (auto it : request.ids)
    lasermark_ids[it] = 1;
  laser_->setLocationLaermarksID(lasermark_ids);
  response.success = true;
  return true;
}

void MarkLocalization::clear()
{
  for (auto &srv : servers_)
    srv.shutdown();

  laser_->clear();
  gps_->clear();
  img_sub_.shutdown();
  scan_sub_.shutdown();
  gps_sub_.shutdown();
  strip_detect_sub_.shutdown();
  precise_localization_client_.shutdown();
  believe_in_lidar_localization_client_.shutdown();
  LandMark_pub_.shutdown();
  marker_pub_.shutdown();
  gps_pose_pub_.shutdown();
}
} // namespace map_associator
// bool MarkLocalization::setScanLocaliserSwitch(vtr_msgs::SetParams::Request&
// request,
// vtr_msgs::SetParams::Response& response)
// {
// 	laser_->use_scan_localiser_ = request.laser_localization_enabled ;
// 	return true;
// }
