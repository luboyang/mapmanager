/*
 * @Author: Cyy && cyycyag@163.com
 * @Date: 2023-03-06 16:37:55
 * @LastEditors: Cyy && cyycyag@163.com
 * @LastEditTime: 2023-03-10 11:26:03
 * @FilePath: /map_manager/map_associator/src/mark_localization.h
 * @Description: 定位主要接口
 */


#ifndef LANDMARKLOCALIZATION_H
#define LANDMARKLOCALIZATION_H
// #include "visualmark/apriltag_detect.h"
// #include "visualmark/groundtag_perception.h"

#include <glog/logging.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <std_srvs/SetBool.h>
#include <vtr_msgs/SetInitialPose.h>
#include <vtr_msgs/SetParams.h>

#include "gps/gps_localization.h"
#include "lasermark/lasermark_localization.h"
#include "mark_interface.h"

// #include <visualization_msgs/MarkerArray.h>
namespace map_associator
{
class MarkLocalization : public MarkInterface
{
  public:
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2,
                                                          geometry_msgs::PoseStamped>
      cloudPoseSync;
  MarkLocalization();
  ~MarkLocalization();

  private:
  void HandleImage(const sensor_msgs::ImageConstPtr &img_msg);

  /**
   * @description: 将检测到的反射板进行数据数据关联
   * @param {LandmarkListConstPtr} &msg 反射板消息
   * @return {*}
   */
  void HandleLasermarksPoints(const cartographer_ros_msgs::LandmarkListConstPtr &msg);

  /**
   * @description: 
   * @param {PointCloud2ConstPtr} &cloud_msg
   * @param {PoseStampedConstPtr} &pose_msg
   * @return {*}
   */
  void handleMergeCloudPoints(const sensor_msgs::PointCloud2ConstPtr &cloud_msg,
                              const geometry_msgs::PoseStampedConstPtr &pose_msg);

  /**
   * @description: 处理gps消息
   * @param {ConstPtr} &gps_msg gps msg
   * @return {*}
   */
  void HandleGps(const gps_common::GPSFix::ConstPtr &gps_msg);

  /**
   * @description: 处理垂直线
   * @param {ConstPtr} &msg 垂直线topic
   * @return {*}
   */
  void handleVerticalLineLandmark(
      const cartographer_ros_msgs::LandmarkInDifferentCoordinates::ConstPtr &msg);

  /**
   * @description: 处理条带图像
   * @param {ImageConstPtr} &msg 图像信息
   * @return {*}
   */
  void HandleStripImage(const sensor_msgs::ImageConstPtr &msg);

  /**
   * @description: 更换定位源
   * @param {Request} &request 服务request 定位源名称及是否开启
   * @param {Response} &response 服务response 
   * @return {*}
   */
  bool setMarkSwitch(::cartographer_ros_msgs::SetSwitch::Request &request,
                     ::cartographer_ros_msgs::SetSwitch::Response &response);

  /**
   * @description: 重加载动态物体框选区域
   * @param {Request} &request
   * @param {  } std_srvs
   * @return {*}
   */
  bool reloadFreeSpaceImageSrv(std_srvs::SetBool::Request &request,
                               std_srvs::SetBool::Response &response);

  /**
   * @description: 通知定位节点当前是否信任激光定位
   * @param {bool} &believe_in_lidar_localization_flag true-信赖，false-不信赖
   * @return {*}
   */
  void callBelieveInLidarLocalizationSrv(const bool &believe_in_lidar_localization_flag);
  
  /**
   * @description: 设置当前路径中可识别的反射板id
   * @param {Request} &request 反射板ids
   * @param {Response} &response
   * @return {*}
   */
  bool setLocationLasermarksID(emma_tools_msgs::SetIDs::Request &request,
                               emma_tools_msgs::SetIDs::Response &response);

  /**
   * @description: 取消消息和服务订阅
   */
  virtual void clear();
  message_filters::Subscriber<sensor_msgs::PointCloud2> cloud1_sub_;
  message_filters::Subscriber<geometry_msgs::PoseStamped> pose2_sub_;

  std::shared_ptr<message_filters::Synchronizer<cloudPoseSync>> cloud_pose_sync_;
  ros::Subscriber img_sub_, scan_sub_, gps_sub_, visual_points_sub_, merge_cloud_sub_;
  ros::Subscriber strip_detect_sub_;

  std::vector<::ros::ServiceServer> servers_;

  std::unique_ptr<lasermark::LasermarkLocalization> laser_;
  //   std::unique_ptr<VisualmarkInterface> cam_;
  std::unique_ptr<gps::GpsLocalization> gps_;
  std::unique_ptr<StripDetect> strip_detect_;
  std::string camera_topic_, scan_topic_, gps_topic_;
  bool is_cam_subscribed_, is_lasermark_subscribed_, is_gps_subscribed_, is_curb_subscribed_;
  bool is_strip_detect_subscribed_;
  bool is_uwb_subscribed_;
  //   sensor_msgs::PointCloud markers_;
  ros::Publisher marker_pub_;
  ros::Publisher gps_pose_pub_;
  ros::Publisher landmark_detect_pub_;
  ros::ServiceClient precise_localization_client_;
  ros::ServiceClient believe_in_lidar_localization_client_;
  ros::ServiceClient last_trackings_reset_client_;
  int precise_localization_num_range_data_;
  bool use_lidar_;
  bool gps_origin_flag_;
  bool last_gps_quality_;
};
} // namespace map_associator
#endif // LANDMARKLOCALIZATION_H
