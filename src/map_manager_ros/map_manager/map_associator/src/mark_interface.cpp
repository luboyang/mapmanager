#include "mark_interface.h"

#include <cartographer_ros_msgs/LandmarkList.h>
#include <glog/logging.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
using namespace std;
namespace map_associator
{
MarkInterface::MarkInterface(const int &mode)
    : mode_(mode), cam2base_(Eigen::Affine3d::Identity()), cam_initialised_(false),
      receive_tags_(false)
{
  last_strip_image_time_ = ros::Time::now();
  last_pub_landmark_time_ = ros::Time::now();
  float strip_image_frq = 10;
  ros::param::get("~strip_image_frq", strip_image_frq);
  intervel_between_strip_images_ = 1.0 / strip_image_frq;
  string landmark_topic = (mode_ == 1 ? "/visual_landmarks" : "/laser_marker");
  LandMark_pub_ = nh_.advertise<cartographer_ros_msgs::LandmarkList>(landmark_topic, 3);
  LOG(WARNING) << "mark interface initialized done!";
  detect_client_ =
      nh_.serviceClient<cartographer_ros_msgs::SetSwitch>("/sensor_detector/set_switch");
}

MarkInterface::~MarkInterface() {}

bool MarkInterface::judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    LOG(WARNING) << file_name << " is not existed!";
    existed_flag = false;
  }
  return existed_flag;
}

void MarkInterface::getPoseInMap(
    const std::string &fileName, const std::string &con_ns,
    std::vector<lasermark::LasermarkInterface::LandmarkPose> &pose_list, const int mode)
{
  boost::property_tree::ptree pt;
  try
  {
    boost::property_tree::xml_parser::read_xml(fileName, pt);
  }
  catch (std::exception &e)
  {
    LOG(WARNING) << e.what();
    LOG(WARNING) << "Can't find or open landmark.xml file and it will not use "
                    "lasermark. ";
    return;
  }
  LOG(WARNING) << "fileName: " << fileName;
  string con_ns2 = con_ns + "_4x4";
  string xml, xml_id;
  for (auto &m : pt)
  {
    if (m.first == "landmark")
    {
      string ns = m.second.get("ns", " ");
      string is_visible_tmp = m.second.get("visible", " ");
      xml_id = m.second.get("id", " ");
      LOG(WARNING) << is_visible_tmp << ": " << xml_id;
      int is_visible = stoi(is_visible_tmp);
      if ((ns != con_ns && ns != con_ns2) || (is_visible == 0 && mode == 1)) continue;
      xml_id = m.second.get("id", " ");
      xml = m.second.get("transform", " ");
      string pole_radius_str = m.second.get("pole_radius", "-1");
      float value[7];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6]);
      lasermark::LasermarkInterface::LandmarkPose pose1;
      pose1.id = stoi(xml_id); // id string 2 int
                               //       pose1.position =
                               //       Eigen::Vector2d(value[0],value[1]);
      geometry_msgs::Pose pose;
      pose.position.x = value[0];
      pose.position.y = value[1];
      pose.position.z = value[2];
      pose.orientation.x = value[3];
      pose.orientation.y = value[4];
      pose.orientation.z = value[5];
      pose.orientation.w = value[6];
      pose1.pose = pose;
      pose1.pole_radius = stof(pole_radius_str);
      pose_list.push_back(pose1);

      {
        geometry_msgs::Point32 point;
        point.x = value[0];
        point.y = value[1];
        point.z = value[2];
        markers_.points.push_back(point);
      }
    }
  }
  markers_.header.frame_id = "map";
  LOG(INFO) << "landmark size: " << pose_list.size();
}

} // namespace map_associator
