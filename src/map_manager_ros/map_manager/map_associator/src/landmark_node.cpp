/*
 * @Date: 2023-02-24 18:34:12
 * @Author: Cyy cyycyag@163.com
 * @LastEditors: C Yy cyycyag@163.com
 * @LastEditTime: 2023-03-06 18:44:49
 * @FilePath: /map_manager/map_associator/src/landmark_node.cpp
 * @Description: 切换模式， 定位or建图
 */

#include "cartographer_ros_msgs/LaunchNode.h"
#include "iplus_log_sink.hpp"
#include "mark_localization.h"
#include "mark_mapping.h"
using namespace std;
std::unique_ptr<map_associator::MarkInterface> mark;
ros::ServiceClient sensor_detector_client_;
enum STATUS
{
  IDLE = -1,
  MAPPING,
  LOCALIZATION,
  PATCH_MAPPING
};

bool launchNode(cartographer_ros_msgs::LaunchNode::Request &request,
                cartographer_ros_msgs::LaunchNode::Response &response)
{
  int mode = request.status;
  bool is_3d = false;
  if (mark != nullptr) mark->clear();

  LOG(INFO) << "new mode: " << mode << ", " << (is_3d ? "is" : "is not") << " 3d";
  cartographer_ros_msgs::LaunchNode srv;
  srv.request.status = 1;
  if (mode == -1) srv.request.status = 0;
  sensor_detector_client_.call(srv);
  if (mode == IDLE)
  {
    LOG(INFO) << "IDLE!";
    auto temp = std::move(mark);
  }
  else if (mode == MAPPING || mode == PATCH_MAPPING)
  {
    mark.reset(new map_associator::MarkMapping());
    mark->loadSome(mode);
    LOG(INFO) << "start mapping!";
  }
  else
  {
    mark.reset(new map_associator::MarkLocalization());
    mark->loadSome(mode);
    LOG(INFO) << "start localization!";
  }

  return true;
}

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
  IplusLogSink iplus_log_sink;
  google::AddLogSink(&iplus_log_sink);

  FLAGS_colorlogtostderr = true;
  // 	ScopedRosLogSink ros_log_sink;
  ros::init(argc, argv, "lasermark_node");
  ros::NodeHandle n;
  // 	LasermarkInterface* mark;
  ros::ServiceServer srv = n.advertiseService("/lasermark_launcher", launchNode);
  sensor_detector_client_ =
      n.serviceClient<cartographer_ros_msgs::LaunchNode>("/sensor_detector_launch");
  ros::spin();
  google::ShutdownGoogleLogging();
  return 1;
}