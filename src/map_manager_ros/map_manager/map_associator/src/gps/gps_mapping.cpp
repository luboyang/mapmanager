/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gps/gps_mapping.h"
namespace map_associator
{
namespace gps
{
using namespace std;
GpsMapping::GpsMapping(const ros::NodeHandle &n) : GpsInterface(n), init_flag_(false)
{
  path_pub_ = nh_.advertise<nav_msgs::Path>("gps_fix_path", 1);
  init_dip_ = 0;
}

GpsMapping::~GpsMapping() {}

// void GpsMapping::ToNavSatFix(const poslvx::INS::ConstPtr& msg,
// sensor_msgs::NavSatFix& fix_msg)
// {
//   fix_msg.header = msg->header;
//   fix_msg.latitude = msg->latitude;
//   fix_msg.longitude = msg->longitude;
//   fix_msg.altitude = msg->altitude;
//   fix_msg.position_covariance_type = 0;
//   fix_msg.header.frame_id = "fix_link";
// }

void GpsMapping::pathPub(const gps_common::GPSFix::ConstPtr &msg)
{
  Rigid3d northGps2northBase;
  double dip = msg->dip * M_PI / 180;
  Eigen::Quaterniond north2gps(cos(dip / 2), 0, 0, sin(dip / 2));
  northGps2northBase.q = gps2base_.q * north2gps;
  northGps2northBase.t = gps2base_.t;
  //   LOG(INFO) << "northBase2northGps:" << northBase2northGps ;
  if (!init_flag_)
  {
    init_flag_ = true;
    //     LOG(INFO) <<"\033[1m\033[36m"  <<"gps2base_:" << gps2base_
    //     <<"\033[0m" ;
    gps_to_base_init_ = northGps2northBase;
    ecef_to_local_frame_ = ComputeLocalFrameFromLatLong(msg->latitude, msg->longitude, 0.0);
  }
  Rigid3d fix_pose1; //= ComputeLocalFrameFromLatLong(msg->latitude,
                     // msg->longitude,msg->altitude);

  fix_pose1.t =
      ecef_to_local_frame_.q * LatLongAltToEcef(msg->latitude, msg->longitude, msg->altitude) +
      ecef_to_local_frame_.t;
  fix_pose1.q = Eigen::Quaterniond::Identity();
  geometry_msgs::PoseStamped temp_pose;
  Rigid3d base2map_gps = gps_to_base_init_ * fix_pose1 * northGps2northBase.inverse();
  //   LOG(INFO) << "base2map_gps:" << base2map_gps ;
  temp_pose.header.stamp = msg->header.stamp;
  temp_pose.pose.position.x = base2map_gps.t(0);
  temp_pose.pose.position.y = base2map_gps.t(1);
  temp_pose.pose.position.z = base2map_gps.t(2);
  //   LOG(INFO) << base2map_gps ;
  gps_path_.poses.push_back(temp_pose);
  gps_path_.header.frame_id = "gps_fix_link";
  if (path_pub_.getNumSubscribers())
  {
    path_pub_.publish(gps_path_);
  }
}

void GpsMapping::clear() { path_pub_.shutdown(); }
} // namespace gps
} // namespace map_associator
