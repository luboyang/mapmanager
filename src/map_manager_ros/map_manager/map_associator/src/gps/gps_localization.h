/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef GPSLOCALIZATION_H
#define GPSLOCALIZATION_H
#include <nav_msgs/Path.h>
#include <vtr_msgs/GlobalLocalizationPose.h>

#include <chrono>
#include <queue>

#include "gps/gps_interface.h"
namespace map_associator
{
namespace gps
{
class GpsLocalization : public GpsInterface
{
  public:
  GpsLocalization(const ros::NodeHandle &n, const std::string &path);
  ~GpsLocalization();
  bool handleGps(const gps_common::GPSFix::ConstPtr &msg,
                 vtr_msgs::GlobalLocalizationPose &pose_temp);
  bool hasGpsInfo() { return has_gps_data_info_; }
  void setParam();
  void setUseLidar(const bool &use_lidar);
  void setUseGpsManual(const bool &use_gps_manual);
  bool useGpsManual();
  virtual void clear();

  private:
  void saveTF(const ros::WallTimerEvent &unused_timer_event);
  void pubStaticTf(const ros::WallTimerEvent &unused_timer_event);
  Rigid3d ecef_in_map_;

  double fix_frame_in_map_yaw_;
  // gps_type: poslvx INS
  std::queue<gps_common::GPSFix> gps_datas_;
  gps_common::GPSFix valid_msg_;

  std::chrono::steady_clock::time_point last_pub_time_;
  bool initialized_;
  ros::Time last_ros_time_;
  geometry_msgs::TransformStamped base2odom_tf_;
  float position_covariance_, orientation_covariance_;
  std::string map_path_;

  ros::Publisher path_pub_;
  nav_msgs::Path gps_path_;
  ros::WallTimer timer_;

  bool debug_;
  bool use_lidar_;

  Rigid3d ecef_to_local_frame_, gps_to_base_init_, fix_in_map_;

  nav_msgs::Path gps_mapping_path_;
  bool init_flag_;
  bool use_gps_manual_;
  std::queue<float> altitude_queue_;
  float sum_altitude_;
  bool has_gps_data_info_;
  Eigen::Affine3d last_base2odom_;
  bool last_use_manual_;
  Rigid3d reference0_in_map_;

  geometry_msgs::TransformStamped map_in_ecef_tf_;
  tf2_ros::TransformBroadcaster tf_broadcaster_;
};
} // namespace gps
} // namespace map_associator
#endif // GPSLOCALIZATION_H
