/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gps/gps_localization.h"

#include <eigen_conversions/eigen_msg.h>

#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
// #include <dbg.h>
#include <thread>
using namespace std;
using namespace std::chrono;
namespace map_associator
{
namespace gps
{
template <typename T> T GetYaw(const Eigen::Quaternion<T> &rotation)
{
  const Eigen::Matrix<T, 3, 1> direction = rotation * Eigen::Matrix<T, 3, 1>::UnitX();
  return atan2(direction.y(), direction.x());
}
bool inline judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    LOG(WARNING) << file_name << " is not existed!";
    existed_flag = false;
  }
  return existed_flag;
}
GpsLocalization::GpsLocalization(const ros::NodeHandle &n, const string &path)
    : GpsInterface(n), map_path_(path), has_gps_data_info_(false),
      last_base2odom_(Eigen::Affine3d::Identity()), last_use_manual_(false)
{
  position_covariance_ = 1;
  orientation_covariance_ = 1e9;
  ::ros::param::get("~gps_position_covariance", position_covariance_);
  ::ros::param::get("~gps_orientation_covariance", orientation_covariance_);
  LOG(INFO) << "gps_position_covariance:" << position_covariance_;
  LOG(INFO) << "gps_orientation_covariance:" << orientation_covariance_;

  path_pub_ = nh_.advertise<nav_msgs::Path>("/mark_localization/gps_path", 1);
  initialized_ = false;
  debug_ = false;
  //   if(debug_)
  //     timer_ = nh_.createWallTimer(ros::WallDuration(0.2),
  //                                &GpsLocalization::saveTF,this);
  timer_ = nh_.createWallTimer(ros::WallDuration(0.1), &GpsLocalization::pubStaticTf, this);
  timer_.stop();
  use_lidar_ = true;
  init_flag_ = false;
  use_gps_manual_ = false;
}

GpsLocalization::~GpsLocalization() { timer_.stop(); }

void GpsLocalization::setParam()
{
  boost::property_tree::ptree pt;
  LOG(WARNING) << map_path_ << "/gps_data.xml! ";
  if (!judgeFileExist(map_path_ + "/gps_data.xml"))
  {
    return;
  }

  boost::property_tree::xml_parser::read_xml(map_path_ + "/gps_data.xml", pt);
  if (pt.empty())
  {
    LOG(WARNING) << "Can not get gps_data.xml! ";
    return;
  }
  double pose[7];
  string data = pt.get<std::string>("ecef_to_local_frame_pose");

  sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
         &pose[4], &pose[5], &pose[6]);
  LOG(INFO) << "ecef_to_local_frame_pose: ";
  for (int i = 0; i < 7; i++)
    cout << std::setprecision(15) << pose[i] << " ";
  cout << endl;
  ecef_to_local_frame_.q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
  ecef_to_local_frame_.t = Eigen::Vector3d(pose[0], pose[1], pose[2]);
  data.clear();
  data = pt.get<std::string>("fix_frame_origin_in_map_pose");
  LOG(WARNING) << "fix_frame_origin_in_map_pose: " << data;
  sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
         &pose[4], &pose[5], &pose[6]);
  fix_in_map_.q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
  fix_in_map_.t = Eigen::Vector3d(pose[0], pose[1], pose[2]);
  ecef_in_map_.q = fix_in_map_.q * ecef_to_local_frame_.q;
  ecef_in_map_.t = fix_in_map_.q * ecef_to_local_frame_.t + fix_in_map_.t;

  auto item = pt.get_child_optional("gps_to_base_init_pose");
  if (!item)
  {
    gps_to_base_init_.t = Eigen::Vector3d(0, 0, 0);
    gps_to_base_init_.q = Eigen::Quaterniond::Identity();
  }
  else
  {
    data = pt.get<std::string>("gps_to_base_init_pose");
    sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
           &pose[4], &pose[5], &pose[6]);
    gps_to_base_init_.q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
    gps_to_base_init_.t = Eigen::Vector3d(pose[0], pose[1], pose[2]);
  }
  initialized_ = false;
  last_pub_time_ = steady_clock::now();
  LOG(INFO) << "fix_frame_origin_in_map: " << fix_in_map_;
  LOG(INFO) << "ecef_to_local_frame: " << ecef_to_local_frame_;
  LOG(INFO) << "ecef_in_map_: " << ecef_in_map_;
  LOG(INFO) << "gps_to_base_init_: " << gps_to_base_init_;
  fix_frame_in_map_yaw_ = (fix_in_map_ * gps_to_base_init_).getYaw();
  //   dbg(fix_frame_in_map_yaw_);
  while (!gps_datas_.empty())
    gps_datas_.pop();
  if (pose[0] != 0 || pose[1] != 0 || pose[2] != 0) has_gps_data_info_ = true;
  //   string map_path_;
  bool is_use_pcd_submap = false;
  ros::param::get("/vtr/use_pcd_submap", is_use_pcd_submap);
  string frame0_path = map_path_ + "/frames/0/data.xml";
  if (is_use_pcd_submap)
  {
    frame0_path = map_path_ + "/pcd_frames/frames/0/data.xml";
    boost::property_tree::ptree pt, pt_poses;
    boost::property_tree::read_xml(frame0_path, pt_poses);
    //  pt_poses = pt.get_child("submap_poses");
    std::string data;
    data = pt_poses.get<std::string>("submap_global_pose");
    float pose[7];
    sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3], &pose[4],
           &pose[5], &pose[6]);

    Eigen::Quaternionf q(pose[6], pose[3], pose[4], pose[5]);

    reference0_in_map_.t = Eigen::Vector3d(pose[0], pose[1], pose[2]);
    reference0_in_map_.q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
  }
  else
  {
    reference0_in_map_.t = Eigen::Vector3d(0, 0, 0);
    reference0_in_map_.q = Eigen::Quaterniond::Identity();
  }
  LOG(INFO) << "reference0_in_map: " << reference0_in_map_;
  {
    Rigid3d map_in_ecef = (fix_in_map_ * gps_to_base_init_ * ecef_to_local_frame_).inverse();
    map_in_ecef_tf_.header.frame_id = "ecef";
    map_in_ecef_tf_.child_frame_id = "map";
    map_in_ecef_tf_.transform.translation.x = map_in_ecef.t.x();
    map_in_ecef_tf_.transform.translation.y = map_in_ecef.t.y();
    map_in_ecef_tf_.transform.translation.z = map_in_ecef.t.z();

    map_in_ecef_tf_.transform.rotation.x = map_in_ecef.q.x();
    map_in_ecef_tf_.transform.rotation.y = map_in_ecef.q.y();
    map_in_ecef_tf_.transform.rotation.z = map_in_ecef.q.z();
    map_in_ecef_tf_.transform.rotation.w = map_in_ecef.q.w();
    timer_.start();
    LOG(WARNING) << "Start pub map in ecef tf! ";
  }
}

void GpsLocalization::setUseLidar(const bool &use_lidar)
{
  std::string switch_msg = use_lidar ? "USE" : "NOT USE";
  LOG(WARNING) << "Gps: " << switch_msg << " LIDAR!";
  use_lidar_ = use_lidar;
}

void GpsLocalization::setUseGpsManual(const bool &use_gps_manual)
{
  std::string switch_msg = use_gps_manual ? "USE" : "NOT USE";
  LOG(WARNING) << "Gps: " << switch_msg << " Gps Manual!";

  use_gps_manual_ = use_gps_manual;
  if (!use_gps_manual) last_use_manual_ = false;
}

bool GpsLocalization::useGpsManual() { return use_gps_manual_; }

bool GpsLocalization::handleGps(const gps_common::GPSFix::ConstPtr &msg,
                                vtr_msgs::GlobalLocalizationPose &pose_temp)
{
  if (!has_gps_data_info_) return false;
  //   if(msg->status.status == 2)
  gps_datas_.push(*msg);
  steady_clock::time_point cur_time = steady_clock::now();
  duration<double> dur_time = duration_cast<duration<double>>(cur_time - last_pub_time_);
  ros::Time cur_ros_time = msg->header.stamp;
  if (gps_datas_.empty()) return false;
  Eigen::Affine3d cur_base2odom;

  while (!gps_datas_.empty())
  {
    try
    {
      base2odom_tf_ =
          tfBuffer_.lookupTransform("odom", "base_footprint", gps_datas_.front().header.stamp);
      tf::transformMsgToEigen(base2odom_tf_.transform, cur_base2odom);
      initialized_ = true;
      valid_msg_ = gps_datas_.front();
      gps_datas_.pop();
    }
    catch (tf::TransformException ex)
    {
      ros::Duration temp_dur = msg->header.stamp - gps_datas_.front().header.stamp;
      if (temp_dur.toSec() >= 10)
        gps_datas_.pop();
      else
        break;
    }
  }
  LOG_EVERY_N(WARNING, 500) << "gps time DUR:" << dur_time.count();
  if (dur_time.count() < 0.09 || !initialized_ ||
      (cur_ros_time - last_ros_time_).toSec() < 0.09 /*|| msg->err_dip == 0*/)
    return false;

  geometry_msgs::TransformStamped base2map;
  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  while (ros::ok())
  {
    try
    {
      base2map = tfBuffer_.lookupTransform("map", "base_footprint", valid_msg_.header.stamp);
      break;
    }
    catch (tf::TransformException ex)
    {
      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
      if (duration.count() > 0.5)
      {
        LOG(WARNING) << "Can't get transform between odom and base_footprint!";
        return false;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }
  Eigen::Quaterniond base2map_tf_q(base2map.transform.rotation.w, base2map.transform.rotation.x,
                                   base2map.transform.rotation.y, base2map.transform.rotation.z);
  pose_temp.header.stamp = valid_msg_.header.stamp;
  pose_temp.header.frame_id = "map";
  pose_temp.sensor2odom.position.x = base2odom_tf_.transform.translation.x;
  pose_temp.sensor2odom.position.y = base2odom_tf_.transform.translation.y;
  pose_temp.sensor2odom.position.z = base2odom_tf_.transform.translation.z;
  pose_temp.sensor2odom.orientation.x = base2odom_tf_.transform.rotation.x;
  pose_temp.sensor2odom.orientation.y = base2odom_tf_.transform.rotation.y;
  pose_temp.sensor2odom.orientation.z = base2odom_tf_.transform.rotation.z;
  pose_temp.sensor2odom.orientation.w = base2odom_tf_.transform.rotation.w;
  pose_temp.reference_id = 0;
  if ((last_base2odom_.inverse() * cur_base2odom).translation().norm() < 1.0)
    pose_temp.localization_type = "rtk_IGNORE";
  else
    last_base2odom_ = cur_base2odom;
  if (use_gps_manual_)
  {
    if (valid_msg_.status.status != 2) return false;
    if (!last_use_manual_)
    {
      pose_temp.localization_type = "rtk_MANUAL";
      last_base2odom_ = cur_base2odom;
      last_use_manual_ = true;
    }
  }

  if (valid_msg_.status.status != 2 || valid_msg_.err_dip == 0)
  {
    pose_temp.pose_valid = false;
    pose_temp.sensor2reference.pose.orientation.w = base2map.transform.rotation.w;
    pose_temp.sensor2reference.pose.orientation.x = base2map.transform.rotation.x;
    pose_temp.sensor2reference.pose.orientation.y = base2map.transform.rotation.y;
    pose_temp.sensor2reference.pose.orientation.z = base2map.transform.rotation.z;
    pose_temp.sensor2reference.pose.position.x = base2map.transform.translation.x;
    pose_temp.sensor2reference.pose.position.y = base2map.transform.translation.y;
    pose_temp.sensor2reference.pose.position.z = base2map.transform.translation.z;
    for (int i = 0; i < 6; i++)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e9;
    }
    return true;
  }
  Rigid3d northGps2northBase;
  double dip = valid_msg_.dip * M_PI / 180;
  Eigen::Quaterniond north2gps(cos(dip / 2), 0, 0, sin(dip / 2));
  northGps2northBase.q = gps2base_.q * north2gps;
  northGps2northBase.t = gps2base_.t;

  Rigid3d fix_pose1;
  fix_pose1.t = ecef_to_local_frame_.q * LatLongAltToEcef(valid_msg_.latitude, valid_msg_.longitude,
                                                          valid_msg_.altitude) +
                ecef_to_local_frame_.t;
  fix_pose1.q = Eigen::Quaterniond::Identity();
  geometry_msgs::PoseStamped temp_pose;

  Rigid3d base2fix = (gps_to_base_init_ * fix_pose1) * northGps2northBase.inverse();
  //   LOG(INFO) << "base2map_gps:" << base2map_gps ;
  Rigid3d base_to_map_gps = fix_in_map_ * base2fix;

  Eigen::Vector3d gps2fix_eul(-dip, 0, 0);
  Eigen::Quaterniond gps2fix_q = eul2quat(gps2fix_eul);
  Eigen::Quaterniond base2map_gps_q =
      (fix_in_map_ * gps_to_base_init_).q * gps2fix_q * (gps2base_.q.conjugate());
  Eigen::Vector3d corrected_eul(0, 0, 0);
  Eigen::Vector3d base2map_eul(0, 0, 0);
  tf::Matrix3x3(tf::Quaternion(base2map_gps_q.x(), base2map_gps_q.y(), base2map_gps_q.z(),
                               base2map_gps_q.w()))
      .getRPY(corrected_eul[2], corrected_eul[1], corrected_eul[0]);
  tf::Matrix3x3(
      tf::Quaternion(base2map_tf_q.x(), base2map_tf_q.y(), base2map_tf_q.z(), base2map_tf_q.w()))
      .getRPY(base2map_eul[2], base2map_eul[1], base2map_eul[0]);

  double yaw_corrected = corrected_eul[0];
  double myPitch, mzYaw;
  mzYaw = base2map_eul(0);
  myPitch = base2map_eul(1);
  LOG(INFO) << "base2map_eul_origin: " << 180 / M_PI * base2map_eul.transpose();
  if (cos(myPitch) < 0)
    base2map_eul(0) = M_PI + yaw_corrected;
  else
    base2map_eul(0) = yaw_corrected;
  LOG(INFO) << "base2map_eul_fix: " << 180 / M_PI * base2map_eul.transpose();
  Rigid3d base2map_gps;
  base2map_gps.t = base_to_map_gps.t;
  base2map_gps.q = eul2quat(base2map_eul); // base2map_gps.q = base_to_map_gps.q

  Rigid3d base2reference = reference0_in_map_.inverse() * base2map_gps;
  pose_temp.sensor2reference.pose.orientation.w = base2reference.q.w();
  pose_temp.sensor2reference.pose.orientation.x = base2reference.q.x();
  pose_temp.sensor2reference.pose.orientation.y = base2reference.q.y();
  pose_temp.sensor2reference.pose.orientation.z = base2reference.q.z();

  pose_temp.sensor2reference.pose.position.x = base2reference.t(0);
  pose_temp.sensor2reference.pose.position.y = base2reference.t(1);
  pose_temp.sensor2reference.pose.position.z = base2reference.t(2);

  LOG(WARNING) << " [" << valid_msg_.header.stamp << "]"
               << "base2map_gps: " << base2map_gps.t.transpose();
  Eigen::Vector2d det_t =
      Eigen::Vector2d(base2map_gps.t(0), base2map_gps.t(1)) -
      Eigen::Vector2d(base2map.transform.translation.x, base2map.transform.translation.y);
  bool higher_translation_cov = false;
  bool higher_rotation_cov = false;
  {
    if (det_t.norm() > 1. && !use_gps_manual_)
    {
      LOG(WARNING) << "det translation norm is " << det_t.norm() << ", is bigger than 1.0";
      higher_translation_cov = true;
      return false;
    }

    if (fabs(sin(base2map_eul(0) - mzYaw)) > 0.1 && !use_gps_manual_)
    {
      LOG(WARNING) << "sin(det_rotation) is bigger than 0.1";
      higher_rotation_cov = true;
      return false;
    }
  }

  float temp_average = valid_msg_.altitude;
  if (!altitude_queue_.empty()) temp_average = sum_altitude_ / altitude_queue_.size();

  altitude_queue_.push(valid_msg_.altitude);
  sum_altitude_ += valid_msg_.altitude;
  if (altitude_queue_.size() > 20)
  {
    sum_altitude_ -= altitude_queue_.front();
    altitude_queue_.pop();
  }

  for (int i = 0; i < 6; i++)
  {
    if (i < 3)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = position_covariance_;
      if (fabs(valid_msg_.altitude - temp_average) > 0.3 || higher_translation_cov)
      {
        LOG_EVERY_N(WARNING, 10) << "det_x: "
                                 << fabs(base2map.transform.translation.x - base2map_gps.t(0))
                                 << " det_y: "
                                 << fabs(base2map.transform.translation.y - base2map_gps.t(1));
        pose_temp.sensor2reference.covariance[i * 6 + i] = 1e6;
      }
    }
    else
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = orientation_covariance_;
      if (valid_msg_.err_dip == 0)
      {
        LOG_EVERY_N(WARNING, 10) << "err_dip is too big: " << valid_msg_.err_dip;
        pose_temp.sensor2reference.covariance[i * 6 + i] = 1e6;
      }
      else if (valid_msg_.err_dip > 5.0 || higher_rotation_cov)
      {
        LOG_EVERY_N(WARNING, 10) << "err_dip is " << valid_msg_.err_dip;
        pose_temp.sensor2reference.covariance[i * 6 + i] = 1e3;
      }
    }
    if (use_gps_manual_)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-9;
    }

    if (valid_msg_.status.status != 2) pose_temp.sensor2reference.covariance[i * 6 + i] = 1e9;
  }

  pose_temp.pose_valid = true;
  last_pub_time_ = steady_clock::now();

  last_ros_time_ = cur_ros_time;

  {
    if (path_pub_.getNumSubscribers())
    {
      geometry_msgs::PoseStamped temp_pose;
      temp_pose.header.stamp = valid_msg_.header.stamp;
      temp_pose.pose.position.x = base2map_gps.t(0);
      temp_pose.pose.position.y = base2map_gps.t(1);
      temp_pose.pose.position.z = base2map_gps.t(2);
      gps_path_.poses.push_back(temp_pose);
      gps_path_.header.frame_id = "map";
      path_pub_.publish(gps_path_);
    }
    else if (!gps_path_.poses.empty())
      gps_path_.poses.clear();
  }
  if (debug_)
  {
    fstream f(std::string(std::getenv("HOME")) + "/gps_path.txt", ios::app);
    f << valid_msg_.status.satellites_used << "\t" << valid_msg_.hdop << "\t" << valid_msg_.err_dip
      << "\t" << base2map_gps.t(0) << "\t" << base2map_gps.t(1) << "\t" << base2map_gps.t(2) << "\t"
      << base2map_eul(0) << "\t";
    //     f << base2map.transform.translation.x << "\t" <<
    //     base2map.transform.translation.y << "\t" <<
    //     base2map.transform.translation.z <<"\t" << mzYaw;
    f.close();
  }
  return true;
}

void GpsLocalization::saveTF(const ros::WallTimerEvent &unused_timer_event)
{
  if (!initialized_) return;
  geometry_msgs::TransformStamped base2map;
  try
  {
    base2map = tfBuffer_.lookupTransform("map", "base_footprint", ros::Time(0));
  }
  catch (tf::TransformException ex)
  {
  }
  fstream f2(std::string(std::getenv("HOME")) + "/tf.txt", ios::app);
  f2 << base2map.transform.translation.x << "\t" << base2map.transform.translation.y << "\t"
     << base2map.transform.translation.z << endl;
  f2.close();
}

void GpsLocalization::pubStaticTf(const ros::WallTimerEvent &unused_timer_event)
{
  map_in_ecef_tf_.header.stamp = ros::Time::now();
  tf_broadcaster_.sendTransform(map_in_ecef_tf_);
}

void GpsLocalization::clear()
{
  timer_.stop();
  path_pub_.shutdown();
}
} // namespace gps
} // namespace map_associator