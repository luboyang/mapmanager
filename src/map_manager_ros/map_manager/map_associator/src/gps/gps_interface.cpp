/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "gps/gps_interface.h"
namespace map_associator
{
namespace gps
{
constexpr double DegToRad(double deg) { return M_PI * deg / 180.; }

// Converts form radians to degrees.
constexpr double RadToDeg(double rad) { return 180. * rad / M_PI; }

GpsInterface::GpsInterface(const ros::NodeHandle &n)
    : nh_(n), tfBuffer_(ros::Duration(20.)), tfListener_(tfBuffer_)
{
  double gps2base_x = 0, gps2base_y = 0, gps2base_z = 0, gps2base_yaw = 0;
  ::ros::param::get("/jzhw/calib/gps/default/px", gps2base_x);
  ::ros::param::get("/jzhw/calib/gps/default/py", gps2base_y);
  ::ros::param::get("/jzhw/calib/gps/default/pz", gps2base_z);
  ::ros::param::get("/jzhw/calib/gps/default/yaw", gps2base_yaw);
  geometry_msgs::TransformStamped gps2base_tf;
  int k = 0;
  ros::Rate rate(10);
  while (ros::ok())
  {
    try
    {
      gps2base_tf = tfBuffer_.lookupTransform("base_footprint", "gnss_link", ros::Time(0));
      gps2base_.t =
          Eigen::Vector3d(gps2base_tf.transform.translation.x, gps2base_tf.transform.translation.y,
                          gps2base_tf.transform.translation.z);

      gps2base_.q =
          Eigen::Quaterniond(gps2base_tf.transform.rotation.w, gps2base_tf.transform.rotation.x,
                             gps2base_tf.transform.rotation.y, gps2base_tf.transform.rotation.z);
      break;
    }
    catch (tf::TransformException ex)
    {
      if (k++ > 20)
      {
        LOG(WARNING) << "Can not get gps2base tf!";
        gps2base_.t = Eigen::Vector3d(gps2base_x, gps2base_y, gps2base_z);
        gps2base_.q = Eigen::Quaterniond(cos(gps2base_yaw / 2), 0, 0, sin(gps2base_yaw / 2));
        break;
      }
      rate.sleep();
    }
  }

  LOG(INFO) << "gps2base_:" << gps2base_;
}

GpsInterface::~GpsInterface() {}

Eigen::Quaterniond GpsInterface::eul2quat(const Eigen::Vector3d &eul)
{
  Eigen::Matrix3d mat;
  mat = Eigen::AngleAxisd(eul[0], ::Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(eul[1], ::Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(eul[2], ::Eigen::Vector3d::UnitX());
  return Eigen::Quaterniond(mat);
}

Eigen::Vector3d GpsInterface::LatLongAltToEcef(const double latitude, const double longitude,
                                               const double altitude)
{
  // https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
  constexpr double a = 6378137.; // semi-major axis, equator to center.
  constexpr double f = 1. / 298.257223563;
  constexpr double b = a * (1. - f); // semi-minor axis, pole to center.
  constexpr double a_squared = a * a;
  constexpr double b_squared = b * b;
  constexpr double e_squared = (a_squared - b_squared) / a_squared;
  const double sin_phi = std::sin(DegToRad(latitude));
  const double cos_phi = std::cos(DegToRad(latitude));
  const double sin_lambda = std::sin(DegToRad(longitude));
  const double cos_lambda = std::cos(DegToRad(longitude));
  const double N = a / std::sqrt(1 - e_squared * sin_phi * sin_phi);
  const double x = (N + altitude) * cos_phi * cos_lambda;
  const double y = (N + altitude) * cos_phi * sin_lambda;
  const double z = (b_squared / a_squared * N + altitude) * sin_phi;

  return Eigen::Vector3d(x, y, z);
}

const GpsInterface::Rigid3d GpsInterface::ComputeLocalFrameFromLatLong(const double latitude,
                                                                       const double longitude,
                                                                       const double altitude)
{
  const Eigen::Vector3d translation = LatLongAltToEcef(latitude, longitude, altitude);
  const Eigen::Quaterniond rotation =
      Eigen::AngleAxisd(DegToRad(latitude - 90.), Eigen::Vector3d::UnitY()) *
      Eigen::AngleAxisd(DegToRad(-longitude), Eigen::Vector3d::UnitZ());
  return GpsInterface::Rigid3d({rotation * -translation, rotation});
}
} // namespace gps
} // namespace map_associator