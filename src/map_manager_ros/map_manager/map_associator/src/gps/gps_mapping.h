/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef GPSMAPPING_H
#define GPSMAPPING_H
#include <nav_msgs/Path.h>
#include <sensor_msgs/NavSatFix.h>

#include "gps_interface.h"
namespace map_associator
{
namespace gps
{
class GpsMapping : public GpsInterface
{
  public:
  GpsMapping(const ros::NodeHandle &n);
  ~GpsMapping();
  //   void ToNavSatFix(const poslvx::INS::ConstPtr &msg, sensor_msgs::NavSatFix
  //   &fix_msg);
  void pathPub(const gps_common::GPSFix::ConstPtr &msg);
  virtual void clear();

  private:
  bool init_flag_;
  ros::Publisher path_pub_;

  Rigid3d ecef_to_local_frame_, gps_to_base_init_;
  nav_msgs::Path gps_path_;
  double init_dip_;
};
} // namespace gps
} // namespace map_associator
#endif // GPSMAPPING_H
