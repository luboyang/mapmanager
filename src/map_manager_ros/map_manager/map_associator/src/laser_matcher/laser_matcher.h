/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef LASERMATCHER_H
#define LASERMATCHER_H
#include <cartographer_ros_msgs/LandmarkList.h>
#include <csm_eigen/csm_all.h>
#include <math.h>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <iostream>

#include "Eigen/Eigen"
#undef min
#undef max
namespace map_associator
{
namespace laser_matcher
{
class LaserMatcher
{
  public:
  LaserMatcher(const ros::NodeHandle &n);
  ~LaserMatcher();
  bool HandleScenes(const int &id, cartographer_ros_msgs::LandmarkList &landmark_list);
  bool saveScenes(const std::string &map_path);
  bool loadScenes(const std::string &map_path);
  void clear();

  private:
  void initParams();
  std::string vec_double2string(std::vector<double> &feature);
  std::vector<double> string_to_vec_double(const std::string &str);
  void scanCallBack(const sensor_msgs::LaserScan::ConstPtr &msg);
  void laserScanToLDP(const sensor_msgs::LaserScan &scan_msg, LDP &ldp);
  bool computePose(const LDP &ldp1, const LDP &ldp2, tf::Transform &f2l);
  void createTfFromXYTheta(double x, double y, double theta, tf::Transform &t);
  bool match(sensor_msgs::LaserScan scan1_msg, sensor_msgs::LaserScan scan2_msg,
             tf::Transform &f2l);

  sm_params input_;
  sm_result output_;
  double pr_ch_x_, pr_ch_y_, pr_ch_a_;

  ros::NodeHandle nh_;
  ros::Publisher scan1_pub_, scan2_pub_;
  tf::TransformBroadcaster broadcaster_;

  std::map<int, sensor_msgs::LaserScan> origin_scan_with_ids_;
  boost::shared_ptr<const sensor_msgs::LaserScan> last_scan_msg_;
  ros::Subscriber scan_sub_;
};
} // namespace laser_matcher
} // namespace map_associator
#endif // LASERMATCHER_H
