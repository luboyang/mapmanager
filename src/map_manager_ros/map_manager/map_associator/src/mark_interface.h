/*
 * @Date: 2023-02-24 18:34:12
 * @Author: Cyy cyycyag@163.com
 * @LastEditors: C Yy cyycyag@163.com
 * @LastEditTime: 2023-03-06 21:35:51
 * @FilePath: /map_manager/map_associator/src/mark_interface.h
 * @Description: 建图定位接口
 */

#ifndef MARKINTERFACE_H
#define MARKINTERFACE_H
#include <cartographer_ros_msgs/SetSwitch.h>
#include <glog/logging.h>
#include <memory.h>
#include <ros/ros.h>
#include <strip_detect.h>

#include <mutex>

#include "lasermark/lasermark_interface.h"
namespace map_associator
{
class MarkInterface
{
  public:
  /**
   * @description: 初始化变量
   * @param {int} &mode 模式： 0-建图，1-定位，2-续建
   * @return {*}
   */
  MarkInterface(const int &mode = 1);
  virtual ~MarkInterface();
  virtual void clear() = 0;
  virtual void loadSome(const int &mode){};
  /**
   * @description: 判断文件是否存在
   * @param {string} &file_name 文件路径
   * @return {*} 存在返回true，否则false
   */
  bool judgeFileExist(const std::string &file_name);

  /**
   * @description: 从xml文件中提取指定命名空间的marker poses
   * @param {string} &fileName xml文件路径
   * @param {string} &con_ns 命名空间名字
   * @param {  } &pose_list marker poses
   * @param {int} mode 是否提取被隐藏的marker，1-不提取，0-提取
   * @return {*}
   */
  void getPoseInMap(const std::string &fileName, const std::string &con_ns,
                    std::vector<lasermark::LasermarkInterface::LandmarkPose> &pose_list,
                    const int mode = 1);

  sensor_msgs::PointCloud markers_;

  protected:
  ros::NodeHandle nh_;
  ros::Publisher LandMark_pub_;
  ros::ServiceClient detect_client_;
  ros::Time last_strip_image_time_;
  int mode_;
  float intervel_between_strip_images_;
  ros::WallTimer wall_timer_;
  Eigen::Affine3d cam2base_;

  float tag_weight_;
  ros::Time last_pub_landmark_time_;
  bool cam_initialised_;
  std::mutex mutex_;
  float proj_index_ratio_;
  bool receive_tags_;
};
} // namespace map_associator
#endif // MARKINTERFACE_H
