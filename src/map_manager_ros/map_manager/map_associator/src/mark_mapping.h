/*
 * @Author: Cyy && cyycyag@163.com
 * @Date: 2023-02-24 18:34:12
 * @LastEditors: Cyy && cyycyag@163.com
 * @LastEditTime: 2023-03-06 21:58:50
 * @FilePath: /map_manager/map_associator/src/mark_mapping.h
 * @Description: 建图接口
 */


#ifndef LANDMARKMAPPING_H
#define LANDMARKMAPPING_H
// #include "visualmark/apriltag_detect.h"
// #include "visualmark/groundtag_perception.h"
#include <cartographer_ros_msgs/SaveSubmapServer.h>
#include <cartographer_ros_msgs/SetSwitch.h>
#include <jzstd_srvs/SetInteger.h>
#include <nav_msgs/Odometry.h>

#include "gps/gps_mapping.h"
#include "laser_matcher/laser_matcher.h"
#include "lasermark/lasermark_mapping.h"
#include "mark_interface.h"

namespace map_associator
{
class MarkMapping : public MarkInterface
{
  public:
  MarkMapping();
  ~MarkMapping();

  private:
  /**
   * @description: 处理检测到的反射板：与之前检测到的反射板进行数据关联或新赋予该反射板id
   * @param {LandmarkListConstPtr} &msg 检测到的反射板点云
   * @return {*}
   */
  void HandleLasermarksPoints(const cartographer_ros_msgs::LandmarkListConstPtr &msg);

  /**
   * @description: 将检测到的二维码信息进行转发
   * @param {LandmarkListConstPtr} &msg
   * @return {*}
   */
  void HandleImage(const cartographer_ros_msgs::LandmarkListConstPtr &msg);

  /**
   * @description: 处理gps信息：筛选有用的信息
   * @param {ConstPtr} &gps_msg
   * @return {*}
   */
  void HandleGps(const gps_common::GPSFix::ConstPtr &gps_msg);

  /**
   * @description: 处理条带图像信息
   * @param {ImageConstPtr} &img_msg 
   * @return {*}
   */
  void HandleStripImage(const sensor_msgs::ImageConstPtr &img_msg);

  /**
   * @description: 处理轮式里程计信息，有速度时不接受二维码，只有在静止时接受不了二维码信息
   * @param {ConstPtr} &msg
   * @return {*}
   */
  void HandleWheelOdom(const nav_msgs::Odometry::ConstPtr &msg);

  /**
   * @description: 
   * @param {LandmarkList} &list
   * @return {*}
   */
  void addToSortListAndPub(const cartographer_ros_msgs::LandmarkList &list);
  bool setMarkSwitch(::cartographer_ros_msgs::SetSwitch::Request &request,
                     ::cartographer_ros_msgs::SetSwitch::Response &response);
  bool SetScenesId(jzstd_srvs::SetInteger::Request &request,
                   jzstd_srvs::SetInteger::Response &response);
  bool saveScenesSrv(cartographer_ros_msgs::SaveSubmapServer::Request &request,
                     cartographer_ros_msgs::SaveSubmapServer::Response &response);

  virtual void clear();
  virtual void loadSome(const int &mode) override;
  ros::Publisher gps_pub_;
  ros::Publisher strip_points_pub_;
  ros::Subscriber laser_sub_;
  ros::Subscriber img_sub_;
  ros::Subscriber gps_sub_;
  ros::Subscriber strip_img_sub_;
  ros::Subscriber wheel_odom_sub_;
  std::unique_ptr<lasermark::LaserMarkMapping> laser_;
  std::unique_ptr<gps::GpsMapping> gps_;
  std::unique_ptr<laser_matcher::LaserMatcher> laser_scenes_;
  std::unique_ptr<StripDetect> strip_detect_;
  bool laser_makr_initialized_;
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;
  ros::Time last_lasermark_time_;
  bool use_lasermark_, use_tag_, use_gps_, use_scenes_;
  bool use_strip_detect_;
  std::set<cartographer_ros_msgs::LandmarkList, lasermark::landmarkListsSort> sortLists_;
  std::vector<::ros::ServiceServer> servers_;
  int lasermark_cnt_, tag_cnt_;
  std::queue<float> altitude_queue_;
  float sum_altitude_;
  int mark_receive_cnt_;
  ros::Time last_wheel_odom_time_;
};
} // namespace map_associator
#endif // LANDMARKMAPPING_H
