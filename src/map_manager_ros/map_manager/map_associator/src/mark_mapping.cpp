/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mark_mapping.h"
using namespace std;
namespace map_associator
{
MarkMapping::MarkMapping()
    : MarkInterface(0), tfListener_(tfBuffer_), mark_receive_cnt_(0),
      last_lasermark_time_(ros::Time::now()), last_wheel_odom_time_(last_lasermark_time_)
{
  laser_makr_initialized_ = false;
  servers_.push_back(
      nh_.advertiseService("/mark_mapping/set_mark_switch", &MarkMapping::setMarkSwitch, this));
  servers_.push_back(nh_.advertiseService("/save_scenes_data", &MarkMapping::saveScenesSrv, this));
  if (!ros::param::get("/cartographer_node/use_lasermarks", use_lasermark_))
  {
    LOG(WARNING) << ("Can not get param of use_lasermark, exit!");
    use_lasermark_ = true;
  }
  bool is_3d = false;
  if (!ros::param::get("~is_3d", is_3d))
  {
    LOG(WARNING) << ("Can not get param of ~is_3d!");
    is_3d = false;
  }
  if (!ros::param::get("/cartographer_node/use_visualmarks", use_tag_))
  {
    LOG(WARNING) << ("Can not get param of use_tag, exit!");
    use_tag_ = false;
    // exit(1);
  }
  if (!ros::param::get("/cartographer_node/use_gps", use_gps_))
  {
    LOG(WARNING) << ("Can not get param of use_gps, exit!");
    use_gps_ = false;
    // exit(1);
  }
  if (!ros::param::get("/cartographer_node/use_scenes", use_scenes_))
  {
    LOG(WARNING) << ("Can not get param of use_scenes, exit!");
    use_scenes_ = false;
  }
  if (!ros::param::get("/cartographer_node/use_strip_detect", use_strip_detect_))
  {
    LOG(WARNING) << ("Can not get param of use_scenes, exit!");
    use_strip_detect_ = false;
  }

  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "\n use_tag: " << use_tag_ << "\n use_lasermark: " << use_lasermark_
            << "\n use_gps: " << use_gps_;

  cartographer_ros_msgs::SetSwitch srv;
  srv.request.flag = true;
  if (use_lasermark_)
  {
    laser_.reset(new map_associator::lasermark::LaserMarkMapping(nh_));
    laser_->setIs3D(is_3d);
    ::ros::param::set("/mapping_associator/lasermark_points_topic",
                      "/cartographer_ros/lidar_mark_point_cloud");
    string laser_scan_topic = "/sensor_detector/lasermarks";
    laser_sub_ = nh_.subscribe(laser_scan_topic, 3, &MarkMapping::HandleLasermarksPoints, this);
    srv.request.type = "Lasermarks";
    detect_client_.call(srv);
  }

  if (use_tag_)
  {
    string odom_topic = "/emma_odom";
    ::ros::param::get("/algo/property/motion_odom_topic", odom_topic);
    wheel_odom_sub_ = nh_.subscribe(odom_topic, 3, &MarkMapping::HandleWheelOdom, this);
    img_sub_ = nh_.subscribe("/sensor_detector/visualmarks", 3, &MarkMapping::HandleImage, this);
    srv.request.type = "Visualmarks";
    detect_client_.call(srv);
  }

  if (use_gps_)
  {
    gps_.reset(new map_associator::gps::GpsMapping(nh_));
    string gps_topic;
    gps_topic = "/jzhw/gps/fix";
    if (!ros::param::get("/jzhw/gps/default/property/gps_fix", gps_topic))
    {
      LOG(WARNING) << "LaserMarkNode: can not get param of gps_topic,use default:" << gps_topic;
    }
    LOG(INFO) << "lasermark_mapping/gps_topic: " << gps_topic;
    gps_sub_ = nh_.subscribe<gps_common::GPSFix>(gps_topic, 3, &MarkMapping::HandleGps, this);
    gps_pub_ = nh_.advertise<gps_common::GPSFix>("/fix", 3);
    sum_altitude_ = 0;
  }

  if (use_scenes_)
  {
    laser_scenes_.reset(new map_associator::laser_matcher::LaserMatcher(nh_));
    servers_.push_back(
        nh_.advertiseService("/mark_mapping/set_scenes_id", &MarkMapping::SetScenesId, this));
  }

  if (use_strip_detect_)
  {
    strip_detect_.reset(new StripDetect(nh_));
    strip_points_pub_ = nh_.advertise<map_manager_msgs::LinePoints>("base_point", 10);
    string strip_camera_topic = "/front/color/lower_frq_image_raw";
    ::ros::param::get("~front_color_image_topic", strip_camera_topic);
    strip_img_sub_ = nh_.subscribe(strip_camera_topic, 50, &MarkMapping::HandleStripImage, this);
    LOG(INFO) << "strip_camera_topic: " << strip_camera_topic;
  }
}

MarkMapping::~MarkMapping() {}

void MarkMapping::HandleLasermarksPoints(const cartographer_ros_msgs::LandmarkListConstPtr &msg)
{
  if (!laser_makr_initialized_)
  {
    // initialized_ = true;
    geometry_msgs::TransformStamped base2global_stamped;
    try
    {
      base2global_stamped = tfBuffer_.lookupTransform("map", "odom", ros::Time(0));
      laser_makr_initialized_ = true;
    }
    catch (tf2::TransformException &ex)
    {
      LOG_EVERY_N(INFO, 20) << ("waiting for cartographer !");
      return;
    }
    LOG(INFO) << "[" << ros::Time::now() << "] "
              << "Lasermark initialized down! ";
  }
  ros::Duration dur = msg->header.stamp - last_lasermark_time_;
  if (dur.toSec() < 0)
  {
    LOG(ERROR) << "laserscan time stamp is wrong!" << endl
               << "Msg time: " << msg->header.stamp.toSec() << endl
               << "last time: " << last_lasermark_time_.toSec();

    return;
  }
  if (msg->header.stamp <= last_pub_landmark_time_)
  {
    return;
  }

  last_lasermark_time_ = msg->header.stamp;
  cartographer_ros_msgs::LandmarkList landmark_list;
  laser_->HandleLaser(msg, landmark_list);
  landmark_list.header.stamp = msg->header.stamp;
  landmark_list.header.frame_id = "base_footprint";
  std::unique_lock<std::mutex> lk(mutex_);

  LandMark_pub_.publish(landmark_list);
  //   addToSortListAndPub(landmark_list);
  last_pub_landmark_time_ = msg->header.stamp;
}

void MarkMapping::HandleImage(const cartographer_ros_msgs::LandmarkListConstPtr &msg)
{
  if (receive_tags_) LandMark_pub_.publish(msg);
}

void MarkMapping::HandleGps(const gps_common::GPSFix::ConstPtr &gps_msg)
{
  //   sensor_msgs::NavSatFix fix_msg;
  gps_->pathPub(gps_msg);
  if (gps_pub_.getNumSubscribers() && use_gps_)
  {
    if (gps_msg->err_dip > 5.0 || gps_msg->status.status != 2 || gps_msg->err_dip == 0)
      return;
    else
    {
      float temp_average = gps_msg->altitude;
      if (!altitude_queue_.empty()) temp_average = sum_altitude_ / altitude_queue_.size();
      altitude_queue_.push(gps_msg->altitude);
      sum_altitude_ += gps_msg->altitude;
      if (altitude_queue_.size() > 20)
      {
        sum_altitude_ -= altitude_queue_.front();
        altitude_queue_.pop();
      }
      if (fabs(gps_msg->altitude - temp_average) > 0.3) return;
    }

    gps_pub_.publish(*gps_msg);
  }
}

void MarkMapping::HandleStripImage(const sensor_msgs::ImageConstPtr &img_msg)
{
  ros::Duration tmp = img_msg->header.stamp - last_strip_image_time_;
  if (tmp.toSec() < intervel_between_strip_images_) return;
  map_manager_msgs::LinePoints strip_points_msg;
  strip_detect_->HandleImage(img_msg, strip_points_msg);
  if (strip_points_pub_.getNumSubscribers() > 0 &&
      (strip_points_msg.left_points_in_base.size() > 0 ||
       strip_points_msg.right_points_in_base.size() > 0))
    strip_points_pub_.publish(strip_points_msg);
  last_strip_image_time_ = img_msg->header.stamp;
}

void MarkMapping::HandleWheelOdom(const nav_msgs::Odometry::ConstPtr &msg)
{
  //   std::unique_lock<std::mutex> lk(cam_mutex_);
  if (std::fabs(msg->twist.twist.linear.x) < 1e-4 && std::fabs(msg->twist.twist.linear.y < 1e-4) &&
      std::fabs(msg->twist.twist.linear.z < 1e-4) && std::fabs(msg->twist.twist.angular.x < 1e-4) &&
      std::fabs(msg->twist.twist.angular.y < 1e-4) && std::fabs(msg->twist.twist.angular.z < 1e-4))
  {
    if (mark_receive_cnt_ > 10 && (msg->header.stamp - last_wheel_odom_time_).toSec() > 0.98)
      receive_tags_ = true;
    ++mark_receive_cnt_;
  }
  else
  {
    receive_tags_ = false;
    mark_receive_cnt_ = 0;
    last_wheel_odom_time_ = msg->header.stamp;
  }
  static bool last_flag = false;
  if (last_flag != receive_tags_)
  {
    LOG(WARNING) << (receive_tags_ ? "receive tags." : "not receive tags");
    last_flag = receive_tags_;
  }
}

void MarkMapping::addToSortListAndPub(const cartographer_ros_msgs::LandmarkList &list)
{
  if (use_lasermark_ && use_tag_)
  {
    if (list.header.frame_id == "base_footprint")
    {
      ++lasermark_cnt_;
    }
    else
    {
      ++tag_cnt_;
    }
    sortLists_.insert(list);
    set<cartographer_ros_msgs::LandmarkList>::iterator it = sortLists_.begin();
    while ((lasermark_cnt_ > 0) || (tag_cnt_ > 0))
    {
      if (it->header.frame_id == "base_footprint")
      {
        --lasermark_cnt_;
      }
      else
      {
        --tag_cnt_;
      }
      LandMark_pub_.publish(*it);
      sortLists_.erase(it);
    }
  }
  else
  {
    LandMark_pub_.publish(list);
  }
}

bool MarkMapping::setMarkSwitch(cartographer_ros_msgs::SetSwitch::Request &request,
                                cartographer_ros_msgs::SetSwitch::Response &response)
{
  if (request.type == "gps")
  {
    if (request.flag)
    {
      use_gps_ = true;
    }
    else
    {
      use_gps_ = false;
    }
    std::string switch_msg = request.flag ? "use" : "not use";
    LOG(INFO) << "[" << ros::Time::now() << "] "
              << "lasermark_node/mapping " << switch_msg << " gps!";
  }
  return true;
}

bool MarkMapping::SetScenesId(jzstd_srvs::SetInteger::Request &request,
                              jzstd_srvs::SetInteger::Response &response)
{
  cartographer_ros_msgs::LandmarkList landmark_list;
  if (!laser_scenes_->HandleScenes(request.data, landmark_list) ||
      landmark_list.header.stamp <= last_pub_landmark_time_)
  {
    LOG(WARNING) << "match failed or landmark_list.header.stamp <= "
                    "last_pub_landmark_time :"
                 << landmark_list.header.stamp << " < " << last_pub_landmark_time_;
    response.message = "match failed or landmark_list.header.stamp <= last_pub_landmark_time";
    response.success = false;
    return true;
  }
  last_pub_landmark_time_ = landmark_list.header.stamp;
  LandMark_pub_.publish(landmark_list);
  response.success = true;
  return true;
}

bool MarkMapping::saveScenesSrv(cartographer_ros_msgs::SaveSubmapServer::Request &request,
                                cartographer_ros_msgs::SaveSubmapServer::Response &response)
{
  if (use_scenes_)
  {
    if (!laser_scenes_->saveScenes(request.map_name))
    {
      response.success = false;
      string error = "Save laser scan failed!";
      LOG(WARNING) << error;
      response.error_msgs = error;
      return false;
    }
    LOG(INFO) << "save success";
  }
  return true;
}

void MarkMapping::loadSome(const int &mode)
{
  if (mode == 2)
  {
    string origin_path;
    if (::ros::param::get("/vtr/origin_path", origin_path))
    {
      if (use_scenes_)
      {
        if (judgeFileExist(origin_path + "/scenes_data.yaml"))
          laser_scenes_->loadScenes(origin_path + "/");
      }

      if (use_lasermark_)
      {
        vector<map_associator::lasermark::LasermarkInterface::LandmarkPose> marks_pose;
        if (judgeFileExist(origin_path + "/landmark.xml"))
          getPoseInMap(origin_path + "/landmark.xml", "Lasermarks", marks_pose, mode);
        laser_->addExistMarks(marks_pose);
      }
    }
    else
      LOG(WARNING) << "get /vtr/origin_path failed, can not load origin scenes!";
  }
}

void MarkMapping::clear()
{
  for (auto &srv : servers_)
    srv.shutdown();
  LOG(INFO) << "mark_mapping srvs shutdown!";
  if (use_lasermark_)
  {
    laser_->clear();
    LOG(INFO) << "laser clear!";
  }
  if (use_gps_)
  {
    gps_->clear();
    LOG(INFO) << "gps clear!";
  }
  if (use_scenes_)
  {
    laser_scenes_->clear();
    LOG(INFO) << "laser_scenes clear!";
  }
  laser_sub_.shutdown();
  img_sub_.shutdown();
  gps_sub_.shutdown();
  LOG(INFO) << "mark_mapping all subs shutdown!";
  LandMark_pub_.shutdown();
  gps_pub_.shutdown();
  LOG(INFO) << "mark_mapping all pubs shutdown!";
}
} // namespace map_associator
//
