/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "lasermark/lasermark_interface.h"

#include <ros/package.h>
#include <tf2_eigen/tf2_eigen.h>
#include <yaml-cpp/yaml.h>
namespace map_associator
{
namespace lasermark
{
template <class T> bool parseYamlValue(const YAML::Node &node, std::string key, T &value)
{
  if (node[key])
  {
    value = node[key].as<T>();
    return true;
  }
  else
  {
    LOG(INFO) << "fail to parse key " << key;
    return false;
  }
}
LasermarkInterface::LasermarkInterface(const ros::NodeHandle &n) : nh_(n)
{
  //   if (!ros::param::get("~min_distance", min_distance_))
  //   {
  //     LOG(WARNING) << ("Can not get param of min_distance!");
  //     min_distance_ = 0.5;
  //   }
  //   if (!ros::param::get("~max_distance", max_distance_))
  //   {
  //     LOG(WARNING) << ("Can not get param of max_distance_!");
  //     max_distance_ = 8;
  //   }
  //   if (!ros::param::get("~min_neighbour_points_size",
  //   min_neighbour_points_size_))
  //   {
  //     LOG(WARNING) << ("Can not get param of min_neighbour_points_size!");
  //     min_neighbour_points_size_ = 3;
  //   }
  translation_weight_ = 1e3;
  if (!ros::param::get("~weight", translation_weight_))
  {
    LOG(ERROR) << ("Lasermark: can not get param of weight_!");
  }
  int mode = 0;
  if (!::ros::param::get("/vtr/mode", mode))
  {
    LOG(ERROR) << ("Lasermark: can not get param of /vtr/mode!");
  }
  //   if(mode == 2)
  //     min_distance_ = 3. * min_distance_;

  std::string path_prefix = ros::package::getPath("map_associator") + "/config/";

  std::string laser_type = "default";
  ::ros::param::get("/jzhw/calib/laser/front/laser_type", laser_type);
  float min_intensity = 251.0;
  ros::param::get("/lasermark_intensity_threshold/" + laser_type, min_intensity);
  std::string lasermark_yaml_path = path_prefix + "lasermark_location_config.yaml";
  if (mode != 1) lasermark_yaml_path = path_prefix + "lasermark_mapping_config.yaml";

  YAML::Node node = YAML::LoadFile(lasermark_yaml_path);
  CHECK(parseYamlValue(node, "min_distance_between_points", min_distance_between_points_))
      << "No min_distance_between_points.";
  CHECK(parseYamlValue(node, "max_distance", max_distance_)) << "No max_distance.";
  CHECK(parseYamlValue(node, "min_distance", min_distance_)) << "No max_distance.";
  CHECK(parseYamlValue(node, "min_neighbour_points_size", min_neighbour_points_size_))
      << "No min_neighbour_points_size.";

  LOG(INFO) << "laser_type: " << laser_type;
  LOG(INFO) << "min_intensity: " << min_intensity;
  LOG(INFO) << "min_distance: " << min_distance_;
  LOG(INFO) << "max_distance: " << max_distance_;
  LOG(INFO) << "min_neighbour_points_size_: " << min_neighbour_points_size_;

  use_laser_pole_marks_2D_ = true;
}

LasermarkInterface::~LasermarkInterface() {}

Eigen::Vector3d LasermarkInterface::getNewCoor(const Eigen::Vector3d &t2,
                                               const geometry_msgs::TransformStamped &transform)
{
  Eigen::Affine3d transform_affine3d = tf2::transformToEigen(transform);
  // 	Eigen::Vector3d t2(x,y,0);
  Eigen::Vector3d t = transform_affine3d * t2;
  return t;
}

Eigen::Vector3d LasermarkInterface::getNewCoor(const Eigen::Vector3d &t2,
                                               const Eigen::Affine3d &transform)
{
  //  Eigen::Vector3d t2(x,y,0);
  Eigen::Vector3d t = transform * t2;
  return t;
}

bool LasermarkInterface::isNewPointChecked(
    const Eigen::Vector2d &new_coor, std::vector<LasermarkInterface::LandmarkPose> &poses_in_fix,
    LasermarkInterface::LandmarkPose &result_pose)
{
  double x, y;
  x = new_coor[0];
  y = new_coor[1];
  double last_distance = 1e5;
  result_pose.id = -1; // id string 2 int
  for (unsigned int i = 0; i < poses_in_fix.size(); i++)
  {
    LandmarkPose pose = poses_in_fix[i];
    double x0, y0, distance;
    x0 = pose.pose.position.x;
    y0 = pose.pose.position.y;
    distance = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
    if (distance < min_distance_)
    {
      if (last_distance > distance)
      {
        last_distance = distance;
        result_pose = pose;
        // 				poses_in_fix[i].pose.position.x = x;
        // 				poses_in_fix[i].pose.position.y = y;
      }
    }
  }
  if (result_pose.id == -1)
  { // id string 2 int
    // new point
    result_pose.id = -2; // id string 2 int
    result_pose.pose.position.x = x;
    result_pose.pose.position.y = y;
    return true;
  }
  else
    return false;
}

std::vector<Eigen::Vector3f> LasermarkInterface::getPointFromFile(const std::string &path)
{
  std::vector<Eigen::Vector3f> points;
  std::ifstream fi;
  fi.open(path);
  if (fi.is_open())
  {
    while (fi.good())
    {
      Eigen::Vector3f data;
      fi >> data(0) >> data(1) >> data(2);
      points.push_back(data);
    }
  }
  return points;
}

std::vector<std::vector<Eigen::Vector3f>>
LasermarkInterface::refineLasermarks(const std::vector<std::vector<Eigen::Vector3f>> &marks)
{
  std::vector<std::vector<Eigen::Vector3f>> refine_results;
  for (const auto &it : marks)
  {
    if (it.size() < min_neighbour_points_size_) continue;
    std::vector<Eigen::Vector3f> candidate;
    Eigen::Vector3f tmp = it[0];
    for (const auto &it2 : it)
    {
      if ((tmp - it2).norm() < min_distance_between_points_)
      {
        candidate.push_back(it2);
      }
      else
      {
        tmp = it2;
        if (candidate.size() >= min_neighbour_points_size_) refine_results.push_back(candidate);
        std::vector<Eigen::Vector3f>().swap(candidate);
      }
    }
    if (!candidate.empty()) refine_results.push_back(candidate);
  }
  //   LOG(INFO) << "refine_results size:" << refine_results.size();
  for (auto it = refine_results.begin(); it != refine_results.end();)
  {
    if ((*it)[0].norm() > max_distance_)
      it = refine_results.erase(it);
    else
      ++it;
  }
  return refine_results;
}

} // namespace lasermark
} // namespace map_associator
// choose the mid point of candidates
// choose the point that it must have "min_neighbour_points_size_" points

// const char* GetBasename(const char* filepath) {
//   const char* base = std::strrchr(filepath, '/');
//   return base ? (base + 1) : filepath;
// }
// ScopedRosLogSink::ScopedRosLogSink() : will_die_(false) { AddLogSink(this); }
// ScopedRosLogSink::~ScopedRosLogSink() { RemoveLogSink(this); }
//
// void ScopedRosLogSink::send(const ::google::LogSeverity severity,
//                             const char* const filename,
//                             const char* const base_filename, const int line,
//                             const struct std::tm* const tm_time,
//                             const char* const message,
//                             const size_t message_len) {
//   const std::string message_string = ::google::LogSink::ToString(
//       severity, GetBasename(filename), line, tm_time, message, message_len);
//   switch (severity) {
//     case ::google::GLOG_INFO:
//       ROS_INFO_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_WARNING:
//       ROS_WARN_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_ERROR:
//       ROS_ERROR_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_FATAL:
//       ROS_FATAL_STREAM(message_string);
//       will_die_ = true;
//       break;
//   }
// }
//
// void ScopedRosLogSink::WaitTillSent() {
//   if (will_die_) {
//     // Give ROS some time to actually publish our message.
//     std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//   }
// }
