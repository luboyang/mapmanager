/*
 * @Date: 2023-03-06 16:37:47
 * @Author: Cyy && cyycyag@163.com
 * @LastEditors: Cyy && cyycyag@163.com
 * @LastEditTime: 2023-03-10 13:52:48
 * @FilePath: /map_manager/map_associator/src/lasermark/lasermark_localization.h
 * @Description: 
 */

#ifndef LASERMARKLOCALIZATION_H
#define LASERMARKLOCALIZATION_H
#include <cartographer_ros_msgs/LandmarkInDifferentCoordinates.h>
#include <cartographer_ros_msgs/LasermarkEntry.h>
#include <cartographer_ros_msgs/LinktrackNodeframe2.h>
#include <cartographer_ros_msgs/Submap.h>
#include <cartographer_ros_msgs/Submap3DLidarmark.h>
#include <cartographer_ros_msgs/SubmapLasermark.h>
#include <cartographer_ros_msgs/SubmapList.h>
#include <cartographer_ros_msgs/Submaps.h>
#include <eigen_conversions/eigen_msg.h>
#include <emma_tools_msgs/SetIDs.h>
#include <geometry_msgs/TransformStamped.h>
#include <iplus_perception/pole_like_detection.h>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/PointCloud.h>
#include <stdlib.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/MarkerArray.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <mutex>
#include <opencv2/opencv.hpp>

#include "cartographer_ros_msgs/CloudWithPose.h"
#include "curb_detection/curb_detection.hpp"
#include "lasermark_interface.h"

namespace map_associator
{
namespace lasermark
{
class LasermarkLocalization : public LasermarkInterface
{
  // struct LandmarkPose
  // {
  // 	ros::Time time_stamp;
  // 	std::string id;
  // 	Eigen::Vector2d position;
  // 	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  // };
  struct LandmarkPoses
  {
    ros::Time time_stamp;
    int id; // id string 2 int
    Eigen::Vector3d pose_in_map;
    Eigen::Vector3d pose_in_odom;
    Eigen::Vector3d pose_in_base;
    float distance;
    bool operator<(const LandmarkPoses &x) const
    {
      if (distance == x.distance)
        return distance < x.distance;
      else
        return distance < x.distance;
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  public:
  LasermarkLocalization(const ros::NodeHandle &n);
  ~LasermarkLocalization();
  virtual void clear();

  /**
   * @description: 处理收到的反射板点云，进行数据关联，再加入缓存中
   * @param {cartographer_ros_msgs::LandmarkListConstPtr} &msg 反射板点消息
   * @param {cartographer_ros_msgs::LandmarkList} landmark_list 返回数据关联上反射板点
   * @return {*}
   */
  void handleScanMatchedPoints(const cartographer_ros_msgs::LandmarkListConstPtr &msg,
                               cartographer_ros_msgs::LandmarkList &landmark_list);
                               
  /**
   * @description: 将点云中的路沿信息提取出来，并与地图中的路沿点云进行关联，再加入缓存
   * @param {PointCloud2ConstPtr} &cloud_msg 原始点云信息
   * @param {PoseStampedConstPtr} &pose_msg base2odom 的pose
   * @param {  } cartographer_ros_msgs 输出的点云对
   * @return {*}
   */
  void handleMergeCloudPoints(const sensor_msgs::PointCloud2ConstPtr &cloud_msg,
                              const geometry_msgs::PoseStampedConstPtr &pose_msg,
                              cartographer_ros_msgs::LandmarkList &landmark_list);

  /**
   * @description: 将垂直线特征进行数据格式转换并加入缓存
   * @param {ConstPtr} &msg 垂直线特征消息
   * @param {  } cartographer_ros_msgs 
   * @return {*}
   */
  void handleVerticalLineLandmark(
      const cartographer_ros_msgs::LandmarkInDifferentCoordinates::ConstPtr &msg,
      cartographer_ros_msgs::LandmarkList &landmark_list);

  /**
   * @description: 将条带特征进行数据格式转换并加入缓存
   * @param {PointCloud} &msg 条带特征点云消息
   * @return {*}
   */
  void handleStripDetectPoints(const sensor_msgs::PointCloud &msg);
  
  /**
   * @description: 将uwb信息进行数据格式转换并加入缓存
   * @param {ConstPtr} &msg 
   * @return {*}
   */
  void handleUwbPoints(const cartographer_ros_msgs::LinktrackNodeframe2::ConstPtr &msg);
  
  /**
   * @description: 将动态框选的区域中的scan-filter的点云截取并发出
   * @param {LaserScanConstPtr} &msg 
   * @return {*}
   */
  void handleScanFilter(const sensor_msgs::LaserScanConstPtr &msg);
  
  /**
   * @description: 通过轮式里程计判断当前是否是静止状态
   * @param {OdometryConstPtr} &msg
   * @return {*}
   */
  void handleWheelOdom(const nav_msgs::OdometryConstPtr &msg);

  /**
   * @description: 设置初始参数
   * @return {*}
   */
  void setParam();

  /**
   * @description: 设置是否使用激光定位
   * @param {bool} &use_scan_localiser
   * @return {*}
   */
  void setUseScanLocaliser(const bool &use_scan_localiser);

  /**
   * @description: 设置是否使用反射板定位
   * @param {bool} &use_lasermark
   * @return {*}
   */
  void setUseLasermark(const bool &use_lasermark);
  
  /**
   * @description: 设置是否使用uwb定位
   * @param {bool} &use_uwb
   * @return {*}
   */
  void setUseUwb(const bool &use_uwb);

  /**
   * @description: 设置是否使用局部激光定位
   * @param {bool} &use_precise_localization
   * @return {*}
   */
  void setUsePreciseLocalization(const bool &use_precise_localization);
  
  /**
   * @description: 设置是否使用条带检测
   * @param {bool} &use_strip_detect
   * @return {*}
   */
  void setUseStripDetect(const bool &use_strip_detect);

  /**
   * @description: 设置 gps info flag
   * @param {bool} &flag
   * @return {*}
   */
  void setBoolGpsInfo(const bool &flag);
  
  /**
   * @description: 设置是否使用路沿定位
   * @param {bool} &flag
   * @return {*}
   */
  void setUseCurb(const bool &flag);
  
  /**
   * @description: 设置是否使用动态检测
   * @param {bool} &use_dynamic_detect
   * @return {*}
   */
  void setUseDynamicDetect(const bool &use_dynamic_detect);
  
  /**
   * @description: 清楚条带点
   * @return {*}
   */
  void clearStripPoints();
  
  /**
   * @description: 返回无效uwb个数
   * @return {*}
   */
  int unValidUwbSize() { return uwb_novalid_cnt_; }

  /**
   * @description: 设置是否有uwb信息
   * @param {bool} &flag
   * @return {*}
   */
  void setHasUwb(const bool &flag) { has_uwb_ = flag; }

  /**
   * @description: 重新加载地图忽略区域图
   * @return {*}
   */
  void reloadFreeSpaceImage();

  /**
   * @description: 设置当前定位要忽略的反射板id
   * @param {map<int, int>} &ignored_lasermarks
   * @return {*}
   */
  void setIgnoredLaermarks(const std::map<int, int> &ignored_lasermarks);

  /**
   * @description: 获取是否使用柱状物点云
   * @return {*}
   */
  bool GetUsePoleMarks() { return use_pole_marks_3D_; };

  /**
   * @description: 设置定位要使用的二维码id
   * @param {map<int, int>} &location_lasermark_ids
   * @return {*}
   */
  void setLocationLaermarksID(const std::map<int, int> &location_lasermark_ids);
  
  /**
   * @description: 获取当前是否使用uwb
   * @return {*}
   */  
  bool useUwb() { return use_uwb_; }
  std::vector<LandmarkPose> landmark_pose_list_;

  private:
  /**
   * @description: 处理2d 激光定位点云：数据格式转换+marker
   * @param {SubmapsPtr} &msg
   * @return {*}
   */
  void handleSubmap(const cartographer_ros_msgs::SubmapsPtr &msg);

  /**
   * @description: 处理2d 激光定位点云：：数据格式转换+marker
   * @param {Submap3DPtr} &msg
   * @return {*}
   */
  void handleSubmap3D(const cartographer_ros_msgs::Submap3DPtr &msg);

  /**
   * @description: 处理2d 激光定位点云：：数据格式转换+marker
   * @param {CloudWithPosePtr} &msg
   * @return {*}
   */
  void handleSubmap3DLio(const cartographer_ros_msgs::CloudWithPosePtr &msg);

  // 	void getPoseInMap(const std::string& fileName);
  void judgeLaserMarker(const Eigen::Vector3d &point_in_odom, ros::Time time_stamp,
                        const geometry_msgs::TransformStamped &odom2map_tf,
                        const geometry_msgs::TransformStamped &odom2base_tf,
                        cartographer_ros_msgs::LandmarkEntry &landmark_entry);
  // 	LandmarkPose getID(const Eigen::Vector2d &coor);
  /**
   * @description: 更新缓存中的marker信息
   * @param {int} &id
   * @param {Vector3d} &coor
   * @param {Time} &time_stamp
   * @param {  } std
   * @return {*}
   */
  void updateMarks(const int &id, const Eigen::Vector3d &coor, const ros::Time &time_stamp,
                   std::vector<LandmarkPose> &marks); // id string 2 int

  /**
   * @description: 将时间戳比较老的反射板点修剪掉
   * @param {Time} &time
   * @return {*}
   */
  void cropLasermark(const ros::Time &time);
  
  /**
   * @description: carto submap grid 转到 sensor::pointcloud 
   * @param {Submap} &submap
   * @param {float} &matching_point_thresh
   * @param {bool} &has_odom2map
   * @param {Affine3d} &odom2map
   * @return {*}
   */
  const sensor_msgs::PointCloud
  submap2DToPointCloud(const cartographer_ros_msgs::Submap &submap,
                       const float &matching_point_thresh, const bool &has_odom2map,
                       const Eigen::Affine3d &odom2map = Eigen::Affine3d::Identity());
                       
  /**
   * @description: 通过定位忽略图判断当前激光点是否用于定位
   * @param {Mat} &bool_image
   * @param {Vector3d} &point
   * @return {*}
   */
  bool isPointValid(const cv::Mat &bool_image, const Eigen::Vector3d &point);

  ros::NodeHandle nh_;
  ros::Subscriber submap_local_sub_;
  ros::Subscriber scan_sub_;
  ros::Subscriber landmark_list_sub_;
  ros::Subscriber uwb_sub_;
  ros::Subscriber dynamic_detect_sub_;
  ros::Subscriber wheel_odom_sub_;

  ros::Publisher submap_lasermark_pub_;
  ros::Publisher point_pub_;
  ros::Publisher strip_points_in_map_pub_;
  ros::Publisher pole_marker_pub_;
  ros::Publisher submap_cloud_pub_;
  ros::Publisher detect_curb_cloud_pub_;
  ros::Publisher curb_cloud_in_map_pub_;
  ros::Publisher dynamic_detect_pub_;
  ros::ServiceClient submap_server_client_;
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  std::mutex mutex_, dynamic_detect_mutex_;
  std::vector<LandmarkPose> marks_in_odom_, marks_in_map_;
  sensor_msgs::PointCloud strip_detect_points_;
  std::set<int> marks_id_;
  std::map<std::string, int> marks_cnt_;
  bool initialized_;
  long landmark_id_;
  bool show_state_print_;
  bool debug_flag_;

  std::string map_path_;
  cv::Mat bool_img_;
  double max_box_[2];
  double resolution_;
  ros::ServiceServer handle_rect_invalid_area_server_;
  bool is_3d_;

  bool use_scan_localiser_;
  bool use_precise_localization_;
  bool use_lasermark_;
  bool use_uwb_;
  bool use_strip_detect_;
  bool use_visual_points_;
  bool use_curb_;
  const double matching_point_thresh_finished_, matching_point_thresh_unfinished_;

  geometry_msgs::Pose last_strip_detect_base2odom_;
  std::map<ros::Time, std::vector<Eigen::Vector4d>> uwb_datas_;
  std::map<int, Eigen::Vector3d> uwb_marks_;
  int uwb_novalid_cnt_;
  int has_uwb_;
  bool has_gps_info_;

  std::map<int, int> location_lasermark_ids_;
  std::map<int, int> ignored_lasermarks_;
  bool use_pole_marks_3D_;
  std::shared_ptr<PoleLikeDetection> pole_detection_;
  std::shared_ptr<curb::curbDetector> curb_detection_;

  std::map<int, float> pole_radius_with_id_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr curb_points_;
  pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr kdtree_curb_;
  int curb_id_;
  cv::Mat dynamic_free_space_image_;
  bool zero_velocity_flag_;

  protected:
  template <typename T1, typename T2> void handleSubmap3DMsg(const T1 &msg)
  {
    if (uwb_novalid_cnt_ != 0 && !msg->manual_valid)
    {
      return;
    }
    if (use_scan_localiser_ || use_lasermark_ || use_uwb_ || curb_detection_ != nullptr ||
        msg->manual_valid)
    {
      LOG(WARNING) << "submap 3d time stamp:" << msg->header.stamp;
      // TODO: consider how to add uwb marks
      T2 submap3d_lidar_msg;
      if (use_lasermark_ || use_pole_marks_3D_ || curb_detection_ != nullptr)
      {
        mutex_.lock();
        std::vector<LandmarkPose>::iterator it1 = marks_in_odom_.begin();
        std::vector<LandmarkPose>::iterator it2 = marks_in_map_.begin();
        LOG(INFO) << "mark_id: ";
        for (; it1 != marks_in_odom_.end(); it1++)
        {
          cartographer_ros_msgs::LasermarkEntry mark1;
          mark1.id = std::to_string(it1->id);
          mark1.x = it1->pose.position.x;
          mark1.y = it1->pose.position.y;
          mark1.z = it1->pose.position.z;
          submap3d_lidar_msg.landmarks_in_odom.push_back(mark1);
          std::cout << it1->id << " ";
        }
        std::cout << std::endl;

        for (; it2 != marks_in_map_.end(); it2++)
        {
          cartographer_ros_msgs::LasermarkEntry mark2;
          mark2.id = std::to_string(it2->id);
          mark2.x = it2->pose.position.x;
          mark2.y = it2->pose.position.y;
          mark2.z = it2->pose.position.z;
          submap3d_lidar_msg.landmarks_in_map.push_back(mark2);
          //           std::cout << it1->id <<" " ;
        }

        mutex_.unlock();
      }

      if (use_uwb_ && !uwb_datas_.empty())
      {
        Eigen::Vector3d base2map_t;
        try
        {
          geometry_msgs::TransformStamped base2map_tf;
          base2map_tf = tfBuffer_.lookupTransform("map", "base_footprint", ros::Time(0));
          base2map_t = Eigen::Vector3d(base2map_tf.transform.translation.x,
                                       base2map_tf.transform.translation.y,
                                       base2map_tf.transform.translation.z);
        }
        catch (tf::TransformException ex)
        {
          LOG(ERROR) << "Looking for the tf between map and laser_link is timed out.";
        }
        std::map<int, std::map<ros::Time, double>> uwd_id_times;

        ros::Time time = msg->header.stamp;
        int total_size = 0;
        LOG(WARNING) << time << ", " << uwb_datas_.begin()->first << ", "
                     << uwb_datas_.rbegin()->first;
        if ((uwb_datas_.begin()->first - time).toSec() < 0.)
        {
          for (auto it = uwb_datas_.begin(); it != uwb_datas_.end();)
          {
            if (std::fabs((it->first - time).toSec()) < 0.2)
            {
              for (auto it2 : it->second)
              {
                uwd_id_times[static_cast<int>(it2(0))][it->first] = it2(1);
              }
              total_size++;
              ++it;
            }
            else if ((it->first - time).toSec() >= 0.2)
              break;
            else
              it = uwb_datas_.erase(it);
          }

          LOG(WARNING) << total_size << ", " << uwd_id_times.size();
          for (auto it = uwd_id_times.begin(); it != uwd_id_times.end();)
          {
            if (it->second.size() < std::floor(total_size / 2))
            {
              it = uwd_id_times.erase(it);
            }
            else
            {
              for (auto it2 = it->second.begin(); it2 != std::prev(it->second.end()); it2++)
              {
                double det_prev = (time - it2->first).toSec();
                double det_next = (std::next(it2)->first - time).toSec();
                int id = it->first;

                while (uwb_marks_.find(100000 + id) != uwb_marks_.end() &&
                       (base2map_t - uwb_marks_[id]).norm() > 100.)
                {
                  id += 100000;
                }
                if ((base2map_t - uwb_marks_[id]).norm() > 100.) continue;

                double cur_distance = -1.;
                if (det_prev > 0 && det_next >= 0)
                {
                  double det_time = det_prev + det_next;
                  cur_distance = (1.0 - det_prev / det_time) * it2->second +
                                 det_prev / det_time * std::next(it2)->second;
                }
                else if (it2 == std::prev(std::prev(it->second.end())))
                {
                  LOG(WARNING) << det_prev << ", " << det_next << ", " << it2->first << ", " << time
                               << ", " << std::next(it2)->first;
                  cur_distance = std::next(it2)->second + (std::next(it2)->second - it2->second) /
                                                              (det_prev + det_next) * det_next;
                  LOG(WARNING) << it2->second << ", " << std::next(it2)->second << ", "
                               << cur_distance;
                }
                if (cur_distance != -1.)
                {
                  cartographer_ros_msgs::LasermarkEntry mark1, mark2;
                  mark1.id = std::to_string(id);
                  mark1.x = cur_distance;
                  mark1.y = 0.;
                  mark1.z = 0.;
                  submap3d_lidar_msg.landmarks_in_odom.push_back(mark1);

                  mark2.id = std::to_string(id);
                  mark2.x = uwb_marks_[id].x();
                  mark2.y = uwb_marks_[id].y();
                  mark2.z = uwb_marks_[id].z();
                  submap3d_lidar_msg.landmarks_in_map.push_back(mark2);
                  break;
                }
              }

              ++it;
            }
          }
        }
      }
      if (!msg->manual_valid)
        LOG(INFO) << "[" << ros::Time::now() << "] "
                  << "Auto localization with " << submap3d_lidar_msg.landmarks_in_odom.size()
                  << " marks: " << use_scan_localiser_ << " " << use_lasermark_ << " " << use_uwb_;
      else
        LOG(INFO) << "[" << ros::Time::now() << "] "
                  << "Manual localization with " << submap3d_lidar_msg.landmarks_in_odom.size()
                  << " marks: " << use_scan_localiser_ << " " << use_lasermark_ << " " << use_uwb_;

      submap3d_lidar_msg.submap = *msg;
      submap3d_lidar_msg.use_lidar_localization = use_scan_localiser_;
      if (msg->manual_valid) submap3d_lidar_msg.use_lidar_localization = true;

      submap3d_lidar_msg.header.stamp = ros::Time::now();
      submap3d_lidar_msg.debug_flag = debug_flag_;

      if (msg->manual_valid)
        submap3d_lidar_msg.localization_type = "laser_MANUAL";
      else
      {
        if (use_lasermark_ && use_scan_localiser_)
          submap3d_lidar_msg.localization_type = "laser_laserMark";
        else if (use_lasermark_)
          submap3d_lidar_msg.localization_type = "laserMark";
        else if (curb_detection_ != nullptr)
          submap3d_lidar_msg.localization_type = "laser_curb";
        else
          submap3d_lidar_msg.localization_type = "laser_NORMAL";
      }

      submap_lasermark_pub_.publish(submap3d_lidar_msg);
      if (marks_id_.size() > 0)
      {
        LOG(INFO) << "detect lasermarks id : ";
        for (auto &tmp_id : marks_id_)
        {
          std::cout << tmp_id << " ";
        }
        std::cout << std::endl;
      }
      std::vector<LandmarkPose>().swap(marks_in_map_);
      std::vector<LandmarkPose>().swap(marks_in_odom_);
      std::set<int>().swap(marks_id_);
      curb_id_ = 0;
      std::map<std::string, int>::iterator it_map;
      for (it_map = marks_cnt_.begin(); it_map != marks_cnt_.end();)
      {
        it_map = marks_cnt_.erase(it_map);
      }
    }

    if (show_state_print_ != use_scan_localiser_)
    {
      show_state_print_ = use_scan_localiser_;
      std::string state_tmp =
          use_scan_localiser_ ? "use scan localiser!" : "not use scan localiser";
      LOG(WARNING) << "Laser localization state has been changed, now it will " << state_tmp;
    }
  }
};
} // namespace lasermark
} // namespace map_associator
#endif // LASERMARKLOCALIZATION_H
