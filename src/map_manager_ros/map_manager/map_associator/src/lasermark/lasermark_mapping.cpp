/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "lasermark/lasermark_mapping.h"

#include <tf2_eigen/tf2_eigen.h>
// #define MIN_INTENSITY 1500
#define MIN_DISTANCE 0.5
using namespace std;
namespace map_associator
{
namespace lasermark
{
const string ID_PREFIX = "lasermark_";
inline int getMarkNum(const string &id)
{
  uint i = 0;
  int result = 0;
  for (i = 0; i <= id.size() - 1; i++)
  {
    if (id[i] >= '0' && id[i] <= '9')
    {
      result = result * 10 + id[i] - 48;
    }
  }
  return result;
}

LaserMarkMapping::LaserMarkMapping(const ros::NodeHandle &n)
    : LasermarkInterface(n), nh_(n), tfBuffer_(ros::Duration(10.)), tfListener_(tfBuffer_)
{
  landmark_id_ = 1000000;
  string laser_scan_topic = scan_topic_;
  point_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/marker_point", 1);

  string landmark_list_topic_name = "/landmark_poses_list";
  landmark_list_sub_ =
      nh_.subscribe(landmark_list_topic_name, 0, &LaserMarkMapping::HandleLandmarkList, this);
  pole_radius_ = 0.0375;
  ::ros::param::get("~pole_radius", pole_radius_);
  LOG(INFO) << "pole_radius: " << pole_radius_;
  initialized_ = false; //
  pub_cnt_ = 0;
  pub_mark_cnt_ = 0;
  origin_map_max_landmark_id_ = 1000000;
  origin_map_lasermark_size_ = 0;
}

LaserMarkMapping::~LaserMarkMapping() {}

bool LaserMarkMapping::judgeLaserMarker(
    const Eigen::Vector3d &point_in_base, const geometry_msgs::TransformStamped &base2map,
    cartographer_ros_msgs::LandmarkEntry &landmark,
    vector<LasermarkInterface::LandmarkPose> &mark_pose_in_frame)
{
  LandmarkPose temp_pose;
  Eigen::Vector2d point_temp(point_in_base(0), point_in_base(1));
  // 判断当前点 是否在当前帧marker中存在
  if (isNewPointChecked(point_temp, mark_pose_in_frame, temp_pose))
  {
    mark_pose_in_frame.push_back(temp_pose);
  }
  else
    return false;
  Eigen::Affine3d base2map_affine3d;
  tf::transformMsgToEigen(base2map.transform, base2map_affine3d);
  Eigen::Vector3d point_in_map = base2map_affine3d * point_in_base;
  // 判断当前点是否在map marker 中存在，若不存在将赋新id
  if (isNewPointChecked({point_in_map(0), point_in_map(1)}, mark_pose_in_map_, temp_pose))
  {
    landmark.id = /*ID_PREFIX + */ to_string(landmark_id_);
    landmark_id_++;
  }
  else
    landmark.id = to_string(temp_pose.id);
  points_.points.resize(1);
  points_.channels.resize(1);
  points_.channels[0].values.resize(1);
  points_.header.frame_id = "map";

  points_.points[0].x = point_in_map(0);
  points_.points[0].y = point_in_map(1);
  points_.channels[0].values[0] = 255;
  point_pub_.publish(points_);
  // 	Eigen::Affine3d map2base_affine3d;
  // 	Eigen::Affine3d point2map = Eigen::Affine3d::Identity();
  Eigen::Affine3d point2base_pose = Eigen::Affine3d::Identity();
  point2base_pose(0, 3) = point_in_base(0);
  point2base_pose(1, 3) = point_in_base(1);
  point2base_pose(2, 3) = point_in_base(2);
  // 	tf::transformMsgToEigen(map2base.transform,map2base_affine3d);
  // 	point2base_pose = map2base_affine3d * point2map;
  tf::poseEigenToMsg(point2base_pose, landmark.tracking_from_landmark_transform);
  landmark.rotation_weight = 1e-9;
  landmark.translation_weight = translation_weight_;
  return true;
}

void LaserMarkMapping::HandleLaser(const cartographer_ros_msgs::LandmarkListConstPtr &msg,
                                   cartographer_ros_msgs::LandmarkList &landmark_list)
{
  if (initialized_)
  {
    // 降低接受频率
    if (pub_cnt_ < 4)
    {
      ++pub_cnt_;
      return;
    }
    // 等待建图端返回获取到的反射板点及pose
    if (pub_mark_cnt_ > mark_pose_in_map_.size())
    {
      // 			landmark_list = last_landmark_list_;
      // 			laserMark_pub_.publish(last_landmark_list_);
      LOG_EVERY_N(WARNING, 30) << ("wait for /landmark_poses_list")
                               << "\n pub_mark_cnt_: " << pub_mark_cnt_
                               << "\n mark_pose_in_map_.size(): " << mark_pose_in_map_.size()
                               << "\n last_landmark_list_.size(): "
                               << last_landmark_list_.landmarks.size();
      return;
    }
    // 		cartographer_ros_msgs::LandmarkList landmark_list;
    // 		sensor_msgs::LaserScan scan_msg = *msg;
    // 		double angle_increment = msg->angle_increment;
    // 		double angle_min = msg->angle_min;
    geometry_msgs::TransformStamped base2map;
    string link1 = "map";
    // 		string link2 = msg->header.frame_id;
    string link2 = "base_footprint";
    double start_time, end_time;
    start_time = ros::Time::now().toSec();
    end_time = start_time - 1.;
    ros::Rate rate_tmp(1000);
    if (msg->landmarks.empty()) return;
    // 获取base2map tf pose
    while (ros::ok())
    {
      try
      {
        end_time = ros::Time::now().toSec();
        base2map = tfBuffer_.lookupTransform(link1, link2, msg->header.stamp);
        break;
      }
      catch (tf::TransformException ex)
      {
        if (end_time - start_time > 1.0)
        {
          LOG(ERROR) << "Looking for the tf between map and laser_link is timed out.";
          break;
        }
      }
      rate_tmp.sleep();
    }

    if (end_time - start_time > 1)
    {
      LOG_EVERY_N(INFO, 100) << "time loss of getting transform from odom to laser "
                             << end_time - start_time;
      return;
    }
    std::vector<LandmarkPose> mark_pose_in_frame;
    Eigen::Affine3d base2map_affine3d_tmp = tf2::transformToEigen(base2map);
    // scan matched points in map
    //     sensor_msgs::PointCloud2 point_cloud2 = *msg;
    //     pcl::PointCloud<pcl::PointXYZI>::Ptr pcl_cloud(new
    //     pcl::PointCloud<pcl::PointXYZI>); pcl::moveFromROSMsg(point_cloud2,
    //     *pcl_cloud);
    std::vector<std::vector<Eigen::Vector3f>> init_marks;
    int cur_id = stoi(msg->landmarks[0].id);
    std::vector<Eigen::Vector3f> candidate;

    // 遍历检测到的反射板点，并根据id 进行归类到candidate
    for (const cartographer_ros_msgs::LandmarkEntry &it : msg->landmarks)
    {
      if (cur_id != stoi(it.id))
      {
        cur_id = stoi(it.id);
        init_marks.push_back(candidate);
        std::vector<Eigen::Vector3f>().swap(candidate);
      }
      else
      {
        candidate.push_back({it.tracking_from_landmark_transform.position.x,
                             it.tracking_from_landmark_transform.position.y,
                             it.tracking_from_landmark_transform.position.z});
      }
    }
    if (!candidate.empty()) init_marks.push_back(candidate);
    LOG(INFO) << init_marks.size();
    // 进一步判定反射板点是否有效
    std::vector<std::vector<Eigen::Vector3f>> marks_heap = refineLasermarks(init_marks);
    // 反光柱模式， 会计算 圆心位置
    if (use_laser_pole_marks_2D_)
    {
      for (std::vector<Eigen::Vector3f> points : marks_heap)
      {
        cartographer_ros_msgs::LandmarkEntry landmark;
        const auto last_mark_id = landmark_id_;
        if (judgeLaserMarker(points[0].cast<double>(), base2map, landmark, mark_pose_in_frame))
        {
          for (const auto &p : points)
          {
            geometry_msgs::Point32 point32;
            point32.x = p.x();
            point32.y = p.y();
            point32.z = p.z();
            landmark.observation_points_in_base.push_back(point32);
          }
          if (last_mark_id != landmark_id_)
          {
            Eigen::Vector3f center = points[points.size() / 2];
            float norm_xy = sqrt(center(0) * center(0) + center(1) * center(1)) + pole_radius_;
            float yaw = atan2(center(1), center(0));
            LOG(WARNING) << "old: " << center.transpose();
            center(0) = norm_xy * cos(yaw);
            center(1) = norm_xy * sin(yaw);
            LOG(WARNING) << "new: " << center.transpose();
            Eigen::Affine3d point2base_pose = Eigen::Affine3d::Identity();
            point2base_pose(0, 3) = center(0);
            point2base_pose(1, 3) = center(1);
            point2base_pose(2, 3) = center(2);
            tf::poseEigenToMsg(point2base_pose, landmark.tracking_from_landmark_transform);
          }
          landmark.pole_radius = pole_radius_;
          landmark_list.landmarks.push_back(landmark);
        }
      }
    }
    else
    {
      // 反光板模式， 会将中间值作为反射板位置
      for (std::vector<Eigen::Vector3f> points : marks_heap)
      {
        vector<Eigen::Vector3f> refine_points = prescreenPoints(points);
        for (auto &point_in_base : refine_points)
        {
          cartographer_ros_msgs::LandmarkEntry landmark;
          if (judgeLaserMarker(point_in_base.cast<double>(), base2map, landmark,
                               mark_pose_in_frame))
          {
            landmark_list.landmarks.push_back(landmark);
          }
        }
      }
    }

    if (landmark_list.landmarks.size() > 0)
    {
      pub_cnt_ = 0;
      // 			laserMark_pub_.publish(landmark_list);
      LOG(INFO) << "landmark_list.landmarks size: " << landmark_list.landmarks.size();
      pub_mark_cnt_ = landmark_id_ - origin_map_max_landmark_id_ + origin_map_lasermark_size_;

      last_landmark_list_ = landmark_list;
    }
  }
}

void LaserMarkMapping::HandleLandmarkList(const visualization_msgs::MarkerArray::Ptr &msg)
{
  mutex_.try_lock();
  visualization_msgs::MarkerArray list = *msg;
  int list_size = list.markers.size();
  for (int i = 0; i < list_size; i++)
  {
    visualization_msgs::Marker temp_mark = list.markers[i];
    // TODO id  is not real published id
    if (temp_mark.ns != "Lasermarks") continue;
    int id = stoi(temp_mark.text);
    // 		int int_id = getMarkNum(id);
    LandmarkPose temp_pose;
    temp_pose.id = id;
    temp_pose.time_stamp = temp_mark.header.stamp;
    temp_pose.pose = temp_mark.pose;
    std::vector<LandmarkPose>::iterator it;
    it = mark_pose_in_map_.begin();
    for (; it != mark_pose_in_map_.end(); it++)
    {
      if (id == it->id) break;
    }
    if (it == mark_pose_in_map_.end())
      mark_pose_in_map_.push_back(temp_pose);
    else
      it->pose = temp_pose.pose;
  }
  mutex_.unlock();
  initialized_ = true;
}

void LaserMarkMapping::addExistMarks(
    const vector<LasermarkInterface::LandmarkPose> &mark_pose_in_map)
{
  int max_lasermark_id = 0;
  for (int i = 0; i < mark_pose_in_map.size(); i++)
  {
    mark_pose_in_map_.push_back(mark_pose_in_map[i]);

    mark_pose_in_map_[i].id = mark_pose_in_map[i].id + 1000000;
    LOG(WARNING) << "landmark id: " << mark_pose_in_map_[i].id;
    if (mark_pose_in_map_[i].id > max_lasermark_id) max_lasermark_id = mark_pose_in_map_[i].id;
  }
  if (!mark_pose_in_map.empty())
  {
    origin_map_lasermark_size_ = mark_pose_in_map_.size();
    origin_map_max_landmark_id_ = max_lasermark_id + 1;
    landmark_id_ = origin_map_max_landmark_id_;
  }
  LOG(WARNING) << "landmark_id_: " << landmark_id_;
}

void LaserMarkMapping::clear()
{
  laser_sub_.shutdown();
  landmark_list_sub_.shutdown();
  laserMark_pub_.shutdown();
  point_pub_.shutdown();
  tfBuffer_.clear();
}
} // namespace lasermark
} // namespace map_associator
