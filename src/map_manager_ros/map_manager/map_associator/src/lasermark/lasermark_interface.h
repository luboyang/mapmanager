/*
 * @Date: 2023-02-24 18:34:12
 * @Author: Cyy && cyycyag@163.com
 * @LastEditors: Cyy && cyycyag@163.com
 * @LastEditTime: 2023-03-08 11:44:59
 * @FilePath: /map_manager/map_associator/src/lasermark/lasermark_interface.h
 * @Description: 反射板接口
 */

#ifndef LASERMARKINTERFACE_H
#define LASERMARKINTERFACE_H

#include <cartographer_ros_msgs/LandmarkList.h>
#include <geometry_msgs/TransformStamped.h>
#include <glog/logging.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "sensor_msgs/point_cloud2_iterator.h"
namespace map_associator
{
namespace lasermark
{
struct landmarkListsSort
{
  bool operator()(const cartographer_ros_msgs::LandmarkList &list1,
                  const cartographer_ros_msgs::LandmarkList &list2)
  {
    ros::Duration dur = list1.header.stamp - list2.header.stamp;
    if (dur.toSec() > 0)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
};

class LasermarkInterface
{
  public:
  struct LandmarkPose
  {
    ros::Time time_stamp;
    int id; // id string 2 int
    geometry_msgs::Pose pose;
    float pole_radius;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  public:
  LasermarkInterface(const ros::NodeHandle &n);
  virtual ~LasermarkInterface();
  virtual void clear() = 0;
  /**
   * @description: 变换坐标 T*t
   * @param {Vector3d} &t2 t
   * @param {TransformStamped} &transform T
   * @return {Eigen::Vector3d} 返回T*t的结果
   */
  Eigen::Vector3d getNewCoor(const Eigen::Vector3d &t2,
                             const geometry_msgs::TransformStamped &transform);

  /**
   * @description: 变换坐标 T*t
   * @param {Vector3d} &t2 t
   * @param {Affine3d} &transform T
   * @return {*}返回T*t的结果
   */
  Eigen::Vector3d getNewCoor(const Eigen::Vector3d &t2, const Eigen::Affine3d &transform);

  /**
   * @description: 判断该点是否已经在poses_in_fix 中存在
   * @param {Vector2d} &new_coor
   * @param {vector<LandmarkPose>} &poses_in_fix
   * @param {LandmarkPose} &pose
   * @return {*}
   */
  bool isNewPointChecked(const Eigen::Vector2d &new_coor, std::vector<LandmarkPose> &poses_in_fix,
                         LandmarkPose &pose);
  std::vector<std::vector<Eigen::Vector3f>>
  refineLasermarks(const std::vector<std::vector<Eigen::Vector3f>> &marks);
  std::vector<Eigen::Vector3f> getPointFromFile(const std::string &path);
  ros::NodeHandle nh_;
  double min_distance_, max_distance_;
  std::string scan_topic_;
  int min_neighbour_points_size_;
  float min_distance_between_points_;
  double translation_weight_;
  //   std::shared_ptr<LasermarkPerception> lasermark_perception_;
  bool use_laser_pole_marks_2D_;

  /**
   * @description: 预筛选反射板点
   * @param {vector<Eigen::Matrix<FloatType, 3, 1>>} &candidates 反射板点云
   * @return {*} 返回反射大体位置
   */
  template <class FloatType>
  std::vector<Eigen::Matrix<FloatType, 3, 1>>
  prescreenPoints(const std::vector<Eigen::Matrix<FloatType, 3, 1>> &candidates)
  {
    Eigen::Matrix<FloatType, 3, 1> first_point = candidates[0];
    std::vector<Eigen::Matrix<FloatType, 3, 1>> results;
    std::vector<Eigen::Matrix<FloatType, 3, 1>> refine_candidates;
    refine_candidates.push_back(first_point);
    for (int i = 1; i != candidates.size(); ++i)
    {
      Eigen::Matrix<FloatType, 3, 1> it = candidates[i];
      // 将距离接近的点放到 refine_candidates 中
      if ((first_point - it).norm() < min_distance_)
      {
        refine_candidates.push_back(it);
        first_point = it;
      }
      else
      {
        // 聚集的反射板个数要大于阈值，并且聚类的最大反射板点距离要小于某阈值，此时将聚类的反射板中点放入result中作为反射板位置
        if (refine_candidates.size() > min_neighbour_points_size_)
        {
          if ((refine_candidates.front() - refine_candidates.back()).norm() < min_distance_)
          {
            int cnt = refine_candidates.size() / 2;
            results.push_back(refine_candidates[cnt]);
          }
        }
        first_point = it;
        std::vector<Eigen::Matrix<FloatType, 3, 1>>().swap(refine_candidates);
        refine_candidates.push_back(first_point);
      }
    }
    if (refine_candidates.size() > min_neighbour_points_size_) // not use else in upper for()
    {
      if ((refine_candidates.front() - refine_candidates.back()).norm() < min_distance_)
      {
        int cnt = refine_candidates.size() / 2;
        results.push_back(refine_candidates[cnt]);
      }
    }
    return results;
  }
};
} // namespace lasermark
} // namespace map_associator
#endif // LASERMARKINTERFACE_H
