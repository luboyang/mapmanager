/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#define MIN_DISTANCE 0.5
#include "lasermark/lasermark_localization.h"

#include <assert.h>
#include <math.h>

#include <chrono>
#include <thread>

#include "cartographer/mapping/probability_values.h"
#include "cartographer_ros_msgs/SubmapLioSam.h"

using namespace std;
using namespace Eigen;
using namespace cv;
namespace map_associator
{
namespace lasermark
{
const string ID_PREFIX = "lasermark_";
inline int getMarkNum(const string &id)
{
  uint i = 0;
  int result = 0;
  for (i = 0; i <= id.size() - 1; i++)
  {
    if (id[i] >= '0' && id[i] <= '9')
    {
      result = result * 10 + id[i] - 48;
    }
  }
  return result;
}

LasermarkLocalization::LasermarkLocalization(const ros::NodeHandle &n)
    : LasermarkInterface(n), nh_(n), tfBuffer_(ros::Duration(60.)), tfListener_(tfBuffer_),
      initialized_(false), matching_point_thresh_finished_(0.3),
      matching_point_thresh_unfinished_(0.5), use_uwb_(false), uwb_novalid_cnt_(0), has_uwb_(false),
      has_gps_info_(false), use_pole_marks_3D_(false), zero_velocity_flag_(false)
{
  pole_detection_.reset(new PoleLikeDetection(nh_));
  is_3d_ = false;
  if (!ros::param::get("~is_3d", is_3d_))
  {
    ROS_WARN("Can not get param of ~is_3d!");
    is_3d_ = false;
  }

  point_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/marker_point", 1);
  string submap_local_topic = "/submap_local";
  //  string landmark_pose_list_topic = "/landmark_poses_list";
  string pub_topic_ = "/submap_lasermark";
  if (is_3d_)
  {
    bool use_lio = nh_.param<bool>("/lio_sam_odom", true);
    pub_topic_ = "/submap3D_lidarmark";
    if (!use_lio)
    {
      submap_local_topic = "/submap3D_local";
      submap_local_sub_ =
          nh_.subscribe(submap_local_topic, 10, &LasermarkLocalization::handleSubmap3D, this);
      submap_lasermark_pub_ =
          nh_.advertise<cartographer_ros_msgs::Submap3DLidarmark>(pub_topic_, 20);
    }
    else
    {
      submap_local_topic = "/submap3D_local_lio";
      submap_local_sub_ =
          nh_.subscribe(submap_local_topic, 10, &LasermarkLocalization::handleSubmap3DLio, this);

      submap_lasermark_pub_ = nh_.advertise<cartographer_ros_msgs::SubmapLioSam>(pub_topic_, 20);
    }

    LOG(INFO) << "use 3d: " << (use_lio ? "lio-sam." : "lego-loam.");
  }
  else
  {
    submap_local_sub_ =
        nh_.subscribe(submap_local_topic, 10, &LasermarkLocalization::handleSubmap, this);
    submap_lasermark_pub_ = nh_.advertise<cartographer_ros_msgs::SubmapLasermark>(pub_topic_, 20);
  }
  use_precise_localization_ = false;
  use_scan_localiser_ = true;
  use_lasermark_ = false;
  show_state_print_ = use_scan_localiser_;
  use_strip_detect_ = false;
  use_curb_ = false;
  //  string map_folder = map_name + "/landmark.xml";
  //  LOG(INFO) << "path : " << map_folder ;
  //  getPoseInMap(map_folder);
  //  ros::param::get("/cartographer_node/use_lasermarks", is_use_lasermark_);
  //  LOG(INFO) << "is_use_lasermark_ " <<is_use_lasermark_ ;
  debug_flag_ = false;
  if (!ros::param::get("~debugFlag", debug_flag_))
  {
    LOG(WARNING) << ("Can't get debug flag!");
  }

  if (!::ros::param::get("/vtr/path", map_path_))
  {
    LOG(ERROR) << ("Please set map name! ");
    map_path_ = "test";
  }
  bool_img_ = imread(map_path_ + "/bool_image.png", cv::IMREAD_UNCHANGED);
  if (bool_img_.empty())
  {
    bool_img_ = Mat(1, 1, CV_8UC1);
  }

  // TODO: tmp process method to get resolution and max_box
  if (!is_3d_)
  {
    boost::property_tree::ptree pt;
    boost::property_tree::read_xml(map_path_ + "/frames/0" + "/data.xml", pt);
    resolution_ = pt.get<double>("probability_grid.resolution");
    string data = pt.get<std::string>("probability_grid.max");
    sscanf(data.c_str(), "%lf %lf", &max_box_[0], &max_box_[1]);
    LOG(INFO) << "resolution: " << resolution_ << "\t "
              << "max_box: " << max_box_[0] << "," << max_box_[1];
  }
  strip_points_in_map_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/strip_points_in_map", 10);
  //   point_cloud_temp_pub1_ = nh_.advertise<sensor_msgs::PointCloud>("/temp0",
  //   10); point_cloud_temp_pub2_ =
  //   nh_.advertise<sensor_msgs::PointCloud>("/temp1", 10);
  use_visual_points_ = false;
  use_pole_marks_3D_ = false;
  setUseDynamicDetect(false);

  LOG(INFO) << "matching_point_thresh_finished:" << matching_point_thresh_finished_
            << "\t matching_point_thresh_unfinished:" << matching_point_thresh_unfinished_;
  pole_marker_pub_ = nh_.advertise<sensor_msgs::PointCloud>("submap_pole_marks", 10);
  submap_cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("submap_cloud", 10);
  detect_curb_cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud>("cur_curb_points", 10);
  curb_cloud_in_map_pub_ = nh_.advertise<sensor_msgs::PointCloud>("curb_points_in_map", 10);
  curb_id_ = 0;
}

LasermarkLocalization::~LasermarkLocalization() {}
void LasermarkLocalization::setParam()
{
  if (initialized_) return;
  initialized_ = true;

  tf::StampedTransform base_laser_transform;
  tf::TransformListener listener;
  // 为区分二维码id， 反射板id起始值为 1000000.
  landmark_id_ = 1000000;

  if (!landmark_pose_list_.empty())
  {
    for (auto it : landmark_pose_list_)
    {
      if (has_uwb_)
        uwb_marks_[it.id] =
            Eigen::Vector3d(it.pose.position.x, it.pose.position.y, it.pose.position.z);
      pole_radius_with_id_[it.id] = it.pole_radius;
    }
  }
  LOG(WARNING) << "has_uwb_: " << has_uwb_
               << ",  landmark_pose_list size: " << landmark_pose_list_.size();
}

void LasermarkLocalization::setUseScanLocaliser(const bool &use_scan_localiser)
{
  use_scan_localiser_ = use_scan_localiser;
}

void LasermarkLocalization::setUseLasermark(const bool &use_lasermark)
{
  use_lasermark_ = use_lasermark;
}

void LasermarkLocalization::setUseUwb(const bool &use_uwb)
{
  use_uwb_ = use_uwb;
  if (use_uwb_)
  {
    uwb_sub_ = nh_.subscribe("/filter_uwb_datas", 1, &LasermarkLocalization::handleUwbPoints, this);
    if (uwb_marks_.empty()) setParam();
  }
  else
    uwb_sub_.shutdown();
}

void LasermarkLocalization::setUsePreciseLocalization(const bool &use_precise_localization)
{
  use_precise_localization_ = use_precise_localization;
}

void LasermarkLocalization::setUseStripDetect(const bool &use_strip_detect)
{
  use_strip_detect_ = use_strip_detect;
}

void LasermarkLocalization::setUseDynamicDetect(const bool &use_dynamic_detect)
{
  std::unique_lock<std::mutex> lk(dynamic_detect_mutex_);
  if (use_dynamic_detect)
  {
    dynamic_free_space_image_ = imread(map_path_ + "/free_space.png", CV_8UC1);
    if (dynamic_free_space_image_.empty())
    {
      LOG(WARNING) << map_path_ + "/free_space.png empty!";
      //       return;
    }
    dynamic_detect_sub_ =
        nh_.subscribe("/scan_filtered", 100, &LasermarkLocalization::handleScanFilter, this);
    string odom_topic = "/emma_odom";
    ::ros::param::get("/algo/property/motion_odom_topic", odom_topic);
    LOG(INFO) << "odom topic: " << odom_topic;
    wheel_odom_sub_ = nh_.subscribe(odom_topic, 2, &LasermarkLocalization::handleWheelOdom, this);
    dynamic_detect_pub_ = nh_.advertise<sensor_msgs::LaserScan>("/scan_filtered_dynamic", 10);
  }
  else
  {
    dynamic_detect_pub_.shutdown();
    dynamic_detect_sub_.shutdown();
    wheel_odom_sub_.shutdown();
  }
}

void LasermarkLocalization::reloadFreeSpaceImage()
{
  dynamic_free_space_image_ = imread(map_path_ + "/free_space.png", CV_8UC1);
  if (dynamic_free_space_image_.empty())
  {
    LOG(WARNING) << map_path_ + "/free_space.png empty!";
  }
}

void LasermarkLocalization::setUseCurb(const bool &flag)
{
  if (!is_3d_) return;
  curb_detection_.reset();
  kdtree_curb_.reset();
  curb_points_.reset();
  if (flag)
  {
    // load curbs
    kdtree_curb_.reset(new pcl::KdTreeFLANN<pcl::PointXYZ>());
    curb_points_.reset(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<Eigen::Vector3f> points;
    auto points1 = getPointFromFile(map_path_ + "/curbs.txt");
    //     auto points2 = getPointFromFile(map_path_ + "/curb_right.txt");
    points.insert(points.end(), points1.begin(), points1.end());
    //     points.insert(points.end(), points2.begin(), points2.end());
    curb_points_->points.reserve(points.size());
    for (const auto &it : points)
    {
      pcl::PointXYZ p;
      p.x = it.x();
      p.y = it.y();
      p.z = it.z();
      curb_points_->points.push_back(p);
    }
    LOG(INFO) << "curb points size: " << curb_points_->size();
    kdtree_curb_->setInputCloud(curb_points_);
    curb_detection_.reset(new curb::curbDetector);
  }
}

void LasermarkLocalization::handleSubmap(const cartographer_ros_msgs::SubmapsPtr &msg)
{
  try
  {
    if (use_scan_localiser_ || use_lasermark_ || use_visual_points_ || use_strip_detect_)
    {
      cartographer_ros_msgs::Submaps submap_temp = *msg;
      cartographer_ros_msgs::SubmapLasermark submap_laser_msg;
      // 		ros::Time crop_time = submap_temp.header.stamp;
      // 	cropLasermark(crop_time);
      LOG(INFO) << "use_scan_localiser: " << use_scan_localiser_
                << "\t use_lasermark:" << use_lasermark_
                << "\t use_visual_points:" << use_visual_points_
                << "\t use_strip_detect:" << use_strip_detect_;
      if (use_lasermark_ || use_visual_points_)
      {
        std::unique_lock<std::mutex> lk(mutex_);
        std::vector<LandmarkPose>::iterator it1 = marks_in_odom_.begin();
        std::vector<LandmarkPose>::iterator it2 = marks_in_map_.begin();
        //       LOG(WARNING) << "marks_in_map_ size:" << marks_in_map_.size();
        sensor_msgs::PointCloud points;
        points.points.resize(marks_in_odom_.size());
        points.channels.resize(1);
        points.channels[0].values.resize(1);
        points.header.frame_id = "odom";
        uint ii = 0;
        for (; it1 != marks_in_odom_.end(); it1++, it2++)
        {
          cartographer_ros_msgs::LasermarkEntry mark1, mark2;
          mark1.id = it1->id;
          mark1.x = it1->pose.position.x;
          mark1.y = it1->pose.position.y;
          mark1.z = 0.;
          mark1.pole_radius = it1->pole_radius;
          submap_laser_msg.landmarks_in_odom.push_back(mark1);

          mark2.id = it2->id;
          mark2.x = it2->pose.position.x;
          mark2.y = it2->pose.position.y;
          mark2.z = 0.;
          mark2.pole_radius = it2->pole_radius;
          submap_laser_msg.landmarks_in_map.push_back(mark2);
          points.points[ii].x = it1->pose.position.x;
          points.points[ii].y = it1->pose.position.y;
          ii++;
        }
        points.channels[0].values[0] = 255;
        point_pub_.publish(points);
      }

      cartographer_ros_msgs::PointCloudSubmap large_submap, little_submap;
      if (use_scan_localiser_)
      {
        float matching_point_thresh;
        if (submap_temp.point_cloud_finished)
          matching_point_thresh = matching_point_thresh_finished_;
        else
          matching_point_thresh = matching_point_thresh_unfinished_;
        sensor_msgs::PointCloud large_point_cloud;
        try
        {
          geometry_msgs::TransformStamped odom2map_tf;
          odom2map_tf = tfBuffer_.lookupTransform("map", "odom", ros::Time(0));
          Eigen::Affine3d odom2map;
          tf::transformMsgToEigen(odom2map_tf.transform, odom2map);
          large_point_cloud =
              submap2DToPointCloud(submap_temp.submap, matching_point_thresh, true, odom2map);
          sensor_msgs::PointCloud &little_point_cloud = submap_temp.point_cloud;
          std::vector<geometry_msgs::Point32>::iterator it_points =
              little_point_cloud.points.begin();

          //         sensor_msgs::PointCloud temp_points, temp_points2;
          //         temp_points.header.stamp = msg->header.stamp;
          //         temp_points.header.frame_id = "map";
          //         temp_points2 = temp_points;
          Eigen::Affine3d base2local;
          tf::poseMsgToEigen(submap_temp.point_cloud_local_pose, base2local);
          for (int i = 0; it_points != little_point_cloud.points.end(); i++)
          {
            Eigen::Vector3d pointInBase(it_points->x, it_points->y, it_points->z);
            Eigen::Vector3d pointInMap = odom2map * base2local * pointInBase;
            //           geometry_msgs::Point32 temp_point;
            //           temp_point.x = pointInMap(0);
            //           temp_point.y = pointInMap(1);
            //           temp_point.z = pointInMap(2);
            //           temp_points.points.push_back(temp_point);
            if (!isPointValid(bool_img_, pointInMap))
              little_point_cloud.points.erase(it_points);
            else
            {
              //             temp_points2.points.push_back(temp_point);
              it_points++;
            }
          }
          //         point_cloud_temp_pub1_.publish(temp_points2);
          //         point_cloud_temp_pub2_.publish(temp_points);
        }
        catch (tf::TransformException ex)
        {
          large_point_cloud =
              submap2DToPointCloud(submap_temp.submap, matching_point_thresh, false);
          LOG(ERROR) << "Looking for the tf between map and laser_link is timed out.";
        }
        large_submap.header = submap_temp.submap.header;
        large_submap.point_cloud_frame_cnt = submap_temp.submap.num_range_data;
        large_submap.point_cloud_local_pose = submap_temp.submap.pose;
        large_submap.point_cloud_in_tracking = large_point_cloud;

        little_submap.header = submap_temp.header;
        little_submap.point_cloud_frame_cnt = submap_temp.point_cloud_frame_cnt;
        little_submap.point_cloud_local_pose = submap_temp.point_cloud_local_pose;
        little_submap.point_cloud_in_tracking = submap_temp.point_cloud;
      }
      else
      {
        large_submap.header = submap_temp.submap.header;
        large_submap.point_cloud_frame_cnt = 0;
        large_submap.point_cloud_local_pose = submap_temp.submap.pose;

        little_submap.header = submap_temp.header;
        little_submap.point_cloud_frame_cnt = 0;
        little_submap.point_cloud_local_pose = submap_temp.point_cloud_local_pose;
      }
      submap_laser_msg.submaps.push_back(large_submap);
      submap_laser_msg.submaps.push_back(little_submap);
      submap_laser_msg.point_cloud_finished = submap_temp.point_cloud_finished;

      if (use_precise_localization_) submap_laser_msg.submaps[0].point_cloud_frame_cnt = 0;

      submap_laser_msg.header.stamp = ros::Time::now();
      submap_laser_msg.debug_flag = debug_flag_;

      if (!submap_temp.point_cloud_finished)
        submap_laser_msg.localization_type = "laser_MANUAL";
      else
      {
        if (use_lasermark_ && use_scan_localiser_)
          submap_laser_msg.localization_type = "laser_laserMark";
        else if (use_lasermark_)
          submap_laser_msg.localization_type = "laserMark";
        else if (use_precise_localization_)
          submap_laser_msg.localization_type = "laser_precise";
        else
          submap_laser_msg.localization_type = "laser_NORMAL";
      }

      if (submap_temp.point_cloud_finished)
        LOG(INFO) << "[" << ros::Time::now() << "] "
                  << "Auto localization with " << submap_laser_msg.landmarks_in_odom.size()
                  << " marks, " << (use_strip_detect_ ? strip_detect_points_.points.size() : 0)
                  << " line_points!";
      else
        LOG(INFO) << "[" << ros::Time::now() << "] "
                  << "Manual localization with " << submap_laser_msg.landmarks_in_odom.size()
                  << " marks!";

      cartographer_ros_msgs::PointCloudSubmap line_detect_submap;
      if (use_strip_detect_)
      {
        line_detect_submap.header = strip_detect_points_.header;
        line_detect_submap.point_cloud_local_pose = last_strip_detect_base2odom_;
        line_detect_submap.point_cloud_in_tracking = strip_detect_points_;
        line_detect_submap.point_cloud_frame_cnt = strip_detect_points_.points.size();
        if (!strip_detect_points_.points.empty()) strip_detect_points_.points.clear();
      }
      submap_laser_msg.line_detect_submap = line_detect_submap;

      submap_lasermark_pub_.publish(submap_laser_msg);
      if (marks_id_.size() > 0)
      {
        LOG(INFO) << "detect lasermarks id : ";
        for (auto &tmp_id : marks_id_)
        {
          std::cout << tmp_id << " ";
        }
        std::cout << endl;
      }
      std::vector<LandmarkPose>().swap(marks_in_map_);
      std::vector<LandmarkPose>().swap(marks_in_odom_);
      std::set<int>().swap(marks_id_);

      map<string, int>::iterator it_map;
      for (it_map = marks_cnt_.begin(); it_map != marks_cnt_.end();)
      {
        it_map = marks_cnt_.erase(it_map);
      }
    }

    if (show_state_print_ != use_scan_localiser_)
    {
      show_state_print_ = use_scan_localiser_;
      std::string state_tmp =
          use_scan_localiser_ ? "use scan localiser!" : "not use scan localiser";
      LOG(WARNING) << "Laser localization state has been changed, now it will " << state_tmp;
    }
  }
  catch (const std::exception &e)
  {
    LOG(WARNING) << e.what();
  }
}

void LasermarkLocalization::handleSubmap3DLio(const cartographer_ros_msgs::CloudWithPosePtr &msg)
{
  if (use_pole_marks_3D_)
  {
    sensor_msgs::PointCloud2 tmp_cloud_msg = msg->clouds[0];
    tmp_cloud_msg.header.stamp = msg->header.stamp;
    tmp_cloud_msg.header.frame_id = "base_footprint";
    submap_cloud_pub_.publish(tmp_cloud_msg);

    sensor_msgs::PointCloud marks;
    for (LandmarkPose it : marks_in_odom_)
    {
      geometry_msgs::Point32 p;
      p.x = it.pose.position.x;
      p.y = it.pose.position.y;
      p.z = 1.0;
      marks.points.push_back(p);
    }
    marks.header.stamp = msg->header.stamp;
    marks.header.frame_id = "odom";
    pole_marker_pub_.publish(marks);
  }
  handleSubmap3DMsg<cartographer_ros_msgs::CloudWithPosePtr, cartographer_ros_msgs::SubmapLioSam>(
      msg);
}

void LasermarkLocalization::handleSubmap3D(const cartographer_ros_msgs::Submap3DPtr &msg)
{
  handleSubmap3DMsg<cartographer_ros_msgs::Submap3DPtr, cartographer_ros_msgs::Submap3DLidarmark>(
      msg);
}

void LasermarkLocalization::handleScanMatchedPoints(
    const cartographer_ros_msgs::LandmarkListConstPtr &msg,
    cartographer_ros_msgs::LandmarkList &landmark_list)
{
  // get tf (laser to map, laser to odom)
  //   	LOG(WARNING) <<use_lasermark_<< " landmark_pose_list_.size(): "
  //   <<landmark_pose_list_.size();
  string map_link = "map";
  string odom_link = "odom";
  string base_link = "base_footprint";
  if (use_lasermark_ && landmark_pose_list_.size() != 0 && !use_pole_marks_3D_)
  {
    geometry_msgs::TransformStamped odom2map_tf, odom2base_tf;
    Eigen::Affine3d odom2map;

    double start_time, end_time;
    start_time = ros::Time::now().toSec();
    end_time = start_time - 1.;
    ros::Rate rate_tmp(100);
    while (ros::ok())
    {
      try
      {
        end_time = ros::Time::now().toSec();
        odom2map_tf = tfBuffer_.lookupTransform(map_link, odom_link, msg->header.stamp);
        tf::transformMsgToEigen(odom2map_tf.transform, odom2map);
        break;
      }
      catch (tf::TransformException ex)
      {
        if (end_time - start_time > 1.0)
        {
          LOG(ERROR) << "Looking for the tf between map and odom is timed out.";
          break;
        }
      }
      rate_tmp.sleep();
    }
    if (end_time - start_time > 1) return;
    start_time = ros::Time::now().toSec();
    while (ros::ok())
    {
      try
      {
        end_time = ros::Time::now().toSec();
        odom2base_tf = tfBuffer_.lookupTransform(base_link, odom_link, msg->header.stamp);
        break;
      }
      catch (tf::TransformException ex)
      {
        if (end_time - start_time > 1.0)
        {
          LOG(ERROR) << "Looking for the tf between odom and laser_link is timed out.";
          break;
        }
      }
      rate_tmp.sleep();
    }
    if (end_time - start_time > 1) return;
    // screen out

    std::unique_lock<std::mutex> lk(mutex_);
    int range_size = msg->landmarks.size();
    if (range_size > 0)
    {
      sensor_msgs::PointCloud points;
      points.channels.resize(1);
      points.channels[0].values.resize(1);
      points.header.frame_id = "map";
      Eigen::Affine3d odom2base;
      tf::transformMsgToEigen(odom2base_tf.transform, odom2base);
      Eigen::Affine3f odom2basef = odom2base.cast<float>();
      std::vector<std::vector<Eigen::Vector3f>> init_marks;
      int cur_id = stoi(msg->landmarks[0].id);
      std::vector<Eigen::Vector3f> candidate;
      for (const cartographer_ros_msgs::LandmarkEntry &it : msg->landmarks)
      {
        if (cur_id != stoi(it.id))
        {
          cur_id = stoi(it.id);
          init_marks.push_back(candidate);
          std::vector<Eigen::Vector3f>().swap(candidate);

          Eigen::Vector3f tmp2{it.tracking_from_landmark_transform.position.x,
                               it.tracking_from_landmark_transform.position.y,
                               it.tracking_from_landmark_transform.position.z};

          candidate.push_back(tmp2);
        }
        else
        {
          Eigen::Vector3f tmp2{it.tracking_from_landmark_transform.position.x,
                               it.tracking_from_landmark_transform.position.y,
                               it.tracking_from_landmark_transform.position.z};

          candidate.push_back(tmp2);
        }
      }
      init_marks.push_back(candidate);
      //       LOG(INFO)<<"landmarks size: " << msg->landmarks.size();
      //       for(auto  it : init_marks)
      //       {
      //         LOG(INFO) << it.size();
      //       }
      std::vector<std::vector<Eigen::Vector3f>> marks_heap = refineLasermarks(init_marks);
      //       LOG(INFO)<<"landmarks size: " << msg->landmarks.size() <<" " <<
      //       init_marks.size() <<" " <<marks_heap.size();

      for (const auto &it : marks_heap)
      {
        for (const auto &it2 : it)
        {
          LandmarkPose temp_pose;
          Eigen::Vector3d pose_in_base = it2.cast<double>();
          Eigen::Vector3d point_in_odom = getNewCoor(pose_in_base, odom2base.inverse());
          Eigen::Vector3d point2map_coor = getNewCoor(point_in_odom, odom2map_tf);
          if (!isNewPointChecked({point2map_coor(0), point2map_coor(1)}, landmark_pose_list_,
                                 temp_pose))
          {
            LandmarkPoses poses;
            poses.time_stamp = temp_pose.time_stamp;
            poses.id = temp_pose.id;
            poses.pose_in_map =
                Eigen::Vector3d(temp_pose.pose.position.x, temp_pose.pose.position.y, 0);
            poses.pose_in_odom = point_in_odom;
            poses.pose_in_base = pose_in_base;
            poses.distance = (poses.pose_in_map - point2map_coor).norm();
            if (!location_lasermark_ids_.empty() &&
                location_lasermark_ids_.find(poses.id) == location_lasermark_ids_.end())
              continue;
            updateMarks(poses.id, poses.pose_in_odom, poses.time_stamp,
                        marks_in_odom_); // id string 2 int
            updateMarks(poses.id, poses.pose_in_map, poses.time_stamp,
                        marks_in_map_); // id string 2 int
            marks_id_.insert(poses.id);
            if (marks_in_odom_.size() >= 10000)
            {
              marks_in_odom_.erase(marks_in_odom_.begin());
              marks_in_map_.erase(marks_in_map_.begin());
            }

            // pub to show
            // scan_point_in_base
            cartographer_ros_msgs::LandmarkEntry landmark_entry;
            landmark_entry.id = to_string(landmark_id_ + poses.id);
            //                 tf::poseEigenToMsg(point2base,
            //                                    landmark_entry.tracking_from_landmark_transform);
            landmark_entry.tracking_from_landmark_transform.position.x = poses.pose_in_base(0);
            landmark_entry.tracking_from_landmark_transform.position.y = poses.pose_in_base(1);
            landmark_entry.tracking_from_landmark_transform.position.z = poses.pose_in_base(2);
            landmark_entry.rotation_weight = 1e-9;
            landmark_entry.translation_weight = translation_weight_;
            landmark_list.header.stamp = msg->header.stamp;
            landmark_list.landmarks.push_back(landmark_entry);
          }
          geometry_msgs::Point32 temp_point;
          temp_point.x = point2map_coor(0);
          temp_point.y = point2map_coor(1);
          temp_point.z = point2map_coor(2);
          points.points.push_back(temp_point);
        }
      }
      if (points.points.size() != 0)
      {
        points.channels[0].values[0] = 255;
        point_pub_.publish(points);
      }
    }
    else
    {
      std::string error_string = "no lasermarks.";
      LOG_EVERY_N(ERROR, 100) << error_string;
    }
  }

  if (use_pole_marks_3D_)
  {
    double start_time, end_time;
    start_time = ros::Time::now().toSec();
    end_time = start_time - 1.;
    ros::Rate rate_tmp(100);
    geometry_msgs::TransformStamped odom2map_tf, base2odom_tf;
    Eigen::Affine3d odom2map, base2odom;
    while (ros::ok())
    {
      try
      {
        end_time = ros::Time::now().toSec();
        odom2map_tf = tfBuffer_.lookupTransform(map_link, odom_link, msg->header.stamp);
        base2odom_tf = tfBuffer_.lookupTransform(odom_link, base_link, msg->header.stamp);
        tf::transformMsgToEigen(odom2map_tf.transform, odom2map);
        tf::transformMsgToEigen(base2odom_tf.transform, base2odom);
        break;
      }
      catch (tf::TransformException ex)
      {
        if (end_time - start_time > 1.0)
        {
          LOG(ERROR) << "Looking for the tf between map and odom is timed out.";
          break;
        }
      }
      if (end_time - start_time > 1) return;
      rate_tmp.sleep();
    }
    if (end_time - start_time > 1) return;
    vector<Eigen::Vector3d> points;
    for (const cartographer_ros_msgs::LandmarkEntry &it : msg->landmarks)
    {
      Eigen::Vector3d tmp2{it.tracking_from_landmark_transform.position.x,
                           it.tracking_from_landmark_transform.position.y,
                           it.tracking_from_landmark_transform.position.z};
      points.push_back(tmp2);
    }
    LOG(INFO) << "points size: " << points.size();
    for (auto it : points)
    {
      Eigen::Vector3d point = it;
      point(2) = 0;
      Eigen::Vector3d mark_in_odom = base2odom * point;
      Eigen::Vector3d mark_in_map = odom2map * mark_in_odom;
      LandmarkPose temp_pose;
      if (!isNewPointChecked({mark_in_map(0), mark_in_map(1)}, landmark_pose_list_, temp_pose))
      {
        updateMarks(temp_pose.id, mark_in_odom, msg->header.stamp,
                    marks_in_odom_); // id string 2 int
        updateMarks(temp_pose.id,
                    Eigen::Vector3d(temp_pose.pose.position.x, temp_pose.pose.position.y, 0),
                    msg->header.stamp, marks_in_map_); // id string 2 int
        marks_id_.insert(temp_pose.id);
      }
    }
  }
}

void LasermarkLocalization::handleMergeCloudPoints(
    const sensor_msgs::PointCloud2ConstPtr &msg, const geometry_msgs::PoseStampedConstPtr &pose_msg,
    cartographer_ros_msgs::LandmarkList &landmark_list)
{
  if (curb_detection_ == nullptr) return;
  static Eigen::Isometry3d base_to_lidar2 = Eigen::Isometry3d::Identity();
  static Eigen::Isometry3d base_to_lidar0 = Eigen::Isometry3d::Identity();
  static bool get_tf_static_flag = false;
  if (!get_tf_static_flag)
  {
    try
    {
      geometry_msgs::TransformStamped base2lidar, base2lidar2;
      base2lidar = tfBuffer_.lookupTransform("lidar_link", "base_footprint", ros::Time(0));
      base2lidar2 = tfBuffer_.lookupTransform("lidar_link2", "base_footprint", ros::Time(0));
      tf::transformMsgToEigen(base2lidar.transform, base_to_lidar0);
      tf::transformMsgToEigen(base2lidar2.transform, base_to_lidar2);
      get_tf_static_flag = true;
      //       break;
    }
    catch (tf::TransformException ex)
    {
      return;
    }
  }
  Eigen::Isometry3d odom2map;
  try
  {
    geometry_msgs::TransformStamped odom2map_tf;
    odom2map_tf = tfBuffer_.lookupTransform("map", "odom", msg->header.stamp);
    tf::transformMsgToEigen(odom2map_tf.transform, odom2map);
  }
  catch (tf::TransformException ex)
  {
    LOG(WARNING) << "Get base2map tf failed...";
    return;
  }
  pcl::PointCloud<curb::VPoint>::Ptr cloud_vpoint(new pcl::PointCloud<curb::VPoint>);
  pcl::fromROSMsg(*msg, *cloud_vpoint);
  // 路沿检测， 结果以点云形式给出
  pcl::PointCloud<pcl::PointXYZ> cloud_out = curb_detection_->curb_detector(
      *cloud_vpoint, msg->header.stamp, base_to_lidar0, base_to_lidar2);
  sensor_msgs::PointCloud detect_curb_points;
  sensor_msgs::PointCloud curb_points_in_map;
  //   pcl::toROSMsg(cloud_out, cloud_final);
  detect_curb_points.header.stamp = msg->header.stamp;
  detect_curb_points.header.frame_id = "odom";

  curb_points_in_map.header.stamp = msg->header.stamp;
  curb_points_in_map.header.frame_id = "map";
  // filter points with pca
  Eigen::Isometry3d base2map, base2odom;
  tf::poseMsgToEigen(pose_msg->pose, base2odom);
  base2map = odom2map * base2odom;

  // 通过先验去得到点云在map下的大致位置，通过近邻搜索找到与地图中路沿点云最近的点，并通过PCA分析该点是否是直线，若是则将点云对放到landmark_list中
  for (const auto &it : cloud_out.points)
  {
    Eigen::Vector3d p = base2map * Eigen::Vector3d(it.x, it.y, it.z);
    pcl::PointXYZ p2;
    p2.x = p.x();
    p2.y = p.y();
    p2.z = p.z();
    std::vector<int> pointSearchInd;
    std::vector<float> pointSearchSqDis;
    kdtree_curb_->nearestKSearch(p2, 5, pointSearchInd, pointSearchSqDis);

    if (pointSearchSqDis[4] > 0.8) continue;
    float cx = 0, cy = 0, cz = 0;
    cv::Mat matA1(3, 3, CV_32F, cv::Scalar::all(0));
    cv::Mat matD1(1, 3, CV_32F, cv::Scalar::all(0));
    cv::Mat matV1(3, 3, CV_32F, cv::Scalar::all(0));
    for (int j = 0; j < 5; j++)
    {
      cx += curb_points_->points[pointSearchInd[j]].x;
      cy += curb_points_->points[pointSearchInd[j]].y;
      cz += curb_points_->points[pointSearchInd[j]].z;
    }
    cx /= 5;
    cy /= 5;
    cz /= 5;

    float a11 = 0, a12 = 0, a13 = 0, a22 = 0, a23 = 0, a33 = 0;
    for (int j = 0; j < 5; j++)
    {
      float ax = curb_points_->points[pointSearchInd[j]].x - cx;
      float ay = curb_points_->points[pointSearchInd[j]].y - cy;
      float az = curb_points_->points[pointSearchInd[j]].z - cz;

      a11 += ax * ax;
      a12 += ax * ay;
      a13 += ax * az;
      a22 += ay * ay;
      a23 += ay * az;
      a33 += az * az;
    }
    a11 /= 5;
    a12 /= 5;
    a13 /= 5;
    a22 /= 5;
    a23 /= 5;
    a33 /= 5;

    matA1.at<float>(0, 0) = a11;
    matA1.at<float>(0, 1) = a12;
    matA1.at<float>(0, 2) = a13;
    matA1.at<float>(1, 0) = a12;
    matA1.at<float>(1, 1) = a22;
    matA1.at<float>(1, 2) = a23;
    matA1.at<float>(2, 0) = a13;
    matA1.at<float>(2, 1) = a23;
    matA1.at<float>(2, 2) = a33;

    cv::eigen(matA1, matD1, matV1);

    if (matD1.at<float>(0, 0) > 3 * matD1.at<float>(0, 1))
    {
      float x1 = cx + 0.1 * matV1.at<float>(0, 0);
      float y1 = cy + 0.1 * matV1.at<float>(0, 1);
      float z1 = cz + 0.1 * matV1.at<float>(0, 2);
      float x2 = cx - 0.1 * matV1.at<float>(0, 0);
      float y2 = cy - 0.1 * matV1.at<float>(0, 1);
      float z2 = cz - 0.1 * matV1.at<float>(0, 2);

      // add points to marks in map and odom
      Eigen::Vector3d p = base2odom * Eigen::Vector3d(it.x, it.y, it.z);
      LandmarkPose pose;
      pose.id = curb_id_;
      pose.pose.position.x = p.x();
      pose.pose.position.y = p.y();
      pose.pose.position.z = p.z();
      marks_in_odom_.push_back(pose);
      geometry_msgs::Point32 msg_point;
      msg_point.x = pose.pose.position.x;
      msg_point.y = pose.pose.position.y;
      msg_point.z = pose.pose.position.z;
      detect_curb_points.points.push_back(msg_point);
      {
        cartographer_ros_msgs::LandmarkEntry mark_entry;
        mark_entry.id = pose.id;
        mark_entry.tracking_from_landmark_transform = pose.pose;
        landmark_list.landmarks.push_back(mark_entry);
      }
      pose.pose.position.x = x1;
      pose.pose.position.y = y1;
      pose.pose.position.z = z1;
      marks_in_map_.push_back(pose);
      msg_point.x = pose.pose.position.x;
      msg_point.y = pose.pose.position.y;
      msg_point.z = pose.pose.position.z;
      curb_points_in_map.points.push_back(msg_point);
      pose.pose.position.x = x2;
      pose.pose.position.y = y2;
      pose.pose.position.z = z2;
      marks_in_map_.push_back(pose);
      ++curb_id_;
      msg_point.x = pose.pose.position.x;
      msg_point.y = pose.pose.position.y;
      msg_point.z = pose.pose.position.z;
      curb_points_in_map.points.push_back(msg_point);
    }
  }

  detect_curb_cloud_pub_.publish(detect_curb_points);
  curb_cloud_in_map_pub_.publish(curb_points_in_map);
}

// void LasermarkLocalization::getPoseInMap(const string& fileName)
// {
// 	boost::property_tree::ptree pt;
// 	try{
// 		boost::property_tree::xml_parser::read_xml(fileName, pt);
// 	}
// 	catch(std::exception& e)
// 	{
// 		LOG(INFO) <<e.what() ;
// 		LOG(WARNING) << ("Can't find or open landmark.xml file and it
// will not use lasermark. "); 		return;
// 	}
// 	string xml,xml_id;
// 	for (auto &m : pt){
// 		if (m.first == "landmark")
// 		{
// 			string ns = m.second.get("ns"," ");
// 			if(ns != "Lasermarks")
// 				continue;
// 			xml_id = m.second.get("id"," ");
// 			xml = m.second.get("transform"," ");
// 			float value[7];
//       sscanf(xml.c_str(), "%f %f %f %f %f %f %f",&value[0], &value[1],
//       &value[2],
//             &value[3], &value[4], &value[5], &value[6]);
// 			LandmarkPose pose;
// 			pose.id = stoi(xml_id); // id string 2 int
// 			pose.position = Eigen::Vector2d(value[0],value[1]);
// 			landmark_pose_list_.push_back(pose);
// 		}
// 	}
// }

void LasermarkLocalization::judgeLaserMarker(const Vector3d &point_in_odom, ros::Time time_stamp,
                                             const geometry_msgs::TransformStamped &odom2map_tf,
                                             const geometry_msgs::TransformStamped &odom2base_tf,
                                             cartographer_ros_msgs::LandmarkEntry &landmark_entry)
{
  Eigen::Vector3d point2map_coor = getNewCoor(point_in_odom, odom2map_tf);
  Eigen::Affine3d point2base, odom2base;
  Eigen::Affine3d point2odom = Eigen::Affine3d::Identity();
  tf::transformMsgToEigen(odom2base_tf.transform, odom2base);
  point2odom(0, 3) = point_in_odom(0);
  point2odom(1, 3) = point_in_odom(1);
  point2odom(2, 3) = point_in_odom(2);
  point2base = odom2base * point2odom;
  if (point2base.translation().norm() > max_distance_ + 2) return;
  // get id
  LandmarkPose temp_pose;
  // 	= getID(laser2map_coor);
  if (isNewPointChecked({point2map_coor(0), point2map_coor(1)}, landmark_pose_list_, temp_pose))
  // 	if(temp_pose.id == "")
  {
    //     ros::Duration dur = ros::Time::now() - last_print_warn_time_ ;
    //
    // 		LOG_IF(WARNING,) << ( "this mark do not exit in map");
    return;
  }
  else if (0)
  {
    LOG(WARNING) << "temp_pose.id: " << temp_pose.id << endl
                 << "temp_pose: " << temp_pose.pose.position.x << "," << temp_pose.pose.position.y
                 << "," << endl
                 << "point_in_odom:  " << point_in_odom.transpose() << endl
                 << "laser2map_coor:  " << point2map_coor.transpose();
  }

  // update position and timestamp
  updateMarks(temp_pose.id, point_in_odom, time_stamp,
              marks_in_odom_); // id string 2 int
                               // 	temp_pose.time_stamp = time_stamp;
                               // 	marks_in_map_.push_back(temp_pose);
  updateMarks(temp_pose.id,
              Eigen::Vector3d(temp_pose.pose.position.x, temp_pose.pose.position.y, 0), time_stamp,
              marks_in_map_); // id string 2 int
  marks_id_.insert(temp_pose.id);
  if (marks_in_odom_.size() >= 10000)
  {
    marks_in_odom_.erase(marks_in_odom_.begin());
    marks_in_map_.erase(marks_in_map_.begin());
  }

  // pub to show
  // scan_point_in_base
  landmark_entry.id = to_string(landmark_id_ + temp_pose.id);
  tf::poseEigenToMsg(point2base, landmark_entry.tracking_from_landmark_transform);
  landmark_entry.rotation_weight = 1e-9;
  landmark_entry.translation_weight = translation_weight_;
}

void LasermarkLocalization::updateMarks(const int &id, const Vector3d &coor,
                                        const ros::Time &time_stamp,
                                        std::vector<LandmarkPose> &marks)
{
  // 	vector<LandmarkPose>::iterator it = marks.begin();
  //
  // 	std::map<string,int>::iterator it_cnt;
  // 	it_cnt = marks_cnt_.find(id);
  // 	if(it_cnt == marks_cnt_.end())
  // 		marks_cnt_.insert(std::pair<string,int>(id,0));
  // 	else
  // 		it_cnt->second = it_cnt->second + 1;
  //
  // 	int cnt_temp = it_cnt->second;
  // 	for(;it!=marks.end(); it++)
  // 	{
  // 		string id_temp = it->id;
  // 		if(id_temp == id)
  // 		{
  // 			it->position[0] = (it->position[0] *cnt_temp +
  // coor[0])/(cnt_temp+1) ; 			it->position[1] = (it->position[1]
  // *cnt_temp + coor[1])/(cnt_temp+1) ; 			it->time_stamp =
  // time_stamp; 			return;
  // 		}
  // 	}
  LandmarkPose pose;
  pose.id = id;
  pose.pose.position.x = coor[0];
  pose.pose.position.y = coor[1];
  pose.pose.position.z = coor[2];
  pose.pole_radius = pole_radius_with_id_[id];
  pose.time_stamp = time_stamp;
  marks.push_back(pose);
}

void LasermarkLocalization::cropLasermark(const ros::Time &time)
{
  vector<LandmarkPose>::iterator it1 = marks_in_odom_.begin();
  vector<LandmarkPose>::iterator it2 = marks_in_map_.begin();
  assert(marks_in_odom_.size() == marks_in_map_.size());
  for (; it1 != marks_in_odom_.end();)
  {
    assert(it1->time_stamp == it2->time_stamp);
    if (it1->time_stamp < time)
    {
      it1 = marks_in_odom_.erase(it1);
      it2 = marks_in_map_.erase(it2);
    }
    else
    {
      ++it1;
      ++it2;
    }
  }
}

const sensor_msgs::PointCloud LasermarkLocalization::submap2DToPointCloud(
    const cartographer_ros_msgs::Submap &submap, const float &matching_point_thresh,
    const bool &has_odom2map, const Eigen::Affine3d &odom2map)
{
  float resolution = submap.resolution;
  boost::array<float, 2> max = submap.max;
  int num_x_cells = submap.num_x_cells;
  int num_y_cells = submap.num_y_cells;
  const std::vector<uint16_t> &cells = submap.cells;
  uint index;

  CHECK(num_x_cells != 0);
  CHECK(num_y_cells != 0);

  Eigen::Isometry3d tracking2d_to_local = Eigen::Isometry3d::Identity();
  tracking2d_to_local.rotate(
      Eigen::Quaterniond(submap.pose.orientation.w, submap.pose.orientation.x,
                         submap.pose.orientation.y, submap.pose.orientation.z));
  tracking2d_to_local.pretranslate(
      Eigen::Vector3d(submap.pose.position.x, submap.pose.position.y, submap.pose.position.z));
  Eigen::Isometry3d local_to_tracking2d = tracking2d_to_local.inverse();
  sensor_msgs::PointCloud large_point_cloud;
  if (has_odom2map)
  {
    for (uint i = 0; i < num_y_cells; i++)
    {
      index = i * num_x_cells;
      for (uint j = 0; j < num_x_cells; j++)
      {
        if (cells[index] != 0 &&
            cartographer::mapping::ValueToProbability(cells[index]) < matching_point_thresh)
        {
          Eigen::Vector3d pointInLocal, pointInTracking2d;
          Eigen::Vector3d pointInMap;
          pointInLocal[0] = max[0] - resolution * (i + 0.5);
          pointInLocal[1] = max[1] - resolution * (j + 0.5);
          pointInLocal[2] = 0;

          pointInMap = odom2map * pointInLocal;
          if (isPointValid(bool_img_, pointInMap))
          {
            pointInTracking2d = local_to_tracking2d * pointInLocal;
            geometry_msgs::Point32 point;
            point.x = pointInTracking2d(0);
            point.y = pointInTracking2d(1);
            point.z = pointInTracking2d(2);
            large_point_cloud.points.push_back(point);
          }
        }

        index++;
      }
    }
  }
  else
  {
    for (uint i = 0; i < num_y_cells; i++)
    {
      index = i * num_x_cells;
      for (uint j = 0; j < num_x_cells; j++)
      {
        if (cells[index] != 0 &&
            cartographer::mapping::ValueToProbability(cells[index]) < matching_point_thresh)
        {
          Eigen::Vector3d pointInLocal, pointInTracking2d;
          pointInLocal[0] = max[0] - resolution * (i + 0.5);
          pointInLocal[1] = max[1] - resolution * (j + 0.5);
          pointInLocal[2] = 0;
          pointInTracking2d = local_to_tracking2d * pointInLocal;
          geometry_msgs::Point32 point;
          point.x = pointInTracking2d(0);
          point.y = pointInTracking2d(1);
          point.z = pointInTracking2d(2);
          large_point_cloud.points.push_back(point);
        }

        index++;
      }
    }
  }
  return large_point_cloud;
}

bool LasermarkLocalization::isPointValid(const cv::Mat &bool_image, const Vector3d &point)
{
  if (bool_image.cols == 1) // no bool image
    return true;
  int img_y = (max_box_[0] - point(0)) / resolution_;
  int img_x = (max_box_[1] - point(1)) / resolution_;

  if (img_x > bool_image.cols - 1 || img_y > bool_image.rows - 1 || img_x < 0 || img_y < 0)
  {
    //     cout  << "This point is out of img: " <<max_box_[0]<< "," <<
    //     max_box_[1]<< "," <<point(0)<< "," << point(1)<< "," << img_x << ","
    //     << img_y <<endl;
    return true;
  }
  //   LOG(INFO)  << max_box_[0]<< "," << max_box_[1]<< "," <<point(0)<< "," <<
  //   point(1)<< "," << img_x << "," << img_y ;
  uchar flag = bool_image.at<uchar>(img_y, img_x);
  if (flag == 0)
  {
    return false;
  }
  return true;
}

void LasermarkLocalization::handleStripDetectPoints(const sensor_msgs::PointCloud &msg)
{
  geometry_msgs::TransformStamped base2odom_tf;
  geometry_msgs::TransformStamped odom2map_tf;
  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  // 获取 base2odom tf pose
  while (ros::ok())
  {
    try
    {
      base2odom_tf = tfBuffer_.lookupTransform("odom", "base_footprint", msg.header.stamp);
      break;
    }
    catch (tf::TransformException ex)
    {
      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
      if (duration.count() > 0.5)
      {
        LOG(WARNING) << "Can't get transform between odom and base_footprint!";
        return;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }
  // 获取 odom2map tf pose
  while (ros::ok())
  {
    try
    {
      odom2map_tf = tfBuffer_.lookupTransform("map", "odom", ros::Time(0));
      break;
    }
    catch (tf::TransformException ex)
    {
      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
      if (duration.count() > 0.5)
      {
        LOG(WARNING) << "Can't get transform between map and base_footprint!";
        return;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }

  geometry_msgs::Pose base2odom_pose;
  base2odom_pose.position.x = base2odom_tf.transform.translation.x;
  base2odom_pose.position.y = base2odom_tf.transform.translation.y;
  base2odom_pose.position.z = base2odom_tf.transform.translation.z;
  base2odom_pose.orientation.x = base2odom_tf.transform.rotation.x;
  base2odom_pose.orientation.y = base2odom_tf.transform.rotation.y;
  base2odom_pose.orientation.z = base2odom_tf.transform.rotation.z;
  base2odom_pose.orientation.w = base2odom_tf.transform.rotation.w;
  Eigen::Isometry3d base2odom;
  Eigen::Isometry3d base_last2odom;
  tf::poseMsgToEigen(last_strip_detect_base2odom_, base_last2odom);
  tf::poseMsgToEigen(base2odom_pose, base2odom);
  LOG(WARNING) << "strip_detect_points_ size:" << strip_detect_points_.points.size();
  if (strip_detect_points_.points.size() == 0)
  {
    last_strip_detect_base2odom_ = base2odom_pose;
    strip_detect_points_.header = msg.header;
    strip_detect_points_.header.frame_id = "base_footprint";
    strip_detect_points_.points.insert(strip_detect_points_.points.end(), msg.points.begin(),
                                       msg.points.end());
  }
  else
  {
    Eigen::Isometry3d relative_transform = base_last2odom.inverse() * base2odom;
    for (geometry_msgs::Point32 it : msg.points)
    {
      Eigen::Vector3d point(it.x, it.y, it.z);
      Eigen::Vector3d point_in_pre_base = relative_transform * point;

      geometry_msgs::Point32 new_point;
      new_point.x = point_in_pre_base(0);
      new_point.y = point_in_pre_base(1);
      new_point.z = point_in_pre_base(2);
      strip_detect_points_.points.push_back(new_point);
    }
  }
  LOG(WARNING) << "pub strip_points_in_map";
  {
    if (strip_points_in_map_pub_.getNumSubscribers() > 0)
    {
      geometry_msgs::Pose odom2map_pose;
      odom2map_pose.position.x = odom2map_tf.transform.translation.x;
      odom2map_pose.position.y = odom2map_tf.transform.translation.y;
      odom2map_pose.position.z = odom2map_tf.transform.translation.z;
      odom2map_pose.orientation.x = odom2map_tf.transform.rotation.x;
      odom2map_pose.orientation.y = odom2map_tf.transform.rotation.y;
      odom2map_pose.orientation.z = odom2map_tf.transform.rotation.z;
      odom2map_pose.orientation.w = odom2map_tf.transform.rotation.w;
      Eigen::Isometry3d base2map, odom2map;
      tf::poseMsgToEigen(odom2map_pose, odom2map);
      base2map = odom2map * base2odom;
      sensor_msgs::PointCloud cloud_in_map;
      for (geometry_msgs::Point32 it : msg.points)
      {
        Eigen::Vector3d point(it.x, it.y, it.z);
        Eigen::Vector3d point_in_map = base2map * point;

        geometry_msgs::Point32 new_point;
        new_point.x = point_in_map(0);
        new_point.y = point_in_map(1);
        new_point.z = point_in_map(2);
        cloud_in_map.points.push_back(new_point);
      }
      cloud_in_map.header.frame_id = "map";
      cloud_in_map.header.stamp = msg.header.stamp;
      strip_points_in_map_pub_.publish(cloud_in_map);
    }
  }
  LOG(WARNING) << "pub strip_points_in_map done!";
}

void LasermarkLocalization::handleVerticalLineLandmark(
    const cartographer_ros_msgs::LandmarkInDifferentCoordinates::ConstPtr &msg,
    cartographer_ros_msgs::LandmarkList &landmark_list)
{
  int size_landmarks = msg->marks_in_global.size();
  // 遍历所有marker点， 并更新 marks_in_odom_ 和 marks_in_map_
  for (int i = 0; i < size_landmarks; i++)
  {
    Eigen::Vector3d global_pose(msg->marks_in_global[i].x, msg->marks_in_global[i].y,
                                msg->marks_in_global[i].z);
    Eigen::Vector3d odom_pose(msg->marks_in_odom[i].x, msg->marks_in_odom[i].y,
                              msg->marks_in_odom[i].z);
    int id = stoi(msg->marks_in_global[i].id);
    // 更新 marks_in_odom_
    updateMarks(id, odom_pose, msg->header.stamp,
                marks_in_odom_); // id string 2 int

    //   temp_pose.time_stamp = time_stamp;
    //   marks_in_map_.push_back(temp_pose);
    // 更新 marks_in_map_
    updateMarks(id, global_pose, msg->header.stamp,
                marks_in_map_); // id string 2 int
    marks_id_.insert(id);
    if (marks_in_odom_.size() >= 10000)
    {
      marks_in_odom_.erase(marks_in_odom_.begin());
      marks_in_map_.erase(marks_in_map_.begin());
    }

    LOG(WARNING) << "id:" << id << "\nglobal_pose:" << global_pose.transpose()
                 << "\nodom_pose:" << odom_pose.transpose()
                 << "\nmarks_in_odom_ size: " << marks_in_odom_.size()
                 << "\nmarks_in_map_ size: " << marks_in_map_.size();
    //     cartographer_ros_msgs::LandmarkEntry landmark_entry;
    //     landmark_entry.id = to_string(id);
    //     //                 tf::poseEigenToMsg(point2base,
    //     // landmark_entry.tracking_from_landmark_transform);
    //     landmark_entry.tracking_from_landmark_transform.position.x =
    //     temp_poses[i].pose_in_base(0);
    //     landmark_entry.tracking_from_landmark_transform.position.y =
    //     temp_poses[i].pose_in_base(1);
    //     landmark_entry.tracking_from_landmark_transform.position.z =
    //     temp_poses[i].pose_in_base(2); landmark_entry.rotation_weight = 1e-9;
    //     landmark_entry.translation_weight = translation_weight_;
    //     landmark_list.landmarks.push_back(landmark_entry);
  }
}

void LasermarkLocalization::handleUwbPoints(
    const cartographer_ros_msgs::LinktrackNodeframe2::ConstPtr &msg)
{
  std::unique_lock<std::mutex> lk(mutex_);
  ros::Time time = msg->ros_time;
  std::set<float> sort_distances;
  for (auto &it : msg->nodes)
  {
    if (uwb_marks_.find(it.id) == uwb_marks_.end()) continue;

    // TODO: consider whether to limit distance
    Eigen::Vector4d tmp(it.id, it.dis, it.fp_rssi, it.rx_rssi);
    uwb_datas_[time].push_back(tmp);
    sort_distances.insert(it.dis);
  }
  if (!use_uwb_ || !has_gps_info_)
  {
    uwb_novalid_cnt_ = 0;
    return;
  }
  if (sort_distances.size() > 1)
  {
    // 前两个uwb 与车的距离要满足一定
    auto it = sort_distances.begin();
    if (*it <= 58 && *(++it) <= 105)
    {
      uwb_novalid_cnt_ = 0;
    }
    else
      uwb_novalid_cnt_++;
  }
  else
    uwb_novalid_cnt_++;
}

void LasermarkLocalization::setBoolGpsInfo(const bool &flag)
{
  has_gps_info_ = flag;
  LOG(WARNING) << "set has_gps_info: " << has_gps_info_;
}

void LasermarkLocalization::clearStripPoints()
{
  LOG(INFO) << "clear strip points";
  if (!strip_detect_points_.points.empty()) strip_detect_points_.points.clear();
}

void LasermarkLocalization::setLocationLaermarksID(const map<int, int> &location_lasermark_ids)
{
  location_lasermark_ids_.clear();
  location_lasermark_ids_ = location_lasermark_ids;
  LOG(INFO) << "location_lasermark id: ";
  for (auto it : location_lasermark_ids_)
    cout << it.first << " ";
  cout << endl;
  LOG(INFO) << "set location_lasermark_ids done!";
}

void LasermarkLocalization::handleScanFilter(const sensor_msgs::LaserScanConstPtr &msg)
{
  std::unique_lock<std::mutex> lk(dynamic_detect_mutex_);
  //   if(!zero_velocity_flag_)
  //     return;
  Eigen::Affine3d base2map;
  static int zero_velocity_cnt = 0;
  static int no_zero_velocity_cnt = 0;
  ros::Rate rate(100);
  int failed_cnt = 0;
  // 获取laser2map 的tf pose
  while (ros::ok())
  {
    try
    {
      geometry_msgs::TransformStamped odom2map_tf;
      odom2map_tf = tfBuffer_.lookupTransform("map", msg->header.frame_id, msg->header.stamp);
      tf::transformMsgToEigen(odom2map_tf.transform, base2map);
      break;
    }
    catch (tf::TransformException ex)
    {
      if (failed_cnt++ > 8)
      {
        LOG(WARNING) << "Looking for the tf between map and base_footprint is "
                        "timed out.";
        return;
      }
      rate.sleep();
    }
  }
  sensor_msgs::LaserScan pub_msg = *msg;
  // 判断车是否已经停下
  if (dynamic_free_space_image_.empty() || !zero_velocity_flag_)
  {
    pub_msg.ranges.assign(msg->ranges.size(), std::numeric_limits<float>::infinity());
    dynamic_detect_pub_.publish(pub_msg);
    no_zero_velocity_cnt++;
    if (no_zero_velocity_cnt > 50)
    {
      LOG(INFO) << zero_velocity_cnt << ", " << no_zero_velocity_cnt;
      zero_velocity_cnt = 0;
      no_zero_velocity_cnt = 0;
    }
    return;
  }
  float angle = msg->angle_min;
  // 遍历所有点，映射到图像中， 判断其是否再动态区域内
  for (size_t i = 0; i < msg->ranges.size(); ++i)
  {
    const auto &echoes = msg->ranges[i];
    if (msg->range_min <= echoes && echoes <= msg->range_max)
    {
      const Eigen::AngleAxisd rotation(angle, Eigen::Vector3d::UnitZ());
      Eigen::Vector3d points_in_base = rotation * (echoes * Eigen::Vector3d::UnitX());
      Eigen::Vector3d points_in_map = base2map * points_in_base;
      if (isPointValid(dynamic_free_space_image_, points_in_map))
      {
        pub_msg.ranges[i] = std::numeric_limits<float>::infinity();
      }
    }
    angle += msg->angle_increment;
  }
  dynamic_detect_pub_.publish(pub_msg);
  zero_velocity_cnt++;
  if (zero_velocity_cnt > 50)
  {
    LOG(INFO) << zero_velocity_cnt << ", " << no_zero_velocity_cnt;
    zero_velocity_cnt = 0;
    no_zero_velocity_cnt = 0;
  }
}

void LasermarkLocalization::handleWheelOdom(const nav_msgs::OdometryConstPtr &msg)
{
  std::unique_lock<std::mutex> lk(dynamic_detect_mutex_);
  zero_velocity_flag_ = false;
  if (fabs(msg->twist.twist.linear.x) < 5e-3 && fabs(msg->twist.twist.linear.y) < 1e-3 &&
      fabs(msg->twist.twist.linear.z) < 1e-3 && fabs(msg->twist.twist.angular.x) < 1e-3 &&
      fabs(msg->twist.twist.angular.y) < 1e-3 && fabs(msg->twist.twist.angular.z) < 3e-2)
    zero_velocity_flag_ = true;
}
void LasermarkLocalization::clear()
{
  handle_rect_invalid_area_server_.shutdown();
  submap_local_sub_.shutdown();
  scan_sub_.shutdown();
  landmark_list_sub_.shutdown();
  dynamic_detect_sub_.shutdown();
  wheel_odom_sub_.shutdown();
  submap_lasermark_pub_.shutdown();
  point_pub_.shutdown();
  submap_server_client_.shutdown();
  strip_points_in_map_pub_.shutdown();
  dynamic_detect_pub_.shutdown();
}
} // namespace lasermark
} // namespace map_associator
