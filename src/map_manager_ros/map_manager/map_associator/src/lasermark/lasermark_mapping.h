/*
 * @Date: 2023-02-24 18:34:12
 * @Author: Cyy && cyycyag@163.com
 * @LastEditors: Cyy && cyycyag@163.com
 * @LastEditTime: 2023-03-09 19:59:43
 * @FilePath: /map_manager/map_associator/src/lasermark/lasermark_mapping.h
 * @Description: 
 */
/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef LASERMARK_MAPPING_H
#define LASERMARK_MAPPING_H

#include <eigen_conversions/eigen_msg.h>
#include <math.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/MarkerArray.h>

#include <cmath>
#include <mutex>

#include "lasermark_interface.h"
namespace map_associator
{
namespace lasermark
{
class LaserMarkMapping : public LasermarkInterface
{
  public:
  LaserMarkMapping(const ros::NodeHandle &n);
  ~LaserMarkMapping();
  /**
   * @description: 消息与服务shutdown
   * @return {*}
   */
  virtual void clear();
  
  /**
   * @description: 将检测到的反射板点，进行聚类，数据关联，中心化
   * @param {LandmarkListConstPtr} &msg
   * @param {  } cartographer_ros_msgs
   * @return {*}
   */
  void HandleLaser(const cartographer_ros_msgs::LandmarkListConstPtr &msg,
                   cartographer_ros_msgs::LandmarkList &landmark_list);
                   
  /**
   * @description: 获取建图节点处理后的反射板点及pose，以便后续做数据关联，避免生成重复的反射板id
   * @param {Ptr} &msg
   * @return {*}
   */
  void HandleLandmarkList(const visualization_msgs::MarkerArray::Ptr &msg);

  /**
   * @description: 设定是否是3d模式
   * @param {bool} &flag
   * @return {*}
   */
  void setIs3D(const bool &flag) { is_3d_ = flag; };

  /**
   * @description: 续建时 添加已有地图中的反射板信息
   * @param {vector<LandmarkPose>} &mark_pose_in_map
   * @return {*}
   */
  void addExistMarks(const std::vector<LandmarkPose> &mark_pose_in_map);

  private:

  /**
   * @description: 判断是否添加该反射板点
   * @param {Vector2d} new_coor
   * @param {int} &id
   * @return {*}
   */
  bool isAddNew(Eigen::Vector2d new_coor, int &id);

  /**
   * @description: 判断反射板点云中的点是否满足使用要求(判断是否是outlier点)
   * @param {Vector3d} &point_in_map
   * @param {TransformStamped} &map2base
   * @param {  } cartographer_ros_msgs
   * @param {  } std
   * @return {*}
   */
  bool judgeLaserMarker(const Eigen::Vector3d &point_in_map,
                        const geometry_msgs::TransformStamped &map2base,
                        cartographer_ros_msgs::LandmarkEntry &landmark_list,
                        std::vector<LandmarkPose> &mark_pose_in_frame);
  std::vector<Eigen::Vector2d> laser_markers_in_map_;
  long landmark_id_, origin_map_max_landmark_id_;
  ros::Publisher laserMark_pub_, point_pub_;
  ros::Subscriber laser_sub_, landmark_list_sub_;
  ros::NodeHandle nh_;
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  sensor_msgs::PointCloud points_;
  std::mutex mutex_;
  std::vector<LandmarkPose> mark_pose_in_map_, mark_pose_in_fix_;
  bool initialized_;
  int pub_cnt_;
  unsigned int pub_mark_cnt_;
  cartographer_ros_msgs::LandmarkList last_landmark_list_;
  double min_intensity_;
  bool is_3d_;
  float pole_radius_;
  int origin_map_lasermark_size_;
};
} // namespace lasermark
} // namespace map_associator
#endif // LASERMARK_MAPPING_H
