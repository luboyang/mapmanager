cmake_minimum_required(VERSION 2.8.3)
project(map_associator)
add_definitions(-std=c++11)
SET(CMAKE_BUILD_TYPE "Release") 
# SET(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g -DDEBUG -ggdb")
SET(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -w -g -DNDEBUG")	

# set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
#     ${CMAKE_CURRENT_SOURCE_DIR}/modules)
# set(PACKAGE_DEPENDENCIES
#   cartographer_ros_msgs
#   eigen_conversions
# )
# google_initialize_cartographer_project()
# set(CARTOGRAPHER_INCLUDE_DIRS "/home/cyy/cartographer_ws/install_isolated//include") 
find_package(catkin REQUIRED COMPONENTS
  cartographer_ros_msgs
  cv_bridge
  roscpp
  roslib
  sensor_msgs
  tf
  tf2
  eigen_conversions
  tf_conversions
  tf2_eigen
  tf2_ros
  tf2_sensor_msgs
  message_filters
  vtr_msgs
  iplus_perception
  jdrv_msgs
)
find_package(absl REQUIRED)
find_package(cartographer REQUIRED)
# # include("${CARTOGRAPHER_CMAKE_DIR}/functions.cmake")
# find_package(cartographer_ros REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(OpenCV REQUIRED)
find_package(PCL REQUIRED COMPONENTS common)
find_package(Boost REQUIRED COMPONENTS system iostreams)
find_package(libnabo REQUIRED PATHS ${LIBNABO_INSTALL_DIR})
find_package(PCL REQUIRED COMPONENTS common io)
find_package(line_detector REQUIRED)
find_package(csm_eigen REQUIRED)
find_package(yaml-cpp REQUIRED)
# include(FindPkgConfig)
PKG_SEARCH_MODULE(CAIRO REQUIRED cairo>=1.12.16)
catkin_package(
  CATKIN_DEPENDS
    ${PACKAGE_DEPENDENCIES}
  INCLUDE_DIRS "."
)


# find_package(cartographer REQUIRED)
# find_package(cartographer_ros REQUIRED)
include_directories(
BEFORE ${GLOG_INCLUDE_DIR})
include_directories(
  src/
  ${catkin_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIR}
  ${OpenCV_INCLUDE_DIRS}
	${CAIRO_INCLUDE_DIRS}
	${PCL_INCLUDE_DIRS}
	${cartographer_INCLUDE_DIRS}
# 	${cartographer_ros_INCLUDE_DIRS}
	${line_detector_INSTALL_PREFIX}/include/
)
# "${CARTOGRAPHER_CMAKE_DIR}/../../../include")
file(GLOB_RECURSE ALL_SRCS "*.cc" "*.h")
# set(CMAKE_AUTOMOC ON)

link_directories(
${line_detector_INSTALL_PREFIX}/lib/
${CARTOGRAPHER_CMAKE_DIR}/../../../lib/)
###########
## Build ##
###########
# add_library(map_manager ${PROJECT_SOURCE_DIR}/src/map_manager.cpp)
# link_directories(/home/cyy/kdevelopBUILD/map_manager/build/devel/lib/)
# MESSAGE(STATUS "project_source:  ${}") 

# link_directories(${CARTOGRAPHER_CMAKE_DIR}/../../../lib/)

add_executable(lasermark_node
    src/mark_interface.cpp
    src/landmark_node.cpp
    src/lasermark/lasermark_mapping.cpp
    src/lasermark/lasermark_localization.cpp
    src/lasermark/lasermark_interface.cpp
    src/mark_mapping.cpp
    src/mark_localization.cpp
    src/gps/gps_interface.cpp
    src/gps/gps_localization.cpp
    src/gps/gps_mapping.cpp
    src/laser_matcher/laser_matcher.cpp)
    
# target_include_directories(lasermark_node SYSTEM PUBLIC
#   "${GLOG_INCLUDE_DIRS}")
target_link_libraries(lasermark_node PUBLIC glog cartographer strip_detect ${catkin_LIBRARIES} ${OpenCV_LIBS} ${YAML_CPP_LIBRARIES}
${csm_EXPORTED_TARGETS} ${csm_eigen_LIBRARIES})

install(TARGETS  lasermark_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
#RUNTIME DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION } / ${PROJECT_NAME }
)
install(DIRECTORY launch config
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(DIRECTORY ../scripts 
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/../../
)