#include<iostream>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;


	/****************直线检测************************/
// int main()
// {
//     cout << "hello world" << endl;
//     cv::Mat image = cv::imread("/home/lidongxu/map/dst.jpg");
//     cv::Mat grayImage;
//     cvtColor(image,grayImage,CV_BGR2GRAY);


// 	cv::imshow("1111111", grayImage);

// #if 1
//     Ptr<LineSegmentDetector> ls = createLineSegmentDetector(LSD_REFINE_ADV);
//     Ptr<LineSegmentDetector> ls = createLineSegmentDetector(LSD_REFINE_NONE);
// #endif
//     double start = double(getTickCount());
//     vector<Vec4f> lines_std;
//     // Detect the lines
//     ls->detect(grayImage, lines_std);//检测到的直线线段都存入了lines_std中，4个float的值，分别为起止点的坐标
//     for (int i = 0; i < lines_std.size(); i++)
//     {
//         cout << lines_std[i][0] <<" "<< lines_std[i][1]<<" "
//             << lines_std[i][2]<<" "<<lines_std[i][3]<<endl;
//     }
//     double duration_ms = (double(getTickCount()) - start) * 1000 / getTickFrequency();
//     std::cout << "It took " << duration_ms << " ms." << std::endl;
//     // Show found lines
//     cv::Mat drawnLines(grayImage);
//     ls->drawSegments(drawnLines, lines_std);
//     imshow("Standard refinement", drawnLines);
//     waitKey(0);
//     return 0;
// }



//Zhang-Sun细化算法
void SkeletonExtraction(cv::Mat& src, cv::Mat& srcImg)
{

 	// src.copyTo(dst);
	//灰度化
	// cvtColor(dst, dst, COLOR_RGB2GRAY);
	// Mat Img;
	//二值化
	// threshold(src, Img, 125, 255, THRESH_BINARY_INV);


	 src.copyTo(srcImg);
	/****************骨架提取算法：Zhang-Suen法************************/
	vector<Point> deleteList;
	int neighbourhood[9];
	int row = srcImg.rows;
	int col = srcImg.cols;
	bool inOddIterations = true;
	while (true) {
		for (int j = 1; j < (row - 1); j++) {
			uchar* data_last = srcImg.ptr<uchar>(j - 1);
			uchar* data = srcImg.ptr<uchar>(j);
			uchar* data_next = srcImg.ptr<uchar>(j + 1);
			for (int i = 1; i < (col - 1); i++) {
				if (data[i] == 255) {
					int whitePointCount = 0;
					neighbourhood[0] = 1;
					//判断中心点8邻域的像素特征
					if (data_last[i] == 255) neighbourhood[1] = 1;
					else  neighbourhood[1] = 0;
					if (data_last[i + 1] == 255) neighbourhood[2] = 1;
					else  neighbourhood[2] = 0;
					if (data[i + 1] == 255) neighbourhood[3] = 1;
					else  neighbourhood[3] = 0;
					if (data_next[i + 1] == 255) neighbourhood[4] = 1;
					else  neighbourhood[4] = 0;
					if (data_next[i] == 255) neighbourhood[5] = 1;
					else  neighbourhood[5] = 0;
					if (data_next[i - 1] == 255) neighbourhood[6] = 1;
					else  neighbourhood[6] = 0;
					if (data[i - 1] == 255) neighbourhood[7] = 1;
					else  neighbourhood[7] = 0;
					if (data_last[i - 1] == 255) neighbourhood[8] = 1;
					else  neighbourhood[8] = 0;
					for (int k = 1; k < 9; k++) {
						//二进制值为1的个数
						whitePointCount += neighbourhood[k];
					}
					//条件  2<=B(p1)<=6
					if ((whitePointCount >= 2) && (whitePointCount <= 6)) {
						int ap = 0;
						//条件  A(p1)值
						if ((neighbourhood[1] == 0) && (neighbourhood[2] == 1)) ap++;
						if ((neighbourhood[2] == 0) && (neighbourhood[3] == 1)) ap++;
						if ((neighbourhood[3] == 0) && (neighbourhood[4] == 1)) ap++;
						if ((neighbourhood[4] == 0) && (neighbourhood[5] == 1)) ap++;
						if ((neighbourhood[5] == 0) && (neighbourhood[6] == 1)) ap++;
						if ((neighbourhood[6] == 0) && (neighbourhood[7] == 1)) ap++;
						if ((neighbourhood[7] == 0) && (neighbourhood[8] == 1)) ap++;
						if ((neighbourhood[8] == 0) && (neighbourhood[1] == 1)) ap++;
						if (ap == 1) {
							if (inOddIterations && (neighbourhood[3] * neighbourhood[5] * neighbourhood[7] == 0)
								&& (neighbourhood[1] * neighbourhood[3] * neighbourhood[5] == 0)) {
								deleteList.push_back(Point(i, j));
							}
							else if (!inOddIterations && (neighbourhood[1] * neighbourhood[5] * neighbourhood[7] == 0)
								&& (neighbourhood[1] * neighbourhood[3] * neighbourhood[7] == 0)) {
								deleteList.push_back(Point(i, j));
							}
						}
					}
				}
			}
		}
		if (deleteList.size() == 0)
			break;
		for (size_t i = 0; i < deleteList.size(); i++) {
			Point tem;
			tem = deleteList[i];
			uchar* data = srcImg.ptr<uchar>(tem.y);
			data[tem.x] = 0;
		}
		deleteList.clear();

		// inOddIterations = !inOddIterations;
		if (!inOddIterations) break; 
	}

}
int main(int argc,char** argv) {
    cv::Mat img = cv::imread("/home/lidongxu/map/dst.jpg");
   
 if(img.empty())
    {
        std::cout<<"error!"<<std::endl;
        
    }
    // cv::Mat  gray_binary,gray;
	// if(img.type() != CV_8UC1)
    // 	cv::cvtColor(img, gray, CV_BGR2GRAY);
  	// else
    // 	gray = img;

    // cv::threshold(gray, gray_binary, 125,255, cv::THRESH_BINARY_INV);

    //图像细化
    cv::Mat dst;
    SkeletonExtraction(img, dst);
    // //反色
    // int height = dst.rows;
    // int width = dst.cols;
    // for (int row = 0; row < height; row++)
    // {
    //     for (int col = 0; col < width; col++)
    //     {
    //         int gray = dst.at<uchar>(row, col);
    //         dst.at<uchar>(row, col) = 255 - gray;
    //     }
    // }
    //保存图像
    imwrite(string(getenv("HOME")) + "/map/" + "dst1.jpg" , dst);
    cv::waitKey(0);
    
    return 0;
}
