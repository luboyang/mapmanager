#!/bin/bash
export HOME=/home/jz
source /opt/ros/kinetic/setup.bash && source /opt/jz/iplus-algo-msgs/setup.bash --extend && source /opt/jz/cartographer-iplus-mod/setup.bash --extend && source /opt/jz/map-manager/setup.bash --extend && rostopic list >/dev/null 2>&1 || { >&2 echo `date` ": roscore for map-manager not ready"; }
# source /opt/ros/kinetic/setup.bash && source /home/cyy/jz_project/catkin_ws/install/setup.bash --extend && source /home/cyy/jz_project/cartographer_ws/install_isolated/setup.bash --extend && source /home/cyy/kDevelopBuild/emma_tools/install/setup.bash --extend && source /home/cyy/kDevelopBuild/map_manager/install/setup.bash --extend && rostopic list >/dev/null 2>&1 || { >&2 echo `date` ": roscore for map-manager not ready"; exit 1; }

use3d=false
if cat /home/jz/templates/robotInfo | grep laserType | grep 2d >/dev/null
then
  use3d=false
else
  use3d=true
fi

roslaunch --wait map_manager map_manager.launch use3d:=$use3d
echo "launch success"
