// #include "map_management.h"
#include <cartographer_ros/node_options.h>
#include <cartographer_ros/trajectory_options.h>
#include <cartographer_ros_msgs/LaunchNode.h>
#include <map_manager_msgs/MapsAlign.h>
#include <cartographer_ros_msgs/SubmapImagesPreviewServer.h>
#include <map_manager_msgs/MergeSubmaps.h>
#include <map_manager_msgs/TagInfos.h>
#include <map_manager_msgs/TagMapInfos.h>
#include <ros/package.h>

#include "2d/map_localization.h"
#include "2d/map_mapping.h"
#include "3d/map_localization_3d.h"
#include "3d/map_mapping_3d.h"
#include "map_manager_msgs/CropPointService.h"
#include "map_manager_msgs/GetTagMap.h"
#include "map_manager_msgs/GhostingDetectionService.h"
#include "map_manager_msgs/MergeMapService.h"
#include "map_manager_msgs/ReflectorDetectionService.h"
#include "map_manager_ulitity.h"
#include "map_tools/ghosting_detection.h"
#include "map_tools/maps_align_with_landmarks.h"
#include "map_tools/merge_map.h"
#include "map_tools/pbstream_convertor.h"
#include "map_tools/pub_points_from_map.h"
#include "std_srvs/Trigger.h"
#include "tag_odom/tag_test.h"
#include "tag_odom/wheel_odom_to_tf.h"
#include "utility/iplus_log_sink.hpp"
#include "map_tools/ghosting_detection.h"
#include "tag_odom/tag_test.h"
#include <mutex>
using namespace std;
std::unique_ptr<map_manager::MapInterface> map_;
std::unique_ptr<map_manager::MergeMap> merge_map_;
std::unique_ptr<PubPointsFromMap> pub_;
std::unique_ptr<GhostingDetection> ghosting_detection_;
std::unique_ptr<WheelOdomToTF> tag_odom_;
std::unique_ptr<GetTagMap> get_tag_map_;
vector<Tag_info> tag_msgs;
std::mutex mutex_;
enum STATUS{IDLE = -1, MAPPING_2D, LOCALIZATION_2D, PATCH_MAPPING_2D, 
  MAPPING_3D, LOCALIZATION_3D, PATCH_MAPPING_3D};
struct TagInfo
{
  int tagMapId;
  geometry_msgs::Pose pose;
};
bool launchNode(cartographer_ros_msgs::LaunchNode::Request &request,
                cartographer_ros_msgs::LaunchNode::Response &response)
{
  int mode = request.status;
  bool is_3d = false;
  ros::param::get("~is_3d", is_3d);
  if (map_ != nullptr) map_.reset();

  if (is_3d && mode != IDLE) mode += 3;
  LOG(INFO) << "new mode: " << mode << ", " << (is_3d ? "is" : "is not") << " 3d";

  if (mode == IDLE)
  {
    LOG(INFO) << "IDLE!";
  }
  else if (mode == MAPPING_2D || mode == PATCH_MAPPING_2D)
  {
    map_.reset(new map_manager::MapMapping());
    map_->setMode(mode);
    LOG(INFO) << "start 2d mapping!";
  }
  else if (mode == LOCALIZATION_2D)
  {
    map_.reset(new map_manager::MapLocalization());
    LOG(INFO) << "start 2d localization!";
  }
  else if (mode == MAPPING_3D || mode == PATCH_MAPPING_3D)
  {
    map_.reset(new map_manager::MapMapping3D());
    LOG(INFO) << "start 3d mapping!";
  }
  else if (mode == LOCALIZATION_3D)
  {
    map_.reset(new map_manager::MapLocalization3D());
    LOG(INFO) << "start 3d localization!";
  }
  return true;
}

// bool tagOdomLaunchNode(cartographer_ros_msgs::LaunchNode::Request& request,
//                 cartographer_ros_msgs::LaunchNode::Response& response)
// {
//   int mode = request.status;
//   if(tag_odom_ != nullptr)
//     tag_odom_.reset();

//   LOG(INFO) << "new mode: " <<mode;

//   if(mode != IDLE)
//   {
//     tag_odom_.reset(new WheelOdomToTF);
//     LOG(INFO) << "start 2d tag localization!";
//   }

//   return true;
// }

bool mergeSubmaps(map_manager_msgs::MergeSubmaps::Request &request,
                  map_manager_msgs::MergeSubmaps::Response &response)
{
  cartographer_ros::NodeOptions node_options;
  cartographer_ros::TrajectoryOptions trajectory_options;
  std::string config_path = ::ros::package::getPath("cartographer_ros") + "/configuration_files";
  std::tie(node_options, trajectory_options) =
      cartographer_ros::LoadOptions(config_path, "backpack_2d_jz_mapping.lua");
  {
    std::unique_ptr<map_manager::pbstream_convertor::PbstreamConvertor> pbstream(
        new map_manager::pbstream_convertor::PbstreamConvertor(node_options.map_builder_options));
    pbstream->setFrezon(!request.fix_nav_points);
    std::string origin_map_name = request.origin_map_name;
    std::string new_map_name = request.new_map_name;
    if (pbstream->handlePbstream(origin_map_name, new_map_name))
      LOG(INFO) << "save merged pbstream successfully !";
    else
    {
      LOG(WARNING) << "save merged pbstream failed! ";
      response.success = false;
      response.error_msgs = "save merged pbstream failed! ";
      return false;
    }
    std::vector<Pose3d> poses;
    for (auto it : request.origin_nav_points)
    {
      geometry_msgs::Pose point = it;
      Pose3d point_pose;
      point_pose.p = Eigen::Vector3d(point.position.x, point.position.y, point.position.z);
      point_pose.q = Eigen::Quaterniond(point.orientation.w, point.orientation.x,
                                        point.orientation.y, point.orientation.z);
      poses.push_back(point_pose);
    }
    pbstream->updatePoints(poses);

    for (const auto &pose : poses)
    {
      geometry_msgs::Pose point;
      point.position.x = pose.p(0);
      point.position.y = pose.p(1);
      point.position.z = pose.p(2);

      point.orientation.x = pose.q.x();
      point.orientation.y = pose.q.y();
      point.orientation.z = pose.q.z();
      point.orientation.w = pose.q.w();
      response.update_nav_points.push_back(point);
    }
  }
  response.success = true;
  return true;
}
using namespace cartographer;
using namespace cartographer::mapping;
int GetCurTrajectoryIdFromPbstream(const std::string &filename)
{
  io::ProtoStreamReader stream(filename);
  io::ProtoStreamDeserializer deserializer(&stream);
  proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  int max_trajectory = pose_graph_proto.trajectory_size();
  //   for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
  //   {
  //     auto& trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
  //     max_trajectory = std::max(max_trajectory,
  //     trajectory_proto.trajectory_id()); std::cout << max_trajectory <<" ";
  //   }
  //   std::cout <<endl;
  std::cout << max_trajectory << endl;
  return max_trajectory;
}

void SaveTrajectoryArea(const std::string &path, const int &cur_trajectory,
                        const std::vector<cv::Point2f> &final_points)
{
  //   double max[2];
  cv::Point2f point_in_map[4];
  {
    for (int i = 0; i < 4; i++)
    {
      LOG(INFO) << final_points[i].x << ", " << final_points[i].y;

      point_in_map[i].x = final_points[i].x;
      point_in_map[i].y = final_points[i].y;
      geometry_msgs::Point32 p;
      p.x = point_in_map[i].x;
      p.y = point_in_map[i].y;
      p.z = 0;
    }
  }

  {
    boost::property_tree::ptree pt;
    std::string filename = path + "trajectory_area.xml";
    bool rewrite = false;
    std::string points_in_map = "";
    for (int i = 0; i < 4; i++)
      points_in_map +=
          std::to_string(point_in_map[i].x) + " " + std::to_string(point_in_map[i].y) + " ";

    if (0 && map_manager::judgeFileExist(filename))
    {
      boost::property_tree::xml_parser::read_xml(filename, pt,
                                                 boost::property_tree::xml_parser::trim_whitespace);
      for (auto &mark : pt)
      {
        if (mark.first == "trajectory_area")
        {
          int id = mark.second.get<int>("trajectory_id");
          if (id == cur_trajectory)
          {
            rewrite = true;
            mark.second.put<std::string>("points_in_map", points_in_map);
          }
        }
      }
    }

    if (!rewrite)
    {
      boost::property_tree::ptree p_child_info;
      p_child_info.put("trajectory_id", cur_trajectory);
      p_child_info.put("points_in_map", points_in_map);
      pt.add_child("trajectory_area", p_child_info);
    }

    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    boost::property_tree::write_xml(filename, pt, std::locale(), setting);
  }
}

bool setPatchingArea(map_manager_msgs::Set2dPoints::Request &request,
                     map_manager_msgs::Set2dPoints::Response &response)
{
  if (map_ == nullptr)
  {
    response.error_msgs = "Mode is idle!";
    LOG(WARNING) << response.error_msgs;
    return true;
  }
  std::string map_name = request.map_name;
  std::string path = std::string(getenv("HOME")) + "/map/" + map_name + "/";
  LOG(WARNING) << "origin path: " << path;
  if (!map_manager::judgeFileExist(path + "frames/0/data.xml"))
  {
    response.error_msgs = "cur map file is not existed!";
    LOG(WARNING) << response.error_msgs;
    return true;
  }
  const int cur_trajectory = GetCurTrajectoryIdFromPbstream(path + "map.pbstream");

  if (request.topo_corner_points[0].points.size() != 4)
  {
    response.error_msgs = "topo_corner_points[0].points.size() != 4";
    return true;
  }
  std::vector<cv::Point2f> final_points;
  std::vector<cv::Point2f> points_in_map;
  final_points.reserve(request.topo_corner_points[0].points.size());
  points_in_map.reserve(request.topo_corner_points[0].points.size());
  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(path + "frames/0/data.xml", pt);
  std::string data = pt.get<std::string>("probability_grid.max");
  double max[2];
  sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
  double resolution = pt.get<double>("probability_grid.resolution");
  for (const map_manager_msgs::Point2d p : request.topo_corner_points[0].points)
  {
    points_in_map.push_back(cv::Point2f(p.x, p.y));
    cv::Point2f point_in_img;
    point_in_img.x = (max[1] - p.y) / resolution;
    point_in_img.y = (max[0] - p.x) / resolution;
    final_points.push_back(point_in_img);
  }
  SaveTrajectoryArea(path, cur_trajectory, points_in_map);
  LOG(WARNING) << "save trajectory area done!";
  cv::Mat img = cv::imread(path + "map.png");
  cv::line(img, final_points[0], final_points[1], cv::Scalar(0, 255, 0), 2);
  cv::line(img, final_points[1], final_points[2], cv::Scalar(255, 255, 0), 2);
  cv::line(img, final_points[2], final_points[3], cv::Scalar(255, 255, 0), 2);
  cv::line(img, final_points[3], final_points[0], cv::Scalar(255, 255, 0), 2);
  cv::imwrite(path + "trajectory_" + std::to_string(cur_trajectory) + ".png", img);
  return true;
}

bool getOutlierAreas(map_manager_msgs::Get2dPoints::Request &request,
                     map_manager_msgs::Get2dPoints::Response &response)
{
  std::string path = std::string(getenv("HOME")) + "/map/" + request.map_name + "/";
  LOG(INFO) << "load image: " << path + "map.png";
  cv::Mat map_png2 = cv::imread(path + "map.png");
  cv::Mat map_png;
  if (map_png2.type() != CV_8UC1)
    cv::cvtColor(map_png2, map_png, CV_BGR2GRAY);
  else
    map_png = map_png2;

  pcl::PointCloud<pcl::PointXY>::Ptr points(new pcl::PointCloud<pcl::PointXY>);
  for (int i = 0; i < map_png.rows; i++)
  {
    for (int j = 0; j < map_png.cols; j++)
    {
      int pixel = map_png.at<uchar>(i, j);
      pcl::PointXY p;
      p.x = i;
      p.y = j;
      if (pixel < 80) points->push_back(p);
    }
  }
  LOG(INFO) << "points size: " << points->size();
  std::vector<std::vector<int>> clusters_index;
  map_manager::dbscan(points, clusters_index, 6, 1);
  LOG(INFO) << "clusters size: " << clusters_index.size();
  std::vector<std::vector<int>> outlier_clusters_index;
  for (const std::vector<int> &cluster : clusters_index)
  {
    if (cluster.size() < 10)
    {
      outlier_clusters_index.push_back(cluster);
    }
  }

  std::vector<map_manager_msgs::Points2d> topo_corner_points;
  for (const std::vector<int> &it : outlier_clusters_index)
  // for(const std::vector<int>& cluster : outlier_clusters_index)
  {
    map_manager_msgs::Points2d cluster_points;
    for (const auto &it2 : it)
    {
      map_manager_msgs::Point2d pd;
      pd.x = points->points[it2].x;
      pd.y = points->points[it2].y;
      cluster_points.points.push_back(pd);
    }
    topo_corner_points.push_back(cluster_points);
  }
  response.topo_corner_points = topo_corner_points;

  LOG(INFO) << "outlier_clusters size: " << outlier_clusters_index.size();
  cv::Mat png_bgr;
  if (map_png.type() == CV_8UC1)
    cv::cvtColor(map_png, png_bgr, CV_GRAY2BGR);
  else
    png_bgr = map_png;
  for (const std::vector<int> &it : outlier_clusters_index)
  {
    int arv_i = 0, arv_j = 0;
    for (const auto &it2 : it)
    {
      arv_i += points->points[it2].x;
      arv_j += points->points[it2].y;
    }
    arv_i = arv_i / it.size();
    arv_j = arv_j / it.size();
    cv::circle(png_bgr, cv::Point(arv_j, arv_i), 5, cv::Scalar(0, 0, 255));
  }
  imwrite(std::string(getenv("HOME")) + "/outlier.png", png_bgr);
  // cv::imshow("bgr" , png_bgr);
  cv::waitKey(0);
  return true;
}

double toAngle(const double w, const double x, const double y, const double z)
{
  double siny_cosp = 2 * (w * z + x * y);
  double cosy_cosp = 1 - 2 * (y * y + z * z);
  double yaw = std::atan2(siny_cosp, cosy_cosp);

  return -yaw;
}

transform::Rigid2d getTransformFromImagePoints(const Eigen::Vector2d &max1,
                                               const Eigen::Vector2d &max2,
                                               const Eigen::Vector2d &img_point,
                                               const double &angle, const double &resolution)
{
  Eigen::Vector2d t1;
  t1[0] = max1[0] - img_point[1] * resolution;
  t1[1] = max1[1] - img_point[0] * resolution;
  transform::Rigid2d img2map1(t1, angle);
  LOG(INFO) << img2map1;
  transform::Rigid2d img2map2({max2[0], max2[1]}, 0);
  LOG(INFO) << img2map2;
  return img2map1 * img2map2.inverse();
}

bool mergeMapSrv(map_manager_msgs::MergeMapService::Request &request,
                 map_manager_msgs::MergeMapService::Response &response)
{
  LOG(INFO) << "init_pose: " << request.init_pose;
  merge_map_.reset(new map_manager::MergeMap);

  double px = request.init_pose.position.x;
  double py = request.init_pose.position.y;
  double pz = request.init_pose.position.z;

  double ow = request.init_pose.orientation.w;
  double ox = request.init_pose.orientation.x;
  double oy = request.init_pose.orientation.y;
  double oz = request.init_pose.orientation.z;

  double angle = toAngle(ow, ox, oy, oz);

  boost::property_tree::ptree pt1, pt2;
  boost::property_tree::read_xml(request.path1 + "/frames/0/data.xml", pt1);
  boost::property_tree::read_xml(request.path2 + "/frames/0/data.xml", pt2);
  std::string data1, data2;
  Eigen::Vector2d max1, max2;
  double resolution;
  Eigen::Vector2d image_point;
  image_point[0] = px;
  image_point[1] = py;
  data1 = pt1.get<std::string>("probability_grid.max");
  sscanf(data1.c_str(), "%lf %lf", &max1[0], &max1[1]);
  data2 = pt2.get<std::string>("probability_grid.max");
  sscanf(data2.c_str(), "%lf %lf", &max2[0], &max2[1]);
  LOG(INFO) << "max1: " << max1;
  LOG(INFO) << "max2: " << max2;
  LOG(INFO) << "image_point: " << image_point;

  resolution = pt1.get<double>("probability_grid.resolution");
  LOG(INFO) << "resolution: " << resolution;
  transform::Rigid2d relative_pose =
      getTransformFromImagePoints(max1, max2, image_point, angle, resolution);
  LOG(INFO) << "input_pose: " << relative_pose;

  merge_map_->start(request.path1, request.path2, transform::Embed3D(relative_pose));

  // cartographer::transform::Rigid3d init_pose1(Eigen::Vector3d(px,py,pz),
  // Eigen::Quaterniond(ox,oy,oz,ow)); LOG(INFO)<<"input_pose: "<<init_pose1;
  // merge_map_->start(request.path1, request.path2, init_pose1);
  // LOG(INFO)<<"transform::Embed3D(relative_pose):"<<transform::Embed3D(relative_pose);

  response.success = true;
  return true;
}
bool saveTagMapSrv(map_manager_msgs::TagMapInfos::Request &request,
                   map_manager_msgs::TagMapInfos::Response &response)
{
  LOG(INFO) << "save Tag Map Srv..";
  std::map<int, transform::Rigid3d> tag_map_poses_;
  std::map<int, int> id_cnt_;
  int tag_cnt = 0;
  LOG(INFO) << "request.tags size: " << request.tags.size();
  for (const map_manager_msgs::TagPose &it : request.tags)
  {
    LOG(INFO) << it.id << ", pose: " << it.pose.position.x << "," << it.pose.position.y << ","
              << it.pose.position.z << "," << it.pose.orientation.w << "," << it.pose.orientation.x
              << "," << it.pose.orientation.y << "," << it.pose.orientation.z;
    transform::Rigid3d tag_pose({it.pose.position.x, it.pose.position.y, it.pose.position.z},
                                {it.pose.orientation.w, it.pose.orientation.x,
                                 it.pose.orientation.y, it.pose.orientation.z});
    tag_map_poses_[it.id] = tag_pose;
    id_cnt_[it.id] = tag_cnt++;
  }

  std::map<int, std::map<int, transform::Rigid3d>> tags_map;
  for (const map_manager_msgs::TagPose &it : request.tags)
  {
    std::map<int, transform::Rigid3d> neighbors;
    for (const int &neighbor_id : it.neighbors_id)
    {
      neighbors[neighbor_id] = tag_map_poses_[it.id].inverse() * tag_map_poses_[neighbor_id];
    }
    tags_map[it.id] = neighbors;
  }

  std::string tag_map_path_ = request.tag_map_path;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  LOG(INFO) << "tags size: " << tags_map.size();
  if (!tags_map.empty())
  {
    boost::property_tree::ptree p_top;
    boost::property_tree::ptree p_map;
    boost::property_tree::ptree p_node_list;
    for (auto it : tags_map)
    {
      boost::property_tree::ptree p_node;
      p_node.put("id", id_cnt_[it.first]);
      LOG(INFO) << it.first;
      boost::property_tree::ptree p_neighbour_list;
      for (auto it2 : it.second)
      {
        boost::property_tree::ptree p_neighbour;
        transform::Rigid3d relative_pose = it2.second;
        p_neighbour.put("id", id_cnt_[it2.first]);
        std::string data = "";
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(relative_pose.translation()[i]) + " ";
        data = data + std::to_string(relative_pose.rotation().w()) + " ";
        data = data + std::to_string(relative_pose.rotation().x()) + " ";
        data = data + std::to_string(relative_pose.rotation().y()) + " ";
        data = data + std::to_string(relative_pose.rotation().z());
        p_neighbour.put("transform", data);
        p_neighbour.put("reachable", true);
        p_neighbour_list.add_child("neighbour", p_neighbour);
        LOG(INFO) << it2.first << ": " << data;
      }
      p_node.add_child("neighbour_list", p_neighbour_list);
      p_node_list.add_child("node", p_node);
    }
    p_map.add_child("node_list", p_node_list);
    boost::property_tree::ptree p_property;
    p_property.put("node_count", tags_map.size());

    p_map.add_child("property", p_property);
    p_top.add_child("map", p_map);
    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    //     string folderstring = string(getenv("HOME"))+ "/map/tags";
    std::string system_command = "mkdir -p " + tag_map_path_;
    LOG(INFO) << system_command;
    int status = system(system_command.c_str());
    boost::property_tree::write_xml(tag_map_path_ + "/map.xml", p_top, std::locale(), setting);

    std::map<int, transform::Rigid3d> final_map_poses = tag_map_poses_;
    //     for(auto it:tag_map_poses_)
    //     {
    //       final_map_poses[it.first] = it.second;
    //     }
    boost::property_tree::ptree global_pose_map;
    boost::property_tree::ptree p_landmark_info;

    for (auto it : final_map_poses)
    {
      boost::property_tree::ptree p_map;
      std::string data;
      //   NodeId tmp_node_id{0,0};
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(it.second.translation()[i]) + " ";
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(it.second.rotation().coeffs()[i]) + " ";
      data = data + std::to_string(it.second.rotation().coeffs()[3]);
      p_map.put("id", id_cnt_[it.first]);
      p_map.put("tag_id", it.first);
      p_map.put("pose", data);

      global_pose_map.put("ns", "Visualmarks");
      global_pose_map.put("tag_id", it.first);
      global_pose_map.put("id", id_cnt_[it.first]);
      global_pose_map.put("visible", 1);
      global_pose_map.put("global_pose", data);
      p_landmark_info.add_child("tag", global_pose_map);
      data.clear();
      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      std::string folderstring = tag_map_path_ + "/frames/" + std::to_string(id_cnt_[it.first]);
      std::string system_command = "mkdir -p " + folderstring;
      int status = system(system_command.c_str());
      boost::property_tree::write_xml(folderstring + "/data.xml", p_map, std::locale(), setting);
    }
    boost::property_tree::write_xml(tag_map_path_ + "/tag_global_poses.xml", p_landmark_info,
                                    std::locale(), setting);
  }
  LOG(INFO) << "save done..";

  response.success = true;
  return true;
}

bool mapsAlignSrv(map_manager_msgs::MapsAlign::Request &request,
                  map_manager_msgs::MapsAlign::Response &response)
{
  LOG(INFO) << "maps align srv...";
  std::string laser_map_path_ = request.laser_map_path;
  std::string tag_map_path_ = request.tag_map_path;
  LOG(INFO) << "laser_map_path_: " << laser_map_path_;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  std::map<int, transform::Rigid3d> tag_map_poses_;
  // load tag map
  LOG(INFO) << "load tag map: ";
  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(tag_map_path_ + "/tag_global_poses.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "tag_global_poses.xml is empty!";
      response.success = false;
      response.error_msgs = "tag map: tag_global_poses.xml is empty!";
      return true;
    }
    std::string xml, tag_id;
    for (auto &mark : pt)
    {
      // if (mark.first == "landmark")
      // {
      //   int visible = std::stoi(mark.second.get("visible",""));
      //   if(!visible)
      //     continue;
      tag_id = mark.second.get("tag_id", "");
      xml = mark.second.get("global_pose", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                 {value[6], value[3], value[4], value[5]});
      int id = std::stoi(tag_id);
      LOG(INFO) << id << ": " << tag2map;
      tag_map_poses_[id] = tag2map;
      // }
    }
  }
  LOG(INFO) << "load laser map: ";
  std::map<int, transform::Rigid3d> tags_in_laser_map_;

  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(laser_map_path_ + "/landmark.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "landmark.xml is empty!";
      //       response.success = false;
      //       response.error_msgs = "laser map: landmark.xml is empty!";
      for (const auto &it : tag_map_poses_)
      {
        map_manager_msgs::TagPose tag_in_laser_map;
        tag_in_laser_map.id = it.first;
        tag_in_laser_map.pose.position.x = it.second.translation().x();
        tag_in_laser_map.pose.position.y = it.second.translation().y();

        tag_in_laser_map.pose.orientation.w = it.second.rotation().w();
        tag_in_laser_map.pose.orientation.x = it.second.rotation().x();
        tag_in_laser_map.pose.orientation.y = it.second.rotation().y();
        tag_in_laser_map.pose.orientation.z = it.second.rotation().z();
        response.tags_in_laser_map.push_back(tag_in_laser_map);
      }
      return true;
    }
    std::string xml, xml_id;
    std::string ns;
    //     bool rewrite = false;
    for (auto &mark : pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (!visible) continue;
        ns = mark.second.get("ns", "");
        xml_id = mark.second.get("id", " ");
        xml = mark.second.get("transform", " ");
        float value[7];
        std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                    &value[4], &value[5], &value[6]);
        if (ns != "Visualmarks") continue;

        transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                   {value[6], value[3], value[4], value[5]});
        int id = std::stoi(xml_id);
        tags_in_laser_map_[id] = tag2map;
        LOG(INFO) << id << ": " << tag2map;
      }
    }
  }

  std::vector<cv::Point2f> src_points, ref_points;

  std::vector<std::pair<transform::Rigid3d, transform::Rigid3d>> tag_poses_pair;
  for (const auto &it : tags_in_laser_map_)
  {
    if (tag_map_poses_.find(it.first) != tag_map_poses_.end())
    {
      map_manager_msgs::TagPose tag_in_laser_map;
      tag_in_laser_map.id = it.first;
      tag_in_laser_map.pose.position.x = it.second.translation().x();
      tag_in_laser_map.pose.position.y = it.second.translation().y();

      tag_in_laser_map.pose.orientation.w = it.second.rotation().w();
      tag_in_laser_map.pose.orientation.x = it.second.rotation().x();
      tag_in_laser_map.pose.orientation.y = it.second.rotation().y();
      tag_in_laser_map.pose.orientation.z = it.second.rotation().z();
      response.tags_in_laser_map.push_back(tag_in_laser_map);

      //       src_points.push_back({tag_map_poses_[it.first].translation().x(),
      //       tag_map_poses_[it.first].translation().y()});
      //       ref_points.push_back({it.second.translation().x(),
      //       it.second.translation().y()});

      tag_poses_pair.push_back(std::make_pair(it.second, tag_map_poses_[it.first]));
    }
  }

  if (response.tags_in_laser_map.empty())
  {
    response.error_msgs = "No matched tags.";
    response.success = false;
    return true;
  }

  for (const auto &it : tag_map_poses_)
  {
    if (tags_in_laser_map_.find(it.first) != tags_in_laser_map_.end()) continue;
    std::map<float, std::pair<transform::Rigid3d, transform::Rigid3d>> sort_poses;
    for (const auto &pair : tag_poses_pair)
    {
      float det_t = (it.second.translation() - pair.second.translation()).norm();
      sort_poses[det_t] = pair;
    }

    std::vector<std::pair<float, transform::Rigid3d>> dis_with_poses;
    float dis_sum = 0;
    for (const auto &sort_pose : sort_poses)
    {
      if (dis_with_poses.size() == 2) break;
      dis_with_poses.push_back(std::make_pair(
          sort_pose.first, sort_pose.second.first * sort_pose.second.second.inverse()));
      dis_sum += sort_pose.first;
    }
    std::vector<std::pair<float, transform::Rigid3d>> weight_with_poses;
    for (const auto &dis_pose : dis_with_poses)
    {
      float weight = dis_pose.first / dis_sum;
      weight_with_poses.push_back(std::make_pair(weight, dis_pose.second));
    }

    transform::Rigid3d tag2laser_pose;
    if (weight_with_poses.size() == 2)
    {
      Eigen::Quaterniond q1 = weight_with_poses[0].second.rotation();
      Eigen::Quaterniond q2 = weight_with_poses[1].second.rotation();
      Eigen::Quaterniond q = q1.slerp(weight_with_poses[1].first, q2);
      Eigen::Vector3d t = weight_with_poses[0].first * weight_with_poses[0].second.translation() +
                          weight_with_poses[1].first * weight_with_poses[1].second.translation();
      tag2laser_pose = transform::Rigid3d(t, q);
    }
    else
      tag2laser_pose = weight_with_poses[0].second;

    transform::Rigid3d laser_map_pose = tag2laser_pose * it.second;
    map_manager_msgs::TagPose tag_in_laser_map;
    tag_in_laser_map.id = it.first;
    tag_in_laser_map.pose.position.x = laser_map_pose.translation().x();
    tag_in_laser_map.pose.position.y = laser_map_pose.translation().y();

    tag_in_laser_map.pose.orientation.w = laser_map_pose.rotation().w();
    tag_in_laser_map.pose.orientation.x = laser_map_pose.rotation().x();
    tag_in_laser_map.pose.orientation.y = laser_map_pose.rotation().y();
    tag_in_laser_map.pose.orientation.z = laser_map_pose.rotation().z();
    response.tags_in_laser_map.push_back(tag_in_laser_map);

    //     src_points.push_back({tag_map_poses_[it.first].translation().x(),
    //     tag_map_poses_[it.first].translation().y()});
    //     ref_points.push_back({it.second.translation().x(),
    //     it.second.translation().y()});
  }

  boost::property_tree::ptree p_list;

  for (int i = 0; i < response.tags_in_laser_map.size(); i++)
  {
    boost::property_tree::ptree p_landmark_info;
    p_landmark_info.put("tag_id", response.tags_in_laser_map[i].id);
    std::string data;
    data = std::to_string(response.tags_in_laser_map[i].pose.position.x) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.position.y) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.position.z) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.orientation.w) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.orientation.x) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.orientation.y) + " " +
           std::to_string(response.tags_in_laser_map[i].pose.orientation.z);

    p_landmark_info.put("tag_in_laser_map_pose", data);
    p_list.add_child("tag", p_landmark_info);
    data.clear();
  }
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(tag_map_path_ + "/tag_in_laser_map.xml", p_list, std::locale(),
                                  setting);

  response.success = true;
  LOG(INFO) << "done..";
  return true;
}

bool mapsAlignWithLandmarksSrv(
  map_manager_msgs::MapsAlign::Request& request, 
  map_manager_msgs::MapsAlign::Response& response)
{
  cartographer::MapsAlignWithLandmarks map(request.tag_map_path, request.laser_map_path, false);
  response.success = true;
  return true;
}

bool cropMergeArea(map_manager_msgs::CropPointService::Request &request,
                   map_manager_msgs::CropPointService::Response &response)
{
  vector<vector<Eigen::Vector2f>> p;

  float x1 = request.p1.x;
  float y1 = request.p1.y;
  float x2 = request.p2.x;
  float y2 = request.p2.y;
  float x3 = request.p3.x;
  float y3 = request.p3.y;
  float x4 = request.p4.x;
  float y4 = request.p4.y;
  vector<Eigen::Vector2f> b;

  b.push_back(Eigen::Vector2f(x1, y1));
  b.push_back(Eigen::Vector2f(x2, y2));
  b.push_back(Eigen::Vector2f(x3, y3));
  b.push_back(Eigen::Vector2f(x4, y4));
  p.push_back(b);

  bool flag = request.flag;
  merge_map_->crop(p, flag);
  // cv::imwrite(std::string(std::getenv("HOME")) +
  // "/previweMap.png",merge_map_);

  response.finish = true;
  return true;
}

bool previewMergeMap(map_manager_msgs::GetImage::Request &request,
                     map_manager_msgs::GetImage::Response &response)
{
  // merge_map_.reset(new map_manager::MergeMap);
  cv::Mat src = merge_map_->getResultImage();
  cv::Mat show_img = map_manager::image16ToImage8(src);

  cv::imwrite(std::string(std::getenv("HOME")) + "/previewMap.png", show_img);
  sensor_msgs::CompressedImagePtr msg_ptr =
      cv_bridge::CvImage(std_msgs::Header(), "bgra8", show_img)
          .toCompressedImageMsg(cv_bridge::PNG);
  response.image = *msg_ptr;
  return true;
}

bool saveMergeMap(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response)
{
  merge_map_->save(std::string(std::getenv("HOME")) + "/map/aaaaaaaaa");
  response.success = true;
  return true;
}

bool ghostingDetection(map_manager_msgs::GhostingDetectionService::Request &request,
                       map_manager_msgs::GhostingDetectionService::Response &response)
{
  LOG(INFO) << "input_path1: " << request.path;
  std::vector<std::pair<Line, Line>> ghostingLines;
  cv::Mat img = ghosting_detection_->ghostDetection(request.path, ghostingLines);
  sensor_msgs::CompressedImagePtr msg_ptr =
      cv_bridge::CvImage(std_msgs::Header(), "bgra8", img).toCompressedImageMsg(cv_bridge::PNG);
  for (const auto &lines : ghostingLines)
  {
    map_manager_msgs::GhostingLines tmpGhostingLines;
    tmpGhostingLines.firstLine.startPoint.x = lines.first.x1;
    tmpGhostingLines.firstLine.startPoint.y = lines.first.y1;
    tmpGhostingLines.firstLine.endPoint.x = lines.first.x2;
    tmpGhostingLines.firstLine.endPoint.y = lines.first.y2;
    tmpGhostingLines.secondLine.startPoint.x = lines.second.x1;
    tmpGhostingLines.secondLine.startPoint.y = lines.second.y1;
    tmpGhostingLines.secondLine.endPoint.x = lines.second.x2;
    tmpGhostingLines.secondLine.endPoint.y = lines.second.y2;
    response.ghosting_lines.emplace_back(std::move(tmpGhostingLines));
  }
  response.image = *msg_ptr;
  response.success = true;
  return true;
}

bool getTagMap(map_manager_msgs::GetTagMap::Request &request,
               map_manager_msgs::GetTagMap::Response &response)
{
  LOG(INFO) << "input_path1: " << request.path1;
  tag_msgs = get_tag_map_->getTagMap(request.path1);
  response.success = true;
  return true;
}

bool reflectorDetection(map_manager_msgs::ReflectorDetectionService::Request &request,
                        map_manager_msgs::ReflectorDetectionService::Response &response)
{
  LOG(INFO) << "input_path: " << request.path;
  DataInPbstream data_in_pbstream;
  for (int i = 0; i < request.tags.size(); i++)
  {
    double px = request.tags[i].pose.position.x;
    double py = request.tags[i].pose.position.y;
    double pz = request.tags[i].pose.position.z;

    double ow = request.tags[i].pose.orientation.w;
    double ox = request.tags[i].pose.orientation.x;
    double oy = request.tags[i].pose.orientation.y;
    double oz = request.tags[i].pose.orientation.z;
    // data_in_pbstream.id.push_back(request.tra[i].node_index);
    // data_in_pbstream.data.push_back(request.tra[i].pose);
    int id = request.tags[i].id;
    cartographer::transform::Rigid3d global_pose(Eigen::Vector3d(px, py, pz),
                                                 Eigen::Quaterniond(ox, oy, oz, ow));
    data_in_pbstream.nodes_pose.Insert(cartographer::mapping::NodeId{1, id}, global_pose);
  }

  std::vector<cartographer::mapping::NodeId> node_id;
  pub_->getPointsFormMap(request.path, /*data_in_pbstream,*/ node_id);

  for (const cartographer::mapping::NodeId &id : node_id)
  {
    response.nodes_id.push_back(id.node_index);
  }

  response.success = true;
  LOG(INFO) << "Detection Finish!";

  return true;
}

bool getTagMapPoseInLaserMap(const std::string &tagMapPath, const std::string &laserMapPath,
                             std::unordered_map<int, geometry_msgs::Pose> &tagMapIdWithPose)
{
  LOG(INFO) << "maps align srv...";
  std::string laser_map_path_ = laserMapPath;
  std::string tag_map_path_ = tagMapPath;
  LOG(INFO) << "laser_map_path_: " << laser_map_path_;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  std::map<int, cartographer::transform::Rigid3d> tag_map_poses_;
  std::vector<map_manager_msgs::TagPose> tags_in_laser_map;
  // load tag map
  LOG(INFO) << "load tag map: ";
  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(tag_map_path_ + "/tag_global_poses.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "tag_global_poses.xml is empty!";
      return false;
    }
    std::string xml, tag_id;
    for (auto &mark : pt)
    {
      // if (mark.first == "landmark")
      // {
      //   int visible = std::stoi(mark.second.get("visible",""));
      //   if(!visible)
      //     continue;
      tag_id = mark.second.get("tag_id", "");
      xml = mark.second.get("global_pose", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      cartographer::transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                               {value[6], value[3], value[4], value[5]});
      int id = std::stoi(tag_id);
      LOG(INFO) << id << ": " << tag2map;
      tag_map_poses_[id] = tag2map;
      // }
    }
  }
  LOG(INFO) << "load laser map: ";
  std::map<int, cartographer::transform::Rigid3d> tags_in_laser_map_;

  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(laser_map_path_ + "/landmark.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "landmark.xml is empty!";
      //       response.success = false;
      //       response.error_msgs = "laser map: landmark.xml is empty!";
      for (const auto &it : tag_map_poses_)
      {
        geometry_msgs::Pose pose;
        pose.position.x = it.second.translation().x();
        pose.position.y = it.second.translation().y();

        pose.orientation.w = it.second.rotation().w();
        pose.orientation.x = it.second.rotation().x();
        pose.orientation.y = it.second.rotation().y();
        pose.orientation.z = it.second.rotation().z();
        tagMapIdWithPose.insert(std::make_pair(it.first, std::move(pose)));
      }
      return true;
    }
    std::string xml, xml_id;
    std::string ns;
    //     bool rewrite = false;
    for (auto &mark : pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (!visible) continue;
        ns = mark.second.get("ns", "");
        xml_id = mark.second.get("id", " ");
        xml = mark.second.get("transform", " ");
        float value[7];
        std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                    &value[4], &value[5], &value[6]);
        if (ns != "Visualmarks" && ns != "Visualmarks_4x4") continue;

        cartographer::transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                                 {value[6], value[3], value[4], value[5]});
        int id = std::stoi(xml_id);
        tags_in_laser_map_[id] = tag2map;
        LOG(INFO) << id << ": " << tag2map;
      }
    }
  }

  std::vector<cv::Point2f> src_points, ref_points;

  std::vector<std::pair<cartographer::transform::Rigid3d, cartographer::transform::Rigid3d>>
      tag_poses_pair;
  for (const auto &it : tags_in_laser_map_)
  {
    if (tag_map_poses_.find(it.first) != tag_map_poses_.end())
    {
      map_manager_msgs::TagPose tag_in_laser_map;
      tag_in_laser_map.id = it.first;
      tag_in_laser_map.pose.position.x = it.second.translation().x();
      tag_in_laser_map.pose.position.y = it.second.translation().y();

      tag_in_laser_map.pose.orientation.w = it.second.rotation().w();
      tag_in_laser_map.pose.orientation.x = it.second.rotation().x();
      tag_in_laser_map.pose.orientation.y = it.second.rotation().y();
      tag_in_laser_map.pose.orientation.z = it.second.rotation().z();
      tags_in_laser_map.push_back(tag_in_laser_map);

      //       src_points.push_back({tag_map_poses_[it.first].translation().x(),
      //       tag_map_poses_[it.first].translation().y()});
      //       ref_points.push_back({it.second.translation().x(),
      //       it.second.translation().y()});

      tag_poses_pair.push_back(std::make_pair(it.second, tag_map_poses_[it.first]));
    }
  }

  if (tags_in_laser_map.empty())
  {
    return true;
  }

  for (const auto &it : tag_map_poses_)
  {
    if (tags_in_laser_map_.find(it.first) != tags_in_laser_map_.end()) continue;
    std::map<float, std::pair<cartographer::transform::Rigid3d, cartographer::transform::Rigid3d>>
        sort_poses;
    for (const auto &pair : tag_poses_pair)
    {
      float det_t = (it.second.translation() - pair.second.translation()).norm();
      sort_poses[det_t] = pair;
    }

    std::vector<std::pair<float, cartographer::transform::Rigid3d>> dis_with_poses;
    float dis_sum = 0;
    for (const auto &sort_pose : sort_poses)
    {
      if (dis_with_poses.size() == 2) break;
      dis_with_poses.push_back(std::make_pair(
          sort_pose.first, sort_pose.second.first * sort_pose.second.second.inverse()));
      dis_sum += sort_pose.first;
    }
    std::vector<std::pair<float, cartographer::transform::Rigid3d>> weight_with_poses;
    for (const auto &dis_pose : dis_with_poses)
    {
      float weight = dis_pose.first / dis_sum;
      weight_with_poses.push_back(std::make_pair(weight, dis_pose.second));
    }

    cartographer::transform::Rigid3d tag2laser_pose;
    if (weight_with_poses.size() == 2)
    {
      Eigen::Quaterniond q1 = weight_with_poses[0].second.rotation();
      Eigen::Quaterniond q2 = weight_with_poses[1].second.rotation();
      Eigen::Quaterniond q = q1.slerp(weight_with_poses[1].first, q2);
      Eigen::Vector3d t = weight_with_poses[0].first * weight_with_poses[0].second.translation() +
                          weight_with_poses[1].first * weight_with_poses[1].second.translation();
      tag2laser_pose = cartographer::transform::Rigid3d(t, q);
    }
    else
      tag2laser_pose = weight_with_poses[0].second;

    cartographer::transform::Rigid3d laser_map_pose = tag2laser_pose * it.second;
    map_manager_msgs::TagPose tag_in_laser_map;
    tag_in_laser_map.id = it.first;
    tag_in_laser_map.pose.position.x = laser_map_pose.translation().x();
    tag_in_laser_map.pose.position.y = laser_map_pose.translation().y();

    tag_in_laser_map.pose.orientation.w = laser_map_pose.rotation().w();
    tag_in_laser_map.pose.orientation.x = laser_map_pose.rotation().x();
    tag_in_laser_map.pose.orientation.y = laser_map_pose.rotation().y();
    tag_in_laser_map.pose.orientation.z = laser_map_pose.rotation().z();
    tags_in_laser_map.push_back(tag_in_laser_map);

    //     src_points.push_back({tag_map_poses_[it.first].translation().x(),
    //     tag_map_poses_[it.first].translation().y()});
    //     ref_points.push_back({it.second.translation().x(),
    //     it.second.translation().y()});
  }

  boost::property_tree::ptree p_list;
  for (int i = 0; i < tags_in_laser_map.size(); i++)
  {
    boost::property_tree::ptree p_landmark_info;
    p_landmark_info.put("tag_id", tags_in_laser_map[i].id);
    std::string data;
    data = std::to_string(tags_in_laser_map[i].pose.position.x) + " " +
           std::to_string(tags_in_laser_map[i].pose.position.y) + " " +
           std::to_string(tags_in_laser_map[i].pose.position.z) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.w) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.x) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.y) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.z);
    p_landmark_info.put("tag_in_laser_map_pose", data);
    p_list.add_child("tag", p_landmark_info);
    data.clear();
    tagMapIdWithPose.insert(std::make_pair(tags_in_laser_map[i].id, tags_in_laser_map[i].pose));
  }
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(tag_map_path_ + "/tag_in_laser_map.xml", p_list, std::locale(),
                                  setting);

  LOG(INFO) << "done..";
  return true;
}

bool getTagMapXmlInfo(const std::string &tagMapPath,
                      std::map<int, std::vector<TagInfo>> &tmpTagInfos)
{
  tmpTagInfos.clear();
  std::vector<TagInfo> tmpTagInfoList;
  TagInfo tmpTagInfo;
  int tagMapId = 0;
  // load tag map
  LOG(INFO) << "load tag map: ";

  boost::property_tree::ptree tagMapXmlPt;
  boost::property_tree::xml_parser::read_xml(tagMapPath + "/map.xml", tagMapXmlPt,
                                             boost::property_tree::xml_parser::trim_whitespace);
  if (tagMapXmlPt.empty())
  {
    LOG(WARNING) << "map.xml is empty!";

    return -1;
  }
  std::string xml, submap_id_str, neighbour_id;
  boost::property_tree::ptree nodeList = tagMapXmlPt.get_child("map.node_list");

  for (auto &node : nodeList)
  {
    tmpTagInfoList.clear();
    submap_id_str = node.second.get("id", "");
    tagMapId = std::atoi(submap_id_str.c_str());
    boost::property_tree::ptree neighbourList = node.second.get_child("neighbour_list");
    for (auto &neighbour : neighbourList)
    {
      tmpTagInfo.tagMapId = std::atoi(neighbour.second.get("id", "").c_str());
      xml = neighbour.second.get("transform", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      tmpTagInfo.pose.position.x = value[0];
      tmpTagInfo.pose.position.y = value[1];
      tmpTagInfo.pose.position.z = value[2];

      tmpTagInfo.pose.orientation.w = value[6];
      tmpTagInfo.pose.orientation.x = value[3];
      tmpTagInfo.pose.orientation.y = value[4];
      tmpTagInfo.pose.orientation.z = value[5];
      tmpTagInfoList.push_back(tmpTagInfo);
    }
    tmpTagInfos.insert(std::make_pair(tagMapId, tmpTagInfoList));
  }
  return 0;
}

bool getTagInfosSrv(map_manager_msgs::TagInfos::Request &request,
                    map_manager_msgs::TagInfos::Response &response)
{
  LOG(INFO) << "get TagInfos srv...";
  std::string tag_map_path_ = request.tag_map_path;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  std::map<int, transform::Rigid3d> tag_map_poses_;
  // load tag map
  LOG(INFO) << "load tag map: ";
  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(tag_map_path_ + "/tag_global_poses.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "tag_global_poses.xml is empty!";
      response.success = false;
      response.error_msgs = "tag map: tag_global_poses.xml is empty!";
      return true;
    }
    std::unordered_map<int, int> tagIdAndSubmapId;
    std::unordered_map<int, geometry_msgs::Pose> tagIdAndPose;
    std::string xml, tag_id_str, submap_id_str;
    for (auto &mark : pt)
    {
      tag_id_str = mark.second.get("tag_id", "");
      submap_id_str = mark.second.get("id", "");

      xml = mark.second.get("global_pose", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                 {value[6], value[3], value[4], value[5]});
      int tag_id = std::stoi(tag_id_str);
      int submap_id = std::stoi(submap_id_str);

      // map_manager_msgs::TagMapInfo tag_info;
      geometry_msgs::Pose temp_pose;

      temp_pose.position.x = value[0];
      temp_pose.position.y = value[1];
      temp_pose.position.z = value[2];

      temp_pose.orientation.x = value[3];
      temp_pose.orientation.y = value[4];
      temp_pose.orientation.z = value[5];
      temp_pose.orientation.w = value[6];

      tagIdAndSubmapId.insert(std::make_pair(tag_id, submap_id));
      tagIdAndPose.insert(std::make_pair(tag_id, temp_pose));
      // tag_info.tag_id = tag_id;
      // tag_info.submap_id=submap_id;
      // tag_info.pose=temp_pose;

      // response.tags.push_back(tag_info);
    }
    std::string laserMapPath;

    if (!::ros::param::get("/vtr/path", laserMapPath))
    {
      LOG(ERROR) << "Can not get laser path!";
      return false;
    }
    std::unordered_map<int, geometry_msgs::Pose> tagMapIdWithPose;
    getTagMapPoseInLaserMap(tag_map_path_, laserMapPath, tagMapIdWithPose);
    std::map<int, std::vector<TagInfo>> tmpTagInfos;
    getTagMapXmlInfo(tag_map_path_, tmpTagInfos);

    for (auto &it : tagIdAndSubmapId)
    {
      map_manager_msgs::TagMapInfo tag_info;
      tag_info.tag_id = it.first;
      tag_info.submap_id = it.second;
      tag_info.pose = tagIdAndPose[it.first];
      tag_info.tag_pose_in_laser = tagMapIdWithPose[it.first];
      for (auto &tagInfo : tmpTagInfos[it.second])
      {
        tag_info.neighbors_id.push_back(tagInfo.tagMapId);
      }
      LOG(INFO) << "tagId: " << tag_info.tag_id
                << ", pose: " << tag_info.tag_pose_in_laser.position.x << ","
                << tag_info.tag_pose_in_laser.position.y << ","
                << tag_info.tag_pose_in_laser.position.z << ","
                << tag_info.tag_pose_in_laser.orientation.w << ","
                << tag_info.tag_pose_in_laser.orientation.x << ","
                << tag_info.tag_pose_in_laser.orientation.y << ","
                << tag_info.tag_pose_in_laser.orientation.z;

      response.tags.push_back(tag_info);
    }
  }

  response.success = true;
  LOG(INFO) << "done..";
  return true;
}


bool previewSubmapImages(cartographer_ros_msgs::SubmapImagesPreviewServer::Request& request,
                         cartographer_ros_msgs::SubmapImagesPreviewServer::Response& response)
{
  std::lock_guard<std::mutex> lock(mutex_);
	LOG(INFO) <<"[" <<ros::Time::now() <<"] "<< ("Preview SubmapImages ...");
  ToRosMsg to_ros_msg_node_;
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> pose1,pose2;
  std::vector<double> maxes;
  int size_of_map[2];
  if(!to_ros_msg_node_.loadSubmaps(request.map_path1,response.submap_images, pose1,pose2,maxes,size_of_map))
  {
    response.error_message = "Load submaps failed";
    LOG(WARNING) << response.error_message;
  }
  if(!to_ros_msg_node_.loadSubmaps(request.map_path2,response.submap_images, pose1,pose2,maxes,size_of_map))
  {
    response.error_message = "Load submaps failed";
    LOG(WARNING) << response.error_message;
  }
  LOG(INFO) <<"[" <<ros::Time::now() <<"] "<< ("Load submaps done!");
	return true;
}

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  // 	google::SetStderrLogging(google::GLOG_INFO);
  //   MyInfoLogger my_info_logger;
  IplusLogSink iplus_log_sink;
  google::AddLogSink(&iplus_log_sink);

  FLAGS_colorlogtostderr = true;
  ros::init(argc, argv, "map_manager_node");
  ros::NodeHandle n;
  std::vector<ros::ServiceServer> srvs;
  srvs.push_back(n.advertiseService("/map_manager_launcher", launchNode));
  srvs.push_back(n.advertiseService("/map_manager_node/merge_map", mergeSubmaps));
  srvs.push_back(n.advertiseService("/map_manager_node/set_patching_area", setPatchingArea));
  srvs.push_back(n.advertiseService("/map_manager_node/get_outlier_areas", getOutlierAreas));
  srvs.push_back(n.advertiseService("/map_manager_node/align_maps", mergeMapSrv));
  srvs.push_back(n.advertiseService("/map_manager_node/get_tag_map", getTagMap));
  srvs.push_back(n.advertiseService("/map_manager_node/save_tag_map", saveTagMapSrv));
  srvs.push_back(
      n.advertiseService("/map_manager_node/tag_map_align_with_laser_map", mapsAlignSrv));
  srvs.push_back(
      n.advertiseService("/map_manager_node/maps_align_with_landmarks", mapsAlignWithLandmarksSrv));
  srvs.push_back(n.advertiseService("/map_manager_node/get_tag_info", getTagInfosSrv));
  srvs.push_back(n.advertiseService("/map_manager_node/preview_submap_images",previewSubmapImages));
  srvs.push_back(n.advertiseService("/map_manager_node/reflector_detection", reflectorDetection));
  srvs.push_back(n.advertiseService("/map_manager_node/get_crop_area", cropMergeArea));
  srvs.push_back(n.advertiseService("/map_manager_node/preview_merge_map", previewMergeMap));
  srvs.push_back(n.advertiseService("/map_manager_node/save_merge_map", saveMergeMap));
  srvs.push_back(n.advertiseService("/map_manager_node/ghosting_detection", ghostingDetection));
  ros::spin();
  google::ShutdownGoogleLogging();
  return 1;
}