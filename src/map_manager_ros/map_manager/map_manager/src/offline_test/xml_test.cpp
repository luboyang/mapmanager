#include "tinyxml2/tinyxml2.h"
#include <iostream>
#include "utils/utils_string.hpp"
#include "file_data_type/file_data.hpp"
using namespace std;
using namespace tinyxml2;
file_data_type::SubmapData node_;
inline void setXmlEDocument(XMLDocument& doc,const std::string& name, const std::string& value)
{
  XMLElement *ele = doc.NewElement(name.c_str());
  ele->InsertFirstChild(doc.NewText(value.c_str()));
  doc.InsertEndChild(ele);
}

inline void setXmlElement(XMLDocument& doc, XMLElement* ele,const std::string& name, const std::string& value)
{
  XMLElement* userNode = doc.NewElement(name.c_str());
  userNode->InsertFirstChild(doc.NewText(value.c_str()));
  //4.3设置结束标签
  ele->InsertEndChild(userNode);
}
bool SetLaserDataXml(const std::string& path)
{
 
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  setXmlEDocument(doc, "id", std::to_string(node_.id));
  std::string poseStr;
  stringutils::PoseXmlToString(node_.pose, poseStr);
  setXmlEDocument(doc, "pose", poseStr);
  std::string localPoseStr;
  stringutils::PoseXmlToString(node_.localPose, localPoseStr);
  setXmlEDocument(doc, "local_pose", localPoseStr);
  setXmlEDocument(doc, "num_range_data", std::to_string(node_.numRangeData));
  setXmlEDocument(doc, "finished", node_.finished ? "true" : "false");
  XMLElement *pProbabilityGrid = doc.NewElement("probability_grid");
  doc.InsertFirstChild(pProbabilityGrid);
  doc.InsertEndChild(pProbabilityGrid);
  setXmlElement(doc, pProbabilityGrid, "resolution", std::to_string(node_.resolution));
  std::string maxStr = std::to_string(node_.max[0]) + " " +
      std::to_string(node_.max[1]);
  setXmlElement(doc, pProbabilityGrid, "max", maxStr);
  std::string numCellsStr = std::to_string(node_.numCells[0]) + " " +
      std::to_string(node_.numCells[1]);
  setXmlElement(doc, pProbabilityGrid, "num_cells", numCellsStr);
  std::string knownCellsBoxStr = std::to_string(node_.knownCellsBox[0]) + " " +
      std::to_string(node_.knownCellsBox[1]) + " " +
      std::to_string(node_.knownCellsBox[2]) + " " +
      std::to_string(node_.knownCellsBox[3]);
  setXmlElement(doc, pProbabilityGrid, "known_cells_box", knownCellsBoxStr);    
  return doc.SaveFile("data.xml");
}

bool GetLaserDataXml(const std::string& path)
{
  XMLDocument doc;
  string file_path = path + "data.xml";
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  node_.id = std::atoi(doc.FirstChildElement("id")->GetText());
  node_.finished = doc.FirstChildElement("finished")->BoolText();
  node_.numRangeData = std::atoi(doc.FirstChildElement("num_range_data")->GetText());
  std::string poseStr = doc.FirstChildElement("pose")->GetText();
  stringutils::PoseStringToXml(poseStr, node_.pose);
  std::string localPoseStr = doc.FirstChildElement("local_pose")->GetText();
  stringutils::PoseStringToXml(localPoseStr, node_.localPose);
  std::string maxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("max")->GetText();
  sscanf(maxStr.c_str(), "%lf %lf", &node_.max[0], &node_.max[1]);
  std::string numCellsStr = doc.FirstChildElement("probability_grid")->FirstChildElement("num_cells")->GetText();
  sscanf(numCellsStr.c_str(), "%d %d", &node_.numCells[0], &node_.numCells[1]);
  std::string knownCellsBoxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("known_cells_box")->GetText();
  sscanf(knownCellsBoxStr.c_str(), "%d %d %d %d", &node_.knownCellsBox[0], &node_.knownCellsBox[1],
          &node_.knownCellsBox[2], &node_.knownCellsBox[3]);
  node_.resolution = doc.FirstChildElement("probability_grid")->FirstChildElement("resolution")->DoubleText();
  return true;
}
std::vector<file_data_type::MapData> nodeList_;
bool GetXml(const string& path)
{
  XMLDocument doc;
  string file_path = path + "map.xml";
  cout<<file_path<<endl;
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  cout<<file_path<<endl;
  XMLElement *pmap = doc.RootElement();
  cout<<file_path<<endl;
  XMLElement *pnode_list = pmap->FirstChildElement("node_list");
  cout<<file_path<<endl;
  XMLElement *pproperty = pmap->FirstChildElement("property");
  cout<<file_path<< 5<<endl;
  XMLElement *pnode = pnode_list->FirstChildElement();
  cout<<file_path<< 6<<endl;
  file_data_type::MapData tmpNode;
  file_data_type::NeighbourInMapData neighbourInNode;
  nodeList_.clear();
  while(pnode != NULL)
  {
    tmpNode.id = pnode->FirstChildElement("id")->IntText();
    cout << "tmpNode.id" << tmpNode.id <<endl;
    XMLElement *pneighbour_list = pnode->FirstChildElement("neighbour_list");
    XMLElement *pneighbour = pneighbour_list->FirstChildElement();
    while(pneighbour != NULL)
    {
      neighbourInNode.id = pneighbour->FirstChildElement("id")->IntText();
      cout << "neighbourInNode.id" << neighbourInNode.id<<endl;
      string tmpPoseStr = pneighbour->FirstChildElement("transform")->GetText();
      cout << "neighbourInNode.transform" << tmpPoseStr<<endl;
      stringutils::PoseStringToXml(tmpPoseStr, neighbourInNode.transform, true);
      neighbourInNode.reachable = pneighbour->FirstChildElement("reachable")->BoolText();
      pneighbour = pneighbour->NextSiblingElement();
      tmpNode.neighbourList.emplace_back(std::move(neighbourInNode));
    }
    nodeList_.emplace_back(std::move(tmpNode));
    pnode = pnode->NextSiblingElement();
  }
//   this->nodeCount_=this->nodeList_.size();
  return true;
}


bool SetXml(const string& path)
{
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  XMLElement *pmap= doc.NewElement("map");
  doc.InsertFirstChild(pmap);
  doc.InsertEndChild(pmap);
  
  XMLElement *pnode_list= doc.NewElement("node_list");
  pmap->InsertFirstChild(pnode_list);
  pmap->InsertEndChild(pnode_list);
  
  for (const auto &node : nodeList_)
  {
    XMLElement *pnode= doc.NewElement("node");
    pnode_list->InsertFirstChild(pnode);
    pnode_list->InsertEndChild(pnode);
    setXmlElement(doc, pnode, "id", std::to_string(node.id));
    
    XMLElement *pneighbour_list= doc.NewElement("neighbour_list");
    pnode->InsertFirstChild(pneighbour_list);
    pnode->InsertEndChild(pneighbour_list);
    for (const auto &neighbour : node.neighbourList)
    {
      XMLElement *pneighbour= doc.NewElement("neighbour");
      pneighbour_list->InsertFirstChild(pneighbour);
      pneighbour_list->InsertEndChild(pneighbour);
    
      setXmlElement(doc, pneighbour, "id", std::to_string(neighbour.id));
      std::string transformStr;
      stringutils::PoseXmlToString(neighbour.transform, transformStr, true);
      setXmlElement(doc, pneighbour, "transform", transformStr);
      setXmlElement(doc, pneighbour, "reachable", neighbour.reachable?"true":"false");
    }
    
  }
  XMLElement *pproperty= doc.NewElement("property");
  pmap->InsertFirstChild(pproperty);
  pmap->InsertEndChild(pproperty);
  setXmlElement(doc, pproperty, "node_count", std::to_string(nodeList_.size()));
  return doc.SaveFile("map.xml");
}

std::vector<file_data_type::LandmarkInfo> nodeLandmarkList_;
bool SetLandmarkXml(const string& path)
{  
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  for(const auto &node : nodeLandmarkList_)
  {
    XMLElement *plandmark = doc.NewElement("landmark");
    doc.InsertFirstChild(plandmark);
    doc.InsertEndChild(plandmark);
    setXmlElement(doc, plandmark, "ns", node.ns);
    setXmlElement(doc, plandmark, "id", node.id);
    setXmlElement(doc, plandmark, "pole_radius", std::to_string(node.pole_radius));
    setXmlElement(doc, plandmark, "visible", std::to_string(node.visible));
    std::string transformStr;
    stringutils::PoseXmlToString(node.transform, transformStr);
    setXmlElement(doc, plandmark, "transform", transformStr);
    std::string translationInImageStr = std::to_string(node.translation_in_image[0]) + " " +
    std::to_string(node.translation_in_image[1]);
    setXmlElement(doc, plandmark, "translation_in_image", translationInImageStr);
  }
//   string save_path = path + "/landmark.xml";
//   cout << "save_path: " << save_path <<endl;
  return doc.SaveFile("landmark.xml"); 
}

bool GetlandmarkXml(const string& path)
{
  XMLDocument doc;
  string file_path = path + "/landmark.xml";
  cout << "file_path: " << file_path <<endl;
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  XMLElement *plandmark = doc.FirstChildElement("landmark");
  file_data_type::LandmarkInfo mark_info;
  std::string tmpTranslationInImageStr;
  while(plandmark != NULL)
  {
    mark_info.ns = plandmark->FirstChildElement("ns")->GetText();
    mark_info.id = plandmark->FirstChildElement("id")->GetText();
    if(plandmark->FirstChildElement("pole_radius") == NULL)
      mark_info.pole_radius = 0;
    else
      mark_info.pole_radius = plandmark->FirstChildElement("pole_radius")->FloatText();
    
    mark_info.visible = plandmark->FirstChildElement("visible")->IntText();
    std::string tmpPoseStr = plandmark->FirstChildElement("transform")->GetText();
    
    if(plandmark->FirstChildElement("translation_in_image") == NULL)
      tmpTranslationInImageStr = "0 0";
    else
      tmpTranslationInImageStr = plandmark->FirstChildElement("translation_in_image")->GetText();
    plandmark->FirstChildElement();
    stringutils::PoseStringToXml(tmpPoseStr, mark_info.transform);
    std::sscanf(tmpTranslationInImageStr.c_str(), "%lf %lf",
      &mark_info.translation_in_image[0], &mark_info.translation_in_image[1]);
    nodeLandmarkList_.emplace_back(std::move(mark_info));
    plandmark = plandmark->NextSiblingElement();
  }
//   nodeCount_ = nodeList_.size();
  return true;
}

// int createXML(const char * xmlPath)
// {
//   XMLDocument doc;
// //   if(XML_ERROR_FILE_NOT_FOUND != doc.LoadFile(xmlPath))
// //   {
// //     cout<<"file is exits！"<<endl;
// //     return 0;
// //   }
//   //1.添加声明 方法一
//   //const char* declaration ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
//   //doc.Parse(declaration);//会覆盖xml所有内容
//   //2.添加声明 方法二
//   //<?xml version="1.0" encoding="UTF-8"?>
//   XMLDeclaration *declaration = doc.NewDeclaration();
//   doc.InsertFirstChild(declaration);
//   setXmlElement(doc, "id", "0");
//   setXmlElement(doc, "pose", "0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 1.000000");
//   setXmlElement(doc, "local_pose", "0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 1.000000");
//   setXmlElement(doc, "num_range_data", "286");
//   setXmlElement(doc, "finished", "true");
// //   XMLElement *root = doc.NewElement("id");
// //   
// //   doc.InsertFirstChild(root);
// //   doc.InsertEndChild(root);
// //   root->InsertFirstChild(doc.NewText("0"));
//   //3.新建root根节点
// //   XMLElement *root = doc.NewElement("id");
// //   root->SetAttribute();
// //   doc.InsertFirstChild(root);
// //   doc.InsertEndChild(root);
//   //4.给root添加节点
// //   for(int i = 0; i < 10; i++)
// //   {
// //     XMLElement* userNode = doc.NewElement("person");
// //     //4.1设置属性
// //     userNode->SetAttribute("name","MenAngel");
// //     userNode->SetAttribute("passwd","111");
// //     //4.2设置开始标签及文本值
// //     userNode->InsertFirstChild(doc.NewText("sunjimeng"));
// //     //4.3设置结束标签
// //     root->InsertEndChild(userNode);
// //   }
//   return doc.SaveFile(xmlPath);
// }
int addXML(const char *xmlPath)
{
  XMLDocument doc;
  if( XML_SUCCESS != doc.LoadFile(xmlPath))
  {
    cout<<"load xml file failed"<<endl;
    return -1;
  }
  //再添加一个没有属性的name = MenAngel的 person节点
  XMLElement *newPerson = doc.NewElement("person");
  newPerson->InsertFirstChild(doc.NewText("MenAngel"));
  XMLElement *root = doc.RootElement();
  root->InsertEndChild(newPerson);
  return doc.SaveFile(xmlPath);
}
int modifyXML(const char *xmlPath)
{
  XMLDocument doc;
  if( XML_SUCCESS != doc.LoadFile(xmlPath))
  {
    cout<<"load xml file failed"<<endl;
    return -1;
  }
  //1.获得根节点
  XMLElement *root = doc.RootElement();
  //2.获得根节点root指定名称的第一个子元素
  //获得根节点第一个子元素 root->FirstChild();
  XMLElement *newPerson = root->FirstChildElement("person");
  newPerson->SetAttribute("name","user");
  newPerson->SetText("MenAngel");
  newPerson->DeleteAttribute("passwd");
  //3.查询属性及值
  cout<<"<person> name = "<<newPerson->Attribute("name")<<" value = "<<newPerson->GetText()<<endl;
  newPerson = newPerson->NextSiblingElement();
  root->DeleteChild(newPerson);
  return doc.SaveFile(xmlPath);
}
int main(int argv,char *argc[])
{
//   GetLaserDataXml("/home/cyy/map/2d_0707_xu/frames/0/");
//   SetLaserDataXml("");
//   GetXml("/home/cyy/map/2d_0707_xu/");
//   SetXml("");
  GetlandmarkXml("/home/cyy/map/sy28_remap");
  SetLandmarkXml("");
//   XMLDocument doc;
//   if(XML_SUCCESS != doc.LoadFile("/home/cyy/map/2d_0707_xu/frames/0/data.xml"))
//   {
//     cout<<"load xml file failed!"<<endl;
//     return 0;
//   }
//   XMLElement *root = doc.RootElement();
//   int id = std::atoi(doc.FirstChildElement("id")->GetText());
//   bool finished = doc.FirstChildElement("finished")->BoolText();
//   double resolution = doc.FirstChildElement("probability_grid")->FirstChildElement("resolution")->DoubleText();
//   cout<<"id "<<id<<endl;
//   cout << finished <<endl;
//   cout<<"pose "<<doc.FirstChildElement("pose")->GetText()<<endl;
//   cout<<"resolution "<<resolution<<endl;
//   int v1;
//   const char *s1 = new char[];
//   //查询取值
//   ptrElement->FirstChildElement("score")->QueryIntText(&v1);
//   cout<<"root scores value score = "<<v1<<endl;
//   //查询取属性值
//   doc.RootElement()->FirstChildElement("scores")->QueryStringAttribute("type",&s1);
//   const char * filename = "create.xml";
//   int iRet = createXML(filename);
//   if(iRet != XML_SUCCESS)
//     cout<<"create xml fail!"<<endl;
//   iRet = addXML(filename);
//   if(iRet != XML_SUCCESS)
//     cout<<"add xml fail!"<<endl;
//   iRet = modifyXML(filename);
//   if(iRet != XML_SUCCESS)
//     cout<<"modify or delte xml fail!"<<endl;
  return 1;
}
