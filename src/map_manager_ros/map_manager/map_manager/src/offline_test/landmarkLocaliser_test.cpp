/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "landmarkLocaliser_test.h"

#include <cartographer/common/time.h>
#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/mapping/2d/probability_grid_range_data_inserter_2d.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/internal/optimization/cost_functions/spa_cost_function_2d.h>
#include <cartographer/mapping/internal/optimization/optimization_problem_2d.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/timestamped_transform.h>
#include <cartographer/transform/transform.h>
#include <cartographer_ros/node_options.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/registration/icp.h>
#include <ros/package.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "cartographer/common/ceres_solver_options.h"
#include "map_interface.h"
#include "map_manager_ulitity.h"
#include "utility/point_optimization_ceres.h"

using namespace std;
using namespace cv;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager
{

inline std::array<double, 3> FromPose(const transform::Rigid2d &pose)
{
  return {{pose.translation().x(), pose.translation().y(), pose.normalized_angle()}};
}

void LandmarkTest::start(const string &origin_path)
{
  LOG(INFO) << "start..";
  origin_path1_ = origin_path;

  loadPbstream(origin_path1_);
  LOG(INFO) << origin_path1_;

  LOG(INFO) << "loadPbstream done..";

  {
    for (const auto &it : node_constant_datas_)
    {
      optimization::NodeSpec2D node_data{it.data.time, transform::Project2D(it.data.local_pose),
                                         transform::Project2D(node_poses.at(it.id)),
                                         it.data.gravity_alignment};
      node_data_.Insert(it.id, node_data);
    }
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud_frame_ptr(new pcl::PointCloud<pcl::PointXYZ>);

  pcl::PointCloud<pcl::PointXYZ> map_cloud;
  pcl::PointCloud<pcl::PointXYZ> map_cloud_frame;

  // XML
  std::string xml_data;
  boost::property_tree::ptree p_landmark_info;
  boost::property_tree::ptree p_neighbour_list;
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  // -----------------------------------------------------------------------

  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;

      const auto &begin_of_trajectory = node_data_.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.
      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data_.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation was made, but the next trajectory node has
      // not been added yet.
      if (next == node_data_.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;

      transform::Rigid3d mark_to_base = observation.landmark_to_tracking_transform;

      transform::Rigid3d pose_to_local_3d =
          transform::Embed3D(node_data_.at(prev->id).local_pose_2d) *
          transform::Rigid3d::Rotation(node_data_.at(prev->id).gravity_alignment);
      transform::Rigid3d pose_to_global_3d =
          transform::Embed3D(node_data_.at(prev->id).global_pose_2d) *
          transform::Rigid3d::Rotation(node_data_.at(prev->id).gravity_alignment);

      transform::Rigid3d base_to_global =
          pose_to_global_3d * pose_to_local_3d.inverse() * base_to_local;

      for (auto it : observation.observation_points)
      {
        Eigen::Vector3d trans = base_to_global.translation();
        Eigen::Matrix3d rotat = base_to_global.rotation().toRotationMatrix();

        Eigen::Vector3d temp_result;
        temp_result(0) = it(0);
        temp_result(1) = it(1);
        temp_result(2) = it(2);

        temp_result = rotat * temp_result + trans;

        pcl::PointXYZ temp_point;
        temp_point.x = temp_result(0);
        temp_point.y = temp_result(1);
        temp_point.z = temp_result(2);
        map_cloud.points.push_back(temp_point);

        {
          int id_i = stoi(landmark_id);
          p_landmark_info.put("id", id_i);
          for (int i = 0; i < 3; i++)
            xml_data = xml_data + std::to_string(temp_result[i]) + " ";

          p_landmark_info.put("point", xml_data);

          p_neighbour_list.add_child("global_observation", p_landmark_info);

          xml_data.clear();
        }
      }

      cout << "landmark id: " << landmark_id << " "
           << "observation time: " << observation.time << endl;
    }
  }

  // save XML
  {
    boost::property_tree::write_xml(origin_path1_ + "/landmark_new.xml", p_neighbour_list,
                                    std::locale(), setting);
  }

  int64 start_time = 637992482581103500;
  Eigen::Matrix4d prior_T = Eigen::Matrix4d::Identity();

  int flag = 0;
  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;

      const auto &begin_of_trajectory = node_data_.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.
      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data_.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation was made, but the next trajectory node has
      // not been added yet.
      if (next == node_data_.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;

      transform::Rigid3d mark_to_base = observation.landmark_to_tracking_transform;

      transform::Rigid3d pose_to_local_3d =
          transform::Embed3D(node_data_.at(prev->id).local_pose_2d) *
          transform::Rigid3d::Rotation(node_data_.at(prev->id).gravity_alignment);
      transform::Rigid3d pose_to_global_3d =
          transform::Embed3D(node_data_.at(prev->id).global_pose_2d) *
          transform::Rigid3d::Rotation(node_data_.at(prev->id).gravity_alignment);

      transform::Rigid3d base_to_global =
          pose_to_global_3d * pose_to_local_3d.inverse() * base_to_local;

      int64 temp_time = common::ToUniversal(observation.time);

      double diff_time = (temp_time - start_time) / 1000000000.0;

      if (diff_time > 0 && diff_time < 0.01)
      {
        if (flag == 0)
        {
          Eigen::Vector3d trans = base_to_global.translation();
          Eigen::Matrix3d rotat = base_to_global.rotation().toRotationMatrix();
          prior_T.block(0, 0, 3, 3) = rotat;
          prior_T.block(0, 3, 3, 1) = trans;
        }

        flag++;

        cout << "landmark id: " << landmark_id << endl;
        cout << " dif time: " << diff_time << endl;

        for (auto it : observation.observation_points)
        {
          Eigen::Vector3d trans = base_to_global.translation();
          Eigen::Matrix3d rotat = base_to_global.rotation().toRotationMatrix();

          cout << "prior_T now: "
               << "\n"
               << rotat << "\n"
               << trans << endl;

          Eigen::Vector3d temp_result;
          temp_result(0) = it(0);
          temp_result(1) = it(1);
          temp_result(2) = it(2);
          // 	  temp_result=rotat*temp_result+trans;

          pcl::PointXYZ temp_point;
          temp_point.x = temp_result(0);
          temp_point.y = temp_result(1);
          temp_point.z = temp_result(2);
          map_cloud_frame.points.push_back(temp_point);
        }
      }
    }
  }

  cout << "prior_T ori: "
       << "\n"
       << prior_T << endl;

  map_cloud_ptr = map_cloud.makeShared();
  map_cloud_frame_ptr = map_cloud_frame.makeShared();

  prior_T(0, 3) = prior_T(0, 3);
  prior_T(1, 3) = prior_T(1, 3);

  cout << "prior_T add noise: "
       << "\n"
       << prior_T << endl;

  if (1)
  {
    Eigen::Matrix4f prior_T_f = Eigen::Matrix4f::Identity();

    for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        prior_T_f(i, j) = prior_T(i, j);
      }
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr icp_result_point_cloud_ptr(
        new pcl::PointCloud<pcl::PointXYZ>);
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    icp.setMaximumIterations(30);
    icp.setTransformationEpsilon(1e-8);
    icp.setMaxCorrespondenceDistance(1);
    icp.setInputSource(map_cloud_frame_ptr);
    icp.setInputTarget(map_cloud_ptr);

    icp.align(*icp_result_point_cloud_ptr, prior_T_f);

    Eigen::Matrix4f icp_result_T = icp.getFinalTransformation();

    cout << icp_result_T << endl;
    for (int i = 0; i < 4; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        prior_T(i, j) = icp_result_T(i, j);
      }
    }

    cout << "prior_T final: "
         << "\n"
         << prior_T << endl;
  }

  map_cloud_ptr->height = 1;
  map_cloud_ptr->width = map_cloud_ptr->size();

  pcl::PCDWriter writer;
  writer.write(origin_path1_ + "/test.pcd", *map_cloud_ptr);

  pcl::PLYWriter writer_ply;
  writer_ply.write(origin_path1_ + "/test.ply", *map_cloud_ptr);

  // show
  {
    PbstreamInfos infos;

    pcl::PointCloud<pcl::PointXYZ> pic_cloud;
    pcl::PointCloud<pcl::PointXYZ> pic_cloud_frame;

    cv::Mat cells_mat;

    readPointsFromPbstream(origin_path1_, infos, pic_cloud, pic_cloud_frame, prior_T, cells_mat);
    cout << "prior_T: "
         << "\n"
         << prior_T << endl;
    addPointsInPicture_ori(map_cloud, prior_T, cells_mat, 2);

    //     addPointsInPicture_ori(map_cloud_frame,prior_T,cells_mat,1);
    cout << "prior_T: "
         << "\n"
         << prior_T << endl;
    addPointsInPicture(map_cloud_frame, prior_T, cells_mat, 1);

    string img_name = origin_path1_ + "/my_test.png";

    cv::imwrite(img_name, cells_mat);

    cout << "save done!" << endl;

    cv::imshow("Display window", cells_mat);

    cells_mat.release();

    cv::waitKey(0);
  }

  // read xml
  {
    vector<Eigen::Vector3d> global_points;

    boost::property_tree::ptree global_observation;
    boost::property_tree::read_xml(origin_path1_ + "/landmark_new.xml", global_observation);
    for (auto &point : global_observation)
    {
      std::string xml_id = point.second.get("id", " ");
      int id = std::stoi(xml_id);

      std::string data = point.second.get("point", " ");
      float pose[3];
      sscanf(data.c_str(), "%f %f %f", &pose[0], &pose[1], &pose[2]);

      Eigen::Vector3d temp_point;
      temp_point(0) = pose[0];
      temp_point(1) = pose[1];
      temp_point(2) = pose[2];

      global_points.push_back(temp_point);
    }

    cout << "size of global points: " << global_points.size() << endl;
  }
}

void LandmarkTest::loadPbstream(const string &path)
{
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);

  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();

  map<int, int> ignore_trajectory;
  kd_tree::poseVec poses;
  int update_node_index = 0;
  for (const mapping::proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(mapping::NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
      poses.push_back({global_pose.translation(), global_pose.rotation(),
                       Eigen::Vector2i(0, update_node_index++)});
    }
  }

  kd_tree_.reset(new kd_tree::KDTree(poses));

  {
    boost::property_tree::ptree mark_pt;
    boost::property_tree::read_xml(path + "/landmark.xml", mark_pt);
    for (auto &mark : mark_pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (visible)
        {
          std::string ns = mark.second.get("ns", "");
          std::string xml_id = mark.second.get("id", " ");
          int id = std::stoi(xml_id);
          if (ns == "Lasermarks")
          {
            id += 1000000;
          }
          else if (ns == "Scenes")
          {
            id += 5000000;
          };

          std::string data = mark.second.get("transform", " ");
          float pose[7];
          sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3],
                 &pose[4], &pose[5], &pose[6]);

          Eigen::Vector3d temp_trans;
          temp_trans(0) = pose[0];
          temp_trans(1) = pose[1];
          temp_trans(2) = pose[2];
          Eigen::Quaterniond temp_quat;
          temp_quat.x() = pose[3];
          temp_quat.y() = pose[4];
          temp_quat.z() = pose[5];
          temp_quat.w() = pose[6];

          transform::Rigid3d temp_pose(temp_trans, temp_quat);
          landmarks_[std::to_string(id)] = temp_pose;
        }
      }
    }
  }

  mapping::proto::SerializedData proto_tmp;

  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case mapping::proto::SerializedData::kNode:
    {
      mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      proto_node->mutable_node_id()->set_trajectory_id(0);
      const mapping::proto::TrajectoryNodeData proto_node_data = *proto_node->mutable_node_data();
      const mapping::TrajectoryNode::Data node_data = mapping::FromProto(proto_node_data);
      mapping::NodeId id(0, proto_node->node_id().node_index());

      node_constant_datas_.Insert(id, node_data);
      //         new_node_id_to_node_.Insert(id, proto_tmp.node());

      break;
    }
    case mapping::proto::SerializedData::kSubmap:
    {
      break;
    }
    case mapping::proto::SerializedData::kImuData:
    {
      break;
    }
    case mapping::proto::SerializedData::kOdometryData:
    {
      break;
    }
    case mapping::proto::SerializedData::kFixedFramePoseData:
    {
      break;
    }
    case mapping::proto::SerializedData::kLandmarkData:
    {
      landmark_datas_.push_back(sensor::FromProto(proto_tmp.landmark_data().landmark_data()));
      break;
    }
    }
  }

  int count = 0;

  for (sensor::LandmarkData landmark_data : landmark_datas_)
  {
    for (const auto &observation : landmark_data.landmark_observations)
    {
      // TODO observation.pole_radius
      landmark_nodes[observation.id].landmark_observations.emplace_back(
          PoseGraphInterface::LandmarkNode::LandmarkObservation{
              0, landmark_data.time, observation.landmark_to_tracking_transform,
              observation.tranking_to_local_transform, 1000 * observation.translation_weight,
              1000 * observation.rotation_weight, observation.observation_points,
              observation.pole_radius});
      cout << "count: " << count << " "
           << "id: " << observation.id << " "
           << "num of points: " << observation.observation_points.size() << endl;
    }
    count++;
  }

  int flag = 0;
}

void LandmarkTest::readPointsFromPbstream(const std::string &path, PbstreamInfos &infos,
                                          pcl::PointCloud<pcl::PointXYZ> &cloud_all,
                                          pcl::PointCloud<pcl::PointXYZ> &cloud_frame,
                                          Eigen::Matrix4d &prior_T, cv::Mat &cells_mat)
{
  readPosesFromPbstream(path, infos);

  sensor::PointCloud all_points;

  int count_0 = 0;
  for (const auto &it : infos.node_poses)
  {
    const transform::Rigid3d gravity_alignment =
        transform::Rigid3d::Rotation(infos.node_datas.at(it.id).gravity_alignment);
    transform::Rigid3d node_global_pose = it.data * gravity_alignment.inverse();
    const sensor::PointCloud points =
        infos.node_datas.at(it.id).filtered_gravity_aligned_point_cloud;
    sensor::PointCloud points_in_global =
        sensor::TransformPointCloud(points, node_global_pose.cast<float>());
    all_points.insert(all_points.end(), points_in_global.begin(), points_in_global.end());

    if (count_0 == 0)
    {
      Eigen::Vector3d trans = node_global_pose.translation();
      Eigen::Matrix3d rotat = node_global_pose.rotation().toRotationMatrix();
      //       prior_T.block(0,0,3,3)=rotat;
      //       prior_T.block(0,3,3,1)=trans;

      for (const auto &iter : points)
      {
        pcl::PointXYZ temp_point;
        temp_point.x = iter.position.x();
        temp_point.y = iter.position.y();
        temp_point.z = iter.position.z();
        cloud_frame.points.push_back(temp_point);
      }
    }
    count_0++;
  }
  cout << "all_points" << all_points.size() << endl;

  sensor::PointCloud filtered_points = sensor::VoxelFilter(0.05).Filter(all_points);
  //   sensor::PointCloud filtered_points = all_points;

  cout << "filtered_points" << filtered_points.size() << endl;

  x_min = filtered_points[0].position.x();
  float x_max = filtered_points[0].position.x();
  y_min = filtered_points[0].position.y();
  float y_max = filtered_points[0].position.y();

  for (const auto &it : filtered_points)
  {
    float temp_x = it.position.x();
    float temp_y = it.position.y();
    if (temp_x < x_min)
    {
      x_min = temp_x;
    }
    if (temp_x > x_max)
    {
      x_max = temp_x;
    }
    if (temp_y < y_min)
    {
      y_min = temp_y;
    }
    if (temp_y > y_max)
    {
      y_max = temp_y;
    }
  }

  cout << x_min << endl;
  cout << x_max << endl;
  cout << y_min << endl;
  cout << y_max << endl;

  //   float resolution=0.05;
  num_x = (x_max - x_min) / resolution;
  num_y = (y_max - y_min) / resolution;

  cells_mat = cv::Mat::zeros(num_x + 1, num_y + 1, CV_8UC3);

  for (const auto &it : filtered_points)
  {
    int row = (it.position.x() - x_min) / resolution;
    int col = (it.position.y() - y_min) / resolution;

    cells_mat.at<cv::Vec3b>(row, col)[0] = 255;
    cells_mat.at<cv::Vec3b>(row, col)[1] = 255;
    cells_mat.at<cv::Vec3b>(row, col)[2] = 255;
  }

  // get the date of point
  for (const auto &it : filtered_points)
  {
    pcl::PointXYZ temp_point;
    temp_point.x = it.position.x();
    temp_point.y = it.position.y();
    temp_point.z = it.position.z();
    cloud_all.points.push_back(temp_point);
  }
}

void LandmarkTest::addPointsInPicture_ori(pcl::PointCloud<pcl::PointXYZ> &cloud_frame,
                                          Eigen::Matrix4d &T, cv::Mat &cells_mat, int color)
{
  Eigen::Vector3d t = T.block(0, 3, 3, 1);
  Eigen::Matrix3d r = T.block(0, 0, 3, 3);

  for (int i = 0; i < cloud_frame.points.size(); ++i)
  {
    Eigen::Vector3d temp_point;
    temp_point(0) = cloud_frame.points[i].x;
    temp_point(1) = cloud_frame.points[i].y;
    temp_point(2) = cloud_frame.points[i].z;

    Eigen::Vector3d global_point;
    global_point = temp_point;

    int row = (global_point.x() - x_min) / resolution;
    int col = (global_point.y() - y_min) / resolution;

    cells_mat.at<cv::Vec3b>(row, col)[0] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[1] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[2] = 0;

    cells_mat.at<cv::Vec3b>(row, col)[color] = 255;
  }
}

void LandmarkTest::addPointsInPicture(pcl::PointCloud<pcl::PointXYZ> &cloud_frame,
                                      Eigen::Matrix4d &T, cv::Mat &cells_mat, int color)
{
  Eigen::Vector3d t = T.block(0, 3, 3, 1);
  Eigen::Matrix3d r = T.block(0, 0, 3, 3);

  for (int i = 0; i < cloud_frame.points.size(); ++i)
  {
    Eigen::Vector3d temp_point;
    temp_point(0) = cloud_frame.points[i].x;
    temp_point(1) = cloud_frame.points[i].y;
    temp_point(2) = cloud_frame.points[i].z;

    Eigen::Vector3d global_point;
    global_point = r * temp_point + t;

    int row = (global_point.x() - x_min) / resolution;
    int col = (global_point.y() - y_min) / resolution;

    cells_mat.at<cv::Vec3b>(row, col)[0] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[1] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[2] = 0;

    cells_mat.at<cv::Vec3b>(row, col)[color] = 255;
  }
}

} // namespace map_manager

int main(int argc, char **argv)
{
  //   const std::string path0="/home/shao/map/landmarktest";

  const std::string path0 = "/home/shao/map/pxcc";

  map_manager::LandmarkTest landmark_test;

  landmark_test.start(path0);

  return 0;
}