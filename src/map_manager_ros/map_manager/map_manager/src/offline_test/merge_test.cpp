/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "merge_test.h"

#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/mapping/2d/probability_grid_range_data_inserter_2d.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/internal/optimization/cost_functions/spa_cost_function_2d.h>
#include <cartographer/mapping/internal/optimization/optimization_problem_2d.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/transform.h>
#include <cartographer_ros/node_options.h>
#include <ros/package.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "cartographer/common/ceres_solver_options.h"
#include "map_interface.h"
#include "map_manager_ulitity.h"
#include "utility/point_optimization_ceres.h"

using namespace std;
using namespace cv;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager
{

inline std::array<double, 3> FromPose(const transform::Rigid2d &pose)
{
  return {{pose.translation().x(), pose.translation().y(), pose.normalized_angle()}};
}

inline transform::Rigid2d ToPose(const std::array<double, 3> &values)
{
  return transform::Rigid2d({values[0], values[1]}, values[2]);
}

MergeTest::MergeTest() : aligned_submap_id_(0, 0), merge_map_(1, 1, CV_16UC1)
{
  options_.mutable_ceres_solver_options()->set_max_num_iterations(10);
  options_.mutable_ceres_solver_options()->set_use_nonmonotonic_steps(false);
  options_.mutable_ceres_solver_options()->set_num_threads(2);
  options_.set_odometry_rotation_weight(1e1);
  options_.set_huber_scale(1e1);
  options_.set_local_slam_pose_translation_weight(1e5);
  options_.set_local_slam_pose_rotation_weight(1e5);
  options_.set_log_solver_summary(false);
  options_.set_use_online_imu_extrinsics_in_3d(false);
}

MergeTest::~MergeTest() {}

// void MergeTest::saveLandmarkDataToXML(const string&
// path,std::map<std::string, cartographer::transform::Rigid3d>& save_landmarks)
// {
//   boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
//   std::string data;
//   boost::property_tree::ptree p_landmark_info;
//   boost::property_tree::ptree p_neighbour_list;
//
//   for (const auto& landmark : save_landmarks_)
//   {
//     transform::Rigid3d::Vector translation;
//     transform::Rigid3d::Quaternion quaternion;
//     std::string ns;
//     std::string  id_s = landmark.first;
//     int id_i = stoi(id_s);
//     if(id_i >= 5000000)
//     {
//       id_i = id_i - 5000000;
//       ns = "Scenes";
//     }
//     else if (id_i >= 1000000) {
//       id_i = id_i -1000000;
//       ns = "Lasermarks";
//     }
//     else if(id_i >= 44032)
//       ns = "Visualmarks_4x4";
//     else
//       ns = "Visualmarks";
//
//
//     p_landmark_info.put("ns", ns);
//     p_landmark_info.put("id", id_i);
//     p_landmark_info.put("pole_radius", 0.0375);
//     p_landmark_info.put("visible", 1);
//     transform::Rigid3d landmark_pose = landmark.second;
//     translation = landmark_pose.translation();
//     quaternion = landmark_pose.rotation();
//     for (int i = 0; i < 3; i++)
//       data = data + std::to_string(translation[i]) + " ";
//     for (int i = 0; i < 4; i++)
//       data = data + std::to_string(quaternion.coeffs()[i]) + " ";
//     p_landmark_info.put("transform", data);
//     p_neighbour_list.add_child("landmark", p_landmark_info);
//     data.clear();
//   }
//   boost::property_tree::write_xml("/home/shao/myTemp/landmark.xml",
//   p_neighbour_list, std::locale(),
// 				  setting);
// }

void MergeTest::start(const string &origin_path, const std::string &new_path,
                      const transform::Rigid3d &init_map_pose)
{
  LOG(INFO) << "start..";
  origin_path1_ = origin_path;
  new_path_ = new_path;
  loadSubamp(origin_path);
  //   loadPbstreamTest(origin_path);

  LOG(INFO) << "loadSubamp done..";

  loadPbstream(new_path);
  LOG(INFO) << new_path;

  LOG(INFO) << "loadPbstream done..";
  LOG(INFO) << "trajectory_id: " << new_trajectory_id_;

  //--------------------------------------------------
  int max_idx = 0;
  int ini_max_idx = 0;
  for (const auto &it : new_landmarks_ori_)
  {
    save_landmarks_ori_[it.first] = it.second;

    std::string mark_id = it.first;
    int landmark_id_int = std::stoi(mark_id);
    if (landmark_id_int > max_idx)
    {
      max_idx = landmark_id_int;
    }
  }
  ini_max_idx = max_idx;
  std::map<std::string, std::string> updateId;
  cout << "new_landmarks_ size: " << new_landmarks_.size() << endl;

  //----------------------------------------------------

  bool first_matched_flag = false;
  transform::Rigid3d relative_pose = init_map_pose;
  LOG(INFO) << "constraints size: " << constraints_.size();
  for (const auto &it : node_poses_)
  {
    kd_tree::pose pose{it.data.translation(), Eigen::Quaterniond::Identity(),
                       Eigen::Vector2i(0, 0)};
    kd_tree::pose result = kd_tree_->nearest_point(pose);
    if ((result.p - it.data.translation()).norm() > 5.) continue;
    transform::Rigid3d init_pose;
    transform::Rigid2d pose_estimate;
    transform::Rigid3d gravity_align =
        transform::Rigid3d::Rotation(node_constant_datas_.at(it.id).gravity_alignment);
    init_pose = relative_pose * it.data * gravity_align.inverse();
    bool success = match(node_constant_datas_.at(it.id).filtered_gravity_aligned_point_cloud,
                         transform::Project2D(init_pose), pose_estimate);
    transform::Rigid3d pose_estimate_3d = transform::Embed3D(pose_estimate);
    if (!success) continue;
    //     LOG(INFO) << "match node "<<it.id << " success: " <<pose_estimate;
    //     if(!first_matched_flag)
    {
      first_matched_flag = true;
      relative_pose = pose_estimate_3d * (it.data * gravity_align.inverse()).inverse();
      for (const sensor::RangefinderPoint &p :
           node_constant_datas_.at(it.id).filtered_gravity_aligned_point_cloud)
      {
        Eigen::Vector3f p2 = pose_estimate_3d.cast<float>() * p.position;
        float img_y = (maxes_[0] - p2.x()) / 0.05;
        float img_x = (maxes_[1] - p2.y()) / 0.05;
        circle(probability_grid_, {img_x, img_y}, 2, Scalar(255, 0, 0));
      }
      //       init_map_pose = relative_pose;
      //       imwrite("/home/cyy/001.png", probability_grid_);
    }

    // landmark mix
    {
      for (auto it_0 = new_landmarks_.begin(); it_0 != new_landmarks_.end();)
      {
        transform::Rigid3d global_pose = relative_pose * it_0->second;
        transform::Rigid3d temp_pose = relative_pose * it.data * gravity_align.inverse();

        Eigen::Vector3d disOfLandmarkAndNode = global_pose.translation() - temp_pose.translation();

        if (disOfLandmarkAndNode.norm() < 10)
        {
          Eigen::Vector3d trans_0 = global_pose.translation();
          int count = 0;

          for (const auto &it_1 : new_landmarks_ori_)
          {
            transform::Rigid3d global_pose_temp = it_1.second;
            Eigen::Vector3d trans_1 = global_pose_temp.translation();
            Eigen::Vector3d trans_dif = trans_0 - trans_1;

            if (std::stoi(it_0->first) == 1000032)
            {
              cout << "ID: " << it_1.first << "Distance: " << trans_dif.norm() << endl;
            }

            if (trans_dif.norm() < 0.5)
            {
              cout << "match id:" << it_0->first << "," << it_1.first << endl;
              // mix landmark id
              save_landmarks_[it_1.first] = it_0->second;

              updateId[it_0->first] = it_1.first;
              break;
            }
            count++;
          }
          if (count == new_landmarks_ori_.size())
          {
            // add landmark
            max_idx = max_idx + 1;
            string temp_id = std::to_string(max_idx);

            save_landmarks_[temp_id] = it_0->second;
            save_landmarks_ori_[temp_id] = global_pose;

            updateId[it_0->first] = temp_id;
          }
          // delete used landmark
          {
            new_landmarks_.erase(it_0++);
          }
        }

        else
        {
          it_0++;
        }
      }
    }

    //     imwrite(string(getenv("HOME")) + "/debug/" +
    //     to_string(it.id.node_index) +".png", probability_grid_);
    constraints_.push_back(mapping::PoseGraph::Constraint{
        mapping::SubmapId(0, 0), it.id, pose_estimate_3d * gravity_align,
        mapping::PoseGraph::Constraint::Tag::INTER_SUBMAP});
  }

  {
    for (sensor::LandmarkData landmark_data : landmark_datas_)
    {
      for (const auto &observation : landmark_data.landmark_observations)
      {
        if (updateId.count(observation.id))
        {
          // TODO observation.pole_radius
          if (std::stoi(updateId[observation.id]) > ini_max_idx)
          {
            landmark_nodes[updateId[observation.id]].landmark_observations.emplace_back(
                PoseGraphInterface::LandmarkNode::LandmarkObservation{
                    new_trajectory_id_, landmark_data.time,
                    observation.landmark_to_tracking_transform,
                    observation.tranking_to_local_transform, 1000 * observation.translation_weight,
                    1000 * observation.rotation_weight, observation.observation_points,
                    observation.pole_radius});
          }
          else
          {
            landmark_nodes[updateId[observation.id]].landmark_observations.emplace_back(
                PoseGraphInterface::LandmarkNode::LandmarkObservation{
                    new_trajectory_id_, landmark_data.time,
                    observation.landmark_to_tracking_transform,
                    observation.tranking_to_local_transform, 1000 * observation.translation_weight,
                    1000 * observation.rotation_weight, observation.observation_points,
                    observation.pole_radius});
            landmark_nodes[updateId[observation.id]].frozen = true;
          }

          cout << "observation id: " << observation.id << endl;
          cout << "update id: " << updateId[observation.id] << endl;
        }
      }
    }
  }

  cout << "new_landmarks_ size: " << new_landmarks_.size() << endl;

  LOG(INFO) << "relative_pose: " << transform::Project2D(relative_pose);
  LOG(INFO) << "constraints size: " << constraints_.size();
  LOG(INFO) << "match done..";
  mapping::MapById<mapping::NodeId, transform::Rigid3d> node_poses;
  mapping::MapById<mapping::SubmapId, transform::Rigid3d> submap_poses;
  LOG(INFO) << "node_poses_ size: " << node_poses_.size();
  for (const auto &it : node_poses_)
  {
    node_poses.Insert(it.id, relative_pose * it.data);
  }

  LOG(INFO) << "submap_poses_ size: " << submap_poses_.size();
  for (const auto &it : submap_poses_)
  {
    submap_poses.Insert(it.id, relative_pose * it.data);
  }
  node_poses_ = node_poses;
  submap_poses_ = submap_poses;
  LOG(INFO) << "change done..";
  //   C_submaps.Insert({new_trajectory_id_ +
  //   1,0},FromPose(transform::Rigid2d::Identity())); poseGraph();
  {
    for (const auto &it : node_constant_datas_)
    {
      optimization::NodeSpec2D node_data{it.data.time, transform::Project2D(it.data.local_pose),
                                         transform::Project2D(node_poses.at(it.id)),
                                         it.data.gravity_alignment};
      node_data_.Insert(it.id, node_data);
    }
    //     node_data_.Insert({0,0}, node_data);
    submap_data_.Insert({0, 0}, optimization::SubmapSpec2D{transform::Rigid2d::Identity()});
    for (const auto &it : submap_poses)
    {
      submap_data_.Insert(it.id, optimization::SubmapSpec2D{transform::Project2D(it.data)});
    }
    //     constraints_.push_back(mapping::PoseGraph::Constraint{mapping::SubmapId(0,0),
    //     {0,0}, transform::Rigid3d::Identity() ,
    //     mapping::PoseGraph::Constraint::Tag::INTER_SUBMAP});
    sensor::MapByTime<sensor::OdometryData> odometry_data_;
    for (const auto &it : odom_datas_)
      odometry_data_.Append(1, it);

    LOG(INFO) << "start solveing...";

    //     Solve(constraints_, odometry_data_, node_data_,submap_data_);

    SolveOld(constraints_, odometry_data_, node_data_, submap_data_);

    //     MyTest(landmark_nodes, node_data_);
    //     cout<<"------------------------------------------------"<<endl;
    //     MyTest(landmark_nodes_ori_, node_data_ori_);

    for (const auto &C_submap_id_data : submap_data_)
    {
      if (submap_poses_.find(C_submap_id_data.id) == submap_poses_.end())
      {
        LOG(WARNING) << C_submap_id_data.id;
        continue;
      }
      submap_poses_.at(C_submap_id_data.id) =
          transform::Embed3D((C_submap_id_data.data.global_pose));
    }

    for (const auto &C_node_id_data : node_data_)
    {
      node_poses_.at(C_node_id_data.id) = transform::Embed3D((C_node_id_data.data.global_pose_2d));
      mapping::TrajectoryNode::Data node_data = node_constant_datas_.at(C_node_id_data.id);
      const transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(node_data.gravity_alignment);
      transform::Rigid3f node_global_pose = node_poses_.at(C_node_id_data.id).cast<float>() *
                                            gravity_alignment.inverse().cast<float>();

      const sensor::PointCloud points = node_data.filtered_gravity_aligned_point_cloud;
      sensor::PointCloud points_in_global = sensor::TransformPointCloud(points, node_global_pose);
      transform::Rigid2f global_pose_2d = transform::Project2D(node_global_pose);
      Eigen::Vector2f origin;
      origin = global_pose_2d.translation();
      sensor::RangeData rangedata{{origin(0), origin(1), 0}, points_in_global, {}};
      trajectory_ranges_.push_back(rangedata);
    }
  }
  LOG(INFO) << "poseGraph done..";

  // save landmark infomation
  {
    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    std::string data;
    boost::property_tree::ptree p_landmark_info;
    boost::property_tree::ptree p_neighbour_list;

    for (const auto &landmark : save_landmarks_)
    {
      transform::Rigid3d::Vector translation;
      transform::Rigid3d::Quaternion quaternion;
      std::string ns;
      std::string id_s = landmark.first;
      int id_i = stoi(id_s);
      if (id_i >= 5000000)
      {
        id_i = id_i - 5000000;
        ns = "Scenes";
      }
      else if (id_i >= 1000000)
      {
        id_i = id_i - 1000000;
        ns = "Lasermarks";
      }
      else if (id_i >= 44032)
        ns = "Visualmarks_4x4";
      else
        ns = "Visualmarks";

      p_landmark_info.put("ns", ns);
      p_landmark_info.put("id", id_i);
      p_landmark_info.put("pole_radius", 0.0375);
      p_landmark_info.put("visible", 1);
      transform::Rigid3d landmark_pose = landmark.second;
      translation = landmark_pose.translation();
      quaternion = landmark_pose.rotation();
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      for (int i = 0; i < 4; i++)
        data = data + std::to_string(quaternion.coeffs()[i]) + " ";
      p_landmark_info.put("transform", data);
      p_neighbour_list.add_child("landmark", p_landmark_info);
      data.clear();
    }
    boost::property_tree::write_xml("/home/shao/myTemp/1/landmark.xml", p_neighbour_list,
                                    std::locale(), setting);
  }

  //   std::unique_ptr<mapping::ProbabilityGrid> new_merge_grid;
  //   double last_resolution= 0.05;
  // //   double last_max[2]={-1,-1};
  //   constexpr int kInitialSubmapSize = 20;
  //   mapping::ValueConversionTables new_conversion_tables;
  //   new_merge_grid.reset(new mapping::ProbabilityGrid(
  //           mapping::MapLimits(last_resolution,
  //                        Eigen::Vector2d(0.,0.) + 0.5 * kInitialSubmapSize *
  //                        last_resolution * Eigen::Vector2d::Ones(),
  //                        mapping::CellLimits(kInitialSubmapSize,
  //                        kInitialSubmapSize)),
  //                                                      &new_conversion_tables));
  //
  //   merge_map_info_ = mergeCurTrajectory(submap_probability_grid_);
  //   merge_map_ = merge_map_info_.cells_mat;
  //   new_submap_info_ = mergeCurTrajectory(new_merge_grid.get());
  //   // imwrite(string(getenv("HOME") )+
  //   "/new_submapaaaaa.png",new_submap_info_.cells_mat); LOG(INFO) <<
  //   "mergeCurTrajectory done..";
  //   // save(std::string(std::getenv("HOME")) + "/map/aaaaaaaaa");
  //   // cv::Mat show_img1 = image16ToImage8(merge_map_info_.cells_mat);
  //   imwrite(string(getenv("HOME") )+ "/new_submap.png",merge_map_);
}

void MergeTest::loadSubamp(const std::string &path)
{
  cv::Mat img1 = cv::imread(path + "/map.png");
  cv::line(img1, Point(0, 0), Point(0, 300), cv::Scalar(0, 255, 0), 2);
  cv::line(img1, Point(0, 300), Point(300, 300), cv::Scalar(255, 0, 0), 2);
  cv::line(img1, Point(300, 300), Point(300, 0), cv::Scalar(0, 0, 255), 2);
  cv::line(img1, Point(300, 0), Point(0, 0), cv::Scalar(200, 200, 2), 2);
  imwrite(string(getenv("HOME")) + "/aaaaaaaaaaa.png", img1);

  origin_map_path_ = path;
  //   int num_range_data;
  double resolution;
  Eigen::Vector2d max;
  int num_x_cells;
  int num_y_cells;
  Eigen::AlignedBox2i known_cells_box;
  Eigen::Quaternionf q;
  Eigen::Matrix3f rotMat;
  int min_x, min_y, max_x, max_y;

  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(path + "/frames/0/data.xml", pt);
  std::string data;

  resolution = pt.get<double>("probability_grid.resolution");
  data = pt.get<std::string>("probability_grid.max");
  sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
  data = pt.get<std::string>("probability_grid.num_cells");
  sscanf(data.c_str(), "%d %d", &num_x_cells, &num_y_cells);
  data = pt.get<std::string>("probability_grid.known_cells_box");
  sscanf(data.c_str(), "%d %d %d %d", &min_x, &min_y, &max_x, &max_y);
  known_cells_box =
      Eigen::AlignedBox2i(Eigen::Vector2i(min_x, min_y), Eigen::Vector2i(max_x, max_y));
  maxes_[0] = max[0];
  maxes_[1] = max[1];

  Mat cellMat = imread(path + "/frames/0/probability_grid.png", CV_LOAD_IMAGE_UNCHANGED);
  origin_map_info_.known_cells_box = known_cells_box;
  origin_map_info_.max[0] = max[0];
  origin_map_info_.max[1] = max[1];
  origin_map_info_.cells_mat = cellMat;
  Mat image8(cellMat.rows, cellMat.cols, CV_8UC1);
  for (int i = 0; i < cellMat.rows; i++)
    for (int j = 0; j < cellMat.cols; j++)
    {
      int value = cellMat.at<uint16_t>(i, j);
      if (value == 0)
        image8.at<uchar>(i, j) = 128;
      else
        image8.at<uchar>(i, j) = value / 128;
    }
  cvtColor(image8, probability_grid_, CV_GRAY2BGR);
  // cv::imwrite(std::string(std::getenv("HOME")) + "/dddddddddd.png",image8);
  if (cellMat.data == nullptr)
  {
    LOG(FATAL) << "Load " << path << "/probability_grid.png failed!";
    return;
  }
  mapping::MapLimits limits(resolution, max, mapping::CellLimits(num_x_cells, num_y_cells));

  submap_probability_grid_ = new mapping::ProbabilityGrid(limits, &conversion_tables_);
  submap_probability_grid_->set_known_cells_box(known_cells_box);
  submap_probability_grid_->set_cells(cellMat);
  LOG(WARNING) << 111;
  {
    mapping::scan_matching::proto::FastCorrelativeScanMatcherOptions2D
        fast_correlative_scan_matcher_options;
    fast_correlative_scan_matcher_options.set_linear_search_window(3);
    fast_correlative_scan_matcher_options.set_angular_search_window(15. * M_PI / 180);
    fast_correlative_scan_matcher_options.set_branch_and_bound_depth(5);

    auto submap_scan_matcher =
        absl::make_unique<mapping::scan_matching::FastCorrelativeScanMatcher2D>(
            *submap_probability_grid_, fast_correlative_scan_matcher_options);

    submap_scan_matcher_l_ = {submap_probability_grid_, std::move(submap_scan_matcher)};
  }
  LOG(WARNING) << 111;
  {
    cartographer::common::proto::CeresSolverOptions ceres_solver_options;
    ceres_solver_options.set_use_nonmonotonic_steps(true);
    ceres_solver_options.set_max_num_iterations(10);
    ceres_solver_options.set_num_threads(2);
    LOG(WARNING) << 111;
    cartographer::mapping::scan_matching::proto::CeresScanMatcherOptions2D
        ceres_scan_matcher_options;
    ceres_scan_matcher_options.set_occupied_space_weight(20.);
    ceres_scan_matcher_options.set_translation_weight(10.);
    ceres_scan_matcher_options.set_rotation_weight(1.);
    ceres_scan_matcher_options.set_allocated_ceres_solver_options(&ceres_solver_options);
    LOG(WARNING) << 111;
    ceres_scan_matcher_.reset(
        new mapping::scan_matching::CeresScanMatcher2D(ceres_scan_matcher_options));
    ceres_scan_matcher_options.release_ceres_solver_options();
  }
  LOG(WARNING) << 111;

  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      node_poses;
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);

  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();

  map<int, int> ignore_trajectory;
  kd_tree::poseVec poses;
  int update_node_index = 0;
  for (const mapping::proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(mapping::NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
      poses.push_back({global_pose.translation(), global_pose.rotation(),
                       Eigen::Vector2i(0, update_node_index++)});
    }
  }

  kd_tree_.reset(new kd_tree::KDTree(poses));

  //-----------------------------------------------------------

  {
    boost::property_tree::ptree mark_pt;
    boost::property_tree::read_xml(path + "/landmark.xml", mark_pt);
    for (auto &mark : mark_pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (visible)
        {
          std::string ns = mark.second.get("ns", "");
          std::string xml_id = mark.second.get("id", " ");
          int id = std::stoi(xml_id);
          if (ns == "Lasermarks")
          {
            id += 1000000;
          }
          else if (ns == "Scenes")
          {
            id += 5000000;
          };

          std::string data = mark.second.get("transform", " ");
          float pose[7];
          sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3],
                 &pose[4], &pose[5], &pose[6]);

          Eigen::Vector3d temp_trans;
          temp_trans(0) = pose[0];
          temp_trans(1) = pose[1];
          temp_trans(2) = pose[2];
          Eigen::Quaterniond temp_quat;
          temp_quat.x() = pose[3];
          temp_quat.y() = pose[4];
          temp_quat.z() = pose[5];
          temp_quat.w() = pose[6];

          transform::Rigid3d temp_pose(temp_trans, temp_quat);
          new_landmarks_ori_[std::to_string(id)] = temp_pose;
        }
      }
    }
  }
  //   for (const auto& landmark : pose_graph_proto.landmark_poses()) {
  //     new_landmarks_ori_[landmark.landmark_id()] =
  //     transform::ToRigid3(landmark.global_pose());
  // //     cout<<transform::ToRigid3(landmark.global_pose())<<endl;
  //   }
  cout << "size of ld 0: " << new_landmarks_ori_.size() << endl;
}

void MergeTest::loadPbstreamTest(const string &path)
{
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);

  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  //   CHECK(pose_graph_proto.trajectory().size() == 1) <<
  //   pose_graph_proto.trajectory().size();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();
  for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
  {
    auto &trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
    const auto &options_with_sensor_ids_proto =
        all_builder_options_proto.options_with_sensor_ids(i);
    trajectory_builders_ori_.emplace_back();
    new_trajectory_id_ori_ = trajectory_builders_ori_.size();
  }
  LOG(WARNING) << "trajectory_proto.trajectory_id: " << new_trajectory_id_ori_;

  mapping::proto::SerializedData proto_tmp;
  LOG(WARNING) << "my flag 0";
  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case mapping::proto::SerializedData::kNode:
    {
      // 	LOG(WARNING) << "my flag 1";
      mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      //         proto_node->mutable_node_id()->set_trajectory_id(new_trajectory_id_ori_);
      const mapping::proto::TrajectoryNodeData proto_node_data = *proto_node->mutable_node_data();
      const mapping::TrajectoryNode::Data node_data = mapping::FromProto(proto_node_data);
      mapping::NodeId id(proto_node->node_id().trajectory_id(), proto_node->node_id().node_index());

      node_constant_datas_ori_.Insert(id, node_data);
      // 	LOG(WARNING) << "my flag 2";
      break;
    }

    case mapping::proto::SerializedData::kSubmap:
    {
      break;
    }
    case mapping::proto::SerializedData::kImuData:
    {
      break;
    }
    case mapping::proto::SerializedData::kOdometryData:
    {
      break;
    }
    case mapping::proto::SerializedData::kFixedFramePoseData:
    {
      break;
    }
    case mapping::proto::SerializedData::kLandmarkData:
    {
      landmark_datas_ori.push_back(sensor::FromProto(proto_tmp.landmark_data().landmark_data()));
      break;
    }
    }
  }
  LOG(WARNING) << "add sensor data done...";

  for (sensor::LandmarkData landmark_data : landmark_datas_ori)
  {
    for (const auto &observation : landmark_data.landmark_observations)
    {
      // TODO observation.pole_radius
      landmark_nodes_ori_[observation.id].landmark_observations.emplace_back(
          PoseGraphInterface::LandmarkNode::LandmarkObservation{
              0, landmark_data.time, observation.landmark_to_tracking_transform,
              observation.tranking_to_local_transform, 1000 * observation.translation_weight,
              1000 * observation.rotation_weight, observation.observation_points,
              observation.pole_radius});
      //        cout<<"num of points:
      //        "<<observation.observation_points.size()<<endl;
    }
  }

  LOG(WARNING) << "change constraints id done...";
}

void MergeTest::loadPbstream(const string &path)
{
  cv::Mat img1 = cv::imread(path + "/map.png");
  cv::line(img1, Point(0, 0), Point(0, 300), cv::Scalar(0, 255, 0), 2);
  cv::line(img1, Point(0, 300), Point(300, 300), cv::Scalar(255, 0, 0), 2);
  cv::line(img1, Point(300, 300), Point(300, 0), cv::Scalar(0, 0, 255), 2);
  cv::line(img1, Point(300, 0), Point(0, 0), cv::Scalar(200, 200, 2), 2);
  imwrite(string(getenv("HOME")) + "/bbbbbbbbbbbbbbb.png", img1);
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);

  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  CHECK(pose_graph_proto.trajectory().size() == 1) << pose_graph_proto.trajectory().size();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();
  for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
  {
    auto &trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
    const auto &options_with_sensor_ids_proto =
        all_builder_options_proto.options_with_sensor_ids(i);
    trajectory_builders_.emplace_back();
    new_trajectory_id_ = trajectory_builders_.size();

    all_trajectory_builder_options_.push_back(options_with_sensor_ids_proto);
    trajectory_proto.set_trajectory_id(new_trajectory_id_);
  }
  LOG(WARNING) << "trajectory_proto.trajectory_id: " << new_trajectory_id_;
  map<int, int> ignore_trajectory;
  kd_tree::poseVec poses;
  for (const mapping::proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses_.Insert(mapping::NodeId{new_trajectory_id_, node_proto.node_index()}, global_pose);
      new_origin_node_poses_.Insert(mapping::NodeId{new_trajectory_id_, node_proto.node_index()},
                                    global_pose);
      poses.push_back({global_pose.translation(), global_pose.rotation(),
                       Eigen::Vector2i(new_trajectory_id_, node_proto.node_index())});
    }
  }
  new_nodes_kd_tree_.reset(new kd_tree::KDTree(poses));

  //----------------------------------------------------------------------------------------
  {
    boost::property_tree::ptree mark_pt;
    boost::property_tree::read_xml(path + "/landmark.xml", mark_pt);
    for (auto &mark : mark_pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (visible)
        {
          std::string ns = mark.second.get("ns", "");
          std::string xml_id = mark.second.get("id", " ");
          int id = std::stoi(xml_id);
          if (ns == "Lasermarks")
          {
            id += 1000000;
          }
          else if (ns == "Scenes")
          {
            id += 5000000;
          };

          std::string data = mark.second.get("transform", " ");
          float pose[7];
          sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3],
                 &pose[4], &pose[5], &pose[6]);

          Eigen::Vector3d temp_trans;
          temp_trans(0) = pose[0];
          temp_trans(1) = pose[1];
          temp_trans(2) = pose[2];
          Eigen::Quaterniond temp_quat;
          temp_quat.x() = pose[3];
          temp_quat.y() = pose[4];
          temp_quat.z() = pose[5];
          temp_quat.w() = pose[6];

          transform::Rigid3d temp_pose(temp_trans, temp_quat);
          new_landmarks_[std::to_string(id)] = temp_pose;
        }
      }
    }
  }
  //   for (const auto& landmark : pose_graph_proto.landmark_poses()) {
  //
  //       new_landmarks_[landmark.landmark_id()] =
  //       transform::ToRigid3(landmark.global_pose());
  // //     cout<<transform::ToRigid3(landmark.global_pose())<<endl;
  //
  //   }
  cout << "size of ld 1: " << new_landmarks_.size() << endl;

  //----------------------------------------------------------------------------------------
  for (const mapping::proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    LOG(WARNING) << "trajectory_proto.trajectory_id: " << trajectory_proto.trajectory_id() << ", "
                 << trajectory_proto.submap().size();
    for (const mapping::proto::Trajectory::Submap &submap_proto : trajectory_proto.submap())
    {
      submap_poses_.Insert(mapping::SubmapId{new_trajectory_id_, submap_proto.submap_index()},
                           transform::ToRigid3(submap_proto.pose()));
    }
  }
  LOG(WARNING) << "add submap_poses done...";
  mapping::proto::SerializedData proto_tmp;

  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case mapping::proto::SerializedData::kNode:
    {
      mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      proto_node->mutable_node_id()->set_trajectory_id(new_trajectory_id_);
      const mapping::proto::TrajectoryNodeData proto_node_data = *proto_node->mutable_node_data();
      const mapping::TrajectoryNode::Data node_data = mapping::FromProto(proto_node_data);
      mapping::NodeId id(new_trajectory_id_, proto_node->node_id().node_index());

      node_constant_datas_.Insert(id, node_data);
      new_node_id_to_node_.Insert(id, proto_tmp.node());

      break;
    }

    case mapping::proto::SerializedData::kSubmap:
    {
      proto_tmp.mutable_submap()->mutable_submap_id()->set_trajectory_id(new_trajectory_id_);
      submap_id_to_submap_.Insert(
          mapping::SubmapId{new_trajectory_id_, proto_tmp.submap().submap_id().submap_index()},
          proto_tmp.submap());
      break;
    }
    case mapping::proto::SerializedData::kImuData:
    {
      //         if (load_frozen_state) break;
      imu_datas_.push_back(sensor::FromProto(proto_tmp.imu_data().imu_data()));

      break;
    }
    case mapping::proto::SerializedData::kOdometryData:
    {
      odom_datas_.push_back(sensor::FromProto(proto_tmp.odometry_data().odometry_data()));
      break;
    }
    case mapping::proto::SerializedData::kFixedFramePoseData:
    {
      //         if (load_frozen_state) break;
      fixed_frame_datas_.push_back(
          sensor::FromProto(proto_tmp.fixed_frame_pose_data().fixed_frame_pose_data()));

      break;
    }
    case mapping::proto::SerializedData::kLandmarkData:
    {
      landmark_datas_.push_back(sensor::FromProto(proto_tmp.landmark_data().landmark_data()));
      break;
    }
    }
  }
  LOG(WARNING) << "add sensor data done...";
  constraints_ = mapping::FromProto(pose_graph_proto.constraint());
  for (mapping::PoseGraphInterface::Constraint &constraint : constraints_)
  {
    constraint.node_id.trajectory_id = new_trajectory_id_;
    constraint.submap_id.trajectory_id = new_trajectory_id_;
  }
  //   for(sensor::LandmarkData landmark_data: landmark_datas_)
  //   {
  //     for (const auto& observation : landmark_data.landmark_observations)
  //     {
  //       // TODO observation.pole_radius
  //       landmark_nodes[observation.id]
  //                   .landmark_observations.emplace_back(
  //                       PoseGraphInterface::LandmarkNode::LandmarkObservation{
  //                           new_trajectory_id_, landmark_data.time,
  //                           observation.landmark_to_tracking_transform,
  //                           observation.tranking_to_local_transform,
  //                           1000 * observation.translation_weight,
  //                           1000 * observation.rotation_weight,
  //                           observation.observation_points,
  //                           observation.pole_radius});
  //
  //        cout<<"observation.id: "<<observation.id<<endl;
  //        cout<<"translation_weight: "<<observation.translation_weight<<endl;
  //        cout<<"rotation_weight: "<<observation.rotation_weight<<endl;
  // //        cout<<"num of points:
  // "<<observation.observation_points.size()<<endl;
  //     }
  //   }

  LOG(WARNING) << "change constraints id done...";
}

bool MergeTest::match(const sensor::PointCloud &node_point_cloud,
                      const transform::Rigid2d &initial_pose, transform::Rigid2d &pose_estimate)
{
  transform::Rigid2d pose_estimate_csm;
  float score;
  bool status = submap_scan_matcher_l_.fast_correlative_scan_matcher->Match(
      initial_pose, node_point_cloud, 0.45, &score, &pose_estimate_csm);
  if (!status) return false;
  ceres::Solver::Summary unused_summary;
  ceres_scan_matcher_->Match(pose_estimate_csm.translation(), pose_estimate_csm, node_point_cloud,
                             *submap_probability_grid_, &pose_estimate, &unused_summary);
  return true;
}

void MergeTest::poseGraphNew()
{
  mapping::MapById<mapping::SubmapId, std::array<double, 3>> C_submaps;
  mapping::MapById<mapping::NodeId, std::array<double, 3>> C_nodes;

  //------------------------------------------------------------------
  std::map<std::string, mapping::optimization::CeresPose> C_landmarks;
  //------------------------------------------------------------------

  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);
  for (const auto &submap_id_data : submap_poses_)
  {
    C_submaps.Insert(submap_id_data.id, FromPose(transform::Project2D(submap_id_data.data)));
    problem.AddParameterBlock(C_submaps.at(submap_id_data.id).data(), 3);
  }
  C_submaps.Insert({new_trajectory_id_ + 1, 0}, FromPose(transform::Rigid2d::Identity()));
  problem.AddParameterBlock(C_submaps.at(mapping::SubmapId{new_trajectory_id_ + 1, 0}).data(), 3);
  problem.SetParameterBlockConstant(
      C_submaps.at(mapping::SubmapId{new_trajectory_id_ + 1, 0}).data());

  for (const auto &node_id_data : node_poses_)
  {
    C_nodes.Insert(node_id_data.id, FromPose(transform::Project2D(node_id_data.data)));
    problem.AddParameterBlock(C_nodes.at(node_id_data.id).data(), 3);
  }
  LOG(WARNING) << 111;
  for (const mapping::PoseGraph::Constraint &constraint : constraints_)
  {
    problem.AddResidualBlock(mapping::optimization::CreateAutoDiffSpaCostFunction(constraint.pose),
                             // Loop closure constraints should have a loss function.
                             nullptr, C_submaps.at(constraint.submap_id).data(),
                             C_nodes.at(constraint.node_id).data());
  }

  //---------------------------------------------------
  AddLandmarkCostFunctions(landmark_nodes, node_data_, &C_nodes, &C_landmarks, &problem, 10);
  //----------------------------------------------------
  LOG(WARNING) << 111;
  ceres::Solver::Summary summary;
  ceres::Solver::Options options;
  options.max_num_iterations = 200;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.function_tolerance = 1e-6;
  ceres::Solve(options, &problem, &summary);

  for (const auto &C_submap_id_data : C_submaps)
  {
    if (submap_poses_.find(C_submap_id_data.id) == submap_poses_.end())
    {
      LOG(WARNING) << C_submap_id_data.id;
      continue;
    }
    submap_poses_.at(C_submap_id_data.id) = transform::Embed3D(ToPose(C_submap_id_data.data));
  }

  for (const auto &C_node_id_data : C_nodes)
  {
    node_poses_.at(C_node_id_data.id) = transform::Embed3D(ToPose(C_node_id_data.data));
    mapping::TrajectoryNode::Data node_data = node_constant_datas_.at(C_node_id_data.id);
    const transform::Rigid3d gravity_alignment =
        transform::Rigid3d::Rotation(node_data.gravity_alignment);
    transform::Rigid3f node_global_pose =
        node_poses_.at(C_node_id_data.id).cast<float>() * gravity_alignment.inverse().cast<float>();

    const sensor::PointCloud points = node_data.filtered_gravity_aligned_point_cloud;
    sensor::PointCloud points_in_global = sensor::TransformPointCloud(points, node_global_pose);
    transform::Rigid2f global_pose_2d = transform::Project2D(node_global_pose);
    Eigen::Vector2f origin;
    origin = global_pose_2d.translation();
    sensor::RangeData rangedata{{origin(0), origin(1), 0}, points_in_global, {}};
    trajectory_ranges_.push_back(rangedata);
  }

  //------------------------------------
  for (const auto &C_landmark : C_landmarks)
  {
    landmark_data_[C_landmark.first] = C_landmark.second.ToRigid();
  }
  //---------------------------------
}

void MergeTest::poseGraph()
{
  mapping::MapById<mapping::SubmapId, std::array<double, 3>> C_submaps;
  mapping::MapById<mapping::NodeId, std::array<double, 3>> C_nodes;
  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);
  for (const auto &submap_id_data : submap_poses_)
  {
    C_submaps.Insert(submap_id_data.id, FromPose(transform::Project2D(submap_id_data.data)));
    problem.AddParameterBlock(C_submaps.at(submap_id_data.id).data(), 3);
  }
  C_submaps.Insert({new_trajectory_id_ + 1, 0}, FromPose(transform::Rigid2d::Identity()));
  problem.AddParameterBlock(C_submaps.at(mapping::SubmapId{new_trajectory_id_ + 1, 0}).data(), 3);
  problem.SetParameterBlockConstant(
      C_submaps.at(mapping::SubmapId{new_trajectory_id_ + 1, 0}).data());

  for (const auto &node_id_data : node_poses_)
  {
    C_nodes.Insert(node_id_data.id, FromPose(transform::Project2D(node_id_data.data)));
    problem.AddParameterBlock(C_nodes.at(node_id_data.id).data(), 3);
  }
  LOG(WARNING) << 111;
  for (const mapping::PoseGraph::Constraint &constraint : constraints_)
  {
    //     if(C_nodes.find(constraint.node_id) == C_nodes.end())
    //     {
    //       LOG(WARNING) << constraint.node_id;
    //       continue;
    //     }
    //     if(C_submaps.find(constraint.submap_id) == C_submaps.end())
    //     {
    //       LOG(WARNING) << constraint.submap_id;
    //       continue;
    //     }
    problem.AddResidualBlock(mapping::optimization::CreateAutoDiffSpaCostFunction(constraint.pose),
                             // Loop closure constraints should have a loss function.
                             nullptr, C_submaps.at(constraint.submap_id).data(),
                             C_nodes.at(constraint.node_id).data());
  }
  LOG(WARNING) << 111;
  ceres::Solver::Summary summary;
  ceres::Solver::Options options;
  options.max_num_iterations = 200;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.function_tolerance = 1e-6;
  ceres::Solve(options, &problem, &summary);

  for (const auto &C_submap_id_data : C_submaps)
  {
    if (submap_poses_.find(C_submap_id_data.id) == submap_poses_.end())
    {
      LOG(WARNING) << C_submap_id_data.id;
      continue;
    }
    submap_poses_.at(C_submap_id_data.id) = transform::Embed3D(ToPose(C_submap_id_data.data));
  }

  for (const auto &C_node_id_data : C_nodes)
  {
    node_poses_.at(C_node_id_data.id) = transform::Embed3D(ToPose(C_node_id_data.data));
    mapping::TrajectoryNode::Data node_data = node_constant_datas_.at(C_node_id_data.id);
    const transform::Rigid3d gravity_alignment =
        transform::Rigid3d::Rotation(node_data.gravity_alignment);
    transform::Rigid3f node_global_pose =
        node_poses_.at(C_node_id_data.id).cast<float>() * gravity_alignment.inverse().cast<float>();

    const sensor::PointCloud points = node_data.filtered_gravity_aligned_point_cloud;
    sensor::PointCloud points_in_global = sensor::TransformPointCloud(points, node_global_pose);
    transform::Rigid2f global_pose_2d = transform::Project2D(node_global_pose);
    Eigen::Vector2f origin;
    origin = global_pose_2d.translation();
    sensor::RangeData rangedata{{origin(0), origin(1), 0}, points_in_global, {}};
    trajectory_ranges_.push_back(rangedata);
  }
}

MergeTest::SubmapInfo MergeTest::mergeCurTrajectory(cartographer::mapping::Grid2D *probability_grid)
{
  //   vector< MergeMap::AreaTransform > area_transfroms =
  //   getTrajectoryArea(rect_points);

  mapping::ValueConversionTables new_conversion_tables;
  mapping::proto::ProbabilityGridRangeDataInserterOptions2D options;
  options.set_hit_probability(0.55);
  options.set_miss_probability(0.49);
  options.set_insert_free_space(true);
  mapping::RangeDataInserterInterface *range_data_inserter =
      new mapping::ProbabilityGridRangeDataInserter2D(options);
  for (const auto &range : trajectory_ranges_)
  {
    range_data_inserter->Insert(range, probability_grid);
  }

  auto final_grid = probability_grid->ComputeCroppedGrid();
  //   const Grid2D* pg = origin_merge_grid->grid();
  //   double resolution = final_grid->limits().resolution();
  SubmapInfo submap_info;
  submap_info.max[0] = final_grid->limits().max()[0];
  submap_info.max[1] = final_grid->limits().max()[1];
  int num_x_cells = final_grid->limits().cell_limits().num_x_cells;
  int num_y_cells = final_grid->limits().cell_limits().num_y_cells;

  submap_info.known_cells_box = final_grid->known_cells_box();

  const std::vector<uint16_t> &cells = final_grid->cells();
  cout << "max: " << submap_info.max[0] << "," << submap_info.max[1] << endl;
  cout << "cells: " << num_x_cells << "," << num_y_cells << endl;
  cout << "known_cells_box: " << submap_info.known_cells_box.min().x() << ","
       << submap_info.known_cells_box.min().y() << "," << submap_info.known_cells_box.max().x()
       << "," << submap_info.known_cells_box.max().y() << endl;
  submap_info.cells_mat = cv::Mat::zeros(num_y_cells, num_x_cells, CV_16UC1);
  for (int i = 0; i < num_y_cells; i++)
    for (int j = 0; j < num_x_cells; j++)
    {
      uint16_t value = cells[i * num_x_cells + j];
      submap_info.cells_mat.at<uint16_t>(i, j) = value;
    }
  return submap_info;
}

vector<MergeTest::AreaTransform>
MergeTest::getTrajectoryArea(const vector<vector<Eigen::Vector2f>> &rect_points)
{
  vector<MergeTest::AreaTransform> area_transfroms;
  for (const auto &it : rect_points)
  {
    std::vector<Eigen::Vector2f> points = it;
    ;
    transform::Rigid3f map2area;
    Eigen::Vector2f new_x_towards = (points[3] - points[0]).normalized();
    Eigen::Vector2f new_y_towards = (points[1] - points[0]).normalized();
    // LOG(INFO) << "1:" << points[0] << "2:" << points[1];
    // LOG(INFO) << "3:" << points[2] << "4:" << points[3];
    LOG(INFO) << "X:" << new_x_towards << "Y:" << new_y_towards;
    float at1 = atan2(new_x_towards(1), new_x_towards(0));
    if (at1 > M_PI / 2) at1 = at1 - 2 * M_PI;
    float at2 = atan2(new_y_towards(1), new_y_towards(0));
    Eigen::Matrix2f map_R_area;
    LOG(WARNING) << at1 << ", " << at2 << ", " << fabs(at1 + M_PI / 2 - at2);
    if (fabs(at1 + M_PI / 2 - at2) > 0.5)
      map_R_area << new_y_towards[0], new_x_towards[0], new_y_towards[1], new_x_towards[1];
    else
      map_R_area << new_x_towards[0], new_y_towards[0], new_x_towards[1], new_y_towards[1];
    Eigen::Vector2f map_t_area = points[0];

    AreaTransform transform2;
    transform2.area_R_map = map_R_area.transpose();
    transform2.area_t_map = -(transform2.area_R_map * map_t_area);
    transform2.max_area = transform2.area_R_map * points[2] + transform2.area_t_map;
    LOG(INFO) << "max:" << transform2.max_area;
    area_transfroms.push_back(transform2);
    LOG(INFO) << "R:" << transform2.area_R_map << "t:" << transform2.area_t_map;
  }
  return area_transfroms;
}

void MergeTest::crop(const vector<vector<Eigen::Vector2f>> &all_rect_points)
{
  bool origin_trajectory_flag = true;
  float resolution = 0.05;
  vector<vector<Eigen::Vector2f>> new_rect_points;
  for (const vector<Eigen::Vector2f> &points : all_rect_points)
  {
    CHECK(points.size() % 4 == 0) << points.size();
    vector<Eigen::Vector2f> ps1;
    // Eigen::Vector2f p1(merge_map_info_.max[0] - points[2][1] * resolution,
    //                    merge_map_info_.max[1] - points[2][0] * resolution);
    // Eigen::Vector2f p2(merge_map_info_.max[0] - points[3][1] * resolution,
    //                    merge_map_info_.max[1] - points[3][0] * resolution);
    // Eigen::Vector2f p3(merge_map_info_.max[0] - points[0][1] * resolution,
    //                    merge_map_info_.max[1] - points[0][0] * resolution);
    // Eigen::Vector2f p4(merge_map_info_.max[0] - points[1][1] * resolution,
    //                    merge_map_info_.max[1] - points[1][0] * resolution);
    Eigen::Vector2f p1(merge_map_info_.max[0] - points[0][1] * resolution,
                       merge_map_info_.max[1] - points[0][0] * resolution);
    Eigen::Vector2f p2(merge_map_info_.max[0] - points[1][1] * resolution,
                       merge_map_info_.max[1] - points[1][0] * resolution);
    Eigen::Vector2f p3(merge_map_info_.max[0] - points[2][1] * resolution,
                       merge_map_info_.max[1] - points[2][0] * resolution);
    Eigen::Vector2f p4(merge_map_info_.max[0] - points[3][1] * resolution,
                       merge_map_info_.max[1] - points[3][0] * resolution);

    ps1.push_back(p1);
    ps1.push_back(p2);
    ps1.push_back(p3);
    ps1.push_back(p4);

    new_rect_points.push_back(ps1);
  }

  for (const vector<Eigen::Vector2f> &points : new_rect_points)
  {
    CHECK(points.size() % 4 == 0) << points.size();
    vector<vector<Eigen::Vector2f>> rect_points;

    for (int i = 0; i < points.size();)
    {
      vector<Eigen::Vector2f> ps;
      for (int j = 0; j < 4; j++, i++)
      {
        ps.push_back(points[i]);
        LOG(INFO) << "ccccc: " << points[i];
      }
      rect_points.push_back(ps);
    }
    vector<MergeTest::AreaTransform> crop_area = getTrajectoryArea(rect_points);
    // LOG(INFO) << "crop_area.size()" << crop_area.size();
    SubmapInfo tmp_info = origin_map_info_;                  //第一张图
    if (origin_trajectory_flag) tmp_info = new_submap_info_; //第二张图
    for (int k = 0; k < crop_area.size(); k++)
    {
      for (int i = 0; i < merge_map_.rows; i++)
        for (int j = 0; j < merge_map_.cols; j++)
        {
          Eigen::Vector2f origin_global_point(merge_map_info_.max[0] - i * resolution,
                                              merge_map_info_.max[1] - j * resolution); //真实坐标值
          Eigen::Vector2f new_point =
              crop_area[k].area_R_map * origin_global_point + crop_area[k].area_t_map;
          // LOG(INFO) << "new_point:" << new_point;
          if ((new_point.x() < crop_area[k].max_area.x() &&
               new_point.y() < crop_area[k].max_area.y() && new_point.x() > 0 && new_point.y() > 0))
          {
            int new_i = round((tmp_info.max[1] - origin_global_point(1)) / resolution); //像素坐标值
            int new_j = round((tmp_info.max[0] - origin_global_point(0)) / resolution);
            if (new_i < 0 || new_j < 0 || new_i > tmp_info.cells_mat.cols ||
                new_j > tmp_info.cells_mat.rows)
            {
              merge_map_.at<uint16_t>(i, j) = 0;
              // LOG(INFO) << "new_point - X:" <<i << "new_point - Y:" << j;
              // LOG(INFO) << "X:" <<new_i << " Y:" << new_j;
              // circle(merge_map_, Point(i,j), 1, Scalar(0, 0, 0));
            }
            else
            {
              merge_map_.at<uint16_t>(i, j) = tmp_info.cells_mat.at<uint16_t>(new_j, new_i);
            }
          }
        }
    }
    origin_trajectory_flag = false;
  }
}

void MergeTest::save(const string &merge_map_path)
{
  std::string system_command = "rm -rf " + merge_map_path;
  LOG(INFO) << system_command;
  system(system_command.c_str());

  system_command = "cp -r " + origin_path1_ + " " + merge_map_path;
  LOG(INFO) << system_command;
  system(system_command.c_str());

  {
    boost::property_tree::ptree p_map;
    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    std::string folderstring_submap = merge_map_path + "/frames/0/";
    system_command = "rm -rf " + folderstring_submap;
    LOG(INFO) << system_command;
    system(system_command.c_str());

    system_command = "mkdir -p " + folderstring_submap;
    LOG(INFO) << system_command;
    int status = system(system_command.c_str());
    checkSystemStatus(system_command, status);
    CHECK(status != -1);

    p_map.put("id", 0);

    string data = "0 0 0 0 0 0 1.0";
    p_map.put("pose", data);
    p_map.put("local_pose", data);
    data.clear();
    p_map.put("num_range_data", 2);
    p_map.put("finished", true);
    boost::property_tree::ptree p_probability_grid;
    uint num_x_cells, num_y_cells;

    p_probability_grid.put("resolution", 0.05);
    data = std::to_string(merge_map_info_.max[0]) + " " + std::to_string(merge_map_info_.max[1]);
    p_probability_grid.put("max", data);
    data.clear();
    num_x_cells = merge_map_info_.cells_mat.cols;
    num_y_cells = merge_map_info_.cells_mat.rows;
    data = std::to_string(num_x_cells) + " " + std::to_string(num_y_cells);
    p_probability_grid.put("num_cells", data);
    data.clear();
    data = std::to_string(0) + " " + std::to_string(0) + " " + std::to_string(num_x_cells - 1) +
           " " + std::to_string(num_y_cells - 1);
    p_probability_grid.put("known_cells_box", data);
    p_map.add_child("probability_grid", p_probability_grid);

    boost::property_tree::write_xml(folderstring_submap + "data.xml", p_map, std::locale(),
                                    setting);

    cv::imwrite(folderstring_submap + "probability_grid.png", merge_map_info_.cells_mat);
  }

  {
    system_command = "rm " + merge_map_path + "/map.png";
    LOG(INFO) << system_command;
    system(system_command.c_str());

    cv::Mat show_img = image16ToImage8(merge_map_info_.cells_mat);
    imwrite(merge_map_path + "/map.png", show_img);
    autoSetIgnoreArea(merge_map_path + "/");
  }

  {
    boost::property_tree::ptree p_map, p_top;
    std::string data;
    p_map.put("width", to_string(merge_map_info_.cells_mat.rows));
    p_map.put("height", to_string(merge_map_info_.cells_mat.cols));
    p_map.put("resolution", to_string(0.05));
    cartographer::transform::Rigid3d::Vector translation(merge_map_info_.max[1] / 0.05,
                                                         merge_map_info_.max[0] / 0.05, 0);
    cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2, sqrt(2) / 2, 0);
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(translation[i]) + " ";
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(quaternion.coeffs()[i]) + " ";
    data = data + std::to_string(quaternion.coeffs()[3]);
    p_map.put("pose", data);
    data.clear();
    p_top.add_child("mapPng", p_map);

    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    boost::property_tree::write_xml(merge_map_path + "/map_data.xml", p_top, std::locale(),
                                    setting);
  }

  // cp pbstream
  {
    system_command = "cp " + new_path_ + "/map.pbstream " + merge_map_path + "/" +
                     to_string(ros::Time::now().sec) + ".pbstream";
    LOG(INFO) << system_command;
    system(system_command.c_str());
  }

  // save landmarks
  if (0)
  {
    auto origin_landmarks = loadLandmarksFromXmlFile(origin_path1_ + "/landmark.xml");
    auto new_landmarks = loadLandmarksFromXmlFile(new_path_ + "/landmark.xml");
    updatePoints(new_landmarks_);
    std::vector<map_manager::MapInterface::LandmarkInfo> landmarks;
  }

  // update bool image
  {
    system_command = "rm " + merge_map_path + "/bool_image.png";
    LOG(INFO) << system_command;
    system(system_command.c_str());
    updateBoolImage(origin_path1_, merge_map_path + "/");
  }
}

void MergeTest::updatePoints(std::map<std::string, transform::Rigid3d> &nav_points)
{
  for (auto &it : nav_points)
  {
    transform::Rigid3d &point = it.second;
    kd_tree::pose pose{point.translation(), Eigen::Quaterniond::Identity(), Eigen::Vector2i(0, 0)};

    kd_tree::pose result = kd_tree_->nearest_point(pose);
    mapping::NodeId node_id(result.id(0), result.id(1));
    transform::Rigid3d origin_nearest_pose = new_origin_node_poses_.at(node_id);
    double nearest_dist = (point.translation() - origin_nearest_pose.translation()).norm();

    kd_tree::pointIndexArr near_nodes = kd_tree_->neighborhood(pose, nearest_dist + 2);

    if (near_nodes.size() < 2)
    {
      transform::Rigid3d new_pose = node_poses_.at(node_id);
      point = new_pose * origin_nearest_pose.inverse() * point;
      continue;
    }
    Eigen::Vector3d opt_p = point.translation();
    ceres::Problem problem;
    ceres::LossFunction *loss_function = NULL;
    double point_angle = transform::GetYaw(point.rotation());

    for (kd_tree::pointIndex node : near_nodes)
    {
      mapping::NodeId node_id(node.first.id(0), node.first.id(1));
      transform::Rigid3d origin_pose = new_origin_node_poses_.at(node_id);
      transform::Rigid3d new_pose = node_poses_.at(node_id);
      double dist = (point.translation() - origin_pose.translation()).norm();
      const transform::Rigid3d point_in_global = new_pose * origin_pose.inverse() * point;

      const double temp_yaw = transform::GetYaw(point_in_global.rotation());
      ceres::CostFunction *cost_function =
          pointConstraint2D::Create(point_in_global.translation().x(),
                                    point_in_global.translation().y(), temp_yaw, 1.0 / dist);
      problem.AddResidualBlock(cost_function, loss_function, &(opt_p(0)), &(opt_p(1)),
                               &point_angle);
    }
    ceres::Solver::Options options;
    options.max_num_iterations = 10;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    Eigen::Quaterniond opt_q(
        Eigen::AngleAxis<double>(point_angle, Eigen::Matrix<double, 3, 1>::UnitZ()));
    point = transform::Rigid3d(opt_p, opt_q);
  }
}

void MergeTest::MyTest(
    const std::map<std::string, mapping::PoseGraphInterface::LandmarkNode> &landmark_nodes,
    const MapById<NodeId, mapping::optimization::NodeSpec2D> &node_data)
{
  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;
      const auto &begin_of_trajectory = node_data.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.

      //       cout<<"observation time: "<<observation.time<<endl;
      //       cout<<"begin_of_trajectory time:
      //       "<<begin_of_trajectory->data.time<<endl;

      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation wasobservation.observati made, but the next
      // trajectory node has not been added yet.
      if (next == node_data.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);
      // Add parameter blocks for the landmark ID if they were not added before.
      //       std::array<double, 3>* next_node_pose = &C_nodes->at(next->id);
      transform::Rigid2d bpre_to_local_2d = node_data.at(prev->id).local_pose_2d;
      transform::Rigid3d bpre_to_local_3d =
          transform::Embed3D(bpre_to_local_2d) *
          transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
      transform::Rigid2d bpre_to_local = transform::Project2D(bpre_to_local_3d);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;
      transform::Rigid2d base_to_local_2d = transform::Project2D(base_to_local);
      transform::Rigid2d bpre_to_base = base_to_local_2d.inverse() * bpre_to_local;
      const std::array<double, 3> base2bpre_array = FromPose(bpre_to_base.inverse());

      std::cout << base2bpre_array.front() << " " << base2bpre_array.at(1) << " "
                << base2bpre_array[2] << std::endl;
    }
  }
}

void MergeTest::AddLandmarkCostFunctionsOld(
    const std::map<std::string, mapping::PoseGraphInterface::LandmarkNode> &landmark_nodes,
    const MapById<NodeId, mapping::optimization::NodeSpec2D> &node_data,
    MapById<NodeId, std::array<double, 3>> *C_nodes,
    std::map<std::string, mapping::optimization::CeresPose> *C_landmarks, ceres::Problem *problem,
    double huber_scale)
{
  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;
      const auto &begin_of_trajectory = node_data.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.
      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation wasobservation.observati made, but the next
      // trajectory node has not been added yet.
      if (next == node_data.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);
      // Add parameter blocks for the landmark ID if they were not added before.
      std::array<double, 3> *prev_node_pose = &C_nodes->at(prev->id);
      //       std::array<double, 3>* next_node_pose = &C_nodes->at(next->id);
      transform::Rigid2d bpre_to_local_2d = node_data.at(prev->id).local_pose_2d;
      transform::Rigid3d bpre_to_local_3d =
          transform::Embed3D(bpre_to_local_2d) *
          transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
      transform::Rigid2d bpre_to_local = transform::Project2D(bpre_to_local_3d);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;
      transform::Rigid2d base_to_local_2d = transform::Project2D(base_to_local);
      transform::Rigid2d bpre_to_base = base_to_local_2d.inverse() * bpre_to_local;
      const std::array<double, 3> base2bpre_array = FromPose(bpre_to_base.inverse());

      if (!C_landmarks->count(landmark_id))
      {
        transform::Rigid3d starting_point_tmp;

        if (landmark_node.second.global_landmark_pose.has_value())
        {
          starting_point_tmp = landmark_node.second.global_landmark_pose.value();
        }
        else
        {
          transform::Rigid3d bpre_to_local_3d =
              transform::Embed3D(node_data.at(prev->id).local_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d bpre_to_global =
              transform::Embed3D(node_data.at(prev->id).global_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d mark_to_base = observation.landmark_to_tracking_transform;
          starting_point_tmp =
              bpre_to_global * bpre_to_local_3d.inverse() * base_to_local * mark_to_base;
          // 					LOG(WARNING) << "lanmark id: "
          // <<landmark_node.first
          // << std::endl
          // 					<<
          // starting_point_tmp<<std::endl;
          // 					LOG(WARNING) << "bpre_to_local:
          // "
          // <<bpre_to_local
          // <<std::endl
          // 					<< "bpre_to_local_3d: "
          // <<bpre_to_local_3d
          // <<std::endl
          // 					<< "base_to_local_2d: "
          // <<base_to_local_2d
          // <<std::endl
          // 					<< "base_to_local: "
          // <<base_to_local
          // <<std::endl
          // 					<< "bpre_to_global: "
          // <<bpre_to_global
          // <<std::endl;
        }
        const transform::Rigid3d starting_point = starting_point_tmp;

        C_landmarks->emplace(landmark_id,
                             mapping::optimization::CeresPose(
                                 starting_point, nullptr /* translation_parametrization */,
                                 absl::make_unique<ceres::QuaternionParameterization>(), problem));
        // Set landmark constant if it is frozen.
        if (landmark_node.second.frozen)
        {
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).translation());
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).rotation());

          cout << "frozen!!" << endl;
        }
      }
      problem->AddResidualBlock(
          mapping::optimization::LandmarkJzCostFunction2D::CreateAutoDiffCostFunction(
              observation, prev->data, base2bpre_array),
          new ceres::HuberLoss(huber_scale), prev_node_pose->data(),
          C_landmarks->at(landmark_id).rotation(), C_landmarks->at(landmark_id).translation());
    }
  }
}

void MergeTest::AddLandmarkCostFunctions(
    const std::map<std::string, mapping::PoseGraphInterface::LandmarkNode> &landmark_nodes,
    const MapById<NodeId, mapping::optimization::NodeSpec2D> &node_data,
    MapById<NodeId, std::array<double, 3>> *C_nodes,
    std::map<std::string, mapping::optimization::CeresPose> *C_landmarks, ceres::Problem *problem,
    double huber_scale)
{
  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;
      const auto &begin_of_trajectory = node_data.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.
      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation wasobservation.observati made, but the next
      // trajectory node has not been added yet.
      if (next == node_data.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);
      // Add parameter blocks for the landmark ID if they were not added before.
      std::array<double, 3> *prev_node_pose = &C_nodes->at(prev->id);
      //       std::array<double, 3>* next_node_pose = &C_nodes->at(next->id);
      transform::Rigid2d bpre_to_local_2d = node_data.at(prev->id).local_pose_2d;
      transform::Rigid3d bpre_to_local_3d =
          transform::Embed3D(bpre_to_local_2d) *
          transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
      transform::Rigid2d bpre_to_local = transform::Project2D(bpre_to_local_3d);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;
      transform::Rigid2d base_to_local_2d = transform::Project2D(base_to_local);
      transform::Rigid2d bpre_to_base = base_to_local_2d.inverse() * bpre_to_local;
      const std::array<double, 3> base2bpre_array = FromPose(bpre_to_base.inverse());

      if (!C_landmarks->count(landmark_id))
      {
        transform::Rigid3d starting_point_tmp;
        if (landmark_node.second.global_landmark_pose.has_value())
        {
          starting_point_tmp = landmark_node.second.global_landmark_pose.value();
          cout << "landmark global pose:" << starting_point_tmp << endl;
        }
        else
        {
          transform::Rigid3d bpre_to_local_3d =
              transform::Embed3D(node_data.at(prev->id).local_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d bpre_to_global =
              transform::Embed3D(node_data.at(prev->id).global_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d mark_to_base = observation.landmark_to_tracking_transform;

          //                 LandmarkNode::LandmarkObservation observation;
          if (!observation.observation_points.empty())
          {
            ceres::Problem problem;
            double center_x = observation.landmark_to_tracking_transform.translation().x();
            double center_y = observation.landmark_to_tracking_transform.translation().y();
            for (int i = 0; i < observation.observation_points.size(); i++)
            {
              double xx = observation.observation_points[i].x();
              double yy = observation.observation_points[i].y();
              ceres::CostFunction *cost =
                  new ceres::AutoDiffCostFunction<mapping::optimization::CircleCost2D, 1, 1, 1>(
                      new mapping::optimization::CircleCost2D(xx, yy, observation.pole_radius));
              problem.AddResidualBlock(cost, nullptr, &center_x, &center_y);
            }
            // Build and solve the problem.
            ceres::Solver::Options options;
            options.max_num_iterations = 500;
            options.linear_solver_type = ceres::DENSE_QR;
            ceres::Solver::Summary summary;
            ceres::Solve(options, &problem, &summary);
            mark_to_base = transform::Rigid3d(Eigen::Vector3d(center_x, center_y, 0.),
                                              Eigen::Quaterniond::Identity());
          }

          starting_point_tmp =
              bpre_to_global * bpre_to_local_3d.inverse() * base_to_local * mark_to_base;
        }
        const transform::Rigid3d starting_point = starting_point_tmp;

        C_landmarks->emplace(landmark_id,
                             mapping::optimization::CeresPose(
                                 starting_point, nullptr /* translation_parametrization */,
                                 absl::make_unique<ceres::QuaternionParameterization>(), problem));
        // Set landmark constant if it is frozen.
        if (landmark_node.second.frozen)
        {
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).translation());
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).rotation());
        }
      }

      auto test_empty = observation.observation_points.empty();
      if (observation.observation_points.empty())
        problem->AddResidualBlock(
            mapping::optimization::LandmarkJzCostFunction2D::CreateAutoDiffCostFunction(
                observation, prev->data, base2bpre_array),
            new ceres::HuberLoss(huber_scale), prev_node_pose->data(),
            C_landmarks->at(landmark_id).rotation(), C_landmarks->at(landmark_id).translation());
      else
      {
        for (Eigen::Vector3f point : observation.observation_points)
        {
          problem->AddResidualBlock(
              mapping::optimization::LandmarkPoleLikeFunction2D::CreateAutoDiffCostFunction(
                  point, observation.pole_radius, prev->data, base2bpre_array, 1.0),
              new ceres::HuberLoss(huber_scale), prev_node_pose->data(),
              C_landmarks->at(landmark_id).translation());
        }
      }
    }
  }
}

std::unique_ptr<transform::Rigid3d>
MergeTest::InterpolateOdometry(const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
                               const int trajectory_id, const common::Time time) const
{
  const auto it = odometry_data_.lower_bound(trajectory_id, time);
  if (it == odometry_data_.EndOfTrajectory(trajectory_id))
  {
    return nullptr;
  }
  if (it == odometry_data_.BeginOfTrajectory(trajectory_id))
  {
    if (it->time == time)
    {
      return absl::make_unique<transform::Rigid3d>(it->pose);
    }
    return nullptr;
  }
  const auto prev_it = std::prev(it);
  return absl::make_unique<transform::Rigid3d>(
      transform::Interpolate(transform::TimestampedTransform{prev_it->time, prev_it->pose},
                             transform::TimestampedTransform{it->time, it->pose}, time)
          .transform);
}

std::unique_ptr<transform::Rigid3d> MergeTest::CalculateOdometryBetweenNodes(
    const sensor::MapByTime<sensor::OdometryData> &odometry_data_, const int trajectory_id,
    const optimization::NodeSpec2D &first_node_data,
    const optimization::NodeSpec2D &second_node_data) const
{
  if (odometry_data_.HasTrajectory(trajectory_id))
  {
    const std::unique_ptr<transform::Rigid3d> first_node_odometry =
        InterpolateOdometry(odometry_data_, trajectory_id, first_node_data.time);
    const std::unique_ptr<transform::Rigid3d> second_node_odometry =
        InterpolateOdometry(odometry_data_, trajectory_id, second_node_data.time);
    if (first_node_odometry != nullptr && second_node_odometry != nullptr)
    {
      transform::Rigid3d relative_odometry =
          transform::Rigid3d::Rotation(first_node_data.gravity_alignment) *
          first_node_odometry->inverse() * (*second_node_odometry) *
          transform::Rigid3d::Rotation(second_node_data.gravity_alignment.inverse());
      return absl::make_unique<transform::Rigid3d>(relative_odometry);
    }
  }
  return nullptr;
}

void MergeTest::SolveOld(const std::vector<PoseGraphInterface::Constraint> &constraints,
                         const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
                         MapById<NodeId, optimization::NodeSpec2D> &node_data_,
                         MapById<SubmapId, optimization::SubmapSpec2D> &submap_data)
{
  std::set<int> frozen_trajectories;
  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);

  // Set the starting point.
  // TODO(hrapp): Move ceres data into SubmapSpec.
  MapById<SubmapId, std::array<double, 3>> C_submaps;
  MapById<NodeId, std::array<double, 3>> C_nodes;
  std::map<std::string, optimization::CeresPose> C_landmarks;
  {
    bool first_submap = true;
    for (const auto &submap_id_data : submap_data)
    {
      //       const bool frozen =
      //           frozen_trajectories.count(submap_id_data.id.trajectory_id) !=
      //           0;
      C_submaps.Insert(submap_id_data.id, FromPose(submap_id_data.data.global_pose));
      problem.AddParameterBlock(C_submaps.at(submap_id_data.id).data(), 3);
      if ((first_submap && submap_id_data.id.trajectory_id == 0))
      {
        first_submap = false;
        // Fix the pose of the first submap or all submaps of a frozen
        // trajectory.
        problem.SetParameterBlockConstant(C_submaps.at(submap_id_data.id).data());
      }
    }
    //     first_submap = true;
    for (const auto &node_id_data : node_data_)
    {
      C_nodes.Insert(node_id_data.id, FromPose(node_id_data.data.global_pose_2d));
      problem.AddParameterBlock(C_nodes.at(node_id_data.id).data(), 3);
      if (node_id_data.id.trajectory_id == 0)
      {
        problem.SetParameterBlockConstant(C_nodes.at(node_id_data.id).data());
      }
    }
  }

  // Add cost functions for intra- and inter-submap constraints.
  for (const PoseGraphInterface::Constraint &constraint : constraints)
  {
    problem.AddResidualBlock(optimization::CreateAutoDiffSpaCostFunction(constraint.pose),
                             // Loop closure constraints should have a loss function.
                             constraint.tag == PoseGraphInterface::Constraint::INTER_SUBMAP
                                 ? new ceres::HuberLoss(options_.huber_scale())
                                 : nullptr,
                             C_submaps.at(constraint.submap_id).data(),
                             C_nodes.at(constraint.node_id).data());
  }

  // Add cost functions for landmarks.
  AddLandmarkCostFunctionsOld(landmark_nodes, node_data_, &C_nodes, &C_landmarks, &problem,
                              options_.huber_scale());

  //   landmarkTest(landmark_nodes, node_data_, &C_nodes, &C_landmarks,
  //                            &problem, options_.huber_scale());

  // Add penalties for violating odometry or changes between consecutive nodes
  // if odometry is not available.

  for (auto node_it = node_data_.begin(); node_it != node_data_.end();)
  {
    const int trajectory_id = node_it->id.trajectory_id;
    const auto trajectory_end = node_data_.EndOfTrajectory(trajectory_id);
    if (frozen_trajectories.count(trajectory_id) != 0)
    {
      node_it = trajectory_end;
      continue;
    }

    auto prev_node_it = node_it;
    for (++node_it; node_it != trajectory_end; ++node_it)
    {
      const NodeId first_node_id = prev_node_it->id;
      const optimization::NodeSpec2D &first_node_data = prev_node_it->data;
      prev_node_it = node_it;
      const NodeId second_node_id = node_it->id;
      const optimization::NodeSpec2D &second_node_data = node_it->data;

      if (second_node_id.node_index != first_node_id.node_index + 1)
      {
        continue;
      }

      // Add a relative pose constraint based on the odometry (if available).
      std::unique_ptr<transform::Rigid3d> relative_odometry = CalculateOdometryBetweenNodes(
          odometry_data_, trajectory_id, first_node_data, second_node_data);
      if (relative_odometry != nullptr)
      {
        problem.AddResidualBlock(
            optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
                *relative_odometry, options_.odometry_translation_weight(),
                options_.odometry_rotation_weight()}),
            nullptr /* loss function */, C_nodes.at(first_node_id).data(),
            C_nodes.at(second_node_id).data());
      }

      // Add a relative pose constraint based on consecutive local SLAM poses.
      const transform::Rigid3d relative_local_slam_pose = transform::Embed3D(
          first_node_data.local_pose_2d.inverse() * second_node_data.local_pose_2d);
      problem.AddResidualBlock(
          optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
              relative_local_slam_pose, options_.local_slam_pose_translation_weight(),
              options_.local_slam_pose_rotation_weight()}),
          nullptr /* loss function */, C_nodes.at(first_node_id).data(),
          C_nodes.at(second_node_id).data());
    }
  }

  // Solve.
  ceres::Solver::Summary summary;
  ceres::Solve(common::CreateCeresSolverOptions(options_.ceres_solver_options()), &problem,
               &summary);
  if (options_.log_solver_summary())
  {
    LOG(INFO) << summary.FullReport();
  }

  // Store the result.
  for (const auto &C_submap_id_data : C_submaps)
  {
    submap_data.at(C_submap_id_data.id).global_pose = ToPose(C_submap_id_data.data);
  }
  for (const auto &C_node_id_data : C_nodes)
  {
    node_data_.at(C_node_id_data.id).global_pose_2d = ToPose(C_node_id_data.data);
  }

  LOG(WARNING) << "End of Solve.";
  //------------------------------------
  for (const auto &C_landmark : C_landmarks)
  {
    save_landmarks_ori_[C_landmark.first] = C_landmark.second.ToRigid();
  }
  //---------------------------------
}

void MergeTest::Solve(const std::vector<PoseGraphInterface::Constraint> &constraints,
                      const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
                      MapById<NodeId, optimization::NodeSpec2D> &node_data_,
                      MapById<SubmapId, optimization::SubmapSpec2D> &submap_data)
{
  std::set<int> frozen_trajectories;
  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);

  // Set the starting point.
  // TODO(hrapp): Move ceres data into SubmapSpec.
  MapById<SubmapId, std::array<double, 3>> C_submaps;
  MapById<NodeId, std::array<double, 3>> C_nodes;
  std::map<std::string, optimization::CeresPose> C_landmarks;
  {
    bool first_submap = true;
    for (const auto &submap_id_data : submap_data)
    {
      //       const bool frozen =
      //           frozen_trajectories.count(submap_id_data.id.trajectory_id) !=
      //           0;
      C_submaps.Insert(submap_id_data.id, FromPose(submap_id_data.data.global_pose));
      problem.AddParameterBlock(C_submaps.at(submap_id_data.id).data(), 3);
      if ((first_submap && submap_id_data.id.trajectory_id == 0))
      {
        first_submap = false;
        // Fix the pose of the first submap or all submaps of a frozen
        // trajectory.
        problem.SetParameterBlockConstant(C_submaps.at(submap_id_data.id).data());
      }
    }
    //     first_submap = true;
    for (const auto &node_id_data : node_data_)
    {
      C_nodes.Insert(node_id_data.id, FromPose(node_id_data.data.global_pose_2d));
      problem.AddParameterBlock(C_nodes.at(node_id_data.id).data(), 3);
      if (node_id_data.id.trajectory_id == 0)
      {
        problem.SetParameterBlockConstant(C_nodes.at(node_id_data.id).data());
      }
    }
  }

  // Add cost functions for intra- and inter-submap constraints.
  for (const PoseGraphInterface::Constraint &constraint : constraints)
  {
    problem.AddResidualBlock(optimization::CreateAutoDiffSpaCostFunction(constraint.pose),
                             // Loop closure constraints should have a loss function.
                             constraint.tag == PoseGraphInterface::Constraint::INTER_SUBMAP
                                 ? new ceres::HuberLoss(options_.huber_scale())
                                 : nullptr,
                             C_submaps.at(constraint.submap_id).data(),
                             C_nodes.at(constraint.node_id).data());
  }

  // Add cost functions for landmarks.
  //   AddLandmarkCostFunctions(landmark_nodes, node_data_, &C_nodes,
  //   &C_landmarks,
  //                            &problem, options_.huber_scale());

  // Add penalties for violating odometry or changes between consecutive nodes
  // if odometry is not available.
  for (auto node_it = node_data_.begin(); node_it != node_data_.end();)
  {
    const int trajectory_id = node_it->id.trajectory_id;
    const auto trajectory_end = node_data_.EndOfTrajectory(trajectory_id);
    if (frozen_trajectories.count(trajectory_id) != 0)
    {
      node_it = trajectory_end;
      continue;
    }

    auto prev_node_it = node_it;
    for (++node_it; node_it != trajectory_end; ++node_it)
    {
      const NodeId first_node_id = prev_node_it->id;
      const optimization::NodeSpec2D &first_node_data = prev_node_it->data;
      prev_node_it = node_it;
      const NodeId second_node_id = node_it->id;
      const optimization::NodeSpec2D &second_node_data = node_it->data;

      if (second_node_id.node_index != first_node_id.node_index + 1)
      {
        continue;
      }

      // Add a relative pose constraint based on the odometry (if available).
      std::unique_ptr<transform::Rigid3d> relative_odometry = CalculateOdometryBetweenNodes(
          odometry_data_, trajectory_id, first_node_data, second_node_data);
      if (relative_odometry != nullptr)
      {
        problem.AddResidualBlock(
            optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
                *relative_odometry, options_.odometry_translation_weight(),
                options_.odometry_rotation_weight()}),
            nullptr /* loss function */, C_nodes.at(first_node_id).data(),
            C_nodes.at(second_node_id).data());
      }

      // Add a relative pose constraint based on consecutive local SLAM poses.
      const transform::Rigid3d relative_local_slam_pose = transform::Embed3D(
          first_node_data.local_pose_2d.inverse() * second_node_data.local_pose_2d);
      problem.AddResidualBlock(
          optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
              relative_local_slam_pose, options_.local_slam_pose_translation_weight(),
              options_.local_slam_pose_rotation_weight()}),
          nullptr /* loss function */, C_nodes.at(first_node_id).data(),
          C_nodes.at(second_node_id).data());
    }
  }

  // Solve.
  ceres::Solver::Summary summary;
  ceres::Solve(common::CreateCeresSolverOptions(options_.ceres_solver_options()), &problem,
               &summary);
  if (options_.log_solver_summary())
  {
    LOG(INFO) << summary.FullReport();
  }

  // Store the result.
  for (const auto &C_submap_id_data : C_submaps)
  {
    submap_data.at(C_submap_id_data.id).global_pose = ToPose(C_submap_id_data.data);
  }
  for (const auto &C_node_id_data : C_nodes)
  {
    node_data_.at(C_node_id_data.id).global_pose_2d = ToPose(C_node_id_data.data);
  }

  LOG(WARNING) << "End of Solve.";
}
} // namespace map_manager

double toAngle(const double w, const double x, const double y, const double z)
{
  double siny_cosp = 2 * (w * z + x * y);
  double cosy_cosp = 1 - 2 * (y * y + z * z);
  double yaw = std::atan2(siny_cosp, cosy_cosp);

  return -yaw;
}

transform::Rigid2d getTransformFromImagePoints(const Eigen::Vector2d &max1,
                                               const Eigen::Vector2d &max2,
                                               const Eigen::Vector2d &img_point,
                                               const double &angle, const double &resolution)
{
  Eigen::Vector2d t1;
  t1[0] = max1[0] - img_point[1] * resolution;
  t1[1] = max1[1] - img_point[0] * resolution;
  transform::Rigid2d img2map1(t1, angle);
  LOG(INFO) << img2map1;
  transform::Rigid2d img2map2({max2[0], max2[1]}, 0);
  LOG(INFO) << img2map2;
  return img2map1 * img2map2.inverse();
}

int main(int argc, char **argv)
{
  const std::string path0 = "/home/shao/map/myRef/2fss";

  const std::string path1 = "/home/shao/map/myRef/fsb_res";

  Eigen::Vector3d ini_trans(4142, 44, 0);
  Eigen::Quaterniond ini_quat(0.9238795, 0, 0, 0.382683);

  transform::Rigid3d ini_pose(ini_trans, ini_quat);

  //   cout<<"test"<<endl;

  std::unique_ptr<map_manager::MergeTest> merge_test_;

  merge_test_.reset(new map_manager::MergeTest);

  double px = 4142;
  double py = 44;
  double pz = 0;

  double ow = 0.9238795;
  double ox = 0;
  double oy = 0;
  double oz = 0.382683;

  double angle = toAngle(ow, ox, oy, oz);

  boost::property_tree::ptree pt1, pt2;
  boost::property_tree::read_xml(path0 + "/frames/0/data.xml", pt1);
  boost::property_tree::read_xml(path1 + "/frames/0/data.xml", pt2);
  std::string data1, data2;
  Eigen::Vector2d max1, max2;
  double resolution;
  Eigen::Vector2d image_point;
  image_point[0] = px;
  image_point[1] = py;
  data1 = pt1.get<std::string>("probability_grid.max");
  sscanf(data1.c_str(), "%lf %lf", &max1[0], &max1[1]);
  data2 = pt2.get<std::string>("probability_grid.max");
  sscanf(data2.c_str(), "%lf %lf", &max2[0], &max2[1]);
  LOG(INFO) << "max1: " << max1;
  LOG(INFO) << "max2: " << max2;
  LOG(INFO) << "image_point: " << image_point;

  resolution = pt1.get<double>("probability_grid.resolution");
  LOG(INFO) << "resolution: " << resolution;
  transform::Rigid2d relative_pose =
      getTransformFromImagePoints(max1, max2, image_point, angle, resolution);
  LOG(INFO) << "input_pose: " << relative_pose;

  merge_test_->start(path0, path1, transform::Embed3D(relative_pose));

  return 0;
}