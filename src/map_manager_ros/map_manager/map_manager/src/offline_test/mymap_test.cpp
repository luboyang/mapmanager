/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "mymap_test.h"

#include <cartographer/io/image.h>
#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer/sensor/point_cloud.h>
#include <cartographer/transform/rigid_transform.h>
#include <cartographer/transform/transform.h>
#include <csm_eigen/csm_all.h>
#include <csm_eigen/laser_data.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/console/time.h>
#include <pcl/filters/convolution_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/ndt.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_line.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>

#include "map_manager_ulitity.h"
#include "utility/kd_tree.h"
// #include <pcl/visualization/pcl_visualizer.h>

// #include <glog/logging.h>
// #include <ros/ros.h>
// #include <boost/property_tree/ptree.hpp>
// #include <boost/property_tree/xml_parser.hpp>
// #include <pcl/search/kdtree.h>
// #include "cartographer/io/proto_stream.h"
// #include "cartographer/io/proto_stream_deserializer.h"
// #include "cartographer/io/internal/mapping_state_serialization.h"

using namespace std;
using namespace cartographer;
using namespace cartographer::mapping;
using namespace map_manager;
using namespace cv;

namespace map_manager
{

void mymap_test::readPointsFromPbstream(const std::string &path, PbstreamInfos &infos,
                                        pcl::PointCloud<pcl::PointXYZ> &cloud_all,
                                        pcl::PointCloud<pcl::PointXYZ> &cloud_frame,
                                        Eigen::Matrix4d &prior_T, cv::Mat &cells_mat)
{
  cout << "my_test 0" << endl;
  readPosesFromPbstream(path, infos);
  cout << "my_test 1" << endl;
  sensor::PointCloud all_points;

  int count_0 = 0;
  for (const auto &it : infos.node_poses)
  {
    cout << count_0 << endl;
    const transform::Rigid3d gravity_alignment =
        transform::Rigid3d::Rotation(infos.node_datas.at(it.id).gravity_alignment);
    transform::Rigid3d node_global_pose = it.data * gravity_alignment.inverse();
    const sensor::PointCloud points =
        infos.node_datas.at(it.id).filtered_gravity_aligned_point_cloud;
    sensor::PointCloud points_in_global =
        sensor::TransformPointCloud(points, node_global_pose.cast<float>());
    all_points.insert(all_points.end(), points_in_global.begin(), points_in_global.end());

    if (count_0 == 0)
    {
      Eigen::Vector3d trans = node_global_pose.translation();
      Eigen::Matrix3d rotat = node_global_pose.rotation().toRotationMatrix();
      prior_T.block(0, 0, 3, 3) = rotat;
      prior_T.block(0, 3, 3, 1) = trans;

      for (const auto &iter : points)
      {
        pcl::PointXYZ temp_point;
        temp_point.x = iter.position.x();
        temp_point.y = iter.position.y();
        temp_point.z = iter.position.z();
        cloud_frame.points.push_back(temp_point);
      }
    }
    cout << count_0 << endl;
    count_0++;
  }
  cout << "all_points" << all_points.size() << endl;

  sensor::PointCloud filtered_points = sensor::VoxelFilter(0.02).Filter(all_points);
  //   sensor::PointCloud filtered_points = all_points;

  cout << "filtered_points" << filtered_points.size() << endl;

  x_min = filtered_points[0].position.x();
  float x_max = filtered_points[0].position.x();
  y_min = filtered_points[0].position.y();
  float y_max = filtered_points[0].position.y();

  for (const auto &it : filtered_points)
  {
    float temp_x = it.position.x();
    float temp_y = it.position.y();
    if (temp_x < x_min)
    {
      x_min = temp_x;
    }
    if (temp_x > x_max)
    {
      x_max = temp_x;
    }
    if (temp_y < y_min)
    {
      y_min = temp_y;
    }
    if (temp_y > y_max)
    {
      y_max = temp_y;
    }
  }

  cout << x_min << endl;
  cout << x_max << endl;
  cout << y_min << endl;
  cout << y_max << endl;

  //   float resolution=0.05;
  num_x = (x_max - x_min) / resolution;
  num_y = (y_max - y_min) / resolution;

  cells_mat = cv::Mat::zeros(num_x, num_y, CV_8UC3);

  for (const auto &it : filtered_points)
  {
    int row = (it.position.x() - x_min) / resolution;
    int col = (it.position.y() - y_min) / resolution;

    cells_mat.at<cv::Vec3b>(row, col)[0] = 255;
    cells_mat.at<cv::Vec3b>(row, col)[1] = 255;
    cells_mat.at<cv::Vec3b>(row, col)[2] = 255;
  }

  cout << num_x << endl;
  cout << num_y << endl;

  // get the date of point
  for (const auto &it : filtered_points)
  {
    pcl::PointXYZ temp_point;
    temp_point.x = it.position.x();
    temp_point.y = it.position.y();
    temp_point.z = it.position.z();
    cloud_all.points.push_back(temp_point);
  }
}

void mymap_test::addPointsInPicture(pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &T,
                                    cv::Mat &cells_mat, int color)
{
  Eigen::Vector3d t = T.block(0, 3, 3, 1);
  Eigen::Matrix3d r = T.block(0, 0, 3, 3);

  for (int i = 0; i < cloud_frame.points.size(); ++i)
  {
    Eigen::Vector3d temp_point;
    temp_point(0) = cloud_frame.points[i].x;
    temp_point(1) = cloud_frame.points[i].y;
    temp_point(2) = cloud_frame.points[i].z;

    Eigen::Vector3d global_point;
    global_point = r * temp_point + t;

    int row = (global_point.x() - x_min) / resolution;
    int col = (global_point.y() - y_min) / resolution;

    cells_mat.at<cv::Vec3b>(row, col)[0] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[1] = 0;
    cells_mat.at<cv::Vec3b>(row, col)[2] = 0;

    cells_mat.at<cv::Vec3b>(row, col)[color] = 255;
  }
}

PLIcp::PLIcp()
{
  source_cloud_.reset(new pcl::PointCloud<pcl::PointXY>);
  target_cloud_.reset(new pcl::PointCloud<pcl::PointXY>);
  kdtree_source_.reset(new pcl::KdTreeFLANN<pcl::PointXY>());
  epslion_ = 1e-8;
  max_iterations_ = 20;
}

PLIcp::~PLIcp() {}
void PLIcp::setSourceCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
  setCloud(cloud, source_cloud_);
  kdtree_source_->setInputCloud(source_cloud_);
}

void PLIcp::setTargetCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
  setCloud(cloud, target_cloud_);
}
void PLIcp::setCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &input,
                     pcl::PointCloud<pcl::PointXY>::Ptr output)
{
  for (auto point : input->points)
  {
    pcl::PointXY temp;
    temp.x = point.x;
    temp.y = point.y;
    output->points.push_back(temp);
  }
}
void PLIcp::setMaximumIterations(const unsigned int &times) { max_iterations_ = times; }

void PLIcp::setTransformationEpsilon(const float &epslion) { epslion_ = epslion; }

void PLIcp::align(Eigen::Matrix4d &transform)
{
  double T[3];
  T[0] = transform(0, 3);
  T[1] = transform(1, 3);
  Eigen::Matrix3d rotation = transform.block(0, 0, 3, 3);
  Eigen::Quaterniond q_temp(rotation);
  T[2] = GetYaw(q_temp);

  Eigen::Matrix2d R_0;
  double yaw_0 = T[2];
  R_0 << cos(yaw_0), -sin(yaw_0), sin(yaw_0), cos(yaw_0);
  Eigen::Vector2d t_0 = Eigen::Vector2d(T[0], T[1]);
  std::vector<Eigen::Vector2d> target_filter_points;
  int count_before = 0;
  int count_after = 0;
  for (auto point : target_cloud_->points)
  {
    count_before++;
    Eigen::Vector2d origin_p(point.x, point.y);
    Eigen::Vector2d p = R_0 * origin_p + t_0;
    pcl::PointXY p_temp;
    p_temp.x = p(0);
    p_temp.y = p(1);
    std::vector<int> pointSearchInd;
    std::vector<float> pointSearchSqDis;
    kdtree_source_->nearestKSearch(p_temp, 2, pointSearchInd, pointSearchSqDis);
    std::vector<Eigen::Vector2d> point_with_nearest_points;
    point_with_nearest_points.push_back(origin_p);
    for (auto index : pointSearchInd)
    {
      Eigen::Vector2d nearest_point(source_cloud_->points[index].x, source_cloud_->points[index].y);
      point_with_nearest_points.push_back(nearest_point);
    }
    if (point_with_nearest_points.size() != 3)
    {
      //         std::cout << "ERROR" <<std::endl;
      LOG(INFO) << "ERROR";
      continue;
    }
    Eigen::Vector2d dis;
    dis = p - point_with_nearest_points[1];
    LOG(INFO) << "distance = " << dis.norm();
    LOG(INFO) << "dis = " << dis;
    LOG(INFO) << "p0 = " << p;
    LOG(INFO) << "p1 = " << point_with_nearest_points[1];
    if (dis.norm() < 0.1)
    {
      target_filter_points.push_back(origin_p);
      count_after++;
    }
  }

  cout << "befor:" << count_before << endl;
  cout << "after:" << count_after << endl;

  for (int i = 0; i < max_iterations_; i++)
  {
    Eigen::Matrix2d R;
    double yaw = T[2];
    R << cos(yaw), -sin(yaw), sin(yaw), cos(yaw);
    Eigen::Vector2d t = Eigen::Vector2d(T[0], T[1]);

    ceres::Problem problem;
    for (auto point : target_filter_points)
    {
      Eigen::Vector2d origin_p(point.x(), point.y());
      Eigen::Vector2d p = R * origin_p + t;
      pcl::PointXY p_temp;
      p_temp.x = p(0);
      p_temp.y = p(1);
      std::vector<int> pointSearchInd;
      std::vector<float> pointSearchSqDis;
      kdtree_source_->nearestKSearch(p_temp, 2, pointSearchInd, pointSearchSqDis);
      std::vector<Eigen::Vector2d> point_with_nearest_points;
      point_with_nearest_points.push_back(origin_p);
      for (auto index : pointSearchInd)
      {
        Eigen::Vector2d nearest_point(source_cloud_->points[index].x,
                                      source_cloud_->points[index].y);
        point_with_nearest_points.push_back(nearest_point);
      }
      if (point_with_nearest_points.size() != 3)
      {
        std::cout << "ERROR" << std::endl;
        continue;
      }
      ceres::CostFunction *cost_function = new ceres::AutoDiffCostFunction<PLCostFunctor, 1, 3>(
          new PLCostFunctor(point_with_nearest_points[0], point_with_nearest_points[1],
                            point_with_nearest_points[2]));
      problem.AddResidualBlock(cost_function, nullptr, T);
    }
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = false;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    if ((fabs(summary.initial_cost - summary.final_cost) / summary.initial_cost) /
            summary.num_residual_blocks <
        epslion_)
    {
      std::cout << "iterations = " << i << std::endl;
      break;
    }
  }
  //   Eigen::Matrix4d result = Eigen::Matrix4d::Identity();
  std::cout << "origin transform: " << transform << std::endl;
  transform(0, 3) = T[0];
  transform(1, 3) = T[1];
  transform(0, 0) = cos(T[2]);
  transform(0, 1) = -sin(T[2]);
  transform(1, 0) = sin(T[2]);
  transform(1, 1) = cos(T[2]);
  std::cout << "result transform: " << transform << std::endl;
  //   return result;
}

} // namespace map_manager

void ransac_multi_line(int num, pcl::PointCloud<pcl::PointXYZ>::Ptr &ori_cloud,
                       pcl::PointCloud<pcl::PointXYZ> &line_cloud,
                       pcl::PointCloud<pcl::PointXYZ> &line_cloud_1)
{
  for (int i = 0; i < num; i++)
  {
    pcl::SampleConsensusModelLine<pcl::PointXYZ>::Ptr model_line(
        new pcl::SampleConsensusModelLine<pcl::PointXYZ>(ori_cloud));
    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model_line);
    ransac.setDistanceThreshold(0.006);
    ransac.setMaxIterations(1000);
    ransac.computeModel();

    vector<int> inliers;
    ransac.getInliers(inliers);

    //     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line(new
    //     pcl::PointCloud<pcl::PointXYZ>);
    //     pcl::copyPointCloud<pcl::PointXYZ>(*ori_cloud, inliers, *line_cloud);

    int n = inliers.size();

    for (int j = n - 1; j >= 0; j--)
    {
      pcl::PointCloud<pcl::PointXYZ>::iterator index = ori_cloud->begin();
      index = ori_cloud->begin() + inliers[j];
      int index_int = inliers[j];
      pcl::PointXYZ temp_point;
      temp_point.x = ori_cloud->points[index_int].x;
      temp_point.y = ori_cloud->points[index_int].y;
      temp_point.z = ori_cloud->points[index_int].z;
      line_cloud.points.push_back(temp_point);

      ori_cloud->erase(index);
    }

    Eigen::VectorXf coefficient;
    ransac.getModelCoefficients(coefficient);
    cout << "直线点向式方程为：\n"
         << "   (x - " << coefficient[0] << ") / " << coefficient[3] << " = (y - " << coefficient[1]
         << ") / " << coefficient[4] << " = (z - " << coefficient[2] << ") / " << coefficient[5];

    if (i == 0)
    {
      double x_l = -2.8;
      double y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

      for (int k = 0; k < 200; k++)
      {
        x_l = x_l + 0.01;
        y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

        pcl::PointXYZ temp_point;
        temp_point.x = x_l;
        temp_point.y = y_l;
        temp_point.z = 0;
        line_cloud_1.points.push_back(temp_point);
      }
    }
    if (i == 1)
    {
      double x_l = -0.82;
      double y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

      for (int k = 0; k < 382; k++)
      {
        x_l = x_l + 0.01;
        y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

        pcl::PointXYZ temp_point;
        temp_point.x = x_l;
        temp_point.y = y_l;
        temp_point.z = 0;
        line_cloud_1.points.push_back(temp_point);
      }
    }

    if (i == 2)
    {
      double x_l = 1.55;
      double y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

      for (int k = 0; k < 150; k++)
      {
        x_l = x_l + 0.01;
        y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

        pcl::PointXYZ temp_point;
        temp_point.x = x_l;
        temp_point.y = y_l;
        temp_point.z = 0;
        line_cloud_1.points.push_back(temp_point);
      }
    }

    if (i == 3)
    {
      double x_l = -1.32;
      double y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

      for (int k = 0; k < 214; k++)
      {
        x_l = x_l + 0.01;
        y_l = (x_l - coefficient[0]) * coefficient[4] / coefficient[3] + coefficient[1];

        pcl::PointXYZ temp_point;
        temp_point.x = x_l;
        temp_point.y = y_l;
        temp_point.z = 0;
        line_cloud_1.points.push_back(temp_point);
      }
    }
  }
}

void PCDToLDP(const std::string path_pcd, LDP &ldp)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr map_pcd_ptr(new pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPCDFile<pcl::PointXYZ>(path_pcd, *map_pcd_ptr) == -1) //* load the file
  {
    LOG(INFO) << "Couldn't read file test.pcd";
  }

  LOG(INFO) << "Loaded "
            << "/" << map_pcd_ptr->width * map_pcd_ptr->height
            << " data points from test.pcd with the following fields: ";

  unsigned int n = map_pcd_ptr->points.size();
  ldp = ld_alloc_new(n);

  double max_theta_temp = 0;
  double min_theta_temp = 0;

  for (unsigned int i = 0; i < n; i++)
  {
    // calculate position in laser frame

    Eigen::Vector2d temp_point;
    temp_point(0) = map_pcd_ptr->points[i].x;
    temp_point(1) = map_pcd_ptr->points[i].y;

    double r = temp_point.norm();

    ldp->valid[i] = 1;
    ldp->readings[i] = r;

    double temp_theta = atan2(temp_point(1), temp_point(0));

    ldp->theta[i] = temp_theta;
    ldp->cluster[i] = -1;
    if (temp_theta < min_theta_temp)
    {
      min_theta_temp = temp_theta;
    }
    if (temp_theta > max_theta_temp)
    {
      max_theta_temp = temp_theta;
    }
  }

  ldp->min_theta = min_theta_temp;
  ldp->max_theta = max_theta_temp;
  // std::cout << "min_theta: " << ldp->min_theta <<std::endl<< "max_theta: " <<
  // ldp->max_theta <<std::endl;
  ldp->odometry[0] = 0.0;
  ldp->odometry[1] = 0.0;
  ldp->odometry[2] = 0.0;

  ldp->true_pose[0] = 0.0;
  ldp->true_pose[1] = 0.0;
  ldp->true_pose[2] = 0.0;

  cout << "data num:" << n << endl;
}

tf::Transform ToTfPose(Eigen::Matrix4d &piror_T)
{
  tf::Transform t;
  t.setOrigin(tf::Vector3(piror_T(0, 3), piror_T(1, 3), 0.0));

  Eigen::Vector3d ypr;
  Eigen::Matrix3d prior_rot = piror_T.block(0, 0, 3, 3);
  ypr = prior_rot.eulerAngles(2, 1, 0);

  tf::Quaternion q;
  q.setRPY(0.0, 0.0, ypr(0));
  t.setRotation(q);
  return t;
}

void initParams(sm_params &input, sm_result &output)
{
  bool use_cloud_input_ = true;
  if (use_cloud_input_)
  {
    double cloud_range_min_ = 0.1;
    double cloud_range_max_ = 50.0;
    double cloud_res_ = 0.05;

    input.min_reading = cloud_range_min_;
    input.max_reading = cloud_range_max_;
  }

  double kf_dist_linear_ = 0.05;
  double kf_dist_angular_ = 5.0 * (M_PI / 180.0);

  double kf_dist_linear_sq_ = kf_dist_linear_ * kf_dist_linear_;

  // **** What predictions are available to speed up the ICP?
  // 1) imu - [theta] from imu yaw angle - /imu topic
  // 2) odom - [x, y, theta] from wheel odometry - /odom topic
  // 3) vel - [x, y, theta] from velocity predictor - see alpha-beta predictors
  // - /vel topic If more than one is enabled, priority is imu > odom > vel

  bool use_imu_ = false;
  bool use_odom_ = false;
  bool use_vel_ = false;

  // **** Are velocity input messages stamped?
  // if false, will subscribe to Twist msgs on /vel
  // if true, will subscribe to TwistStamped msgs on /vel
  bool stamped_vel_ = false;

  // **** How to publish the output?
  // tf (fixed_frame->base_frame),
  // pose message (pose of base frame in the fixed frame)

  bool publish_tf_ = true;
  bool publish_pose_ = true;
  bool publish_pose_stamped_ = false;
  bool publish_pose_with_covariance_ = false;
  bool publish_pose_with_covariance_stamped_ = false;

  std::vector<double> position_covariance_;
  position_covariance_.resize(3);
  std::fill(position_covariance_.begin(), position_covariance_.end(), 1e-9);

  std::vector<double> orientation_covariance_;
  orientation_covariance_.resize(3);
  std::fill(orientation_covariance_.begin(), orientation_covariance_.end(), 1e-9);

  // **** CSM parameters - comments copied from algos.h (by Andrea Censi)

  // Maximum angular displacement between scans
  input.max_angular_correction_deg = 45.0;

  // Maximum translation between scans (m)
  input.max_linear_correction = 0.50;

  // Maximum ICP cycle iterations
  input.max_iterations = 100;

  // A threshold for stopping (m)
  input.epsilon_xy = 0.0001;

  // A threshold for stopping (rad)
  input.epsilon_theta = 0.0001;

  // Maximum distance for a correspondence to be valid
  input.max_correspondence_dist = 0.3;

  // Noise in the scan (m)
  input.sigma = 0.010;

  // Use smart tricks for finding correspondences.
  input.use_corr_tricks = 1;

  // Restart: Restart if error is over threshold
  input.restart = 0;

  // Restart: Threshold for restarting
  input.restart_threshold_mean_error = 0.01;

  // Restart: displacement for restarting. (m)
  input.restart_dt = 1.0;

  // Restart: displacement for restarting. (rad)
  input.restart_dtheta = 0.1;

  // Max distance for staying in the same clustering
  input.clustering_threshold = 0.25;

  // Number of neighbour rays used to estimate the orientation
  input.orientation_neighbourhood = 20;

  // If 0, it's vanilla ICP
  input.use_point_to_line_distance = 1;

  // Discard correspondences based on the angles
  input.do_alpha_test = 0;

  // Discard correspondences based on the angles - threshold angle, in degrees
  input.do_alpha_test_thresholdDeg = 20.0;

  // Percentage of correspondences to consider: if 0.9,
  // always discard the top 10% of correspondences with more error
  input.outliers_maxPerc = 0.90;

  // Parameters describing a simple adaptive algorithm for discarding.
  //  1) Order the errors.
  //  2) Choose the percentile according to outliers_adaptive_order.
  //     (if it is 0.7, get the 70% percentile)
  //  3) Define an adaptive threshold multiplying outliers_adaptive_mult
  //     with the value of the error at the chosen percentile.
  //  4) Discard correspondences over the threshold.
  //  This is useful to be conservative; yet remove the biggest errors.
  input.outliers_adaptive_order = 0.7;

  input.outliers_adaptive_mult = 2.0;

  // If you already have a guess of the solution, you can compute the polar
  // angle of the points of one scan in the new position. If the polar angle is
  // not a monotone function of the readings index, it means that the surface is
  // not visible in the next position. If it is not visible, then we don't use
  // it for matching.
  input.do_visibility_test = 0;

  // no two points in laser_sens can have the same corr.
  input.outliers_remove_doubles = 1;

  // If 1, computes the covariance of ICP using the method
  // http://purl.org/censi/2006/icpcov
  input.do_compute_covariance = 0;

  // Checks that find_correspondences_tricks gives the right answer
  input.debug_verify_tricks = 0;

  // If 1, the field 'true_alpha' (or 'alpha') in the first scan is used to
  // compute the incidence beta, and the factor (1/cos^2(beta)) used to weight
  // the correspondence.");
  input.use_ml_weights = 0;

  // If 1, the field 'readings_sigma' in the second scan is used to weight the
  // correspondence by 1/sigma^2
  input.use_sigma_weights = 0;

  tf::Transform scan_to_odom_;
  tf::Transform key_frame_to_odom_;

  scan_to_odom_.setIdentity();
  key_frame_to_odom_.setIdentity();

  input.laser[0] = 0.0;
  input.laser[1] = 0.0;
  input.laser[2] = 0.0;

  // Initialize output_ vectors as Null for error-checking
  output.cov_x_m = 0;
  output.dx_dy1_m = 0;
  output.dx_dy2_m = 0;
}

void createTfFromXYTheta(double x, double y, double theta, tf::Transform &t)
{
  t.setOrigin(tf::Vector3(x, y, 0.0));
  tf::Quaternion q;
  q.setRPY(0.0, 0.0, theta);
  t.setRotation(q);
}

// void imshowScanInImage(const sensor_msgs::LaserScan& scan1, const
// sensor_msgs::LaserScan& scan2, const Rigid2d& pose)
// {
//   double min_x = 1e9, min_y= 1e9, max_x= -1e9, max_y= -1e9;
//   vector< Eigen::Vector2d > points1 = scanToPoints(scan1,
//   Rigid2d::Identity()); vector< Eigen::Vector2d > points2 =
//   scanToPoints(scan2, Rigid2d::Identity()); for (size_t i = 0; i <
//   points1.size(); ++i) {
//      Eigen::Vector2d it = points1[i];
//     if(it.x() > max_x)
//       max_x = it.x();
//     if(it.x() < min_x)
//       min_x = it.x();
//     if(it.y() > max_y)
//       max_y = it.y();
//     if(it.y() < min_y)
//       min_y = it.y();
//   }
//   double max_box_size = max(max_x - min_x, max_y - min_y) * 120;
//   cv::Point center((max_x + min_x)*50 + max_box_size/2,  (max_y + min_y)*50 +
//   + max_box_size/2); cv::Mat img(1000,1000, CV_8UC3,cv::Scalar(255,255,255));
//
//   for (size_t i = 0; i < points1.size(); ++i)
//   {
//     Point p(points1[i](0) * 10 + 500, points1[i](1) * 10+ 500);
//     if(p.x < img.rows && p.y < img.cols)
//       circle(img, p,1, cv::Scalar(0, 0, 255));
//   }
//
//   for(size_t j =0; j < points2.size(); j++)
//   {
//     Eigen::Vector2d trans_point = pose * points2[j];
//     Point p(trans_point(0) * 10 + 500, trans_point(1) * 10+ 500);
//     if(p.x < img.rows && p.y < img.cols)
//       circle(img, p, 1, cv::Scalar(255, 0, 0));
//   }
// //   string path = string(getenv("HOME"))+ "/calib_scans" +
// to_string(rand()%100) + ".png";
// //   cout << "all scan_points image path: " << path <<endl;
//   imshow("111", img);
//   waitKey(0);
// }

bool CSMComputePose(LDP &ldp_all, LDP &ldp_frame, tf::Transform &f2l)
{
  sm_params input;
  sm_result output;

  initParams(input, output);

  ldp_all->odometry[0] = 0.0;
  ldp_all->odometry[1] = 0.0;
  ldp_all->odometry[2] = 0.0;

  ldp_all->estimate[0] = 0.0;
  ldp_all->estimate[1] = 0.0;
  ldp_all->estimate[2] = 0.0;

  ldp_all->true_pose[0] = 0.0;
  ldp_all->true_pose[1] = 0.0;
  ldp_all->true_pose[2] = 0.0;

  input.laser_ref = ldp_all;
  input.laser_sens = ldp_frame;

  input.first_guess[0] = f2l.getOrigin().getX();
  input.first_guess[1] = f2l.getOrigin().getY();
  input.first_guess[2] = tf::getYaw(f2l.getRotation());

  // If they are non-Null, free covariance gsl matrices to avoid leaking memory
  if (output.cov_x_m)
  {
    gsl_matrix_free(output.cov_x_m);
    output.cov_x_m = 0;
  }
  if (output.dx_dy1_m)
  {
    gsl_matrix_free(output.dx_dy1_m);
    output.dx_dy1_m = 0;
  }
  if (output.dx_dy2_m)
  {
    gsl_matrix_free(output.dx_dy2_m);
    output.dx_dy2_m = 0;
  }

  // *** scan match - using point to line icp from CSM

  sm_icp(&input, &output);
  tf::Transform corr_ch;
  bool flag = false;
  if (output.valid)
  {
    createTfFromXYTheta(output.x[0], output.x[1], output.x[2], corr_ch);
    f2l = corr_ch;
    // imshowScanInImage(scan1,scan2, ToRigid2d(f2l));

    flag = true;
    if (output.error > 10.80)
    {
      std::cout << "***************************too big error: " << output.error
                << "***************************" << std::endl;
      flag = false;
    }
  }
  else
  {
    std::cout << "***************************not valid !!!***************************" << std::endl;
  }

  ld_free(ldp_all);
  ld_free(ldp_frame);
  return flag;
}

int main(int argc, char **argv)
{
  const std::string path = "/home/shao/map/pxcc-200-3-260";

  //   const std::string path="/home/shao/map/axu";
  if (0)
  {
    PbstreamInfos infos;

    pcl::PointCloud<pcl::PointXYZ> map_cloud;
    pcl::PointCloud<pcl::PointXYZ> map_cloud_frame;

    Eigen::Matrix4d prior_T = Eigen::Matrix4d::Identity();
    cv::Mat cells_mat;

    mymap_test test;

    cout << "myflag" << endl;

    test.readPointsFromPbstream(path, infos, map_cloud, map_cloud_frame, prior_T, cells_mat);

    cout << "myflag" << endl;
    //   add x error

    prior_T(0, 3) = prior_T(0, 3) - 0.05;
    prior_T(1, 3) = prior_T(1, 3) - 0.1;

    cout << "prior_T:" << prior_T << endl;

    test.addPointsInPicture(map_cloud_frame, prior_T, cells_mat, 2);

    PLIcp myIcp;

    pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud_frame_ptr(new pcl::PointCloud<pcl::PointXYZ>);

    map_cloud_ptr = map_cloud.makeShared();
    map_cloud_frame_ptr = map_cloud_frame.makeShared();

    // OUR ICP--------------------------------------------------------------
    myIcp.setSourceCloud(map_cloud_ptr);
    myIcp.setTargetCloud(map_cloud_frame_ptr);

    myIcp.align(prior_T);

    //  --------------------------------------------------------------------

    Eigen::Matrix4d prior_T_d;

    //    PCL ICP--------------------------------------------------------------
    if (0)
    {
      Eigen::Matrix4f prior_T_f;
      for (int i = 0; i < 4; i++)
      {
        for (int j = 0; j < 4; j++)
        {
          prior_T_f(i, j) = prior_T(i, j);
        }
      }

      pcl::PointCloud<pcl::PointXYZ>::Ptr icp_result_point_cloud_ptr(
          new pcl::PointCloud<pcl::PointXYZ>);
      pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
      icp.setMaximumIterations(20);
      icp.setTransformationEpsilon(1e-8);
      icp.setMaxCorrespondenceDistance(1.0);
      icp.setInputSource(map_cloud_frame_ptr);
      icp.setInputTarget(map_cloud_ptr);
      //   chrono::steady_clock::time_point icp_t1 =
      //   chrono::steady_clock::now();
      icp.align(*icp_result_point_cloud_ptr, prior_T_f);
      //   chrono::steady_clock::time_point icp_t2 =
      //   chrono::steady_clock::now(); chrono::duration<double> icp_time_used =
      //   chrono::duration_cast<chrono::duration<double>>(icp_t2 - icp_t1);
      Eigen::Matrix4f icp_result_T = icp.getFinalTransformation();

      cout << icp_result_T << endl;

      for (int i = 0; i < 4; i++)
      {
        for (int j = 0; j < 4; j++)
        {
          prior_T_d(i, j) = icp_result_T(i, j);
        }
      }
    }

    //  --------------------------------------------------------------------------------------

    //  PCL NDT--------------------------------------------------------------
    if (0)
    {
      Eigen::Matrix4f prior_T_f;
      for (int i = 0; i < 4; i++)
      {
        for (int j = 0; j < 4; j++)
        {
          prior_T_f(i, j) = prior_T(i, j);
        }
      }
      pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt;
      ndt.setMaximumIterations(20);
      ndt.setTransformationEpsilon(1e-8);
      ndt.setStepSize(0.1);
      ndt.setResolution(1);
      ndt.setInputSource(map_cloud_frame_ptr);
      ndt.setInputTarget(map_cloud_ptr);
      pcl::PointCloud<pcl::PointXYZ>::Ptr ndt_result_point_cloud_ptr(
          new pcl::PointCloud<pcl::PointXYZ>);

      ndt.align(*ndt_result_point_cloud_ptr, prior_T_f);

      Eigen::Matrix4f ndt_result_T = ndt.getFinalTransformation();

      for (int i = 0; i < 4; i++)
      {
        for (int j = 0; j < 4; j++)
        {
          prior_T_d(i, j) = ndt_result_T(i, j);
        }
      }
    }

    map_cloud_ptr->height = 1;
    map_cloud_ptr->width = map_cloud_ptr->size();
    map_cloud_frame_ptr->height = 1;
    map_cloud_frame_ptr->width = map_cloud_frame_ptr->size();

    pcl::PCLPointCloud2 cloud_all, cloud_frame;
    pcl::toPCLPointCloud2(*map_cloud_ptr, cloud_all);
    pcl::toPCLPointCloud2(*map_cloud_frame_ptr, cloud_frame);

    //   pcl::io::saveVTKFile("/home/shao/project/kdevelopBuild/map_manager/map_manager/result/test.vtk",
    //   cloud_all);
    //   pcl::io::saveVTKFile("/home/shao/project/kdevelopBuild/map_manager/map_manager/result/frame.vtk",
    //   cloud_frame);

    test.addPointsInPicture(map_cloud_frame, prior_T, cells_mat, 0);

    //   show the picture
    cv::imshow("Display window", cells_mat);
    cv::waitKey(0);

    //   save the picture
    string img_name = path + "/my_test.png";

    cv::imwrite(img_name, cells_mat);

    map_cloud_ptr->height = 1;
    map_cloud_ptr->width = map_cloud_ptr->size();

    pcl::PCDWriter writer;
    writer.write(path + "/test002.pcd", *map_cloud_ptr);

    map_cloud_frame_ptr->height = 1;
    map_cloud_frame_ptr->width = map_cloud_frame_ptr->size();
    writer.write(path + "/test_frame.pcd", *map_cloud_frame_ptr);

    pcl::PLYWriter writer_ply;
    writer_ply.write(path + "/test002.ply", *map_cloud_ptr);

    if (0)
    {
      string csvOutput_all = "/home/shao/map/ICPtest/test005.csv";

      ofstream zos(csvOutput_all);
      for (const auto &iter : map_cloud_ptr->points)
      {
        zos << iter.x << "," << iter.y << endl;
      }
      cout << "done!";
    }

    if (0)
    {
      string csvOutput_all = "/home/shao/map/ICPtest/test005_frame.csv";

      ofstream zos(csvOutput_all);
      for (const auto &iter : map_cloud_frame_ptr->points)
      {
        zos << iter.x - 0.2 << "," << iter.y - 0.1 << endl;
      }
      cout << "done!";
    }

    // ransac && line fit

    if (0)
    {
      pcl::PointCloud<pcl::PointXYZ> line_d;
      pcl::PointCloud<pcl::PointXYZ> line_d_1;

      ransac_multi_line(4, map_cloud_ptr, line_d, line_d_1);

      pcl::PointCloud<pcl::PointXYZ>::Ptr line_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::PointXYZ>::Ptr line_cloud_ptr_1(new pcl::PointCloud<pcl::PointXYZ>);

      line_cloud_ptr = line_d.makeShared();

      line_cloud_ptr_1 = line_d_1.makeShared();

      line_cloud_ptr->height = 1;
      line_cloud_ptr->width = line_cloud_ptr->size();

      line_cloud_ptr_1->height = 1;
      line_cloud_ptr_1->width = line_cloud_ptr_1->size();

      writer.write("/home/shao/map/ICPTest/line_0.pcd", *line_cloud_ptr);
      writer.write("/home/shao/map/ICPTest/line_1.pcd", *line_cloud_ptr_1);
    }

    if (0)
    {
      const std::string path_pcd = "/home/shao/map/ICPtest/test005.pcd";
      const std::string path_pcd_frame = "/home/shao/map/ICPtest/test_frame.pcd";
      LDP ldp_all, ldp_frame;
      PCDToLDP(path_pcd, ldp_all);
      PCDToLDP(path_pcd_frame, ldp_frame);

      tf::Transform f2l = ToTfPose(prior_T);
      //     LOG(INFO)<<"f2l = " << f2l;

      if (CSMComputePose(ldp_all, ldp_frame, f2l))
      {
        //       LOG(INFO)<<"f2l = " << f2l;
      }
      else
      {
        cout << "failed" << endl;
      }
    }
  }

  // test read pcd from probability grid pic
  if (0)
  {
    // read parameter
    {
      int size_of_map_[2];
      double maxes_[2];
      double resolution_t;
      {
        boost::property_tree::ptree pt1;
        std::string submap_filepath = path + "/frames/0";
        boost::property_tree::read_xml(submap_filepath + "/data.xml", pt1);

        std::string data;

        resolution_t = pt1.get<double>("probability_grid.resolution");
        data = pt1.get<std::string>("probability_grid.max");
        sscanf(data.c_str(), "%lf %lf", &maxes_[0], &maxes_[1]);
        data = pt1.get<std::string>("probability_grid.num_cells");
        sscanf(data.c_str(), "%d %d", &size_of_map_[0], &size_of_map_[1]);

        cout << "size_of_map_: " << size_of_map_[0] << " " << size_of_map_[1] << endl;
        cout << "maxes_: " << maxes_[0] << " " << maxes_[1] << endl;
        cout << "resolution: " << resolution_t << endl;
      }

      Mat cellMat = imread(path + "/frames/0/probability_grid.png", -1);

      int channels = cellMat.channels();
      //   show the picture
      cv::imshow("Display window", cellMat);
      cv::waitKey(0);

      pcl::PointCloud<pcl::PointXYZ> probability_map;
      pcl::PointCloud<pcl::PointXYZ>::Ptr probability_map_ptr(new pcl::PointCloud<pcl::PointXYZ>);

      for (int i = 0; i < size_of_map_[0]; i++)
      {
        for (int j = 0; j < size_of_map_[1]; j++)
        {
          double pro;

          int cv_value = cellMat.at<uint16_t>(i, j);
          if (cv_value == 0)
          {
            continue;
          }
          else
          {
            pro = cv_value / (255 * 255.0);
          }

          // 	  cout<<"cv_value: "<<cv_value<<endl;
          if (pro < 0.25)
          {
            // 	    cout<<"cv_value: "<<cv_value<<endl;
            pcl::PointXYZ temp_point;

            temp_point.x = maxes_[0] - i * resolution_t;
            temp_point.y = maxes_[1] - j * resolution_t;
            temp_point.z = 0;
            probability_map.points.push_back(temp_point);
          }
        }
      }

      probability_map_ptr = probability_map.makeShared();

      probability_map_ptr->height = 1;
      probability_map_ptr->width = probability_map_ptr->size();

      pcl::PCDWriter writer;
      writer.write(path + "/probability_map.pcd", *probability_map_ptr);

      pcl::PLYWriter writer_ply;
      writer_ply.write(path + "/probability_map.ply", *probability_map_ptr);
    }

    //
  }

  {
    io::ProtoStreamReader stream(path + "/map.pbstream");
    io::ProtoStreamDeserializer deserializer(&stream);

    // Create a copy of the pose_graph_proto, such that we can re-write the
    // trajectory ids.
    proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
    const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();

    std::map<int, int> trajectory_remapping;
    LOG(WARNING) << "pose_graph_proto size: " << pose_graph_proto.trajectory_size();
    for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
    {
      auto &trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
      const auto &options_with_sensor_ids_proto =
          all_builder_options_proto.options_with_sensor_ids(i);
      const int new_trajectory_id = 0;
      LOG(INFO) << trajectory_proto.trajectory_id() << ", " << new_trajectory_id;
      CHECK(
          trajectory_remapping.emplace(trajectory_proto.trajectory_id(), new_trajectory_id).second)
          << "Duplicate trajectory ID: " << trajectory_proto.trajectory_id();
      trajectory_proto.set_trajectory_id(new_trajectory_id);
    }

    // Apply the calculated remapping to constraints in the pose graph proto.
    for (auto &constraint_proto : *pose_graph_proto.mutable_constraint())
    {
      if (trajectory_remapping.find(constraint_proto.submap_id().trajectory_id()) ==
              trajectory_remapping.end() ||
          trajectory_remapping.find(constraint_proto.node_id().trajectory_id()) ==
              trajectory_remapping.end())
        continue;
      constraint_proto.mutable_submap_id()->set_trajectory_id(
          trajectory_remapping.at(constraint_proto.submap_id().trajectory_id()));
      constraint_proto.mutable_node_id()->set_trajectory_id(
          trajectory_remapping.at(constraint_proto.node_id().trajectory_id()));
    }

    cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                   cartographer::transform::Rigid3d>
        submap_poses_;

    for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
    {
      if (trajectory_remapping.find(trajectory_proto.trajectory_id()) == trajectory_remapping.end())
        continue;
      LOG(WARNING) << "trajectory_proto.trajectory_id: " << trajectory_proto.trajectory_id() << ", "
                   << trajectory_proto.submap().size();
      for (const proto::Trajectory::Submap &submap_proto : trajectory_proto.submap())
      {
        submap_poses_.Insert(cartographer::mapping::SubmapId{trajectory_proto.trajectory_id(),
                                                             submap_proto.submap_index()},
                             cartographer::transform::ToRigid3(submap_proto.pose()));
      }
    }

    std::map<NodeId, transform::Rigid3d> origin_node_poses_;
    for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
    {
      if (trajectory_remapping.find(trajectory_proto.trajectory_id()) == trajectory_remapping.end())
        continue;
      for (const proto::Trajectory::Node &node_proto : trajectory_proto.node())
      {
        origin_node_poses_[NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()}] =
            transform::ToRigid3(node_proto.pose());
      }
    }
    LOG(WARNING) << "origin_node_poses size: " << origin_node_poses_.size();

    MapById<SubmapId, mapping::proto::Submap> submap_id_to_submap;
    MapById<NodeId, mapping::proto::Node> node_id_to_node;

    std::unique_ptr<PoseGraph> pose_graph_;
    mapping::proto::SerializedData proto;
    while (deserializer.ReadNextSerializedData(&proto))
    {
      switch (proto.data_case())
      {
      case mapping::proto::SerializedData::kPoseGraph:
        LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
                      "stream likely corrupt!.";
        break;
      case mapping::proto::SerializedData::kAllTrajectoryBuilderOptions:
        LOG(ERROR) << "Found multiple serialized "
                      "`AllTrajectoryBuilderOptions`. Serialized stream likely "
                      "corrupt!.";
        break;
      case mapping::proto::SerializedData::kSubmap:
      {
        if (trajectory_remapping.find(proto.submap().submap_id().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        proto.mutable_submap()->mutable_submap_id()->set_trajectory_id(
            trajectory_remapping.at(proto.submap().submap_id().trajectory_id()));
        submap_id_to_submap.Insert(SubmapId{proto.submap().submap_id().trajectory_id(),
                                            proto.submap().submap_id().submap_index()},
                                   proto.submap());
        break;
      }
      case mapping::proto::SerializedData::kNode:
      {
        if (trajectory_remapping.find(proto.node().node_id().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        proto.mutable_node()->mutable_node_id()->set_trajectory_id(
            trajectory_remapping.at(proto.node().node_id().trajectory_id()));
        const NodeId node_id(proto.node().node_id().trajectory_id(),
                             proto.node().node_id().node_index());
        const transform::Rigid3d &node_pose = origin_node_poses_.at(node_id);
        pose_graph_->AddNodeFromProto(node_pose, proto.node());
        node_id_to_node.Insert(node_id, proto.node());
        break;
      }
      case mapping::proto::SerializedData::kTrajectoryData:
      {
        if (trajectory_remapping.find(proto.trajectory_data().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        if (trajectory_remapping.find(proto.trajectory_data().trajectory_id()) ==
            trajectory_remapping.end())
        {
          LOG(WARNING) << proto.trajectory_data().trajectory_id();
          break;
        }
        proto.mutable_trajectory_data()->set_trajectory_id(
            trajectory_remapping.at(proto.trajectory_data().trajectory_id()));
        pose_graph_->SetTrajectoryDataFromProto(proto.trajectory_data());
        break;
      }
      case mapping::proto::SerializedData::kImuData:
      {
        if (trajectory_remapping.find(proto.imu_data().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        //         if (load_frozen_state) break;
        if (trajectory_remapping.find(proto.imu_data().trajectory_id()) ==
            trajectory_remapping.end())
        {
          LOG(WARNING) << proto.imu_data().trajectory_id();
          break;
        }
        pose_graph_->AddImuData(trajectory_remapping.at(proto.imu_data().trajectory_id()),
                                sensor::FromProto(proto.imu_data().imu_data()));
        break;
      }
      case mapping::proto::SerializedData::kOdometryData:
      {
        //         if (load_frozen_state) break;
        if (trajectory_remapping.find(proto.odometry_data().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        if (trajectory_remapping.find(proto.odometry_data().trajectory_id()) ==
            trajectory_remapping.end())
        {
          LOG(WARNING) << proto.odometry_data().trajectory_id();
          break;
        }
        pose_graph_->AddOdometryData(trajectory_remapping.at(proto.odometry_data().trajectory_id()),
                                     sensor::FromProto(proto.odometry_data().odometry_data()));
        break;
      }
      case mapping::proto::SerializedData::kFixedFramePoseData:
      {
        //         if (load_frozen_state) break;
        if (trajectory_remapping.find(proto.fixed_frame_pose_data().trajectory_id()) ==
            trajectory_remapping.end())
          break;
        if (trajectory_remapping.find(proto.fixed_frame_pose_data().trajectory_id()) ==
            trajectory_remapping.end())
        {
          LOG(WARNING) << proto.fixed_frame_pose_data().trajectory_id();
          continue;
        }
        pose_graph_->AddFixedFramePoseData(
            trajectory_remapping.at(proto.fixed_frame_pose_data().trajectory_id()),
            sensor::FromProto(proto.fixed_frame_pose_data().fixed_frame_pose_data()));
        break;
      }
      case mapping::proto::SerializedData::kLandmarkData:
      {
        break;
      }
      default:
        LOG(WARNING) << "Skipping unknown message type in stream: " << proto.GetTypeName();
      }
    }
  }

  return 0;
}
