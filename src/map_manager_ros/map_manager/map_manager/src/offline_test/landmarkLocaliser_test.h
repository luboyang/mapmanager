/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef LANDMARKLOCALISER_TEST_H
#define LANDMARKLOCALISER_TEST_H

#include <cartographer/io/image.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer/sensor/point_cloud.h>
#include <cartographer/transform/rigid_transform.h>
#include <cartographer/transform/transform.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/impl/instantiate.hpp>
#include <pcl/search/impl/search.hpp>

#include "map_manager_ulitity.h"
#include "utility/kd_tree.h"

namespace map_manager
{
class LandmarkTest
{
  public:
  float resolution = 0.05;
  float x_min;
  float y_min;
  int num_x;
  int num_y;

  void readPointsFromPbstream(const std::string &path, PbstreamInfos &infos,
                              pcl::PointCloud<pcl::PointXYZ> &cloud_all,
                              pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &prior_T,
                              cv::Mat &cells_mat);
  void addPointsInPicture(pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &T,
                          cv::Mat &cells_mat, int color);
  void addPointsInPicture_ori(pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &T,
                              cv::Mat &cells_mat, int color);
  void start(const std::string &origin_path);

  private:
  std::string origin_path1_;

  std::unique_ptr<kd_tree::KDTree> kd_tree_;
  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      node_poses;
  std::map<std::string, cartographer::transform::Rigid3d> landmarks_;
  std::vector<cartographer::sensor::LandmarkData> landmark_datas_;

  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::TrajectoryNode::Data>
      node_constant_datas_;

  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::optimization::NodeSpec2D>
      node_data_;

  std::map<std::string /* landmark ID */, cartographer::mapping::PoseGraphInterface::LandmarkNode>
      landmark_nodes;

  void loadPbstream(const std::string &path);
};
} // namespace map_manager
#endif
