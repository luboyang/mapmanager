
#include "../map_tools/merge_map.h"
#include "dbg.h"
#include "maps_area.h"
#include "pcl/io/pcd_io.h"
using namespace std;
using namespace cv;
using namespace cartographer;
transform::Rigid2d getTrajectoryArea(const vector<Point2f> &ori_points)
{
  std::vector<Eigen::Vector2d> points;
  for (auto it : ori_points)
    points.push_back({it.x, it.y});
  LOG(WARNING) << ori_points.size();
  Eigen::Vector2d new_x_towards = (points[3] - points[0]).normalized();
  Eigen::Vector2d new_y_towards = (points[1] - points[0]).normalized();
  float at1 = atan2(new_x_towards(1), new_x_towards(0));
  if (at1 > M_PI / 2) at1 = at1 - 2 * M_PI;
  float at2 = atan2(new_y_towards(1), new_y_towards(0));
  Eigen::Matrix2d map_R_area;
  LOG(WARNING) << at1 << ", " << at2 << ", " << fabs(at1 + M_PI / 2 - at2);
  if (fabs(at1 + M_PI / 2 - at2) > 0.5)
    map_R_area << new_y_towards[0], new_x_towards[0], new_y_towards[1], new_x_towards[1];
  else
    map_R_area << new_x_towards[0], new_y_towards[0], new_x_towards[1], new_y_towards[1];
  Eigen::Vector2d map_t_area = points[0];

  transform::Rigid2d map_T_area(map_t_area, Eigen::Rotation2Dd(map_R_area));
  return map_T_area;
}

transform::Rigid2d getTransformFromImagePoints(const Eigen::Vector2d &max1,
                                               const Eigen::Vector2d &max2,
                                               const Eigen::Vector2d &img_point,
                                               const double &angle, const double &resolution)
{
  Eigen::Vector2d t1;
  t1[0] = max1[0] - img_point[1] * resolution;
  t1[1] = max1[1] - img_point[0] * resolution;
  transform::Rigid2d img2map1(t1, angle);
  LOG(INFO) << img2map1;
  transform::Rigid2d img2map2({max2[0], max2[1]}, 0);
  LOG(INFO) << img2map2;
  return img2map1 * img2map2.inverse();
}

transform::Rigid2d getRelativePose(const string &path1, const string &path2)
{
  ImageOpreator::MapsArea tmp1(path1);
  vector<tuple<bool, string, vector<Point2f>>> infos1 = tmp1.ImgInfos();
  ImageOpreator::MapsArea tmp2(path2);
  vector<tuple<bool, string, vector<Point2f>>> infos2 = tmp2.ImgInfos();

  transform::Rigid2d trans1 = getTrajectoryArea(get<2>(infos1.front()));
  transform::Rigid2d trans2 = getTrajectoryArea(get<2>(infos2.front()));
  return trans1 * trans2.inverse();
}

void getMapFromPcd(const std::string &path, float height)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1(new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ>(path, *cloud1) == -1)
  {
    LOG(INFO) << ("Couldn't read pcd");
  }
  //   float height = -0.85;
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  float min_x = 1e9, min_y = 1e9, max_x = -1e9, max_y = -1e9;
  for (const pcl::PointXYZ &p : cloud1->points)
  {
    if (p.z > height + 0.03 || p.z < height - 0.03) continue;
    if (p.x > max_x) max_x = p.x;
    if (p.y > max_y) max_y = p.y;
    if (p.x < min_x) min_x = p.x;
    if (p.y < min_y) min_y = p.y;
    filtered_cloud->push_back(p);
  }
  const int rows = (max_x - min_x) * 20 + 1;
  const int cols = (max_y - min_y) * 20 + 1;
  LOG(INFO) << min_x << ", " << min_y << ", " << max_x << ", " << max_y;
  LOG(INFO) << filtered_cloud->size();
  cv::Mat debug_img(rows, cols, CV_8UC3, cv::Scalar(255, 255, 255));
  Mat result(rows, cols, CV_16UC1, cv::Scalar(32767));
  for (const pcl::PointXYZ &p : filtered_cloud->points)
  {
    int img_x = (max_y - p.y) * 20;
    int img_y = (max_x - p.x) * 20;
    result.at<uint16_t>(img_y, img_x) = 10;
    // //     if(img_x < 0 || img_x >= 1000 || img_y < 0 || img_y >= 1000)
    // //       continue;
    //     cv::circle(debug_img, {img_x, img_y}, 0.5, cv::Scalar(0,255,0));
  }
  imwrite("/home/cyy/map/lidar/frames/0/probability_grid.png", result);
  Mat image8(result.rows, result.cols, CV_8UC1);
  for (int i = 0; i < result.rows; i++)
    for (int j = 0; j < result.cols; j++)
    {
      int value = result.at<uint16_t>(i, j);
      if (value == 0)
        image8.at<uchar>(i, j) = 128;
      else
        image8.at<uchar>(i, j) = value / 128;
    }

  imwrite("/home/cyy/map/lidar/map.png", image8);
  //   waitKey(0);

  boost::property_tree::ptree p_map;
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  p_map.put("id", 0);

  string data = "0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 1.000000";
  p_map.put("pose", data);
  p_map.put("local_pose", data);
  data.clear();

  p_map.put("num_range_data", 1);

  p_map.put("finished", true);

  boost::property_tree::ptree p_probability_grid;
  uint num_x_cells, num_y_cells;

  p_probability_grid.put("resolution", 0.05);
  data = std::to_string(max_x) + " " + std::to_string(max_y);
  p_probability_grid.put("max", data);
  data.clear();
  num_x_cells = cols;
  num_y_cells = rows;
  data = std::to_string(num_x_cells) + " " + std::to_string(num_y_cells);
  p_probability_grid.put("num_cells", data);
  data.clear();
  data = std::to_string(0) + " " + std::to_string(0) + " " + std::to_string(num_x_cells - 1) + " " +
         std::to_string(num_y_cells - 1);
  p_probability_grid.put("known_cells_box", data);
  p_map.add_child("probability_grid", p_probability_grid);
  boost::property_tree::write_xml("/home/cyy/map/lidar/frames/0/data.xml", p_map, std::locale(),
                                  setting);
}
int main(int argc, char **argv)
{
  if (argc < 2) return 1;
  google::InitGoogleLogging(argv[0]);
  google::SetStderrLogging(google::GLOG_INFO);
  FLAGS_colorlogtostderr = true;
  ros::init(argc, argv, "maps_align_node");
  ros::start();
  LOG(INFO) << argc;
  //   if(argc == 2 )
  //   {
  //     float height = stof(string(argv[1]));
  //     getMapFromPcd("/mnt/hgfs/sharefiles/do_not_remove/Jinsheng_seg_1cm.pcd",
  //     height); return 0;
  //   }

  string path1 = string(getenv("HOME")) + "/map/" + argv[1];
  string path2 = string(getenv("HOME")) + "/map/" + argv[2];
  LOG(INFO) << path1;
  LOG(INFO) << path2;
  transform::Rigid2d relative_pose;
  if (argc == 6)
  {
    string px = argv[3];
    string py = argv[4];
    string yaw = argv[5];
    relative_pose = transform::Rigid2d({stof(px), stof(py)}, stof(yaw));
  }
  else
  {
    relative_pose = getRelativePose(path1, path2);
    dbg(relative_pose);
    dbg(transform::Embed3D(relative_pose));
  }
  //   transform::Rigid2d relative_pose({3.425, 11.699},-1.69);
  //   transform::Rigid2d relative_pose({94.467, -39.840}, -0.0767726);
  map_manager::MergeMap m;
  m.start(path1, path2, transform::Embed3D(relative_pose));
  m.save(path2 + "_merge");
  google::ShutdownGoogleLogging();
  return 0;
}