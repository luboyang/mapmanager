/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MYMAP_TEST_H
#define MYMAP_TEST_H

#include <ceres/ceres.h>
#include <csm_eigen/csm_all.h> // csm defines min and max, but Eigen complains
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <Eigen/Eigen>
#include <iostream>

#include "map_manager_ulitity.h"

namespace map_manager
{
class mymap_test
{
  public:
  float resolution = 0.05;
  float x_min;
  float y_min;
  int num_x;
  int num_y;

  void readPointsFromPbstream(const std::string &path, PbstreamInfos &infos,
                              pcl::PointCloud<pcl::PointXYZ> &cloud_all,
                              pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &prior_T,
                              cv::Mat &cells_mat);
  void addPointsInPicture(pcl::PointCloud<pcl::PointXYZ> &cloud_frame, Eigen::Matrix4d &T,
                          cv::Mat &cells_mat, int color);
};

class PLCostFunctor
{
  public:
  static ceres::CostFunction *CreateAutoDiffCostFunction(const Eigen::Vector2d &point,
                                                         const Eigen::Vector2d &nearest_point1,
                                                         const Eigen::Vector2d &nearest_point2)
  {
    return new ceres::AutoDiffCostFunction<PLCostFunctor, 1 /* residuals */,
                                           3 /* pose variables */>(
        new PLCostFunctor(point, nearest_point1, nearest_point2));
  }

  template <typename T> bool operator()(const T *const pose, T *residual) const
  {
    T yaw = pose[2];
    Eigen::Matrix<T, 2, 2> R;
    R << ceres::cos(yaw), -ceres::sin(yaw), ceres::sin(yaw), ceres::cos(yaw);
    Eigen::Map<const Eigen::Matrix<T, 2, 1>> t(pose);
    Eigen::Matrix<T, 2, 1> p = R * point_.template cast<T>() + t;
    Eigen::Matrix<T, 2, 1> vector_pq = nearest_point1_.template cast<T>() - p;
    residual[0] = vector_pq.dot(vertical_vector_.template cast<T>());
    return true;
  }

  explicit PLCostFunctor(const Eigen::Vector2d &point, const Eigen::Vector2d &nearest_point1,
                         const Eigen::Vector2d &nearest_point2)
      : point_(point), nearest_point1_(nearest_point1),
        vertical_vector_(Eigen::Vector2d((nearest_point2 - nearest_point1)[1],
                                         -(nearest_point2 - nearest_point1)[0]))
  {
  }

  PLCostFunctor(const PLCostFunctor &) = delete;
  PLCostFunctor &operator=(const PLCostFunctor &) = delete;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  private:
  const Eigen::Vector2d point_;
  const Eigen::Vector2d nearest_point1_;
  const Eigen::Vector2d vertical_vector_;
};

class PLIcp
{
  public:
  PLIcp();
  ~PLIcp();

  template <typename T> T GetYaw(const Eigen::Quaternion<T> &rotation)
  {
    const Eigen::Matrix<T, 3, 1> direction = rotation * Eigen::Matrix<T, 3, 1>::UnitX();
    return atan2(direction.y(), direction.x());
  }
  void setCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &input,
                pcl::PointCloud<pcl::PointXY>::Ptr output);
  //   template <typename PointType>
  void setSourceCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
  //   template <typename PointType>
  void setTargetCloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
  void setMaximumIterations(const unsigned int &times);
  void setTransformationEpsilon(const float &epslion);
  void align(Eigen::Matrix4d &prior);

  private:
  //   template <typename PointType>

  unsigned int max_iterations_;
  float epslion_;

  pcl::PointCloud<pcl::PointXY>::Ptr source_cloud_, target_cloud_;
  pcl::KdTreeFLANN<pcl::PointXY>::Ptr kdtree_source_;
};

} // namespace map_manager

#endif // MYMAP_TEST_H
