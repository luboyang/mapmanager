#ifndef MAPSAREA_H_
#define MAPSAREA_H_
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

namespace ImageOpreator
{
using namespace std;
using namespace cv;

struct MouseParam
{
  Mat img;           //用于画好一个后显示
  Mat imgZoomBackup; //用于zoom的还原备份
  Mat imgTmp;        //用于实时显示
  Mat imgBackup;     //清空，显示最初的图
  Point pt1;
  Point pt2;
  bool mouseLflag;
  float scale;
  std::vector<Point> line_points;
};

cv::Point2f final_points[4];

bool inline judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    std::cout << "\033[33m" << file_name << " is not existed!\033[37m" << std::endl;
    existed_flag = false;
  }
  return existed_flag;
}

vector<Point2f> computeCorners(const vector<Point> &line_pts, const Point &c)
{
  Point a, b;
  a = line_pts[0];
  b = line_pts[1];
  float k1 = (b.y - a.y) * 1.0 / (b.x - a.x);

  Point2f p1, p2;
  p1.x = (k1 * k1 * (c.x) - k1 * c.y + b.x + k1 * (b.y)) / (1.0 + k1 * k1);
  p1.y = k1 * (p1.x - c.x) + c.y;

  p2.x = (k1 * k1 * c.x - k1 * c.y + a.x + k1 * (a.y)) / (1.0 + k1 * k1);
  p2.y = k1 * (p2.x - c.x) + c.y;

  return {p1, p2};
};
void draw_crossline(Mat &img, const vector<Point> line_pts, const Point &pt1, const Point &pt2)
{
  circle(img, pt1, 2, Scalar(255, 0, 255));
  circle(img, pt2, 2, Scalar(255, 0, 255));
  cv::line(img, line_pts[0], line_pts[1], Scalar(0, 255, 0), 2);
  cv::line(img, line_pts[1], pt1, Scalar(255, 255, 0), 2);
  cv::line(img, pt1, pt2, Scalar(255, 255, 0), 2);
  cv::line(img, pt2, line_pts[0], Scalar(255, 255, 0), 2);
  //   cv::line(img, pth1, pth2, Scalar(255, 255, 0), 1);
}

void draw_save_points(Mat &img, const vector<Point2f> line_pts)
{
  cv::line(img, line_pts[0], line_pts[1], Scalar(0, 255, 0), 2);
  cv::line(img, line_pts[1], line_pts[2], Scalar(255, 255, 0), 2);
  cv::line(img, line_pts[2], line_pts[3], Scalar(255, 255, 0), 2);
  cv::line(img, line_pts[3], line_pts[0], Scalar(255, 255, 0), 2);
}

void on_mouse(int event, int x, int y, int flags, void *param)
{
  MouseParam *par = (MouseParam *)param;
  Point pt(x, y);
  double value;
  float step = 0.05;
  //   Mat imgzoom;
  //
  if (event == EVENT_RBUTTONDOWN) //按下右键，重画
  {
    par->img = par->imgBackup.clone();
  }

  else if (event == CV_EVENT_MBUTTONDOWN)
  {
    if (par->line_points.size() == 2)
    {
      par->line_points.clear();
      par->img = par->imgBackup.clone();
    }
    cout << pt << endl;
    par->line_points.push_back(pt);
    par->pt1 = pt;
    par->pt2 = pt;
    circle(par->img, pt, 2, Scalar(255, 0, 0));
    par->mouseLflag = true;
    cout << pt << endl;
  }
  else if (!par->line_points.empty() && event == CV_EVENT_MOUSEMOVE &&
           flags == CV_EVENT_FLAG_MBUTTON)
  {
    par->pt2 = pt;
  }
  else if (par->line_points.size() == 2 && event == CV_EVENT_MBUTTONUP)
  {
    par->pt2 = pt;
    auto points = computeCorners(par->line_points, par->pt2);
    draw_crossline(par->img, par->line_points, points[0], points[1]);
    //     par->img = par->imgTmp.clone();
    final_points[0] = par->line_points[0];
    final_points[1] = par->line_points[1];
    final_points[2] = points[0];
    final_points[3] = points[1];
    par->mouseLflag = false;
    cout << pt << endl;
    //     par->line_points.clear();
    //     par->img = par->imgBackup.clone();
  }
  else if (event == CV_EVENT_MOUSEMOVE)
  {
    par->pt1 = pt;
  }
  else if (event == CV_EVENT_MOUSEWHEEL)
  {
    value = getMouseWheelDelta(flags);
    //     cout <<"value: " << value <<endl;
    if (value > 0)
      par->scale += step;
    else if (value < 0)
      par->scale -= step;
    par->scale = max((float)0.1, par->scale);
    //     par->scale = min((float)1.0, par->scale);
    cout << par->pt1 << endl;
    //     zoomInAndOut(par->scale, par->imgZoomBackup, par->img);
    //     zoom(par->img, par->imgZoomBackup, par->pt1, par->scale);
  }
};

class MapsArea
{
#define IMGNAME "test"
#define display_ratio 1
  public:
  vector<tuple<bool, string, vector<Point2f>>> img_infos_;
  MapsArea(const std::string &path1)
  {
    vector<Mat> ori_imgs;
    Mat img = (imread(path1 + "/map.png", CV_LOAD_IMAGE_UNCHANGED));
    Mat img_bgr;
    if (img.type() == CV_8UC1) cvtColor(img, img_bgr, CV_GRAY2BGR);
    MouseParam mouseParam;

    mouseParam.img = img_bgr.clone();
    mouseParam.imgBackup = img_bgr.clone();
    mouseParam.imgZoomBackup = img_bgr.clone();
    mouseParam.mouseLflag = false;
    float step = 0.05;
    mouseParam.scale = 1.0;
    namedWindow(IMGNAME, 0);

    setMouseCallback(IMGNAME, on_mouse, &mouseParam);
    int key;
    int save_cnt = 0;
    //     vector<vector<cv::Point2f>> save_points;
    vector<cv::Point2f> save_points;
    while (1)
    {
      mouseParam.imgTmp = mouseParam.img.clone();

      if (!save_points.empty()) draw_save_points(mouseParam.imgTmp, save_points);
      //     draw_crossline(mouseParam.imgTmp, mouseParam.pt1);
      if (mouseParam.mouseLflag == true && mouseParam.line_points.size() == 2)
      {
        if (mouseParam.pt1 != mouseParam.pt2)
        {
          auto points = computeCorners(mouseParam.line_points, mouseParam.pt2);
          draw_crossline(mouseParam.imgTmp, mouseParam.line_points, points[0], points[1]);
        }
      }
      imshow(IMGNAME, mouseParam.imgTmp);
      key = waitKey(40);
      if (key == 27)
      {
        break;
      }
      else if (key == toascii('s'))
      {
        cout << save_points.size() << ": ";
        for (int i = 0; i < 4; i++)
        {
          save_points.push_back(final_points[i]);
          cout << final_points[i] << " ";
        }
        cout << endl;
        destroyWindow(IMGNAME);
        break;
      }
    }
    // image points to map points
    {
      double max[2];
      vector<Point2f> point_in_map_tmp;
      {
        boost::property_tree::ptree pt;
        string path = path1;
        int is_right_img = 0;
        double resolution = 0.05;
        bool is_3d = false;
        if (judgeFileExist(path + "/show_map_data.xml"))
        {
          is_3d = true;
          boost::property_tree::read_xml(path + "/show_map_data.xml", pt);
          string data = pt.get<std::string>("max_box");
          sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
          resolution = pt.get<double>("resolution");
        }
        else
        {
          boost::property_tree::read_xml(path + "/frames/0/data.xml", pt);
          string data = pt.get<std::string>("probability_grid.max");
          sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
          resolution = pt.get<double>("probability_grid.resolution");
        }
        cout << "max: " << max[0] << ", " << max[1] << endl;
        point_in_map_tmp.resize(4);
        for (int j = 0; j < 4; j++)
        {
          point_in_map_tmp[j].x = max[0] - save_points[j].y * resolution;
          point_in_map_tmp[j].y = max[1] - save_points[j].x * resolution;
        }
        img_infos_.push_back(make_tuple(is_3d, path, point_in_map_tmp));
      }
    }
  }
  MapsArea(const std::string &path1, const std::string &path2)
  {
    vector<Mat> ori_imgs;
    //     Mat img1 = imread(path1 + "/map.png", CV_LOAD_IMAGE_UNCHANGED);
    //     Mat img2 = imread(path2 + "/map.png", CV_LOAD_IMAGE_UNCHANGED);
    ori_imgs.push_back(imread(path1 + "/map.png", CV_LOAD_IMAGE_UNCHANGED));
    ori_imgs.push_back(imread(path2 + "/map.png", CV_LOAD_IMAGE_UNCHANGED));
    vector<int> max_img_size;
    max_img_size.resize(ori_imgs.size());
    int min_size = 1e9;
    for (int i = 0; i < ori_imgs.size(); i++)
    {
      max_img_size[i] = max(ori_imgs[i].cols, ori_imgs[i].rows);
      min_size = min(min_size, max_img_size[i]);
      cout << max_img_size[i] << " " << min_size << endl;
    }
    int new_min_size = min_size * 1.0 * display_ratio;
    cout << "new_min_size: " << new_min_size << endl;
    vector<float> ratios;
    cv::Mat rImg1, rImg2;
    vector<Mat> rImgs;
    rImgs.resize(ori_imgs.size());
    Mat img(new_min_size, new_min_size * 2, CV_8UC1, cv::Scalar(255));
    for (int i = 0; i < ori_imgs.size(); i++)
    {
      ratios.push_back(new_min_size * 1.0 / max_img_size[i]);
      cout << "img ratio: " << ratios[i] << endl;
      cv::resize(ori_imgs[i], rImgs[i],
                 cv::Size(ori_imgs[i].cols * ratios[i], ori_imgs[i].rows * ratios[i]));
      if (rImgs[i].type() != CV_8UC1) cvtColor(rImgs[i], rImgs[i], CV_BGR2GRAY);
      Rect rect(i * new_min_size, 0, rImgs[i].cols, rImgs[i].rows);
      rImgs[i].copyTo(img(rect));
    }

    //     imshow("tmp", img);
    //     waitKey(0);
    Mat img_bgr;
    if (img.type() == CV_8UC1) cvtColor(img, img_bgr, CV_GRAY2BGR);
    MouseParam mouseParam;

    mouseParam.img = img_bgr.clone();
    mouseParam.imgBackup = img_bgr.clone();
    mouseParam.imgZoomBackup = img_bgr.clone();
    mouseParam.mouseLflag = false;
    float step = 0.05;
    mouseParam.scale = 1.0;
    namedWindow(IMGNAME, 0);

    setMouseCallback(IMGNAME, on_mouse, &mouseParam);
    int key;
    int save_cnt = 0;
    vector<vector<cv::Point2f>> save_points;
    while (1)
    {
      mouseParam.imgTmp = mouseParam.img.clone();

      if (!save_points.empty()) draw_save_points(mouseParam.imgTmp, save_points[0]);
      //     draw_crossline(mouseParam.imgTmp, mouseParam.pt1);
      if (mouseParam.mouseLflag == true && mouseParam.line_points.size() == 2)
      {
        if (mouseParam.pt1 != mouseParam.pt2)
        {
          auto points = computeCorners(mouseParam.line_points, mouseParam.pt2);
          draw_crossline(mouseParam.imgTmp, mouseParam.line_points, points[0], points[1]);
        }
      }
      imshow(IMGNAME, mouseParam.imgTmp);
      key = waitKey(40);
      if (key == 27)
      {
        break;
      }
      else if (key == toascii('s'))
      {
        //         vector<cv::Point2f> points;
        //         points.push_back({564, 516});
        //         points.push_back({679, 514});
        //         points.push_back({682.974, 742.496});
        //         points.push_back({567.974, 744.496});
        //         save_points.push_back(points);
        //         points.clear();
        //         points.push_back({1881, 399});
        //         points.push_back({1946, 560});
        //         points.push_back({1560.02, 715.831});
        //         points.push_back({1495.02, 554.831});
        //         save_points.push_back(points);
        //         destroyWindow(IMGNAME);
        //         break;
        ++save_cnt;
        vector<cv::Point2f> points;
        cout << save_points.size() << ": ";
        for (int i = 0; i < 4; i++)
        {
          points.push_back(final_points[i]);
          cout << final_points[i] << " ";
        }
        cout << endl;
        save_points.push_back(points);
        if (save_cnt == 2)
        {
          destroyWindow(IMGNAME);
          break;
        }
      }
    }

    // image points to map points

    for (int i = 0; i < save_points.size(); i++)
    {
      double max[2];
      vector<Point2f> point_in_map_tmp;
      {
        boost::property_tree::ptree pt;
        string path = path1;
        int is_right_img = 0;
        float tmp_ratio = ratios[0];
        if (save_points[i][0].x > new_min_size)
        {
          path = path2;
          is_right_img = 1;
          tmp_ratio = ratios[1];
        }
        double resolution = 0.05;
        bool is_3d = false;
        if (judgeFileExist(path + "/show_map_data.xml"))
        {
          is_3d = true;
          boost::property_tree::read_xml(path + "/show_map_data.xml", pt);
          string data = pt.get<std::string>("max_box");
          sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
          resolution = pt.get<double>("resolution");
        }
        else
        {
          boost::property_tree::read_xml(path + "/frames/0/data.xml", pt);
          string data = pt.get<std::string>("probability_grid.max");
          sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
          resolution = pt.get<double>("probability_grid.resolution");
        }
        cout << "max: " << max[0] << ", " << max[1] << endl;
        point_in_map_tmp.resize(4);
        for (int j = 0; j < 4; j++)
        {
          cout << new_min_size << ", " << save_points[i][j].x << ", " << save_points[i][j].y
               << endl;
          if (is_right_img) save_points[i][j].x = save_points[i][j].x - new_min_size;
          cout << new_min_size << ", " << save_points[i][j].x << ", " << save_points[i][j].y
               << endl;
          point_in_map_tmp[j].x = max[0] - save_points[i][j].y * resolution / tmp_ratio;
          point_in_map_tmp[j].y = max[1] - save_points[i][j].x * resolution / tmp_ratio;
          //           geometry_msgs::Point32 p;
          //           p.x = point_in_map_tmp[j].x;
          //           p.y = point_in_map_tmp[j].y;
          //           p.z = 0;
          //           points_msg.points.push_back(p);
        }
        img_infos_.push_back(make_tuple(is_3d, path, point_in_map_tmp));
      }
    }
  }

  vector<tuple<bool, string, vector<Point2f>>> ImgInfos() { return img_infos_; };

  private:
  void draw_rectangle(Mat &img, const Point &pt1, const Point &pt2)
  {
    // rectangle函数参数： 图片， 左上角， 右下角， 颜色， 线条粗细，
    // 线条类型，点类型
    rectangle(img, pt1, pt2, Scalar(0, 255, 0), 1, 0, 0);
  }

  void zoom(Mat &img, const Mat &srcimg, const Point &pt, const float scale)
  {
    int x1, y1, x2, y2;
    int width, height;
    width = (int)(srcimg.cols * scale / 2.0);
    height = (int)(srcimg.rows * scale / 2.0);
    cout << width << ", " << height << endl;
    x1 = max(pt.x - width, 0);
    y1 = max(pt.y - height, 0);
    x2 = min(pt.x + width, srcimg.cols);
    y2 = min(pt.y + height, srcimg.rows);
    cout << x1 << ", " << y1 << ", " << x2 << ", " << y2 << endl;
    Rect zoomRect(Point(x1, y1), Point(x2, y2));
    Mat tmp_img;

    cv::resize(srcimg, tmp_img, Size(srcimg.cols * scale, srcimg.rows * scale));
    //   img = tmp_img;
    //   imshow("tmp" , tmp_img);
  }

  void zoomInAndOut(const float scale, const Mat srcImg, Mat &dstImg)
  {
    Mat M = Mat::eye(3, 3, CV_32FC1);
    int imgHeight = srcImg.rows;
    int imgWidth = srcImg.cols;

    uchar *pSrcData = (uchar *)srcImg.data;
    uchar *pDstData = (uchar *)dstImg.data;

    Point2f center(imgWidth / 2.0, imgHeight / 2.0);
    //计算仿射矩阵
    M.at<float>(0, 0) = scale;
    M.at<float>(0, 2) = (1 - scale) * center.x;
    M.at<float>(1, 1) = scale;
    M.at<float>(1, 2) = (1 - scale) * center.y;

    float a11 = M.at<float>(0, 0);
    float a12 = M.at<float>(0, 1);
    float a13 = M.at<float>(0, 2);
    float a21 = M.at<float>(1, 0);
    float a22 = M.at<float>(1, 1);
    float a23 = M.at<float>(1, 2);
    float a31 = M.at<float>(2, 0);
    float a32 = M.at<float>(2, 1);
    float a33 = M.at<float>(2, 2);

    float bx = a11 * a22 - a21 * a12;
    float by = a12 * a21 - a11 * a22;
    if (abs(bx) > 1e-3 && abs(by) > 1e-3)
    {
      bx = 1.0 / bx;
      by = 1.0 / by;
      float cx = a13 * a22 - a23 * a12;
      float cy = a13 * a21 - a23 * a11;

      for (int j = 0; j < imgHeight; j++)
      {
        for (int i = 0; i < imgWidth; i++)
        {
          float u = (a22 * i - a12 * j - cx) * bx;
          float v = (a21 * i - a11 * j - cy) * by;

          int u0 = floor(u);
          int v0 = floor(v);
          int u1 = floor(u0 + 1);
          int v1 = floor(v0 + 1);
          if (u0 >= 0 && v0 >= 0 && u1 < imgWidth && v1 < imgHeight)
          {
            float dx = u - u0;
            float dy = v - v0;
            float weight1 = (1 - dx) * (1 - dy);
            float weight2 = dx * (1 - dy);
            float weight3 = (1 - dx) * dy;
            float weight4 = dx * dy;

            for (int k = 0; k < srcImg.channels(); k++)
            {
              pDstData[j * imgWidth * 3 + i * 3 + k] =
                  weight1 * pSrcData[v0 * imgWidth * 3 + u0 * 3 + k] +
                  weight2 * pSrcData[v0 * imgWidth * 3 + u1 * 3 + k] +
                  weight3 * pSrcData[v1 * imgWidth * 3 + u0 * 3 + k] +
                  weight4 * pSrcData[v1 * imgWidth * 3 + u1 * 3 + k];
            }
          }
          else
          {
            for (int k = 0; k < srcImg.channels(); k++)
            {
              pDstData[j * imgWidth * 3 + i * 3 + k] = 0;
            }
          }
        }
      }
    }
  }
};
} // namespace ImageOpreator

#endif