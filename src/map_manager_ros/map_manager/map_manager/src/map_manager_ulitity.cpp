#include "map_manager_ulitity.h"

#include <glog/logging.h>
#include <pcl/search/kdtree.h>
#include <ros/ros.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "cartographer/io/internal/mapping_state_serialization.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
using namespace std;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager
{

void readPosesFromPbstream(const std::string &path, PbstreamInfos &infos)
{
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);
  proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();

  LOG(INFO) << "Insert node poses..";
  std::map<NodeId, transform::Rigid3d> origin_node_poses;
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      infos.node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                              transform::ToRigid3(node_proto.pose()));
    }
  }
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Submap &submap_proto : trajectory_proto.submap())
    {
      infos.submap_poses.Insert(
          SubmapId{trajectory_proto.trajectory_id(), submap_proto.submap_index()},
          transform::ToRigid3(submap_proto.pose()));
    }
  }

  for (const auto &landmark : pose_graph_proto.landmark_poses())
  {
    infos.landmark_poses[landmark.landmark_id()] = transform::ToRigid3(landmark.global_pose());
  }

  cartographer::mapping::proto::SerializedData proto;
  LOG(INFO) << "deserializer pbstream..";
  std::vector<sensor::ImuData> imu_datas;
  std::vector<sensor::OdometryData> odom_datas;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kPoseGraph:
      LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
                    "stream likely corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kAllTrajectoryBuilderOptions:
      LOG(ERROR) << "Found multiple serialized "
                    "`AllTrajectoryBuilderOptions`. Serialized stream likely "
                    "corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kSubmap:
    {
      infos.submap_id_to_submap.Insert(SubmapId{proto.submap().submap_id().trajectory_id(),
                                                proto.submap().submap_id().submap_index()},
                                       proto.submap());
      break;
    }
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      const NodeId node_id(proto.node().node_id().trajectory_id(),
                           proto.node().node_id().node_index());
      infos.node_datas.Insert(node_id, FromProto(proto.node().node_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kImuData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kOdometryData:
    {
      infos.odom_datas.push_back(sensor::FromProto(proto.odometry_data().odometry_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kFixedFramePoseData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kLandmarkData:
    {
      infos.landmark_datas.push_back(sensor::FromProto(proto.landmark_data().landmark_data()));
      break;
    }
    default:
      break;
    }
  }

  infos.constraints = FromProto(pose_graph_proto.constraint());
}

bool judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    LOG(WARNING) << "\033[33m" << file_name << " is not existed!\033[37m";
    existed_flag = false;
  }
  return existed_flag;
}

cv::Mat image16ToImage8(const cv::Mat &image16)
{
  cv::Mat image8(image16.rows, image16.cols, CV_8UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      int value = image16.at<uint16_t>(i, j);
      if (value == 0)
        image8.at<uchar>(i, j) = 128;
      else
        image8.at<uchar>(i, j) = value / 128;
    }

  return image8;
}

cv::Mat image8ToImage16(const cv::Mat &image8)
{
  cv::Mat image16(image8.rows, image8.cols, CV_16UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      const unsigned char color = image8.at<uchar>(i, j);
      if (color == 128)
        image16.at<uint16_t>(i, j) = 0;
      else
        image16.at<uint16_t>(i, j) = color * 128;
    }
  return image16;
}

void checkSystemStatus(const std::string &cmdstring, const int &status)
{
  if (status < 0)
  {
    // 这里务必要把errno信息输出或记入Log
    LOG(ERROR) << "cmd:" << cmdstring << "\n error:" << strerror(errno);
    return;
  }

  if (WIFEXITED(status))
  {
    //取得cmdstring执行结果
    LOG(INFO) << "normal termination, exit status = " << WEXITSTATUS(status);
  }
  else if (WIFSIGNALED(status))
  {
    //如果cmdstring被信号中断，取得信号值
    LOG(ERROR) << "abnormal termination,signal number = " << WTERMSIG(status);
  }
  else if (WIFSTOPPED(status))
  {
    //如果cmdstring被信号暂停执行，取得信号值
    LOG(ERROR) << "process stopped, signal number = " << WSTOPSIG(status);
  }
}
void image2worldLoc(const Eigen::Vector2d &max, const double &resolution,
                    const Eigen::Vector2i &imageLoc, Eigen::Vector2d &worldLoc)
{
  worldLoc[0] = max(0) - imageLoc[1] * resolution;
  worldLoc[1] = max(1) - imageLoc[0] * resolution;
}

void world2imageLoc(const Eigen::Vector2d &max, const double &resolution,
                    const Eigen::Vector2d &worldLoc, Eigen::Vector2i &imageLoc)
{
  imageLoc[0] = (max(1) - worldLoc(1)) / resolution;
  imageLoc[1] = (max(0) - worldLoc(0)) / resolution;
}

void autoSetIgnoreArea(const std::string &path)
{
  // auto set ignore area
  cv::Mat img = cv::imread(path + "map.png", CV_LOAD_IMAGE_UNCHANGED);
  cv::Mat gray_img;
  if (img.type() != CV_8UC1)
    cv::cvtColor(img, gray_img, CV_BGR2GRAY);
  else
    gray_img = img;
  cv::Mat new_bool_image(gray_img.rows, gray_img.cols, CV_8UC1, cv::Scalar(255));
  cv::Mat binary_img;
  cv::threshold(gray_img, binary_img, 100, 255, CV_THRESH_BINARY);
  cv::Mat dstImage;
  cv::distanceTransform(binary_img, dstImage, CV_DIST_L2, CV_DIST_MASK_PRECISE);
  cv::Mat output(dstImage.rows, dstImage.cols, CV_8UC1, cv::Scalar(0));
  int distance_threshold = 3;
  ::ros::param::get("/map_manager_node/distance_threshold", distance_threshold);
  for (int i = 0; i < dstImage.rows; i++)
    for (int j = 0; j < dstImage.cols; j++)
    {
      // if(dstImage.at<uchar>(i,j) != 0)
      if (dstImage.at<float>(i, j) > distance_threshold)
      {
        new_bool_image.at<uchar>(i, j) = 0;
      }
    }

  cv::imwrite(path + "bool_image_auto.png", new_bool_image);
}

void updateBoolImage(const std::string &origin_path, const std::string &path)
{
  if (!judgeFileExist(origin_path + "/corner_points.xml"))
  {
    LOG(WARNING) << "no " + origin_path + "/corner_points.xml file!";
    return;
  }
  std::string system_command =
      "cp " + origin_path + "/corner_points.xml " + path + "corner_points.xml ";
  LOG(INFO) << system_command;
  int status = system(system_command.c_str());
  checkSystemStatus(system_command, status);
  CHECK(status != -1);

  double max_of_new_img[2];
  int size_of_new_img[2];
  double resolution_of_new_img;
  {
    boost::property_tree::ptree pt1;
    LOG(INFO) << path + "frames/0/data.xml";
    boost::property_tree::read_xml(path + "frames/0/data.xml", pt1);
    std::string data;

    data = pt1.get<std::string>("probability_grid.max");
    sscanf(data.c_str(), "%lf %lf", &max_of_new_img[0], &max_of_new_img[1]);
    data = pt1.get<std::string>("probability_grid.num_cells");
    sscanf(data.c_str(), "%d %d", &size_of_new_img[0], &size_of_new_img[1]);
    resolution_of_new_img = pt1.get<double>("probability_grid.resolution");
    LOG(INFO) << path + "init new bool image";
    //     new_bool_image =  Mat(Size(size_of_new_img[0],
    //     size_of_new_img[1]),CV_8UC1,Scalar(255));
    LOG(INFO) << size_of_new_img[0] << ", " << size_of_new_img[1];
  }

  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(origin_path + "/corner_points.xml", pt);
  std::string xml;
  if (pt.empty())
  {
    LOG(WARNING) << "corner_points.xml is empty!";
    return;
  }
  cv::Mat mask = cv::imread(path + "bool_image_auto.png", CV_LOAD_IMAGE_UNCHANGED);
  for (auto &mark : pt)
  {
    if (mark.first == "corner_points")
    {
      int points_size = std::stoi(mark.second.get("points_size", ""));
      CHECK(points_size == 4);
      xml = mark.second.get("points", " ");
      float value[8];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6], &value[7]);
      // TODO new msg will be here
      std::vector<cv::Point> contour;
      for (int i = 0; i < points_size; i++)
      {
        Eigen::Vector2d point_in_map(value[i * 2], value[i * 2 + 1]);
        Eigen::Vector2i imageLoc;
        world2imageLoc(Eigen::Vector2d(max_of_new_img[0], max_of_new_img[1]), resolution_of_new_img,
                       point_in_map, imageLoc);
        cv::Point p(imageLoc(0), imageLoc(1));
        contour.push_back(p);
      }
      std::vector<std::vector<cv::Point>> contours;
      contours.push_back(contour);
      cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
    }
  }
  cv::imwrite(path + "bool_image.png", mask);
  LOG(INFO) << "update bool_image.png done!";
}

void updateModifyArea(const cv::Mat &modify_area_img, const std::string &path)
{
  std::string img_full_path = path + "frames/0/probability_grid_origin.png";
  std::string img_update_path = path + "frames/0/probability_grid.png";
  cv::Mat org = cv::imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
  for (int i = 0; i < modify_area_img.rows; i++)
    for (int j = 0; j < modify_area_img.cols; j++)
    {
      if (modify_area_img.at<uint8_t>(i, j) == 127 || modify_area_img.at<uint8_t>(i, j) == 128)
      {
        continue;
      }
      else if (modify_area_img.at<uint8_t>(i, j) == 255)
        org.at<uint16_t>(i, j) = 32765;
      else
      {
        if (modify_area_img.at<uint8_t>(i, j) == 0)
          org.at<uint16_t>(i, j) = 1;
        else
          org.at<uint16_t>(i, j) = 128 * modify_area_img.at<uint8_t>(i, j);
      }
    }
  cv::imwrite(path + "modify_area.png", modify_area_img);
  LOG(INFO) << "imwrite modify area.png done!";
  cv::imwrite(img_update_path, org);
  LOG(INFO) << "update probability_grid.png done!";
  cv::Mat img_8 = image16ToImage8(org);
  cv::imwrite(path + "map.png", img_8);
  LOG(INFO) << "update map.png done!";
}

void updateModifyMask(const std::string &origin_path, const std::string &path)
{
  //   std::string origin_path;
  //   if(!::ros::param::get("/vtr/origin_path",origin_path))
  //   {
  //     LOG(ERROR) << "Can not get origin path!";
  //     return;
  //   }
  if (!judgeFileExist(origin_path + "/modify_area.png")) return;
  cv::Mat org1 = cv::imread(origin_path + "/modify_area.png", CV_LOAD_IMAGE_UNCHANGED);
  cv::Mat modify_area_img;
  if (org1.type() != CV_8UC1)
    cv::cvtColor(org1, modify_area_img, CV_BGR2GRAY);
  else
    modify_area_img = org1.clone();
  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(origin_path + "/map_data.xml", pt);
  std::string data = "";
  float map_resolution = pt.get<float>("mapPng.resolution");
  data = pt.get<std::string>("mapPng.pose");
  float pose[7];
  sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3], &pose[4],
         &pose[5], &pose[6]);
  float Max_box[2] = {pose[1], pose[0]};
  data.clear();

  if (!judgeFileExist(path + "map.png")) return;
  cv::Mat map_img = cv::imread(path + "map.png", CV_LOAD_IMAGE_UNCHANGED);
  cv::Mat new_modify_area_img(map_img.rows, map_img.cols, CV_8UC1, cv::Scalar(127));
  boost::property_tree::ptree new_pt;
  boost::property_tree::read_xml(path + "map_data.xml", new_pt);
  float new_resolution = new_pt.get<float>("mapPng.resolution");
  data = new_pt.get<std::string>("mapPng.pose");
  float tmp_pose[7];
  sscanf(data.c_str(), "%f %f %f %f %f %f %f", &tmp_pose[0], &tmp_pose[1], &tmp_pose[2],
         &tmp_pose[3], &tmp_pose[4], &tmp_pose[5], &tmp_pose[6]);
  float new_max_box[2] = {tmp_pose[1], tmp_pose[0]};

  for (int i = 0; i < modify_area_img.rows; i++)
  {
    for (int j = 0; j < modify_area_img.cols; j++)
    {
      if (modify_area_img.at<uint8_t>(i, j) == 127) continue;
      float x, y;
      x = (Max_box[0] - i) * map_resolution;
      y = (Max_box[1] - j) * map_resolution;
      int img_x, img_y;
      img_x = new_max_box[0] - x / new_resolution;
      img_y = new_max_box[1] - y / new_resolution;
      if (img_x >= 0 && img_y >= 0 && img_x < new_modify_area_img.rows &&
          img_y < new_modify_area_img.cols)
      {
        new_modify_area_img.at<uint8_t>(img_x, img_y) = modify_area_img.at<uint8_t>(i, j);
      }
    }
  }

  updateModifyArea(new_modify_area_img, path);
}

std::vector<LandmarkInfo> loadLandmarksFromXmlFile(const std::string &path)
{
  std::vector<LandmarkInfo> landmarks;
  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(path + "landmark.xml", pt,
                                             boost::property_tree::xml_parser::trim_whitespace);
  if (pt.empty())
  {
    LOG(WARNING) << "landmark.xml is empty!";
    return landmarks;
  }
  std::string xml, xml_id;
  std::string ns;
  //   bool rewrite = false;
  for (auto &mark : pt)
  {
    if (mark.first == "landmark")
    {
      LandmarkInfo mark_info;
      int visible = std::stoi(mark.second.get("visible", ""));
      ns = mark.second.get("ns", "");
      xml_id = mark.second.get("id", " ");
      mark_info.id = xml_id;
      mark_info.ns = ns;
      mark_info.visible = visible;
      xml = mark.second.get("transform", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      mark_info.pose = cartographer::transform::Rigid3d({value[0], value[1], value[2]},
                                                        {value[6], value[3], value[4], value[5]});
      std::string pole_radius = mark.second.get("pole_radius", " ");
      if (pole_radius == "") pole_radius = "-1";
      mark_info.pole_radius = std::stof(pole_radius);
      std::string translation_in_image = mark.second.get("translation_in_image", "");
      if (translation_in_image == "")
      {
        translation_in_image = "0 0";
      }
      std::sscanf(translation_in_image.c_str(), "%f %f", &value[0], &value[1]);
      mark_info.translation_in_image(0) = value[0];
      mark_info.translation_in_image(1) = value[1];
    }
  }
  return landmarks;
}
void bash(std::string cmd)
{
  LOG(INFO) << cmd;
  auto status = system(cmd.c_str());
  checkSystemStatus(cmd, status);
  assert(status != -1);
}
string adjustMapPixelSingle(string pngDirPath, int size, const bool is_saved_map)
{
  auto png_filepath = pngDirPath;
  std::vector<int> params;
  params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  params.push_back(0);

  std::string origin_path = png_filepath + "/modified_images/erode_0_pixels/";
  cv::Mat tmp_mat = cv::imread(origin_path + "/probability_grid.png", CV_LOAD_IMAGE_UNCHANGED);
  if (tmp_mat.data == nullptr)
  {
    bash("mkdir -p " + origin_path);
    bash("cp " + png_filepath + "/probability_grid.png" + " " + origin_path +
         "/probability_grid_16bit.png");
    cv::Mat tmp_mat16 =
        cv::imread(origin_path + "/probability_grid_16bit.png", CV_LOAD_IMAGE_UNCHANGED);
    cv::Mat map_adjusted = image16ToImage8(tmp_mat16);
    cv::imwrite(origin_path + "probability_grid_8bit.png", map_adjusted, params);
  }
  if (size == 0)
  {
    bash("cp " + origin_path + "/probability_grid_16bit.png" + " " + png_filepath +
         "/probability_grid.png");
    return "";
  }
  cv::Mat cellMat =
      cv::imread(origin_path + "/probability_grid_16bit.png", CV_LOAD_IMAGE_UNCHANGED);
  if (cellMat.data == nullptr)
  {
    return "Load " + origin_path + "/probability_grid_16bit.png failed!";
  }

  cv::Mat org1 = cellMat.clone();
  std::vector<cv::Point> zero_points;
  for (int i = 0; i < org1.rows; i++)
    for (int j = 0; j < org1.cols; j++)
    {
      int value = org1.at<uint16_t>(i, j);
      if (value == 0)
      {
        zero_points.push_back(cv::Point(i, j));
        org1.at<uint16_t>(i, j) = 255 * 128;
      }
    }
  cv::Mat result(cellMat.cols, cellMat.rows, CV_16UC1);

  cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(size, size));
  cv::erode(org1, result, kernel);
  cv::GaussianBlur(result, result, cv::Size(size, size), 1);

  for (auto &pt : zero_points)
    result.at<uint16_t>(pt.x, pt.y) = 0;

  if (is_saved_map)
  {
    std::string tmp_path =
        png_filepath + "/modified_images/erode_" + std::to_string(size) + "_pixels/";
    bash("mkdir -p " + tmp_path);
    cv::imwrite(tmp_path + "probability_grid_16bit.png", result, params);

    cv::Mat map_adjusted = image16ToImage8(result);
    cv::imwrite(tmp_path + "probability_grid_8bit.png", map_adjusted, params);
  }
  else
    cv::imwrite(png_filepath + "/probability_grid.png", result, params);
  return "";
}

std::string adjustMapPixel(const std::string &file_path, const int &size, const bool is_saved_map)
{
  if (size != 3 && size != 5 && size != 0)
  {
    return "Nothing will be done! Because flag is not  0, 3 or 5 !";
  }

  boost::property_tree::ptree pt;
  try
  {
    boost::property_tree::read_xml(file_path + "map.xml", pt,
                                   boost::property_tree::xml_parser::trim_whitespace);
  }
  catch (std::exception &e)
  {
  }
  if (pt.empty())
  {
    const std::string error_string = "Load " + file_path + "map.xml failed!";
    ROS_FATAL("%s", error_string.c_str());
    return error_string;
  }

  int node_count = pt.get<int>("map.property.node_count");
  std::string submap_filepath = file_path + "frames/";

  for (int i = 0; i < node_count; i++)
  {
    auto err = adjustMapPixelSingle(submap_filepath + std::to_string(i), size, is_saved_map);
    if (!err.empty()) return err;
  }
  if (is_saved_map) return "";
  pt.put("map.property.pixel_size", size);
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(file_path + "map.xml", pt, std::locale(), setting);
  //   nh_.setParam("mapPixelSize", size);
  // //   if (!is_mapping)
  // //   {
  // //     createMapMergerFromDisk(file_path);
  // //     compressedMapImage_ = createCompressedMapImage(map_merger_);
  // //   }
  return "";
}

bool dbscan(const pcl::PointCloud<pcl::PointXY>::Ptr &cloud_in,
            std::vector<std::vector<int>> &clusters_index, const double &r, const int &size)
{
  if (!cloud_in->size()) return false;
  pcl::KdTreeFLANN<pcl::PointXY> tree;
  tree.setInputCloud(cloud_in);
  std::vector<bool> cloud_processed(cloud_in->size(), false);

  for (size_t i = 0; i < cloud_in->points.size(); ++i)
  {
    if (cloud_processed[i])
    {
      continue;
    }

    std::vector<int> seed_queue;
    //检查近邻数是否大于给定的size（判断是否是核心对象）
    std::vector<int> indices_cloud;
    std::vector<float> dists_cloud;
    if (tree.radiusSearch(cloud_in->points[i], r, indices_cloud, dists_cloud) >= size)
    {
      seed_queue.push_back(i);
      cloud_processed[i] = true;
    }
    else
    {
      // cloud_processed[i] = true;
      continue;
    }

    int seed_index = 0;
    while (seed_index < seed_queue.size())
    {
      std::vector<int> indices;
      std::vector<float> dists;
      if (tree.radiusSearch(cloud_in->points[seed_queue[seed_index]], r, indices, dists) <
          size) //函数返回值为近邻数量
      {
        // cloud_processed[i] =
        // true;//不满足<size可能是边界点，也可能是簇的一部分，不能标记为已处理
        ++seed_index;
        continue;
      }
      for (size_t j = 0; j < indices.size(); ++j)
      {
        if (cloud_processed[indices[j]])
        {
          continue;
        }
        seed_queue.push_back(indices[j]);
        cloud_processed[indices[j]] = true;
      }
      ++seed_index;
    }
    clusters_index.push_back(seed_queue);
  }
  // std::cout << clusters_index.size() << std::endl;

  if (clusters_index.size())
    return true;
  else
    return false;
}

cv::Mat grey16ToRgba(const cv::Mat& image)
{
	cv::Mat map_adjusted = cv::Mat::zeros(image.rows, image.cols, CV_8UC4);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
			int value = image.at<uint16_t>(i, j);
			if (value == 0)
			{
				map_adjusted.at<cv::Vec4b>(i, j)[0] = 128;
				map_adjusted.at<cv::Vec4b>(i, j)[1] = 128;
				map_adjusted.at<cv::Vec4b>(i, j)[2] = 128;
				map_adjusted.at<cv::Vec4b>(i, j)[3] = 0;
			}
			else
			{
// 				map_adjusted.at<uchar>(i, j) = value / 128;
				map_adjusted.at<cv::Vec4b>(i, j)[0] = value / 128.;
				map_adjusted.at<cv::Vec4b>(i, j)[1] = value / 128.;
				map_adjusted.at<cv::Vec4b>(i, j)[2] = value / 128.;

				const int delta =
        128 -value/128;
// 				const int delta = 128 - int(value / 256);
				if(delta < 0)
				{
					map_adjusted.at<cv::Vec4b>(i, j)[0] = 255;
					map_adjusted.at<cv::Vec4b>(i, j)[1] = 255;
					map_adjusted.at<cv::Vec4b>(i, j)[2] = 255;
				}
				const double alpha = delta > 0 ? delta : -delta;
// 				const unsigned char value1 = delta > 0 ? delta : 0;
// 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
				map_adjusted.at<cv::Vec4b>(i, j)[3] = 255* sqrt(sqrt(alpha/128.));
			}
    }
  }
  return map_adjusted;
}

void loadOldSubmap(const std::string& submap_filepath, cartographer_ros_msgs::SubmapImageEntry &submap_img,
std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> &localInGlobal_poses_,
std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> &posesInGlobal_,
std::vector<double> &maxes_,int size_of_map_[2])
{
  LOG(INFO) << submap_filepath.c_str() << " loading...";
  Eigen::Isometry3d pose_in_global, poseInLocal, local2global;
  double resolution, max[2];
  uint num_x_cells, num_y_cells;

  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(submap_filepath + "/data.xml", pt);
  
  if (pt.empty())
  {
    ROS_FATAL("Load %s/data.xml failed!", submap_filepath.c_str());
    return;
  }
  
  std::string data;
  float pose[7];
  Eigen::Quaternionf q1,q2;
  Eigen::Matrix3f rotMat;

	int id = pt.get<int>("id");
	submap_img.submap_index = id;
  data = pt.get<std::string>("pose");
  sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2],
      &pose[3], &pose[4], &pose[5], &pose[6]);
  q1 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);
  rotMat = q1.matrix();
  pose_in_global.setIdentity();
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      pose_in_global(i, j) = rotMat(i, j);
  pose_in_global(0, 3) = pose[0];
  pose_in_global(1, 3) = pose[1];
  pose_in_global(2, 3) = pose[2];

	posesInGlobal_.push_back(pose_in_global);
  data = pt.get<std::string>("local_pose");
  sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2],
      &pose[3], &pose[4], &pose[5], &pose[6]);
  q2 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);
  rotMat = q2.matrix();
  poseInLocal.setIdentity();
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      poseInLocal(i, j) = rotMat(i, j);
  poseInLocal(0, 3) = pose[0];
  poseInLocal(1, 3) = pose[1];
  poseInLocal(2, 3) = pose[2];

  resolution = pt.get<double>("probability_grid.resolution");
	submap_img.resolution = resolution;
  data = pt.get<std::string>("probability_grid.max");
  sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
  data = pt.get<std::string>("probability_grid.num_cells");
  sscanf(data.c_str(), "%d %d", &num_x_cells, &num_y_cells);
  
  size_of_map_[0] = num_x_cells;
  size_of_map_[1] = num_y_cells;
  
  local2global = pose_in_global * poseInLocal.inverse();
	localInGlobal_poses_.push_back(local2global);
	maxes_.push_back(max[0]);
	maxes_.push_back(max[1]);
	
	Eigen::Vector3d pointInLocal, pointInGlobal;
	pointInLocal[0] = max[0];
	pointInLocal[1] = max[1];
	pointInLocal[2] = 0;
	
	pointInGlobal = local2global * pointInLocal;
	LOG(INFO) << "locate id: " <<id << "  "<< pointInGlobal;
	submap_img.pose_in_map.position.x = pointInGlobal(0);
	submap_img.pose_in_map.position.y = pointInGlobal(1);
	submap_img.pose_in_map.position.z = pointInGlobal(2);
	Eigen::Quaternionf q = q1*q2.conjugate();
	submap_img.pose_in_map.orientation.w = q.w();
	submap_img.pose_in_map.orientation.x = q.x();
	submap_img.pose_in_map.orientation.y = q.y();
	submap_img.pose_in_map.orientation.z = q.z();
	cv::Mat cellMat = cv::imread(submap_filepath + "/probability_grid.png",
                   CV_LOAD_IMAGE_UNCHANGED);
  if (cellMat.data == nullptr)
  {
    ROS_FATAL("Load %s/probability_grid.png failed!", submap_filepath.c_str());
    return;
  }
  // ROS_WARN("(%.2lf, %.2lf) %.3lf", max[0], max[1], resolution);
	cv::Mat map_adjusted = grey16ToRgba(cellMat);

//  imshow("1",map_adjusted);
// 	waitKey(0);
	sensor_msgs::CompressedImagePtr msg_ptr =  cv_bridge::CvImage(std_msgs::Header(),"bgra8",map_adjusted).toCompressedImageMsg(cv_bridge::PNG);
	submap_img.image = *msg_ptr;
}
}
