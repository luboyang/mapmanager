/*
This is a demo application to showcase how you can use an
interaction area (iarea). An iarea can be used to track mouse
interactions over an specific space.

Copyright (c) 2016 Fernando Bevilacqua <dovyski@gmail.com>
Licensed under the MIT license.
*/

#include <ros/ros.h>

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <thread>

#define CVUI_IMPLEMENTATION
#include <jsoncpp/json/json.h>

#include "cartographer_ros_msgs/FinishMapping.h"
#include "cartographer_ros_msgs/LaunchNode.h"
#include "cartographer_ros_msgs/ProgressFeedback.h"
#include "cartographer_ros_msgs/SetSwitch.h"
#include "client/map_client.h"
#include "client/map_client_3d.h"
#include "map_manager_msgs/MergeSubmaps.h"
#include "utility/cvui.h"
#define WINDOW_NAME "offline ui"

using namespace std;
ros::ServiceClient carto_launcher;
ros::ServiceClient map_manager_launcher;
ros::ServiceClient lasermark_launcher;
ros::ServiceClient localiser_launcher;
ros::ServiceClient save_map_client;
ros::ServiceClient merge_submaps_client;
ros::Subscriber mapping_shutdown_sub_;
cartographer_ros_msgs::LaunchNode launcher_msg;
std::unique_ptr<map_manager::MapInterface> map_client;
bool use_3d = false;
bool use_lego = false;
bool use_scans = false;
bool use_imu = true;
bool use_odom = false;
bool use_gps = false;
bool use_lasermarks = false;
bool use_visualmarks = false;
bool use_scenes = false;
string laser_topic1 = "";
string laser_topic2 = "";
string imu_topic = "";
string odom_topic = "";
string map_name = "";
string origin_map_name = "";

static string client_state = "Open client";
bool localiser_launched = false;
bool lego_launched = false;
bool use_lidar_localiser = false;
bool initialised = false;

struct PoseWithID
{
  int id;
  Eigen::Vector3d position;
  Eigen::Quaterniond orientation;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

inline bool judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    LOG(INFO) << "\033[33m" << file_name << " is not existed!\033[37m";
    existed_flag = false;
  }
  return existed_flag;
}
geometry_msgs::Pose ToGeometryMsgsPose(const PoseWithID &pose)
{
  geometry_msgs::Pose p;
  p.position.x = pose.position(0);
  p.position.y = pose.position(1);
  p.position.z = pose.position(2);

  p.orientation.w = pose.orientation.w();
  p.orientation.x = pose.orientation.x();
  p.orientation.y = pose.orientation.y();
  p.orientation.z = pose.orientation.z();
  return p;
}
std::vector<PoseWithID> getWorldNavPoints(const std::string &map_path)
{
  Json::Reader reader;
  Json::Value root;

  ifstream in(map_path, ios::binary);
  if (!in.is_open())
  {
    LOG(WARNING) << "Error opening file\n";
  }
  std::vector<PoseWithID> poses;
  if (reader.parse(in, root))
  {
    for (auto node : root["nodes"])
    {
      Json::Value world_pose = node["worldPose"];
      Json::Value world_pose_position = world_pose["position"];
      Json::Value world_pose_orientation = world_pose["orientation"];
      PoseWithID pose;
      pose.position(0) = world_pose_position["x"].asDouble();
      pose.position(1) = world_pose_position["y"].asDouble();
      pose.position(2) = world_pose_position["z"].asDouble();

      pose.orientation = Eigen::Quaterniond(
          world_pose_orientation["w"].asDouble(), world_pose_orientation["x"].asDouble(),
          world_pose_orientation["y"].asDouble(), world_pose_orientation["z"].asDouble());
      pose.id = node["iD"].asInt();
      poses.push_back(pose);
    }
  }
  LOG(WARNING) << "nav points size:" << poses.size();
  return poses;
}

void setWorldNavPoints(const std::string &origin_map_path, const std::string &new_map_path,
                       const std::vector<geometry_msgs::Pose> &nav_points)
{
  Json::Reader reader;
  Json::Value root;
  if (!judgeFileExist(origin_map_path + "/info_WorldRefer.json")) return;
  ifstream in(origin_map_path + "/info_WorldRefer.json", ios::in);

  if (!in.is_open())
  {
    LOG(WARNING) << "Error opening file\n";
  }
  std::vector<PoseWithID> poses;
  if (reader.parse(in, root, true))
  {
    int i = 0;
    LOG(WARNING) << root["nodes"].size() << ", " << nav_points.size();
    for (auto &node : root["nodes"])
    {
      //       Json::Value& world_pose = node["worldPose"];
      //       Json::Value& world_pose_position = node["worldPose"]["position"];
      //       Json::Value& world_pose_orientation =
      //       node["worldPose"]["orientation"];
      node["worldPose"]["position"]["x"] = nav_points[i].position.x;
      node["worldPose"]["position"]["y"] = nav_points[i].position.y;
      node["worldPose"]["position"]["z"] = nav_points[i].position.z;

      node["worldPose"]["orientation"]["w"] = nav_points[i].orientation.w;
      node["worldPose"]["orientation"]["x"] = nav_points[i].orientation.x;
      node["worldPose"]["orientation"]["y"] = nav_points[i].orientation.y;
      node["worldPose"]["orientation"]["z"] = nav_points[i].orientation.z;
      i++;
    }
  }
  Json::StyledWriter sw;
  ofstream os;
  os.open(new_map_path + "/info_WorldRefer.json");
  LOG(WARNING) << new_map_path + "/info_WorldRefer.json";
  os << sw.write(root);
  os.close();
}

void launchClient()
{
  if (client_state == "Open client")
  {
    client_state = "Close client";
    string cmd_set = "rosrun map_manager map_client_node";
    int status = system(cmd_set.c_str());
  }
  else
  {
    client_state = "Open client";
    string cmd_set = "kill -9 $(pgrep map_client_node)";
    int status = system(cmd_set.c_str());
  }
}

void launchLocaliser()
{
  string cmd_set;
  if (localiser_launched)
  {
    if (use_lidar_localiser)
      cmd_set = "kill -9 $(pgrep lidar_localiser)";
    else
      cmd_set = "kill -9 $(pgrep scan_localiser)";
    localiser_launched = false;
  }
  else
  {
    if (use_3d)
    {
      use_lidar_localiser = true;
      cmd_set = "roslaunch lidar_localiser lidar_localiser.launch";
    }
    else
    {
      use_lidar_localiser = false;
      cmd_set = "roslaunch laser_localiser scan_localiser.launch";
    }
    localiser_launched = true;
  }
  int status = system(cmd_set.c_str());
}

void changeClientState()
{
  std::thread client_thread(launchClient);
  client_thread.detach();
  LOG(INFO) << "\033[34m client_state: " << client_state;
}

void changeLocaliserState()
{
  //   std::thread localiser_thread(launchLocaliser);
  //   localiser_thread.detach();
}

void callMppingSrv()
{
  if (client_state == "Close client") changeClientState();
  if (localiser_launched) changeLocaliserState();

  launcher_msg.request.status = 0;
  carto_launcher.call(launcher_msg);
  map_manager_launcher.call(launcher_msg);
  lasermark_launcher.call(launcher_msg);
}

void callLocalizationSrv()
{
  if (client_state == "Close client") changeClientState();
  LOG(INFO) << "Close client success!";
  //   changeLocaliserState();
  launcher_msg.request.status = 1;
  carto_launcher.call(launcher_msg);
  LOG(INFO) << "Call cartographer_node success!";
  map_manager_launcher.call(launcher_msg);
  LOG(INFO) << "Call map_manager_node success!";
  lasermark_launcher.call(launcher_msg);
  LOG(INFO) << "Call lasermark_node success!";
  localiser_launcher.call(launcher_msg);
  LOG(INFO) << "Call localiser_node success!";
}

void callPatchMappingSrv()
{
  if (client_state == "Close client") changeClientState();
  if (localiser_launched) changeLocaliserState();
  launcher_msg.request.status = 2;
  carto_launcher.call(launcher_msg);
  map_manager_launcher.call(launcher_msg);
  lasermark_launcher.call(launcher_msg);
}

void callIDLESrv()
{
  if (client_state == "Close client") changeClientState();
  launcher_msg.request.status = -1;
  if (localiser_launched)
  {
    localiser_launcher.call(launcher_msg);
    changeLocaliserState();
  }
  carto_launcher.call(launcher_msg);
  map_manager_launcher.call(launcher_msg);
  lasermark_launcher.call(launcher_msg);
}

void callSaveMap()
{
  cartographer_ros_msgs::FinishMapping srv;
  srv.request.type = "cartographer_node";
  string world_info_path = origin_map_name + "/info_WorldRefer.json";
  if (!judgeFileExist(world_info_path))
  {
    LOG(INFO) << (world_info_path + " not existed!");
    geometry_msgs::Pose temp;
    srv.request.origin_nav_points.push_back(temp);
  }
  else
    for (auto nav_point : getWorldNavPoints(world_info_path))
    {
      srv.request.origin_nav_points.push_back(ToGeometryMsgsPose(nav_point));
    }

  save_map_client.call(srv);
  if (!srv.response.success) LOG(WARNING) << "error msg: " << srv.response.error_message;
  if (srv.response.update_nav_points.size() == 0) return;

  std::string new_map_path = std::getenv("HOME");
  new_map_path = new_map_path + "/map/" + map_name;
  setWorldNavPoints(origin_map_name, new_map_path, srv.response.update_nav_points);

  //   changeClientState("");
}

void callMergeSubmaps()
{
  map_manager_msgs::MergeSubmaps srv;

  string origin_name = "";
  string new_name = "";
  ::ros::param::get("/offline_ui_node/origin_map_name", origin_name);
  ::ros::param::get("/offline_ui_node/new_map_name", new_name);

  LOG(INFO) << "/merge_submaps/origin_map_name:" << origin_name;
  LOG(INFO) << "/merge_submaps/new_map_name:" << new_name;

  srv.request.new_map_name = new_name;
  srv.request.origin_map_name = origin_name;
  std::string origin_map_path = std::getenv("HOME");
  origin_map_path = origin_map_path + "/map/" + srv.request.origin_map_name;
  std::string new_map_path = std::getenv("HOME");
  new_map_path = new_map_path + "/map/" + srv.request.new_map_name;
  for (auto nav_point : getWorldNavPoints(origin_map_path))
  {
    srv.request.origin_nav_points.push_back(ToGeometryMsgsPose(nav_point));
  }

  bool fix_nac_points = true;
  ::ros::param::get("/cartographer_node/fix_nav_points", fix_nac_points);
  srv.request.fix_nav_points = fix_nac_points;
  merge_submaps_client.call(srv);

  setWorldNavPoints(origin_map_path, new_map_path, srv.response.update_nav_points);
  ::ros::param::get("/vtr/path", new_map_path);
}

void updateParams(cv::Mat &frame, const string &state)
{
  if (!initialised)
  {
    //     ::ros::param::get("/cartographer_node/is_3d",use_3d);
    //     ::ros::param::get("/lasermark_node/is_3d",use_3d);
    //     ::ros::param::get("/map_manager_node/is_3d",use_3d);
    ::ros::param::get("/cartographer_node/use_2scans", use_scans);
    ::ros::param::get("/cartographer_node/use_odom", use_odom);
    ::ros::param::get("/cartographer_node/use_imu", use_imu);
    ::ros::param::get("/cartographer_node/use_lasermarks", use_lasermarks);
    ::ros::param::get("/cartographer_node/use_visualmarks", use_visualmarks);
    ::ros::param::get("/cartographer_node/use_gps", use_gps);
    ::ros::param::get("/cartographer_node/use_lego_loam_odom", use_lego);
    ::ros::param::get("/cartographer_node/use_scenes", use_scenes);
    initialised = true;
  }
  else
  {
    ::ros::param::set("/cartographer_node/is_3d", use_3d);
    ::ros::param::set("/lasermark_node/is_3d", use_3d);
    ::ros::param::set("/map_manager_node/is_3d", use_3d);
    ::ros::param::set("/cartographer_node/use_2scans", use_scans);
    ::ros::param::set("/cartographer_node/use_odom", use_odom);
    ::ros::param::set("/cartographer_node/use_imu", use_imu);
    ::ros::param::set("/cartographer_node/use_lasermarks", use_lasermarks);
    ::ros::param::set("/cartographer_node/use_visualmarks", use_visualmarks);
    ::ros::param::set("/cartographer_node/use_gps", use_gps);
    ::ros::param::set("/cartographer_node/use_lego_loam_odom", use_lego);
    ::ros::param::set("/cartographer_node/use_scenes", use_scenes);
  }
  laser_topic1 = "";
  laser_topic2 = "";
  imu_topic = "";
  odom_topic = "";
  ::ros::param::get("/algo/property/imu_topic", imu_topic);
  ::ros::param::get("/algo/property/motion_odom_topic", odom_topic);
  if (!use_3d)
  {
    ::ros::param::get("/algo/property/laser2d_nav_topic_1", laser_topic1);
    if (use_scans)
    {
      ::ros::param::get("/algo/property/laser2d_nav_topic_2", laser_topic2);
    }
  }
  else
  {
    laser_topic1 = "/pointcloud_front";
    if (use_scans)
    {
      laser_topic1 = "/pointcloud_front";
      laser_topic2 = "/pointcloud_back";
    }
  }
  if (state == "Mapping" || state == "PatchMapping" || state == "SaveMap")
  {
    ::ros::param::set("/map_client/mode", 0);
    ::ros::param::set("/vtr/mode", 0);
    if (state == "PatchMapping") ::ros::param::set("/vtr/mode", 2);
    ::ros::param::get("/map_name", map_name);
    if (state == "PatchMapping") ::ros::param::get("/vtr/origin_path", origin_map_name);
  }
  else if (state == "Localization")
  {
    ::ros::param::set("/map_client/mode", 1);
    ::ros::param::set("/vtr/mode", 1);
    ::ros::param::get("/vtr/path", map_name);
  }
  else
  {
    ::ros::param::set("/vtr/mode", -1);
    ::ros::param::set("/map_client/mode", -1);
  }

  LOG(INFO) << "laser_topic1:" << laser_topic1;
  LOG(INFO) << "laser_topic2:" << laser_topic2;
}

void handleProgressFeedback(const cartographer_ros_msgs::ProgressFeedback::ConstPtr &msg)
{
  if (msg->feedback_value >= 100)
  {
    callIDLESrv();
  }
}

void handleUI()
{
  cv::Mat frame = cv::Mat(300, 600, CV_8UC3);
  cvui::init(WINDOW_NAME);
  updateParams(frame, "IDLE");

  while (true)
  {
    // Fill the frame with a nice color
    frame = cv::Scalar(49, 52, 49);
    if (cvui::button(frame, 60, 220, "Mapping"))
    {
      updateParams(frame, "Mapping");
      callMppingSrv();
    }
    if (cvui::button(frame, 180, 220, "PatchMapping"))
    {
      updateParams(frame, "PatchMapping");
      callPatchMappingSrv();
    }
    if (cvui::button(frame, 340, 220, "Localization"))
    {
      updateParams(frame, "Localization");
      callLocalizationSrv();
    }
    if (cvui::button(frame, 280, 260, "IDLE"))
    {
      updateParams(frame, "IDLE");
      callIDLESrv();
    }
    if (cvui::button(frame, 20, 260, "MergeMaps"))
    {
      updateParams(frame, "MergeMaps");
      callMergeSubmaps();
    }

    if (cvui::button(frame, 150, 260, "SaveMap"))
    {
      updateParams(frame, "SaveMap");
      callSaveMap();
    }

    if (cvui::button(frame, 390, 260, client_state))
    {
      changeClientState();
    }

    cvui::checkbox(frame, 480, 10, "3D", &use_3d);
    cvui::checkbox(frame, 480, 40, "LegoOdom", &use_lego);
    cvui::checkbox(frame, 480, 70, "2Lasers", &use_scans);
    cvui::checkbox(frame, 480, 100, "Imu", &use_imu);
    cvui::checkbox(frame, 480, 130, "Odom", &use_odom);
    cvui::checkbox(frame, 480, 160, "Gps", &use_gps);
    cvui::checkbox(frame, 480, 190, "Lasermarks", &use_lasermarks);
    cvui::checkbox(frame, 480, 220, "Visualmarks", &use_visualmarks);
    cvui::checkbox(frame, 480, 250, "Scenes", &use_scenes);

    //     cvui::printf(frame, 10, 10, 0.4, 0xff0000, "laser1_topic:
    //     %s",laser_topic1.c_str()); cvui::printf(frame, 10, 25, 0.4, 0xff0000,
    //     "laser2_topic: %s",laser_topic2.c_str());
    cvui::printf(frame, 10, 10, 0.4, 0xff0000, "Laser1_topic:");
    cvui::printf(frame, 100, 10, 0.4, 0x00ffff, "%s", laser_topic1.c_str());
    cvui::printf(frame, 10, 30, 0.4, 0xff0000, "Laser2_topic:");
    cvui::printf(frame, 100, 30, 0.4, 0x00ffff, "%s", laser_topic2.c_str());

    cvui::printf(frame, 10, 50, 0.4, 0xff0000, "Imu_topic:");
    cvui::printf(frame, 80, 50, 0.4, 0x00ffff, "%s", imu_topic.c_str());
    cvui::printf(frame, 10, 70, 0.4, 0xff0000, "Odom_topic:");
    cvui::printf(frame, 95, 70, 0.4, 0x00ffff, "%s", odom_topic.c_str());

    cvui::printf(frame, 10, 90, 0.4, 0xff0000, "Map_name:");
    cvui::printf(frame, 85, 90, 0.4, 0x00ffff, "%s", map_name.c_str());
    cvui::printf(frame, 10, 110, 0.4, 0xff0000, "OriginMap_name:");
    cvui::printf(frame, 120, 110, 0.4, 0x00ffff, "%s", origin_map_name.c_str());

    cvui::update();
    cv::imshow(WINDOW_NAME, frame);
    cv::waitKey(2);
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "offline_ui_node");
  ros::NodeHandle n;
  carto_launcher = n.serviceClient<cartographer_ros_msgs::LaunchNode>("/cartographer_launcher");
  map_manager_launcher =
      n.serviceClient<cartographer_ros_msgs::LaunchNode>("/map_manager_launcher");
  lasermark_launcher = n.serviceClient<cartographer_ros_msgs::LaunchNode>("/lasermark_launcher");
  localiser_launcher =
      n.serviceClient<cartographer_ros_msgs::LaunchNode>("/vtr/node_switch_server");
  save_map_client = n.serviceClient<cartographer_ros_msgs::FinishMapping>("/shutdown_cartographer");
  merge_submaps_client =
      n.serviceClient<map_manager_msgs::MergeSubmaps>("/map_manager_node/merge_map");
  mapping_shutdown_sub_ = n.subscribe<cartographer_ros_msgs::ProgressFeedback>(
      "/cartographer_ros/progress_feedback", 10, handleProgressFeedback);
  std::thread m_thread(handleUI);
  m_thread.detach();
  ros::spin();
  if (client_state == "Close client") changeClientState();
  return 0;
}