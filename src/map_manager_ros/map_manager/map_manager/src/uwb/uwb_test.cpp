#include "uwb_pose_optimization.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "uwb_test");
  map_manager::uwbPoseOptimization uwb(std::string(std::getenv("HOME")) + "/map/t12/");
  uwb.compute();
  return 1;
}