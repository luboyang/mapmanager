/*
 * Copyright 2023 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "xml_handle.h"
using namespace tinyxml2;
using namespace std;
namespace tiny_xml {
XmlHandle::XmlHandle()
{

}

XmlHandle::~XmlHandle()
{

}

inline void setXmlEDocument(XMLDocument& doc,const std::string& name, const std::string& value)
{
  XMLElement *ele = doc.NewElement(name.c_str());
  ele->InsertFirstChild(doc.NewText(value.c_str()));
  doc.InsertEndChild(ele);
}

inline void setXmlElement(XMLDocument& doc,  XMLElement* ele,const std::string& name, const std::string& value)
{
  XMLElement* userNode = doc.NewElement(name.c_str());
  userNode->InsertFirstChild(doc.NewText(value.c_str()));
  //4.3设置结束标签
  ele->InsertEndChild(userNode);
}

bool SubmapDataXml::SetXml(const std::string& path)
{
  std::string folderStringSubmap = path + "/frames/" + std::to_string(node_.id);
  std::string systemCommand = "mkdir -p " + folderStringSubmap;
  std::string systemCommandErrorMsg;
  if (!systemutils::CheckSystemStatus(systemCommand,systemCommandErrorMsg))
  {
    printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
    return false;
  }
  
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  setXmlEDocument(doc, "id", std::to_string(node_.id));
  std::string poseStr;
  stringutils::PoseXmlToString(node_.pose, poseStr);
  setXmlEDocument(doc, "pose", poseStr);
  std::string localPoseStr;
  stringutils::PoseXmlToString(node_.localPose, localPoseStr);
  setXmlEDocument(doc, "local_pose", localPoseStr);
  setXmlEDocument(doc, "num_range_data", std::to_string(node_.numRangeData));
  setXmlEDocument(doc, "finished", node_.finished ? "true" : "false");
  XMLElement *pProbabilityGrid = doc.NewElement("probability_grid");
  doc.InsertFirstChild(pProbabilityGrid);
  doc.InsertEndChild(pProbabilityGrid);
  setXmlElement(doc, pProbabilityGrid, "resolution", std::to_string(node_.resolution));
  std::string maxStr = std::to_string(node_.max[0]) + " " +
      std::to_string(node_.max[1]);
  setXmlElement(doc, pProbabilityGrid, "max", maxStr);
  std::string numCellsStr = std::to_string(node_.numCells[0]) + " " +
      std::to_string(node_.numCells[1]);
  setXmlElement(doc, pProbabilityGrid, "num_cells", numCellsStr);
  std::string knownCellsBoxStr = std::to_string(node_.knownCellsBox[0]) + " " +
      std::to_string(node_.knownCellsBox[1]) + " " +
      std::to_string(node_.knownCellsBox[2]) + " " +
      std::to_string(node_.knownCellsBox[3]);
  setXmlElement(doc, pProbabilityGrid, "known_cells_box", knownCellsBoxStr);   
  string save_path = folderStringSubmap + "/data.xml";
  cout << "save_path: " << save_path <<endl;
  return XML_SUCCESS == doc.SaveFile(save_path.c_str());
}

bool SubmapDataXml::GetXml(const std::string& path)
{
  XMLDocument doc;
  string file_path = path + "/data.xml";
  cout << "file_path: " << file_path <<endl;
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  node_.id = std::atoi(doc.FirstChildElement("id")->GetText());
  node_.finished = doc.FirstChildElement("finished")->BoolText();
  node_.numRangeData = std::atoi(doc.FirstChildElement("num_range_data")->GetText());
  std::string poseStr = doc.FirstChildElement("pose")->GetText();
  stringutils::PoseStringToXml(poseStr, node_.pose);
  std::string localPoseStr = doc.FirstChildElement("local_pose")->GetText();
  stringutils::PoseStringToXml(localPoseStr, node_.localPose);
  std::string maxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("max")->GetText();
  sscanf(maxStr.c_str(), "%lf %lf", &node_.max[0], &node_.max[1]);
  std::string numCellsStr = doc.FirstChildElement("probability_grid")->FirstChildElement("num_cells")->GetText();
  sscanf(numCellsStr.c_str(), "%d %d", &node_.numCells[0], &node_.numCells[1]);
  std::string knownCellsBoxStr = doc.FirstChildElement("probability_grid")->FirstChildElement("known_cells_box")->GetText();
  sscanf(knownCellsBoxStr.c_str(), "%d %d %d %d", &node_.knownCellsBox[0], &node_.knownCellsBox[1],
          &node_.knownCellsBox[2], &node_.knownCellsBox[3]);
  node_.resolution = doc.FirstChildElement("probability_grid")->FirstChildElement("known_cells_box")->DoubleText();
  return true;
}


bool MapXml::SetXml(const string& path)
{
  std::string systemCommand = "mkdir -p " + path;
  std::string systemCommandErrorMsg;
  if (!systemutils::CheckSystemStatus(systemCommand,systemCommandErrorMsg))
  {
    printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
    return false;
  }
  if(0 == nodeCount_)
  {
    printf("NodeList is empty!");
    return false;
  }
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  XMLElement *pmap= doc.NewElement("map");
  doc.InsertFirstChild(pmap);
  doc.InsertEndChild(pmap);
  
  XMLElement *pnode_list= doc.NewElement("node_list");
  pmap->InsertFirstChild(pnode_list);
  pmap->InsertEndChild(pnode_list);
  
  for (const auto &node : nodeList_)
  {
    XMLElement *pnode= doc.NewElement("node");
    pnode_list->InsertFirstChild(pnode);
    pnode_list->InsertEndChild(pnode);
    setXmlElement(doc, pnode, "id", std::to_string(node.id));
    
    XMLElement *pneighbour_list= doc.NewElement("neighbour_list");
    pnode->InsertFirstChild(pneighbour_list);
    pnode->InsertEndChild(pneighbour_list);
    for (const auto &neighbour : node.neighbourList)
    {
      XMLElement *pneighbour= doc.NewElement("neighbour");
      pneighbour_list->InsertFirstChild(pneighbour);
      pneighbour_list->InsertEndChild(pneighbour);
    
      setXmlElement(doc, pneighbour, "id", std::to_string(neighbour.id));
      std::string transformStr;
      stringutils::PoseXmlToString(neighbour.transform, transformStr, true);
      setXmlElement(doc, pneighbour, "transform", transformStr);
      setXmlElement(doc, pneighbour, "reachable", neighbour.reachable?"true":"false");
    }
    
  }
  XMLElement *pproperty= doc.NewElement("property");
  pmap->InsertFirstChild(pproperty);
  pmap->InsertEndChild(pproperty);
  setXmlElement(doc, pproperty, "node_count", std::to_string(nodeList_.size()));
  string save_path = path + "/map.xml";
  cout << "save_path: " << save_path <<endl;
  return XML_SUCCESS == doc.SaveFile(save_path.c_str());
}

bool MapXml::GetXml(const string& path)
{
  XMLDocument doc;
  string file_path = path + "/map.xml";
  cout << "file_path: " << file_path <<endl;
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  XMLElement *pmap = doc.RootElement();
  XMLElement *pnode_list = pmap->FirstChildElement("node_list");
  XMLElement *pproperty = pmap->FirstChildElement("property");
  XMLElement *pnode = pnode_list->FirstChildElement();
  file_data_type::MapData tmpNode;
  file_data_type::NeighbourInMapData neighbourInNode;
  nodeList_.clear();
  while(pnode != NULL)
  {
    tmpNode.id = pnode->FirstChildElement("id")->IntText();
    XMLElement *pneighbour_list = pnode->FirstChildElement("neighbour_list");
    XMLElement *pneighbour = pneighbour_list->FirstChildElement();
    while(pneighbour != NULL)
    {
      neighbourInNode.id = pneighbour->FirstChildElement("id")->IntText();
      string tmpPoseStr = pneighbour->FirstChildElement("transform")->GetText();
      stringutils::PoseStringToXml(tmpPoseStr, neighbourInNode.transform, true);
      neighbourInNode.reachable = pneighbour->FirstChildElement("reachable")->BoolText();
      pneighbour = pneighbour->NextSiblingElement();
      tmpNode.neighbourList.emplace_back(std::move(neighbourInNode));
    }
    nodeList_.emplace_back(std::move(tmpNode));
    pnode = pnode->NextSiblingElement();
  }
  this->nodeCount_=this->nodeList_.size();
  return true;
}

bool LandmarkXml::SetXml(const string& path)
{  
  XMLDocument doc;
  XMLDeclaration *declaration = doc.NewDeclaration();
  doc.InsertFirstChild(declaration);
  for(const auto &node : nodeList_)
  {
    XMLElement *plandmark = doc.NewElement("landmark");
    doc.InsertFirstChild(plandmark);
    doc.InsertEndChild(plandmark);
    setXmlElement(doc, plandmark, "ns", node.ns);
    setXmlElement(doc, plandmark, "id", node.id);
    setXmlElement(doc, plandmark, "pole_radius", std::to_string(node.pole_radius));
    setXmlElement(doc, plandmark, "visible", std::to_string(node.visible));
    std::string transformStr;
    stringutils::PoseXmlToString(node.transform, transformStr);
    setXmlElement(doc, plandmark, "transform", transformStr);
    std::string translationInImageStr = std::to_string(node.translation_in_image[0]) + " " +
    std::to_string(node.translation_in_image[1]);
    setXmlElement(doc, plandmark, "translation_in_image", translationInImageStr);
  }
  string save_path = path + "landmark.xml";
  cout << nodeList_.size() << " save_path: " << save_path <<endl;
  return XML_SUCCESS == doc.SaveFile(save_path.c_str()); 
}

bool LandmarkXml::GetXml(const string& path)
{
  XMLDocument doc;
  string file_path = path + "landmark.xml";
  cout << "file_path: " << file_path <<endl;
  if(XML_SUCCESS != doc.LoadFile(file_path.c_str()))
  {
    cout<<"load xml file failed!"<<endl;
    return false;
  }
  XMLElement *plandmark = doc.FirstChildElement("landmark");
  file_data_type::LandmarkInfo mark_info;
  std::string tmpTranslationInImageStr;
  while(plandmark != NULL)
  {
    mark_info.ns = plandmark->FirstChildElement("ns")->GetText();
    mark_info.id = plandmark->FirstChildElement("id")->GetText();
    if(plandmark->FirstChildElement("pole_radius") == NULL)
      mark_info.pole_radius = 0;
    else
      mark_info.pole_radius = plandmark->FirstChildElement("pole_radius")->FloatText();
    
    mark_info.visible = plandmark->FirstChildElement("visible")->IntText();
    std::string tmpPoseStr = plandmark->FirstChildElement("transform")->GetText();
    
    if(plandmark->FirstChildElement("translation_in_image") == NULL)
      tmpTranslationInImageStr = "0 0";
    else
      tmpTranslationInImageStr = plandmark->FirstChildElement("translation_in_image")->GetText();
    plandmark->FirstChildElement();
    stringutils::PoseStringToXml(tmpPoseStr, mark_info.transform);
    std::sscanf(tmpTranslationInImageStr.c_str(), "%lf %lf",
      &mark_info.translation_in_image[0], &mark_info.translation_in_image[1]);
    nodeList_.emplace_back(std::move(mark_info));
    plandmark = plandmark->NextSiblingElement();
  }
  nodeCount_ = nodeList_.size();
  return true;
}



}
