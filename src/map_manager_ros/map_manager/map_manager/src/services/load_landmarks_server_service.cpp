#include "load_landmarks_server_service.h"

common::ErrorInfo
LoadLandmarksService::LoadLandmarksData(std::vector<file_data_type::LandmarkInfo> &markerList)
{
  tiny_xml::LandmarkXml landmarksXml;
  if (!landmarksXml.GetXml(filePath_))
  {
    return common::ErrorInfo(-1, "Get landmark.xml failed!", common::ErrorType::kError);
  }
  markerList = landmarksXml.GetNodeList();
  return common::ErrorInfo();
}

common::ErrorInfo
LoadLandmarksService::UpDateLandmarks(const std::vector<file_data_type::LandmarkInfo> &newLandmarkList)
{
  tiny_xml::LandmarkXml landmarksXml;
  if (!landmarksXml.GetXml(filePath_))
  {
    return common::ErrorInfo(-1, "Get landmark.xml failed!", common::ErrorType::kError);
  }
  auto landmarkList = landmarksXml.GetNodeList();

  for (const auto &newLandmark : newLandmarkList)
  {
    for (auto &landmark : landmarkList)
    {
      if (newLandmark.id == landmark.id && newLandmark.ns == landmark.ns)
      {
        landmark.visible = 0;
      }
    }
  }
  tiny_xml::LandmarkXml newLandmarksXml(landmarkList);
  if (!newLandmarksXml.SetXml(filePath_))
  {
    return common::ErrorInfo(-1, "Set new landmark.xml failed!", common::ErrorType::kError);
  }
  return common::ErrorInfo();
}

common::ErrorInfo
LoadLandmarksService::ModifyLandmarksId(const std::vector<output_data_type::IdRemap> &idList)
{
  if (idList.empty())
  {
    std::cout << "empty" <<std::endl;
    return common::ErrorInfo(-1, "IdList is empty!", common::ErrorType::kError);
  }

  tiny_xml::LandmarkXml landmarksXml;
  if (!landmarksXml.GetXml(filePath_))
  {
    std::cout << "Get landmark.xml failed" <<std::endl;
    return common::ErrorInfo(-1, "Get landmark.xml failed!", common::ErrorType::kError);
  }
  auto landmarkList = landmarksXml.GetNodeList();
  std::map<std::string, file_data_type::LandmarkInfo> mark_list_map;
  for (auto &landmark : landmarkList)
  {
    std::string key = landmark.ns+landmark.id;
    mark_list_map[key] = landmark;
    
  }
  
  for (const auto &id : idList)
  {
    std::string key = id.ns+std::to_string(id.origin_id);
    if(mark_list_map.find(key) == mark_list_map.end())
    {
      std::cout << "Can not find key : " << key <<std::endl;
      continue;
    }
    mark_list_map[key].id = std::to_string(id.new_id);
  }
  std::vector<file_data_type::LandmarkInfo> new_mark_list;
  new_mark_list.reserve(mark_list_map.size());
  for(const auto& mark : mark_list_map)
    new_mark_list.push_back(mark.second);
  tiny_xml::LandmarkXml newLandmarksXml(new_mark_list);
  
  if (!newLandmarksXml.SetXml(filePath_))
  {
    std::cout << "Set new landmark.xml failed" <<std::endl;
    return common::ErrorInfo(-1, "Set new landmark.xml failed!", common::ErrorType::kError);
  }
  return common::ErrorInfo();
}