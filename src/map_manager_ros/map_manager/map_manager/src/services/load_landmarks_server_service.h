#ifndef LOAD_LANDMARKS_SERVER_SERVICE
#define LOAD_LANDMARKS_SERVER_SERVICE
#include <vector>
#include "output_data_type/output_data.hpp"
#include "error_info.hpp"
#include "tinyxml2/xml_handle.h"
class LoadLandmarksService
{
  public:
  LoadLandmarksService(const std::string &filePath) : filePath_(filePath){};
  common::ErrorInfo LoadLandmarksData(std::vector<file_data_type::LandmarkInfo> &markerList);
  common::ErrorInfo UpDateLandmarks(const std::vector<file_data_type::LandmarkInfo> &newLandmarkList);
  common::ErrorInfo ModifyLandmarksId(const std::vector<output_data_type::IdRemap> &idList);

  private:
  std::string filePath_;
};

#endif