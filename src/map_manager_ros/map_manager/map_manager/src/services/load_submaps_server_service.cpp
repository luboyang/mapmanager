
#include "load_submaps_server_service.h"
#include "utils/utils_cv.hpp"
common::ErrorInfo LoadSubmapsServerService::GetSubmapImages(
    std::vector<output_data_type::SubmapImageEntry> &submapImages,
    output_data_type::MapPosesInfo &map_poses_info)
{
//   data_xml::MapXml mapXml;
  tiny_xml::MapXml mapXml;
  if (!mapXml.GetXml(fullPath_))
  {
    return common::ErrorInfo(-1, "Get map.xml failed!", common::ErrorType::kError);
  }

  auto nodeCount = mapXml.GetNodeCount();
  if (1 == nodeCount)
  {
    std::string submapFilepath = fullPath_ + "/frames/";
    output_data_type::SubmapImageEntry submap_img;
    LoadOldMap(submapFilepath + std::to_string(0), submap_img, map_poses_info);
    submapImages.emplace_back(std::move(submap_img));

    // LOG(INFO) << "[" << ros::Time::now() << "] " << ("Load submaps done!");
    return common::ErrorInfo();
  }
  else
  {
    return common::ErrorInfo(-1, "NodeCount in map.xml is " + std::to_string(nodeCount), common::ErrorType::kError);
  }
}

common::ErrorInfo LoadSubmapsServerService::LoadOldMap(const std::string &submapFilepath,
                                               output_data_type::SubmapImageEntry &submap_image,
                                               output_data_type::MapPosesInfo &map_poses_info)
{
  tiny_xml::SubmapDataXml laserDataXml;
  if (!laserDataXml.GetXml(submapFilepath))
  {
    return common::ErrorInfo(-1, "Get data.xml failed!", common::ErrorType::kError);
  }
  file_data_type::SubmapData submap_data = laserDataXml.GetNode();
  Rigid3d local2global = submap_data.pose * submap_data.localPose.inverse();
  Eigen::Vector3d pointInLocal(submap_data.max[0], submap_data.max[1], 0); 
  submap_image.submap_index = submap_data.id;
  submap_image.resolution = submap_data.resolution;
  submap_image.pose_in_map = Rigid3d(local2global * pointInLocal, local2global.rotation());
  cv::Mat cellMat = cv::imread(submapFilepath + "/probability_grid.png", CV_LOAD_IMAGE_UNCHANGED);
  submap_image.image = cvutils::Grey16ToRgba(cellMat);
  map_poses_info.local_in_global_poses.push_back(ToIsometry(local2global));
  map_poses_info.poses_in_global.push_back(ToIsometry(submap_data.pose));
  map_poses_info.maxes.push_back(submap_data.max[0]);
  map_poses_info.maxes.push_back(submap_data.max[1]);
  map_poses_info.size_of_map.push_back(submap_data.numCells[0]);
  map_poses_info.size_of_map.push_back(submap_data.numCells[1]);
  return common::ErrorInfo();
}