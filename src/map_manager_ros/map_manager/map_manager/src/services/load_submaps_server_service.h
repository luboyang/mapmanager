#ifndef LOAD_SUBMAPS_SERVER_SERVICE_
#define LOAD_SUBMAPS_SERVER_SERVICE_
#include "tinyxml2/xml_handle.h"
#include "output_data_type/output_data.hpp"
#include "error_info.hpp"

class LoadSubmapsServerService
{
public:
  LoadSubmapsServerService(const std::string &fullPath):fullPath_(fullPath){};
  common::ErrorInfo GetSubmapImages(std::vector<output_data_type::SubmapImageEntry> &submap_images,
                            output_data_type::MapPosesInfo &map_poses_info);

private:
  common::ErrorInfo LoadOldMap(const std::string &filePath,
                       output_data_type::SubmapImageEntry &submapImage,
                       output_data_type::MapPosesInfo &map_poses_info);
  std::string fullPath_;
};

#endif // LOAD_SUBMAPS_SERVER_SERVICE_