/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPMAPPING_H
#define MAPMAPPING_H

#include <cartographer_ros/msg_conversion.h>
#include <cartographer_ros/node_constants.h>
#include <cartographer_ros/ros_log_sink.h>
#include <cartographer_ros/submap.h>
#include <cartographer_ros_msgs/FinishMapping.h>
#include <cartographer_ros_msgs/GlobalMap.h>
#include <cartographer_ros_msgs/SaveMapServer.h>
#include <cartographer_ros_msgs/SaveSubmapServer.h>
#include <cartographer_ros_msgs/SetSwitch.h>
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <cartographer_ros_msgs/SubmapList.h>
#include <cartographer_ros_msgs/SubmapQuery.h>
#include <cartographer_ros_msgs/SubmapServer.h>
#include <cartographer_ros_msgs/TrajectoryNodePoses.h>
#include <map_manager_msgs/CreateBlankMapService.h>
#include <map_manager_msgs/GetImage.h>
#include <map_manager_msgs/LinePoints.h>

#include "map_interface.h"
namespace map_manager
{
struct TrajectoryTransform
{
  Eigen::Matrix2d area_R_map;
  Eigen::Vector2d area_t_map;
  Eigen::Vector2d max_area;
};
class MapMapping : public MapInterface
{
  public:
  MapMapping();
  ~MapMapping();
  virtual void clear() override;
  virtual void setMode(const int &mode) override;
  bool saveMapServer(::cartographer_ros_msgs::SaveMapServer::Request &request,
                     ::cartographer_ros_msgs::SaveMapServer::Response &response);

  private:
  const Eigen::Vector2d getMax(const cartographer::transform::Rigid3d &slice_pose,
                               const cartographer::transform::Rigid3d &local_pose);
  void HandleSubmapList(const cartographer_ros_msgs::SubmapList::ConstPtr &msg);
  void HandleStripDetectResult(const map_manager_msgs::LinePoints::ConstPtr &msg);
  void HandleNodePoses(const cartographer_ros_msgs::TrajectoryNodePoses::ConstPtr &msg);
  void DrawAndPublish(const ros::WallTimerEvent &unused_timer_event);

  void saveToXml(const std::string &path, const int &id);
  void saveAndMerge(const std::string &full_path, const std::string &hide_full_path);
  void preHandleMergeMaps(
      std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> &submap_slices,
      const std::vector<ProbabilityGridInfo> &infos, ProbabilityGridInfo &merge_info,
      const int &mid_num);
  void saveMapXml(const std::string &full_path,
                  const std::map<cartographer::mapping::SubmapId,
                                 std::vector<cartographer::mapping::SubmapId>> &constraints,
                  std::map<cartographer::mapping::SubmapId, int> id_num,
                  std::map<int, cartographer_ros_msgs::SubmapID> num_id);
  void saveLandmarkXml(const std::string &full_path, const std::string &hide_full_path,
                       const std::vector<LandmarkInfo> &landmarks);
  void saveImageFromPbstream(const std::string &filename, const std::string &full_map_path);
  std::map<int, TrajectoryTransform> getTrajectoryArea(const std::string &path);
  std::map<int, TrajectoryTransform>
  getTrajectoryArea(const std::map<int, std::vector<Eigen::Vector2d>> &points_with_trajectory);
  bool saveSubmapServer(::cartographer_ros_msgs::SaveSubmapServer::Request &request,
                        ::cartographer_ros_msgs::SaveSubmapServer::Response &response);
  bool loadMappingSubmapsServer(::cartographer_ros_msgs::SubmapImagesServer::Request &request,
                                ::cartographer_ros_msgs::SubmapImagesServer::Response &response);

  bool shutdownTopicServer(cartographer_ros_msgs::SetSwitch::Request &request,
                           cartographer_ros_msgs::SetSwitch::Response &response);
  bool saveMapFromPbstream(cartographer_ros_msgs::SaveSubmapServer::Request &request,
                           cartographer_ros_msgs::SaveSubmapServer::Response &response);

  bool saveBlankMapServer(map_manager_msgs::CreateBlankMapService::Request &request,
                          map_manager_msgs::CreateBlankMapService::Response &response);
  bool shutdownCarto(cartographer_ros_msgs::FinishMapping::Request &request,
                     cartographer_ros_msgs::FinishMapping::Response &response);
  //   bool getAutoIgnoredArea(map_manager_msgs::GetImage::Request& request,
  //                           map_manager_msgs::GetImage::Response& response);
  // 	void saveMergedPbstream(const std::string& path);
  void saveStripInfo(const std::vector<geometry_msgs::PoseStamped> &node_poses,
                     const std::string &map_path);

  //   ros::NodeHandle nh_;

  ros::Publisher submap_images_publisher_;
  ros::Publisher global_map_publisher_;
  ros::Publisher strip_detect_result_pub_;

  ros::Subscriber submap_list_subscriber_;
  ros::Subscriber strip_detect_result_sub_;
  ros::Subscriber node_poses_sub_;
  ros::ServiceClient submap_query_client_, submap_server_client_, shutdown_carto_client_;
  std::vector<::ros::WallTimer> wall_timers_;
  std::vector<::ros::ServiceServer> servers_;
  ros::Time last_timestamp_;
  absl::Mutex mutex_;
  std::vector<Eigen::Vector2d> maxs_;
  std::vector<cartographer::transform::Rigid3d> local_poses_, global_poses_;
  std::vector<ProbabilityGridInfo> probability_infos_, merge_probability_infos_;

  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> submap_slices_,
      save_submap_slices_;
  std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry> submap_images_;
  std::map<cartographer::common::Time, std::map<int, std::vector<Eigen::Vector3d>>>
      points_with_stamp_;
  std::map<cartographer::common::Time, cartographer::transform::Rigid3d> node_poses_with_time_;
  std::map<cartographer::common::Time, std::vector<cartographer::common::Time>>
      strip_time_with_nodes_time_;

  std::string last_frame_id_;
  unsigned int merge_cnt_;
  unsigned int sub_cnt_;
  int mode_;
  bool shutdown_flag_;
  cv::Mat img_test_;
  absl::optional<::cartographer::transform::Rigid3d> base2tracking_;
  cartographer::transform::Rigid3d last_base2odom_;
  std::vector<Eigen::Vector2d> origin_strip_points_;
  std::vector<geometry_msgs::Point32> filtered_origin_strip_points_;
  Eigen::Vector2d max_box_;
};
} // namespace map_manager
#endif // MAPMAPPING_H
