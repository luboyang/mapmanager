/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VOXEL_FILTER_H_
#define VOXEL_FILTER_H_

#include <sensor_msgs/PointCloud.h>

#include <Eigen/Eigen>
#include <bitset>
#include <cmath>

#include "parallel_hashmap/phmap.h"
namespace map_manager
{
class VoxelFilter
{
  public:
  // 'size' is the length of a voxel edge.
  explicit VoxelFilter(float size) : resolution_(size) {}

  VoxelFilter(const VoxelFilter &) = delete;
  VoxelFilter &operator=(const VoxelFilter &) = delete;

  // Returns a voxel filtered copy of 'point_cloud'.
  sensor_msgs::PointCloud Filter(const sensor_msgs::PointCloud &point_cloud)
  {
    sensor_msgs::PointCloud results;
    results.header = point_cloud.header;
    for (const auto &point : point_cloud.points)
    {
      auto it_inserted =
          voxel_set_.insert(IndexToKey(GetCellIndex(Eigen::Vector3f(point.x, point.y, point.z))));
      if (it_inserted.second)
      {
        results.points.push_back(point);
      }
    }
    return results;
  }
  std::vector<geometry_msgs::Point32> Filter(const std::vector<geometry_msgs::Point32> &point_cloud)
  {
    std::vector<geometry_msgs::Point32> results;
    for (const auto &point : point_cloud)
    {
      auto it_inserted =
          voxel_set_.insert(IndexToKey(GetCellIndex(Eigen::Vector3f(point.x, point.y, point.z))));
      if (it_inserted.second)
      {
        results.push_back(point);
      }
    }
    return results;
  }

  std::vector<geometry_msgs::Point32> Filter(const std::vector<Eigen::Vector2d> &point_cloud)
  {
    std::vector<geometry_msgs::Point32> results;
    for (const auto &point : point_cloud)
    {
      auto it_inserted =
          voxel_set_.insert(IndexToKey(GetCellIndex(Eigen::Vector3f(point.x(), point.y(), 0))));
      if (it_inserted.second)
      {
        geometry_msgs::Point32 temp;
        temp.x = point.x();
        temp.y = point.y();
        temp.z = 0;
        results.push_back(temp);
      }
    }
    return results;
  }

  private:
  using KeyType = std::bitset<3 * 32>;

  static KeyType IndexToKey(const Eigen::Array3i &index)
  {
    KeyType k_0(static_cast<uint32_t>(index[0]));
    KeyType k_1(static_cast<uint32_t>(index[1]));
    KeyType k_2(static_cast<uint32_t>(index[2]));
    return (k_0 << 2 * 32) | (k_1 << 1 * 32) | k_2;
  }
  Eigen::Array3i GetCellIndex(const Eigen::Vector3f &point) const
  {
    Eigen::Array3f index = point.array() / resolution_;
    return Eigen::Array3i(std::lround(index.x()), std::lround(index.y()), std::lround(index.z()));
  }

  float resolution_;
  phmap::flat_hash_set<KeyType> voxel_set_;
};
} // namespace map_manager
#endif // VOXEL_FILTER_H_
