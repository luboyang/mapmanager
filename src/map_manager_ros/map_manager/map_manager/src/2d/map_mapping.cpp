/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "2d/map_mapping.h"

#include <malloc.h>
#include <ros/package.h>

#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/mapping/2d/probability_grid_range_data_inserter_2d.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/submaps.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/timestamped_transform.h>
#include <cartographer/transform/transform.h>
#include <cartographer_ros/node.h>
#include <cartographer_ros/node_options.h>
#include <cartographer_ros/ros_map.h>

#include "2d/voxel_filter.h"
#include "line_fit/line_fit.h"
#include "map_manager_ulitity.h"
#include "map_tools/get_map_from_pbstream.h"
using namespace cv;
using namespace std;
using namespace cartographer_ros;
using namespace cartographer;
using namespace cartographer::mapping;
using ::cartographer::io::PaintSubmapSlicesResult;
using ::cartographer::io::SubmapSlice;
using ::cartographer::mapping::SubmapId;
namespace map_manager
{
std::unique_ptr<cartographer::mapping::GridInterface>
CreateGrid(const Eigen::Vector2f &origin, mapping::ValueConversionTables *conversion_tables)
{

  constexpr int kInitialSubmapSize = 20;
  float resolution = 0.05;
  return absl::make_unique<mapping::ProbabilityGrid>(
      mapping::MapLimits(resolution,
                         origin.cast<double>() +
                             0.5 * kInitialSubmapSize * resolution * Eigen::Vector2d::Ones(),
                         mapping::CellLimits(kInitialSubmapSize, kInitialSubmapSize)),
      conversion_tables);
}

MapMapping::MapMapping() : last_frame_id_("")
{
  shutdown_flag_ = false;
  bool use_strip_detect = false;
  ::ros::param::get("/cartographer_node/use_strip_detect", use_strip_detect);
  string submap_query_topic, submap_list_topic, submap_images_topic, submap_server_name;
  submap_list_topic = "/submap_list";
  submap_images_topic = "/map_manager_mapping/submap_images";
  submap_query_topic = "/submap_query";
  submap_server_name = "/submap_server";
  submap_query_client_ = nh_.serviceClient<cartographer_ros_msgs::SubmapQuery>(submap_query_topic);
  shutdown_carto_client_ =
      nh_.serviceClient<cartographer_ros_msgs::FinishMapping>("/shutdown_cartographer2");
  submap_server_client_ =
      nh_.serviceClient<cartographer_ros_msgs::SubmapServer>(submap_server_name);

  submap_images_publisher_ =
      nh_.advertise<cartographer_ros_msgs::SubmapImages>(submap_images_topic, 100);
  strip_detect_result_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/strip_points_in_map", 10);
  submap_list_subscriber_ =
      nh_.subscribe(submap_list_topic, 3, &MapMapping::HandleSubmapList, this);

  if (use_strip_detect)
  {
    strip_detect_result_sub_ =
        nh_.subscribe("/base_point", 3, &MapMapping::HandleStripDetectResult, this);
    node_poses_sub_ = nh_.subscribe("/cartographer_ros/trajectory_node_poses", 3,
                                    &MapMapping::HandleNodePoses, this);
  }
  // global_map_publisher_ =
  // nh_.advertise<cartographer_ros_msgs::GlobalMap>("/map_manager_mapping/global_map",10);

  wall_timers_.push_back(
      nh_.createWallTimer(ros::WallDuration(0.50), &MapMapping::DrawAndPublish, this));
  servers_.push_back(nh_.advertiseService("/map_manager_mapping/save_submaps",
                                          &MapMapping::saveSubmapServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_mapping/load_mapping_submaps",
                                          &MapMapping::loadMappingSubmapsServer, this));
  servers_.push_back(
      nh_.advertiseService("/map_manager_mapping/save_map", &MapMapping::saveMapServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_mapping/shutdown_map_manager_topics",
                                          &MapMapping::shutdownTopicServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_mapping/save_map_from_pbstream",
                                          &MapMapping::saveMapFromPbstream, this));
  servers_.push_back(nh_.advertiseService("/map_manager_mapping/save_blank_map",
                                          &MapMapping::saveBlankMapServer, this));
  servers_.push_back(
      nh_.advertiseService("/shutdown_cartographer", &MapMapping::shutdownCarto, this));
  int merge_cnt = 10000;
  if (!::ros::param::get("~merge_cnt", merge_cnt))
  {
    LOG(ERROR) << ("can not get merge_cnt_!");
    // 		exit(0);
  }
  merge_cnt_ = merge_cnt;
  sub_cnt_ = 0;
  mode_ = 0;
}

MapMapping::~MapMapping() {}

const Eigen::Vector2d MapMapping::getMax(const cartographer::transform::Rigid3d &slice_pose,
                                         const cartographer::transform::Rigid3d &local_pose)
{
  Eigen::Vector3d trans = slice_pose.translation();
  Eigen::Vector3d max1 = local_pose * trans;
  Eigen::Vector2d max;
  max(0) = max1(0);
  max(1) = max1(1);
  return max;
}

void MapMapping::HandleSubmapList(const cartographer_ros_msgs::SubmapList::ConstPtr &msg)
{
  absl::MutexLock locker(&mutex_);
  // Keep track of submap IDs that don't appear in the message anymore.
  interpretateSubmapList(msg, submap_slices_, submap_images_, &submap_query_client_);
  last_timestamp_ = msg->header.stamp;
  last_frame_id_ = msg->header.frame_id;
}

void MapMapping::HandleStripDetectResult(const map_manager_msgs::LinePoints::ConstPtr &msg)
{
  absl::MutexLock locker(&mutex_);
  std::map<int, std::vector<Eigen::Vector3d>> points;
  for (auto it : msg->left_points_in_base)
  {
    points[0].push_back(Eigen::Vector3d(it.position.x, it.position.y, it.position.z));
  }
  for (auto it : msg->right_points_in_base)
  {
    points[1].push_back(Eigen::Vector3d(it.position.x, it.position.y, it.position.z));
  }
  const common::Time time = cartographer_ros::FromRos(msg->header.stamp);
  points_with_stamp_[time] = points;
  {
    geometry_msgs::TransformStamped base2odom_tf;
    try
    {
      base2odom_tf = tfBuffer_.lookupTransform("odom", "base_footprint", ros::Time(0));
      Eigen::Vector3d translation(base2odom_tf.transform.translation.x,
                                  base2odom_tf.transform.translation.y,
                                  base2odom_tf.transform.translation.z);

      Eigen::Quaterniond rotation(
          base2odom_tf.transform.rotation.w, base2odom_tf.transform.rotation.x,
          base2odom_tf.transform.rotation.y, base2odom_tf.transform.rotation.z);
      transform::Rigid3d base2odom(translation, rotation);
      transform::Rigid3d relative_pose = last_base2odom_.inverse() * base2odom;
      if (relative_pose.translation().norm() < 0.1 && transform::GetAngle(relative_pose) < 0.2)
        return;
      last_base2odom_ = base2odom;
    }
    catch (tf::TransformException ex)
    {
      LOG(WARNING) << ex.what();
    }
  }

  strip_time_with_nodes_time_[time].push_back(time);
}

void MapMapping::HandleNodePoses(const cartographer_ros_msgs::TrajectoryNodePoses::ConstPtr &msg)
{
  absl::MutexLock locker(&mutex_);
  for (const cartographer_ros_msgs::NodePose &node_pose_id : msg->node_poses)
    node_poses_with_time_[cartographer_ros::FromRos(node_pose_id.header.stamp)] =
        ToRigid3d(node_pose_id.pose);
  if (!base2tracking_.has_value())
  {
    geometry_msgs::TransformStamped base2tracking_tf;
    try
    {
      base2tracking_tf =
          tfBuffer_.lookupTransform(msg->header.frame_id, "base_footprint", ros::Time(0));
      Eigen::Vector3d translation(base2tracking_tf.transform.translation.x,
                                  base2tracking_tf.transform.translation.y,
                                  base2tracking_tf.transform.translation.z);

      Eigen::Quaterniond rotation(
          base2tracking_tf.transform.rotation.w, base2tracking_tf.transform.rotation.x,
          base2tracking_tf.transform.rotation.y, base2tracking_tf.transform.rotation.z);
      base2tracking_ = transform::Rigid3d(translation, rotation);
    }
    catch (tf::TransformException ex)
    {
      LOG(WARNING) << ex.what();
      return;
    }
  }
  if (node_poses_with_time_.empty()) return;
  sensor_msgs::PointCloud pcd;
  for (auto &it : strip_time_with_nodes_time_)
  {
    if (it.first > node_poses_with_time_.rbegin()->first ||
        it.first < node_poses_with_time_.begin()->first)
      continue;
    if (it.second.size() != 2)
    {
      std::map<common::Time, transform::Rigid3d>::iterator right =
          node_poses_with_time_.upper_bound(it.first);
      auto left = right;
      left--;
      it.second.resize(2);
      if (left != node_poses_with_time_.end() && right != node_poses_with_time_.end())
      {
        it.second[0] = (left->first);
        it.second[1] = (right->first);
      }
    }

    const common::Time &t11 = it.second[0];
    const common::Time &t22 = it.second[1];
    cartographer::transform::Rigid3d transform =
        cartographer::transform::Interpolate(
            cartographer::transform::TimestampedTransform{t11, node_poses_with_time_[t11]},
            cartographer::transform::TimestampedTransform{t22, node_poses_with_time_[t22]},
            it.first)
            .transform;
    for (int i = 0; i < 2; i++)
    {
      for (auto &point : points_with_stamp_[it.first][i])
      {
        Eigen::Vector3d temp = transform * base2tracking_.value() * point;
        geometry_msgs::Point32 p;
        p.x = temp(0);
        p.y = temp(1);
        p.z = temp(2);
        pcd.points.push_back(p);
      }
    }
  }

  if (strip_detect_result_pub_.getNumSubscribers() > 0)
  {
    pcd.header.stamp = ros::Time::now();
    pcd.header.frame_id = "map";
    if (!filtered_origin_strip_points_.empty())
      pcd.points.insert(pcd.points.end(), filtered_origin_strip_points_.begin(),
                        filtered_origin_strip_points_.end());
    strip_detect_result_pub_.publish(VoxelFilter(0.1).Filter(pcd));
  }
}

void MapMapping::DrawAndPublish(const ros::WallTimerEvent &unused_timer_event)
{

  absl::MutexLock locker(&mutex_);
  if (submap_slices_.empty() || last_frame_id_.empty())
  {
    LOG(WARNING) << "submap_slices size: " << submap_slices_.size()
                 << "   last_frame_id_: " << last_frame_id_;
    return;
  }
  if (shutdown_flag_)
  {
    LOG(INFO) << "shutdown map-manager node! ";
    return;
  }
  CHECK(submap_images_.size() == submap_slices_.size());
  unsigned int i = 0;
  for (auto &slice_pair : submap_slices_)
  {
    SubmapId id = slice_pair.first;
    auto &slice_temp = slice_pair.second;
    bool is_old = true;

    //     sensor_msgs::CompressedImage tmp;
    cartographer_ros_msgs::SubmapImageEntry &submap_image = submap_images_[slice_pair.first];

    cartographer::transform::Rigid3d pose_in_map = slice_temp.pose * slice_temp.slice_pose;
    submap_image.pose_in_map = ToGeometryMsgPose(pose_in_map);
    submap_image.resolution = slice_temp.resolution;

    if (submap_image.submap_version != slice_temp.version)
    {
      submap_image.submap_version = slice_temp.version;
      is_old = false;
    }
    else
    {
      if (i >= (submap_images_.size() - 3) || submap_images_.size() <= 3)
        is_old = false;
      else
      {
        if (slice_temp.cairo_data.size() > 1)
        {
          std::vector<uint32_t> empty_temp{1};
          submap_slices_[id].cairo_data.swap(empty_temp);
          std::vector<uint8_t> empty_temp2{1};
          submap_images_[id].image.data.swap(empty_temp2);
          //           std::vector<uint8_t>().swap(submap_image.image.data);
          submap_slices_[id].surface.reset(nullptr);
          submap_slices_[id].cairo_data.clear();
          submap_slices_[id].surface = std::move(
              cartographer::io::DrawTexture({1}, {1}, 1, 1, &submap_slices_[id].cairo_data));
        }
      }
    }

    if (is_old)
    {
      i++;
      continue;
    }
    // get mat map
    double resolution = slice_temp.resolution;
    auto painted_slice = PaintSubmapSlice(slice_temp, resolution);
    Mat img = getMatMap(painted_slice, PUB_MAP);
    std_msgs::Header header;
    header.stamp = last_timestamp_;
    header.frame_id = last_frame_id_;

    if (img.rows == 0 && img.cols == 0) exit(0);
    sensor_msgs::CompressedImagePtr msg_ptr =
        cv_bridge::CvImage(header, "bgra8", img).toCompressedImageMsg(cv_bridge::PNG);
    submap_image.image = *msg_ptr;
    i++;

    // 		LOG(INFO) <<"submap_slices_.size(): " <<submap_slices_.size()  <<"  id: " <<
    // submap_pair.submap_index << "   version: " <<submap_pair.submap_version;
  }

  cartographer_ros_msgs::SubmapImages submap_images_msg;
  submap_images_msg.header.stamp = last_timestamp_;
  submap_images_msg.submap_images.reserve(submap_images_.size());
  for (auto pair : submap_images_)
  {
    submap_images_msg.submap_images.push_back(pair.second);
  }
  if (0)
  {
    cartographer_ros_msgs::SubmapImages imgs_tmp;
    cartographer_ros_msgs::SubmapImageEntry img;
    double resolution_tmp = 0.05;
    auto painted_slices = PaintSubmapsSlices(submap_slices_, resolution_tmp);
    Mat img_tmp = getMatMap(painted_slices, PUB_MAP);
    // 		imshow("66", img_tmp);waitKey(2);
    img.image = *cv_bridge::CvImage(submap_images_msg.header, "bgra8", img_tmp)
                     .toCompressedImageMsg(cv_bridge::PNG);
    img.resolution = resolution_tmp;
    img.submap_index = 0;
    img.trajectory_id = 0;
    img.submap_version = 0;
    LOG(INFO) << endl
              << -img_tmp.cols + painted_slices.origin.x() << ", "
              << -img_tmp.rows + painted_slices.origin.y() << endl
              << img_tmp.cols << ", " << img_tmp.rows << endl
              << painted_slices.origin.x() << ", " << painted_slices.origin.y();
    // 		img.pose_in_map = ToGeometryMsgPose(submap_slices_.begin()->second.pose *
    // submap_slices_.begin()->second.slice_pose); 		cout <<"pose in map: "<<
    // img.pose_in_map.position.x << ", " << img.pose_in_map.position.y <<endl;

    // 		cartographer::transform::Rigid3d tmp = submap_slices_.begin()->second.pose *
    // submap_slices_.begin()->second.slice_pose;
    LOG(INFO) << endl
              << "pose: " << submap_slices_.begin()->second.pose << endl
              << "slice pose: " << submap_slices_.begin()->second.slice_pose;
    // 		cartographer::io::SubmapSlice tmp;
    // 		LOG(INFO) << tmp ;
    img.pose_in_map.position.x = (img_tmp.rows - painted_slices.origin.x()) * resolution_tmp;
    img.pose_in_map.position.y = (painted_slices.origin.y()) * resolution_tmp;
    imgs_tmp.submap_images.push_back(img);
    submap_images_publisher_.publish(imgs_tmp);
  }
  else
  {
    // 		double resolution_tmp = 0.05;
    // 		auto painted_slices  = PaintSubmapsSlices(submap_slices_, resolution_tmp);
    // 		Mat img_tmp = getMatMap(painted_slices,PUB_MAP);
    submap_images_publisher_.publish(submap_images_msg);
    // 		LOG(INFO) <<"box:" <<img_tmp.rows-painted_slices.origin.x() <<","
    // <<painted_slices.origin.y() ;
  }
}

void MapMapping::saveToXml(const string &path, const int &id)
{
  boost::property_tree::ptree p_map;
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  std::string folderstring_submap = path + std::to_string(id) + "/";
  string system_command = "mkdir -p " + folderstring_submap;
  LOG(INFO) << system_command;
  int status = system(system_command.c_str());
  checkSystemStatus(system_command, status);
  CHECK(status != -1);

  string str = "/.";
  const char *compare_reslut;
  compare_reslut = strstr(path.c_str(), str.c_str());
  ProbabilityGridInfo info;
  if (compare_reslut == NULL)
    info = merge_probability_infos_[id];
  else
    info = probability_infos_[id];
  p_map.put("id", id);

  cartographer::transform::Rigid3d global_pose = info.global_pose;
  Eigen::Vector3d translation = global_pose.translation();
  Eigen::Quaterniond quaternion = global_pose.rotation();
  string data;
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(translation[i]) + " ";
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(quaternion.coeffs()[i]) + " ";
  data = data + std::to_string(quaternion.coeffs()[3]);
  p_map.put("pose", data);
  data.clear();

  cartographer::transform::Rigid3d local_pose = info.local_pose;
  translation = local_pose.translation();
  quaternion = local_pose.rotation();
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(translation[i]) + " ";
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(quaternion.coeffs()[i]) + " ";
  data = data + std::to_string(quaternion.coeffs()[3]);
  p_map.put("local_pose", data);
  data.clear();

  p_map.put("num_range_data", info.num_range_data);

  p_map.put("finished", info.finished);

  boost::property_tree::ptree p_probability_grid;
  uint num_x_cells, num_y_cells;

  p_probability_grid.put("resolution", info.resolution);
  data = std::to_string(info.max[0]) + " " + std::to_string(info.max[1]);
  p_probability_grid.put("max", data);
  data.clear();
  num_x_cells = info.num_cells[0];
  num_y_cells = info.num_cells[1];
  data = std::to_string(num_x_cells) + " " + std::to_string(num_y_cells);
  p_probability_grid.put("num_cells", data);
  data.clear();
  data = std::to_string(0) + " " + std::to_string(0) + " " + std::to_string(num_x_cells - 1) + " " +
         std::to_string(num_y_cells - 1);
  p_probability_grid.put("known_cells_box", data);
  p_map.add_child("probability_grid", p_probability_grid);
  boost::property_tree::write_xml(folderstring_submap + "data.xml", p_map, std::locale(), setting);
}

void MapMapping::saveAndMerge(const string &full_path, const string &hide_full_path)
{
  // 	for(auto& it :submap_images_msg_.submap_images)
  // 	{
  // 		LOG(INFO) <<"id: " << it.submap_index << "  " << it.pose_in_map.position ;
  // 	}

#if 0
	boost::property_tree::ptree pt;
	boost::property_tree::read_xml(full_path + "map.xml", pt);
	if (pt.empty())
	{
		string error_message = "Load "+ full_path+"map.xml failed!";
		LOG(INFO) << error_message ;
		return;
	}
	int node_count = pt.get<int>("map.property.node_count");
	std::string submap_filepath = full_path + "frames/";
	for(int i = 0; i < node_count ; i ++)
	{
		string sub_path = submap_filepath + to_string(i);
		cartographer::transform::Rigid3d local_pose = getPose(sub_path,true);
		cartographer::transform::Rigid3d global_pose = getPose(sub_path,false);
		Eigen::Vector2d max = getMax(global_pose,local_pose);
		local_poses_.push_back(local_pose);
		global_poses_.push_back(global_pose);
		maxs_.push_back(max);
	}
#endif
  // 	for(auto& pair:submap_slices_)
  // 	{
  // 		SubmapId submap_id = pair.first;
  // 		cartographer_ros_msgs::SubmapServer src;
  // 		src.request.submap_index = submap_id.submap_index;
  // 		src.request.trajectory_id = submap_id.trajectory_id;
  //
  // 		if (!submap_server_client_.call(src))
  //     {
  //       LOG(ERROR) << ("Failed to call service submap_server!");
  //       continue;
  //     }
  // 		cartographer_ros_msgs::SubmapServer::Response response = src.response;
  // 		cartographer::transform::Rigid3d local2global_pose = ToRigid3d(response.pose);
  // 		cartographer::transform::Rigid3d global_pose = pair.second.pose;
  // 		cartographer::transform::Rigid3d local_pose_inv = global_pose.inverse()*local2global_pose;
  // 		local_poses_.push_back(local_pose_inv.inverse());
  // 	}

  // save to origin floder
  int sub_folder_cnt = 0;
  vector<ProbabilityGridInfo>().swap(probability_infos_);
  LOG(INFO) << "start saving submaps!";

  for (auto &slice_pair : save_submap_slices_)
  {
    auto &slice_temp = slice_pair.second;
    auto painted_slice = PaintSubmapSlice(slice_temp, slice_temp.resolution);
    Mat save_img = getMatMap(painted_slice, SAVE_MAP);

    string sub_folder_path = hide_full_path + "frames/";
    // 		createPath(sub_folder_path);
    ProbabilityGridInfo info;
    info.global_pose = slice_temp.pose;
    info.local_pose = local_poses_[sub_folder_cnt];
    Eigen::Vector2d max_temp = getMax(slice_temp.slice_pose, local_poses_[sub_folder_cnt]);
    cartographer::transform::Rigid3d pt_tmp;
    pt_tmp.Identity();
    cartographer::transform::Rigid3d::Vector tmp =
        info.global_pose * (info.local_pose.inverse()) *
        cartographer::transform::Rigid3d::Vector(max_temp(0), max_temp(1), 0.);
    // 		LOG(INFO) << "save id: " << slice_pair.first.submap_index << "   " << tmp;
    info.max[0] = max_temp(0);
    info.max[1] = max_temp(1);
    info.resolution = slice_temp.resolution;
    info.num_range_data = slice_temp.version;
    info.num_cells[0] = save_img.cols;
    info.num_cells[1] = save_img.rows;
    probability_infos_.push_back(info);
    saveToXml(sub_folder_path, sub_folder_cnt);
    string img_name = sub_folder_path + to_string(sub_folder_cnt) + "/probability_grid.png";
    string origin_img_name = sub_folder_path + to_string(merge_probability_infos_.size() - 1) +
                             "/probability_grid_origin.png";
    LOG(INFO) << "save img of " << img_name;
    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(0);
    imwrite(img_name, save_img, compression_params);
    imwrite(origin_img_name, save_img, compression_params);
    Mat save_img1 = getMatMap(painted_slice, SHOW_MAP);
    imwrite(sub_folder_path + to_string(sub_folder_cnt) + "/map.png", save_img1);
    sub_folder_cnt++;
  }

  LOG(INFO) << "Start saving save and show map!";
  saveImageFromPbstream(full_path + "map.pbstream", full_path);
  LOG(INFO) << "Save show map done!";
  {
    double resolution = 0.05;
    auto result = PaintSubmapsSlices(save_submap_slices_, resolution);
    string system_command = "mkdir -p " + full_path + "map_server";
    LOG(INFO) << system_command;
    int status = system(system_command.c_str());
    checkSystemStatus(system_command, status);
    CHECK(status != -1);
    ::cartographer::io::StreamFileWriter pgm_writer(full_path + "map_server/map.pgm");
    ::cartographer::io::Image image(std::move(result.surface));
    image.Rotate90DegreesClockwise();
    image.Rotate90DegreesClockwise();
    image.Rotate90DegreesClockwise();
    WritePgm(image, resolution, &pgm_writer);
    LOG(INFO) << result.origin.x() * resolution << ", " << result.origin.y() * resolution << ", "
              << image.height() * resolution << ", " << image.width() * resolution;
    const Eigen::Vector2d origin(-result.origin.x() * resolution, result.origin.y() * resolution);
    ::cartographer::io::StreamFileWriter yaml_writer(full_path + "map_server/map.yaml");
    const std::string output =
        "image: " + pgm_writer.GetFilename() + "\n" + "resolution: " + std::to_string(resolution) +
        "\n" + "origin: [" + std::to_string(origin.x()) + ", " + std::to_string(origin.y()) + ", " +
        std::to_string(-M_PI / 2) + "]\nnegate: 0\noccupied_thresh: 0.65\nfree_thresh: 0.196\n";
    yaml_writer.Write(output.data(), output.size());
  }
  LOG(INFO) << "Save map_server_files done!";
  return;

  LOG(INFO) << "start saving show map!";
  {
    ProbabilityGridInfo merge_info;
    preHandleMergeMaps(save_submap_slices_, probability_infos_, merge_info, 0);
    double resolution = 0.05;
    auto painted_slice = PaintSubmapsSlices(save_submap_slices_, resolution);
    Mat save_img = getMatMap(painted_slice, SHOW_MAP);
    merge_info.resolution = resolution;
    merge_info.num_cells[0] = save_img.cols;
    merge_info.num_cells[1] = save_img.rows;
    // 		merge_probability_infos_.push_back(merge_info);

    string sub_folder_path = full_path;
    // // 		saveToXml(full_path,merge_probability_infos_.size()-1);
    string img_name = sub_folder_path + "/map.png";
    LOG(INFO) << "save img of " << img_name;
    imwrite(img_name, save_img);

    // waiting for testing
    {
      boost::property_tree::ptree p_map, p_top;
      std::string data;
      const int width = cairo_image_surface_get_width(painted_slice.surface.get());
      const int height = cairo_image_surface_get_height(painted_slice.surface.get());
      data = to_string(height);
      p_map.put("width", data);
      data.clear();
      data = to_string(width);
      p_map.put("height", data);
      data.clear();
      data = to_string(resolution);
      p_map.put("resolution", data);
      data.clear();
      double x = (width - painted_slice.origin.x());
      double y = (painted_slice.origin.y());
      cartographer::transform::Rigid3d::Vector translation(y, x, 0);
      cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2, sqrt(2) / 2, 0);
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(quaternion.coeffs()[i]) + " ";
      data = data + std::to_string(quaternion.coeffs()[3]);
      p_map.put("pose", data);
      data.clear();
      p_top.add_child("mapPng", p_map);

      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      boost::property_tree::write_xml(sub_folder_path + "map_data.xml", p_top, std::locale(),
                                      setting);
    }
  }
  // save to show floder
  LOG(INFO) << "start saving merge map!";
  for (unsigned int i = 0; i < save_submap_slices_.size(); i += merge_cnt_)
  {
    std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> submap_slices;

    vector<ProbabilityGridInfo> infos;
    if (i + merge_cnt_ < save_submap_slices_.size())
    {
      unsigned int j = 0;
      for (auto &pair : save_submap_slices_)
      {
        if (j < i)
        {
          j++;
          continue;
        }
        else if (j >= i + merge_cnt_)
          break;
        SubmapSlice &slice = submap_slices[pair.first];
        slice.height = pair.second.height;
        slice.cairo_data = pair.second.cairo_data;
        slice.pose = pair.second.pose;
        slice.slice_pose = pair.second.slice_pose;
        slice.surface = std::move(pair.second.surface);
        slice.version = pair.second.version;
        slice.width = pair.second.width;
        slice.resolution = pair.second.resolution;
        j++;
      }
      infos.insert(infos.end(), probability_infos_.begin() + i,
                   probability_infos_.begin() + i + merge_cnt_);
    }
    else
    {
      unsigned int j = 0;
      for (auto &pair : save_submap_slices_)
      {
        if (j < i)
        {
          j++;
          continue;
        }
        SubmapSlice &slice = submap_slices[pair.first];
        slice.height = pair.second.height;
        slice.cairo_data = pair.second.cairo_data;
        slice.pose = pair.second.pose;
        slice.slice_pose = pair.second.slice_pose;
        slice.surface = std::move(pair.second.surface);
        slice.version = pair.second.version;
        slice.width = pair.second.width;
        slice.resolution = pair.second.resolution;
        j++;
      }
      infos.insert(infos.end(), probability_infos_.begin() + i, probability_infos_.end());
    }
    ProbabilityGridInfo merge_info;
    preHandleMergeMaps(submap_slices, infos, merge_info, 0);
    double resolution = 0.05;
    auto painted_slice = PaintSubmapsSlices(submap_slices, resolution);

    Mat save_img = getMatMap(painted_slice, SAVE_MAP);
    merge_info.resolution = resolution;
    merge_info.num_cells[0] = save_img.cols;
    merge_info.num_cells[1] = save_img.rows;
    merge_info.max[0] = (save_img.rows - painted_slice.origin.x()) * resolution;
    merge_info.max[1] = painted_slice.origin.y() * resolution;
    merge_probability_infos_.push_back(merge_info);

    string sub_folder_path = full_path + "frames/";
    saveToXml(sub_folder_path, merge_probability_infos_.size() - 1);
    string img_name =
        sub_folder_path + to_string(merge_probability_infos_.size() - 1) + "/probability_grid.png";
    string origin_img_name = sub_folder_path + to_string(merge_probability_infos_.size() - 1) +
                             "/probability_grid_origin.png";
    LOG(INFO) << "save img of " << img_name;
    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(0);
    LOG(INFO) << endl
              << "******************************************************************************"
              << endl
              << (save_img.rows - painted_slice.origin.x()) * resolution << ", "
              << painted_slice.origin.y() * resolution << endl
              << save_img.cols << ", " << save_img.rows << endl
              << painted_slice.origin.x() << ", " << painted_slice.origin.y() << endl
              << merge_info.max[0] << ", " << merge_info.max[1] << endl
              << "******************************************************************************"
              << endl;

    imwrite(img_name, save_img, compression_params);
    imwrite(origin_img_name, save_img, compression_params);
    // move back surface to submap_slice_
    // 		for(auto& pair : submap_slices)
    // 		{
    // 			std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice>::iterator it;
    // 			it = submap_slices_.find(pair.first);
    // 			it->second.surface = std::move(pair.second.surface);
    // 		}
  }
  // TODO save a new constraints
  {
  }
}

void MapMapping::saveMapXml(const string &full_path,
                            const map<SubmapId, vector<SubmapId>> &constraints,
                            std::map<cartographer::mapping::SubmapId, int> id_num,
                            std::map<int, cartographer_ros_msgs::SubmapID> num_id)
{
  ::cartographer::transform::Rigid3d::Vector translation;
  ::cartographer::transform::Rigid3d::Quaternion quaternion;
  std::string data;
  boost::property_tree::ptree p_top;
  boost::property_tree::ptree p_map;
  boost::property_tree::ptree p_node_list;
  int submap_cnt = constraints.size();
  for (auto &constraint : constraints)
  {
    SubmapId id = constraint.first;
    if (id_num.find(id) == id_num.end())
    {
      continue;
    }
    boost::property_tree::ptree p_node;
    p_node.put("id", id_num.find(id)->second);

    boost::property_tree::ptree p_neighbour_list;
    std::set<int> connected_ids;
    for (auto &pair : constraint.second)
    {
      std::map<cartographer::mapping::SubmapId, int>::iterator it;
      it = id_num.find(pair);
      connected_ids.insert(it->second);
    }
    for (auto &pair : connected_ids)
    {
      boost::property_tree::ptree p_neighbour;
      p_neighbour.put("id", pair);
      //       LOG(WARNING) << pair <<", " << num_id.find(pair)->second;
      cartographer_ros_msgs::SubmapID msg_id = num_id.find(pair)->second;
      SubmapId connect_id(msg_id.trajectory_id, msg_id.submap_index);

      ::cartographer::transform::Rigid3d neighbour2current =
          cartographer::transform::Rigid3d::Identity();
      if (save_submap_slices_.find(id) != save_submap_slices_.end() &&
          save_submap_slices_.find(connect_id) != save_submap_slices_.end())
        neighbour2current = save_submap_slices_.find(id)->second.pose.inverse() *
                            save_submap_slices_.find(connect_id)->second.pose;
      translation = neighbour2current.translation();
      quaternion = neighbour2current.rotation();
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      data = data + std::to_string(quaternion.w()) + " ";
      data = data + std::to_string(quaternion.x()) + " ";
      data = data + std::to_string(quaternion.y()) + " ";
      data = data + std::to_string(quaternion.z());
      p_neighbour.put("transform", data);
      data.clear();
      p_neighbour.put("reachable", true);

      p_neighbour_list.add_child("neighbour", p_neighbour);
    }

    p_node.add_child("neighbour_list", p_neighbour_list);

    p_node_list.add_child("node", p_node);
  }
  p_map.add_child("node_list", p_node_list);
  boost::property_tree::ptree p_property;
  p_property.put("node_count", submap_cnt);

  p_map.add_child("property", p_property);

  p_top.add_child("map", p_map);
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(full_path + "map.xml", p_top, std::locale(), setting);
}

void MapMapping::saveLandmarkXml(const string &full_path, const string &hide_full_path,
                                 const vector<MapInterface::LandmarkInfo> &landmarks)
{
  std::string data;
  boost::property_tree::ptree p_landmark_info;
  boost::property_tree::ptree p_neighbour_list;

  for (const auto &landmark : landmarks)
  {
    cartographer::transform::Rigid3d::Vector translation;
    cartographer::transform::Rigid3d::Quaternion quaternion;
    p_landmark_info.put("ns", landmark.ns);
    p_landmark_info.put("id", landmark.id);
    p_landmark_info.put("pole_radius", landmark.pole_radius);
    p_landmark_info.put("visible", landmark.visible);
    cartographer::transform::Rigid3d landmark_pose = landmark.pose;
    translation = landmark_pose.translation();
    quaternion = landmark_pose.rotation();
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(translation[i]) + " ";
    for (int i = 0; i < 4; i++)
      data = data + std::to_string(quaternion.coeffs()[i]) + " ";
    p_landmark_info.put("transform", data);
    data.clear();
    data = std::to_string(landmark.translation_in_image(0)) + " " +
           std::to_string(landmark.translation_in_image(1));
    p_landmark_info.put("translation_in_image", data);
    p_neighbour_list.add_child("landmark", p_landmark_info);
    data.clear();
  }
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(full_path + "landmark.xml", p_neighbour_list, std::locale(),
                                  setting);
  boost::property_tree::write_xml(hide_full_path + "landmark.xml", p_neighbour_list, std::locale(),
                                  setting);
}

void MapMapping::preHandleMergeMaps(map<SubmapId, SubmapSlice> &submap_slices,
                                    const vector<MapInterface::ProbabilityGridInfo> &infos,
                                    MapInterface::ProbabilityGridInfo &merge_info,
                                    const int &mid_num)
{
  assert(submap_slices.size() == infos.size());

  map<SubmapId, SubmapSlice>::iterator it = submap_slices.begin();
  map<SubmapId, SubmapSlice>::iterator it_g = submap_slices.begin();
  vector<ProbabilityGridInfo>::const_iterator it_l = infos.begin();
  for (int i = 0; i < mid_num; i++)
  {
    it_g++;
    it_l++;
  }
  cartographer::transform::Rigid3d init_global_pose = it_g->second.pose;
  cartographer::transform::Rigid3d init_global_pose_inv = init_global_pose.inverse();
  vector<ProbabilityGridInfo>::const_iterator it2 = infos.begin();
  // 	cartographer::transform::Rigid3d init_local_pose = it_l->local_pose;
  vector<double> reserve_x, reserve_y;
  int merge_num_range_data = 0;
  for (; it != submap_slices.end(); it++, it2++)
  {
    cartographer::transform::Rigid3d current_global_pose = it->second.pose;
    it->second.pose = init_global_pose_inv * current_global_pose;
    cartographer::transform::Rigid3d current_local_pose = it2->local_pose;
    double max_x = it2->max[0];
    double max_y = it2->max[1];
    // 之前旋转了图像，但是 sliace 里面没有旋转，所以 width heigeht 要反一下
    double width = it2->num_cells[1];
    double height = it2->num_cells[0];
    double current_resolution = it2->resolution;
    Eigen::Vector3d p0, p1, p2, p3;

    p0 =
        Eigen::Vector3d(max_x - height * current_resolution, max_y - width * current_resolution, 0);
    p1 = Eigen::Vector3d(max_x, max_y - width * current_resolution, 0);
    p2 = Eigen::Vector3d(max_x - height * current_resolution, max_y, 0);
    p3 = Eigen::Vector3d(max_x, max_y, 0);

    Eigen::Vector3d p0_global, p1_global, p2_global, p3_global;
    p0_global = init_global_pose_inv * current_global_pose * current_local_pose.inverse() * p0;
    p1_global = init_global_pose_inv * current_global_pose * current_local_pose.inverse() * p1;
    p2_global = init_global_pose_inv * current_global_pose * current_local_pose.inverse() * p2;
    p3_global = init_global_pose_inv * current_global_pose * current_local_pose.inverse() * p3;
    reserve_x.push_back(p0_global(0));
    reserve_x.push_back(p1_global(0));
    reserve_x.push_back(p2_global(0));
    reserve_x.push_back(p3_global(0));
    reserve_y.push_back(p0_global(1));
    reserve_y.push_back(p1_global(1));
    reserve_y.push_back(p2_global(1));
    reserve_y.push_back(p3_global(1));

    merge_num_range_data += it2->num_range_data;
  }
  auto merge_max_x = max_element(std::begin(reserve_x), std::end(reserve_x));
  auto merge_max_y = max_element(std::begin(reserve_y), std::end(reserve_y));
  Eigen::Vector3d new_max(*merge_max_x, *merge_max_y, 0);
  // 	Eigen::Vector3d new_point_in_local = init_global_pose * new_max;
  // 	merge_info.max[0] = new_point_in_local(0);
  // 	merge_info.max[1] = new_point_in_local(1);
  merge_info.local_pose = init_global_pose;
  merge_info.global_pose = init_global_pose;
  merge_info.num_range_data = merge_num_range_data;
  //
}

bool MapMapping::saveSubmapServer(cartographer_ros_msgs::SaveSubmapServer::Request &request,
                                  cartographer_ros_msgs::SaveSubmapServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "start saving map! ";
  string map_name = request.map_name;
  std::string full_path, hide_full_path;
  string hide_map_name = "." + request.map_name;
  getPath(request.map_name, full_path);
  createPath(full_path);
  getPath(hide_map_name, hide_full_path);
  createPath(hide_full_path);
  saveAndMerge(full_path, hide_full_path);
  return true;
}

bool MapMapping::loadMappingSubmapsServer(
    cartographer_ros_msgs::SubmapImagesServer::Request &request,
    cartographer_ros_msgs::SubmapImagesServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  if (submap_slices_.size() == 0)
  {
    response.error_message = "submaps size is zero!";
    return false;
  }
  response.error_message = "";
  for (auto &slice_pair : submap_slices_)
  {
    auto &slice_temp = slice_pair.second;
    cartographer_ros_msgs::SubmapImageEntry submap_pair;
    submap_pair.submap_index = slice_pair.first.submap_index;
    submap_pair.trajectory_id = slice_pair.first.trajectory_id;
    submap_pair.submap_version = slice_temp.version;
    // 		LOG(INFO) << slice_pair.first.submap_index ;
    // compare id with msg and ensure is necessary to update
    double resolution = slice_temp.resolution;
    auto painted_slice = PaintSubmapSlice(slice_temp, resolution);
    Mat img = getMatMap(painted_slice, PUB_MAP);
    std_msgs::Header header;
    header.stamp = last_timestamp_;
    header.frame_id = last_frame_id_;
    cv_bridge::CvImage img1 = cv_bridge::CvImage(header, "bgra8", img);

    sensor_msgs::CompressedImagePtr msg_ptr =
        cv_bridge::CvImage(header, "bgra8", img).toCompressedImageMsg(cv_bridge::PNG);

    submap_pair.resolution = resolution;

    cartographer::transform::Rigid3d pose_in_map = slice_temp.pose * slice_temp.slice_pose;

    submap_pair.pose_in_map = ToGeometryMsgPose(pose_in_map);
    submap_pair.image = *msg_ptr;
    response.submap_images.push_back(submap_pair);
  }
  return true;
}

bool MapMapping::saveMapServer(cartographer_ros_msgs::SaveMapServer::Request &request,
                               cartographer_ros_msgs::SaveMapServer::Response &response)
{
  LOG(WARNING) << "[" << ros::Time::now() << "] "
               << "save map!";
  absl::MutexLock locker(&mutex_);
  submap_images_publisher_.shutdown();
  for (auto it = submap_slices_.begin(); it != submap_slices_.end();)
  {
    it = submap_slices_.erase(it);
  }
  for (auto it = submap_images_.begin(); it != submap_images_.end();)
  {
    it = submap_images_.erase(it);
  }
  std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry>().swap(
      submap_images_);
  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice>().swap(
      submap_slices_);
  malloc_trim(0);
  LOG(WARNING) << "erase slices done!";
  std::string full_path, hide_full_path;
  string hide_map_name = "." + request.map_name;
  getPath(request.map_name, full_path);
  // 	createPath(full_path);
  getPath(hide_map_name, hide_full_path);
  // 	createPath(hide_full_path);
  // 	std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice>::iterator it =
  // submap_slices_.begin();
  std::map<SubmapId, std::vector<SubmapId>> constraints;
  int num = 0;
  std::map<cartographer::mapping::SubmapId, int> id_num1;
  std::map<int, cartographer_ros_msgs::SubmapID> num_id1;
  for (auto &it : request.submaps)
  {
    cartographer_ros_msgs::SaveSubmap submap;
    submap = it;
    cartographer::mapping::SubmapId id(submap.id.trajectory_id, submap.id.submap_index);
    cartographer_ros_msgs::SubmapID msg_id;
    msg_id.trajectory_id = id.trajectory_id;
    msg_id.submap_index = id.submap_index;
    ::cartographer::io::SubmapSlice &slice = save_submap_slices_[id];
    slice.pose = ToRigid3d(submap.pose_in_map);
    {
      auto fetched_textures = absl::make_unique<::cartographer::io::SubmapTextures>();
      fetched_textures->version = submap.submap_version;
      for (const auto &texture : submap.textures)
      {
        const std::string compressed_cells(texture.cells.begin(), texture.cells.end());
        fetched_textures->textures.emplace_back(::cartographer::io::SubmapTexture{
            ::cartographer::io::UnpackTextureData(compressed_cells, texture.width, texture.height),
            texture.width, texture.height, texture.resolution, ToRigid3d(texture.slice_pose)});
      }
      slice.version = fetched_textures->version;
      const auto fetched_texture = fetched_textures->textures.begin();
      slice.width = fetched_texture->width;
      slice.height = fetched_texture->height;
      slice.slice_pose = fetched_texture->slice_pose;
      slice.resolution = fetched_texture->resolution;
      slice.cairo_data.clear();

      slice.surface = ::cartographer::io::DrawTexture(
          fetched_texture->pixels.intensity, fetched_texture->pixels.alpha, fetched_texture->width,
          fetched_texture->height, &slice.cairo_data);
    }

    num_id1[num] = msg_id;
    id_num1[id] = num;
    num++;
    for (auto &constraint : submap.constraints)
    {
      constraints[id].push_back(SubmapId(constraint.trajectory_id, constraint.submap_index));
    }
    local_poses_.push_back(ToRigid3d(submap.pose_in_local));
  }
  saveAndMerge(full_path, hide_full_path);
  LOG(INFO) << "Start save hide_full_path map.xml!";
  saveMapXml(hide_full_path, constraints, id_num1, num_id1);
  // temp
  LOG(INFO) << "Start save full_path map.xml!";
  {
    std::map<SubmapId, std::vector<SubmapId>> constraints1;
    int ii;
    std::map<cartographer::mapping::SubmapId, int> id_num;
    std::map<int, cartographer_ros_msgs::SubmapID> num_id;
    if (constraints.size() % merge_cnt_ == 0)
      ii = constraints.size() / merge_cnt_;
    else
      ii = constraints.size() / merge_cnt_ + 1;
    for (int i = 0; i < ii; i++)
    {
      SubmapId id_temp(0, i);
      cartographer_ros_msgs::SubmapID msg_id;
      msg_id.trajectory_id = id_temp.trajectory_id;
      msg_id.submap_index = id_temp.submap_index;
      num_id[i] = msg_id;
      id_num[id_temp] = i;
      constraints1[id_temp].push_back(id_temp);
    }
    saveMapXml(full_path, constraints1, id_num, num_id);
  }

  LOG(INFO) << "Start save landmarks!";
  std::vector<LandmarkInfo> landmarks;
  double resolution = 0.05;
  for (auto &it : request.landmarks)
  {
    LandmarkInfo landmark;
    landmark.ns = it.ns;
    landmark.id = it.id;
    landmark.pole_radius = it.pole_radius;
    landmark.pose = ToRigid3d(it.tracking_from_landmark_transform);
    landmark.visible = it.visible;
    landmark.rotation_weight = it.rotation_weight;
    landmark.translation_weight = it.translation_weight;
    landmark.translation_in_image(1) = (max_box_[0] - landmark.pose.translation().x()) / resolution;
    landmark.translation_in_image(0) = (max_box_[1] - landmark.pose.translation().y()) / resolution;
    landmarks.push_back(landmark);
  }

  saveLandmarkXml(full_path, hide_full_path, landmarks);

  LOG(INFO) << "Start save map pixel image!";
  adjustMapPixel(full_path, 0, true);
  adjustMapPixel(full_path, 3, true);
  adjustMapPixel(full_path, 5, true);
  autoSetIgnoreArea(full_path);
  if (mode_ == 2)
  {
    LOG(INFO) << "[" << ros::Time::now() << "] "
              << "updating bool image! ";
    std::string origin_path;
    if (!::ros::param::get("/vtr/origin_path", origin_path))
    {
      LOG(ERROR) << "Can not get origin path!";
      return false;
    }
    LOG(INFO) << "[" << ros::Time::now() << "] "
              << "updating bool image! ";
    updateBoolImage(origin_path, full_path);
    updateModifyMask(origin_path, full_path);
  }

  //   saveMergedPbstream(hide_full_path);
  shutdown_flag_ = true;
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "points_with_stamp size: " << points_with_stamp_.size();
  if (!points_with_stamp_.empty())
  {
    LOG(INFO) << "[" << ros::Time::now() << "] "
              << "start saving strip infomation! ";
    LOG(INFO) << "points_with_stamp_.size: " << points_with_stamp_.size();
    saveStripInfo(request.nodes_poses_with_stamp, full_path);
  }
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "return now! ";
  return true;
}

bool MapMapping::saveBlankMapServer(map_manager_msgs::CreateBlankMapService::Request &request,
                                    map_manager_msgs::CreateBlankMapService::Response &response)
{
  // LOG(WARNING) << "[" << ros::Time::now() << "] "<< "save blank map!";
  absl::MutexLock locker(&mutex_);
  submap_images_publisher_.shutdown();
  for (auto it = submap_slices_.begin(); it != submap_slices_.end();)
  {
    it = submap_slices_.erase(it);
  }
  for (auto it = submap_images_.begin(); it != submap_images_.end();)
  {
    it = submap_images_.erase(it);
  }
  std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry>().swap(
      submap_images_);
  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice>().swap(
      submap_slices_);
  malloc_trim(0);
  // LOG(WARNING) << "erase slices done!";
  std::string fullPath, hideFullPath;
  std::string hide_map_name = "." + request.map_name;
  getPath(request.map_name, fullPath);
  getPath(hide_map_name, hideFullPath);
  std::string mapPngImgName = fullPath + "map.png";
  std::string subFolderPath = fullPath + "frames/0";
  std::string gridPngImgName = subFolderPath + "/probability_grid.png";
  std::string dataXmlPath = subFolderPath + "/data.xml";
  std::string mapXmlPath = fullPath + "map.xml";
  std::string mapDataXmlPath = fullPath + "map_data.xml";
  std::string landmarkXmlPath = fullPath + "landmark.xml";
  std::string cmd = "mkdir -p " + subFolderPath;
  int status = system(cmd.c_str());
  checkSystemStatus(cmd, status);
  if (status < 0)
  {
    response.error_msgs = "mkdir error";
    return false;
  }

  int width = 1000;
  int height = 1000;
  // grid.png
  cv::Mat gridPngImg = cv::Mat::zeros(width, height, CV_16UC1);
  std::vector<int> compression_params;
  compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(0);
  cv::imwrite(gridPngImgName, gridPngImg, compression_params);
  compression_params.clear();
  // map.png
  cv::Mat mapPngImg = map_manager::image16ToImage8(gridPngImg);
  // cv::Mat mapPngImg = cv::Mat::ones(width, height, CV_8UC1) * 255;
  compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(0);
  cv::imwrite(mapPngImgName, mapPngImg, compression_params);

  boost::property_tree::ptree p_top;
  boost::property_tree::ptree p_map;
  boost::property_tree::ptree p_node_list;

  // map_data.xml
  double resolution = 0.05;
  p_map.put("width", std::to_string(height));
  p_map.put("height", std::to_string(width));
  p_map.put("resolution", std::to_string(resolution));
  cartographer::transform::Rigid3d::Vector translation(0, 0, 0);
  cartographer::transform::Rigid3d::Quaternion quaternion(1, 0, 0, 0);
  cartographer::transform::Rigid3d::Quaternion quaternionMapData(0, -0.707107, 0.707107, 0);
  std::string poseStr;
  for (int i = 0; i < 3; i++)
  {
    poseStr = poseStr + std::to_string(translation[i]) + " ";
  }
  for (int i = 0; i < 3; i++)
  {
    poseStr = poseStr + std::to_string(quaternionMapData.coeffs()[i]) + " ";
  }
  poseStr = poseStr + std::to_string(quaternionMapData.coeffs()[3]);
  p_map.put("pose", poseStr);
  p_top.add_child("mapPng", p_map);
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(mapDataXmlPath, p_top, std::locale(), setting);
  p_map.clear();
  p_top.clear();

  // data.xml
  boost::property_tree::ptree p_probability_grid;
  p_map.put("id", 0);
  p_map.put("pose", poseStr);
  p_map.put("local_pose", poseStr);
  p_map.put("num_range_data", 1);
  p_map.put("finished", true);
  p_probability_grid.put("resolution", 0.05);
  std::string maxStr("0 0");
  p_probability_grid.put("max", maxStr);
  std::string numCellsStr("1000 1000");
  p_probability_grid.put("num_cells", numCellsStr);
  std::string knownCellsBoxStr("0 0 999 999");
  p_probability_grid.put("known_cells_box", knownCellsBoxStr);
  p_map.add_child("probability_grid", p_probability_grid);
  boost::property_tree::write_xml(dataXmlPath, p_map, std::locale(), setting);
  p_map.clear();

  // map.xml
  boost::property_tree::ptree p_node;
  p_node.put("id", 0);
  boost::property_tree::ptree p_neighbour_list;
  boost::property_tree::ptree p_neighbour;
  p_neighbour.put("id", 0);
  std::string data;
  for (int i = 0; i < 3; i++)
  {
    data = data + std::to_string(translation[i]) + " ";
  }
  data = data + std::to_string(quaternion.w()) + " ";
  data = data + std::to_string(quaternion.x()) + " ";
  data = data + std::to_string(quaternion.y()) + " ";
  data = data + std::to_string(quaternion.z());
  p_neighbour.put("transform", data);
  data.clear();
  p_neighbour.put("reachable", true);
  p_neighbour_list.add_child("neighbour", p_neighbour);
  p_node.add_child("neighbour_list", p_neighbour_list);
  p_node_list.add_child("node", p_node);
  p_map.add_child("node_list", p_node_list);
  boost::property_tree::ptree p_property;
  p_property.put("node_count", 1);
  p_map.add_child("property", p_property);
  p_top.add_child("map", p_map);
  boost::property_tree::write_xml(mapXmlPath, p_top, std::locale(), setting);

  // landmark.xml
  boost::property_tree::ptree p_landmark;
  boost::property_tree::write_xml(landmarkXmlPath, p_landmark, std::locale(), setting);
  return true;
}

bool MapMapping::shutdownTopicServer(cartographer_ros_msgs::SetSwitch::Request &request,
                                     cartographer_ros_msgs::SetSwitch::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "start shuting down map manager topics! ";
  if (request.type == "MapManager" && request.flag) submap_list_subscriber_.shutdown();
  LOG(INFO) << "shut down map manager topics! ";
  return true;
}

bool MapMapping::saveMapFromPbstream(cartographer_ros_msgs::SaveSubmapServer::Request &request,
                                     cartographer_ros_msgs::SaveSubmapServer::Response &response)
{
  string map_path = string(std::getenv("HOME")) + "/map/" + request.map_name;
  map_manager::GetMapFromPbstream g1(map_path, map_path);
  return true;
}

bool MapMapping::shutdownCarto(cartographer_ros_msgs::FinishMapping::Request &request,
                               cartographer_ros_msgs::FinishMapping::Response &response)
{
  absl::MutexLock locker(&mutex_);
  submap_list_subscriber_.shutdown();
  cartographer_ros_msgs::FinishMapping srv;
  srv.request = request;
  if (!shutdown_carto_client_.call(srv))
  {
    LOG(ERROR) << "call shutdown_cartographer2 failed.";
    response.success = false;
  }
  response = srv.response;
  return true;
}

void MapMapping::saveImageFromPbstream(const std::string &filename,
                                       const std::string &full_map_path)
{
  vector<cartographer::mapping::TrajectoryNode::Data> node_datas;
  std::unique_ptr<ProbabilityGrid> new_merge_grid;
  mapping::ValueConversionTables conversion_tables;
  mapping::ValueConversionTables new_conversion_tables;
  mapping::proto::ProbabilityGridRangeDataInserterOptions2D options;
  options.set_hit_probability(0.55);
  options.set_miss_probability(0.49);
  options.set_insert_free_space(true);
  mapping::RangeDataInserterInterface *new_range_data_inserter =
      new mapping::ProbabilityGridRangeDataInserter2D(options);
  io::ProtoStreamReader stream(filename);
  io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  MapById<NodeId, transform::Rigid3d> node_poses;
  map<int, int> ignore_trajectory;
  for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
       pose_graph_proto.trajectory())
  {
    if (trajectory_proto.submap().size() == 0)
      ignore_trajectory[trajectory_proto.trajectory_id()] = (trajectory_proto.trajectory_id());
    node_datas.resize(trajectory_proto.node().size());
    for (const cartographer::mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
    }
  }
  cartographer::mapping::proto::SerializedData proto_tmp;
  bool initialised = false;
  LOG(INFO) << "node_poses size:" << node_poses.size();
  std::map<int, vector<Eigen::Vector2d>> points_with_trajectory;
  for (const proto::TrimArea &area : pose_graph_proto.trim_area())
  {
    std::vector<Eigen::Vector2d> points;
    for (const transform::proto::Vector2d &it : area.points())
      points.push_back({it.x(), it.y()});
    points_with_trajectory[area.trajectory_id()] = points;
  }
  auto trajectory_transforms = getTrajectoryArea(points_with_trajectory);
  std::map<int, std::vector<sensor::RangeData>> trajectory_ranges;
  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      cartographer::mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      const cartographer::mapping::proto::TrajectoryNodeData proto_node_data =
          *proto_node->mutable_node_data();
      const cartographer::mapping::TrajectoryNode::Data node_data =
          cartographer::mapping::FromProto(proto_node_data);

      const sensor::PointCloud points = node_data.filtered_gravity_aligned_point_cloud;

      NodeId id(proto_node->node_id().trajectory_id(), proto_node->node_id().node_index());
      if (ignore_trajectory.find(id.trajectory_id) != ignore_trajectory.end()) continue;
      const transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(node_data.gravity_alignment);
      transform::Rigid3f node_global_pose =
          node_poses.at(id).cast<float>() * gravity_alignment.inverse().cast<float>();
      sensor::PointCloud points_in_global = sensor::TransformPointCloud(points, node_global_pose);
      transform::Rigid2f global_pose_2d = transform::Project2D(node_global_pose);
      Eigen::Vector2f origin;
      origin = global_pose_2d.translation();
      sensor::RangeData rangedata{{origin(0), origin(1), 0}, points_in_global, {}};
      trajectory_ranges[id.trajectory_id].push_back(rangedata);

      //         TrajectoryTransform trajectory_transform;
      //         bool has_trajectory_transfrom = false;
      //         if(trajectory_transforms.find(id.trajectory_id) != trajectory_transforms.end())
      //         {
      //           trajectory_transform = trajectory_transforms[id.trajectory_id];
      //           has_trajectory_transfrom = true;
      //         }
      //         sensor::PointCloud new_global_points;
      //         new_global_points.reserve(points_in_global.size());
      //         for(const sensor::RangefinderPoint& it: points_in_global)
      //         {
      //           if(!has_trajectory_transfrom)
      //             new_global_points.push_back(it);
      //           else
      //           {
      //             Eigen::Vector2d origin_point(it.position.x(), it.position.y());
      //             Eigen::Vector2d new_point = trajectory_transform.area_R_map * origin_point +
      //             trajectory_transform.area_t_map; if((new_point.x() <
      //             trajectory_transform.max_area.x()
      //                 && new_point.y() < trajectory_transform.max_area.y()
      //                 && new_point.x() > 0 && new_point.y() > 0))
      //               new_global_points.push_back(it);
      //           }
      //         }
      //         new_global_points.resize(new_global_points.size());
    }
    break;
    default:
      break;
    }
  }

  cv::Mat last_cell_mat;
  double last_resolution = 0.05;
  double last_max[2] = {-1, -1};
  LOG(WARNING) << "trajectory_ranges.size: " << trajectory_ranges.size();
  int cur_trajectory_id = trajectory_ranges.rbegin()->first;
  LOG(WARNING) << "cur_trajectory_id: " << cur_trajectory_id;
  int last_num_x_cells;
  int last_num_y_cells;
  std::vector<uint16_t> last_cells;
  for (std::map<int, vector<sensor::RangeData>>::iterator it = trajectory_ranges.begin();
       it != trajectory_ranges.end(); ++it)
  {
    if (!initialised)
    {
      constexpr int kInitialSubmapSize = 20;
      new_merge_grid.reset(new ProbabilityGrid(
          mapping::MapLimits(last_resolution,
                             Eigen::Vector2d(0., 0.) + 0.5 * kInitialSubmapSize * last_resolution *
                                                           Eigen::Vector2d::Ones(),
                             mapping::CellLimits(kInitialSubmapSize, kInitialSubmapSize)),
          &new_conversion_tables));
      initialised = true;
    }

    bool new_merge_add_flag = true;

    if (trajectory_transforms.find(cur_trajectory_id) != trajectory_transforms.end() &&
        trajectory_transforms.find(it->first) != trajectory_transforms.end())
    {
      new_merge_add_flag = false;
    }
    LOG(WARNING) << "trajectory id " << it->first << " : "
                 << (new_merge_add_flag ? "add nodes" : "not add nodes");
    for (const auto &range : it->second)
    {
      new_range_data_inserter->Insert(range, new_merge_grid.get());
    }

    if (!new_merge_add_flag)
    {
      TrajectoryTransform cur_transform = trajectory_transforms[it->first];
      double max[2];
      max[0] = new_merge_grid->limits().max()[0];
      max[1] = new_merge_grid->limits().max()[1];
      double resolution = new_merge_grid->limits().resolution();
      const std::vector<uint16_t> &cells = new_merge_grid->cells();
      int num_x_cells = new_merge_grid->limits().cell_limits().num_x_cells;
      int num_y_cells = new_merge_grid->limits().cell_limits().num_y_cells;
      cv::Mat cells_mat = cv::Mat::zeros(num_y_cells, num_x_cells, CV_16UC1);
      for (int i = 0; i < num_y_cells; i++)
        for (int j = 0; j < num_x_cells; j++)
        {
          uint16_t value = cells[i * num_x_cells + j];
          cells_mat.at<uint16_t>(i, j) = value;
        }
      //       imshow("22", cells_mat);
      //       waitKey(0);
      cv::Mat last_cell_mat = cv::Mat::zeros(last_num_y_cells, last_num_x_cells, CV_16UC1);
      for (int i = 0; i < last_num_y_cells; i++)
        for (int j = 0; j < last_num_x_cells; j++)
        {
          uint16_t value = last_cells[i * last_num_x_cells + j];
          last_cell_mat.at<uint16_t>(i, j) = value;
          Eigen::Vector2d origin_global_point(last_max[0] - i * last_resolution,
                                              last_max[1] - j * last_resolution);
          Eigen::Vector2d new_point =
              cur_transform.area_R_map * origin_global_point + cur_transform.area_t_map;
          if ((new_point.x() >= cur_transform.max_area.x() ||
               new_point.y() >= cur_transform.max_area.y() || new_point.x() < 0 ||
               new_point.y() < 0))
          {

            int new_i = round((max[1] - origin_global_point(1)) / resolution);
            int new_j = round((max[0] - origin_global_point(0)) / resolution);
            if (new_i >= num_y_cells || new_j >= num_x_cells || new_i < 0 || new_j < 0)
            {
              //               LOG(INFO) << new_i <<" " << new_j << "  " << num_x_cells << " " <<
              //               num_y_cells;
              continue;
            }
            cells_mat.at<uint16_t>(new_j, new_i) = value;
          }
          else
            last_cell_mat.at<uint16_t>(i, j) = 65535;
        }
      LOG(WARNING) << "start imwrite trajectory image";
      imwrite(full_map_path + "trajectory_" + to_string(it->first) + ".png", last_cell_mat);
      imwrite(full_map_path + "origin_trajectory_" + to_string(it->first) + ".png", cells_mat);
      LOG(WARNING) << "imwrite trajectory image done";
      new_merge_grid->set_cells(cells_mat);
    }

    if (std::next(it) != trajectory_ranges.end())
    {
      auto it_next = std::next(it);
      int next_trajectory_id = it_next->first;
      LOG(WARNING) << it->first << ", " << it_next->first;
      if (trajectory_transforms.find(next_trajectory_id) != trajectory_transforms.end())
      {
        last_max[0] = new_merge_grid->limits().max()[0];
        last_max[1] = new_merge_grid->limits().max()[1];
        last_resolution = new_merge_grid->limits().resolution();
        const std::vector<uint16_t> &cells = new_merge_grid->cells();
        last_cells = cells;
        last_num_x_cells = new_merge_grid->limits().cell_limits().num_x_cells;
        last_num_y_cells = new_merge_grid->limits().cell_limits().num_y_cells;
      }
    }
  }

  if (new_range_data_inserter != nullptr)
  {
    delete new_range_data_inserter;
  }
  auto final_grid = new_merge_grid->ComputeCroppedGrid();
  double resolution = final_grid->limits().resolution();
  double max[2];
  max[0] = final_grid->limits().max()[0];
  max[1] = final_grid->limits().max()[1];
  int num_x_cells = final_grid->limits().cell_limits().num_x_cells;
  int num_y_cells = final_grid->limits().cell_limits().num_y_cells;
  const Eigen::AlignedBox2i &known_cells_box = final_grid->known_cells_box();

  const std::vector<uint16_t> &cells = final_grid->cells();
  LOG(INFO) << "max: " << max[0] << "," << max[1];
  LOG(INFO) << "cells: " << num_x_cells << "," << num_y_cells;
  LOG(INFO) << "known_cells_box: " << known_cells_box.min().x() << "," << known_cells_box.min().y()
            << "," << known_cells_box.max().x() << "," << known_cells_box.max().y();
  cv::Mat cells_mat = cv::Mat::zeros(num_y_cells, num_x_cells, CV_16UC1);
  for (int i = 0; i < num_y_cells; i++)
    for (int j = 0; j < num_x_cells; j++)
    {
      uint16_t value = cells[i * num_x_cells + j];
      cells_mat.at<uint16_t>(i, j) = value;
    }
  //   pg->set_cells();

  ProbabilityGridInfo merge_info;

  merge_info.resolution = resolution;
  merge_info.num_cells[0] = num_x_cells;
  merge_info.num_cells[1] = num_y_cells;
  merge_info.max[0] = max[0];
  merge_info.max[1] = max[1];
  merge_info.num_range_data = node_poses.size();
  merge_info.finished = true;
  merge_probability_infos_.push_back(merge_info);

  string sub_folder_path = full_map_path + "frames/";
  saveToXml(sub_folder_path, merge_probability_infos_.size() - 1);

  string img_name =
      sub_folder_path + to_string(merge_probability_infos_.size() - 1) + "/probability_grid.png";
  string origin_img_name = sub_folder_path + to_string(merge_probability_infos_.size() - 1) +
                           "/probability_grid_origin.png";
  LOG(INFO) << "save img of " << img_name;
  std::vector<int> compression_params;
  compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(0);

  imwrite(img_name, cells_mat, compression_params);
  imwrite(origin_img_name, cells_mat, compression_params);

  cv::Mat show_img = image16ToImage8(cells_mat);
  img_test_ = show_img.clone();
  imwrite(full_map_path + "map.png", show_img, compression_params);
  {
    max_box_ = Eigen::Vector2d(max[0], max[1]);
  }
  // waiting for testing
  {
    boost::property_tree::ptree p_map, p_top;
    std::string data;
    const int width = show_img.cols;
    const int height = show_img.rows;
    data = to_string(height);
    p_map.put("width", data);
    data.clear();
    data = to_string(width);
    p_map.put("height", data);
    data.clear();
    data = to_string(resolution);
    p_map.put("resolution", data);
    data.clear();
    double x = max[0] / resolution;
    double y = max[1] / resolution;
    cartographer::transform::Rigid3d::Vector translation(y, x, 0);
    cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2, sqrt(2) / 2, 0);
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(translation[i]) + " ";
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(quaternion.coeffs()[i]) + " ";
    data = data + std::to_string(quaternion.coeffs()[3]);
    p_map.put("pose", data);
    data.clear();
    p_top.add_child("mapPng", p_map);

    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    boost::property_tree::write_xml(full_map_path + "map_data.xml", p_top, std::locale(), setting);
  }
}

std::map<int, TrajectoryTransform> MapMapping::getTrajectoryArea(const string &path)
{
  std::map<int, TrajectoryTransform> trajectory_transforms;

  if (judgeFileExist(path + "/trajectory_area.xml"))
  {
    boost::property_tree::ptree pt, pt2;
    boost::property_tree::read_xml(path + "/trajectory_area.xml", pt);
    std::map<int, std::string> trajectory_with_points;
    for (auto it : pt)
    {
      std::vector<Eigen::Vector2d> points;
      points.resize(4);
      if (it.first == "trajectory_area")
      {
        int id = it.second.get<int>("trajectory_id");
        std::string points_str = it.second.get<std::string>("points_in_map");
        sscanf(points_str.c_str(), "%lf %lf %lf %lf %lf %lf %lf %lf", &points[0][0], &points[0][1],
               &points[1][0], &points[1][1], &points[2][0], &points[2][1], &points[3][0],
               &points[3][1]);
        transform::Rigid3d map2area;
        Eigen::Vector2d new_x_towards = (points[3] - points[0]).normalized();
        Eigen::Vector2d new_y_towards = (points[1] - points[0]).normalized();
        float at1 = atan2(new_x_towards(1), new_x_towards(0));
        if (at1 > M_PI / 2) at1 = at1 - 2 * M_PI;
        float at2 = atan2(new_y_towards(1), new_y_towards(0));
        Eigen::Matrix2d map_R_area;
        LOG(WARNING) << at1 << ", " << at2 << ", " << fabs(at1 + M_PI / 2 - at2);
        if (fabs(at1 + M_PI / 2 - at2) > 0.5)
          map_R_area << new_y_towards[0], new_x_towards[0], new_y_towards[1], new_x_towards[1];
        else
          map_R_area << new_x_towards[0], new_y_towards[0], new_x_towards[1], new_y_towards[1];
        Eigen::Vector2d map_t_area = points[0];

        TrajectoryTransform transform2;
        transform2.area_R_map = map_R_area.transpose();
        transform2.area_t_map = -(transform2.area_R_map * map_t_area);
        transform2.max_area = transform2.area_R_map * points[2] + transform2.area_t_map;
        trajectory_transforms[id] = transform2;
        LOG(WARNING) << id << " trajectory max_area: " << transform2.max_area.transpose();
      }
    }
  }
  return trajectory_transforms;
}

std::map<int, TrajectoryTransform>
MapMapping::getTrajectoryArea(const std::map<int, vector<Eigen::Vector2d>> &points_with_trajectory)
{
  std::map<int, TrajectoryTransform> trajectory_transforms;

  for (auto it : points_with_trajectory)
  {
    std::vector<Eigen::Vector2d> points = it.second;
    int id = it.first;

    transform::Rigid3d map2area;
    Eigen::Vector2d new_x_towards = (points[3] - points[0]).normalized();
    Eigen::Vector2d new_y_towards = (points[1] - points[0]).normalized();
    float at1 = atan2(new_x_towards(1), new_x_towards(0));
    if (at1 > M_PI / 2) at1 = at1 - 2 * M_PI;
    float at2 = atan2(new_y_towards(1), new_y_towards(0));
    Eigen::Matrix2d map_R_area;
    LOG(WARNING) << at1 << ", " << at2 << ", " << fabs(at1 + M_PI / 2 - at2);
    if (fabs(at1 + M_PI / 2 - at2) > 0.5)
      map_R_area << new_y_towards[0], new_x_towards[0], new_y_towards[1], new_x_towards[1];
    else
      map_R_area << new_x_towards[0], new_y_towards[0], new_x_towards[1], new_y_towards[1];
    Eigen::Vector2d map_t_area = points[0];

    TrajectoryTransform transform2;
    transform2.area_R_map = map_R_area.transpose();
    transform2.area_t_map = -(transform2.area_R_map * map_t_area);
    transform2.max_area = transform2.area_R_map * points[2] + transform2.area_t_map;
    trajectory_transforms[id] = transform2;
    LOG(WARNING) << id << " trajectory max_area: " << transform2.max_area.transpose();
  }

  return trajectory_transforms;
}

// void MapMapping::saveMergedPbstream(const std::string& path)
// {
//   cartographer_ros::NodeOptions node_options;
//   cartographer_ros::TrajectoryOptions trajectory_options;
//   std::string cartographer_path = ros::package::getPath("cartographer_ros");
//   LOG(INFO) <<"\ncartographer_path:" << cartographer_path;
//   std::tie(node_options, trajectory_options) =
//      cartographer_ros::LoadOptions(cartographer_path + "/configuration_files",
//      "backpack_2d_jz_mapping.lua");
//   {
//     std::unique_ptr<pbstream_convertor::PbstreamConvertor>
//       pbstream(new pbstream_convertor::PbstreamConvertor(node_options.map_builder_options));
//     LOG(INFO) << "\nload_map_path:" << path;
//     if(pbstream->handlePbstream(path))
//       LOG(INFO) << "save merged pbstream successfully !";
//     else
//       LOG(WARNING) << "save merged pbstream failed! ";
//     LOG(INFO) << "save merged pbstream successfully too!";
//   }
// }

void MapMapping::saveStripInfo(const vector<geometry_msgs::PoseStamped> &node_poses,
                               const std::string &map_path)
{
  std::map<ros::Time, cartographer::transform::Rigid3d> node_poses_with_times;
  for (const auto &node : node_poses)
  {
    Eigen::Vector3d t(node.pose.position.x, node.pose.position.y, node.pose.position.z);
    Eigen::Quaterniond q(node.pose.orientation.w, node.pose.orientation.x, node.pose.orientation.y,
                         node.pose.orientation.z);
    cartographer::transform::Rigid3d pose(t, q);
    node_poses_with_times[node.header.stamp] = pose;
  }
  std::map<int, std::vector<Eigen::Vector3d>> points_in_map;
  for (auto &points : points_with_stamp_)
  {
    common::Time cur_t = points.first;
    map<ros::Time, cartographer::transform::Rigid3d>::iterator it1 = node_poses_with_times.begin();
    map<ros::Time, cartographer::transform::Rigid3d>::iterator it2 = node_poses_with_times.begin();
    ++it2;
    for (; it2 != node_poses_with_times.end(); ++it1, ++it2)
    {
      ros::Time t1 = it1->first;
      ros::Time t2 = it2->first;
      common::Time t11 = cartographer_ros::FromRos(t1);
      common::Time t22 = cartographer_ros::FromRos(t2);
      if (cur_t > t11 && cur_t < t22)
      {
        cartographer::transform::Rigid3d transform =
            cartographer::transform::Interpolate(
                cartographer::transform::TimestampedTransform{t11, it1->second},
                cartographer::transform::TimestampedTransform{t22, it2->second}, cur_t)
                .transform;
        for (int i = 0; i < 2; i++)
        {
          for (auto &point : points.second[i])
          {

            points_in_map[i].push_back(transform * base2tracking_.value() * point);
          }
        }
        break;
      }
    }
  }
  lineFit lin_fit(map_path);
  if (mode_ == 2)
  {
    if (!origin_strip_points_.empty())
    {
      lin_fit.setOriginStripPoints(origin_strip_points_);
    }
  }
  lin_fit.linDraw(points_in_map);
}

void MapMapping::clear()
{

  for (auto &srv : servers_)
    srv.shutdown();
  submap_query_client_.shutdown();
  submap_server_client_.shutdown();
  submap_list_subscriber_.shutdown();
  strip_detect_result_sub_.shutdown();
  node_poses_sub_.shutdown();
  submap_images_publisher_.shutdown();
  strip_detect_result_pub_.shutdown();
  global_map_publisher_.shutdown();
}

void MapMapping::setMode(const int &mode)
{
  mode_ = mode;
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "mode: " << mode_;
  if (mode_ == 2)
  {
    string origin_path = "";
    if (::ros::param::get("/vtr/origin_path", origin_path))
    {
      LOG(INFO) << "origin_map_path:" << origin_path;
      if (origin_strip_points_.empty())
      {
        ifstream origin_data(origin_path + "/final_strip_points_in_map.txt");

        if (origin_data)
        {
          LOG(INFO) << "Load origin_strip_points!";
          while (!origin_data.eof())
          {
            double x_temp, y_temp;
            origin_data >> x_temp;
            origin_data >> y_temp;
            origin_strip_points_.push_back(Eigen::Vector2d(x_temp, y_temp));
          }
          filtered_origin_strip_points_ = VoxelFilter(0.1).Filter(origin_strip_points_);
          origin_data.close();
        }
        else
          LOG(WARNING) << "origin_strip_points empty!";
      }

      if (judgeFileExist(origin_path + "/trajectory_area.xml"))
      {
        string system_command = "rm " + origin_path + "/trajectory_area.xml";
        LOG(INFO) << system_command;
        int status = system(system_command.c_str());
        checkSystemStatus(system_command, status);
        CHECK(status != -1);
      }
    }
  }
}

} // namespace map_manager