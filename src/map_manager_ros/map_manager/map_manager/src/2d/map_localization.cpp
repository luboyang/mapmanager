/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "2d/map_localization.h"

#include <assert.h>
#include <cartographer_ros/node.h>
#include <cartographer_ros/node_options.h>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <map_manager_msgs/TagPose.h>
#include <map_manager_msgs/VisualmarksDetectInfo.h>
#include <ros/package.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <std_srvs/SetBool.h>
#include <visualization_msgs/MarkerArray.h>
#include <vtr_msgs/GlobalLocalizationPose.h>

#include <boost/circular_buffer.hpp>

#include "map_manager_ulitity.h"
using namespace cv;
using namespace std;
namespace map_manager
{

MapLocalization::MapLocalization()
{
  tag_Initialization_lasttime_ = ros::Time(0);
  if (!ros::param::get("~tagmark_localize_interval", tagmark_localize_interval_))
  {
    LOG(WARNING) << ("no tagmark_localize_interval given, using default");
    tagmark_localize_interval_ = 1.0;
  }
  if (!ros::param::get("~tagmark_localize_buffer_size", tagmark_localize_buffer_size_))
  {
    LOG(WARNING) << ("no tagmark_localize_buffer_size given, using default");
    tagmark_localize_buffer_size_ = 10;
  }
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "tagmark_localize_interval: " << tagmark_localize_interval_;
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "tagmark_localize_buffer_size: " << tagmark_localize_buffer_size_;

  size_of_map_[0] = 1;
  size_of_map_[1] = 1;
  resolution_ = 0.05;
  pbstream_merged_flag_ = false;

  // ros matter
  landmarklist_localizing_pub_ = nh_.advertise<visualization_msgs::MarkerArray>(
      "/cartographer_front_end/landmark_pose_full", 5);
  localization_request_pub_ = nh_.advertise<std_msgs::Empty>("/vtr/submap_localization", 1);
  global_localization_pub_ =
      nh_.advertise<vtr_msgs::GlobalLocalizationPose>("/vtr/global_localization", 1);
  tag_global_localization_pub_ =
      nh_.advertise<vtr_msgs::GlobalLocalizationPose>("/tag_localiser/global_localization", 1);
  landmark_detect_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("/landmark_poses_list", 1);
  landmark_detect_pub2_ =
      nh_.advertise<visualization_msgs::MarkerArray>("/base_pose_in_tag_map", 1);
  visualmarks_detect_pub_ =
      nh_.advertise<map_manager_msgs::VisualmarksDetectInfo>("/visualmarks_detect", 1);
  initial_pose_client_ =
      nh_.serviceClient<vtr_msgs::SetGlobalPose>("/vtr/global_localization_server");
  texture_initial_pose_client_ =
      nh_.serviceClient<vtr_msgs::SetGlobalPose>("/fm_localiser/global_localization_server");
  reload_arm_free_space_image_client_ =
      nh_.serviceClient<std_srvs::SetBool>("/map_manager_localization/reload_free_space_image");
  tag_initial_pose_client_ =
      nh_.serviceClient<vtr_msgs::SetGlobalPose>("/tag_localiser/global_localization_server");

  landmark_detect_sub_ = nh_.subscribe("/sensor_detector/visualmarks", 1,
                                       &MapLocalization::handleLandmarkLocalizing, this);

  wall_timers_.push_back(
      nh_.createWallTimer(::ros::WallDuration(0.1), &MapLocalization::publishLandmarks, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_submaps",
                                          &MapLocalization::loadSubmapsServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_landmarks",
                                          &MapLocalization::loadLandmarksServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/reference_location_server",
                                          &MapLocalization::referenceLocationServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/update_landmarks",
                                          &MapLocalization::updateLandmarks, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/modify_map",
                                          &MapLocalization::modifyMapServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/get_points_in_image",
                                          &MapLocalization::getpointsInMapServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/adjust_map_pixel",
                                          &MapLocalization::adjustMapPixelServer, this));

  servers_.push_back(nh_.advertiseService("/map_manager_localization/set_location_region",
                                          &MapLocalization::set2DPointsServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/get_location_region",
                                          &MapLocalization::get2DPointsServer, this));
  servers_.push_back(
      nh_.advertiseService("/mark_localization/initial_pose", &MapLocalization::setInitPose, this));

  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_strip_image",
                                          &MapLocalization::loadLineImageServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_auto_ignored_area_image",
                                          &MapLocalization::getAutoIgnoredArea, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_tag_map",
                                          &MapLocalization::loadTagMap, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/modify_arm_region",
                                          &MapLocalization::modifyFreeSpaceRegionServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/modify_landmarks_id",
                                          &MapLocalization::modifyLandmarksId, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/detect_landmark",
                                          &MapLocalization::detectLandmarkServer, this));
  last_pub_marker_time_ = ros::Time::now();
}

MapLocalization::~MapLocalization() {}


// void MapLocalization::loadOldSubmap(const string& submap_filepath, cartographer_ros_msgs::SubmapImageEntry &submap_img)
// {
//   LOG(INFO) << submap_filepath.c_str() << " loading...";
//   Eigen::Isometry3d pose_in_global, poseInLocal, local2global;
//   double resolution, max[2];
//   uint num_x_cells, num_y_cells;

//   boost::property_tree::ptree pt;
//   boost::property_tree::read_xml(submap_filepath + "/data.xml", pt);
  
//   if (pt.empty())
//   {
//     ROS_FATAL("Load %s/data.xml failed!", submap_filepath.c_str());
//     return;
//   }
  
//   std::string data;
//   float pose[7];
//   Eigen::Quaternionf q1,q2;
//   Eigen::Matrix3f rotMat;

// 	int id = pt.get<int>("id");
// 	submap_img.submap_index = id;
//   data = pt.get<std::string>("pose");
//   sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2],
//       &pose[3], &pose[4], &pose[5], &pose[6]);
//   q1 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);
//   rotMat = q1.matrix();
//   pose_in_global.setIdentity();
//   for (int i = 0; i < 3; i++)
//     for (int j = 0; j < 3; j++)
//       pose_in_global(i, j) = rotMat(i, j);
//   pose_in_global(0, 3) = pose[0];
//   pose_in_global(1, 3) = pose[1];
//   pose_in_global(2, 3) = pose[2];

// 	posesInGlobal_.push_back(pose_in_global);
//   data = pt.get<std::string>("local_pose");
//   sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2],
//       &pose[3], &pose[4], &pose[5], &pose[6]);
//   q2 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);
//   rotMat = q2.matrix();
//   poseInLocal.setIdentity();
//   for (int i = 0; i < 3; i++)
//     for (int j = 0; j < 3; j++)
//       poseInLocal(i, j) = rotMat(i, j);
//   poseInLocal(0, 3) = pose[0];
//   poseInLocal(1, 3) = pose[1];
//   poseInLocal(2, 3) = pose[2];

//   resolution = pt.get<double>("probability_grid.resolution");
// 	submap_img.resolution = resolution;
//   data = pt.get<std::string>("probability_grid.max");
//   sscanf(data.c_str(), "%lf %lf", &max[0], &max[1]);
//   data = pt.get<std::string>("probability_grid.num_cells");
//   sscanf(data.c_str(), "%d %d", &num_x_cells, &num_y_cells);
  
//   size_of_map_[0] = num_x_cells;
//   size_of_map_[1] = num_y_cells;
  
//   local2global = pose_in_global * poseInLocal.inverse();
// 	localInGlobal_poses_.push_back(local2global);
// 	maxes_.push_back(max[0]);
// 	maxes_.push_back(max[1]);
	
// 	Eigen::Vector3d pointInLocal, pointInGlobal;
// 	pointInLocal[0] = max[0];
// 	pointInLocal[1] = max[1];
// 	pointInLocal[2] = 0;
	
// 	pointInGlobal = local2global * pointInLocal;
// 	LOG(INFO) << "locate id: " <<id << "  "<< pointInGlobal;
// 	submap_img.pose_in_map.position.x = pointInGlobal(0);
// 	submap_img.pose_in_map.position.y = pointInGlobal(1);
// 	submap_img.pose_in_map.position.z = pointInGlobal(2);
// 	Eigen::Quaternionf q = q1*q2.conjugate();
// 	submap_img.pose_in_map.orientation.w = q.w();
// 	submap_img.pose_in_map.orientation.x = q.x();
// 	submap_img.pose_in_map.orientation.y = q.y();
// 	submap_img.pose_in_map.orientation.z = q.z();
// 	Mat cellMat = imread(submap_filepath + "/probability_grid.png",
//                    CV_LOAD_IMAGE_UNCHANGED);
//   if (cellMat.data == nullptr)
//   {
//     ROS_FATAL("Load %s/probability_grid.png failed!", submap_filepath.c_str());
//     return;
//   }
//   // ROS_WARN("(%.2lf, %.2lf) %.3lf", max[0], max[1], resolution);
// 	Mat map_adjusted = grey16ToRgba(cellMat);

// //  imshow("1",map_adjusted);
// // 	waitKey(0);
// 	sensor_msgs::CompressedImagePtr msg_ptr =  cv_bridge::CvImage(std_msgs::Header(),"bgra8",map_adjusted).toCompressedImageMsg(cv_bridge::PNG);
// 	submap_img.image = *msg_ptr;
// }

void MapLocalization::publishLandmarks(const ros::WallTimerEvent &unused_timer_event)
{
  absl::MutexLock locker(&mutex_);
  if (landmarklist_localizing_pub_.getNumSubscribers() != 0)
  {
    landmarklist_localizing_pub_.publish(landmark_full_array_);
  }
  return;
}

void MapLocalization::getNearestPose(const Eigen::Vector2d &worldLoc,
                                     Eigen::Isometry3d &nearestPose, int &nearestIdx)
{
  double minDistance = 100000;

  for (uint i = 0; i < posesInGlobal_.size(); i++)
  {
    //     LOG(INFO) << "posesInGlobal_[i]:\n" << posesInGlobal_[i].matrix() <<;

    Eigen::Vector2d dis_vec =
        Eigen::Vector2d(posesInGlobal_[i](0, 3), posesInGlobal_[i](1, 3)) - worldLoc;

    double distance = dis_vec.norm();
    if (distance < minDistance)
    {
      minDistance = distance;
      nearestIdx = i;

      for (int row = 0; row < 4; row++)
        for (int col = 0; col < 4; col++)
          nearestPose(row, col) = posesInGlobal_[i](row, col);
    }
  }
}

bool MapLocalization::loadSubmapsServer(
    cartographer_ros_msgs::SubmapImagesServer::Request &request,
    cartographer_ros_msgs::SubmapImagesServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] " << ("Loading submaps ...");
  if(!to_ros_msg_node_.loadSubmaps(request.map_path,response.submap_images, localInGlobal_poses_,posesInGlobal_,maxes_,size_of_map_))
  {
    response.error_message = "Load submaps failed";
    LOG(WARNING) << response.error_message;
  }
  return true;
}

bool MapLocalization::referenceLocationServer(
    cartographer_ros_msgs::ReferenceLocationFromMap::Request &request,
    cartographer_ros_msgs::ReferenceLocationFromMap::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "getting reference coordinates ...";
  LOG(INFO) << "request pose size: " << request.mapPose.size();
  if (request.mapPose.size() == 0)
  {
    const std::string error_string = "No mapPose need to be transformed!";
    LOG(WARNING) << error_string;
    throw std::runtime_error(error_string);
    return true;
  }
  if (posesInGlobal_.size() == 0)
  {
    const std::string error_string = "Have not loaded submaps!";
    LOG(WARNING) << error_string;
    throw std::runtime_error(error_string);
    return true;
  }
  uint pose_cnt = request.mapPose.size();

  response.reference_id.resize(pose_cnt);
  response.referencePose.resize(pose_cnt);
  response.worldPose.resize(pose_cnt);

  for (uint cnt = 0; cnt < pose_cnt; cnt++)
  {
    Eigen::Vector2d worldLoc;
    //     double theta;
    geometry_msgs::Pose pose = request.mapPose[cnt];
    worldLoc[0] = pose.position.x;
    worldLoc[1] = pose.position.y;

    Eigen::Quaterniond q(pose.orientation.w, pose.orientation.x, pose.orientation.y,
                         pose.orientation.z);
    // 		Eigen::Vector3d eulerAngle=q.matrix().eulerAngles(0,1,2);
    // 		theta = eulerAngle(2);

    //     imageLoc_direction[0] = imageLoc[0] + 100 * cos(theta) + 0.5;
    //     imageLoc_direction[1] = imageLoc[1] + 100 * sin(theta) + 0.5;
    //
    //     map_merger_.image2worldLoc(imageLoc, worldLoc);
    //     map_merger_.image2worldLoc(imageLoc_direction, worldLoc_direction);
    //
    //     yaw = atan2(worldLoc_direction[1] - worldLoc[1],
    //         worldLoc_direction[0] - worldLoc[0]);

    Eigen::Isometry3d targetPoseInWorld;
    Eigen::Matrix3d rotMat = q.matrix();
    //         (Eigen::AngleAxisd(theta, Eigen::Matrix<double, 3, 1>::UnitZ()))
    //         .toRotationMatrix();
    targetPoseInWorld.setIdentity();
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        targetPoseInWorld(i, j) = rotMat(i, j);
    targetPoseInWorld(0, 3) = worldLoc[0];
    targetPoseInWorld(1, 3) = worldLoc[1];
    targetPoseInWorld(2, 3) = 0;

    tf::poseEigenToMsg(targetPoseInWorld, response.worldPose[cnt]);

    int nearestIdx = -1;
    Eigen::Isometry3d nearestPose;
    getNearestPose(worldLoc, nearestPose, nearestIdx);

    if (nearestIdx == -1)
    {
      const std::string error_string =
          "Invalid map with " + std::to_string(posesInGlobal_.size()) + " poses inside!";
      LOG(WARNING) << error_string;
      throw std::runtime_error(error_string);
      return true;
    }

    //  LOG(INFO) << "targetPoseInWorld:\n" << targetPoseInWorld.matrix() <<
    //  std::endl;
    //  LOG(INFO) << "nearestPose:\n" << nearestPose.matrix();
    Eigen::Isometry3d target2reference;
    target2reference = nearestPose.inverse() * targetPoseInWorld;
    // double target_yaw =
    // GetYaw(Eigen::Quaterniond(target2reference.rotation()));

    response.reference_id[cnt] = nearestIdx;
    tf::poseEigenToMsg(target2reference, response.referencePose[cnt]);
    //    response.referencePose.x = target2reference(0, 3);
    //    response.referencePose.y = target2reference(1, 3);
    //    response.referencePose.theta = target_yaw;
  }

  //   response.header = header_;

  return true;
}
vector< cartographer_ros_msgs::LandmarkNewEntry > MapLocalization::loadAndUpdateLandmark()
{
  vector< cartographer_ros_msgs::LandmarkNewEntry > landmarks;
  to_ros_msg_node_.loadLandmarks(localization_path_, landmarks);
  landmark_full_array_.markers.clear();
  landmark_full_map_.clear();
  for (const cartographer_ros_msgs::LandmarkNewEntry &landmark : landmarks)
  {
    
    visualization_msgs::Marker marker;
    marker.ns = landmark.ns;
    marker.id = stoi(landmark.id);
    marker.pose = landmark.tracking_from_landmark_transform;
    marker.type = 1;
    landmark_full_array_.markers.push_back(marker);
    
    Eigen::Isometry3d landmark2world;
    tf::poseMsgToEigen(marker.pose, landmark2world);
    int id = marker.id;
    if (marker.ns == "Lasermarks")
    {
      id += 1000000;
    }
    else if (marker.ns == "Scenes")
    {
      id += 5000000;
    };
    landmark_full_map_[id] = landmark2world;
  }
  return landmarks;
}

bool MapLocalization::loadLandmarksServer(
    cartographer_ros_msgs::LandmarkListServer::Request &request,
    cartographer_ros_msgs::LandmarkListServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "Loading landmarks ...";
  getPath(request.map_name, localization_path_);
  response.landmarks = loadAndUpdateLandmark();
  if(response.landmarks.empty())
  {
    response.error_message = "Can not load landmarks from " + localization_path_;
    LOG(WARNING) << response.error_message;
  }
  return true;
}

bool MapLocalization::loadLineImageServer(map_manager_msgs::GetImage::Request &request,
                                          map_manager_msgs::GetImage::Response &response)
{
  string path;
  getPath(request.map_name, path);
  Mat line_img = imread(path + "strip.png", CV_LOAD_IMAGE_UNCHANGED);
  if (line_img.empty())
  {
    response.success = false;
    response.error_message = "This map does not have strip map!";
    return true;
  }
  cv::Mat line_4channels_img(line_img.rows, line_img.cols, CV_8UC4);
  for (int row = 0; row < line_img.rows; row++)
  {
    Vec3b *ptr = line_img.ptr<Vec3b>(row);
    Vec4b *ptr2 = line_4channels_img.ptr<Vec4b>(row);
    for (int col = 0; col < line_img.cols; col++)
    {
      ptr2[col][0] = ptr[col][0];
      ptr2[col][1] = ptr[col][1];
      ptr2[col][2] = ptr[col][2];
      if (ptr[col][0] != 255 || ptr[col][1] != 255 || ptr[col][2] != 255)
      {
        ptr2[col][3] = 128;
      }
      else
      {
        ptr2[col][3] = 0;
      }
    }
  }
  std::vector<int> compression_params;
  compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(0);
  compression_params.push_back(cv::IMWRITE_PNG_STRATEGY);
  compression_params.push_back(cv::IMWRITE_PNG_STRATEGY_DEFAULT);
  imwrite(path + "strip_4channels.png", line_4channels_img, compression_params);
  sensor_msgs::CompressedImagePtr msg_ptr =
      cv_bridge::CvImage(std_msgs::Header(), "bgra8", line_4channels_img)
          .toCompressedImageMsg(cv_bridge::PNG);
  response.image = *msg_ptr;
  response.success = true;
  return true;
}

bool MapLocalization::updateLandmarks(
    cartographer_ros_msgs::UpdateLandmarkServer::Request &request,
    cartographer_ros_msgs::UpdateLandmarkServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "Updating landmarks ...";
  LOG(INFO) << "request.landmarks.size: " << request.landmarks.size();
  if (request.landmarks.size() == 0)
  {
    response.error_message = "no landmarks!";
    LOG(WARNING) << response.error_message;
    return true;
  }
  
  if(to_ros_msg_node_.updateLandmarks(localization_path_, request.landmarks))
  {
    loadAndUpdateLandmark();
  }
  else
  {
    response.error_message = "update landmarks failed!";
    LOG(WARNING) << response.error_message;
  }
  
  return true;
}

bool MapLocalization::modifyMapServer(cartographer_ros_msgs::MapModify::Request &request,
                                      cartographer_ros_msgs::MapModify::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "modify map ...";
  LOG(INFO) << "request.modified_maps.size: " << request.modified_maps.size();
  if (request.modified_maps.size() == 0)
  {
    response.error_message = "modified_maps size is zero! ";
    LOG(WARNING) << response.error_message;
    return true;
  }
  string full_path;
  getPath(request.map_name, full_path);
  cv::Mat modify_area_img;
  if (!judgeFileExist(full_path + "modify_area.png"))
  {
    cv::Mat map_img = imread(full_path + "map.png");
    modify_area_img = cv::Mat(map_img.rows, map_img.cols, CV_8UC1, cv::Scalar(127));
  }
  else
    modify_area_img = imread(full_path + "modify_area.png", CV_LOAD_IMAGE_UNCHANGED);

  boost::property_tree::ptree map_pt;
  boost::property_tree::read_xml(full_path + "map_data.xml", map_pt);
  if (map_pt.empty())
  {
    ROS_FATAL("Load %s/map_data.xml failed!", full_path.c_str());
    return false;
  }
  string data = "";
  float map_resolution = map_pt.get<float>("mapPng.resolution");
  data = map_pt.get<std::string>("mapPng.pose");
  float pose[7];
  sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3], &pose[4],
         &pose[5], &pose[6]);
  float Max_box[2] = {pose[1], pose[0]};
  data.clear();

  for (auto &it : request.modified_maps)
  {
    cartographer_ros_msgs::SubmapImageEntry submap = it;
    int id = submap.submap_index;
    string img_full_path = full_path + "frames/" + to_string(id) + "/probability_grid.png";
    string img_origin_path = full_path + "frames/" + to_string(id) + "/probability_grid_origin.png";

    Mat org = imread(img_origin_path, CV_LOAD_IMAGE_UNCHANGED);
    // 		Mat org = imread(img_full_path,CV_LOAD_IMAGE_UNCHANGED);
    if (org.empty())
    {
      org = imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
      imwrite(img_origin_path, org);
    }

    Mat img;
    try
    {
      img = cv_bridge::toCvCopy(submap.image, sensor_msgs::image_encodings::BGRA8)->image;
    }
    catch (cv_bridge::Exception &e)
    {
      LOG(ERROR) << "cv_bridge Exception: " << e.what();
      return false;
    }

    assert(org.cols == img.cols && org.rows == img.rows);

    boost::property_tree::ptree submap_pt;
    boost::property_tree::read_xml(full_path + "frames/" + to_string(id) + "/data.xml", submap_pt);
    if (submap_pt.empty())
    {
      ROS_FATAL("Load %s/data.xml failed!", full_path.c_str());
      return false;
    }
    float submap_resolution = submap_pt.get<float>("probability_grid.resolution");
    data = submap_pt.get<std::string>("probability_grid.max");
    float submap_box[2];
    sscanf(data.c_str(), "%f %f", &submap_box[0], &submap_box[1]);
    data.clear();
    data = submap_pt.get<std::string>("pose");
    float global_pose[7];
    sscanf(data.c_str(), "%f %f %f %f %f %f %f", &global_pose[0], &global_pose[1], &global_pose[2],
           &global_pose[3], &global_pose[4], &global_pose[5], &global_pose[6]);
    cartographer::transform::Rigid3d submap_global_pose(
        Eigen::Vector3d(global_pose[0], global_pose[1], global_pose[2]),
        Eigen::Quaterniond(global_pose[6], global_pose[3], global_pose[4], global_pose[5]));
    int kk = 0;
    imwrite(full_path + "modify_tmp.png", img);
    for (int i = 0; i < img.rows; i++)
    {
      for (int j = 0; j < img.cols; j++)
      {
        int img_delta = img.at<Vec4b>(i, j)[0];
        int img_alpha = img.at<Vec4b>(i, j)[3];
        //         uint16_t img_value;
        if (img_alpha > 0)
        {
          Eigen::Vector3d point_in_submap(submap_box[0] - (i + 0.5) * submap_resolution,
                                          submap_box[1] - (j + 0.5) * submap_resolution, 0);
          Eigen::Vector3d point_in_map = submap_global_pose * point_in_submap;
          int map_i = Max_box[0] - point_in_map[0] / map_resolution;
          int map_j = Max_box[1] - point_in_map[1] / map_resolution;
          if (map_i > 0 && map_i < modify_area_img.rows && map_j > 0 &&
              map_j < modify_area_img.cols)
          {
            modify_area_img.at<uint8_t>(map_i, map_j) = img_delta;
            if (img_delta == 0) modify_area_img.at<uint8_t>(map_i, map_j) = 255 - img_alpha;
          }

          // 					uint16_t grey;
          //
          // 					grey = img_alpha * 32766. / 255;
          // 					if (!img.at<cv::Vec4b>(i, j)[0] &&
          // !img.at<cv::Vec4b>(i, j)[1] && !img.at<cv::Vec4b>(i, j)[2])
          // 					{
          // 						//brush add
          // 						grey = 32766 - grey;
          // 						if (grey == 0)
          // 							grey = 1;
          // 					}
          // 					else
          // 					{
          // 						//erase
          // 						grey = 32766;
          // 					}
          // 					org.at<uint16_t>(i, j) = grey;

          if (kk++ == 0)
          {
            LOG(WARNING) << i << ", " << j << ", " << map_i << ", " << map_j << ", " << img_alpha
                         << "\n"
                         << submap_global_pose << "\n"
                         << point_in_submap.transpose() << "\n"
                         << point_in_map.transpose();
            LOG(WARNING) << submap_box[0] << ", " << submap_box[1] << ", " << Max_box[0] << ", "
                         << Max_box[1];
          }
          // 					if(img_delta == 128 && img_alpha ==
          // 0)
          // 					{
          // 						img_value = 0;
          // 					}
          // 					else
          // 					{
          // 						double temp_alpha =
          // img_alpha/255.; 						temp_alpha = temp_alpha * temp_alpha *temp_alpha *
          // temp_alpha; 						temp_alpha *= 128; 						if(img_delta == 255)
          // 						{
          // 							temp_alpha =
          // -temp_alpha;
          // 						}
          // 						int alpha = (128 -
          // temp_alpha); 						img_value = 128 * alpha;
          // 					}
          // 					org.at<uint16_t>(i, j) =
          // img_value;
        }
      }
    }
    updateModifyArea(modify_area_img, full_path);
  }
  autoSetIgnoreArea(full_path);
  updateIgnoreArea(full_path);
  return true;
}

bool MapLocalization::getpointsInMapServer(
    cartographer_ros_msgs::GetPointsInImgFromGlobal::Request &request,
    cartographer_ros_msgs::GetPointsInImgFromGlobal::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "get points in Img ...";
  LOG(INFO) << "request.global_points.size: " << request.global_points.size();

  if (request.global_points.size() == 0)
  {
    response.error_message = "No global_points!";
    LOG(WARNING) << response.error_message;
    return false;
  }
  for (auto point : request.global_points)
  {
    // 		geometry_msgs::Point pointInGlobal = point;
    int nearestIdx = -1;
    Eigen::Isometry3d nearestPose;
    Eigen::Vector2d pointInworld_tmp(point.x, point.y);
    Eigen::Vector3d pointInworld(point.x, point.y, point.z);
    getNearestPose(pointInworld_tmp, nearestPose, nearestIdx);
    if (nearestIdx == -1)
    {
      geometry_msgs::Point p1;
      response.submap_ids.push_back(-1);
      response.img_points.push_back(p1);
      continue;
    }
    double max[2];
    max[0] = maxes_[nearestIdx * 2];
    max[1] = maxes_[nearestIdx * 2 + 1];
    Eigen::Isometry3d local2global = localInGlobal_poses_[nearestIdx];

    Eigen::Vector3d pointInLocal = local2global.inverse() * pointInworld;
    double y = (max[0] - pointInLocal(0)) / resolution_;
    double x = (max[1] - pointInLocal(1)) / resolution_;
    geometry_msgs::Point p;
    p.x = x;
    p.y = y;
    p.z = 0;
    response.submap_ids.push_back(nearestIdx);
    response.img_points.push_back(p);
  }
  return true;
}

bool MapLocalization::adjustMapPixelServer(cartographer_ros_msgs::MapModify::Request &request,
                                           cartographer_ros_msgs::MapModify::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "adjust Map Pixel ...";
  LOG(INFO) << "request.input_images.size: " << request.modified_maps.size();
  if (request.modified_maps.size() == 0)
  {
    response.error_message = "input images size is zero! ";
    LOG(WARNING) << response.error_message;
    return true;
  }
  string full_path;
  getPath(request.map_name, full_path);

  // load images if size == 0
  size_t img_cnt = 0;
  std::vector<cv::Mat> erode_images;
  for (auto &it : request.modified_maps)
  {
    int id = it.submap_index;
    if (erode_images_.size() <= img_cnt)
    {
      for (int i = 0; i < 3; i++)
      {
        int cnt = 2 * i + 1;
        if (i == 0)
        {
          cnt = 0;
        }
        string tmp_path = full_path + "frames/" + to_string(id) + "/modified_images/" + "erode_" +
                          to_string(cnt) + "_pixels/";

        Mat img16 = imread(tmp_path + "probability_grid_16bit.png", CV_LOAD_IMAGE_UNCHANGED);
        Mat img8 = imread(tmp_path + "probability_grid_8bit.png", CV_LOAD_IMAGE_UNCHANGED);
        erode_images.push_back(img16);
        erode_images.push_back(img8);
      }
      erode_images_.push_back(erode_images);
    }
    else
      erode_images = erode_images_[img_cnt];
    img_cnt++;
    sensor_msgs::CompressedImage compressed_image = it.image;
    cv_bridge::CvImagePtr p_adjusted_image = cv_bridge::toCvCopy(compressed_image, "rgba8");
    const Mat &adjusted_image = p_adjusted_image->image;

    string img_full_path = full_path + "frames/" + to_string(id) + "/probability_grid.png";
    Mat org[2];
    org[0] = imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
    org[1] = imread(full_path + "map.png", CV_LOAD_IMAGE_UNCHANGED);
    // 		assert(org.cols == adjusted_image.cols && org.rows ==
    // adjusted_image.rows);
    for (int i = 0; i < adjusted_image.rows; i++)
    {
      for (int j = 0; j < adjusted_image.cols; j++)
      {
        int img_delta = adjusted_image.at<Vec4b>(i, j)[0];
        int img_alpha = adjusted_image.at<Vec4b>(i, j)[3];
        // 				uint16_t img_value;
        if (img_alpha > 0)
        {
          if (img_delta == 1)
          {
            org[0].at<uint16_t>(i, j) = erode_images[0].at<uint16_t>(i, j);
            org[1].at<uchar>(i, j) = erode_images[1].at<uchar>(i, j);
            // 						org.at<uint16_t>(i,j) =
          }
          else if (img_delta == 2)
          {
            org[0].at<uint16_t>(i, j) = erode_images[2].at<uint16_t>(i, j);
            org[1].at<uchar>(i, j) = erode_images[3].at<uchar>(i, j);
          }
          else if (img_delta == 3)
          {
            org[0].at<uint16_t>(i, j) = erode_images[4].at<uint16_t>(i, j);
            org[1].at<uchar>(i, j) = erode_images[5].at<uchar>(i, j);
          }
        }
      }
    }
    response.error_message = "";
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(0);
    cv::imwrite(img_full_path, org[0], params);

    std_msgs::Header header;
    header.stamp = ros::Time::now();
    cartographer_ros_msgs::SubmapImageEntry entry;
    entry.submap_index = id;
    sensor_msgs::CompressedImagePtr msg_ptr =
        cv_bridge::CvImage(header, "bgra8", org[1]).toCompressedImageMsg(cv_bridge::PNG);
    entry.image = *msg_ptr;
    // 		response.fixed_maps.push_back(entry);
  }
  return true;
}

// bool MapLocalization::getErodeImages(
// 	cartographer_ros_msgs::GetImages::Request& request,
// 	cartographer_ros_msgs::GetImages::Response& response)
// {
// 	absl::MutexLock locker(&mutex_);
// 	LOG(INFO) << "adjust Map Pixel ...";
// 	LOG(INFO) << "request.map_name " <<request.map_name;
// 	std::vector<int> params;
// 	params.push_back(CV_IMWRITE_PNG_COMPRESSION);
// 	params.push_back(0);
// 	string full_path;
// 	getPath(request.map_name,full_path);
// 	for(int i =0; i < 3; i++)
// 	{
// 		int cnt = 2*i+1;
// 		if(i ==0)
// 		{
// 			cnt = 0;
// 		}
// 		string tmp_path = full_path + "frames/" +  to_string(0) +
// "/modified_images/" + "erode_"+to_string(cnt)+"_pixels/";
// 		//only 0 pakage under frames
// // 		Mat img16 = imread(tmp_path + "probability_grid_16bit.png",
// CV_LOAD_IMAGE_UNCHANGED); 		Mat img8 = imread(tmp_path +
// "probability_grid_8bit.png", CV_LOAD_IMAGE_UNCHANGED); 		if(img8.empty())
// 		{
// 			response.error_message = "can not get image!";
// 			return false;
//
// 		}
// 		std_msgs::Header header;
// 		header.stamp = ros::Time::now();
// 		sensor_msgs::CompressedImagePtr msg_ptr =
// cv_bridge::CvImage(header,"bgra8",grey8ToRgba(img8)).toCompressedImageMsg(cv_bridge::PNG);
// 		response.erode_images.push_back(*msg_ptr);
// 	}
// 	return false;
// }

bool MapLocalization::modifyLocationRegionServer(
    cartographer_ros_msgs::MapModify::Request &request,
    cartographer_ros_msgs::MapModify::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "modify ignored location region";
  string full_path;
  cv::Mat bool_image;
  getPath(request.map_name, full_path);
  bool_image = imread(full_path + "bool_image.png", CV_8UC1);
  if (bool_image.empty())
  {
    bool_image = Mat(Size(size_of_map_[0], size_of_map_[1]), CV_8UC1, Scalar(255));
  }

  cartographer_ros_msgs::SubmapImageEntry submap = request.modified_maps[0];

  Mat img;
  try
  {
    img = cv_bridge::toCvCopy(submap.image, sensor_msgs::image_encodings::BGRA8)->image;
  }
  catch (cv_bridge::Exception &e)
  {
    LOG(ERROR) << "cv_bridge Exception: " << e.what();
    return false;
  }
  for (int i = 0; i < img.rows; i++)
  {
    for (int j = 0; j < img.cols; j++)
    {
      int value = img.at<uchar>(i, j);
      if (value > 0)
      {
        bool_image.at<uchar>(i, j) = 0;
      }
    }
  }

  imwrite(full_path + "bool_image.png", bool_image);
  return true;
}

bool MapLocalization::modifyFreeSpaceRegionServer(map_manager_msgs::Set2dPoints::Request &request,
                                                  map_manager_msgs::Set2dPoints::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "modify free space region";
  modifyBoolImageFromPoints(request, "free_space_corner_points.xml", "free_space.png");
  std_srvs::SetBool srv;
  srv.request.data = true;
  reload_arm_free_space_image_client_.call(srv);
  return true;
}

bool MapLocalization::set2DPointsServer(map_manager_msgs::Set2dPoints::Request &request,
                                        map_manager_msgs::Set2dPoints::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "set 2d points";
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  string full_path;
  getPath(request.map_name, full_path);
  Mat mask = imread(full_path + "bool_image_auto.png", CV_LOAD_IMAGE_UNCHANGED);
  if (mask.empty())
  {
    Mat mask = Mat(Size(size_of_map_[0], size_of_map_[1]), CV_8UC1, Scalar(255));
    imwrite(full_path + "bool_image_auto.png", mask);
  }

  std::string data;
  boost::property_tree::ptree p_info;
  boost::property_tree::ptree p_list;
  int cnt = 0;
  for (auto &it : request.topo_corner_points)
  {
    map_manager_msgs::Points2d corner_points = it;
    vector<vector<Point>> contours;
    vector<Point> contour;
    for (auto &point : corner_points.points)
    {
      Eigen::Vector2d point_in_map(point.x, point.y);
      Eigen::Vector2i imageLoc;
      world2imageLoc(Eigen::Vector2d(maxes_[0], maxes_[1]), resolution_, point_in_map, imageLoc);
      if (imageLoc(1) > size_of_map_[1] || imageLoc(0) > size_of_map_[0] || imageLoc(0) < 0 ||
          imageLoc(1) < 0)
      {
        LOG(INFO) << "point_in_map: " << point_in_map.transpose() << endl
                  << "imageLoc: " << imageLoc.transpose() << endl
                  << "max: " << maxes_[0] << ", " << maxes_[1] << endl
                  << "size_of_map_: " << size_of_map_[0] << ", " << size_of_map_[1] << endl
                  << "resolution_: " << resolution_;
      }
      Point p(imageLoc(0), imageLoc(1));
      contour.push_back(p);
      data = data + std::to_string(point.x) + " ";
      if (point.x == corner_points.points.back().x && point.y == corner_points.points.back().y)
        data = data + std::to_string(point.y);
      else
        data = data + std::to_string(point.y) + " ";
      cnt++;
    }

    p_info.put("points_size", cnt);
    p_info.put("points", data);
    p_list.add_child("corner_points", p_info);
    LOG(INFO) << "ignore area points : " << data;
    data.clear();
    cnt = 0;
    contours.push_back(contour);
    cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
  }
  boost::property_tree::write_xml(full_path + "corner_points.xml", p_list, std::locale(), setting);

  imwrite(full_path + "bool_image.png", mask);
  return true;
}

bool MapLocalization::get2DPointsServer(map_manager_msgs::Get2dPoints::Request &request,
                                        map_manager_msgs::Get2dPoints::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "get 2d points";
  string full_path;
  getPath(request.map_name, full_path);

  if (!judgeFileExist(full_path + "corner_points.xml"))
  {
    response.error_msgs = "no " + full_path + "corner_points.xml file!";
    return true;
  }
  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(full_path + "corner_points.xml", pt);

  string xml;
  if (pt.empty())
  {
    LOG(WARNING) << "corner_points.xml is empty!";
    return true;
  }
  for (auto &mark : pt)
  {
    if (mark.first == "corner_points")
    {
      int points_size = stoi(mark.second.get("points_size", ""));
      CHECK(points_size == 4);

      xml = mark.second.get("points", " ");
      float value[8];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6], &value[7]);
      // TODO new msg will be here
      LOG(INFO) << "Ignore corner_points: " << xml.c_str();
      map_manager_msgs::Points2d corner_points;
      for (int i = 0; i < points_size; i++)
      {
        map_manager_msgs::Point2d p;
        p.x = value[i * 2];
        p.y = value[i * 2 + 1];
        corner_points.points.push_back(p);
      }
      response.topo_corner_points.push_back(corner_points);
    }
  }
  return true;
}

void MapLocalization::updateIgnoreArea(const string &full_path)
{
  if (!judgeFileExist(full_path + "corner_points.xml"))
  {
    LOG(WARNING) << "no " + full_path + "corner_points.xml file!";
    return;
  }
  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(full_path + "corner_points.xml", pt);

  string xml;
  if (pt.empty())
  {
    LOG(WARNING) << "corner_points.xml is empty!";
    return;
  }
  Mat mask = imread(full_path + "bool_image_auto.png", CV_LOAD_IMAGE_UNCHANGED);
  for (auto &mark : pt)
  {
    if (mark.first == "corner_points")
    {
      int points_size = stoi(mark.second.get("points_size", ""));
      CHECK(points_size == 4);

      xml = mark.second.get("points", " ");
      float value[8];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6], &value[7]);
      // TODO new msg will be here
      vector<Point> contour;
      for (int i = 0; i < points_size; i++)
      {
        Eigen::Vector2d point_in_map(value[i * 2], value[i * 2 + 1]);
        Eigen::Vector2i imageLoc;
        world2imageLoc(Eigen::Vector2d(maxes_[0], maxes_[1]), resolution_, point_in_map, imageLoc);
        Point p(imageLoc(0), imageLoc(1));
        contour.push_back(p);
      }
      vector<vector<Point>> contours;
      contours.push_back(contour);
      cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
    }
  }
  imwrite(full_path + "bool_image.png", mask);
  LOG(INFO) << "update bool_image.png done!";
}

bool MapLocalization::setInitPose(vtr_msgs::SetInitialPose::Request &request,
                                  vtr_msgs::SetInitialPose::Response &response)
{
  // request.base2map is base2reference.
  LOG(INFO) << "manual localization type: " << request.type
            << "\n reference_id:" << request.base2map.reference_id
            << "\n pose_valid:" << request.base2map.pose_valid << "\nrequest base2reference pose: "
            << "\n position(x,y,z):" << request.base2map.pose.pose.position.x << ", "
            << request.base2map.pose.pose.position.y << ", "
            << request.base2map.pose.pose.position.z
            << "\t orientation(x,y,z,w):" << request.base2map.pose.pose.orientation.x << ", "
            << request.base2map.pose.pose.orientation.y << ","
            << request.base2map.pose.pose.orientation.z << ", "
            << request.base2map.pose.pose.orientation.w;
  LOG(INFO) << "Manual localization: init pose come!";
  const string request_type = request.type;
  //   if (initial_pose_client_.call(pose_initial_query))
  //   {
  //     if (!pose_initial_query.response.is_succeed)
  //     {
  //       LOG(WARNING) << "/vtr/initial_pose service can't init the pose with
  //       the input values."; return false;
  //     }
  //   }
  //   else
  //   {
  //     const string error_string = "call /vtr/initial_pose service failed.";
  //     LOG(WARNING) <<  error_string;
  //     response.error_message = error_string.c_str();
  //     return false;
  //   }
  {
    vtr_msgs::GlobalLocalizationPose pose_temp;
    pose_temp.header.stamp = ros::Time::now();
    pose_temp.reference_id = request.base2map.reference_id;
    pose_temp.header.frame_id = "map";
    pose_temp.pose_valid = request.base2map.pose_valid;
    pose_temp.sensor2reference.pose.orientation.w = request.base2map.pose.pose.orientation.w;
    pose_temp.sensor2reference.pose.orientation.x = request.base2map.pose.pose.orientation.x;
    pose_temp.sensor2reference.pose.orientation.y = request.base2map.pose.pose.orientation.y;
    pose_temp.sensor2reference.pose.orientation.z = request.base2map.pose.pose.orientation.z;

    pose_temp.sensor2reference.pose.position.x = request.base2map.pose.pose.position.x;
    pose_temp.sensor2reference.pose.position.y = request.base2map.pose.pose.position.y;
    pose_temp.sensor2reference.pose.position.z = request.base2map.pose.pose.position.z;
    //     pose_temp.sensor2odom
    geometry_msgs::TransformStamped base2odom_tf;
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

    string odom_name = "odom";
    string base_footprint_name = "base_footprint";
    if (request.type == "Texture")
    {
      odom_name = "fm_odom";
      base_footprint_name = "fm_base_footprint";
    }
    else if (request.type == "Tag")
    {
      odom_name = "tag_odom";
      base_footprint_name = "tag_base_footprint";
    }

    while (ros::ok())
    {
      try
      {
        base2odom_tf = tfBuffer_.lookupTransform(odom_name, base_footprint_name, ros::Time(0));
        if (base2odom_tf.header.stamp >= pose_temp.header.stamp) break;
      }
      catch (tf::TransformException ex)
      {
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
        if (duration.count() > 2)
        {
          LOG(WARNING) << ex.what();
          response.is_succeed = false;
          response.error_message = "Can't get transform between odom and base_footprint!";
          return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
    }
    pose_temp.header.stamp = base2odom_tf.header.stamp;
    pose_temp.sensor2odom.position.x = base2odom_tf.transform.translation.x;
    pose_temp.sensor2odom.position.y = base2odom_tf.transform.translation.y;
    pose_temp.sensor2odom.position.z = base2odom_tf.transform.translation.z;

    pose_temp.sensor2odom.orientation.x = base2odom_tf.transform.rotation.x;
    pose_temp.sensor2odom.orientation.y = base2odom_tf.transform.rotation.y;
    pose_temp.sensor2odom.orientation.z = base2odom_tf.transform.rotation.z;
    pose_temp.sensor2odom.orientation.w = base2odom_tf.transform.rotation.w;

    LOG(INFO) << "request base2odom pose: "
              << "\n position(x,y,z):" << pose_temp.sensor2odom.position.x << ", "
              << pose_temp.sensor2odom.position.y << ", " << pose_temp.sensor2odom.position.z
              << "\t orientation(x,y,z,w):" << pose_temp.sensor2odom.orientation.x << ", "
              << pose_temp.sensor2odom.orientation.y << "," << pose_temp.sensor2odom.orientation.z
              << ", " << pose_temp.sensor2odom.orientation.w;
    for (int i = 0; i < 6; i++)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-9;
    }
    pose_temp.localization_type = "dragging";
    //     global_localization_pub_.publish(pose_temp);
    pose_temp.pose_valid = true;
    vtr_msgs::SetGlobalPose global_pose_srv;
    global_pose_srv.request.pose = pose_temp;
    int k = 0;
    while (ros::ok())
    {
      if (request.type == "Texture")
      {
        if (texture_initial_pose_client_.call(global_pose_srv))
        {
          LOG(INFO) << "initial_pose client call success!";
          break;
        }
        else
        {
          k++;
          if (k > 20)
          {
            LOG(ERROR) << "initial_pose_client_ call failed!";
            return false;
          }
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
      }
      else if (request.type == "Tag")
      {
        if (tag_initial_pose_client_.call(global_pose_srv))
        {
          LOG(INFO) << "initial_pose client call success!";
          break;
        }
        else
        {
          k++;
          if (k > 20)
          {
            LOG(ERROR) << "initial_pose_client_ call failed!";
            return false;
          }
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
      }
      else
      {
        if (initial_pose_client_.call(global_pose_srv))
        {
          LOG(INFO) << "initial_pose client call success!";
          break;
        }
        else
        {
          k++;
          if (k > 20)
          {
            LOG(ERROR) << "initial_pose_client_ call failed!";
            return false;
          }
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
      }
    }
  }
  if (request.type != "Texture" && request.type != "Tag" && request_type != "DraggingOnly")
  {
    ros::Duration(0.06).sleep();
    localization_request_pub_.publish(std_msgs::Empty());
  }
  response.is_succeed = true;
  return true;
}

void MapLocalization::handleLandmarkLocalizing(
    const cartographer_ros_msgs::LandmarkListConstPtr &p_landmarkList)
{
  //   if(!tag_poses_.empty())
  //   {
  //     for (auto p_landmark = p_landmarkList->landmarks.cbegin();
  //        p_landmark != p_landmarkList->landmarks.cend(); p_landmark++)
  //     {
  //
  //     }
  //     return;
  //   }
  bool use_tag_map = (!tag_poses_.empty());
  if (use_tag_map)
  {
    LOG_EVERY_N(INFO, 200) << "use_tag_map";
  }
  else
  {
    LOG_EVERY_N(INFO, 200) << "not use_tag_map";
  }
  const std::map<int, Eigen::Isometry3d> landmark_full_map =
      use_tag_map ? tag_poses_ : landmark_full_map_;
  const auto posesInGlobal = use_tag_map ? posesInGlobal_in_tag_map_ : posesInGlobal_;
  static boost::circular_buffer<double> tagmark_distance_cache(tagmark_localize_buffer_size_,
                                                               100000000000.);
  static boost::circular_buffer<geometry_msgs::PoseStamped> tagmark_base2world_cache(
      tagmark_localize_buffer_size_, geometry_msgs::PoseStamped());
  static boost::circular_buffer<int> tag_ids(tagmark_localize_buffer_size_, -1);
  LOG_EVERY_N(INFO, 200) << "Recieve " << p_landmarkList->landmarks.size() << " Landmarks.";
  bool is_tagmark = false;
  visualization_msgs::MarkerArray landmark_find;
  for (auto p_landmark = p_landmarkList->landmarks.cbegin();
       p_landmark != p_landmarkList->landmarks.cend(); p_landmark++)
  {
    // ROS_INFO("receive landmark num: %d",p_landmarkList->landmark.size());
    int id;
    geometry_msgs::PoseStamped landmark2base_pose;
    //     double translation_weight;
    //     double rotation_weight;
    id = std::stoi(p_landmark->id);
    landmark2base_pose.header.stamp = p_landmarkList->header.stamp;
    landmark2base_pose.pose = p_landmark->tracking_from_landmark_transform;
    //     translation_weight = p_landmark->translation_weight;
    //     rotation_weight = p_landmark->rotation_weight;
    //     if(id >= 1000000)//not use Lasermarks
    //       continue;

    if (landmark_full_map.find(id) == landmark_full_map.end()) continue;
    // TODO坐标变换
    Eigen::Isometry3d landmark2base, base2world;
    geometry_msgs::PoseStamped base2world_pose;
    tf::poseMsgToEigen(landmark2base_pose.pose, landmark2base);
    base2world = landmark_full_map.at(id) * landmark2base.inverse();

    tf::poseEigenToMsg(base2world, base2world_pose.pose);
    base2world_pose.header.stamp = p_landmarkList->header.stamp;

    // 压入
    visualization_msgs::Marker landmark;

    landmark.id = id;
    if (id >= 5000000)
      continue;
    else if (id >= 1000000)
    {
      landmark.id = id - 1000000;
      landmark.ns = "Lasermarks";
    }
    else if (id >= 44032)
      landmark.ns = "Visualmarks_4x4";
    else
      landmark.ns = "Visualmarks";

    if (id < 1000000)
    {
      is_tagmark = true;
      double distance = landmark2base_pose.pose.position.x * landmark2base_pose.pose.position.x +
                        landmark2base_pose.pose.position.y * landmark2base_pose.pose.position.y;
      tagmark_distance_cache.push_back(distance);
      tagmark_base2world_cache.push_back(base2world_pose);
      tag_ids.push_back(id);
    }
    landmark.pose = base2world_pose.pose;
    landmark_find.markers.push_back(landmark);
  }

  // push MSG;
  if (!landmark_find.markers.empty() &&
      (p_landmarkList->header.stamp - last_pub_marker_time_).toSec() > 0.3)
  {
    landmark_detect_pub_.publish(landmark_find);
    landmark_detect_pub2_.publish(landmark_find);
    LOG_EVERY_N(INFO, 50) << "publish " << landmark_find.markers.size() << " detected landmarks.";
    last_pub_marker_time_ = p_landmarkList->header.stamp;
  }

  if (!is_tagmark)
  {
    tagmark_distance_cache.push_back(100000000000.);
    tagmark_base2world_cache.push_back(geometry_msgs::PoseStamped());
    tag_ids.push_back(-1);
  }

  if ((ros::Time::now() - tag_Initialization_lasttime_).toSec() < tagmark_localize_interval_)
  {
    return;
  }

  // check if the tenth tag before now is the nearest pose
  if (tagmark_localize_buffer_size_ > 1)
  {
    for (int i = 1; i < tagmark_distance_cache.size(); i++)
    {
      if (tagmark_distance_cache[0] >= tagmark_distance_cache[i]) return;
    }
  }
  else
  {
    if (tagmark_distance_cache[0] == 100000000000.) return;
  }

  absl::MutexLock locker(&mutex_);
  // use tag to do localizing
  // if detect more than one tag, just use  the first tag
  Eigen::Vector2d worldLoc;
  worldLoc[0] = tagmark_base2world_cache[0].pose.position.x;
  worldLoc[1] = tagmark_base2world_cache[0].pose.position.y;
  Eigen::Isometry3d base2world;
  tf::poseMsgToEigen(tagmark_base2world_cache[0].pose, base2world);

  double yaw, pitch, roll;
  Eigen::Quaterniond tmp_q(base2world.rotation());
  //   tf::Matrix3x3(tf::Quaternion(tmp_q.x(),tmp_q.y(),tmp_q.z(),tmp_q.w())).getRPY(roll,
  //   pitch, yaw); if( fabs(roll - 3.1415) < 0.2 || fabs(pitch - 3.1415) < 0.2)
  //   {
  //     LOG(INFO) << roll << ", " << pitch << ", " <<yaw;
  //     yaw += M_PI;
  //   }
  Eigen::Isometry3d targetPoseInWorld;
  Eigen::Matrix3d rotMat = tmp_q.toRotationMatrix();
  //     (Eigen::AngleAxisd(yaw, Eigen::Matrix<double, 3, 1>::UnitZ()))
  //     .toRotationMatrix();
  targetPoseInWorld.setIdentity();
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      targetPoseInWorld(i, j) = rotMat(i, j);
  targetPoseInWorld(0, 3) = worldLoc[0];
  targetPoseInWorld(1, 3) = worldLoc[1];
  targetPoseInWorld(2, 3) = 0;

  int nearestIdx = -1;
  Eigen::Isometry3d nearestPose;
  double minDistance = 100000;
  //   LOG(WARNING) << "worldLoc: " << worldLoc.transpose();
  for (uint i = 0; i < posesInGlobal.size(); i++)
  {
    Eigen::Vector2d dis_vec =
        Eigen::Vector2d(posesInGlobal[i](0, 3), posesInGlobal[i](1, 3)) - worldLoc;

    double distance = dis_vec.norm();
    if (distance < minDistance)
    {
      minDistance = distance;
      nearestIdx = i;

      for (int row = 0; row < 4; row++)
        for (int col = 0; col < 4; col++)
          nearestPose(row, col) = posesInGlobal[i](row, col);
    }
  }

  if (nearestIdx == -1)
  {
    LOG(WARNING) << "Invalid map with " << posesInGlobal.size() << " poses inside!";
    // throw std::runtime_error(error_string);
    return;
  }

  Eigen::Isometry3d target2reference;
  target2reference = nearestPose.inverse() * targetPoseInWorld;
  // LOG(INFO) << target2reference.matrix();
  //    LOG(INFO) <<"nearestPose: \n" << nearestPose.matrix();
  //    localization_cnt_ = 0;

  //   vtr_msgs::Pose pose_reference_current_msg;
  //   pose_reference_current_msg.header.stamp =
  //   tagmark_base2world_cache[0].header.stamp;
  //   pose_reference_current_msg.reference_id = nearestIdx;
  //   pose_reference_current_msg.pose_valid = true;
  //
  //   //initial_pose_pub_.publish(pose_reference_current_msg);
  //
  //   vtr_msgs::SetInitialPose pose_initial_query;
  //   pose_initial_query.request.base2map = pose_reference_current_msg;
  //   if (!initial_pose_client_.call(pose_initial_query))
  //   {
  //     LOG(ERROR) <<"[" <<ros::Time::now() <<"] " << "call initialPoseServer
  //     failed, desc: " << pose_initial_query.response.error_message.c_str();
  //   t}
  const string frame_id_prefix = use_tag_map ? "tag_" : "";
  {
    vtr_msgs::GlobalLocalizationPose pose_temp;
    pose_temp.header.stamp = tagmark_base2world_cache[0].header.stamp;
    pose_temp.header.frame_id = frame_id_prefix + "map";
    tf::poseEigenToMsg(target2reference, pose_temp.sensor2reference.pose);

    geometry_msgs::TransformStamped base2odom_tf;
    //     tf::TransformListener listener;
    //     // find tf between sensor frame to base frame
    //     tf::StampedTransform tdddransformTF;
    //     try {
    //         listener.waitForTransform("odom", "base_footprint",
    //         pose_temp.header.stamp, ros::Duration(10.0) );
    //         listener.lookupTransform("odom", "base_footprint",
    //         pose_temp.header.stamp, transformTF);
    //     } catch (tf::TransformException ex) {
    //         ROS_ERROR("%s",ex.what());
    //     }
    {
      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

      while (ros::ok())
      {
        try
        {
          base2odom_tf = tfBuffer_.lookupTransform(
              frame_id_prefix + "odom", frame_id_prefix + "base_footprint", pose_temp.header.stamp);
          break;
        }
        catch (tf::TransformException ex)
        {
          std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
          if (duration.count() > 0.5)
          {
            LOG(WARNING) << "Can't get transform between odom and base_footprint!";
            return;
          }
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
      }
    }

    pose_temp.sensor2odom.position.x = base2odom_tf.transform.translation.x;
    pose_temp.sensor2odom.position.y = base2odom_tf.transform.translation.y;
    pose_temp.sensor2odom.position.z = base2odom_tf.transform.translation.z;

    pose_temp.sensor2odom.orientation.x = base2odom_tf.transform.rotation.x;
    pose_temp.sensor2odom.orientation.y = base2odom_tf.transform.rotation.y;
    pose_temp.sensor2odom.orientation.z = base2odom_tf.transform.rotation.z;
    pose_temp.sensor2odom.orientation.w = base2odom_tf.transform.rotation.w;
    for (int i = 0; i < 6; i++)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-8;
    }
    pose_temp.localization_type = "visualMark";
    pose_temp.pose_valid = true;
    //     pose_temp.reference_id = tag_ids[0];
    pose_temp.reference_id = nearestIdx;

    geometry_msgs::TransformStamped base2map_tf;
    {
      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

      while (ros::ok())
      {
        try
        {
          base2map_tf = tfBuffer_.lookupTransform(
              frame_id_prefix + "map", frame_id_prefix + "base_footprint", pose_temp.header.stamp);
          break;
        }
        catch (tf::TransformException ex)
        {
          std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
          if (duration.count() > 0.5)
          {
            LOG(WARNING) << "Can't get transform between map and base_footprint!";
            return;
          }
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
      }
    }
    geometry_msgs::Pose tagMapPose;
    tagMapPose.position.x = base2map_tf.transform.translation.x;
    tagMapPose.position.y = base2map_tf.transform.translation.y;
    tagMapPose.position.z = base2map_tf.transform.translation.z;

    tagMapPose.orientation.x = base2map_tf.transform.rotation.x;
    tagMapPose.orientation.y = base2map_tf.transform.rotation.y;
    tagMapPose.orientation.z = base2map_tf.transform.rotation.z;
    tagMapPose.orientation.w = base2map_tf.transform.rotation.w;

    if (use_tag_map)
    {
      LOG_EVERY_N(INFO, 200) << "use_tag_map";
      if (tag_id_with_submap_id_.find(tag_ids[0]) != tag_id_with_submap_id_.end())
      {
        pose_temp.reference_id = tag_id_with_submap_id_[tag_ids[0]];
        tag_global_localization_pub_.publish(pose_temp);
      }

      bool detectVisualmarksRes = true;
      LOG_EVERY_N(INFO, 200) << "detectVisualmarksRes = true";
      if (landmark_find.markers.size() != 0 && landmark_find.markers.back().id < 1000000)
      {
        LOG_EVERY_N(INFO, 200) << "landmark_find.markers.size() != 0";
        if (detectVisualmarksFlag_)
        {
          LOG_EVERY_N(INFO, 200) << "detectVisualmarksFlag = true";
          mapLocalization::TagInfo recvTagInfo;
          geometry_msgs::Pose recvGlobalPose;
          recvTagInfo.tagPose = landmark_find.markers.back().pose;
          recvTagInfo.tagMapId = landmark_find.markers.back().id;
          recvGlobalPose = tagMapPose;
          detectVisualmarksRes = detectVisualmarks(recvTagInfo, recvGlobalPose);
        }
      }
      else
      {
        LOG(INFO) << "landmark_find.markers.size() = " << landmark_find.markers.size();
        LOG(INFO) << "landmark_find.markers.back().id = " << landmark_find.markers.back().id;
      }

      if (landmark_full_map_.find(tag_ids[0]) != landmark_full_map_.end())
      {
        LOG(INFO) << "landmark_full_map_ find = true";
        target2reference = landmark_full_map_.at(tag_ids[0]) *
                           landmark_full_map.at(tag_ids[0]).inverse() * targetPoseInWorld;
        tf::poseEigenToMsg(target2reference, pose_temp.sensor2reference.pose);
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
        while (ros::ok())
        {
          try
          {
            base2odom_tf =
                tfBuffer_.lookupTransform("odom", "base_footprint", pose_temp.header.stamp);
            break;
          }
          catch (tf::TransformException ex)
          {
            std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
            if (duration.count() > 0.5)
            {
              LOG(WARNING) << "Can't get transform between odom and base_footprint!";
              return;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
          }
        }

        pose_temp.sensor2odom.position.x = base2odom_tf.transform.translation.x;
        pose_temp.sensor2odom.position.y = base2odom_tf.transform.translation.y;
        pose_temp.sensor2odom.position.z = base2odom_tf.transform.translation.z;

        pose_temp.sensor2odom.orientation.x = base2odom_tf.transform.rotation.x;
        pose_temp.sensor2odom.orientation.y = base2odom_tf.transform.rotation.y;
        pose_temp.sensor2odom.orientation.z = base2odom_tf.transform.rotation.z;
        pose_temp.sensor2odom.orientation.w = base2odom_tf.transform.rotation.w;

        for (int i = 0; i < 6; i++)
        {
          pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-8;
        }
        pose_temp.reference_id = 0;
        if (detectVisualmarksRes)
        {
          global_localization_pub_.publish(pose_temp);
        }
      }
    }
    else
      global_localization_pub_.publish(pose_temp);
  }

  tagmark_base2world_cache.pop_front();
  tag_ids.pop_front();
  tag_Initialization_lasttime_ = ros::Time::now();
}

bool MapLocalization::getAutoIgnoredArea(map_manager_msgs::GetImage::Request &request,
                                         map_manager_msgs::GetImage::Response &response)
{
  LOG(INFO) << "getAutoIgnoredArea";
  string path = "";
  getPath(request.map_name, path);
  cv::Mat origin_bool_image = imread(path + "bool_image_auto.png", CV_LOAD_IMAGE_UNCHANGED);
  if (origin_bool_image.empty())
  {
    response.success = false;
    response.error_message = "There is not the image named bool_image_auto.png!";
    LOG(WARNING) << response.error_message;
    return true;
  }
  cv::Mat bool_image_auto;
  if (origin_bool_image.type() != CV_8UC1)
    cv::cvtColor(origin_bool_image, bool_image_auto, CV_BGR2GRAY);
  else
    bool_image_auto = origin_bool_image.clone();
  cv::Mat pub_img(bool_image_auto.rows, bool_image_auto.cols, CV_8UC4, Scalar(255, 255, 255, 0));
  for (int x = 0; x < bool_image_auto.rows; ++x)
  {
    for (int y = 0; y < bool_image_auto.cols; ++y)
    {
      if (bool_image_auto.at<uint8_t>(x, y) == 0)
      {
        pub_img.at<Vec4b>(x, y)[0] = 0;
        pub_img.at<Vec4b>(x, y)[1] = 140;
        pub_img.at<Vec4b>(x, y)[2] = 255;
        pub_img.at<Vec4b>(x, y)[3] = 0.05 * 255;
      }
    }
  }
  std_msgs::Header header;
  header.stamp = ros::Time::now();
  header.frame_id = "";
  sensor_msgs::CompressedImagePtr msg_ptr =
      cv_bridge::CvImage(header, "bgra8", pub_img).toCompressedImageMsg(cv_bridge::PNG);
  response.image = *msg_ptr;
  response.success = true;
  return true;
}

bool MapLocalization::loadTagMap(cartographer_ros_msgs::LandmarkListServer::Request &request,
                                 cartographer_ros_msgs::LandmarkListServer::Response &response)
{
  LOG(INFO) << "loadTagMap";
  tag_poses_.clear();
  if (request.map_name == "")
  {
    response.error_message = "shuting down the tag map.";
    return true;
  }
  std::string laserMapPath;

  if (!::ros::param::get("/vtr/path", laserMapPath))
  {
    LOG(ERROR) << "Can not get laser path!";
    return false;
  }

  string full_path = request.map_name + "/";
  // getPath(request.map_name, full_path);
  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(full_path + "tag_global_poses.xml", pt,
                                             boost::property_tree::xml_parser::trim_whitespace);
  if (pt.empty())
  {
    LOG(WARNING) << "tag_global_poses.xml is empty!";
    return true;
  }

  std::unordered_map<int, geometry_msgs::Pose> tagMapIdWithPose;
  if (!getTagMapPoseInLaserMap(full_path, laserMapPath, tagMapIdWithPose))
  {
    LOG(WARNING) << "getTagMapPoseInLaserMap failed!";
    return true;
  }
  string xml, xml_id, submap_id;
  string ns;
  //    bool rewrite = false;
  std::map<int, Eigen::Isometry3d> poses_with_submap_id;
  for (auto &mark : pt)
  {
    if (mark.first == "tag")
    {
      ns = mark.second.get("ns", "");
      xml_id = mark.second.get("tag_id", " ");
      xml = mark.second.get("global_pose", " ");
      submap_id = mark.second.get("id", "");
      float value[7];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6]);
      int id = stoi(xml_id);
      Eigen::Isometry3d landmark2world;
      tf::poseMsgToEigen(tagMapIdWithPose[id], landmark2world);
      tag_poses_[id] = landmark2world;
      poses_with_submap_id[stoi(submap_id)] = landmark2world;
      tag_id_with_submap_id_[id] = stoi(submap_id);
      cartographer_ros_msgs::LandmarkNewEntry landmark;
      landmark.id = xml_id;
      landmark.ns = ns;
      landmark.visible = 1;
      landmark.tracking_from_landmark_transform = tagMapIdWithPose[id];
      response.landmarks.push_back(landmark);
      // tagMapIdWithPose.insert(std::make_pair(std::stoi(xml_id),
      // std::move(pose)));
    }
  }

  // if (!landmarkEmpty)
  // {
  //   for (const auto &tmp : landmarkIdWithPose)
  //   {
  //     if (tagMapIdWithPose.find(tmp.first) != tagMapIdWithPose.end())
  //     {
  //       geometry_msgs::Pose tmpPose;
  //       Eigen::Isometry3d tmpLandmark2world;
  //       double x = tagMapIdWithPose[tmp.first].position.x -
  //       tmp.second.position.x; double y =
  //       tagMapIdWithPose[tmp.first].position.y - tmp.second.position.y; for
  //       (auto &tagMap : poses_with_submap_id)
  //       {
  //         tf::poseEigenToMsg(tagMap.second, tmpPose);
  //         tmpPose.orientation = tmp.second.orientation;
  //         tmpPose.position.x -= x;
  //         tmpPose.position.y -= y;
  //         tf::poseMsgToEigen(tmpPose, tagMap.second);
  //       }
  //       break;
  //     }
  //   }
  // }

  for (auto it : poses_with_submap_id)
    posesInGlobal_in_tag_map_.push_back(it.second);
  return true;
}

bool MapLocalization::modifyLandmarksId(map_manager_msgs::IdRemapSrv::Request &request,
                                        map_manager_msgs::IdRemapSrv::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "Updating landmarks ...";
  LOG(INFO) << "request.landmarks.size: " << request.ids.size();
  if (request.ids.size() == 0)
  {
    response.error_msgs = "no landmarks!";
    LOG(WARNING) << response.error_msgs;
    response.success = false;
    return true;
  }
  response.success = true;
  if(!to_ros_msg_node_.modifyLandmarksId(request.map_path, request.ids))
  {
    response.error_msgs = "modify failed!";
    response.success = false;
  }
  else
    loadAndUpdateLandmark();
  LOG(WARNING) << response.error_msgs;
  return true;
}

void MapLocalization::clear()
{
  for (auto &srv : servers_)
    srv.shutdown();
  landmark_detect_sub_.shutdown();
  landmarklist_localizing_pub_.shutdown();
  localization_request_pub_.shutdown();
  landmark_detect_pub_.shutdown();
  global_localization_pub_.shutdown();
  initial_pose_client_.shutdown();
}

bool MapLocalization::modifyBoolImageFromPoints(map_manager_msgs::Set2dPoints::Request &request,
                                                const std::string &xml_name,
                                                const std::string &image_name)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "set 2d points";
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  string full_path;
  getPath(request.map_name, full_path);
  Mat mask = Mat(Size(size_of_map_[0], size_of_map_[1]), CV_8UC1, Scalar(255));

  std::string data;
  boost::property_tree::ptree p_info;
  boost::property_tree::ptree p_list;
  int cnt = 0;
  for (auto &it : request.topo_corner_points)
  {
    map_manager_msgs::Points2d corner_points = it;
    vector<vector<Point>> contours;
    vector<Point> contour;
    for (auto &point : corner_points.points)
    {
      Eigen::Vector2d point_in_map(point.x, point.y);
      Eigen::Vector2i imageLoc;
      world2imageLoc(Eigen::Vector2d(maxes_[0], maxes_[1]), resolution_, point_in_map, imageLoc);
      if (imageLoc(1) > size_of_map_[1] || imageLoc(0) > size_of_map_[0] || imageLoc(0) < 0 ||
          imageLoc(1) < 0)
      {
        LOG(INFO) << "point_in_map: " << point_in_map.transpose() << endl
                  << "imageLoc: " << imageLoc.transpose() << endl
                  << "max: " << maxes_[0] << ", " << maxes_[1] << endl
                  << "size_of_map_: " << size_of_map_[0] << ", " << size_of_map_[1] << endl
                  << "resolution_: " << resolution_;
      }
      Point p(imageLoc(0), imageLoc(1));
      contour.push_back(p);
      data = data + std::to_string(point.x) + " ";
      if (point.x == corner_points.points.back().x && point.y == corner_points.points.back().y)
        data = data + std::to_string(point.y);
      else
        data = data + std::to_string(point.y) + " ";
      cnt++;
    }

    p_info.put("points_size", cnt);
    p_info.put("points", data);
    p_list.add_child("corner_points", p_info);
    LOG(INFO) << "ignore area points : " << data;
    data.clear();
    cnt = 0;
    contours.push_back(contour);
    cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
  }
  boost::property_tree::write_xml(full_path + xml_name, p_list, std::locale(), setting);

  imwrite(full_path + image_name, mask);
  return true;
}

bool MapLocalization::getTagMapPoseInLaserMap(
    const std::string &tagMapPath, const std::string &laserMapPath,
    std::unordered_map<int, geometry_msgs::Pose> &tagMapIdWithPose)
{
  LOG(INFO) << "maps align srv...";
  std::string laser_map_path_ = laserMapPath;
  std::string tag_map_path_ = tagMapPath;
  LOG(INFO) << "laser_map_path_: " << laser_map_path_;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  std::map<int, cartographer::transform::Rigid3d> tag_map_poses_;
  std::vector<map_manager_msgs::TagPose> tags_in_laser_map;
  // load tag map
  LOG(INFO) << "load tag map: ";
  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(tag_map_path_ + "/tag_global_poses.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "tag_global_poses.xml is empty!";
      return false;
    }
    std::string xml, tag_id;
    for (auto &mark : pt)
    {
      // if (mark.first == "landmark")
      // {
      //   int visible = std::stoi(mark.second.get("visible",""));
      //   if(!visible)
      //     continue;
      tag_id = mark.second.get("tag_id", "");
      xml = mark.second.get("global_pose", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      cartographer::transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                               {value[6], value[3], value[4], value[5]});
      int id = std::stoi(tag_id);
      LOG(INFO) << id << ": " << tag2map;
      tag_map_poses_[id] = tag2map;
      // }
    }
  }
  LOG(INFO) << "load laser map: ";
  std::map<int, cartographer::transform::Rigid3d> tags_in_laser_map_;

  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(laser_map_path_ + "/landmark.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "landmark.xml is empty!";
      //       response.success = false;
      //       response.error_msgs = "laser map: landmark.xml is empty!";
      for (const auto &it : tag_map_poses_)
      {
        geometry_msgs::Pose pose;
        pose.position.x = it.second.translation().x();
        pose.position.y = it.second.translation().y();

        pose.orientation.w = it.second.rotation().w();
        pose.orientation.x = it.second.rotation().x();
        pose.orientation.y = it.second.rotation().y();
        pose.orientation.z = it.second.rotation().z();
        tagMapIdWithPose.insert(std::make_pair(it.first, std::move(pose)));
      }
      return true;
    }
    std::string xml, xml_id;
    std::string ns;
    //     bool rewrite = false;
    for (auto &mark : pt)
    {
      if (mark.first == "landmark")
      {
        int visible = std::stoi(mark.second.get("visible", ""));
        if (!visible) continue;
        ns = mark.second.get("ns", "");
        xml_id = mark.second.get("id", " ");
        xml = mark.second.get("transform", " ");
        float value[7];
        std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                    &value[4], &value[5], &value[6]);
        if (ns != "Visualmarks" && ns != "Visualmarks_4x4") continue;

        cartographer::transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                                 {value[6], value[3], value[4], value[5]});
        int id = std::stoi(xml_id);
        tags_in_laser_map_[id] = tag2map;
        LOG(INFO) << id << ": " << tag2map;
      }
    }
  }

  std::vector<cv::Point2f> src_points, ref_points;

  std::vector<std::pair<cartographer::transform::Rigid3d, cartographer::transform::Rigid3d>>
      tag_poses_pair;
  for (const auto &it : tags_in_laser_map_)
  {
    if (tag_map_poses_.find(it.first) != tag_map_poses_.end())
    {
      map_manager_msgs::TagPose tag_in_laser_map;
      tag_in_laser_map.id = it.first;
      tag_in_laser_map.pose.position.x = it.second.translation().x();
      tag_in_laser_map.pose.position.y = it.second.translation().y();

      tag_in_laser_map.pose.orientation.w = it.second.rotation().w();
      tag_in_laser_map.pose.orientation.x = it.second.rotation().x();
      tag_in_laser_map.pose.orientation.y = it.second.rotation().y();
      tag_in_laser_map.pose.orientation.z = it.second.rotation().z();
      tags_in_laser_map.push_back(tag_in_laser_map);

      //       src_points.push_back({tag_map_poses_[it.first].translation().x(),
      //       tag_map_poses_[it.first].translation().y()});
      //       ref_points.push_back({it.second.translation().x(),
      //       it.second.translation().y()});

      tag_poses_pair.push_back(std::make_pair(it.second, tag_map_poses_[it.first]));
    }
  }

  if (tags_in_laser_map.empty())
  {
    return true;
  }

  for (const auto &it : tag_map_poses_)
  {
    if (tags_in_laser_map_.find(it.first) != tags_in_laser_map_.end()) continue;
    std::map<float, std::pair<cartographer::transform::Rigid3d, cartographer::transform::Rigid3d>>
        sort_poses;
    for (const auto &pair : tag_poses_pair)
    {
      float det_t = (it.second.translation() - pair.second.translation()).norm();
      sort_poses[det_t] = pair;
    }

    std::vector<std::pair<float, cartographer::transform::Rigid3d>> dis_with_poses;
    float dis_sum = 0;
    for (const auto &sort_pose : sort_poses)
    {
      if (dis_with_poses.size() == 2) break;
      dis_with_poses.push_back(std::make_pair(
          sort_pose.first, sort_pose.second.first * sort_pose.second.second.inverse()));
      dis_sum += sort_pose.first;
    }
    std::vector<std::pair<float, cartographer::transform::Rigid3d>> weight_with_poses;
    for (const auto &dis_pose : dis_with_poses)
    {
      float weight = dis_pose.first / dis_sum;
      weight_with_poses.push_back(std::make_pair(weight, dis_pose.second));
    }

    cartographer::transform::Rigid3d tag2laser_pose;
    if (weight_with_poses.size() == 2)
    {
      Eigen::Quaterniond q1 = weight_with_poses[0].second.rotation();
      Eigen::Quaterniond q2 = weight_with_poses[1].second.rotation();
      Eigen::Quaterniond q = q1.slerp(weight_with_poses[1].first, q2);
      Eigen::Vector3d t = weight_with_poses[0].first * weight_with_poses[0].second.translation() +
                          weight_with_poses[1].first * weight_with_poses[1].second.translation();
      tag2laser_pose = cartographer::transform::Rigid3d(t, q);
    }
    else
      tag2laser_pose = weight_with_poses[0].second;

    cartographer::transform::Rigid3d laser_map_pose = tag2laser_pose * it.second;
    map_manager_msgs::TagPose tag_in_laser_map;
    tag_in_laser_map.id = it.first;
    tag_in_laser_map.pose.position.x = laser_map_pose.translation().x();
    tag_in_laser_map.pose.position.y = laser_map_pose.translation().y();

    tag_in_laser_map.pose.orientation.w = laser_map_pose.rotation().w();
    tag_in_laser_map.pose.orientation.x = laser_map_pose.rotation().x();
    tag_in_laser_map.pose.orientation.y = laser_map_pose.rotation().y();
    tag_in_laser_map.pose.orientation.z = laser_map_pose.rotation().z();
    tags_in_laser_map.push_back(tag_in_laser_map);

    //     src_points.push_back({tag_map_poses_[it.first].translation().x(),
    //     tag_map_poses_[it.first].translation().y()});
    //     ref_points.push_back({it.second.translation().x(),
    //     it.second.translation().y()});
  }

  boost::property_tree::ptree p_list;
  for (int i = 0; i < tags_in_laser_map.size(); i++)
  {
    boost::property_tree::ptree p_landmark_info;
    p_landmark_info.put("tag_id", tags_in_laser_map[i].id);
    std::string data;
    data = std::to_string(tags_in_laser_map[i].pose.position.x) + " " +
           std::to_string(tags_in_laser_map[i].pose.position.y) + " " +
           std::to_string(tags_in_laser_map[i].pose.position.z) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.w) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.x) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.y) + " " +
           std::to_string(tags_in_laser_map[i].pose.orientation.z);
    p_landmark_info.put("tag_in_laser_map_pose", data);
    p_list.add_child("tag", p_landmark_info);
    data.clear();
    tagMapIdWithPose.insert(std::make_pair(tags_in_laser_map[i].id, tags_in_laser_map[i].pose));
  }
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(tag_map_path_ + "/tag_in_laser_map.xml", p_list, std::locale(),
                                  setting);

  LOG(INFO) << "done..";
  return true;
}

bool MapLocalization::detectLandmarkServer(
    map_manager_msgs::DetectVisualmarksService::Request &request,
    map_manager_msgs::DetectVisualmarksService::Response &response)
{
  // 传入tagMapPath
  std::string tagMapPath = request.tag_map_path;
  if (tagMapPath == "")
  {
    response.success = false;
    response.error_msgs = "Tag map path is null!";
    detectVisualmarksFlag_ = false;
    return true;
  }

  {
    std::lock_guard<std::mutex> lock(mx_);
    if (!getTagGlobalPosesXmlInfo(tagMapPath))
    {
      response.success = false;
      response.error_msgs = "Get tag_global_poses.xml error!";
      detectVisualmarksFlag_ = false;
      return true;
    }

    if (!getTagMapXmlInfo(tagMapPath))
    {
      response.success = false;
      response.error_msgs = "Get tag map.xml error!";
      detectVisualmarksFlag_ = false;
      return true;
    }
  }

  detectVisualmarksFlag_ = true;
  response.success = true;
  response.error_msgs = "Start detecting!";
  return true;
}

bool MapLocalization::getTagMapXmlInfo(const std::string &tagMapPath)
{
  tmpTagInfos_.clear();
  std::vector<mapLocalization::TagInfo> tmpTagInfoList;
  mapLocalization::TagInfo tmpTagInfo;
  int tagMapId = 0;
  // load tag map
  LOG(INFO) << "load tag map: ";

  boost::property_tree::ptree tagMapXmlPt;
  boost::property_tree::xml_parser::read_xml(tagMapPath + "/map.xml", tagMapXmlPt,
                                             boost::property_tree::xml_parser::trim_whitespace);
  if (tagMapXmlPt.empty())
  {
    LOG(WARNING) << "map.xml is empty!";

    return false;
  }
  std::string xml, submap_id_str, neighbour_id;
  boost::property_tree::ptree nodeList = tagMapXmlPt.get_child("map.node_list");

  for (auto &node : nodeList)
  {
    tmpTagInfoList.clear();
    submap_id_str = node.second.get("id", "");
    tagMapId = submapIdAndTagId_[std::atoi(submap_id_str.c_str())];
    boost::property_tree::ptree neighbourList = node.second.get_child("neighbour_list");
    for (auto &neighbour : neighbourList)
    {
      tmpTagInfo.tagMapId = submapIdAndTagId_[std::atoi(neighbour.second.get("id", "").c_str())];
      xml = neighbour.second.get("transform", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      tmpTagInfo.tagPose.position.x = value[0];
      tmpTagInfo.tagPose.position.y = value[1];
      tmpTagInfo.tagPose.position.z = value[2];

      tmpTagInfo.tagPose.orientation.w = value[6];
      tmpTagInfo.tagPose.orientation.x = value[3];
      tmpTagInfo.tagPose.orientation.y = value[4];
      tmpTagInfo.tagPose.orientation.z = value[5];
      tmpTagInfoList.push_back(tmpTagInfo);
    }
    tmpTagInfos_.insert(std::make_pair(tagMapId, tmpTagInfoList));
  }
  return true;
}

bool MapLocalization::getTagGlobalPosesXmlInfo(const std::string &tagMapPath)
{
  tagIdAndSubmapId_.clear();
  submapIdAndTagId_.clear();
  tagIdAndGlobalPose_.clear();
  LOG(INFO) << "get TagInfos srv...";
  std::string tag_map_path_ = tagMapPath;
  LOG(INFO) << "tag_map_path_: " << tag_map_path_;
  std::map<int, cartographer::transform::Rigid3d> tag_map_poses_;
  // load tag map
  LOG(INFO) << "load tag map: ";
  {
    boost::property_tree::ptree pt;
    boost::property_tree::xml_parser::read_xml(tag_map_path_ + "/tag_global_poses.xml", pt,
                                               boost::property_tree::xml_parser::trim_whitespace);
    if (pt.empty())
    {
      LOG(WARNING) << "tag_global_poses.xml is empty!";
      return false;
    }

    std::string xml, tag_id_str, submap_id_str;
    for (auto &mark : pt)
    {
      tag_id_str = mark.second.get("tag_id", "");
      submap_id_str = mark.second.get("id", "");

      xml = mark.second.get("global_pose", " ");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      cartographer::transform::Rigid3d tag2map({value[0], value[1], value[2]},
                                               {value[6], value[3], value[4], value[5]});
      int tag_id = std::stoi(tag_id_str);
      int submap_id = std::stoi(submap_id_str);

      // map_manager_msgs::TagMapInfo tag_info;
      geometry_msgs::Pose temp_pose;

      temp_pose.position.x = value[0];
      temp_pose.position.y = value[1];
      temp_pose.position.z = value[2];

      temp_pose.orientation.x = value[3];
      temp_pose.orientation.y = value[4];
      temp_pose.orientation.z = value[5];
      temp_pose.orientation.w = value[6];

      tagIdAndSubmapId_.insert(std::make_pair(tag_id, submap_id));
      submapIdAndTagId_.insert(std::make_pair(submap_id, tag_id));
      tagIdAndGlobalPose_.insert(std::make_pair(tag_id, temp_pose));
    }
    return true;
  }
}

bool MapLocalization::detectVisualmarks(const mapLocalization::TagInfo &tagInfo,
                                        const geometry_msgs::Pose &globalPose)
{
  LOG(INFO) << "detectVisualmarks";
  int targetId = -1;
  static int lastId = -1;
  int recvId = -1;
  std::string errorMsg;
  map_manager_msgs::VisualmarksDetectInfo detectInfo;
  double minDistance = DBL_MAX;
  double tmpDistance = 0.0;

  geometry_msgs::Pose recvTagPose;
  geometry_msgs::Pose recvGlobalPose;
  geometry_msgs::Pose targetTagPose;

  recvId = tagInfo.tagMapId;
  recvTagPose = tagInfo.tagPose;
  recvGlobalPose = globalPose;

  if (lastId < 0)
  {
    lastId = recvId;

    detectInfo.tag_id = recvId;
    detectInfo.error_code = 0;
    detectInfo.error_msg = "";
    visualmarks_detect_pub_.publish(detectInfo);
    return true;
  }

  {
    // 加锁，防止成员变量在读数据时被修改
    std::lock_guard<std::mutex> lock(mx_);
    for (auto it = tmpTagInfos_[lastId].begin(); it != tmpTagInfos_[lastId].end(); ++it)
    {
      tmpDistance =
          (tagIdAndGlobalPose_[(*it).tagMapId].position.x - recvGlobalPose.position.x) *
              (tagIdAndGlobalPose_[(*it).tagMapId].position.x - recvGlobalPose.position.x) +
          (tagIdAndGlobalPose_[(*it).tagMapId].position.y - recvGlobalPose.position.y) *
              (tagIdAndGlobalPose_[(*it).tagMapId].position.y - recvGlobalPose.position.y);
      if (tmpDistance < minDistance)
      {
        targetId = (*it).tagMapId;
        targetTagPose = (*it).tagPose;
        minDistance = tmpDistance;
      }
    }
  }

  if (recvId != lastId && recvId != targetId)
  {
    detectInfo.tag_id = recvId;
    detectInfo.error_code = -1;
    detectInfo.error_msg = "Target id error!";
    visualmarks_detect_pub_.publish(detectInfo);
    lastId = recvId;
    return false;
  }

  if (recvId == targetId)
  {
    Eigen::Quaterniond e1;
    Eigen::Quaterniond e2;
    tf::quaternionMsgToEigen(recvTagPose.orientation, e1);
    tf::quaternionMsgToEigen(targetTagPose.orientation, e2);
    Eigen::Quaterniond detQ = e1.conjugate() * e2;
    double roll, pitch, yaw;
    tf::Matrix3x3((tf::Quaternion(detQ.x(), detQ.y(), detQ.z(), detQ.w())))
        .getRPY(roll, pitch, yaw);
    double res = std::abs(yaw * 180.0 / M_PI);
    if (std::fabs(res) > 20)
    {
      detectInfo.tag_id = recvId;
      detectInfo.error_code = -2;
      detectInfo.error_msg = "Target pose error!";
      visualmarks_detect_pub_.publish(detectInfo);
      lastId = recvId;
      return false;
    }
  }

  detectInfo.tag_id = recvId;
  detectInfo.error_code = 0;
  detectInfo.error_msg = "";
  visualmarks_detect_pub_.publish(detectInfo);
  return true;
}
} // namespace map_manager