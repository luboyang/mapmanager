/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPLOCALIZATION_H
#define MAPLOCALIZATION_H

#include <cartographer_ros_msgs/GetPointsInImgFromGlobal.h>
#include <cartographer_ros_msgs/LandmarkList.h>
#include <cartographer_ros_msgs/LandmarkListServer.h>
#include <cartographer_ros_msgs/MapModify.h>
#include <cartographer_ros_msgs/ReferenceLocationFromMap.h>
#include <cartographer_ros_msgs/SubmapImages.h>
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <cartographer_ros_msgs/UpdateLandmarkServer.h>
#include <map_manager_msgs/DetectVisualmarksService.h>
#include <map_manager_msgs/Get2dPoints.h>
#include <map_manager_msgs/GetImage.h>
#include <map_manager_msgs/IdRemapSrv.h>
#include <map_manager_msgs/Set2dPoints.h>
#include <visualization_msgs/MarkerArray.h>
#include <vtr_msgs/SetGlobalPose.h>
#include <vtr_msgs/SetInitialPose.h>

#include "map_interface.h"
#include "impl/to_ros_msg.h"
// #include <cartographer_ros_msgs/GetImages.h>
namespace map_manager
{

namespace mapLocalization
{
struct TagInfo
{
  int tagMapId;
  geometry_msgs::Pose tagPose;
};
}; // namespace mapLocalization
class MapLocalization : public MapInterface
{
  public:
  MapLocalization();
  ~MapLocalization();
  virtual void clear() override;

  private:
//   void loadOldSubmap(const std::string &submap_filepath,
//                      cartographer_ros_msgs::SubmapImageEntry &submap_img);
  void getNearestPose(const Eigen::Vector2d &worldLoc, Eigen::Isometry3d &nearestPose,
                      int &nearestIdx);
  std::vector<cartographer_ros_msgs::LandmarkNewEntry> loadAndUpdateLandmark();
  void handleLandmarkLocalizing(const cartographer_ros_msgs::LandmarkListConstPtr &p_landmarkList);
  bool modifyBoolImageFromPoints(map_manager_msgs::Set2dPoints::Request &request,
                                 const std::string &xml_name, const std::string &image_name);
  void publishLandmarks(const ros::WallTimerEvent &unused_timer_event);
  bool loadSubmapsServer(::cartographer_ros_msgs::SubmapImagesServer::Request &request,
                         ::cartographer_ros_msgs::SubmapImagesServer::Response &response);
  bool loadLandmarksServer(::cartographer_ros_msgs::LandmarkListServer::Request &request,
                           ::cartographer_ros_msgs::LandmarkListServer::Response &response);
  bool
  referenceLocationServer(::cartographer_ros_msgs::ReferenceLocationFromMap::Request &request,
                          ::cartographer_ros_msgs::ReferenceLocationFromMap::Response &response);
  bool updateLandmarks(::cartographer_ros_msgs::UpdateLandmarkServer::Request &request,
                       ::cartographer_ros_msgs::UpdateLandmarkServer::Response &response);
  bool modifyMapServer(cartographer_ros_msgs::MapModify::Request &request,
                       cartographer_ros_msgs::MapModify::Response &response);

  bool getpointsInMapServer(cartographer_ros_msgs::GetPointsInImgFromGlobal::Request &request,
                            cartographer_ros_msgs::GetPointsInImgFromGlobal::Response &response);

  bool adjustMapPixelServer(cartographer_ros_msgs::MapModify::Request &request,
                            cartographer_ros_msgs::MapModify::Response &response);

  bool modifyLocationRegionServer(cartographer_ros_msgs::MapModify::Request &request,
                                  cartographer_ros_msgs::MapModify::Response &response);
  bool modifyFreeSpaceRegionServer(map_manager_msgs::Set2dPoints::Request &request,
                                   map_manager_msgs::Set2dPoints::Response &response);
  bool set2DPointsServer(map_manager_msgs::Set2dPoints::Request &request,
                         map_manager_msgs::Set2dPoints::Response &response);
  bool get2DPointsServer(map_manager_msgs::Get2dPoints::Request &request,
                         map_manager_msgs::Get2dPoints::Response &response);
  bool setInitPose(::vtr_msgs::SetInitialPose::Request &request,
                   ::vtr_msgs::SetInitialPose::Response &response);
  bool loadLineImageServer(map_manager_msgs::GetImage::Request &request,
                           map_manager_msgs::GetImage::Response &response);
  bool getAutoIgnoredArea(map_manager_msgs::GetImage::Request &request,
                          map_manager_msgs::GetImage::Response &response);
  bool loadTagMap(::cartographer_ros_msgs::LandmarkListServer::Request &request,
                  ::cartographer_ros_msgs::LandmarkListServer::Response &response);
  bool modifyLandmarksId(map_manager_msgs::IdRemapSrv::Request &request,
                         map_manager_msgs::IdRemapSrv::Response &response);
  bool detectLandmarkServer(map_manager_msgs::DetectVisualmarksService::Request &request,
                            map_manager_msgs::DetectVisualmarksService::Response &response);
  void updateIgnoreArea(const std::string &path);

  bool getTagMapPoseInLaserMap(const std::string &tagMapPath, const std::string &laserMapPath,
                               std::unordered_map<int, geometry_msgs::Pose> &tagMapIdWithPose);

  bool getTagMapXmlInfo(const std::string &tagMapPath);

  bool getTagGlobalPosesXmlInfo(const std::string &tagMapPath);

  bool detectVisualmarks(const mapLocalization::TagInfo &tagInfo,
                         const geometry_msgs::Pose &globalPose);
  // 	bool getErodeImages(
  // 		cartographer_ros_msgs::GetImages::Request& request,
  // 		cartographer_ros_msgs::GetImages::Response& response);

  //   ros::NodeHandle nh_;

  ros::Publisher landmarklist_localizing_pub_;
  ros::Publisher localization_request_pub_;
  ros::Publisher landmark_detect_pub_;
  ros::Publisher landmark_detect_pub2_;
  ros::Publisher global_localization_pub_;
  ros::Publisher tag_global_localization_pub_;
  ros::Publisher visualmarks_detect_pub_;
  ros::ServiceClient initial_pose_client_;
  ros::ServiceClient texture_initial_pose_client_;
  ros::ServiceClient reload_arm_free_space_image_client_;
  ros::ServiceClient tag_initial_pose_client_;

  ros::Subscriber landmark_detect_sub_;

  std::vector<::ros::WallTimer> wall_timers_;
  std::vector<::ros::ServiceServer> servers_;

  ros::Time tag_Initialization_lasttime_;

  visualization_msgs::MarkerArray landmark_full_array_;

  absl::Mutex mutex_;
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> posesInGlobal_;
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>>
      posesInGlobal_in_tag_map_;
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> localInGlobal_poses_;
  std::vector<double> maxes_;
  std::vector<std::vector<cv::Mat>> erode_images_;
  std::map<int, Eigen::Isometry3d> landmark_full_map_;

  std::string localization_path_;
  std::string load_map_path_;
  double resolution_;
  double tagmark_localize_interval_;
  int size_of_map_[2];
  int tagmark_localize_buffer_size_;
  bool pbstream_merged_flag_;
  ros::Time last_pub_marker_time_;
  std::map<int, Eigen::Isometry3d> tag_poses_;
  std::map<int, int> tag_id_with_submap_id_;

  std::unordered_map<int, int> tagIdAndSubmapId_;
  std::unordered_map<int, int> submapIdAndTagId_;
  std::unordered_map<int, geometry_msgs::Pose> tagIdAndGlobalPose_;
  std::unordered_map<int, std::vector<mapLocalization::TagInfo>> tmpTagInfos_;
  bool detectVisualmarksFlag_ = false;
  std::mutex mx_;
  
  ToRosMsg to_ros_msg_node_;
};
} // namespace map_manager
#endif // MAPLOCALIZATION_H
