/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPLOCALIZATION3D_H
#define MAPLOCALIZATION3D_H

#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/mapping/3d/submap_3d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/trajectory_node.h>
#include <cartographer_ros_msgs/LandmarkListServer.h>
#include <cartographer_ros_msgs/ReferenceLocationFromMap.h>
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <cartographer_ros_msgs/UpdateLandmarkServer.h>
#include <map_manager_msgs/Get2dPoints.h>
#include <map_manager_msgs/Set2dPoints.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <visualization_msgs/MarkerArray.h>
#include <vtr_msgs/SetGlobalPose.h>
#include <vtr_msgs/SetInitialPose.h>

#include "map_interface.h"
#include "utility/kd_tree.h"

namespace map_manager {
class MapLocalization3D : public MapInterface {
 public:
  MapLocalization3D();
  ~MapLocalization3D();
  void mergeFromPbstream(const std::string& map_path);
  sensor_msgs::PointCloud2 getPointCloud2(const std::size_t& submap_id);

  void getShowImage(const std::string& map_path, cv::Mat& img, double* max_box,
                    const bool& is_high_resolution);
  void getSubmapAndPose(const std::string& path, const int& index,
                        cartographer::transform::Rigid3d& global_pose);
  bool getReferenceLocationPoses(const geometry_msgs::Pose& worldLoc,
                                 geometry_msgs::Pose& targetPoseInWorld_pose,
                                 geometry_msgs::Pose& target2reference_pose,
                                 int& nearestIdx);
  bool loadLandmarksServer(
      ::cartographer_ros_msgs::LandmarkListServer::Request& request,
      ::cartographer_ros_msgs::LandmarkListServer::Response& response);
  void loadLandmark();
  void setResolution(const double& resolution) { resolution_ = resolution; };
  int getSubmapSize() { return submap_size_; };
  virtual void clear() override;

 private:
  void loadDataFromPbstream(const std::string& path,
                            std::vector<NodeData>& nodes_data_);
  void mergeAndSaveSubmaps(const std::vector<NodeData>& nodes_data,
                           const std::string& path);
  void mergeShowMap(const std::vector<NodeData>& nodes_data,
                    const std::string& path);
  void writeSubmap3DToPbstream(
      const std::shared_ptr<cartographer::mapping::Submap3D>& submap,
      const std::string& save_path, const int& index);
  void saveMergeSubmapConstraits(const std::string& filename);
  sensor_msgs::PointCloud2 toPointCloud2(
      const cartographer::sensor::PointCloud& point_cloud);
  void mergeNodesToPointCloud2(
      std::vector<std::shared_ptr<cartographer::mapping::Submap3D>>&
          submap_ptrs);

  void setMaxRangeDataNum(int max_range_data_num) {
    max_range_data_num_ = max_range_data_num;
  };
  void setOverlapNum(int overlap_num) { overlap_num_ = overlap_num; };

  void readSubmap3DFromPbstream(
      const std::string& path,
      std::shared_ptr<cartographer::mapping::Submap3D>& submap_ptr);
  cartographer::io::SubmapSlice submapToSlice(
      const std::shared_ptr<cartographer::mapping::Submap3D> submap,
      const cartographer::transform::Rigid3d& global_pose,
      const bool& is_high_resolution);
  void getNearestPose(const Eigen::Vector3d& worldLoc,
                      Eigen::Affine3d& nearestPose, int& nearestIdx);
  bool referenceLocationServer(
      ::cartographer_ros_msgs::ReferenceLocationFromMap::Request& request,
      ::cartographer_ros_msgs::ReferenceLocationFromMap::Response& response);
  bool loadMapServer(
      cartographer_ros_msgs::SubmapImagesServer::Request& request,
      cartographer_ros_msgs::SubmapImagesServer::Response& response);
  bool loadSubmapsServer(
      cartographer_ros_msgs::SubmapImagesServer::Request& request,
      cartographer_ros_msgs::SubmapImagesServer::Response& response);
  bool updateLandmarks(
      ::cartographer_ros_msgs::UpdateLandmarkServer::Request& request,
      ::cartographer_ros_msgs::UpdateLandmarkServer::Response& response);
  bool set2DPointsServer(map_manager_msgs::Set2dPoints::Request& request,
                         map_manager_msgs::Set2dPoints::Response& response);
  bool get2DPointsServer(map_manager_msgs::Get2dPoints::Request& request,
                         map_manager_msgs::Get2dPoints::Response& response);
  bool setInitialPose(vtr_msgs::SetInitialPose::Request& request,
                      vtr_msgs::SetInitialPose::Response& response);
  bool setInitialPose2(vtr_msgs::SetInitialPose::Request& request,
                       vtr_msgs::SetInitialPose::Response& response);
  void pubSubmapPointCloud(const sensor_msgs::PointCloudPtr& msg);
  void setPosesAndSubmaps(const std::string& path);
  void slicesToImage(
      const std::map<::cartographer::mapping::SubmapId,
                     ::cartographer::io::SubmapSlice>& submap_slices,
      cv::Mat& img, double* max_box);
  cartographer::sensor::PointCloud submap3D2PointCloud(
      std::shared_ptr<cartographer::mapping::Submap3D> submap_ptr);
  void setKDTree(const std::string& path);
  void loadSubmaps();
  // 	ros::NodeHandle nh_;

  int max_range_data_num_;
  int overlap_num_;
  double high_resolution_;
  double low_resolution_;
  double high_resolution_max_range_;
  int origin_num_range_data_;
  int merge_ratio_, overlap_ratio_;

  std::map<int, std::set<int>> origin_id_with_new_ids_;
  int origin_submaps_size_;
  std::vector<cartographer::transform::Rigid3d> submap2global_poses_;
  std::vector<sensor_msgs::PointCloud2> point_clouds_;

  std::vector<::ros::ServiceServer> servers_;
  std::vector<Eigen::Affine3d, Eigen::aligned_allocator<Eigen::Affine3d>>
      posesInGlobal_;
  int submap_size_;
  float resolution_;
  bool show_high_resolution_map_;
  ros::Publisher submap_point_cloud2_pub_, global_localization_pub_,
      localization_request_pub_;
  ros::Subscriber localization_point_cloud2_sub_;
  std::shared_ptr<cartographer::mapping::Submap3D> map_ptr_;
  ros::ServiceClient initial_pose_client_;

  std::vector<std::shared_ptr<cartographer::mapping::Submap3D>> submaps_;
  std::vector<cartographer::transform::Rigid3d> global_poses_;
  bool debug_flag_;

  visualization_msgs::MarkerArray landmark_full_array_;
  std::string localization_path_;
  absl::Mutex mutex_;
  int size_of_map_[2];
  double maxes_[2];
  std::shared_ptr<kd_tree::KDTree> kd_tree_;
  std::string map_path_;
};
}  // namespace map_manager
#endif  // MAPLOCALIZATION3D_H
