/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "3d/map_localization_3d.h"

#include <cartographer/io/submap_painter.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer_ros/node_options.h>
#include <cartographer_ros_msgs/SubmapQuery.h>
#include <eigen_conversions/eigen_msg.h>
#include <ros/package.h>
#include <std_msgs/Empty.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <vtr_msgs/GlobalLocalizationPose.h>

#include "map_manager_ulitity.h"
using namespace std;
using namespace cv;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager
{
MapLocalization3D::MapLocalization3D()
{
  merge_ratio_ = 2;
  overlap_ratio_ = 1;
  max_range_data_num_ = 1000;
  overlap_num_ = 100;
  origin_submaps_size_ = 0;
  debug_flag_ = false;
  if (1)
  {
    cartographer_ros::NodeOptions node_options_temp;
    cartographer_ros::TrajectoryOptions trajectory_options_temp;

    std::string config_path = ::ros::package::getPath("cartographer_ros") + "/configuration_files";
    string configuration_basename = "jz_3d_localization_outdoor_double.lua";

    ::ros::param::get("/cartographer_node/localization_3d_lua_file", configuration_basename);

    LOG(INFO) << "carto config lua file: " << (config_path + "/" + configuration_basename);

    std::tie(node_options_temp, trajectory_options_temp) =
        cartographer_ros::LoadOptions(config_path, configuration_basename);
    ::cartographer::mapping::proto::LocalTrajectoryBuilderOptions3D trajectory_builder_3d_options =
        trajectory_options_temp.trajectory_builder_options.trajectory_builder_3d_options();
    high_resolution_ = trajectory_builder_3d_options.submaps_options().high_resolution();
    low_resolution_ = trajectory_builder_3d_options.submaps_options().low_resolution();
    high_resolution_max_range_ =
        trajectory_builder_3d_options.submaps_options().high_resolution_max_range();
    //     LOG(INFO) << "high_resolution: " << high_resolution_ <<endl
    //       << "low_resolution: " << low_resolution_ <<endl
    //       << "high_resolution_max_range: " << high_resolution_max_range_ ;
  }
  LOG(INFO) << "high_resolution: " << high_resolution_ << endl
            << "low_resolution: " << low_resolution_ << endl
            << "high_resolution_max_range: " << high_resolution_max_range_;
  resolution_ = 0.05;
  ros::param::get("/map_manager_3d/resolution", resolution_);
  ros::param::get("/map_manager_localization_3d/show_high_resolution_map",
                  show_high_resolution_map_);
  ros::param::get("~/map_manager_localization_3d/debug_flag", debug_flag_);

  LOG(INFO) << "3d localization resolution: " << resolution_;
  LOG(INFO) << "show_high_resolution_map: " << show_high_resolution_map_;
  LOG(INFO) << "debug_flag: " << debug_flag_;
  servers_.push_back(nh_.advertiseService("/map_manager_localization_3d/reference_location_server",
                                          &MapLocalization3D::referenceLocationServer, this));
  servers_.push_back(nh_.advertiseService(
      "/map_manager_localization_3d/load_submaps", &MapLocalization3D::loadMapServer,
      this)); // consistent with 2d ,there should be written to load map
  servers_.push_back(nh_.advertiseService("/map_manager_localization_3d/load_little_submaps",
                                          &MapLocalization3D::loadSubmapsServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/load_landmarks",
                                          &MapLocalization3D::loadLandmarksServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/update_landmarks",
                                          &MapLocalization3D::updateLandmarks, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/get_location_region",
                                          &MapLocalization3D::get2DPointsServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization/set_location_region",
                                          &MapLocalization3D::set2DPointsServer, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization_3d/initial_pose",
                                          &MapLocalization3D::setInitialPose, this));
  servers_.push_back(nh_.advertiseService("/map_manager_localization_3d/initial_global_pose",
                                          &MapLocalization3D::setInitialPose2, this));
  if (debug_flag_)
  {
    submap_point_cloud2_pub_ = nh_.advertise<sensor_msgs::PointCloud2>(
        "/map_manager_localization_3d/submap_point_cloud2", 1);
    localization_point_cloud2_sub_ =
        nh_.subscribe("/map_manager_localization_3d/localization_point_cloud", 1,
                      &MapLocalization3D::pubSubmapPointCloud, this);
  }

  global_localization_pub_ =
      nh_.advertise<vtr_msgs::GlobalLocalizationPose>("/vtr/global_localization", 1);
  localization_request_pub_ = nh_.advertise<std_msgs::Empty>("/vtr/submap3D_localization", 1);
  initial_pose_client_ =
      nh_.serviceClient<vtr_msgs::SetGlobalPose>("/vtr/global_localization_server");

  //
  map_path_ = "";
  ::ros::param::get("/vtr/path", map_path_);

  if (judgeFileExist(map_path_ + "/map_3d_to_map_2d_pose.xml"))
  {
    boost::property_tree::ptree pt_poses;
    boost::property_tree::read_xml(map_path_ + "/map_3d_to_map_2d_pose.xml", pt_poses);
    if (pt_poses.empty())
    {
      LOG(WARNING) << "origin_map.xml is empty! ";
      return;
    }
    std::string data;
    data = pt_poses.get<std::string>("map_3d_to_map_2d_pose");
    double pose[7];
    sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
           &pose[4], &pose[5], &pose[6]);
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped static_transform_stamped;
    static_transform_stamped.header.stamp = ros::Time::now();
    static_transform_stamped.header.frame_id = "map_father";
    static_transform_stamped.child_frame_id = "map";

    static_transform_stamped.transform.translation.x = pose[0];
    static_transform_stamped.transform.translation.y = pose[1];
    static_transform_stamped.transform.translation.z = pose[2];
    // 初始化四元数
    static_transform_stamped.transform.rotation.x = pose[3];
    static_transform_stamped.transform.rotation.y = pose[4];
    static_transform_stamped.transform.rotation.z = pose[5];
    static_transform_stamped.transform.rotation.w = pose[6];
    static_broadcaster.sendTransform(static_transform_stamped);
  }
  LOG(INFO) << "map_localization_3d initialised done.";
  map_path_ = map_path_ + "/";
}

MapLocalization3D::~MapLocalization3D() {}

void MapLocalization3D::mergeFromPbstream(const std::string &path)
{
  // 	string path;
  // 	getPath(map_name,path);
  // 	std::vector<NodeData> nodes_data;
  // 	loadDataFromPbstream(path + "map.pbstream",nodes_data);
  // 	std::fstream fi;
  // 	fi.open(path+"show_submap.pbstream",ios::in);
  // 	if(!fi)
  // 	{
  // 		LOG(INFO) << "Submaps have not saved,now we will save them!";
  // 		mergeShowMap(nodes_data,path);
  // 		LOG(INFO) << "save done!";
  // 	}
  // 	else
  // 		LOG(INFO) << "Submaps have been aready!";
  std::vector<std::shared_ptr<Submap3D>> submap3d_ptrs;
  for (int i = 0; i < submap_size_; i++)
  {
    std::shared_ptr<Submap3D> submap3d_ptr;
    string sub_path = path + "frames/" + to_string(i);
    readSubmap3DFromPbstream(sub_path + "/submap.pbstream", submap3d_ptr);
    submap3d_ptrs.push_back(submap3d_ptr);
  }
  mergeNodesToPointCloud2(submap3d_ptrs);
}

sensor_msgs::PointCloud2 MapLocalization3D::getPointCloud2(const std::size_t &submap_id)
{
  if (submap_id > point_clouds_.size() - 1)
  {
    sensor_msgs::PointCloud2 point_null;
    LOG(WARNING) << "submap id is bigger than point_clouds size! It means that "
                    "there is no this submap point cloud!";
    return point_null;
  }
  point_clouds_[submap_id].header.frame_id = "map";
  point_clouds_[submap_id].header.stamp = ros::Time::now();
  return point_clouds_[submap_id];
}

void MapLocalization3D::loadDataFromPbstream(const string &path, std::vector<NodeData> &nodes_data)
{
  io::ProtoStreamReader stream(path);
  io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  MapById<NodeId, transform::Rigid3d> node_poses;
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        transform::ToRigid3(node_proto.pose()));
    }
  }
  cartographer::mapping::proto::SerializedData proto;
  bool tmp_flag = true;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      cartographer::mapping::proto::Node *proto_node = proto.mutable_node();
      cartographer::mapping::proto::TrajectoryNodeData proto_node_data =
          *proto_node->mutable_node_data();

      cartographer::mapping::TrajectoryNode::Data node_data =
          cartographer::mapping::FromProto(proto_node_data);
      // 				proto_node->node_id().trajectory_id();
      // 				proto_node->node_id().node_index();
      NodeId id(proto_node->node_id().trajectory_id(), proto_node->node_id().node_index());
      transform::Rigid3d node_pose = node_poses.at(id);
      sensor::RangeData range_data_origin_high{
          {0., 0., 0.}, node_data.high_resolution_point_cloud, {}};

      sensor::RangeData range_data_high =
          sensor::TransformRangeData(range_data_origin_high, node_pose.cast<float>());
      nodes_data.emplace_back(
          NodeData{range_data_high, node_data.rotational_scan_matcher_histogram,
                   node_data.gravity_alignment /*Eigen::Quaterniond::Identity()*/, node_pose});
    }
    case cartographer::mapping::proto::SerializedData::kSubmap:
    {
      //若不使用unfinished submap，那么 node——data 后面的数据应该舍弃，只保留
      // origin_submaps_size * origin_num_range_data /2 +
      // origin_num_range_data /2 datas

      if (proto.submap().has_submap_3d() /*&&proto.submap().submap_3d().finished()*/)
        ++origin_submaps_size_;
      if (tmp_flag)
      {
        // just get origin_num_range_data
        cartographer::mapping::proto::Submap proto_submap = proto.submap();
        if (proto_submap.has_submap_3d())
        {
          if (proto_submap.submap_3d().finished())
          {
            origin_num_range_data_ = proto_submap.submap_3d().num_range_data();
            max_range_data_num_ =
                origin_num_range_data_ * merge_ratio_ / 2 + origin_num_range_data_ / 2;
            overlap_num_ = overlap_ratio_ * origin_num_range_data_;
            LOG(INFO) << "origin_num_range_data_: " << origin_num_range_data_;
            tmp_flag = false;
          }
        }
      }
      break;
    }
    default:
      break;
    }
  }
  LOG(INFO) << "origin_submaps_size: " << origin_submaps_size_
            << " nodes_data size:" << nodes_data.size();
}

// not use
void MapLocalization3D::mergeAndSaveSubmaps(const std::vector<NodeData> &nodes_data,
                                            const string &path)
{
  // 	const Eigen::VectorXf initial_rotational_scan_matcher_histogram =
  //       Eigen::VectorXf::Zero(nodes_data[0].rotational_scan_matcher_histogram_in_gravity.size());
  vector<std::shared_ptr<Submap3D>> merge_submaps;
  // 	std::unique_ptr<Submap3D> map0;
  // 	map0 = std::unique_ptr<Submap3D>(new Submap3D(
  //       high_resolution_, low_resolution_,
  // 			nodes_data[0].global_pose,
  // 			initial_rotational_scan_matcher_histogram));

  cartographer::mapping::proto::RangeDataInserterOptions3D range_data_inserter_options;
  {
    range_data_inserter_options.set_hit_probability(0.8);
    range_data_inserter_options.set_miss_probability(0.3);
    range_data_inserter_options.set_num_free_space_voxels(2);
  }
  cartographer::mapping::RangeDataInserter3D range_data_inserter_(range_data_inserter_options);
  for (std::size_t i = 0; i < nodes_data.size(); i++)
  {
    if (merge_submaps.size() == 0 ||
        merge_submaps.back()->num_range_data() == max_range_data_num_ - overlap_num_ + 1)
    {
      std::size_t local_id = i + overlap_num_ / overlap_ratio_ * merge_ratio_ / 4;
      if (local_id >= nodes_data.size()) local_id = nodes_data.size() - 1;
      const Eigen::VectorXf initial_rotational_scan_matcher_histogram = Eigen::VectorXf::Zero(
          nodes_data[local_id].rotational_scan_matcher_histogram_in_gravity.size());
      merge_submaps.emplace_back(new Submap3D(high_resolution_, low_resolution_,
                                              nodes_data[local_id].global_pose,
                                              initial_rotational_scan_matcher_histogram));
      submap2global_poses_.push_back(nodes_data[local_id].global_pose);
      merge_submaps.back()->InsertData(
          nodes_data[local_id].range_data, range_data_inserter_, high_resolution_max_range_,
          nodes_data[local_id].local_from_gravity_aligned,
          nodes_data[local_id].rotational_scan_matcher_histogram_in_gravity);
    }
    NodeData range_data;
    range_data = nodes_data[i];
    Eigen::Quaterniond q = range_data.local_from_gravity_aligned; // Eigen::Quaterniond::Identity();
    for (auto &it : merge_submaps)
    {
      if (it->num_range_data() == max_range_data_num_ + 1) continue;
      it->InsertData(range_data.range_data, range_data_inserter_, high_resolution_max_range_, q,
                     range_data.rotational_scan_matcher_histogram_in_gravity);
    }
  }
  int i = 0;
  CHECK(submap2global_poses_.size() == merge_submaps.size());
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  int tmp_ratio = overlap_ratio_;
  for (auto &it : merge_submaps)
  {
    string full_path = path + "frames/" + to_string(i) + "/";
    bash("mkdir -p " + full_path);
    {
      boost::property_tree::ptree p_map;
      transform::Rigid3d pose;
      transform::Rigid3d::Vector translation;
      transform::Rigid3d::Quaternion quaternion;
      std::string data;
      pose = submap2global_poses_[i];
      translation = pose.translation();
      quaternion = pose.rotation();
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      for (int i = 0; i < 4; i++)
        data = data + std::to_string(quaternion.coeffs()[i]) + " ";
      p_map.put("pose", data);
      boost::property_tree::write_xml(full_path + "data.xml", p_map, std::locale(), setting);
    }
    {
      for (int j = 0; j < merge_ratio_; j++)
      {
        if (merge_ratio_ * i + j - i * tmp_ratio < origin_submaps_size_)
        {
          origin_id_with_new_ids_[merge_ratio_ * i + j - i * tmp_ratio].insert(i);
          LOG(INFO) << merge_ratio_ * i + j - i * tmp_ratio;
        }
      }
    }
    writeSubmap3DToPbstream(it, full_path + "submap.pbstream", i);
    i++;
  }
  saveMergeSubmapConstraits(path);
  // 	ofstream fi(path+"",ios::out);
  // 	fi << i;
  // 	fi.close();
}

// not use
void MapLocalization3D::saveMergeSubmapConstraits(const string &path)
{
  map<int, std::set<int>> connections;
  map<int, std::set<int>> node_submap_ids;
  try
  {
    boost::property_tree::ptree p_map1;
    boost::property_tree::read_xml(path + "origin_map.xml", p_map1);
    if (p_map1.empty())
    {
      LOG(WARNING) << "origin_map.xml is empty! ";
      return;
    }
    boost::property_tree::ptree p_node_list = p_map1.get_child("map.node_list");
    for (auto p_node_it = p_node_list.begin(); p_node_it != p_node_list.end(); ++p_node_it)
    {
      int node_id = p_node_it->second.get<int>("id");
      boost::property_tree::ptree p_neighbour_list = p_node_it->second.get_child("neighbour_list");

      for (auto p_neighbour_it = p_neighbour_list.begin(); p_neighbour_it != p_neighbour_list.end();
           ++p_neighbour_it)
      {
        int neighbour_id = p_neighbour_it->second.get<int>("id");
        std::set<int> submap_ids = origin_id_with_new_ids_[neighbour_id];
        for (auto id : submap_ids)
          node_submap_ids[node_id].insert(id);
      }
    }
    for (auto submap_it = node_submap_ids.begin(); submap_it != node_submap_ids.end(); ++submap_it)
    {
      std::set<int> submap_ids = submap_it->second;
      for (auto submap_id : submap_ids)
      {
        for (auto neighbour_id : node_submap_ids[submap_it->first])
        {
          if (submap_id != neighbour_id) connections[submap_id].insert(neighbour_id);
        }
      }
    }
    for (auto connection : connections)
    {
      for (auto id : connection.second)
      {
        LOG(INFO) << "submap_id: " << connection.first << "   neighbour_id: " << id << endl;
      }
    }

    {
      transform::Rigid3d::Vector translation;
      transform::Rigid3d::Quaternion quaternion;
      std::string data;
      boost::property_tree::ptree p_top;
      boost::property_tree::ptree p_map;
      boost::property_tree::ptree p_node_list;
      for (uint i = 0; i < submap2global_poses_.size(); i++)
      {
        boost::property_tree::ptree p_node;

        p_node.put("id", i);

        boost::property_tree::ptree p_neighbour_list;
        for (std::set<int>::iterator p = connections[i].begin(); p != connections[i].end(); ++p)
        {
          boost::property_tree::ptree p_neighbour;
          uint connected_id = *p;

          CHECK(connected_id < submap2global_poses_.size());

          p_neighbour.put("id", connected_id);
          transform::Rigid3d neighbour2current =
              submap2global_poses_[i].inverse() * submap2global_poses_[connected_id];
          translation = neighbour2current.translation();
          quaternion = neighbour2current.rotation();
          for (int i = 0; i < 3; i++)
            data = data + std::to_string(translation[i]) + " ";
          data = data + std::to_string(quaternion.w()) + " ";
          data = data + std::to_string(quaternion.x()) + " ";
          data = data + std::to_string(quaternion.y()) + " ";
          data = data + std::to_string(quaternion.z());
          p_neighbour.put("transform", data);
          data.clear();
          p_neighbour.put("reachable", true);

          p_neighbour_list.add_child("neighbour", p_neighbour);
        }

        p_node.add_child("neighbour_list", p_neighbour_list);

        p_node_list.add_child("node", p_node);
      }
      p_map.add_child("node_list", p_node_list);
      boost::property_tree::ptree p_property;
      p_property.put("node_count", submap2global_poses_.size());

      p_map.add_child("property", p_property);

      p_top.add_child("map", p_map);
      LOG(INFO) << "save  " << path << "map.xml";
      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      boost::property_tree::write_xml(path + "map.xml", p_top, std::locale(), setting);
    }
  }
  catch (boost::property_tree::ptree_error &err)
  {
    LOG(WARNING) << "Error: " << err.what();
  }
}

void MapLocalization3D::writeSubmap3DToPbstream(const std::shared_ptr<Submap3D> &submap,
                                                const string &save_path, const int &index)
{
  io::ProtoStreamWriter writer(save_path);
  cartographer::mapping::proto::SerializedData proto;
  auto *const submap_proto = proto.mutable_submap();
  *submap_proto = submap->ToProto(true);
  submap_proto->mutable_submap_id()->set_trajectory_id(0);
  submap_proto->mutable_submap_id()->set_submap_index(index);
  writer.WriteProto(proto);
  writer.Close();
}

void MapLocalization3D::mergeShowMap(const vector<NodeData> &nodes_data, const std::string &path)
{
  const Eigen::VectorXf initial_rotational_scan_matcher_histogram =
      Eigen::VectorXf::Zero(nodes_data[0].rotational_scan_matcher_histogram_in_gravity.size());
  std::shared_ptr<cartographer::mapping::Submap3D> merge_submap(
      new Submap3D(high_resolution_, low_resolution_, nodes_data[0].global_pose,
                   initial_rotational_scan_matcher_histogram));
  cartographer::mapping::proto::RangeDataInserterOptions3D range_data_inserter_options;
  {
    range_data_inserter_options.set_hit_probability(0.55);
    range_data_inserter_options.set_miss_probability(0.49);
    range_data_inserter_options.set_num_free_space_voxels(2);
  }
  cartographer::mapping::RangeDataInserter3D range_data_inserter_(range_data_inserter_options);
  double high_resolution_max_range = 15;
  //
  for (unsigned int i = 0; i < nodes_data.size(); i++)
  {
    NodeData range_data_high;
    range_data_high = nodes_data[i];
    Eigen::Quaterniond q = Eigen::Quaterniond::Identity();
    merge_submap->InsertData(range_data_high.range_data, range_data_inserter_,
                             high_resolution_max_range, q,
                             range_data_high.rotational_scan_matcher_histogram_in_gravity);
  }
  map_ptr_ = std::move(merge_submap);
  // writeSubmap3DToPbstream(merge_submap, path + "show_submap.pbstream",0);
}

sensor::PointCloud MapLocalization3D::submap3D2PointCloud(std::shared_ptr<Submap3D> submap_ptr)
{
  const HybridGrid &high_grid = submap_ptr->high_resolution_hybrid_grid();
  sensor::PointCloud point_cloud;
  HybridGrid::Iterator it = high_grid.begin();

  while (it != high_grid.end())
  {
    Eigen::Array3i temp = it.GetCellIndex();
    sensor::RangefinderPoint point;
    point.position = high_grid.GetCenterOfCell(temp);
    point.intensity = 0;
    point_cloud.push_back(point);
    it.Next();
  }
  return point_cloud;
}

void MapLocalization3D::mergeNodesToPointCloud2(std::vector<std::shared_ptr<Submap3D>> &submap_ptrs)
{
  cartographer::sensor::proto::AdaptiveVoxelFilterOptions adaptive_voxel_filter_options;
  adaptive_voxel_filter_options.set_max_length(0.8);
  adaptive_voxel_filter_options.set_min_num_points(5000);
  adaptive_voxel_filter_options.set_max_range(1000);
  sensor::AdaptiveVoxelFilter adaptive_voxel_filter_high_resolution(adaptive_voxel_filter_options);
  std::vector<sensor::PointCloud> pointclouds;
  for (auto &it : submap_ptrs)
  {
    pointclouds.push_back(submap3D2PointCloud(it));
  }
  for (const auto &it : pointclouds)
  {
    sensor::PointCloud filtered_point_cloud = adaptive_voxel_filter_high_resolution.Filter(it);
    LOG(INFO) << "filter before: " << it.size()
              << "     filter after: " << filtered_point_cloud.size();

    point_clouds_.push_back(toPointCloud2(filtered_point_cloud));
  }
}

sensor_msgs::PointCloud2 MapLocalization3D::toPointCloud2(const sensor::PointCloud &point_cloud)
{
  sensor_msgs::PointCloud2 msg;
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = "map";
  msg.height = 1;
  msg.width = point_cloud.size();
  msg.fields.resize(3);
  msg.fields[0].name = "x";
  msg.fields[0].offset = 0;
  msg.fields[0].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[0].count = 1;
  msg.fields[1].name = "y";
  msg.fields[1].offset = 4;
  msg.fields[1].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[1].count = 1;
  msg.fields[2].name = "z";
  msg.fields[2].offset = 8;
  msg.fields[2].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[2].count = 1;
  msg.is_bigendian = false;
  msg.point_step = 16;
  msg.row_step = 16 * msg.width;
  msg.is_dense = true;
  msg.data.resize(16 * point_cloud.size());
  ::ros::serialization::OStream stream(msg.data.data(), msg.data.size());
  for (const cartographer::sensor::RangefinderPoint &point : point_cloud)
  {
    // 		if(point.position.z() > 3.0 || point.position.z() < 0.5 )
    // 			continue;
    stream.next(point.position.x());
    stream.next(point.position.y());
    stream.next(point.position.z());
    stream.next(1);
  }
  return msg;
}

void MapLocalization3D::getShowImage(const std::string &path, cv::Mat &img, double *max_box,
                                     const bool &is_high_resolution)
{
  // 	cv::Mat origin_map;
  if (submaps_.empty()) loadSubmaps();
  std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> submap_slices;

  for (int i = 0; i < submap_size_; i++)
  {
    submap_slices.insert(make_pair(
        SubmapId(0, i), submapToSlice(submaps_[i], global_poses_[i], is_high_resolution)));
  }
  slicesToImage(submap_slices, img, max_box);
  // 	return;
  // 	for (int y = origin_map.rows - 1; y >= 0; --y) {
  //     for (int x = 0; x < origin_map.cols; ++x) {
  // 			if(origin_map.at<uint8_t>(y,x) == 128)
  // 			{
  // 				origin_map.at<uint8_t>(y,x) = 255;
  // 			}
  // 		}
  // 	}
  // 	cv::equalizeHist(origin_map,img);
  // 	for (int y = img.rows - 1; y >= 0; --y) {
  //     for (int x = 0; x < img.cols; ++x) {
  // 			if(img.at<uint8_t>(y,x) == 255)
  // 			{
  // 				img.at<uint8_t>(y,x) = 128;
  // 			}
  // 		}
  // 	}
  // 	cv::equalizeHist(origin_map,img);
  // 	cv::Mat kernel = (cv::Mat_<float>(3, 3) << 0, -1, 0, 0, 2, 0, 0, -1, 0);
  // 	cv::filter2D(origin_map, img, CV_8UC1, kernel);
}

void MapLocalization3D::slicesToImage(const map<SubmapId, io::SubmapSlice> &submap_slices, Mat &img,
                                      double *max_box)
{
  auto painted_slices = cartographer::io::PaintSubmapSlices(submap_slices, resolution_);
  img = getMatMap(painted_slices);
  // 	cv::flip(img,img,0);
  // 	transpose(img,img);
  max_box[0] = painted_slices.origin.x();
  max_box[1] = painted_slices.origin.y();
}

void MapLocalization3D::getSubmapAndPose(const string &path, const int &index,
                                         cartographer::transform::Rigid3d &global_pose)
{
  // 	string path;
  // 	getPath(map_name,path);
  // 	std::shared_ptr<Submap3D> submap_ptr;
  {
    boost::property_tree::ptree pt, pt_poses;
    boost::property_tree::read_xml(path + "frames/" + to_string(index) + "/data.xml", pt_poses);
    if (pt_poses.empty())
    {
      LOG(WARNING) << "data.xml is empty! ";
      return;
    }
    // 		pt_poses = pt.get_child("submap_poses");
    std::string data;
    data = pt_poses.get<std::string>("submap_global_pose");
    double pose[7];
    sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
           &pose[4], &pose[5], &pose[6]);

    Eigen::Quaterniond q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
    global_pose = cartographer::transform::Rigid3d({pose[0], pose[1], pose[2]}, q);
  }

  // 	readSubmap3DFromPbstream(path + "frames/"+ to_string(index) +
  // "/submap.pbstream",submap_ptr);

  // 	submapToSlice(submap_ptr, img, max_box, global_pose,
  // is_high_resolution); 	std::map<::cartographer::mapping::SubmapId,
  // ::cartographer::io::SubmapSlice> submap_slices;
  // 	submap_slices.insert(make_pair(SubmapId(0,0),
  // 																 submapToSlice(map_ptr_,map_ptr_->local_pose(),
  // is_high_resolution))); 	auto painted_slices =
  // cartographer::io::PaintSubmapSlices(submap_slices, resolution_); 	img =
  // getMatMap(painted_slices); 	cv::flip(img,img,0); 	transpose(img,img);
  // 	max_box[0] = painted_slices.origin.x();
  // 	max_box[1] = painted_slices.origin.y();
}

void MapLocalization3D::setPosesAndSubmaps(const std::string &path)
{
  boost::property_tree::ptree pt1;
  boost::property_tree::read_xml(path + "map.xml", pt1);
  if (pt1.empty())
  {
    const std::string error_string = "Load " + path + "/map.xml failed!";
    ROS_FATAL("%s", error_string.c_str());
    throw std::runtime_error(error_string);
    return;
  }
  submap_size_ = pt1.get<int>("map.property.node_count");
  LOG(WARNING) << submap_size_;
  for (int i = 0; i < submap_size_; i++)
  {
    //     std::shared_ptr<Submap3D> submap_ptr;
    transform::Rigid3d global_pose;
    getSubmapAndPose(path, i, global_pose);
    //     submaps_.push_back(submap_ptr);
    global_poses_.push_back(global_pose);

    {
      boost::property_tree::ptree pt, pt_poses;
      boost::property_tree::read_xml(path + "frames/" + to_string(i) + "/data.xml", pt_poses);
      if (pt_poses.empty())
      {
        LOG(WARNING) << "data.xml is empty! ";
        return;
      }
      // 			pt_poses = pt.get_child("submap_poses");
      std::string data;
      data = pt_poses.get<std::string>("pose"); // mid_node_global_pose
      double pose[7];
      sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
             &pose[4], &pose[5], &pose[6]);

      Eigen::Quaterniond q = Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
      cartographer::transform::Rigid3d mid_node_global_pose =
          cartographer::transform::Rigid3d({pose[0], pose[1], pose[2]}, q);
      // 			if(!use_pcd_submaps_)
      posesInGlobal_.push_back(ToEigen(mid_node_global_pose));
      LOG(WARNING) << mid_node_global_pose;
    }
    LOG(WARNING) << submap_size_;
    // 		posesInGlobal_.push_back(ToEigen(submap_ptr->local_pose()));
  }

  {
    LOG(INFO) << "start loading node poses...";
    setKDTree(path);
    LOG(INFO) << "load node poses done!";
  }
}
void MapLocalization3D::setKDTree(const std::string &path)
{
  kd_tree::poseVec poses;
  string file_path = path + "ori_submap_poses.txt";
  LOG(INFO) << "file_path: " << file_path;
  if (judgeFileExist(file_path))
  {
    std::ifstream fi;
    fi.open(file_path, std::ios::in);
    if (fi.is_open())
    {
      int id = 0;
      double data[7];
      while (fi >> id >> data[0] >> data[1] >> data[2] >> data[3] >> data[4] >> data[5] >> data[6])
      {
        poses.push_back({{data[0], data[1], data[2]},
                         {data[3], data[4], data[5], data[6]},
                         Eigen::Vector2i(0, id)});
      }
    }
    fi.close();
  }
  else
  {
    io::ProtoStreamReader reader(path + "map.pbstream");
    io::ProtoStreamDeserializer deserializer(&reader);
    cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();

    int k = 0;
    std::ofstream outFile;
    outFile.open(file_path, std::ios::out);
    for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
         pose_graph_proto.trajectory())
    {
      for (const cartographer::mapping::proto::Trajectory::Submap &submap_proto :
           trajectory_proto.submap())
      {
        transform::Rigid3d temp = transform::ToRigid3(submap_proto.pose());

        outFile << k << " " << temp.translation().x() << " " << temp.translation().y() << " "
                << temp.translation().z() << " " << temp.rotation().w() << " "
                << temp.rotation().x() << " " << temp.rotation().y() << " " << temp.rotation().z()
                << std::endl;
        poses.push_back({temp.translation(), temp.rotation(), Eigen::Vector2i(0, k++)});
      }
    }
    outFile.close();
  }
  LOG(WARNING) << poses.size();
  kd_tree_.reset(new kd_tree::KDTree(poses));
}

void MapLocalization3D::readSubmap3DFromPbstream(const std::string &path,
                                                 std::shared_ptr<Submap3D> &submap_ptr)
{
  io::ProtoStreamReader stream(path);

  cartographer::mapping::proto::SerializedData proto;
  submap_ptr = nullptr;
  while (stream.ReadProto(&proto))
  {
    if (proto.data_case() == cartographer::mapping::proto::SerializedData::kSubmap)
    {
      submap_ptr = std::make_shared<cartographer::mapping::Submap3D>(proto.submap().submap_3d());
    }
  }
}

cartographer::io::SubmapSlice
MapLocalization3D::submapToSlice(const std::shared_ptr<Submap3D> submap,
                                 const cartographer::transform::Rigid3d &global_pose,
                                 const bool &is_high_resolution)
{
  proto::SubmapQuery::Response response_proto;
  submap->ToResponseProto(global_pose, &response_proto);
  cartographer_ros_msgs::SubmapQuery::Response response;
  for (const auto &texture_proto : response_proto.textures())
  {
    response.textures.emplace_back();
    auto &texture = response.textures.back();
    texture.cells.insert(texture.cells.begin(), texture_proto.cells().begin(),
                         texture_proto.cells().end());
    texture.width = texture_proto.width();
    texture.height = texture_proto.height();
    texture.resolution = texture_proto.resolution();
    texture.slice_pose =
        ToGeometryMsgPose(cartographer::transform::ToRigid3(texture_proto.slice_pose()));
    // 		LOG(INFO) << texture.width <<", " <<texture.height ;
  }
  response.status.message = "Success.";

  // 	const cartographer::transform::Rigid3d& first_node_global_pose;
  // 	global_tmp_ = first_node_global_pose.inverse();
  // 	std::map<::cartographer::mapping::SubmapId,
  // ::cartographer::io::SubmapSlice> submap_slices; 	SubmapId id(0,0);
  // 	cartographer::io::SubmapSlice& submap_slice = submap_slices[id];
  cartographer::io::SubmapSlice submap_slice;
  submap_slice.pose = global_pose;

  auto fetched_textures = std::make_shared<::cartographer::io::SubmapTextures>();
  //   fetched_textures->version = response.submap_version;
  for (const auto &texture : response.textures)
  {
    const std::string compressed_cells(texture.cells.begin(), texture.cells.end());
    fetched_textures->textures.emplace_back(::cartographer::io::SubmapTexture{
        ::cartographer::io::UnpackTextureData(compressed_cells, texture.width, texture.height),
        texture.width, texture.height, texture.resolution, ToRigid3d(texture.slice_pose)});
  }

  CHECK(!fetched_textures->textures.empty());

  auto fetched_texture = fetched_textures->textures.begin();
  if (!is_high_resolution) fetched_texture += 1;
  submap_slice.width = fetched_texture->width;
  submap_slice.height = fetched_texture->height;
  submap_slice.slice_pose = fetched_texture->slice_pose;
  submap_slice.resolution = fetched_texture->resolution;
  submap_slice.cairo_data.clear();
  submap_slice.surface = ::cartographer::io::DrawTexture(
      fetched_texture->pixels.intensity, fetched_texture->pixels.alpha, fetched_texture->width,
      fetched_texture->height, &submap_slice.cairo_data);
  return submap_slice;
  // 	auto painted_slices = cartographer::io::PaintSubmapSlices(submap_slices,
  // resolution_); 	img = getMatMap(painted_slices); 	cv::flip(img,img,0);
  // 	transpose(img,img);
  // 	max_box[0] = painted_slices.origin.x();
  // 	max_box[1] = painted_slices.origin.y();
}

bool MapLocalization3D::getReferenceLocationPoses(const geometry_msgs::Pose &mapPose,
                                                  geometry_msgs::Pose &targetPoseInWorld_pose,
                                                  geometry_msgs::Pose &target2reference_pose,
                                                  int &nearestIdx)
{
  Eigen::Vector3d worldLoc(mapPose.position.x, mapPose.position.y, mapPose.position.z);
  Eigen::Quaterniond q(mapPose.orientation.w, mapPose.orientation.x, mapPose.orientation.y,
                       mapPose.orientation.z);
  // 	Eigen::Vector3d eulerAngle= q.matrix().eulerAngles(2,1,0);
  kd_tree::pose cur_pose{worldLoc, q, Eigen::Vector2i(0, 0)};
  auto res = kd_tree_->nearest_point(cur_pose);
  worldLoc[2] = res.p(2);

  Eigen::Vector3d new_eul = res.q.toRotationMatrix().eulerAngles(2, 1, 0);
  Eigen::Vector3d origin_eul = q.toRotationMatrix().eulerAngles(2, 1, 0);

  if (cos(origin_eul(1)) < 0) origin_eul(0) += M_PI;

  if (cos(new_eul(1)) < 0)
    new_eul(0) = M_PI + origin_eul(0);
  else
    new_eul(0) = origin_eul(0);

  //   Eigen::Matrix3d rotMat =q.matrix();

  Eigen::Matrix3d rotMat;
  rotMat = Eigen::AngleAxisd(new_eul[0], ::Eigen::Vector3d::UnitZ()) *
           Eigen::AngleAxisd(new_eul[1], ::Eigen::Vector3d::UnitY()) *
           Eigen::AngleAxisd(new_eul[2], ::Eigen::Vector3d::UnitX());

  Eigen::Affine3d targetPoseInWorld;
  targetPoseInWorld.setIdentity();
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      targetPoseInWorld(i, j) = rotMat(i, j);
  targetPoseInWorld(0, 3) = worldLoc[0];
  targetPoseInWorld(1, 3) = worldLoc[1];
  targetPoseInWorld(2, 3) = worldLoc[2];
  tf::poseEigenToMsg(targetPoseInWorld, targetPoseInWorld_pose);
  Eigen::Affine3d nearestPose;
  getNearestPose(worldLoc, nearestPose, nearestIdx);

  if (nearestIdx == -1)
  {
    const std::string error_string =
        ("Invalid map with " + std::to_string(posesInGlobal_.size()) + " poses inside!");
    LOG(WARNING) << error_string;
    throw std::runtime_error(error_string);
    return false;
  }
  Eigen::Affine3d target2reference;
  target2reference = nearestPose.inverse() * targetPoseInWorld;
  // 	target2reference(2,3) = 0.0;
  LOG(INFO) << endl
            << "nearestIdx: " << nearestIdx << "\n origin_pose:\t translation("
            << mapPose.position.x << ", " << mapPose.position.y << ", " << mapPose.position.z
            << ") \t euler(" << q.toRotationMatrix().eulerAngles(2, 1, 0).transpose() << ")"
            << "\n nearest_pose:\t translation(" << res.p.transpose() << ") \t euler("
            << res.q.toRotationMatrix().eulerAngles(2, 1, 0).transpose() << ")"
            << "\n new world pose:\t translation(" << worldLoc.transpose() << ") \t euler("
            << rotMat.eulerAngles(2, 1, 0).transpose() << ")"
            << "\n target2reference: \n " << target2reference.matrix() << endl;
  tf::poseEigenToMsg(target2reference, target2reference_pose);
  //     response.reference_id[cnt] = nearestIdx;
  //     tf::poseEigenToMsg(target2reference, response.referencePose[cnt]);
  return true;
}

bool MapLocalization3D::referenceLocationServer(
    cartographer_ros_msgs::ReferenceLocationFromMap::Request &request,
    cartographer_ros_msgs::ReferenceLocationFromMap::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] " << ("getting reference coordinates ...");
  CHECK(kd_tree_ != nullptr);
  if (request.mapPose.size() == 0)
  {
    const std::string error_string = "No mapPose need to be transformed!";
    LOG(WARNING) << error_string;
    throw std::runtime_error(error_string);
    return true;
  }

  uint pose_cnt = request.mapPose.size();

  response.reference_id.resize(pose_cnt);
  response.referencePose.resize(pose_cnt);
  response.worldPose.resize(pose_cnt);

  for (uint cnt = 0; cnt < pose_cnt; cnt++)
  {
    Eigen::Affine3d targetPoseInWorld;
    Eigen::Affine3d target2reference;
    // 		int nearestIdx = -1;

    if (!getReferenceLocationPoses(request.mapPose[cnt], response.worldPose[cnt],
                                   response.referencePose[cnt], response.reference_id[cnt]))
      return true;
  }

  //   response.header = header_;
  return true;
}

bool MapLocalization3D::loadMapServer(cartographer_ros_msgs::SubmapImagesServer::Request &request,
                                      cartographer_ros_msgs::SubmapImagesServer::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] " << ("Loading 3d map ...");
  string map_path = request.map_path;
  map_path_ = map_path;
  LOG(INFO) << "map_path: " << map_path;
  setPosesAndSubmaps(map_path);
  if (debug_flag_) mergeFromPbstream(map_path);
  cv::Mat show_img;
  cartographer_ros_msgs::SubmapImageEntry entry;

  if (judgeFileExist(map_path + "map_data.xml"))
  {
    boost::property_tree::ptree pt, pt_poses;
    boost::property_tree::read_xml(map_path + "map_data.xml", pt_poses);
    if (pt_poses.empty())
    {
      LOG(WARNING) << "data.xml is empty! ";
    }
    pt = pt_poses.get_child("mapPng");

    size_of_map_[0] = pt.get<int>("width");
    size_of_map_[1] = pt.get<int>("height");
    float temp_resolution = pt.get<double>("resolution");
    std::string data;
    data = pt.get<std::string>("pose");
    double pose[7];
    sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
           &pose[4], &pose[5], &pose[6]);
    entry.pose_in_map.position.x = pose[1] * temp_resolution;
    entry.pose_in_map.position.y = pose[0] * temp_resolution;
    Mat temp_show_img = imread(map_path + "map.png", CV_LOAD_IMAGE_UNCHANGED);
    LOG(WARNING) << "temp_resolution: " << temp_resolution << ", resolution_:" << resolution_;
    if (fabs(temp_resolution - resolution_) > 1e-2)
    {
      LOG(WARNING) << "temp_resolution: " << temp_resolution << ", resolution_:" << resolution_;
      resize(temp_show_img, show_img,
             Size(temp_show_img.cols * temp_resolution / resolution_,
                  temp_show_img.rows * temp_resolution / resolution_));
      //       entry.pose_in_map.position.x = pose[1] * temp_resolution /
      //       resolution_; entry.pose_in_map.position.y = pose[0] *
      //       temp_resolution / resolution_;
    }
    else
      show_img = temp_show_img;
  }
  else
  {
    double max_box[2];
    getShowImage(map_path, show_img, max_box, show_high_resolution_map_);
    entry.pose_in_map.position.y = max_box[1] * resolution_;
    entry.pose_in_map.position.x = (show_img.rows - max_box[0]) * resolution_;

    Mat temp_read = imread(map_path + "show_map.png");
    if (temp_read.empty())
    {
      imwrite(map_path + "show_map.png", show_img);
      {
        boost::property_tree::ptree p_map;
        std::string data;
        data = to_string(resolution_);
        p_map.put("resolution", data);
        data.clear();
        data =
            to_string(entry.pose_in_map.position.x) + " " + to_string(entry.pose_in_map.position.y);
        p_map.put("max_box", data);
        data.clear();
        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(map_path + "show_map_data.xml", p_map, std::locale(),
                                        setting);
      }

      {
        boost::property_tree::ptree p_map, p_top;
        std::string data;
        const int width = show_img.cols;
        const int height = show_img.rows;
        data = to_string(height);
        p_map.put("width", data);
        data.clear();
        data = to_string(width);
        p_map.put("height", data);
        data.clear();
        data = to_string(resolution_);
        p_map.put("resolution", data);
        data.clear();
        double x = entry.pose_in_map.position.x / resolution_;
        double y = entry.pose_in_map.position.y / resolution_;
        cartographer::transform::Rigid3d::Vector translation(y, x, 0);
        cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2, sqrt(2) / 2, 0);
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(translation[i]) + " ";
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(quaternion.coeffs()[i]) + " ";
        data = data + std::to_string(quaternion.coeffs()[3]);
        p_map.put("pose", data);
        data.clear();
        p_top.add_child("mapPng", p_map);

        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(map_path + "map_data.xml", p_top, std::locale(), setting);
      }
    }
  }
  std_msgs::Header header;
  sensor_msgs::CompressedImagePtr msg_ptr =
      cv_bridge::CvImage(header, "bgra8", show_img).toCompressedImageMsg(cv_bridge::PNG);
  entry.image = *msg_ptr;
  entry.resolution = resolution_;
  entry.trajectory_id = 0;
  entry.submap_index = 0;
  response.submap_images.push_back(entry);
  maxes_[0] = entry.pose_in_map.position.x;
  maxes_[1] = entry.pose_in_map.position.y;
  size_of_map_[0] = show_img.cols;
  size_of_map_[1] = show_img.rows;
  LOG(INFO) << ("Loaded 3d map done...");
  return true;
}

bool MapLocalization3D::loadSubmapsServer(
    cartographer_ros_msgs::SubmapImagesServer::Request &request,
    cartographer_ros_msgs::SubmapImagesServer::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] " << ("Loading 3d submaps ...");
  // 	string path = request.map_path;
  if (submaps_.empty()) loadSubmaps();
  for (int i = 0; i < submap_size_; i++)
  {
    Mat img_tmp;
    double max_box[2];
    // 		cartographer::transform::Rigid3d global_pose;
    // 		getSubmapAndPose(path,i,img_tmp,max_box,global_pose,true);
    std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice> submap_slices;
    submap_slices.insert(
        make_pair(SubmapId(0, 0), submapToSlice(submaps_[i], global_poses_[i], true)));
    slicesToImage(submap_slices, img_tmp, max_box);
    cartographer_ros_msgs::SubmapImageEntry entry, entry_box;
    entry_box.pose_in_map.position.y = max_box[1] * resolution_;
    entry_box.pose_in_map.position.x = (img_tmp.rows - max_box[0]) * resolution_;
    entry.pose_in_map = ToGeometryMsgPose(global_poses_[i]);
    std_msgs::Header header;
    sensor_msgs::CompressedImagePtr msg_ptr =
        cv_bridge::CvImage(header, "bgra8", img_tmp).toCompressedImageMsg(cv_bridge::PNG);
    entry.image = *msg_ptr;
    response.submap_images.push_back(entry);
    response.submap_images.push_back(entry_box);
  }
  LOG(INFO) << ("Loaded 3d submaps done...");
  return true;
}

void MapLocalization3D::getNearestPose(const Eigen::Vector3d &worldLoc,
                                       Eigen::Affine3d &nearestPose, int &nearestIdx)
{
  double minDistance = 100000;
  // 	CHECK(worldLoc(2) == 0);
  for (uint i = 0; i < posesInGlobal_.size(); i++)
  {
    //     LOG(INFO) << "posesInGlobal_[i]:\n" << posesInGlobal_[i].matrix() <<
    //     std::endl;

    Eigen::Vector3d dis_vec =
        Eigen::Vector3d(posesInGlobal_[i](0, 3), posesInGlobal_[i](1, 3), worldLoc(2)) - worldLoc;

    double distance = dis_vec.norm();
    if (distance < minDistance)
    {
      minDistance = distance;
      nearestIdx = i;
      nearestPose = posesInGlobal_[i];
      //       for (int row = 0; row < 4; row++)
      //         for (int col = 0; col < 4; col++)
      //           nearestPose(row, col) = posesInGlobal_[i](row, col);
    }
  }
}

void MapLocalization3D::pubSubmapPointCloud(const sensor_msgs::PointCloudPtr &msg)
{
  int id = msg->channels[0].values[0];
  sensor_msgs::PointCloud2 pub_msg = getPointCloud2(id);
  // 	pub_msg.header = msg->header;
  LOG(INFO) << "here";
  submap_point_cloud2_pub_.publish(pub_msg);
}

void MapLocalization3D::loadLandmark()
{
  landmark_full_array_.markers.clear();

  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(localization_path_ + "landmark.xml", pt);
  if (pt.empty())
  {
    LOG(WARNING) << "landmark.xml is empty! ";
    return;
  }
  string xml, xml_id;
  string ns;
  for (auto &mark : pt)
  {
    if (mark.first == "landmark")
    {
      int visible = stoi(mark.second.get("visible", ""));
      if (!visible) continue;
      ns = mark.second.get("ns", "");
      xml_id = mark.second.get("id", " ");
      xml = mark.second.get("transform", " ");
      float value[7];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6]);
      // TODO new msg will be here
      //      cartographer_ros_msgs::LandmarkEntry landmark;
      //      landmark.id = xml_id;
      geometry_msgs::Pose pose;
      pose.position.x = value[0];
      pose.position.y = value[1];
      pose.position.z = value[2];

      pose.orientation.x = value[3];
      pose.orientation.y = value[4];
      pose.orientation.z = value[5];
      pose.orientation.w = value[6];
      //      landmark.tracking_from_landmark_transform = pose;
      visualization_msgs::Marker marker;
      marker.ns = ns;
      marker.id = stoi(xml_id);
      marker.pose = pose;
      marker.type = 1;
      landmark_full_array_.markers.push_back(marker);
    }
  }
}

bool MapLocalization3D::loadLandmarksServer(
    cartographer_ros_msgs::LandmarkListServer::Request &request,
    cartographer_ros_msgs::LandmarkListServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "Loading landmarks ...";
  getPath(request.map_name, localization_path_);
  loadLandmark();
  if (landmark_full_array_.markers.size() == 0)
  {
    response.error_message = "can not successfully read landmark.xml ";
    LOG(WARNING) << response.error_message;
    return true;
  }

  for (auto &mark : landmark_full_array_.markers)
  {
    cartographer_ros_msgs::LandmarkNewEntry landmark;
    landmark.id = to_string(mark.id);
    landmark.ns = mark.ns;
    landmark.visible = 1;
    landmark.tracking_from_landmark_transform = mark.pose;
    response.landmarks.push_back(landmark);
  }
  return true;
}

bool MapLocalization3D::updateLandmarks(
    cartographer_ros_msgs::UpdateLandmarkServer::Request &request,
    cartographer_ros_msgs::UpdateLandmarkServer::Response &response)
{
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "Updating landmarks ...";
  LOG(INFO) << "request.landmarks.size: " << request.landmarks.size();
  if (request.landmarks.size() == 0)
  {
    response.error_message = "no landmarks!";
    LOG(WARNING) << response.error_message;
    return false;
  }
  boost::property_tree::xml_writer_settings<string> setting(' ', 1);
  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(localization_path_ + "landmark.xml", pt,
                                             boost::property_tree::xml_parser::trim_whitespace);
  if (pt.empty())
  {
    LOG(WARNING) << "landmark.xml is empty! ";
    return true;
  }
  for (auto &update_mark : request.landmarks)
  {
    //    for(auto& mark:landmark_full_array_.markers)
    //    {
    //      if(mark.ns==update_mark.ns && mark.id == update_mark.id)
    //      {
    //        mark.type = -1;
    //        break;
    //      }
    //    }
    for (auto &mark : pt)
    {
      string ns = mark.second.get("ns", "");
      string xml_id = mark.second.get("id", "");
      if (ns == update_mark.ns && xml_id == update_mark.id)
      {
        mark.second.put("visible", 0);
        break;
      }
    }
  }

  boost::property_tree::write_xml(localization_path_ + "landmark.xml", pt, std::locale(), setting);
  loadLandmark();
  return true;
}

bool MapLocalization3D::set2DPointsServer(map_manager_msgs::Set2dPoints::Request &request,
                                          map_manager_msgs::Set2dPoints::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "set 2d points";
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  string full_path;
  getPath(request.map_name, full_path);
  Mat mask = Mat(Size(size_of_map_[0], size_of_map_[1]), CV_8UC1, Scalar(255));

  std::string data;
  boost::property_tree::ptree p_info;
  boost::property_tree::ptree p_list;
  int cnt = 0;
  for (auto &it : request.topo_corner_points)
  {
    map_manager_msgs::Points2d corner_points = it;
    vector<vector<Point>> contours;
    vector<Point> contour;
    for (auto &point : corner_points.points)
    {
      Eigen::Vector2d point_in_map(point.x, point.y);
      Eigen::Vector2i imageLoc;
      world2imageLoc(Eigen::Vector2d(maxes_[0], maxes_[1]), resolution_, point_in_map, imageLoc);
      if (imageLoc(1) > size_of_map_[1] || imageLoc(0) > size_of_map_[0] || imageLoc(0) < 0 ||
          imageLoc(1) < 0)
      {
        LOG(INFO) << "point_in_map: " << point_in_map.transpose() << endl
                  << "imageLoc: " << imageLoc.transpose() << endl
                  << "max: " << maxes_[0] << ", " << maxes_[1] << endl
                  << "size_of_map_: " << size_of_map_[0] << ", " << size_of_map_[1] << endl
                  << "resolution_: " << resolution_;
      }
      Point p(imageLoc(0), imageLoc(1));
      contour.push_back(p);
      data = data + std::to_string(point.x) + " ";
      if (point.x == corner_points.points.back().x && point.y == corner_points.points.back().y)
        data = data + std::to_string(point.y);
      else
        data = data + std::to_string(point.y) + " ";
      cnt++;
    }

    p_info.put("points_size", cnt);
    p_info.put("points", data);
    p_list.add_child("corner_points", p_info);
    data.clear();
    cnt = 0;
    contours.push_back(contour);
    cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
  }
  boost::property_tree::write_xml(full_path + "corner_points.xml", p_list, std::locale(), setting);

  imwrite(full_path + "bool_image.png", mask);
  return true;
}

bool MapLocalization3D::get2DPointsServer(map_manager_msgs::Get2dPoints::Request &request,
                                          map_manager_msgs::Get2dPoints::Response &response)
{
  LOG(INFO) << "[" << ros::Time::now() << "] "
            << "get 2d points";
  string full_path;
  getPath(request.map_name, full_path);

  boost::property_tree::ptree pt;
  boost::property_tree::xml_parser::read_xml(full_path + "corner_points.xml", pt);
  if (pt.empty())
  {
    LOG(WARNING) << "corner_points.xml is empty! ";
    return true;
  }
  string xml;
  for (auto &mark : pt)
  {
    if (mark.first == "corner_points")
    {
      int points_size = stoi(mark.second.get("points_size", ""));
      CHECK(points_size == 4);

      xml = mark.second.get("points", " ");
      float value[8];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6], &value[7]);
      // TODO new msg will be here
      map_manager_msgs::Points2d corner_points;
      for (int i = 0; i < points_size; i++)
      {
        map_manager_msgs::Point2d p;
        p.x = value[i * 2];
        p.y = value[i * 2 + 1];
        corner_points.points.push_back(p);
      }
      response.topo_corner_points.push_back(corner_points);
    }
  }
  return true;
}

bool MapLocalization3D::setInitialPose(vtr_msgs::SetInitialPose::Request &request,
                                       vtr_msgs::SetInitialPose::Response &response)
{
  // request.base2map is base2reference.
  LOG(INFO) << "request base2reference pose: "
            << "\n position(x,y,z):" << request.base2map.pose.pose.position.x << ", "
            << request.base2map.pose.pose.position.y << ", "
            << request.base2map.pose.pose.position.z
            << "\t orientation(x,y,z,w):" << request.base2map.pose.pose.orientation.x << ", "
            << request.base2map.pose.pose.orientation.y << ","
            << request.base2map.pose.pose.orientation.z << ", "
            << request.base2map.pose.pose.orientation.w;
  LOG(INFO) << "Manual localization: init pose come!";
  {
    vtr_msgs::GlobalLocalizationPose pose_temp;
    pose_temp.header.stamp = ros::Time::now() - ros::Duration(0.3);
    pose_temp.header.frame_id = "map";
    pose_temp.sensor2reference.pose.orientation.w = request.base2map.pose.pose.orientation.w;
    pose_temp.sensor2reference.pose.orientation.x = request.base2map.pose.pose.orientation.x;
    pose_temp.sensor2reference.pose.orientation.y = request.base2map.pose.pose.orientation.y;
    pose_temp.sensor2reference.pose.orientation.z = request.base2map.pose.pose.orientation.z;

    pose_temp.sensor2reference.pose.position.x = request.base2map.pose.pose.position.x;
    pose_temp.sensor2reference.pose.position.y = request.base2map.pose.pose.position.y;
    pose_temp.sensor2reference.pose.position.z = request.base2map.pose.pose.position.z;
    geometry_msgs::TransformStamped base2odom_tf;
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    while (1)
    {
      try
      {
        base2odom_tf = tfBuffer_.lookupTransform("odom", "base_footprint", pose_temp.header.stamp);
        break;
      }
      catch (tf::TransformException ex)
      {
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
        if (duration.count() > 0.5)
        {
          LOG(WARNING) << "Can't get transform between odom and base_footprint!";
          response.is_succeed = false;
          response.error_message = "Can't get transform between odom and base_footprint!";
          return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
    }
    pose_temp.sensor2odom.position.x = base2odom_tf.transform.translation.x;
    pose_temp.sensor2odom.position.y = base2odom_tf.transform.translation.y;
    pose_temp.sensor2odom.position.z = base2odom_tf.transform.translation.z;

    pose_temp.sensor2odom.orientation.x = base2odom_tf.transform.rotation.x;
    pose_temp.sensor2odom.orientation.y = base2odom_tf.transform.rotation.y;
    pose_temp.sensor2odom.orientation.z = base2odom_tf.transform.rotation.z;
    pose_temp.sensor2odom.orientation.w = base2odom_tf.transform.rotation.w;

    for (int i = 0; i < 6; i++)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-9;
    }
    pose_temp.localization_type = "dragging";
    pose_temp.pose_valid = true;
    pose_temp.reference_id = request.base2map.reference_id;
    vtr_msgs::SetGlobalPose global_pose_srv;
    global_pose_srv.request.pose = pose_temp;

    int k = 0;
    while (1)
    {
      if (initial_pose_client_.call(global_pose_srv))
      {
        LOG(INFO) << "initial_pose client call success!";
        break;
      }
      else
      {
        k++;
        if (k > 20)
        {
          LOG(ERROR) << "initial_pose_client_ call failed!";
          return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    }
  }
  ros::Duration(0.08).sleep();
  localization_request_pub_.publish(std_msgs::Empty());
  response.is_succeed = true;
  return true;
}

bool MapLocalization3D::setInitialPose2(vtr_msgs::SetInitialPose::Request &request,
                                        vtr_msgs::SetInitialPose::Response &response)
{
  // request.base2map is base2reference.
  LOG(INFO) << "request base2reference pose: "
            << "\n position(x,y,z):" << request.base2map.pose.pose.position.x << ", "
            << request.base2map.pose.pose.position.y << ", "
            << request.base2map.pose.pose.position.z
            << "\t orientation(x,y,z,w):" << request.base2map.pose.pose.orientation.x << ", "
            << request.base2map.pose.pose.orientation.y << ","
            << request.base2map.pose.pose.orientation.z << ", "
            << request.base2map.pose.pose.orientation.w;
  LOG(INFO) << "Manual localization: init global pose come!";
  geometry_msgs::Pose ref_pose, world_pose;
  int ref_id;
  getReferenceLocationPoses(request.base2map.pose.pose, world_pose, ref_pose, ref_id);
  {
    vtr_msgs::GlobalLocalizationPose pose_temp;
    pose_temp.header.stamp = ros::Time::now() - ros::Duration(0.3);
    pose_temp.header.frame_id = "map";
    //     pose_temp.sensor2reference.pose.orientation.w =
    //     request.base2map.pose.pose.orientation.w;
    //     pose_temp.sensor2reference.pose.orientation.x =
    //     request.base2map.pose.pose.orientation.x;
    //     pose_temp.sensor2reference.pose.orientation.y =
    //     request.base2map.pose.pose.orientation.y;
    //     pose_temp.sensor2reference.pose.orientation.z =
    //     request.base2map.pose.pose.orientation.z;
    //
    //     pose_temp.sensor2reference.pose.position.x =
    //     request.base2map.pose.pose.position.x;
    //     pose_temp.sensor2reference.pose.position.y =
    //     request.base2map.pose.pose.position.y;
    //     pose_temp.sensor2reference.pose.position.z =
    //     request.base2map.pose.pose.position.z;
    pose_temp.sensor2reference.pose = ref_pose;
    pose_temp.reference_id = ref_id;
    LOG(WARNING) << ref_id << ", " << ref_pose;
    geometry_msgs::TransformStamped base2odom_tf;
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    while (1)
    {
      try
      {
        base2odom_tf = tfBuffer_.lookupTransform("odom", "base_footprint", pose_temp.header.stamp);
        break;
      }
      catch (tf::TransformException ex)
      {
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
        if (duration.count() > 0.5)
        {
          LOG(WARNING) << "Can't get transform between odom and base_footprint!";
          response.is_succeed = false;
          response.error_message = "Can't get transform between odom and base_footprint!";
          return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
    }
    pose_temp.sensor2odom.position.x = base2odom_tf.transform.translation.x;
    pose_temp.sensor2odom.position.y = base2odom_tf.transform.translation.y;
    pose_temp.sensor2odom.position.z = base2odom_tf.transform.translation.z;

    pose_temp.sensor2odom.orientation.x = base2odom_tf.transform.rotation.x;
    pose_temp.sensor2odom.orientation.y = base2odom_tf.transform.rotation.y;
    pose_temp.sensor2odom.orientation.z = base2odom_tf.transform.rotation.z;
    pose_temp.sensor2odom.orientation.w = base2odom_tf.transform.rotation.w;

    for (int i = 0; i < 6; i++)
    {
      pose_temp.sensor2reference.covariance[i * 6 + i] = 1e-9;
    }
    pose_temp.localization_type = "dragging";
    pose_temp.pose_valid = true;
    vtr_msgs::SetGlobalPose global_pose_srv;
    global_pose_srv.request.pose = pose_temp;

    int k = 0;
    while (1)
    {
      if (initial_pose_client_.call(global_pose_srv))
      {
        LOG(INFO) << "initial_pose client call success!";
        break;
      }
      else
      {
        k++;
        if (k > 20)
        {
          LOG(ERROR) << "initial_pose_client_ call failed!";
          return false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    }
  }
  ros::Duration(0.08).sleep();
  localization_request_pub_.publish(std_msgs::Empty());
  response.is_succeed = true;
  return true;
}

void MapLocalization3D::loadSubmaps()
{
  LOG(INFO) << "load submap3D..";
  for (int i = 0; i < submap_size_; i++)
  {
    std::shared_ptr<Submap3D> submap_ptr;
    readSubmap3DFromPbstream(map_path_ + "frames/" + to_string(i) + "/submap.pbstream", submap_ptr);
    submaps_.push_back(submap_ptr);
  }
  LOG(INFO) << "load submap3D done.";
}

void MapLocalization3D::clear()
{
  for (auto &srv : servers_)
    srv.shutdown();
  localization_point_cloud2_sub_.shutdown();
  submap_point_cloud2_pub_.shutdown();
  global_localization_pub_.shutdown();
  localization_request_pub_.shutdown();
  initial_pose_client_.shutdown();
}
} // namespace map_manager