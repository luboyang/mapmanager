/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "3d/map_mapping_3d.h"

#include <cartographer/mapping/id.h>
#include <cartographer_ros_msgs/SubmapQuery.h>
using namespace std;
using namespace cv;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager {
MapMapping3D::MapMapping3D() {
  LOG(INFO) << "start 3d mapping ";
  submap_query_client_ =
      nh_.serviceClient<cartographer_ros_msgs::SubmapQuery>("submap_query");
  shutdown_carto_client_ =
      nh_.serviceClient<cartographer_ros_msgs::FinishMapping>("/shutdown_cartographer2");
  wall_timer_ = nh_.createWallTimer(ros::WallDuration(0.50),
                                    &MapMapping3D::drawAndPublish, this);
  submap_images_publisher_ = nh_.advertise<cartographer_ros_msgs::SubmapImages>(
      "/map_manager_mapping_3d/submap_images", 100);
  submap_list_subscriber_ =
      nh_.subscribe("/submap_list", 1, &MapMapping3D::HandleSubmapList, this);
  servers_.push_back( nh_.advertiseService("/map_manager_mapping_3d/save_map",
                                   &MapMapping3D::saveMapServer, this));
  servers_.push_back(
      nh_.advertiseService("/shutdown_cartographer", &MapMapping3D::shutdownCarto, this));
  display_ratio_ = 0.5;
  save_image_resolution_ = 0.05;
  ros::param::get("/map_manager_3d/resolution", display_ratio_);
  LOG(INFO) << "3d mapping resolution: " << display_ratio_;
}

MapMapping3D::~MapMapping3D() {}

void MapMapping3D::HandleSubmapList(
    const cartographer_ros_msgs::SubmapList::ConstPtr& msg) {
  absl::MutexLock locker(&mutex_);
  interpretateSubmapList(msg, submap_slices_, submap_images_,
                         &submap_query_client_);
  last_timestamp_ = msg->header.stamp;
  last_frame_id_ = msg->header.frame_id;
}

void MapMapping3D::drawAndPublish(
    const ros::WallTimerEvent& unused_timer_event) {
  absl::MutexLock locker(&mutex_);
  if (submap_slices_.empty()) return;
  std::size_t i = 0;
  for (auto& slice_pair : submap_slices_) {
    auto& slice_temp = slice_pair.second;
    bool is_old = true;

    sensor_msgs::CompressedImage tmp;
    cartographer_ros_msgs::SubmapImageEntry& submap_image_tmp =
        submap_images_[slice_pair.first];
    submap_image_tmp.image = tmp;
    cartographer::transform::Rigid3d pose_in_map =
        slice_temp.pose * slice_temp.slice_pose;
    submap_image_tmp.pose_in_map = ToGeometryMsgPose(pose_in_map);
    submap_image_tmp.resolution = display_ratio_;
    //     submap_image_tmp.pose_in_map.position.y = painted_slice.origin.y() *
    //     resolution_; submap_image_tmp.pose_in_map.position.x = (img.rows -
    //     painted_slice.origin.x()) * resolution_;
    if (submap_image_tmp.submap_version != slice_temp.version) {
      submap_image_tmp.submap_version = slice_temp.version;
      is_old = false;
    } else {
      if (i >= (submap_images_.size() - 3) || submap_images_.size() <= 3)
        is_old = false;
      else {
        if (slice_temp.cairo_data.size() != 0) {
          std::vector<uint32_t>().swap(slice_temp.cairo_data);
        }
      }
    }

    if (is_old) {
      i++;
      continue;
    }

    // get mat map
    //     double resolution = slice_temp.resolution;
    cartographer::transform::Rigid3d slice_pose;
    slice_pose = slice_temp.slice_pose;
    //     submap2global.Rotation(slice_temp.pose.rotation());
    //     LOG(INFO) << slice_temp.slice_pose ;
    //     = slice_temp.pose;
    slice_temp.slice_pose = pose_in_map;
    //     LOG(INFO) << slice_temp.slice_pose ;
    auto painted_slice = PaintSubmapSlice(slice_temp, display_ratio_);
    slice_temp.slice_pose = slice_pose;
    Mat img = getMatMap(painted_slice, PUB_MAP);
    std_msgs::Header header;
    header.stamp = last_timestamp_;
    header.frame_id = last_frame_id_;
    sensor_msgs::CompressedImagePtr msg_ptr =
        cv_bridge::CvImage(header, "bgra8", img)
            .toCompressedImageMsg(cv_bridge::PNG);
    submap_image_tmp.image = *msg_ptr;
    i++;
    //    LOG(INFO) <<"submap_slices_.size(): " <<submap_slices_.size()  <<" id:
    //    " << submap_pair.submap_index << "   version: "
    //    <<submap_pair.submap_version;
  }
  // 	auto painted_slice =
  // cartographer::io::PaintSubmapSlices(submap_slices_,resolution_); 	int
  // version_sum = 0; 	for (const auto& pair : submap_slices_)
  // 	{
  // 		version_sum += pair.second.version;
  // 	}
  // 	Mat img = getMatMap(painted_slice,SHOW_MAP);
  // // 	if(img.rows==0&&img.cols==0)
  // // 		exit(0);
  // // 	cv::flip(img,img,0);
  // // 	transpose(img,img);
  // 	std_msgs::Header header;
  // 	header.stamp = last_timestamp_;
  // 	header.frame_id = last_frame_id_;
  // // 	imshow("3d_mapping",img);
  // // 	waitKey(2);
  // 	cartographer_ros_msgs::SubmapImages submap_image;
  // 	submap_image.header.stamp = ros::Time::now();
  // 	submap_image.submap_images.resize(1);
  // 	sensor_msgs::CompressedImagePtr msg_ptr =
  // cv_bridge::CvImage(header,"bgra8",img).toCompressedImageMsg(cv_bridge::PNG);
  // 	cartographer_ros_msgs::SubmapImageEntry& submap_pair=
  // submap_image.submap_images[0];
  // // 	SubmapId id(0,0);
  // // 	io::SubmapSlice& slice = submap_slices_[id];
  // // 	cartographer::transform::Rigid3d pose_in_map = slice.pose *
  // slice.slice_pose;
  // // 	LOG(INFO) << "pose_in_map1: " << pose_in_map.translation() ;
  // // 	submap_pair.pose_in_map = ToGeometryMsgPose(pose_in_map);
  // 	submap_pair.image = *msg_ptr;
  // 	cartographer_ros_msgs::SubmapImageEntry& submap_pair_max=
  // submap_image.submap_images[1]; 	submap_pair.pose_in_map.position.y =
  // painted_slice.origin.y() * resolution_; 	submap_pair.pose_in_map.position.x
  // = (img.rows - painted_slice.origin.x()) * resolution_;
  // // 	LOG(INFO) << "pose_in_map2: " << submap_pair.pose_in_map.position.x
  // <<"," <<submap_pair.pose_in_map.position.y ;
  // 	submap_pair.pose_in_map.orientation.w =1;
  // 	submap_pair.submap_version = version_sum;
  // 	LOG(INFO) << pose_in_map ;
  cartographer_ros_msgs::SubmapImages submap_images_msg;
  submap_images_msg.header.stamp = last_timestamp_;
  for (auto pair : submap_images_) {
    submap_images_msg.submap_images.push_back(pair.second);
  }

  submap_images_publisher_.publish(submap_images_msg);
}
cartographer::io::SubmapSlice MapMapping3D::submapToSlice(
    const std::shared_ptr<Submap3D> submap,
    const cartographer::transform::Rigid3d& global_pose,
    const bool& is_high_resolution) {
  proto::SubmapQuery::Response response_proto;

  submap->ToResponseProto(global_pose, &response_proto);

  cartographer_ros_msgs::SubmapQuery::Response response;
  for (const auto& texture_proto : response_proto.textures()) {
    response.textures.emplace_back();
    auto& texture = response.textures.back();
    texture.cells.insert(texture.cells.begin(), texture_proto.cells().begin(),
                         texture_proto.cells().end());
    texture.width = texture_proto.width();
    texture.height = texture_proto.height();
    texture.resolution = texture_proto.resolution();
    texture.slice_pose = ToGeometryMsgPose(
        cartographer::transform::ToRigid3(texture_proto.slice_pose()));
    //    LOG(INFO) << texture.width <<", " <<texture.height ;
  }
  response.status.message = "Success.";

  cartographer::io::SubmapSlice submap_slice;
  submap_slice.pose = global_pose;

  auto fetched_textures =
      std::make_shared<::cartographer::io::SubmapTextures>();
  //   fetched_textures->version = response.submap_version;
  for (const auto& texture : response.textures) {
    const std::string compressed_cells(texture.cells.begin(),
                                       texture.cells.end());
    fetched_textures->textures.emplace_back(::cartographer::io::SubmapTexture{
        ::cartographer::io::UnpackTextureData(compressed_cells, texture.width,
                                              texture.height),
        texture.width, texture.height, texture.resolution,
        ToRigid3d(texture.slice_pose)});
  }

  CHECK(!fetched_textures->textures.empty());

  auto fetched_texture = fetched_textures->textures.begin();
  if (!is_high_resolution) fetched_texture += 1;
  submap_slice.width = fetched_texture->width;
  submap_slice.height = fetched_texture->height;
  submap_slice.slice_pose = fetched_texture->slice_pose;
  submap_slice.resolution = fetched_texture->resolution;
  submap_slice.cairo_data.clear();
  submap_slice.surface = ::cartographer::io::DrawTexture(
      fetched_texture->pixels.intensity, fetched_texture->pixels.alpha,
      fetched_texture->width, fetched_texture->height,
      &submap_slice.cairo_data);
  return submap_slice;
}

void MapMapping3D::saveShowImage(const string& path) {
  boost::property_tree::ptree pt;
  boost::property_tree::ptree pt2;
  int submap_cnt = -1;
  int submap_cnt2 = -1;
  // get submap size
  {
    try {
      boost::property_tree::read_xml(path + "origin_map.xml", pt);
      if (pt.empty()) {
        const std::string error_string = "Load " + path + "map.xml failed!";
        ROS_FATAL("%s", error_string.c_str());
      }
      submap_cnt = pt.get<int>("map.property.node_count");
    } catch (std::exception& e) {
      LOG(WARNING) << "Load " + path + "origin_map.xml failed!";
    }

    try {
      boost::property_tree::read_xml(path + "map.xml", pt2);
      if (pt2.empty()) {
        const std::string error_string = "Load " + path + "map.xml failed!";
        ROS_FATAL("%s", error_string.c_str());
      }
      submap_cnt2 = pt2.get<int>("map.property.node_count");
    } catch (std::exception& e) {
      LOG(WARNING) << "Load " + path + "map.xml failed!";
    }
    if (submap_cnt2 == -1 && submap_cnt == -1) return;
  }

  int cnt_max = max(submap_cnt, submap_cnt2);
  LOG(INFO) << "submap_cnt: " << submap_cnt << ", " << submap_cnt2;
  std::vector<std::shared_ptr<cartographer::mapping::Submap3D>> submaps;
  std::vector<cartographer::transform::Rigid3d> global_poses;

  {
    for (int i = 0; i < cnt_max; i++) {
      std::shared_ptr<Submap3D> submap_ptr = nullptr;
      io::ProtoStreamReader stream(path + "frames/" + to_string(i) +
                                   "/submap.pbstream");

      cartographer::mapping::proto::SerializedData proto;
      while (stream.ReadProto(&proto)) {
        if (proto.data_case() ==
            cartographer::mapping::proto::SerializedData::kSubmap) {
          submap_ptr = std::make_shared<cartographer::mapping::Submap3D>(
              proto.submap().submap_3d());
        }
      }
      submaps.push_back(submap_ptr);

      // get submap global pose
      {
        boost::property_tree::ptree pt_poses;
        boost::property_tree::read_xml(
            path + "frames/" + to_string(i) + "/data.xml", pt_poses);
        if (pt_poses.empty()) {
          LOG(WARNING) << "data.xml is empty! ";
          return;
        }
        std::string data;
        data = pt_poses.get<std::string>("submap_global_pose");
        double pose[7];
        sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1],
               &pose[2], &pose[3], &pose[4], &pose[5], &pose[6]);

        Eigen::Quaterniond q =
            Eigen::Quaterniond(pose[6], pose[3], pose[4], pose[5]);
        global_poses.push_back(
            cartographer::transform::Rigid3d({pose[0], pose[1], pose[2]}, q));
      }
    }

    std::map<cartographer::mapping::SubmapId, cartographer::io::SubmapSlice>
        submap_slices;

    for (int i = 0; i < cnt_max; i++) {
      submap_slices.insert(make_pair(
          SubmapId(0, i), submapToSlice(submaps[i], global_poses[i], true)));
    }

    auto painted_slices = cartographer::io::PaintSubmapSlices(
        submap_slices, save_image_resolution_);
    Mat show_img = getMatMap(painted_slices);
    double max_box[2];
    max_box[0] =
        (show_img.rows - painted_slices.origin.x()) * save_image_resolution_;
    max_box[1] = painted_slices.origin.y() * save_image_resolution_;
    //     double size_of_img[2];
    //     size_of_img[0] = show_img.rows;
    //     size_of_img[1] = show_img.cols;
    {
      imwrite(path + "show_map.png", show_img);
      imwrite(path + "map.png", show_img);
      {
        boost::property_tree::ptree p_map;
        std::string data;
        data = to_string(save_image_resolution_);
        p_map.put("resolution", data);
        data.clear();
        data = to_string(max_box[0]) + " " + to_string(max_box[1]);
        p_map.put("max_box", data);
        data.clear();
        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(path + "show_map_data.xml", p_map,
                                        std::locale(), setting);
      }

      {
        boost::property_tree::ptree p_map, p_top;
        std::string data;
        const int width = show_img.cols;
        const int height = show_img.rows;
        data = to_string(height);
        p_map.put("width", data);
        data.clear();
        data = to_string(width);
        p_map.put("height", data);
        data.clear();
        data = to_string(save_image_resolution_);
        p_map.put("resolution", data);
        data.clear();
        double x = max_box[0] / save_image_resolution_;
        double y = max_box[1] / save_image_resolution_;
        cartographer::transform::Rigid3d::Vector translation(y, x, 0);
        cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2,
                                                                sqrt(2) / 2, 0);
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(translation[i]) + " ";
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(quaternion.coeffs()[i]) + " ";
        data = data + std::to_string(quaternion.coeffs()[3]);
        p_map.put("pose", data);
        data.clear();
        p_top.add_child("mapPng", p_map);

        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(path + "map_data.xml", p_top,
                                        std::locale(), setting);
      }
    }
  }
  LOG(WARNING) << "save all done";
}

bool MapMapping3D::saveMapServer(
    cartographer_ros_msgs::SaveMapServer::Request& request,
    cartographer_ros_msgs::SaveMapServer::Response& response) {
  absl::MutexLock locker(&mutex_);
  LOG(INFO) << "save map in map-manager!";
  // 	auto painted_slice =
  // cartographer::io::PaintSubmapSlices(submap_slices_,resolution_);
  string map_name = request.map_name;
  LOG(INFO) << "map_name: " << map_name;
  string path;
  getPath(map_name, path);
  LOG(INFO) << "path: " << path;
  saveShowImage(path);
  LOG(WARNING) << "start save uwb points";
  uwb_.reset(new uwbPoseOptimization(path));
  uwb_->compute();
  LOG(WARNING) << "save uwb points";
  return true;
}
bool MapMapping3D::shutdownCarto(cartographer_ros_msgs::FinishMapping::Request &request,
                               cartographer_ros_msgs::FinishMapping::Response &response)
{
  absl::MutexLock locker(&mutex_);
  submap_list_subscriber_.shutdown();
  cartographer_ros_msgs::FinishMapping srv;
  srv.request = request;
  if (!shutdown_carto_client_.call(srv))
  {
    LOG(ERROR) << "call shutdown_cartographer2 failed.";
    response.success = false;
  }
  response = srv.response;
  return true;
}

void MapMapping3D::clear() {
  absl::MutexLock locker(&mutex_);
  for(auto it:servers_)
    it.shutdown();
  submap_query_client_.shutdown();
  submap_list_subscriber_.shutdown();
  submap_images_publisher_.shutdown();
}

}  // namespace map_manager