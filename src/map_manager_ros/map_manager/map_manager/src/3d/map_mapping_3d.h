/*
 * Copyright 2019 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef MAPMAPPING3D_H
#define MAPMAPPING3D_H
#include "map_interface.h"

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>

#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/3d/submap_3d.h>
#include <cartographer/mapping/trajectory_node.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer_ros_msgs/SubmapList.h>
#include <cartographer_ros_msgs/SubmapImages.h>
#include <cartographer_ros_msgs/SaveMapServer.h>
#include <cartographer_ros_msgs/FinishMapping.h>
#include "uwb/uwb_pose_optimization.h"
namespace map_manager{
class MapMapping3D:public MapInterface
{
public:
	MapMapping3D();
	~MapMapping3D();
	virtual void clear() override;
  void saveShowImage(const std::string& path);
private:
	
	void HandleSubmapList(const cartographer_ros_msgs::SubmapList::ConstPtr& msg);
	void drawAndPublish(const ros::WallTimerEvent& unused_timer_event);
	bool saveMapServer(cartographer_ros_msgs::SaveMapServer::Request& request, 
										cartographer_ros_msgs::SaveMapServer::Response& response);
  bool shutdownCarto(cartographer_ros_msgs::FinishMapping::Request &request,
                     cartographer_ros_msgs::FinishMapping::Response &response);
  cartographer::io::SubmapSlice submapToSlice(const std::shared_ptr<cartographer::mapping::Submap3D > submap,  
                                 const cartographer::transform::Rigid3d& global_pose,
                                 const bool& is_high_resolution);
// 	ros::NodeHandle nh_;
	absl::Mutex mutex_;
	
	std::string last_frame_id_;
	ros::Time last_timestamp_;
	ros::WallTimer wall_timer_;
	ros::Subscriber submap_list_subscriber_;
	ros::Publisher submap_images_publisher_;
	ros::ServiceClient submap_query_client_ ,shutdown_carto_client_;
	std::vector<ros::ServiceServer> servers_;
	std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry> submap_images_;
	std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> submap_slices_;
	double display_ratio_;
  double save_image_resolution_;
  std::shared_ptr<uwbPoseOptimization> uwb_;
};
}
#endif // MAPMAPPING3D_H
