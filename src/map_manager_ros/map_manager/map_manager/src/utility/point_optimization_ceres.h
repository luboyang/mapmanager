#ifndef POINTOPTIMIZATIONCERES_H
#define POINTOPTIMIZATIONCERES_H

#include <ceres/autodiff_cost_function.h>
#include <ceres/ceres.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
using ceres::Problem;
using ceres::Solve;
using ceres::Solver;
using namespace Eigen;
struct Pose3d
{
  Eigen::Vector3d p;
  Eigen::Quaterniond q;
  double covariance;

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
class pointConstraint2D
{
  public:
  pointConstraint2D(const double &x, const double &y, const double &angle, const double &info)
      : x_(x), y_(y), angle_(angle), info_(info)
  {
  }
  template <typename T>
  bool operator()(const T *const x_opt, const T *const y_opt, const T *const angle_opt,
                  T *residuals_ptr) const
  {
    residuals_ptr[0] = T(info_) * (*x_opt - T(x_));
    residuals_ptr[1] = T(info_) * (*y_opt - T(y_));
    residuals_ptr[2] = T(info_) * (*angle_opt - T(angle_));
    return true;
  }

  static ceres::CostFunction *Create(const double &x, const double &y, const double &angle,
                                     const double &info)
  {
    return new ceres::AutoDiffCostFunction<pointConstraint2D, 3, 1, 1, 1>(
        new pointConstraint2D(x, y, angle, info));
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  private:
  // The measurement for the position of B relative to A in the A frame.
  const double x_;
  const double y_;
  const double angle_;
  const double info_;
};

class costFunc2D
{
  public:
  costFunc2D(const double &x1, const double &y1, const double &x2, const double &y2)
      : x1_(x1), y1_(y1), x2_(x2), y2_(y2)
  {
  }
  template <typename T>
  bool operator()(const T *const x_opt, const T *const y_opt, const T *const angle_opt,
                  T *residuals_ptr) const
  {
    T sina = T(sin(*angle_opt));
    T cosa = T(cos(*angle_opt));
    residuals_ptr[0] = cosa * T(x2_) - sina * T(y2_) + (*x_opt) - T(x1_);
    residuals_ptr[1] = sina * T(x2_) + cosa * T(y2_) + (*y_opt) - T(y1_);
    return true;
  }

  static ceres::CostFunction *Create(const double &x1, const double &y1, const double &x2,
                                     const double &y2)
  {
    return new ceres::AutoDiffCostFunction<costFunc2D, 2, 1, 1, 1>(new costFunc2D(x1, y1, x2, y2));
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  private:
  const double x1_;
  const double y1_;
  const double x2_;
  const double y2_;
};

// class PoseGraph2dErrorTerm {
//  public:
//   PoseGraph2dErrorTerm(double x_ab, double y_ab, double yaw_ab_radians,
//                        const double& info)
//       : p_ab_(x_ab, y_ab),
//         yaw_ab_radians_(yaw_ab_radians),
//         info_(info) {}
//
//   template <typename T>
//   bool operator()(const T* const x_a, const T* const y_a, const T* const
//   yaw_a,
//                   T* residuals_ptr) const {
//     const Eigen::Matrix<T, 2, 1> p_a(*x_a, *y_a);
//
//     Eigen::Map<Eigen::Matrix<T, 3, 1> > residuals_map(residuals_ptr);
//     residuals_map(0) = info_ *
//     residuals_map.template head<2>() =
//         RotationMatrix2D(*yaw_a).transpose() * (p_b - p_a) -
//         p_ab_.cast<T>();
//     residuals_map(2) = NormalizeAngle((*yaw_b - *yaw_a) -
//     static_cast<T>(yaw_ab_radians_));
//
//     // Scale the residuals by the square root information matrix to account
//     for
//     // the measurement uncertainty.
//     residuals_map = info_.template cast<T>() * residuals_map;
//
//     return true;
//   }
//
//   static ceres::CostFunction* Create(double x_ab, double y_ab,
//                                      double yaw_ab_radians,
//                                      const Eigen::Matrix3d& sqrt_information)
//                                      {
//     return (new ceres::AutoDiffCostFunction<PoseGraph2dErrorTerm, 3, 1, 1, 1,
//     1,
//                                             1, 1>(new PoseGraph2dErrorTerm(
//         x_ab, y_ab, yaw_ab_radians, sqrt_information)));
//   }
//
//   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
//
//  private:
//   // The position of B relative to A in the A frame.
//   const Eigen::Vector2d p_ab_;
//   // The orientation of frame B relative to frame A.
//   const double yaw_ab_radians_;
//   // The inverse square root of the measurement covariance matrix.
//   const Eigen::Matrix3d info_;
// };

#endif // POINTOPTIMIZATIONCERES_H