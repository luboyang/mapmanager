#ifndef MAP_MANAGER_ULITITY_H_
#define MAP_MANAGER_ULITITY_H_
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/transform.h>
#include <pcl/common/common.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <Eigen/Eigen>
#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <cartographer/transform/transform.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer_ros_msgs/SubmapImageEntry.h>
#include <cv_bridge/cv_bridge.h>
namespace map_manager{
struct LandmarkInfo{
  std::string ns;
  std::string id;
  int visible;
  cartographer::transform::Rigid3d pose;
  Eigen::Vector2d translation_in_image;
  float translation_weight;
  float rotation_weight;
  float pole_radius;
};
struct ProbabilityGridInfo {
  int id;
  cartographer::transform::Rigid3d global_pose;
  cartographer::transform::Rigid3d local_pose;
  int num_range_data;
  bool finished;
  double resolution;
  double max[2];
  int num_cells[2];
};
struct PbstreamInfos {
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::transform::Rigid3d>
      node_poses;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::TrajectoryNode::Data>
      node_datas;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                 cartographer::transform::Rigid3d>
      submap_poses;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                 cartographer::mapping::proto::Submap>
      submap_id_to_submap;
  std::map<std::string, cartographer::transform::Rigid3d> landmark_poses;
  std::vector<cartographer::sensor::OdometryData> odom_datas;
  std::vector<cartographer::sensor::LandmarkData> landmark_datas;
  std::vector<cartographer::mapping::PoseGraphInterface::Constraint>
      constraints;
};

void readPosesFromPbstream(const std::string& path, PbstreamInfos& infos);
bool judgeFileExist(const std::string& file_name);
cv::Mat image16ToImage8(const cv::Mat& image16);

cv::Mat image8ToImage16(const cv::Mat& image8);

void checkSystemStatus(const std::string& cmdstring, const int& status);
void image2worldLoc(const Eigen::Vector2d& max, const double& resolution,
                    const Eigen::Vector2i& imageLoc, Eigen::Vector2d& worldLoc);

void world2imageLoc(const Eigen::Vector2d& max, const double& resolution,
                    const Eigen::Vector2d& worldLoc, Eigen::Vector2i& imageLoc);

void autoSetIgnoreArea(const std::string& path);

void updateBoolImage(const std::string& origin_path, const std::string& path);

void updateModifyArea(const cv::Mat& modify_area_img, const std::string& path);

void updateModifyMask(const std::string& origin_path, const std::string& path);

std::vector<LandmarkInfo> loadLandmarksFromXmlFile(const std::string& path);
void bash(std::string cmd);
std::string adjustMapPixelSingle(std::string pngDirPath, int size,
                                 const bool is_saved_map);
std::string adjustMapPixel(const std::string& file_path, const int& size,
                           const bool is_saved_map);
bool dbscan(const pcl::PointCloud<pcl::PointXY>::Ptr& cloud_in, 
            std::vector<std::vector<int>> &clusters_index, 
            const double& r, const int& size);
void loadOldSubmap(const std::string& submap_filepath, cartographer_ros_msgs::SubmapImageEntry &submap_img,
std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> &localInGlobal_poses_,
std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> &posesInGlobal_,
std::vector<double> &maxes_,int size_of_map_[2]);
}

#endif  // MAP_MANAGER_ULITITY_H_