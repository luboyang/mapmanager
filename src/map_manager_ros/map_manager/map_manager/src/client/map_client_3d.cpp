/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "map_client_3d.h"

#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_eigen/tf2_eigen.h>

#include <opencv2/highgui.hpp>

#include "nav_msgs/Odometry.h"
#include "tf2_sensor_msgs/tf2_sensor_msgs.h"
#include "vtr_msgs/SetInitialPose.h"
using namespace std;
using namespace cv;
using namespace cartographer;
using namespace cartographer::mapping;

namespace map_manager
{
double resolution_ = 0.1;
int MapClient3D::mouse_status_ = 0;
Point MapClient3D::start_p_ = Point(0, 0);
Point MapClient3D::end_p_ = Point(0, 0);

MapClient3D::MapClient3D() : tfListener_(tfBuffer_), mode_(1)
{
  // 	string pbstream_path = "/home/cyy/map/test212/map.pbstream";
  // 	loadDataFromPbstream(pbstream_path);
  namedWindow("map");
  setMouseCallback("map", MapClient3D::onMouse, 0);
  wall_timer_ = nh_.createWallTimer(::ros::WallDuration(0.2), &MapClient3D::displayMap, this);

  point_cloud_sub_ = nh_.subscribe("scan_matched_points2", 3, &MapClient3D::handlePointCloud, this);
  ros::param::get("/map_manager_3d/resolution", resolution_);
  LOG(INFO) << "3d client resolution: " << resolution_;
  ros::Rate rate(1000);

  if (!::ros::param::get("/vtr/mode", mode_))
  {
    LOG(INFO) << "use default mode: 1";
    // rate.sleep();
  }

  if (mode_ == 1)
  {
    LOG(INFO) << "localization!";
    debug_point_cloud_sub_ = nh_.subscribe("/lidar_localiser/merge_point_cloud", 3,
                                           &MapClient3D::handleSubmapPointCloud, this);
    // 	pub_ = nh_.advertise<sensor_msgs::PointCloud>("/tmp_point_cloud",10);
    initial_pose_client_ =
        nh_.serviceClient<vtr_msgs::SetInitialPose>("/map_manager_localization_3d/initial_pose");
    reference_client_ = nh_.serviceClient<cartographer_ros_msgs::ReferenceLocationFromMap>(
        "/map_manager_localization_3d/reference_location_server");
    load_map_client_ = nh_.serviceClient<cartographer_ros_msgs::SubmapImagesServer>(
        "/map_manager_localization_3d/load_submaps");
    load_submaps_client_ = nh_.serviceClient<cartographer_ros_msgs::SubmapImagesServer>(
        "/map_manager_localization_3d/load_little_submaps");
    lidar_tf_pub_ =
        nh_.advertise<nav_msgs::Odometry>("/map_manager_localization_3d/lidar_pose", 3, true);
    // 		localization_request_pub_ =
    // nh_.advertise<std_msgs::Empty>("/vtr/submap3D_localization", 1);

    localization_point_cloud_pub_ = nh_.advertise<sensor_msgs::PointCloud>(
        "/map_manager_localization_3d/localization_point_cloud", 1);

    // 		string map_name = "bjsn_424";
    string path;
    if (!::ros::param::get("/vtr/path", path))
    {
      LOG(INFO) << "can not get path";
      exit(0);
    }
    // 		localization_3d_ = new MapLocalization3D(n);
    // 		localization_3d_->saveToPbstream(map_name);

    // 		localization_3d_->setResolution(resolution_);
    // 		localization_3d_->getShowImage(map_name,show_map_,max_,false);
    cartographer_ros_msgs::SubmapImagesServer load_map_srv, load_submaps_srv;
    ;
    // 		getPath(map_name,load_map_srv.request.map_path);
    load_map_srv.request.map_path = path + "/";
    if (!load_map_client_.call(load_map_srv)) LOG(ERROR) << "Get map failed!";
    if (load_map_srv.response.submap_images.size() == 0)
    {
      LOG(ERROR) << "Load map failed!";
      exit(0);
    }
    const cartographer_ros_msgs::SubmapImageEntry entry = load_map_srv.response.submap_images[0];

    Mat tmp_map = cv_bridge::toCvCopy(entry.image)->image;
    if (tmp_map.type() != CV_8UC1)
      cvtColor(tmp_map, show_map_, CV_BGR2GRAY);
    else
      show_map_ = tmp_map;

    max_[0] = entry.pose_in_map.position.x / resolution_;
    max_[1] = entry.pose_in_map.position.y / resolution_;
    LOG(INFO) << "global max_box: " << max_[0] << ", " << max_[1];

    load_submaps_srv.request.map_path = load_map_srv.request.map_path;
    if (!load_submaps_client_.call(load_submaps_srv)) LOG(WARNING) << "Get submaps failed!";

    int submap_size = load_submaps_srv.response.submap_images.size() / 2;
    for (int i = 0; i < submap_size; i++)
    {
      Mat img_tmp;
      double max_box[2];
      cartographer::transform::Rigid3d global_pose;
      cartographer_ros_msgs::SubmapImageEntry entry =
          load_submaps_srv.response.submap_images[2 * i];
      cartographer_ros_msgs::SubmapImageEntry entry_box =
          load_submaps_srv.response.submap_images[2 * i + 1];
      img_tmp = cv_bridge::toCvCopy(entry.image)->image;
      global_pose = ToRigid3d(entry.pose_in_map);
      max_box[0] = entry_box.pose_in_map.position.x / resolution_;
      max_box[1] = entry_box.pose_in_map.position.y / resolution_;
      submap_imgs_.push_back(img_tmp);
      submap_max_boxes_.push_back(max_box[0]);
      submap_max_boxes_.push_back(max_box[1]);
      submap_global_poses_.push_back(global_pose);
      LOG(INFO) << "submap_id: " << i << "max_box: " << max_box[0] << ", " << max_box[1];
    }
  }
  else
  {
    LOG(INFO) << "Mapping!";
    submap_images_sub_ = nh_.subscribe("/map_manager_mapping_3d/submap_images", 1,
                                       &MapClient3D::handleSubmapImages, this);
  }
}

MapClient3D::~MapClient3D()
{
  // 	delete localization_3d_;
  // 	localization_3d_ = NULL;
  // 	delete mapping_3d_;
  // 	mapping_3d_ = NULL;
}

void MapClient3D::handlePointCloud(const sensor_msgs::PointCloud2Ptr msg)
{
  bool lookup_tf_success = true;
  mutex_.lock();
  geometry_msgs::TransformStamped base2global_stamped, odom2global_stamped, tmp;
  try
  {
    base2global_stamped = tfBuffer_.lookupTransform("map", "base_footprint", ros::Time(0));
  }
  catch (tf2::TransformException &ex)
  {
    LOG_EVERY_N(WARNING, 10) << "base2map_stamped: " << ex.what();
    lookup_tf_success = false;
    // return;
  }

  try
  {
    odom2global_stamped = tfBuffer_.lookupTransform("map", "odom", ros::Time(0));
  }
  catch (tf2::TransformException &ex)
  {
    LOG_EVERY_N(WARNING, 10) << "odom2map_stamped: " << ex.what();
    lookup_tf_success = false;
    // return;
  }

  if (lookup_tf_success)
  {
    odom2global_ = Eigen::Affine3d::Identity(); /*tf2::transformToEigen(odom2global_stamped);*/
    base2global_ =
        tf2::transformToEigen(base2global_stamped); // tf2::transformToEigen(base2global_stamped);
    sensor_msgs::PointCloud2 origin_cloud, point_cloud;
    origin_cloud = (*msg);
    // 		point_cloud = origin_cloud;

    // 		tmp = tf2::eigenToTransform(ToEigen(global_tmp_));
    if (mode_ == 1)
      tf2::doTransform(origin_cloud, point_cloud, odom2global_stamped);
    else
      point_cloud = origin_cloud;
    int range_size = point_cloud.width * point_cloud.height;
    if (range_size > 0)
    {
      vector<Eigen::Vector3d>().swap(scan_points_);
      sensor_msgs::PointCloud2Iterator<float> iter_x(point_cloud, "x");
      sensor_msgs::PointCloud2Iterator<float> iter_y(point_cloud, "y");
      sensor_msgs::PointCloud2Iterator<float> iter_z(point_cloud, "z");
      // 			sensor_msgs::PointCloud points;
      // 			points.header = msg->header;
      for (int i = 0; i < range_size; i++)
      {
        Eigen::Vector3d point;
        point(0) = *iter_x;
        point(1) = *iter_y;
        point(2) = *iter_z;
        ++iter_x;
        ++iter_y;
        ++iter_z;
        // 				geometry_msgs::Point32 p;
        // 				p.x = point(0);p.y = point(1);p.z =
        // point(2); 				points.points.push_back(p);

        scan_points_.push_back(point);
      }
      //       pub_.publish(points);
    }
  }
  mutex_.unlock();
}

void MapClient3D::handleSubmapPointCloud(const cartographer_ros_msgs::MergePointCloudPtr &msg)
{
  unsigned int id = msg->submap_id;
  // 	LOG(INFO) << "submap_id: " << id ;
  if (id >= submap_imgs_.size())
  {
    LOG(INFO) << "submap_id: " << id << "\tsubmap_imgs_.size(): " << submap_imgs_.size();
  }
  CHECK(id < submap_imgs_.size());
  Mat img_tmp = submap_imgs_[id];
  double max_box[2] = {submap_max_boxes_[id * 2], submap_max_boxes_[id * 2 + 1]};
  sensor_msgs::PointCloud high_point_cloud_in_node = msg->high_resolution_points;
  sensor_msgs::PointCloud low_point_cloud_in_node = msg->low_resolution_points;
  // 	sensor_msgs::PointCloud high_point_cloud_in_submap_initial,
  // low_point_cloud_in_submap_initial; 	sensor_msgs::PointCloud
  // high_point_cloud_in_submap_final, low_point_cloud_in_submap_final;
  Eigen::Isometry3d initial_pose = ToIsometry(msg->node2submap_initial_pose);
  Eigen::Isometry3d final_pose = ToIsometry(msg->node2submap_final_pose);

  Eigen::Isometry3d relative_pose = final_pose.inverse() * initial_pose;

  Eigen::Isometry3d global_pose = ToEigenIsometry3d(submap_global_poses_[id]);
  sensor_msgs::PointCloud show_points, tmp, tmp_pub_points, pub_points;
  Mat color_map;

  // 	LOG(INFO) << "key: " << key_value_;
  if (img_tmp.type() == CV_8UC1)
    cvtColor(img_tmp, color_map, CV_GRAY2BGR);
  else if (img_tmp.type() == CV_8UC4)
    cvtColor(img_tmp, color_map, CV_BGRA2BGR);
  else
    color_map = img_tmp;
  // 	cvtColor(img_tmp, color_map, CV_GRAY2BGR);
  Vec3b color;
  if (key_value_ == 51)
  {
    doTransformPointCloud(high_point_cloud_in_node, tmp, initial_pose);
    color = Vec3b(0, 255, 0);
  }
  else if (key_value_ == 52)
  {
    doTransformPointCloud(high_point_cloud_in_node, tmp, final_pose);
    color = Vec3b(0, 0, 255);
  }
  else if (key_value_ == 50)
  {
    doTransformPointCloud(low_point_cloud_in_node, tmp, final_pose);
    color = Vec3b(255, 0, 0);
  }
  else
  {
    // 		doTransformPointCloud( low_point_cloud_in_node, tmp,
    // initial_pose); 		color = Vec3b(255, 0, 255);
    doTransformPointCloud(high_point_cloud_in_node, tmp, final_pose);
    color = Vec3b(0, 0, 255);
  }
  doTransformPointCloud(tmp, show_points, global_pose);
  for (auto &p : show_points.points)
  {
    // 		if(p.z < -0.5)
    // 			continue;
    int x, y;
    y = max_box[0] + (-p.x) / resolution_;
    x = max_box[1] + (-p.y) / resolution_;
    if (x >= 0 && x < color_map.cols && y >= 0 && y < color_map.rows)
    {
      color_map.at<Vec3b>(y, x) = color;
    }
  }
  if (fabs(relative_pose.translation()[1]) > 0.15)
  {
    imwrite(std::string(std::getenv("HOME")) + "/final_pose.png", color_map);
    sensor_msgs::PointCloud tmp1, tmp2;
    doTransformPointCloud(high_point_cloud_in_node, tmp1, initial_pose);
    doTransformPointCloud(tmp1, tmp2, global_pose);
    color = Vec3b(255, 0, 0);
    Mat color_map1;
    if (img_tmp.type() == CV_8UC1)
      cvtColor(img_tmp, color_map1, CV_GRAY2BGR);
    else if (img_tmp.type() == CV_8UC4)
      cvtColor(img_tmp, color_map1, CV_BGRA2BGR);
    else
      color_map1 = img_tmp;

    for (auto &p : tmp2.points)
    {
      int x, y;
      y = max_box[0] + (-p.x) / resolution_;
      x = max_box[1] + (-p.y) / resolution_;
      if (x >= 0 && x < color_map1.cols && y >= 0 && y < color_map1.rows)
      {
        color_map1.at<Vec3b>(y, x) = color;
      }
    }
    imwrite(std::string(std::getenv("HOME")) + "/initial_pose.png", color_map1);
  }
  doTransformPointCloud(high_point_cloud_in_node, tmp_pub_points, initial_pose);
  doTransformPointCloud(tmp_pub_points, pub_points, global_pose);
  LOG(INFO) << "submap_id:" << id << "  final_pose:" << final_pose.matrix()
            << "  global_pose:" << global_pose.matrix();
  sensor_msgs::PointCloud filtered_pub_points;
  filtered_pub_points.header.frame_id = "map";
  pub_points.header.frame_id = "map";
  sensor_msgs::ChannelFloat32 tmp_id_channel;
  tmp_id_channel.values.push_back(id);
  pub_points.channels.push_back(tmp_id_channel);
  for (auto point : pub_points.points)
  {
    if (point.z > 3.0 || point.z < 0.5) continue;
    filtered_pub_points.points.push_back(point);
  }
  localization_point_cloud_pub_.publish(pub_points);
  // 	submap_point_cloud2_pub_.publish(localization_3d_->getPointCloud2(id));
  putText(color_map, "submap_id: " + to_string(id), Point(20, 10), FONT_HERSHEY_SIMPLEX, 0.5,
          Scalar(0, 255, 255), 1);
  putText(color_map, "1: low-init(default)", Point(20, 30), FONT_HERSHEY_SIMPLEX, 0.5,
          Scalar(255, 0, 255), 1);
  putText(color_map, "2: low-final", Point(20, 50), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0),
          1);
  putText(color_map, "3: high-init", Point(20, 70), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0),
          1);
  putText(color_map, "4: high-final", Point(20, 90), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255),
          1);
  imshow("submap", color_map);
  int key_tmp = waitKey(2);
  if (key_tmp != -1) key_value_ = key_tmp;
}

void MapClient3D::doTransformPointCloud(const sensor_msgs::PointCloud &origin_point_cloud,
                                        sensor_msgs::PointCloud &final_point_cloud,
                                        const Eigen::Isometry3d &transform)
{
  for (auto point : origin_point_cloud.points)
  {
    Eigen::Vector3d p(point.x, point.y, point.z);
    Eigen::Vector3d p2 = transform * p;
    geometry_msgs::Point32 final_point;
    final_point.x = p2(0);
    final_point.y = p2(1);
    final_point.z = p2(2);
    final_point_cloud.points.push_back(final_point);
  }
}

void MapClient3D::displayMap(const ros::WallTimerEvent &unused_timer_event)
{
  // 	if(pub_point_cloud2_cnt_ > 50)
  // 	{
  // 		pub_point_cloud2_cnt_ = 0;
  // 		submap_point_cloud2_pub_.publish(mapping_3d_->getPointCloud2(0));
  // 	}
  // 	pub_point_cloud2_cnt_ ++;
  if (show_map_.empty()) return;
  cv::Mat color_map;
  if (mode_ == 1)
    cvtColor(show_map_, color_map, CV_GRAY2BGR);
  else
    color_map = show_map_.clone();
  // 	cvtColor(show_map_, color_map, CV_GRAY2BGR);
  // 	color_map = show_map_.clone();

  Point p1, p2;
  p1.y = max_[0] + (-base2global_.translation()[0]) / resolution_;
  p1.x = max_[1] + (-base2global_.translation()[1]) / resolution_;
  double yaw = GetYaw(Eigen::Quaterniond(base2global_.rotation()));

  Point2d world_dir;
  world_dir.y = base2global_.translation()[0] + cos(yaw) * 10 * resolution_;
  world_dir.x = base2global_.translation()[1] + sin(yaw) * 10 * resolution_;
  p2.x = max_[1] + (-world_dir.x) / resolution_;
  p2.y = max_[0] + (-world_dir.y) / resolution_;
  arrowedLine(color_map, p1, p2, Scalar(255, 0, 0), 1, 8);

  if (scan_points_.size() == 0)
  {
    imshow("map", color_map);
    cv::waitKey(2);
    return;
  }

  for (auto &point : scan_points_)
  {
    int x, y;
    x = max_[1] + (-point(1)) / resolution_;
    y = max_[0] + (-point(0)) / resolution_;
    if (x >= 0 && x < color_map.cols && y >= 0 && y < color_map.rows)
      color_map.at<Vec3b>(y, x) = Vec3b(0, 0, 255);
  }

  handleMouse(color_map);

  // 	mutex_.unlock();
  imshow("map", color_map);
  waitKey(2);

  if (mode_ == 1)
  {
    geometry_msgs::TransformStamped base2map_stamped;
    try
    {
      base2map_stamped = tfBuffer_.lookupTransform("map", "base_footprint", ros::Time(0));
    }
    catch (tf2::TransformException &ex)
    {
      LOG_EVERY_N(WARNING, 10) << "base2map_stamped(dispaly): " << ex.what();
      return;
    }

    nav_msgs::Odometry odometry_msgs;
    Eigen::Isometry3d base2map_eigen;

    tf::transformMsgToEigen(base2map_stamped.transform, base2map_eigen);
    tf::poseEigenToMsg(base2map_eigen, odometry_msgs.pose.pose);

    odometry_msgs.header = base2map_stamped.header;
    odometry_msgs.child_frame_id = "lidar_base_footprint";

    lidar_tf_pub_.publish(odometry_msgs);
  }
  // 	if(key_tmp != -1)
  // 		key_value_ = key_tmp;
}

void MapClient3D::handleMouse(Mat &map)
{
  if (mouse_status_ == 1)
  {
    line(map, start_p_, end_p_, Scalar(255, 0, 0));
  }
  else if (mouse_status_ == 2)
  {
    //     cartographer_ros_msgs::FrontEndPoseInitialization
    //     pose_initialization_query;
    //     pose_initialization_query.request.use_currentPose = false;
    //     pose_initialization_query.request.imagePose.x =
    //     start_p_.x/DISPLAY_RATIO;
    //     pose_initialization_query.request.imagePose.y =
    //     start_p_.y/DISPLAY_RATIO;
    //     pose_initialization_query.request.imagePose.theta =
    //         atan2(end_p_.y - start_p_.y, end_p_.x - start_p_.x);
    mouse_status_ = 0;
    Eigen::Vector2d worldLoc, worldLoc_direction;
    Eigen::Vector2i imageLoc, imageLoc_direction;
    double theta, yaw;
    imageLoc[0] = start_p_.x;
    imageLoc[1] = start_p_.y;

    theta = atan2(end_p_.y - start_p_.y, end_p_.x - start_p_.x);
    imageLoc_direction[0] = imageLoc[0] + 100 * cos(theta);
    imageLoc_direction[1] = imageLoc[1] + 100 * sin(theta);
    LOG(INFO) << "imageLoc: " << imageLoc[0] << "," << imageLoc[1] << "," << theta
              << "\n imageLoc_direction: " << imageLoc_direction[0] << "," << imageLoc_direction[1];
    // 		LOG(INFO) << map_box_.max().y()*resolution << ", " <<
    // map_box_.max().x()*resolution;
    //     image2worldLoc(Eigen::Vector2d(max_[0]*resolution_ ,
    //     max_[1]*resolution_), resolution_, imageLoc, worldLoc);
    worldLoc[1] = (max_[1] - imageLoc[0]) * resolution_;
    worldLoc[0] = (max_[0] - imageLoc[1]) * resolution_;

    worldLoc_direction[1] = (max_[1] - imageLoc_direction[0]) * resolution_;
    worldLoc_direction[0] = (max_[0] - imageLoc_direction[1]) * resolution_;
    //     image2worldLoc(Eigen::Vector2d(max_[0]*resolution_ ,
    //     max_[1]*resolution_), resolution_, imageLoc_direction,
    //     worldLoc_direction);

    // 		image2worldLoc(Eigen::Vector2d(max_[0]*resolution_ ,
    // max_[1]*resolution_), resolution_,
    // 									 Eigen::Vector2i(imageLoc[1],imageLoc[0]),
    // worldLoc);
    //     image2worldLoc(Eigen::Vector2d(max_[0]*resolution_ ,
    //     max_[1]*resolution_), resolution_,
    // 									 Eigen::Vector2i(imageLoc_direction[1],imageLoc_direction[0]),
    // worldLoc_direction);
    yaw = atan2(worldLoc_direction[1] - worldLoc[1], worldLoc_direction[0] - worldLoc[0]);
    cartographer_ros_msgs::ReferenceLocationFromMap srv;
    srv.request.mapPose.push_back(ToGeometryPoseMsg(worldLoc[0], worldLoc[1], yaw));
    LOG(INFO) << "worldLoc: " << worldLoc[0] << "," << worldLoc[1] << "," << yaw << endl
              << "worldLoc_direction: " << worldLoc_direction[0] << "," << worldLoc_direction[1];
    // 		geometry_msgs::Pose
    // targetPoseInWorld_pose,target2reference_pose; 		int nearestIdx = -1;
    if (reference_client_.call(srv))
    /*	if(localization_3d_->getReferenceLocationPoses(ToGeometryPoseMsg(worldLoc[0],worldLoc[1],yaw),
                            targetPoseInWorld_pose,target2reference_pose,nearestIdx))*/
    {
      LOG(INFO) << "call reference localization success! ";
      vtr_msgs::Pose pose_reference_current_msg;
      pose_reference_current_msg.header.stamp = ros::Time::now() - ros::Duration(0.1);

      int temp_id = srv.response.reference_id[0];
      geometry_msgs::Pose temp_pose = srv.response.referencePose[0];
      temp_pose.orientation.x = 0;
      temp_pose.orientation.y = 0; // row & pitch must be same as submap when
                                   // using manual localization.
      pose_reference_current_msg.reference_id = temp_id;
      pose_reference_current_msg.pose.pose = temp_pose;
      pose_reference_current_msg.pose_valid = true;
      // initial_pose_pub_.publish(pose_reference_current_msg);

      // 			pose_reference_current_msg.reference_id =
      // nearestIdx; 			pose_reference_current_msg.pose.pose =
      // target2reference_pose; 			pose_reference_current_msg.pose_valid = true;
      // initial_pose_pub_.publish(pose_reference_current_msg);

      vtr_msgs::SetInitialPose pose_initial_query;
      pose_initial_query.request.base2map = pose_reference_current_msg;
      pose_initial_query.request.base2map.pose_valid = true;
      if (initial_pose_client_.call(pose_initial_query))
      {
        // LOG(INFO) << "call initial pose server success! " ;
        if (!pose_initial_query.response.is_succeed)
        {
          LOG(WARNING) << "/vtr/initial_pose service can't init the pose with "
                          "the input values.";
        }
      }
      else
      {
        LOG(INFO) << "call initial pose server failed! ";
        LOG(WARNING) << "call /vtr/initial_pose service failed.";
      }
    }
    else
    {
      LOG(INFO) << "call reference localization failed! ";
    }
  }
}

void MapClient3D::onMouse(int event, int x, int y, int flag, void *)
{
  if (event == EVENT_LBUTTONDOWN)
  {
    start_p_ = Point(x, y);
    end_p_ = Point(x, y);
    mouse_status_ = 1;
  }
  else if (mouse_status_ == 1 && event == EVENT_MOUSEMOVE)
  {
    end_p_ = Point(x, y);
  }
  else if (event == EVENT_LBUTTONUP)
  {
    end_p_ = Point(x, y);
    mouse_status_ = 2;
  }
}

void MapClient3D::handleSubmapImages(const cartographer_ros_msgs::SubmapImagesConstPtr msg)
{
  if (msg->submap_images.size() == 0) return;
  //  mutex_.lock();
  map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(1e5, 1e5), Eigen::Vector2d(-1e5, -1e5));

  for (auto &it : msg->submap_images)
  {
    refineSubmapImage(it);
  }
  Mat base_img(map_box_.max().y() - map_box_.min().y(), map_box_.max().x() - map_box_.min().x(),
               CV_8UC3, Scalar(128, 128, 128));
  getGlobalMap(base_img);
  show_map_ = base_img.clone();
  max_[0] = map_box_.max().y();
  max_[1] = map_box_.max().x();

  //   LOG(INFO)  << "max: " << max_[0] << ", " <<max_[1] ;
  // 	mutex_.lock();
  // 	map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(1e5, 1e5),
  //                                         Eigen::Vector2d(-1e5, -1e5));
  // 	cartographer_ros_msgs::SubmapImageEntry entry = msg->submap_images[0];
  //
  // 	if(entry.image.data.size() == 0)
  // 	{
  // 		LOG(INFO) << "map is empty!" ;
  // 		return;
  // 	}
  // 	Mat tmp_map = cv_bridge::toCvCopy(entry.image)->image;
  // 	if(tmp_map.type()!=CV_8UC1)
  // 			cvtColor(tmp_map, show_map_, CV_BGR2GRAY);
  // 		else
  // 			show_map_ = tmp_map;
  // 	max_[0] = entry.pose_in_map.position.x / resolution_;
  // 	max_[1] = entry.pose_in_map.position.y / resolution_;

  // 	LOG(INFO) << "max: " << max_[0] << "," <<max_[1]  ;
  // 	LOG(INFO) << "size: " << show_map_.cols << "," <<show_map_.rows  ;
}
void MapClient3D::refineSubmapImage(const cartographer_ros_msgs::SubmapImageEntry &submap)
{
  //
  uint new_id = 10000 * submap.trajectory_id + submap.submap_index;
  map<uint, ClientSubmap>::iterator it_submap;
  it_submap = client_submaps_.find(new_id);
  //  if(new_id == 1)
  //  {
  //    LOG(INFO) << "pose in map: " << submap.pose_in_map ;
  //  }
  if (it_submap == client_submaps_.end())
  {
    ClientSubmap temp_submap;
    temp_submap.trajectory = submap.trajectory_id;
    temp_submap.index = submap.submap_index;
    temp_submap.version = submap.submap_version;
    temp_submap.resolution = resolution_;
    geometry_msgs::Pose tmp_pose;
    tmp_pose = submap.pose_in_map;
    //     tmp_pose.orientation.w =1;
    //     tmp_pose.orientation.z = 0;
    temp_submap.global_pose = ToIsometry(tmp_pose);
    temp_submap.image = cv_bridge::toCvCopy(submap.image)->image;
    Eigen::Isometry3d tmp;
    tmp.setIdentity();

    temp_submap.box_in_global = getBox(
        temp_submap.global_pose, Eigen::Vector2d(temp_submap.image.cols, temp_submap.image.rows),
        temp_submap.resolution);

    double yaw = GetYaw(submap.pose_in_map);
    cv::Point2f center(0, 0);
    cv::Mat rot_mat = cv::getRotationMatrix2D(center, yaw, 1.0);
    // imshow("22", in_img);
    Mat transformed_img(temp_submap.box_in_global.max().x() - temp_submap.box_in_global.min().x(),
                        temp_submap.box_in_global.max().y() - temp_submap.box_in_global.min().y(),
                        CV_8UC4, Scalar(128, 128, 128, 0));

    cv::warpAffine(temp_submap.image, transformed_img, rot_mat,
                   Size(temp_submap.box_in_global.max().x() - temp_submap.box_in_global.min().x(),
                        temp_submap.box_in_global.max().y() - temp_submap.box_in_global.min().y()));

    temp_submap.transformed_image = transformed_img;
    //      imshow("22",transformed_img);
    //      waitKey(0);
    client_submaps_[new_id] = temp_submap;
    updateMapBox(temp_submap.box_in_global);
  }
  else
  {
    geometry_msgs::Pose tmp_pose;
    tmp_pose = submap.pose_in_map;
    //     tmp_pose.orientation.w =1;
    //     tmp_pose.orientation.z = 0;
    it_submap->second.global_pose = ToIsometry(tmp_pose);
    if (it_submap->second.version != submap.submap_version || it_submap->second.version == 0)
    {
      it_submap->second.version = submap.submap_version;
      it_submap->second.image = cv_bridge::toCvCopy(submap.image)->image;
      it_submap->second.resolution = submap.resolution;
    }

    it_submap->second.box_in_global =
        getBox(it_submap->second.global_pose,
               Eigen::Vector2d(it_submap->second.image.cols, it_submap->second.image.rows),
               it_submap->second.resolution);
    Mat transformed_img(
        it_submap->second.box_in_global.max().x() - it_submap->second.box_in_global.min().x(),
        it_submap->second.box_in_global.max().y() - it_submap->second.box_in_global.min().y(),
        CV_8UC4, Scalar(128, 128, 128, 0));
    cv::Point2f center(0, 0);
    double yaw = GetYaw(submap.pose_in_map);
    cv::Mat rot_mat = cv::getRotationMatrix2D(center, yaw, 1.0);
    cv::warpAffine(
        it_submap->second.image, transformed_img, rot_mat,
        Size(it_submap->second.box_in_global.max().x() - it_submap->second.box_in_global.min().x(),
             it_submap->second.box_in_global.max().y() -
                 it_submap->second.box_in_global.min().y()));
    it_submap->second.transformed_image = transformed_img;
    updateMapBox(it_submap->second.box_in_global);
  }
}

void MapClient3D::getGlobalMap(cv::Mat &base_img)
{
  for (auto &it : client_submaps_)
  {
    ClientSubmap submap = it.second;
    Mat img1_t1(base_img, cvRect(map_box_.max().x() - submap.box_in_global.max().x(),
                                 map_box_.max().y() - submap.box_in_global.max().y(),
                                 submap.transformed_image.cols, submap.transformed_image.rows));
    mergeTransparentImgs(img1_t1, submap.transformed_image, 1.0);
  }
}
void MapClient3D::updateMapBox(const Eigen::AlignedBox2d &box)
{
  double min_x, min_y, max_x, max_y;
  min_x = MIN(map_box_.min().x(), box.min().x());
  min_y = MIN(map_box_.min().y(), box.min().y());
  max_x = MAX(map_box_.max().x(), box.max().x());
  max_y = MAX(map_box_.max().y(), box.max().y());
  map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(min_x, min_y), Eigen::Vector2d(max_x, max_y));
}
void MapClient3D::clear()
{
  wall_timer_.stop();
  point_cloud_sub_.shutdown();
  debug_point_cloud_sub_.shutdown();
  submap_images_sub_.shutdown();

  reference_client_.shutdown();
  initial_pose_client_.shutdown();
  load_map_client_.shutdown();
  load_submaps_client_.shutdown();

  lidar_tf_pub_.shutdown();
  localization_point_cloud_pub_.shutdown();
}
/*
int main(int argc, char** argv)
{
        google::InitGoogleLogging(argv[0]);
        google::SetStderrLogging(google::GLOG_INFO);
  FLAGS_colorlogtostderr =true;
// 	ScopedRosLogSink ros_log_sink;
  ros::init(argc, argv, "map_client_3d");
  ros::NodeHandle nh;

  MapClient3D client;
  ros::spin();
        google::ShutdownGoogleLogging();
  return 0;
}*/
} // namespace map_manager
