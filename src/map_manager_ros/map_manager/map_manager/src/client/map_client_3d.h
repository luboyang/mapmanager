/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPCLIENT3D_H
#define MAPCLIENT3D_H

#include <cartographer_ros_msgs/MergePointCloud.h>
#include <pcl_ros/filters/radius_outlier_removal.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>

#include "3d/map_localization_3d.h"

namespace map_manager
{
class MapClient3D : public MapInterface
{
  public:
  MapClient3D();
  ~MapClient3D();

  private:
  void handleSubmapImages(const cartographer_ros_msgs::SubmapImagesConstPtr msg);

  void handlePointCloud(const sensor_msgs::PointCloud2Ptr msg);
  void handleSubmapPointCloud(const cartographer_ros_msgs::MergePointCloudPtr &msg);
  void displayMap(const ::ros::WallTimerEvent &unused_timer_event);
  void doTransformPointCloud(const sensor_msgs::PointCloud &origin_point_cloud,
                             sensor_msgs::PointCloud &final_point_cloud,
                             const Eigen::Isometry3d &transform);
  void handleMouse(cv::Mat &map);

  // mapping
  void refineSubmapImage(const cartographer_ros_msgs::SubmapImageEntry &submap);
  void updateMapBox(const Eigen::AlignedBox2d &box);
  void getGlobalMap(cv::Mat &base_img);
  virtual void clear() override;
  struct ClientSubmap
  {
    int trajectory;
    int index;
    int version;
    double resolution;
    Eigen::Isometry3d global_pose;
    cv::Mat image;
    cv::Mat transformed_image;
    Eigen::AlignedBox2d box_in_global;
  };

  static void onMouse(int event, int x, int y, int flag, void *);
  ros::NodeHandle nh_;
  std::mutex mutex_;
  cv::Mat show_map_;
  int mode_;

  std::vector<Eigen::Vector3d> scan_points_;
  Eigen::Affine3d odom2global_, base2global_;
  ros::WallTimer wall_timer_;
  double max_[2];
  // 	double resolution_;
  ros::Subscriber point_cloud_sub_;
  ros::Subscriber debug_point_cloud_sub_;
  ros::ServiceClient reference_client_;
  ros::ServiceClient initial_pose_client_, load_map_client_, load_submaps_client_;

  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;
  // 	ros::Publisher pub_;

  static int mouse_status_;
  static cv::Point start_p_;
  static cv::Point end_p_;
  // 	MapLocalization3D* localization_3d_;

  std::vector<cv::Mat> submap_imgs_;
  std::vector<double> submap_max_boxes_;
  std::vector<cartographer::transform::Rigid3d> submap_global_poses_;
  int key_value_;
  ros::Publisher lidar_tf_pub_;
  ;
  ros::Publisher localization_point_cloud_pub_;
  int pub_point_cloud2_cnt_;

  // mapping
  ros::Subscriber submap_images_sub_;
  Eigen::AlignedBox2d map_box_;
  std::map<uint, ClientSubmap> client_submaps_;
};
} // namespace map_manager
#endif // MAPCLIENT3D_H
