/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "client/map_client.h"

#include <cartographer_ros_msgs/FrontEndMap.h>
#include <cartographer_ros_msgs/ReferenceLocationFromMap.h>
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <vtr_msgs/SetInitialPose.h>

#include "map_manager_ulitity.h"
using namespace cv;
using namespace std;

namespace map_manager
{
int MapClient::mouse_status_ = 0;
Point MapClient::start_p_ = Point(0, 0);
Point MapClient::end_p_ = Point(0, 0);

MapClient::MapClient() : tfListener_(tfBuffer_)
{
  inValid_cnt = 0;
  DISPLAY_RATIO = 1;
  ros::param::get("/map_client/display_ratio", DISPLAY_RATIO);
  //   localization_sub_ = nh_.subscribe(
  //         "/vtr/localization", 1,
  //         &MapClient::handleLocalization, this);
  //
  //   local_submap_sub_ =
  //       nh_.subscribe("/submap_local", 3,
  //                     &MapClient::handleLocalSubmap, this);
  landmark_list_sub_ = nh_.subscribe("/cartographer_front_end/landmark_pose_list", 5,
                                     &MapClient::handleLandmarkList, this);
  landmark_full_sub_ = nh_.subscribe("/cartographer_front_end/landmark_pose_full", 5,
                                     &MapClient::handleLandmarkFull, this);
  point_cloud_sub_ = nh_.subscribe("scan_matched_points2", 1, &MapClient::handlePointCloud, this);

  //   status_client_ =
  //       nh_.serviceClient<cartographer_ros_msgs::FrontEndStatusDisplay>(
  //         "/cartographer_front_end/statusDisplay");

  namedWindow("map");
  setMouseCallback("map", MapClient::onMouse, 0);
  initialized_ = false;
  int mode = 1;
  if (!ros::param::get("/map_client/mode", mode))
  {
    LOG(INFO) << "can not get mode, exit!";
    // 		exit(0);
  }
  if (mode == 1)
    status_ = LOCALIZATION;
  else
    status_ = MAPPING;
  if (status_ == LOCALIZATION)
  {
    LOG(INFO) << "LOCALIZATION";
    initial_pose_client_ =
        nh_.serviceClient<vtr_msgs::SetInitialPose>("/mark_localization/initial_pose");
    load_submaps_client_ = nh_.serviceClient<cartographer_ros_msgs::SubmapImagesServer>(
        "/map_manager_localization/load_submaps");
    reference_client_ = nh_.serviceClient<cartographer_ros_msgs::ReferenceLocationFromMap>(
        "/map_manager_localization/reference_location_server");
    string path; //= std::string(std::getenv("HOME")) + "/map/xu1/";
    if (!ros::param::get("/vtr/path", path))
    {
      LOG(ERROR) << "can not get path! exit!";
      exit(0);
    }
    path = path + "/";
    LOG(INFO) << "start loading map!";
    loadLocalizationMap(path);
    LOG(INFO) << "load map success!";
    // 		wall_timer_ = nh_.createWallTimer(::ros::WallDuration(0.2),
    //                           &MapClient::displayLocalizationMap, this);
  }
  else
  {
    LOG(INFO) << "MAPPING";
    submap_images_sub_ = nh_.subscribe("/map_manager_mapping/submap_images", 1,
                                       &MapClient::handleSubmapImages, this);
  }
  wall_timer_ = nh_.createWallTimer(::ros::WallDuration(0.2), &MapClient::displayMap, this);
}

MapClient::~MapClient()
{
  google::ShutdownGoogleLogging();
  destroyWindow("map");
}

Eigen::Vector2i MapClient::worldPose2ImagePose(const Eigen::Vector3d &world_pose,
                                               const float &display_ratio)
{
  Eigen::Vector2i img_pose;
  float resolution = 0.05;
  Eigen::Vector3d box(map_box_.max().y(), map_box_.max().x(), 0);
  img_pose(1) = box[0] + (-world_pose(0)) / resolution;
  img_pose(0) = box[1] + (-world_pose(1)) / resolution;
  img_pose(1) *= display_ratio;
  img_pose(0) *= display_ratio;
  return img_pose;
}

std::vector<MapClient::PoseWithID> MapClient::getWorldNavPoints(const std::string &map_path)
{
  Json::Reader reader;
  Json::Value root;
  ifstream in(map_path + "info_WorldRefer.json", ios::binary);
  if (!in.is_open())
  {
    LOG(WARNING) << "Error opening file\n";
  }
  std::vector<PoseWithID> poses;
  if (reader.parse(in, root))
  {
    Json::Value nodes = root["nodes"];
    Json::Value laser = root["Laser"];

    for (auto node : nodes)
    {
      Json::Value world_pose = node["worldPose"];
      Json::Value world_pose_position = world_pose["position"];
      Json::Value world_pose_orientation = world_pose["orientation"];
      PoseWithID pose;
      pose.position(0) = world_pose_position["x"].asDouble();
      pose.position(1) = world_pose_position["y"].asDouble();
      pose.position(2) = world_pose_position["z"].asDouble();

      pose.orientation = Eigen::Quaterniond(
          world_pose_orientation["w"].asDouble(), world_pose_orientation["x"].asDouble(),
          world_pose_orientation["y"].asDouble(), world_pose_orientation["z"].asDouble());
      pose.id = node["iD"].asInt();
      poses.push_back(pose);
    }
    LOG(WARNING) << poses[0].position.transpose();
    LOG(WARNING) << map_box_.max().y() << "," << map_box_.max().x();
  }
  return poses;
}
void MapClient::loadLocalizationMap(const string &path)
{
  map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(1e5, 1e5), Eigen::Vector2d(-1e5, -1e5));
  cartographer_ros_msgs::SubmapImagesServer srv;
  srv.request.map_path = path;
  load_submaps_client_.call(srv);
  LOG(INFO) << "submap_img size : " << srv.response.submap_images.size();
  for (auto it : srv.response.submap_images)
  {
    refineSubmapImage(it);
  }
  Mat base_img(map_box_.max().y() - map_box_.min().y(), map_box_.max().x() - map_box_.min().x(),
               CV_8UC3, Scalar(128, 128, 128));
  getGlobalMap(base_img);
  if (judgeFileExist(path + "frames/0/edge_probability_grid.png"))
  {
    Mat edge_map = imread(path + "frames/0/edge_probability_grid.png", CV_LOAD_IMAGE_COLOR);

    threshold(edge_map, edge_map, 1, 255, CV_THRESH_BINARY_INV);

    for (int i = 0; i < base_img.rows; ++i)
    {
      // 获取行指针
      uchar *pRow1 = edge_map.ptr<uchar>(i);
      uchar *pRow = base_img.ptr<uchar>(i);
      for (int j = 0; j < base_img.cols; ++j)
      {
        if (pRow1[j * 3] == 255)
        {
          pRow[j * 3] = 255;
          pRow[j * 3 + 1] = 180;
          pRow[j * 3 + 2] = 180;
        }
      }
    }
  }
  if (judgeFileExist(path + "line_data.txt"))
  {
    ifstream fi(path + "line_data.txt");
    while (!fi.eof())
    {
      //使用eof()函数检测文件是否读结束
      char line[128];
      fi.getline(line, 128);

      string line_data = line;
      string temp = "pos:";
      int nPos = line_data.find("pos: ");

      if (nPos != -1)
      {
        string data = line_data.substr(5, line_data.length());
        float pose[2];
        sscanf(data.c_str(), "%f %f", &pose[0], &pose[1]);
        Eigen::Vector2i img_pose = worldPose2ImagePose(Eigen::Vector3d(pose[0], pose[1], 0), 1.0);
        Point p(img_pose(0), img_pose(1));
        circle(base_img, p, 5, CvScalar(0, 255, 0), -1);
      }
    }
  }
  global_map_ = base_img.clone();
  if (judgeFileExist(path + "info_WorldRefer.json") && 0)
  {
    try
    {
      for (auto nav_point : getWorldNavPoints(path))
      {
        Eigen::Vector2i img_pose = worldPose2ImagePose(nav_point.position, 1.0);
        Point p(img_pose(0), img_pose(1));
        if (p.x >= 0 && p.x < global_map_.cols && p.y >= 0 && p.y < global_map_.rows)
        {
          cv::circle(global_map_, p, 8, CvScalar(255, 190, 130), -1);
          std::string text = std::to_string(nav_point.id);
          int baseline = 0;
          Size text_size = getTextSize(text, FONT_HERSHEY_COMPLEX, 0.3, 2, &baseline);
          Point p1(p.x - text_size.height / 2, p.y + text_size.height / 2);
          cv::putText(global_map_, text, p1, cv::FONT_HERSHEY_COMPLEX, 0.35,
                      cv::Scalar(255, 0, 255));
        }
      }
    }
    catch (cv::Exception &e)
    {
      LOG(WARNING) << e.what();
    }
  }
  LOG(INFO) << "map_box : " << map_box_.max();
}

void MapClient::updateMapBox(const Eigen::AlignedBox2d &box)
{
  double min_x, min_y, max_x, max_y;
  min_x = MIN(map_box_.min().x(), box.min().x());
  min_y = MIN(map_box_.min().y(), box.min().y());
  max_x = MAX(map_box_.max().x(), box.max().x());
  max_y = MAX(map_box_.max().y(), box.max().y());
  map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(min_x, min_y), Eigen::Vector2d(max_x, max_y));
}

void MapClient::handleLandmarkFull(const visualization_msgs::MarkerArrayConstPtr &p_MarkerArray)
{
  static int cnt = 0;
  if (p_MarkerArray->markers.size())
  {
    //     std::unique_lock<std::mutex> lck(landmark_full_mtx_);
    LOG(INFO) << "Recieve " << p_MarkerArray->markers.size() << " full landmarks.";
    landmark_full_ = *p_MarkerArray;
    if (cnt++ > 10)
    {
      cnt = 0;
      landmark_full_sub_.shutdown();
    }
  }
}

void MapClient::handleLandmarkList(const visualization_msgs::MarkerArrayConstPtr &p_MarkerArray)
{
  // 	std::unique_lock<std::mutex> lck(landmark_list_mtx_);
  landmark_list_ = *p_MarkerArray;
}

void MapClient::handleLocalization(const vtr_msgs::PosePtr pose_msg)
{
  // 	std::unique_lock<std::mutex> lk(inValid_cnt_mtx_);
  if (pose_msg->pose_valid)
    inValid_cnt = 0;
  else
    inValid_cnt++;
}

// localization
void MapClient::handleLocalSubmap(const cartographer_ros_msgs::SubmapsPtr submaps)
{
  const cartographer_ros_msgs::Submap submap = submaps->submap;
  uint num_x_cells = submap.num_x_cells;
  uint num_y_cells = submap.num_y_cells;
  const std::vector<uint16_t> &cells_vec = submap.cells;
  assert(num_y_cells * num_x_cells == cells_vec.size());
  cv::Mat cells_mat = cv::Mat::zeros(num_y_cells, num_x_cells, CV_16UC1);
  for (uint i = 0; i < num_y_cells; i++)
  {
    for (uint j = 0; j < num_x_cells; j++)
    {
      uint16_t value = cells_vec[i * num_x_cells + j];
      cells_mat.at<uint16_t>(i, j) = value;
    }
  }
}

void MapClient::handleSubmaps(const cartographer_ros_msgs::SubmapImagesPtr &msg)
{
  cartographer_ros_msgs::SubmapImages submaps = *msg;
  for (auto &entry : submaps.submap_images)
  {
    cartographer_ros_msgs::SubmapImageEntry submap = entry;
    Mat img = cv_bridge::toCvCopy(submap.image)->image;
    Eigen::Isometry3d pose = ToIsometry(submap.pose_in_map);
    uint fuse_id = 10000 * submap.trajectory_id + submap.submap_index;
    std::map<uint, ClientSubmap>::iterator it = client_submaps_.find(fuse_id);
    if (it != client_submaps_.end())
    {
      it->second.global_pose = pose;
      if (it->second.version != submap.submap_version)
      {
        img.copyTo(it->second.image);
        it->second.version = submap.submap_version;
      }
    }
    else
    {
      ClientSubmap temp;
      temp.trajectory = submap.trajectory_id;
      temp.index = submap.submap_index;
      temp.global_pose = pose;
      temp.version = submap.submap_version;
      temp.image = img;
      client_submaps_[fuse_id] = temp;
    }
  }
}

// common
void MapClient::handlePointCloud(const sensor_msgs::PointCloud2Ptr msg)
{
  bool lookup_tf_success = true;

  mutex_.lock();
  geometry_msgs::TransformStamped base2global_stamped, odom2global_stamped;
  try
  {
    base2global_stamped = tfBuffer_.lookupTransform("map", "base_footprint", ros::Time(0));
  }
  catch (tf2::TransformException &ex)
  {
    LOG_EVERY_N(WARNING, 10) << "base2odom_stamped: " << ex.what();
    lookup_tf_success = false;
    // return;
  }
  try
  {
    odom2global_stamped = tfBuffer_.lookupTransform("map", "odom", ros::Time(0));
  }
  catch (tf2::TransformException &ex)
  {
    LOG_EVERY_N(WARNING, 10) << "odom2global_stamped: " << ex.what();
    lookup_tf_success = false;
  }
  if (!initialized_) initialized_ = true;
  if (lookup_tf_success)
  {
    odom2global_ = tf2::transformToEigen(odom2global_stamped);
    base2global_ = tf2::transformToEigen(base2global_stamped);
    sensor_msgs::PointCloud2 origin_cloud, point_cloud;
    origin_cloud = (*msg);
    point_cloud = origin_cloud;
    if (status_ == LOCALIZATION) tf2::doTransform(origin_cloud, point_cloud, odom2global_stamped);
    int range_size = point_cloud.width * point_cloud.height;
    if (range_size > 0)
    {
      vector<Eigen::Vector2d>().swap(scan_points_);
      sensor_msgs::PointCloud2Iterator<float> iter_x(point_cloud, "x");
      sensor_msgs::PointCloud2Iterator<float> iter_y(point_cloud, "y");
      for (int i = 0; i < range_size; i++)
      {
        Eigen::Vector2d point;
        point(0) = *iter_x;
        point(1) = *iter_y;
        ++iter_x;
        ++iter_y;
        scan_points_.push_back(point);
      }
    }
  }
  mutex_.unlock();
}

void MapClient::refineSubmapImage(const cartographer_ros_msgs::SubmapImageEntry &submap)
{
  //
  uint new_id = 10000 * submap.trajectory_id + submap.submap_index;
  map<uint, ClientSubmap>::iterator it_submap;
  it_submap = client_submaps_.find(new_id);
  // 	if(new_id == 1)
  // 	{
  // 		LOG(INFO) << "pose in map: " << submap.pose_in_map ;
  // 	}
  //   LOG(INFO) << "id: (" <<submap.trajectory_id <<"," << submap.submap_index
  //   << ")\t" << submap.pose_in_map ;
  if (it_submap == client_submaps_.end())
  {
    ClientSubmap temp_submap;
    temp_submap.trajectory = submap.trajectory_id;
    temp_submap.index = submap.submap_index;
    temp_submap.version = submap.submap_version;
    temp_submap.resolution = submap.resolution;
    geometry_msgs::Pose tmp_pose;
    tmp_pose = submap.pose_in_map;
    // 		tmp_pose.orientation.w =1;
    // 		tmp_pose.orientation.z = 0;
    temp_submap.global_pose = ToIsometry(tmp_pose);
    temp_submap.image = cv_bridge::toCvCopy(submap.image)->image;
    Eigen::Isometry3d tmp;
    tmp.setIdentity();

    temp_submap.box_in_global = getBox(
        temp_submap.global_pose, Eigen::Vector2d(temp_submap.image.cols, temp_submap.image.rows),
        temp_submap.resolution);

    double yaw = GetYaw(submap.pose_in_map);
    // 		LOG(INFO) << "yaw: " <<yaw ;
    //     LOG(WARNING) << temp_submap.global_pose.matrix();
    float width_temp, height_temp;
    width_temp = temp_submap.box_in_global.max().x() - temp_submap.box_in_global.min().x();
    height_temp = temp_submap.box_in_global.max().y() - temp_submap.box_in_global.min().y();
    cv::Point2f center(temp_submap.image.cols / 2, temp_submap.image.rows / 2);
    cv::Mat rot_mat = cv::getRotationMatrix2D(center, yaw * 180 / M_PI, 1.0);
    rot_mat.at<double>(0, 2) += (width_temp - temp_submap.image.cols) / 2;
    rot_mat.at<double>(1, 2) += (height_temp - temp_submap.image.rows) / 2;
    // imshow("22", in_img);
    //     LOG(WARNING) << "origin image size: " << temp_submap.image.cols <<","
    //     <<temp_submap.image.rows; LOG(WARNING) << "new image size: " <<
    //     temp_submap.box_in_global.max().x() -
    //     temp_submap.box_in_global.min().x() <<","
    //     <<temp_submap.box_in_global.max().y() -
    //     temp_submap.box_in_global.min().y();
    Mat transformed_img(temp_submap.box_in_global.max().x() - temp_submap.box_in_global.min().x(),
                        temp_submap.box_in_global.max().y() - temp_submap.box_in_global.min().y(),
                        CV_8UC4, Scalar(128, 128, 128, 0));

    cv::warpAffine(temp_submap.image, transformed_img, rot_mat,
                   Size(temp_submap.box_in_global.max().x() - temp_submap.box_in_global.min().x(),
                        temp_submap.box_in_global.max().y() - temp_submap.box_in_global.min().y()));

    temp_submap.transformed_image = transformed_img;
    //     imshow("22",temp_submap.image);
    //     imshow("23",temp_submap.transformed_image);
    //     waitKey(2);
    client_submaps_[new_id] = temp_submap;
    updateMapBox(temp_submap.box_in_global);
    // 		it_submap->second.box_in_global = temp_submap.box_in_global;
  }
  else
  {
    geometry_msgs::Pose tmp_pose;
    tmp_pose = submap.pose_in_map;
    it_submap->second.global_pose = ToIsometry(tmp_pose);
    if (it_submap->second.version != submap.submap_version || it_submap->second.version == 0)
    {
      it_submap->second.version = submap.submap_version;
      it_submap->second.image = cv_bridge::toCvCopy(submap.image)->image;
      it_submap->second.resolution = submap.resolution;
    }

    it_submap->second.box_in_global =
        getBox(it_submap->second.global_pose,
               Eigen::Vector2d(it_submap->second.image.cols, it_submap->second.image.rows),
               it_submap->second.resolution);
    Mat transformed_img(
        it_submap->second.box_in_global.max().x() - it_submap->second.box_in_global.min().x(),
        it_submap->second.box_in_global.max().y() - it_submap->second.box_in_global.min().y(),
        CV_8UC4, Scalar(128, 128, 128, 0));
    cv::Point2f center(it_submap->second.image.cols / 2, it_submap->second.image.rows / 2);
    double yaw = GetYaw(submap.pose_in_map);

    float width_temp, height_temp;
    width_temp =
        it_submap->second.box_in_global.max().x() - it_submap->second.box_in_global.min().x();
    height_temp =
        it_submap->second.box_in_global.max().y() - it_submap->second.box_in_global.min().y();

    cv::Mat rot_mat = cv::getRotationMatrix2D(center, yaw * 180 / M_PI, 1.0);
    rot_mat.at<double>(0, 2) += (width_temp - it_submap->second.image.cols) / 2;
    rot_mat.at<double>(1, 2) += (height_temp - it_submap->second.image.rows) / 2;

    cv::warpAffine(
        it_submap->second.image, transformed_img, rot_mat,
        Size(it_submap->second.box_in_global.max().x() - it_submap->second.box_in_global.min().x(),
             it_submap->second.box_in_global.max().y() -
                 it_submap->second.box_in_global.min().y()));
    it_submap->second.transformed_image = transformed_img;
    updateMapBox(it_submap->second.box_in_global);
  }
}

// mapping
bool flag = true;
int iiiii = 0;
void MapClient::handleSubmapImages(const cartographer_ros_msgs::SubmapImagesConstPtr msg)
{
  if (msg->submap_images.size() == 0) return;
  // 	mutex_.lock();
  map_box_ = Eigen::AlignedBox2d(Eigen::Vector2d(1e5, 1e5), Eigen::Vector2d(-1e5, -1e5));

  for (auto &it : msg->submap_images)
  {
    refineSubmapImage(it);
  }
  // 	if(flag && client_submaps_.size() > 2){
  // 		flag = false;
  // 		LOG(INFO) <<"save images!" ;
  // 		for(auto& it:client_submaps_)
  // 		{
  // 			imwrite("/home/cyy/bag/"+to_string(iiiii++)+".png",it.second.image);
  // 			imwrite("/home/cyy/bag/transformed_image"+to_string(iiiii)+".png",it.second.transformed_image);
  // 		}
  // 	}
  Mat base_img(map_box_.max().y() - map_box_.min().y(), map_box_.max().x() - map_box_.min().x(),
               CV_8UC3, Scalar(128, 128, 128));
  getGlobalMap(base_img);
  global_map_ = base_img.clone();
  // 	mutex_.unlock();
  // 	Max_[0] =
}

double resolution;
// common
void MapClient::displayMap(const ros::WallTimerEvent &unused_timer_event)
{
  if (!initialized_) return;
  // 	mutex_.lock();
  double inv_resolution;
  cv::Mat color_map;
  resolution = 0.05;
  inv_resolution = 1 / resolution;
  //   int ret = -1;
  // map
  Mat map;
  if (global_map_.empty()) return;
  resize(global_map_, map,
         Size(global_map_.cols * DISPLAY_RATIO, global_map_.rows * DISPLAY_RATIO));
  // 	cv::Mat temp;
  // 	cv::cvtColor(map, temp,CV_GRAY2BGR);
  map.convertTo(color_map, CV_8U, 1, 0);
  // status
  Eigen::Vector3d Max_box;
  // 	odom2global_.setIdentity();
  if (status_ == LOCALIZATION)
  {
    Max_box = Eigen::Vector3d(map_box_.max().y(), map_box_.max().x(), 0);
  }
  else
    Max_box = /*odom2global_ **/ Eigen::Vector3d(map_box_.max().y(), map_box_.max().x(), 0);

  // 	LOG(INFO) << "map box: " << map_box_.max().y() << ","
  // <<map_box_.max().x() ;
  if (scan_points_.size() == 0) return;
  for (auto &point : scan_points_)
  {
    int x, y;
    y = Max_box(0) + (-point(0)) * inv_resolution;
    x = Max_box(1) + (-point(1)) * inv_resolution;
    y *= DISPLAY_RATIO;
    x *= DISPLAY_RATIO;
    if (x >= 0 && x < color_map.cols && y >= 0 && y < color_map.rows)
      color_map.at<Vec3b>(y, x) = Vec3b(0, 0, 255);
  }

  // merge wordLoc
  Point p1, p2;
  p1.y = map_box_.max().y() + (-base2global_.translation()[0]) * inv_resolution;
  p1.x = map_box_.max().x() + (-base2global_.translation()[1]) * inv_resolution;
  p1.y *= DISPLAY_RATIO;
  p1.x *= DISPLAY_RATIO;
  double yaw = GetYaw(Eigen::Quaterniond(base2global_.rotation()));

  Point2d world_dir;
  world_dir.x = base2global_.translation()[0] + cos(yaw) / DISPLAY_RATIO;
  world_dir.y = base2global_.translation()[1] + sin(yaw) / DISPLAY_RATIO;
  p2.y = map_box_.max().y() + (-world_dir.x) * inv_resolution;
  p2.x = map_box_.max().x() + (-world_dir.y) * inv_resolution;
  p2.y *= DISPLAY_RATIO;
  p2.x *= DISPLAY_RATIO;
  arrowedLine(color_map, p1, p2, Scalar(255, 0, 0), 1, 8);
  handleMouse(color_map);

  // 	mutex_.unlock();
  imshow("map", color_map);
  cv::waitKey(2);
}

void MapClient::handleMouse(Mat &map)
{
  if (mouse_status_ == 1)
  {
    line(map, start_p_, end_p_, Scalar(255, 0, 0));
  }
  else if (mouse_status_ == 2)
  {
    //     cartographer_ros_msgs::FrontEndPoseInitialization
    //     pose_initialization_query;
    //     pose_initialization_query.request.use_currentPose = false;
    //     pose_initialization_query.request.imagePose.x =
    //     start_p_.x/DISPLAY_RATIO;
    //     pose_initialization_query.request.imagePose.y =
    //     start_p_.y/DISPLAY_RATIO;
    //     pose_initialization_query.request.imagePose.theta =
    //         atan2(end_p_.y - start_p_.y, end_p_.x - start_p_.x);
    mouse_status_ = 0;
    Eigen::Vector2d worldLoc, worldLoc_direction;
    Eigen::Vector2i imageLoc, imageLoc_direction;
    double theta, yaw;
    imageLoc[0] = start_p_.x / DISPLAY_RATIO;
    imageLoc[1] = start_p_.y / DISPLAY_RATIO;

    theta = atan2(end_p_.y - start_p_.y, end_p_.x - start_p_.x);
    LOG(INFO) << imageLoc[0] << "," << imageLoc[1] << "," << theta;
    imageLoc_direction[0] = imageLoc[0] + 100 * cos(theta) + 0.5;
    imageLoc_direction[1] = imageLoc[1] + 100 * sin(theta) + 0.5;
    LOG(INFO) << map_box_.max().y() * resolution << ", " << map_box_.max().x() * resolution;
    image2worldLoc(
        Eigen::Vector2d(map_box_.max().y() * resolution, map_box_.max().x() * resolution),
        resolution, imageLoc, worldLoc);
    image2worldLoc(
        Eigen::Vector2d(map_box_.max().y() * resolution, map_box_.max().x() * resolution),
        resolution, imageLoc_direction, worldLoc_direction);

    yaw = atan2(worldLoc_direction[1] - worldLoc[1], worldLoc_direction[0] - worldLoc[0]);
    cartographer_ros_msgs::ReferenceLocationFromMap srv;
    srv.request.mapPose.push_back(ToGeometryPoseMsg(worldLoc[0], worldLoc[1], yaw));
    LOG(INFO) << worldLoc[0] << "," << worldLoc[1] << "," << yaw;
    if (reference_client_.call(srv))
    {
      LOG(INFO) << "call reference localization success! ";
      vtr_msgs::Pose pose_reference_current_msg;
      pose_reference_current_msg.header.stamp = ros::Time::now();
      int temp_id = srv.response.reference_id[0];
      geometry_msgs::Pose temp_pose = srv.response.referencePose[0];
      pose_reference_current_msg.reference_id = temp_id;
      pose_reference_current_msg.pose.pose = temp_pose;
      pose_reference_current_msg.pose_valid = true;
      // initial_pose_pub_.publish(pose_reference_current_msg);

      vtr_msgs::SetInitialPose pose_initial_query;
      pose_initial_query.request.base2map = pose_reference_current_msg;

      if (initial_pose_client_.call(pose_initial_query))
      {
        if (!pose_initial_query.response.is_succeed)
        {
          LOG(WARNING) << "/vtr/initial_pose service can't init the pose with "
                          "the input values.";
        }
      }
      else
      {
        LOG(WARNING) << "call /vtr/initial_pose service failed.";
      }
    }
    else
    {
      LOG(WARNING) << "call reference localization failed! ";
    }
  }
}

void MapClient::getGlobalMap(cv::Mat &base_img_)
{
  // 	LOG(INFO) << "base_img_ size1: " <<
  // map_box_.max().x()-map_box_.min().x() << "," <<
  // map_box_.max().y()-map_box_.min().y();
  //   int i = 0;
  for (auto &it : client_submaps_)
  {
    ClientSubmap submap = it.second;
    // 		LOG(INFO) << "submap.size: " << submap.transformed_image.cols << ","<<
    // submap.transformed_image.rows ; 		LOG(INFO) << "box.size: " <<
    // map_box_.max().x()-submap.box_in_global.max().x() << ","<<
    // map_box_.max().y()-submap.box_in_global.max().y();
    Mat img1_t1(base_img_, cvRect(map_box_.max().x() - submap.box_in_global.max().x(),
                                  map_box_.max().y() - submap.box_in_global.max().y(),
                                  submap.transformed_image.cols, submap.transformed_image.rows));
    // 		Mat img1_t1(base_img_, cvRect(submap.box_in_global.max().x() -
    // map_box_.min().x(), 																 submap.box_in_global.max().y() -
    // map_box_.min().y(), 																 submap.transformed_image.cols,
    // submap.transformed_image.rows));
    //     Mat img1_t1(base_img_, cvRect(submap.box_in_global.min().x(),
    //                                 submap.box_in_global.min().y(),
    //                                 submap.transformed_image.cols,
    //                                 submap.transformed_image.rows));

    //     if(i == 0)
    //     {

    //       imshow("map2",base_img_);
    //       imshow("transformed_image",submap.transformed_image);
    //       waitKey(2);
    //     }
    //     i++;
    mergeTransparentImgs(img1_t1, submap.transformed_image, 1.0);
  }
  // 	LOG(INFO) << "base_img_ size2: " <<
  // map_box_.max().x()-map_box_.min().x() << "," <<
  // map_box_.max().y()-map_box_.min().y(); 	imshow("map2",base_img_);
  // 	waitKey(2);
  // Mat img1_roi = base_img(Rect(0, 0, img1.cols, img1.rows));
}

// void MapClient::displayLocalizationMap(const ::ros::WallTimerEvent&
// unused_timer_event)
// {
// 	if(!initialized_)
// 		return;
//
//   double inv_resolution;
//   cv::Mat color_map;
// 	resolution = 0.05;
// 	inv_resolution = 1/resolution;
//   int ret = -1;
//   map
//   Mat map;
//   resize(global_map_, map, Size(global_map_.cols*DISPLAY_RATIO,
//   global_map_.rows*DISPLAY_RATIO));
// 	cv::Mat temp;
// 	cv::cvtColor(map, temp,CV_GRAY2BGR);
// 	map.convertTo(color_map,CV_8U,1,0);
//   status
//   for(auto& point:scan_points_)
// 	{
// 		int x,y;
// 		y = (map_box_.max().y()*resolution - point(0)) * inv_resolution +
// 0.5; 		x = (map_box_.max().x()*resolution - point(1)) * inv_resolution + 0.5;
//
// 		y *= DISPLAY_RATIO;
// 		x *= DISPLAY_RATIO;
//
// 		if (x >= 0 && x < color_map.cols && y >= 0 && y <
// color_map.rows) 			color_map.at<Vec3b>(y, x) = Vec3b(0, 0, 255);
// 	}
//
//   merge wordLoc
//   Point p1, p2;
//   p1.y = map_box_.max().y() + ( - base2global_.translation()[0]) *
//   inv_resolution + 0.5; p1.x = map_box_.max().x() + ( -
//   base2global_.translation()[1]) * inv_resolution + 0.5; p1.y *=
//   DISPLAY_RATIO; p1.x *= DISPLAY_RATIO; double yaw =
//   GetYaw(Eigen::Quaterniond(base2global_.rotation()));
//
//   Point2d world_dir;
//   world_dir.x = base2global_.translation()[0] +  cos(yaw) / DISPLAY_RATIO;
//   world_dir.y = base2global_.translation()[1] + sin(yaw) / DISPLAY_RATIO;
//   p2.y = map_box_.max().y() + ( - world_dir.x) * inv_resolution + 0.5;
//   p2.x = map_box_.max().x() + ( - world_dir.y) * inv_resolution + 0.5;
//   p2.y *= DISPLAY_RATIO;
//   p2.x *= DISPLAY_RATIO;
//   arrowedLine(color_map, p1, p2, Scalar(255, 0, 0), 1, 8);
// 	handleMouse(color_map);
//   imshow("map",color_map);
//   cv::waitKey(2);
// }

void MapClient::onMouse(int event, int x, int y, int flag, void *)
{
  if (event == EVENT_LBUTTONDOWN)
  {
    start_p_ = Point(x, y);
    end_p_ = Point(x, y);
    mouse_status_ = 1;
  }
  else if (mouse_status_ == 1 && event == EVENT_MOUSEMOVE)
  {
    end_p_ = Point(x, y);
  }
  else if (event == EVENT_LBUTTONUP)
  {
    end_p_ = Point(x, y);
    mouse_status_ = 2;
  }
}
void MapClient::clear()
{
  wall_timer_.stop();
  localization_sub_.shutdown();
  odom_sub_.shutdown();
  imu_sub_.shutdown();
  local_submap_sub_.shutdown();
  landmark_list_sub_.shutdown();
  landmark_full_sub_.shutdown();
  submap_imgs_sub_.shutdown();
  point_cloud_sub_.shutdown();
  submap_images_sub_.shutdown();
  tfBuffer_.clear();
  status_client_.shutdown();
  map_client_.shutdown();
  reference_client_.shutdown();
  load_submaps_client_.shutdown();
}
} // namespace map_manager
  // int main(int argc, char** argv)
  // {
  // 	google::InitGoogleLogging(argv[0]);
  // 	google::SetStderrLogging(google::GLOG_INFO);
  //   FLAGS_colorlogtostderr =true;
  //   ros::init(argc, argv, "map_client");
  //
  //   MapClient client;
  //   ros::spin();
  //   google::ShutdownGoogleLogging();
  //   return 0;
  // }