
#include "client/map_client.h"
#include "client/map_client_3d.h"

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  google::SetStderrLogging(google::GLOG_INFO);
  FLAGS_colorlogtostderr = true;
  //  ScopedRosLogSink ros_log_sink;
  ros::init(argc, argv, "map_client_node");
  ros::NodeHandle nh;
  bool use_3d;
  if (!::ros::param::get("/cartographer_node/is_3d", use_3d))
  {
    LOG(ERROR) << "Can not get /cartographer_node/is_3d! exit;";
    return 0;
  }
  std::unique_ptr<map_manager::MapInterface> map_client;
  if (use_3d)
    map_client.reset(new map_manager::MapClient3D);
  else
    map_client.reset(new map_manager::MapClient);
  ros::spin();
  google::ShutdownGoogleLogging();
  return 0;
}