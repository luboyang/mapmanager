/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPCLIENT_H
#define MAPCLIENT_H
#include <cartographer_ros_msgs/FrontEndPoseInitialization.h>
#include <cartographer_ros_msgs/SubmapImages.h>
#include <cartographer_ros_msgs/Submaps.h>
#include <cv_bridge/cv_bridge.h>
#include <glog/logging.h>
#include <jsoncpp/json/json.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <visualization_msgs/MarkerArray.h>
#include <vtr_msgs/Pose.h>

#include <Eigen/Geometry>
#include <mutex>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

#include "map_interface.h"

namespace map_manager
{

class MapClient : public MapInterface
{
  enum STATUS
  {
    MAPPING = 0,
    LOCALIZATION
  };
  struct ClientSubmap
  {
    int trajectory;
    int index;
    int version;
    double resolution;
    Eigen::Isometry3d global_pose;
    cv::Mat image;
    cv::Mat transformed_image;
    Eigen::AlignedBox2d box_in_global;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  struct PoseWithID
  {
    int id;
    Eigen::Vector3d position;
    Eigen::Quaterniond orientation;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  public:
  MapClient();
  ~MapClient();

  private:
  void handleLocalization(const vtr_msgs::PosePtr pose_msg);
  void handleLocalSubmap(const cartographer_ros_msgs::SubmapsPtr submaps);
  void handleLandmarkList(const visualization_msgs::MarkerArrayConstPtr &p_MarkerArray);
  void handleLandmarkFull(const visualization_msgs::MarkerArrayConstPtr &p_MarkerArray);
  void handleSubmaps(const cartographer_ros_msgs::SubmapImagesPtr &msg);
  void handlePointCloud(const sensor_msgs::PointCloud2Ptr msg);
  void handleSubmapImages(const cartographer_ros_msgs::SubmapImagesConstPtr msg);

  void displayMap(const ::ros::WallTimerEvent &unused_timer_event);
  static void onMouse(int event, int x, int y, int flag, void *);
  void handleMouse(cv::Mat &map);
  void getGlobalMap(cv::Mat &base_img_);
  // 	int mergeTransparentImgs(cv::Mat &dst, cv::Mat &scr, double scale);
  // 	Eigen::AlignedBox2d getBox(Eigen::Isometry3d pose,Eigen::Vector2d
  // sides,const double& resolution);
  void updateMapBox(const Eigen::AlignedBox2d &box);
  // 	void displayLocalizationMap(const ::ros::WallTimerEvent&
  // unused_timer_event);
  void loadLocalizationMap(const std::string &path);
  void refineSubmapImage(const cartographer_ros_msgs::SubmapImageEntry &submap);
  Eigen::Vector2i worldPose2ImagePose(const Eigen::Vector3d &world_pose,
                                      const float &display_ratio);
  std::vector<MapClient::PoseWithID> getWorldNavPoints(const std::string &map_path);
  virtual void clear() override;
  ros::NodeHandle nh_;
  ros::WallTimer wall_timer_;
  ros::ServiceClient initial_pose_client_;
  ros::Subscriber localization_sub_;
  ros::Subscriber odom_sub_;
  ros::Subscriber imu_sub_;
  ros::Subscriber local_submap_sub_;
  ros::Subscriber landmark_list_sub_;
  ros::Subscriber landmark_full_sub_;
  ros::Subscriber submap_imgs_sub_;
  ros::Subscriber point_cloud_sub_;
  ros::Subscriber submap_images_sub_;
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  ::ros::ServiceClient status_client_;
  ::ros::ServiceClient map_client_;
  ros::ServiceClient reference_client_;
  ros::ServiceClient load_submaps_client_;

  visualization_msgs::MarkerArray landmark_list_;
  visualization_msgs::MarkerArray landmark_full_;
  std::mutex landmark_list_mtx_;
  std::mutex landmark_full_mtx_;
  int inValid_cnt;
  std::mutex inValid_cnt_mtx_;
  std::mutex mutex_;
  static int mouse_status_;
  static cv::Point start_p_;
  static cv::Point end_p_;

  // 	std::map<int,ClientSubmap> client_submaps_;
  cv::Mat global_map_;
  Eigen::Affine3d base2global_;

  std::vector<Eigen::Vector2d> scan_points_;
  bool initialized_;
  std::map<uint, ClientSubmap> client_submaps_;
  Eigen::AlignedBox2d map_box_;

  double Max_[2];
  Eigen::Affine3d odom2global_;
  STATUS status_;
  float DISPLAY_RATIO;
  // 	cv::Mat base_img_;
};
} // namespace map_manager
#endif // MAPCLIENT_H
