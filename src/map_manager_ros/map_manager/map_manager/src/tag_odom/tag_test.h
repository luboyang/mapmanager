/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef TAG_TEST_H
#define TAG_TEST_H

#include <map_manager_msgs/TagMapInfos.h>
#include <ros/ros.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <mutex>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <stack>
#include <vector>

using namespace std;

struct Tag_info
{
  vector<int> neighbors_id;
  int id;
  Eigen::Vector3d p;
  Eigen::Quaterniond q;
};

struct tag_map_id
{
  int tag_map_r;
  int tag_map_c;

  bool operator<(const tag_map_id &g) const
  {
    return (tag_map_r < g.tag_map_r) || (tag_map_r == g.tag_map_r && tag_map_c < g.tag_map_c);
  }
};

class GetTagMap
{
  public:
  GetTagMap();
  ~GetTagMap();
  vector<Tag_info> getTagMap(const std::string &path1);
};

#endif // TAG_TEST_H
