/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "tag_test.h"

#include <glog/logging.h>
#include <map_manager_msgs/TagMapInfos.h>
#include <stdlib.h>

#include <Eigen/Core>
#include <fstream>
#include <iostream>
#include <istream>
#include <sstream>
#include <streambuf>
#include <string>
#include <vector>

using namespace std;

GetTagMap::GetTagMap() {}

GetTagMap::~GetTagMap() {}

vector<Tag_info> GetTagMap::getTagMap(const std::string &path1)
{
  std::ifstream csv_data(path1, std::ios::in);
  std::string line;

  if (!csv_data.is_open())
  {
    LOG(INFO) << "Error: opening file fail";
  }

  std::istringstream sin;
  std::vector<std::string> words;
  std::string word;

  map<tag_map_id, Tag_info> all_tag;

  int t_row = 0;
  int t_col = 0;

  while (std::getline(csv_data, line))
  {
    sin.clear();
    sin.str(line);
    words.clear();
    while (std::getline(sin, word, ','))
    {
      words.push_back(word);
      LOG(INFO) << word;

      tag_map_id temp_tag_map_id;
      temp_tag_map_id.tag_map_r = t_row;
      temp_tag_map_id.tag_map_c = t_col;

      int len = word.length();

      int id_s;
      int id_e;

      int x_s;
      int x_e;

      int y_s;
      int y_e;

      for (int i = 0; i < len; i++)
      {
        if (word[i] == 'd')
        {
          id_s = i + 1;
        }
        if (word[i] == '(')
        {
          id_e = i;
          x_s = i + 1;
        }
        if (word[i] == ' ')
        {
          x_e = i;
          y_s = i + 1;
        }
        if (word[i] == ')')
        {
          y_e = i;
        }
      }

      string str_id = word.substr(id_s, (id_e - id_s));
      string str_p_x = word.substr(x_s, (x_e - x_s));
      string str_p_y = word.substr(y_s, (y_e - y_s));

      Tag_info temp_tag;

      temp_tag.id = stoi(str_id);
      temp_tag.p(0) = atof(str_p_x.c_str()) / 100.0;
      temp_tag.p(1) = atof(str_p_y.c_str()) / 100.0;
      temp_tag.p(2) = 0;

      Eigen::Quaterniond t_q(0.0, 1.0, 0.0, 0.0);
      temp_tag.q = t_q;

      all_tag[temp_tag_map_id] = temp_tag;

      t_col++;
    }
    t_row++;
    t_col = 0;
    LOG(INFO) << endl;
  }
  csv_data.close();

  vector<Tag_info> tag_final;

  for (auto &it : all_tag)
  {
    Tag_info temp_tag;
    temp_tag.id = it.second.id;
    temp_tag.p = it.second.p;
    temp_tag.q = it.second.q;

    {
      tag_map_id key;
      key.tag_map_r = it.first.tag_map_r - 1;
      key.tag_map_c = it.first.tag_map_c;
      if (all_tag.find(key) != all_tag.end())
      {
        temp_tag.neighbors_id.push_back(all_tag[key].id);
      }
    }

    {
      tag_map_id key;
      key.tag_map_r = it.first.tag_map_r + 1;
      key.tag_map_c = it.first.tag_map_c;
      if (all_tag.find(key) != all_tag.end())
      {
        temp_tag.neighbors_id.push_back(all_tag[key].id);
      }
    }

    {
      tag_map_id key;
      key.tag_map_r = it.first.tag_map_r;
      key.tag_map_c = it.first.tag_map_c - 1;
      if (all_tag.find(key) != all_tag.end())
      {
        temp_tag.neighbors_id.push_back(all_tag[key].id);
      }
    }
    {
      tag_map_id key;
      key.tag_map_r = it.first.tag_map_r;
      key.tag_map_c = it.first.tag_map_c + 1;
      if (all_tag.find(key) != all_tag.end())
      {
        temp_tag.neighbors_id.push_back(all_tag[key].id);
      }
    }

    tag_final.push_back(temp_tag);
  }

  return tag_final;
}