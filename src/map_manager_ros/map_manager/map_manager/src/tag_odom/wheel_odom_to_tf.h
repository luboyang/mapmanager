/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef WHEELODOMTOTF_H
#define WHEELODOMTOTF_H
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <tf2_ros/transform_broadcaster.h>

#include <Eigen/Eigen>
// #include <iplus_perception/perception.h>
// #include <pluginlib/class_loader.h>
// #include <pluginlib/class_list_macros.h>
#include <cartographer_ros_msgs/SubmapTag.h>
class WheelOdomToTF
{
  public:
  WheelOdomToTF();
  ~WheelOdomToTF();
  void startHandleImage();
  void stopHandleImage();

  private:
  void setCamParams();
  void pubTag(const ::ros::WallTimerEvent &unused_timer_event);
  void HandleWheelOdomtry(const nav_msgs::Odometry::ConstPtr &msg);

  ros::NodeHandle nh_;
  ros::Subscriber sub_;
  ros::Publisher location_msg_pub_;
  ros::WallTimer wall_timer_;
  ros::Time last_pub_landmark_time_;
  //   tf2_ros::Buffer tfBuffer_;
  //   tf2_ros::TransformListener tfListener_;
  tf2_ros::TransformBroadcaster tf_broadcaster_;
  Eigen::Affine3d initial_pose_;
  bool initialised_;
  //   std::shared_ptr<pluginlib::ClassLoader<SVC::Perception>>
  //   perception_base_; boost::shared_ptr<SVC::Perception> cam_perception_ptr_;
  //   std::mutex cam_mutex_;
  Eigen::Affine3d cam2base_;
  bool cam_initialised_;
  float proj_index_ratio_;
};

#endif // WHEELODOMTOTF_H
