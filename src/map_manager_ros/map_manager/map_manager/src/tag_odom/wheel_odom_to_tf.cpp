/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "wheel_odom_to_tf.h"

#include <eigen_conversions/eigen_msg.h>
using namespace std;
WheelOdomToTF::WheelOdomToTF()
    : initial_pose_(Eigen::Affine3d::Identity()), initialised_(false), cam_initialised_(false)
{
  string odom_topic = "/emma_odom";
  ::ros::param::get("/algo/property/motion_odom_topic", odom_topic);
  cout << "odom topic: " << odom_topic << endl;
  sub_ = nh_.subscribe(odom_topic, 10, &WheelOdomToTF::HandleWheelOdomtry, this);
  //   location_msg_pub_ =
  //   nh_.advertise<cartographer_ros_msgs::SubmapTag>("/submap_tag", 5);
  //
  //   proj_index_ratio_ = 0.92f;
  //   perception_base_ =
  //   std::make_shared<pluginlib::ClassLoader<SVC::Perception>>("iplus_perception",
  //   "SVC::Perception"); startHandleImage();
}

WheelOdomToTF::~WheelOdomToTF() { sub_.shutdown(); }

void WheelOdomToTF::HandleWheelOdomtry(const nav_msgs::Odometry::ConstPtr &msg)
{
  geometry_msgs::Pose cur_pose = msg->pose.pose;
  Eigen::Affine3d cur_pose_eigen;
  tf::poseMsgToEigen(cur_pose, cur_pose_eigen);
  if (!initialised_)
  {
    initial_pose_ = cur_pose_eigen;
    initialised_ = true;
  }
  geometry_msgs::TransformStamped pub_msg;
  pub_msg.header.stamp = msg->header.stamp;
  pub_msg.header.frame_id = "tag_odom";
  pub_msg.child_frame_id = "tag_base_footprint";
  tf::transformEigenToMsg(initial_pose_.inverse() * cur_pose_eigen, pub_msg.transform);
  Eigen::Affine3d temp;
  tf::transformMsgToEigen(pub_msg.transform, temp);
  //   cout << "relative_pose: " << temp.translation().transpose() <<endl;
  tf_broadcaster_.sendTransform(pub_msg);
}

// void WheelOdomToTF::setCamParams()
// {
//
//   int towards = 1;//0-up, 1-down, 2-front
//   if(!ros::param::get("/cartographer_node/cam_towards", towards))
//     LOG(ERROR) << "Can not get /cartographer_node/cam_towards param, using
//     down ...";
//
//   int tag_codec_type = 0;
//   if (!ros::param::get("/tag_codec_type", tag_codec_type))
//   {
//     LOG(WARNING) << ("CameraPerception: no tag_codec_type given, using
//     tag36h11 as default."); tag_codec_type = 0;
//   }
//   map<string,boost::any> config_map;
//
//   {
//     cam_perception_ptr_ =
//     perception_base_->createInstance("SVC::CameraPerception");
//     if(tag_codec_type!=2)
//       config_map["tag_type"] = 2;
//     else
//       config_map["tag_type"] = 7;
//   }
//   config_map["debug_mode"] = false;
//   config_map["camera_type"] = towards;// 0shang 1 2
//   config_map["tag_codec_type"] = tag_codec_type; //0 1 2
//   config_map["is_lateral_base"] = false;
//   config_map["teaching"] = false;
//   config_map["sample_ratio"] = 1.0f;
//   config_map["proj_index_ratio"] = proj_index_ratio_;
//   config_map["use_center_tag"] = true;
//   Eigen::Vector3d cam2base_p(0,0,0);
//   Eigen::Vector3d cam2base_eul(0,0,0);
//   string toward_str = (towards==2 ? "front":"down");
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/px", cam2base_p[0]);
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/py", cam2base_p[1]);
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/pz", cam2base_p[2]);
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/yaw", cam2base_eul[0]);
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/pitch", cam2base_eul[1]);
//   nh_.getParam("/jzhw/calib/camera/"+toward_str+"/roll", cam2base_eul[2]);
//   Eigen::Matrix3d rot;
//   rot = (Eigen::AngleAxisd(cam2base_eul[0],Eigen::Vector3d::UnitZ())
//       * Eigen::AngleAxisd(cam2base_eul[1],Eigen::Vector3d::UnitY())
//       * Eigen::AngleAxisd(cam2base_eul[2],Eigen::Vector3d::UnitX())).toRotationMatrix();
//   Eigen::Quaterniond cam2base_q(rot);
//
//   cam2base_ = Eigen::Translation3d(cam2base_p) * cam2base_q;
//   auto nh = std::make_shared<ros::NodeHandle>(nh_);
//   cam_perception_ptr_->initialize(nh, config_map);
//   cout <<"\033[33m" << "cam2base: \n" << cam2base_.matrix() <<endl;
//   wall_timer_ = nh_.createWallTimer(::ros::WallDuration(0.1),
//                         &WheelOdomToTF::pubTag, this);
//   wall_timer_.stop();
// }
//
// void WheelOdomToTF::startHandleImage()
// {
//
//   LOG(INFO) << "Start Handling visual_landmarks...";
//   if(!cam_initialised_)
//   {
//     setCamParams();
//     cam_initialised_ = true;
//   }
//   std::unique_lock<std::mutex> lk(cam_mutex_);
//   LOG(INFO) << "starting detect tags!";
//   cam_perception_ptr_->start();
//   LOG(INFO) << "detect tags started!";
//   wall_timer_.start();
// }
//
// void WheelOdomToTF::stopHandleImage()
// {
//   std::unique_lock<std::mutex> lk(cam_mutex_);
//   LOG(INFO) << "Stop Handling visual_landmarks...";
//   wall_timer_.stop();
//   cam_perception_ptr_->stop();
//   LOG(INFO) << "detect tags stoped!";
// }
//
// void WheelOdomToTF::pubTag(const ros::WallTimerEvent& unused_timer_event)
// {
//   SVC::PerceptionResult pr;
//   std::map<string, Eigen::Affine3d> wMe_map;
//   cam_perception_ptr_->GetPerceptionByClass(pr);
//
//   if (pr.valid)
//   {
//     if(pr.stamp <= last_pub_landmark_time_)
//     {
//       LOG(ERROR) << "stamp WARNING";
//       last_pub_landmark_time_ = pr.stamp;
//       return;
//     }
//     last_pub_landmark_time_ = pr.stamp;
//     int cur_id = pr.tag_id;
//     Eigen::Affine3d cam2tag= pr.wMc;
//     Eigen::Affine3d tag2base = cam2base_ * (cam2tag.inverse());
//     Eigen::Vector3d tag2base_t(tag2base.matrix().block(0,3,3,1));
//     Eigen::Matrix3d tmp_rot = tag2base.matrix().block(0,0,3,3);
//     Eigen::Quaterniond tag2base_q(tmp_rot);
//     cartographer_ros_msgs::SubmapTag msg;
//     msg.header.stamp = pr.stamp;
//     msg.header.frame_id = "tag_base_footprint";
//     msg.localization_type = "tag";
//     msg.tag_id = pr.tag_id;
//     tf::poseEigenToMsg(tag2base, msg.tag2base);
//     Eigen::Affine3d base2odom_tmp(Eigen::Affine3d::Identity());
//     try
//     {
//       geometry_msgs::TransformStamped base2odom_tf =
//           tfBuffer_.lookupTransform("tag_odom", "tag_base_footprint",
//           ros::Time(0));
//
//       tf::transformMsgToEigen(base2odom_tf.transform, base2odom_tmp);
//       tf::poseEigenToMsg(base2odom_tmp, msg.base2odom);
//     }
//     catch (const std::exception ex)
//     {
//       return;
//     }
//     msg.debug_flag = false;
//     msg.tag_ns = "Visualmarks";
//     location_msg_pub_.publish(msg);
//   }
// }

// int main(int argc, char **argv)
// {
//   ros::init(argc, argv, "wheel_odom_to_tf");
//   ros::NodeHandle n;
//   WheelOdomToTF wtf(n);
//
//   ros::spin();
//   return 1;
// }