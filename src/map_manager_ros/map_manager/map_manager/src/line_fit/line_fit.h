

#ifndef MAP_MANAGER_LINE_FIT_H_
#define MAP_MANAGER_LINE_FIT_H_

#include <glog/logging.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <Eigen/Eigen>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

namespace map_manager
{
class lineFit
{
  public:
  struct linePoint
  {
    double up_x;
    double up_y;
    double down_x;
    double down_y;
  };
  struct rectPoint
  {
    cv::Point point_1;
    cv::Point point_2;
    cv::Point point_3;
    cv::Point point_4;
  };
  lineFit(const std::string &map_path);
  ~lineFit();
  void linDraw(std::map<int, std::vector<Eigen::Vector3d>> points_in_map);
  void setOriginStripPoints(const std::vector<Eigen::Vector2d> &origin_points);

  private:
  //最小二乘拟合
  void nihe(double *x, double *y, int N, double &a, double &b);

  //直线的合并与拟合
  bool sampleLineToFit(float &A, float &B, float &C);

  //进行直线的筛选与拟合
  void selectLine(std::vector<linePoint> Points);

  void saveDistanceTransformImage();

  void getCrossPoint();

  //   std::vector<std::queue<linePoint>> clusterPoints(std::queue<linePoint>*
  //   points);
  std::vector<std::queue<cv::Point2f>> clusterPoints(std::queue<lineFit::linePoint> *points);
  //   void savePointsToTxt(std::vector<std::queue<linePoint>>
  //   clustered_points);
  void savePointsToTxt(std::vector<std::queue<cv::Point2f>> clustered_points);
  void savePointsToTxt(std::map<int, std::vector<Eigen::Vector3d>> points_in_map);
  void savePointsToTxt(const std::vector<Eigen::Vector2d> &points);
  void read8Point(int i, int j);
  void read5Point(int i, int j);
  void read3Point_x(int i, int j);
  void read3Point_y(int i, int j);

  int func_nc8(int *b);
  void cvHilditchThin(cv::Mat &src, cv::Mat &dst);
  //   void clusterPoints(std::queue<linePoint>* points);
  int line_side;
  std::vector<linePoint> Points_left_;
  std::vector<linePoint> Points_right_;
  std::vector<linePoint> Line_sati;
  linePoint temp, temp2;
  double lenth = 0;
  double angle = 0;

  cv::Point2f p_cross;

  uchar temp_skeleton[9];
  std::vector<cv::Point> skeleton_p;

  uchar temp_target[5];
  std::vector<cv::Point> target_p;
  std::vector<cv::Point2d> startPoint;
  double lenth_avg;
  double angle_avg;

  cv::Mat skeletonPic;
  cv::Mat contourPic;
  cv::Mat line_img_;
  cv::Mat clusterPic;

  int lenth_line = 0;
  std::vector<int> lenth_all;

  int rows_;
  int cols_;
  float resolution_;
  float max_box_[2];
  std::string map_path_;
  std::vector<Eigen::Vector2d> final_strip_points_;
};
} // namespace map_manager
#endif // MAP_MANAGER_LINE_FIT_H_
