#include "line_fit/line_fit.h"

#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <chrono>

#include "bsplinefitting/spline_curve_fitting.h"
#define LENTH_RANGE 5
#define ANGLE_RANGE 2
//  #define TEST
using namespace cv;
using namespace std;
namespace map_manager
{
lineFit::lineFit(const std::string &map_path) : map_path_(map_path)
{
  boost::property_tree::ptree pt, pt_poses;
  boost::property_tree::read_xml(map_path_ + "map_data.xml", pt_poses);

  skeleton_p.resize(9);
  target_p.resize(5);
  //   target_p.resize(3);
  startPoint.resize(2);

  if (pt_poses.empty())
  {
    LOG(WARNING) << "data.xml is empty! ";
  }
  pt = pt_poses.get_child("mapPng");

  rows_ = pt.get<int>("width");
  cols_ = pt.get<int>("height");
  resolution_ = pt.get<double>("resolution");

  std::string data;
  data = pt.get<std::string>("pose");
  double pose[7];
  sscanf(data.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &pose[0], &pose[1], &pose[2], &pose[3],
         &pose[4], &pose[5], &pose[6]);

  max_box_[0] = pose[1] * resolution_;
  max_box_[1] = pose[0] * resolution_;
  line_img_ = Mat(rows_, cols_, CV_8UC3, Scalar(255, 255, 255));

  LOG(WARNING) << "max_box:" << max_box_[0] << "," << max_box_[1];
  LOG(WARNING) << "resolution_:" << resolution_;
}

lineFit::~lineFit() {}

void lineFit::setOriginStripPoints(const std::vector<Eigen::Vector2d> &origin_points)
{
  if (!origin_points.empty())
  {
    for (auto point : origin_points)
    {
      Point p_temp;
      p_temp.x = (max_box_[1] - point[1]) / resolution_;
      p_temp.y = (max_box_[0] - point[0]) / resolution_;
      circle(line_img_, p_temp, 0.5, Scalar(255, 0, 0));
      final_strip_points_.push_back(point);
    }
    LOG(INFO) << "origin strip points size: " << final_strip_points_.size();
  }
}

void lineFit::linDraw(std::map<int, std::vector<Eigen::Vector3d>> points_in_map)
{
  // get the point in coordinate
  LOG(WARNING) << "Save points in map!";
  savePointsToTxt(points_in_map);
  LOG(WARNING) << "points_in_map size: " << points_in_map.size();

  std::queue<linePoint> points_temp[2];

  for (std::size_t i = 0; i < 2; i++)
  {
    LOG(WARNING) << "points_in_map size[i]: " << points_in_map[i].size();
    for (std::size_t j = 0; j < points_in_map[i].size();)
    {
      if (i == 0 && j == 0)
      {
        startPoint[0] = Point((max_box_[1] - points_in_map[i][j](1)) / resolution_,
                              (max_box_[0] - points_in_map[i][j](0)) / resolution_);
      }
      if (i == 1 && j == 0)
      {
        startPoint[1] = Point((max_box_[1] - points_in_map[i][j](1)) / resolution_,
                              (max_box_[0] - points_in_map[i][j](0)) / resolution_);
      }
      linePoint point;
      point.up_x = points_in_map[i][j](0);
      point.up_y = points_in_map[i][j](1);
      ++j;
      point.down_x = points_in_map[i][j](0);
      point.down_y = points_in_map[i][j](1);
      ++j;
      points_temp[i].push(point);
    }
  }
  // do the point cluster
  LOG(WARNING) << "Start clustering points.";
  vector<queue<Point2f>> points = clusterPoints(points_temp);
  //   vector<std::queue<linePoint>> points = clusterPoints(points_temp);
  LOG(WARNING) << "Clustering points done!";

  //   for(int i=0; i < points.size(); i++)
  //   {
  //   LOG(INFO)<<i;
  //     temp = points[i].front();
  //     double angle1 = fabs(atan((temp.up_y - temp.down_y) / (temp.up_x -
  //     temp.down_x)) * 180 / CV_PI);
  //
  //     double angle2 = angle1;
  //     int num_line = 0;
  //     num_line = 0;
  //     lenth = 0;
  //     angle = 0;
  //     Points_left_.clear();
  //     while(!points[i].empty())
  //     {
  //       temp = points[i].front();
  //       angle1 = fabs(atan((temp.up_y - temp.down_y) / (temp.up_x -
  //       temp.down_x)) * 180 / CV_PI);
  //
  //       if (fabs(angle1 - angle2) < 40.)
  //       {
  //         if(1)
  //         {
  //           Point p11,p22;
  //           p11.x = (max_box_[1]  - temp.up_y) / resolution_;
  //           p11.y = (max_box_[0]  - temp.up_x) / resolution_;
  //
  //           p22.x = (max_box_[1]  - temp.down_y) / resolution_;
  //           p22.y = (max_box_[0]  - temp.down_x) / resolution_;
  // //           circle(line_img_,p11,1,Scalar(0,0,255));
  // //           circle(line_img_,p22,1,Scalar(0,0,255));
  //         }
  //         LOG(WARNING) <<"angle1: " <<  angle1 ;
  //         num_line++;
  //         //计算直线的总的角度和长度
  //         lenth += sqrt((temp.up_x - temp.down_x) * (temp.up_x - temp.down_x)
  //         + (temp.up_y - temp.down_y) * (temp.up_y - temp.down_y)); angle +=
  //         angle1; Points_left_.push_back(temp); points[i].pop(); continue;
  //       }
  //       angle2 = angle1;
  //
  //       int temp_num_line = num_line;
  //       lenth_avg = lenth / temp_num_line;
  //       angle_avg = angle / temp_num_line;
  //       num_line = 0;
  //       lenth = 0;
  //       angle = 0;
  //       angle1 = angle2;
  //       LOG(WARNING) << "Points_left_ size: " << Points_left_.size();
  //       selectLine(Points_left_);
  //       Points_left_.clear();
  //     }
  //   angle2 = angle1;
  //   int temp_num_line = num_line;
  //   lenth_avg = lenth / temp_num_line;
  //   angle_avg = angle / temp_num_line;
  //   num_line = 0;
  //   lenth = 0;
  //   angle = 0;
  //   angle1 = angle2;
  //   LOG(WARNING) << "Points_left_ size: " << Points_left_.size();
  //   selectLine(Points_left_);
  //   Points_left_.clear();
  //
  //   }
  //   imshow("temp", line_img_);
  //   waitKey(0);

  auto temp_points = points;
  LOG(INFO) << "points->size()" << points.size();

  std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  for (std::size_t i = 0; i < points.size(); i++)
  {
    //     for(int k=0;k<points[i].size()/4;k++)
    //     {
    //       points[i].pop();
    //     }
    LOG(INFO) << "lenth_all size: " << lenth_all[i];
    vector<Eigen::Vector2d> points_temp;
    auto temp1 = points[i].front();
    while (!points[i].empty())
    {
      auto temp2 = temp1;
      temp1 = points[i].front();

      float dis = abs(sqrt((temp2.y - temp1.y) * (temp2.y - temp1.y) +
                           (temp2.x - temp1.x) * (temp2.x - temp1.x)));
      if (dis > 10)
      {
        LOG(INFO) << "dis:" << dis;
      }
      points_temp.push_back({temp1.x, temp1.y});
      points[i].pop();
    }
    if (points_temp.size() < 2)
    {
      continue;
    }
    int contr_p;
    contr_p = lenth_all[i];
    if ((contr_p / 20) < 8)
    {
      contr_p = 8;
    }
    else
    {
      contr_p = contr_p / 20;
    }

    int countest;
    contr_p = 4;
#ifdef TEST
    for (std::size_t i = 0; i < points_temp.size(); i++)
    {
      Point2f p1;
      p1.x = points_temp[i](0);
      p1.y = points_temp[i](1);
      int a = (max_box_[1] - p1.y) / resolution_;
      int b = (max_box_[0] - p1.x) / resolution_;
      circle(line_img_, Point(a, b), 0.5, Scalar(0, 0, 255));
      if (i % 50 == 0)
      {
        imshow("test", line_img_);
        waitKey(0);
      }
    }
#endif
    OpenCubicBSplineCurve curve(0.002);
    SplineCurveFitting scf;
    scf.fitAOpenCurve(points_temp, curve, contr_p, 15, 0.005, 0.005, 0.001);

    auto result_points = curve.getSamples();
    for (auto p1 : result_points)
    {
      Point p_temp;
      countest++;
      p_temp.x = p1(0);
      p_temp.y = p1(1);
      p_temp.x = (max_box_[1] - p1(1)) / resolution_;
      p_temp.y = (max_box_[0] - p1(0)) / resolution_;
      //       LOG(INFO)<<"p_temp:"<<p_temp.x<<" "<<p_temp.y;
      circle(line_img_, p_temp, 0.5, Scalar(255, 0, 0));
    }
    final_strip_points_.insert(final_strip_points_.end(), result_points.begin(),
                               result_points.end());
  }
  //   imshow("temp", line_img_);
  //   waitKey(0);

  //   for(int i=0; i < points.size(); i++)
  //   {
  //     LOG(INFO)<<i;
  //
  //     auto temp1 = points[i].front();
  //
  //     double angle1 = fabs(atan((temp.up_y - temp.down_y) / (temp.up_x -
  //     temp.down_x)) * 180 / CV_PI); int num_line = 0; lenth = 0; angle = 0;
  //
  //     Points_left_.clear();
  //     while(!points[i].empty())
  //     {
  //       temp = points[i].front();
  //       angle1 = fabs(atan((temp.up_y - temp.down_y) / (temp.up_x -
  //       temp.down_x)) * 180 / CV_PI); Point p11,p22; p11.x = (max_box_[1]  -
  //       temp.up_y) / resolution_; p11.y = (max_box_[0]  - temp.up_x) /
  //       resolution_; p22.x = (max_box_[1]  - temp.down_y) / resolution_;
  //       p22.y = (max_box_[0]  - temp.down_x) / resolution_;
  //
  //       circle(line_img_,p11,1,Scalar(0,0,255));
  //       circle(line_img_,p22,1,Scalar(0,0,255));
  //
  //       LOG(WARNING) <<"angle1: " <<  angle1 ;
  //       num_line++;
  //       //calculate the line lenth and angle
  //       lenth += sqrt((temp.up_x - temp.down_x) * (temp.up_x - temp.down_x) +
  //       (temp.up_y - temp.down_y) * (temp.up_y - temp.down_y)); angle +=
  //       angle1;
  //
  //       Points_left_.push_back(temp);
  //       points[i].pop();
  //       int temp_num_line = num_line;
  //       lenth_avg = lenth / temp_num_line;
  //       angle_avg = angle / temp_num_line;
  //     }
  //     LOG(WARNING) << "Points_left_ size: " << Points_left_.size();
  //     //select the satisfied line and do the line fitting
  //     selectLine(Points_left_);
  //     Points_left_.clear();
  //   }
  std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
  auto duration_ms = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1);
  LOG(INFO) << "during time: " << duration_ms.count() << "s ";
  savePointsToTxt(final_strip_points_);
  savePointsToTxt(temp_points);
  saveDistanceTransformImage();
}

void lineFit::selectLine(vector<linePoint> Points)
{
  if (Points.size() < 2) return;
  double Max_Y = 0., Min_Y = rows_;
  double Max_X = 0., Min_X = cols_;
  float A = 0, B = 0, C = 0;
  float lenth;

  LOG(WARNING) << "lenth_avg: " << lenth_avg << ", angle_avg: " << angle_avg;
  Line_sati.clear();
  for (unsigned int j = 0; j < Points.size(); j++)
  {
    // the length satisfied
    lenth = sqrt((Points[j].up_x - Points[j].down_x) * (Points[j].up_x - Points[j].down_x) +
                 (Points[j].up_y - Points[j].down_y) * (Points[j].up_y - Points[j].down_y));
    if (lenth < lenth_avg - LENTH_RANGE || lenth > lenth_avg + LENTH_RANGE) continue;

    // get the endpoint of the line
    double temp_p1, temp_p2;

    if (angle_avg > 45.)
    {
      temp_p1 = Points[j].up_y;
      temp_p2 = Points[j].down_y;
      if (temp_p1 > temp_p2)
      {
        Max_Y = (temp_p1 > Max_Y) ? temp_p1 : Max_Y;
        Min_Y = (temp_p2 < Min_Y) ? temp_p2 : Min_Y;
      }
      else if (temp_p1 < temp_p2)
      {
        Max_Y = (temp_p2 > Max_Y) ? temp_p2 : Max_Y;
        Min_Y = (temp_p1 < Min_Y) ? temp_p1 : Min_Y;
      }
    }
    else if (angle_avg < 45.)
    {
      temp_p1 = Points[j].up_x;
      temp_p2 = Points[j].down_x;
      //       if(Points[j].up_x>Points[j].down_x)
      if (temp_p1 > temp_p2)
      {
        Max_X = (temp_p1 > Max_X) ? temp_p1 : Max_X;
        Min_X = (temp_p2 < Min_X) ? temp_p2 : Min_X;
      }
      else if (temp_p1 < temp_p2)
      {
        Max_X = (temp_p2 > Max_X) ? temp_p2 : Max_X;
        Min_X = (temp_p1 < Min_X) ? temp_p1 : Min_X;
      }
    }
    if (0)
    {
      Point p11, p22;
      p11.x = (max_box_[1] - Points[j].up_y) / resolution_;
      p11.y = (max_box_[0] - Points[j].up_x) / resolution_;

      p22.x = (max_box_[1] - Points[j].down_y) / resolution_;
      p22.y = (max_box_[0] - Points[j].down_x) / resolution_;
      circle(line_img_, p11, 1, Scalar(0, 255, 0));
      circle(line_img_, p22, 1, Scalar(0, 255, 0));
      //    LOG(INFO)<<p11.x<<" "<<p11.y<<" "<<p22.x<<" "<<p22.y;
      //    LOG(INFO)<<Points[j].up_x<<" "<<Points[j].up_y;
    }
    Line_sati.push_back(Points[j]);
  }
  LOG(WARNING) << "Line_sati size: " << Line_sati.size();

  sampleLineToFit(A, B, C);
  LOG(WARNING) << "Max_X: " << Max_X << ", Max_Y: " << Max_Y << ", Min_X: " << Min_X
               << ", Min_Y: " << Min_Y;
  LOG(WARNING) << "A: " << A << ", B: " << B << ", C: " << C;
  // do the line fit
  Point2f p1, p2;
  if (angle_avg > 45.)
  {
    Max_X = (-C - B * Max_Y) / A;
    Min_X = (-C - B * Min_Y) / A;

    p1.x = Max_X;
    p1.y = Max_Y;
    p2.x = Min_X;
    p2.y = Min_Y;
  }
  else if (angle_avg <= 45)
  {
    Max_Y = (-C - A * Max_X) / B;
    Min_Y = (-C - A * Min_X) / B;

    p1.x = Max_X;
    p1.y = Max_Y;
    p2.x = Min_X;
    p2.y = Min_Y;
  }
  LOG(INFO) << max_box_[0] << " " << max_box_[1];
  LOG(INFO) << p1.x << " " << p1.y;
  LOG(INFO) << p2.x << " " << p2.y;
  Point p11, p22;
  p11.x = (max_box_[1] - p1.y) / resolution_;
  p11.y = (max_box_[0] - p1.x) / resolution_;

  p22.x = (max_box_[1] - p2.y) / resolution_;
  p22.y = (max_box_[0] - p2.x) / resolution_;

  LOG(INFO) << p11.x << " " << p11.y;
  LOG(INFO) << p22.x << " " << p22.y;
  cv::line(line_img_, p11, p22, Scalar(255, 0, 0), 1);
  Max_Y = 0;
  Min_Y = rows_;
  Max_X = 0;
  Min_X = cols_;
}

bool lineFit::sampleLineToFit(float &A, float &B, float &C)
{
  double x1, x2, y1, y2;
  int curlenIndex = 0;
  // double x[10], y[10];
  vector<float> ax, ay;
  double sumX = 0, sumY = 0;
  // line sampling
  for (unsigned int i = 0; i < Line_sati.size(); i++)
  {
    // LOG(INFO) << Line.size() ;
    x1 = Line_sati[i].up_x;
    y1 = Line_sati[i].up_y;
    x2 = Line_sati[i].down_x;
    y2 = Line_sati[i].down_y;
    // LOG(INFO) << x1 << " " << x2 <<" "<<y1<<" "<<y2 ;
    double len = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    double curX = x2;
    double curY = y2;
    double stepX = (x1 - x2) / len;
    // double stepX = 0;
    double stepY = (y1 - y2) / len;
    // LOG(INFO) << stepY ;
    while ((stepX <= 0) == (curX >= x1) && (stepY <= 0) == (curY >= y1))
    // while(curY<y1 && (stepX <= 0) == (curX >= x1))
    {
      ax.push_back(curX);
      ay.push_back(curY);
      // x[curlenIndex] = curX;
      // y[curlenIndex] = curY;
      sumX += stepX;
      sumY += stepY;
      curX += stepX;
      curY += stepY;

      // LOG(INFO) << y[curlenIndex] ;
      ++curlenIndex;
      // LOG(INFO) << curlenIndex ;
    }
  }
  double *x = new double[curlenIndex];
  double *y = new double[curlenIndex];
  for (int i = 0; i < curlenIndex; i++)
  {
    x[i] = ax[i];
    y[i] = ay[i];
    // LOG(INFO) << x[i] <<' '<< y[i] ;
  }

  // LOG(INFO) << sumX ;
  double a, b;
  // if(1)
  if (abs(sumX) > abs(sumY))
  {
    nihe(x, y, curlenIndex, a, b);
    A = b;
    B = -1;
    C = a;
    double s = sqrt(A * A + B * B + C * C);
    A /= s;
    B /= s;
    C /= s;
  }
  else
  {
    nihe(y, x, curlenIndex, a, b);
    A = -1;
    B = b;
    C = a;
    double s = sqrt(A * A + B * B + C * C);
    A /= s;
    B /= s;
    C /= s;
  }
  delete[] x;
  delete[] y;
  return true;
}

// line fit
void lineFit::nihe(double *x, double *y, int N, double &a, double &b)
{
  double sum_x_square = 0;
  double sum_x = 0;
  double sum_y = 0;
  double sum_x_y = 0;
  double x_sum_square = 0;

  for (int i = 0; i < N; i++)
  {
    sum_x_square += x[i] * x[i];
    sum_y += y[i];
    sum_x += x[i];
    sum_x_y += x[i] * y[i];
  }
  x_sum_square = sum_x * sum_x;

  a = ((sum_x_square * sum_y) - (sum_x * sum_x_y)) / (N * sum_x_square - x_sum_square);
  b = ((N * sum_x_y) - (sum_x * sum_y)) / (N * sum_x_square - x_sum_square);
}

void lineFit::saveDistanceTransformImage()
{
  Mat dist, dist1, threshold_img;
  cvtColor(line_img_, dist1, CV_BGR2GRAY);
  threshold(dist1, threshold_img, 128, 255, CV_THRESH_BINARY);
  cv::distanceTransform(threshold_img, dist, CV_DIST_L1, CV_DIST_MASK_PRECISE);

  Mat c, d;
  dist.convertTo(c, CV_8U, 1., 1.);
  c.convertTo(d, CV_16U, 128.);
  for (int i = 0; i < d.rows; i++)
    for (int j = 0; j < d.cols; j++)
    {
      if (d.at<uint16_t>(i, j) > 800)
      {
        //         d.at<uint16_t>(i, j) =  2 * d.at<uint16_t>(i, j);
        //         if (d.at<uint16_t>(i, j) > 32640)
        d.at<uint16_t>(i, j) = 32640;
      }
    }
  std::vector<int> compression_params;
  compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(0);
  compression_params.push_back(cv::IMWRITE_PNG_STRATEGY);
  compression_params.push_back(cv::IMWRITE_PNG_STRATEGY_DEFAULT);
  //   imshow("123", d);
  //   waitKey(0);
  imwrite(map_path_ + "frames/0/edge_probability_grid.png", d, compression_params);
  imwrite(map_path_ + "strip.png", line_img_, compression_params);
}

// do the point clustering
std::vector<queue<Point2f>> lineFit::clusterPoints(queue<lineFit::linePoint> *points)
// std::vector<queue< lineFit::linePoint >> lineFit::clusterPoints(queue<
// lineFit::linePoint >* points)
{
  clusterPic = Mat(line_img_.rows, line_img_.cols, CV_8UC1, Scalar(0));
  contourPic = Mat(line_img_.rows, line_img_.cols, CV_8UC1, Scalar(0));
  skeletonPic = Mat(line_img_.rows, line_img_.cols, CV_8UC1, Scalar(0));
  //   int color = -1;
  //并将map点与像素点之间定义映射关系初始化映射向量
  vector<vector<vector<Point2f>>> pointmaping;
  pointmaping.resize(clusterPic.rows);

  for (std::size_t s = 0; s < pointmaping.size(); s++)
  {
    pointmaping[s].resize(clusterPic.cols);
  }

  vector<Point> changePoint[2];
  vector<Point> pro_point;
  for (int i = 0; i < 2; i++)
  {
    queue<lineFit::linePoint> temp_queue(points[i]); // copy
    temp = temp_queue.front();

    double angle1 = fabs(atan((temp.up_y - temp.down_y) / (temp.up_x - temp.down_x)) * 180 / CV_PI);
    double angle2 = angle1;
    temp2 = temp;
    while (!temp_queue.empty())
    {
      Point p11, p22;
      Point2f map1, map2;
      temp = temp_queue.front();

      double angle1 =
          fabs(atan((temp.up_y - temp.down_y) / (temp.up_x - temp.down_x)) * 180 / CV_PI);

      if (fabs(angle1 - angle2) < 40)
      {
        p11.x = (max_box_[1] - temp.up_y) / resolution_;
        p11.y = (max_box_[0] - temp.up_x) / resolution_;

        p22.x = (max_box_[1] - temp.down_y) / resolution_;
        p22.y = (max_box_[0] - temp.down_x) / resolution_;
        circle(clusterPic, p11, 1, Scalar(255), -1, 8);
        circle(clusterPic, p22, 1, Scalar(255), -1, 8);

        contourPic.at<uchar>(p11.y, p11.x) = char(128);
        contourPic.at<uchar>(p22.y, p22.x) = char(128);

        map1.x = temp.up_x;
        map1.y = temp.up_y;
        map2.x = temp.down_x;
        map2.y = temp.down_y;
        //         LOG(INFO)<<"map1"<<map1.x<<" "<<map1.y;
        pointmaping[p11.y][p11.x].push_back(map1);
        pointmaping[p22.y][p22.x].push_back(map2);
        temp_queue.pop();
        //         imshow("121",clusterPic);
      }
      else
      {
        Point2f p33;
        //         if(temp_queue.size()>4)
        //         {
        //           for(int k=0;k<1;k++)
        //           {
        //
        //             LOG(INFO)<<"3";
        //             temp = temp_queue.front();
        //             p11.x = (max_box_[1]  - temp.up_y) / resolution_;
        //             p11.y = (max_box_[0]  - temp.up_x) / resolution_;
        //
        //             p22.x = (max_box_[1]  - temp.down_y) / resolution_;
        //             p22.y = (max_box_[0]  - temp.down_x) / resolution_;
        //             contourPic.at<uchar>(p11.y,p11.x)=char(128);
        //             contourPic.at<uchar>(p22.y,p22.x)=char(128);
        //
        //             map1.x = temp.up_x;map1.y = temp.up_y;
        //             map2.x = temp.down_x;map2.y = temp.down_y;
        //             pointmaping[p11.y][p11.x].push_back(map1);
        //             pointmaping[p22.y][p22.x].push_back(map2);
        //             temp_queue.pop();
        //           }
        //         }
        temp = temp_queue.front();

        getCrossPoint();
        p33.x = (max_box_[1] - p_cross.y) / resolution_;
        p33.y = (max_box_[0] - p_cross.x) / resolution_;
        pointmaping[p33.y][p33.x].push_back(p_cross);
        contourPic.at<uchar>(p33.y, p33.x) = char(128);
        //         LOG(INFO)<<"Start:"<<int(p33.x)<<" "<<int(p33.y);
        pro_point.push_back(p33);
        temp_queue.pop();
      }
      angle2 = angle1;
      temp2 = temp;
    }
  }

  for (std::size_t i = 0; i < pro_point.size(); i++)
  {
    circle(clusterPic, pro_point[i], 2, Scalar(128), -1, 8);
  }

  // do the morphological changes
  Mat element2 = getStructuringElement(MORPH_RECT, Size(2, 2));
  erode(clusterPic, clusterPic, element2, Point(-1, -1), 1);
  cvHilditchThin(clusterPic, skeletonPic);
//   for(int i=0;i<pro_point.size();i++)
//   {
//     circle(skeletonPic,pro_point[i],1,Scalar(0),-1,8);
//   }
#ifdef TEST
  imshow("1", clusterPic);
  imshow("2", skeletonPic);
  imshow("3", contourPic);
  waitKey(0);
#endif
  //寻找端点坐标
  vector<Point> endPoint;
  for (int i = 1; i < skeletonPic.rows - 1; i++)
  {
    for (int j = 1; j < skeletonPic.cols - 1; j++)
    {
      int count = 0;
      read8Point(i, j);

      if (int(temp_skeleton[0]) != 255)
      {
        continue;
      }
      for (int a = 1; a < 9; a++)
      {
        if (int(temp_skeleton[a]) == 255)
        {
          count += 1;
        }
      }
      if (count == 1)
      {
        endPoint.push_back(Point(j, i));
      }
    }
  }
  if (endPoint.size() < 2)
  {
    if (endPoint.size() == 1)
    {
      Point p = endPoint.front();
      skeletonPic.at<uchar>(p.y, p.x) = uchar(0);
      endPoint.pop_back();
    }

    endPoint.push_back(startPoint[0]);
    endPoint.push_back(startPoint[1]);
  }
  LOG(INFO) << "endPoint.size():" << endPoint.size();
  //   LOG(INFO)<<"endPoint.size()"<<endPoint.size();

  //遍历每一条直线，并将周围的点按照顺序聚类到这条线上
  Point tmpPoint, tmpPoint2, seleEndPoint;
  Point fir_lasPoint[2];
  std::vector<queue<Point2f>> result_points;
  result_points.resize(endPoint.size());
  for (std::size_t i = 0; i < result_points.size(); i++)
  {
    tmpPoint = endPoint.back();
    endPoint.pop_back();
    fir_lasPoint[0] = tmpPoint;

    //     LOG(INFO)<<"tmpPoint:"<<tmpPoint.x<<","<<tmpPoint.y;
    skeletonPic.at<uchar>(tmpPoint.y, tmpPoint.x) = uchar(0);
    tmpPoint2 = tmpPoint;
    int count = 0;
    //     int z = 0;
    while (count < 9)
    {
      read8Point(tmpPoint.y, tmpPoint.x);
      for (int j = 1; j < 9; j++)
      {
        if (int(temp_skeleton[j]) == 255)
        {
          //           LOG(INFO)<<skeleton_p[j].x<<","<<skeleton_p[j].y;
          lenth_line++;
          tmpPoint2 = tmpPoint;
          Point temp_p1 = skeleton_p[j];
          fir_lasPoint[1] = skeleton_p[j];
          //           LOG(INFO)<<"fir_lasPoint[0]"<<fir_lasPoint[0].x<<"
          //           "<<fir_lasPoint[0].y;
          //           LOG(INFO)<<"fir_lasPoint[1]"<<fir_lasPoint[1].x<<"
          //           "<<fir_lasPoint[1].y;
          tmpPoint = temp_p1;
          ;
          if (tmpPoint.x - tmpPoint2.x == 0)
          {
            read3Point_y(temp_p1.y, temp_p1.x);
          }
          else if (tmpPoint.y - tmpPoint2.y == 0)
          {
            read3Point_x(temp_p1.y, temp_p1.x);
          }

          //           read5Point(temp_p1.y,temp_p1.x);
          //使得单向移动
          skeletonPic.at<uchar>(temp_p1.y, temp_p1.x) = uchar(0);
          for (int k = 0; k < 3; k++)
          {
            if (int(temp_target[k]) == 128)
            {
              Point temp_p2 = target_p[k];
              //               result_points[i].push(temp_p2);
              //               int a = temp_p2.x;
              //               int b = temp_p2.y;
              for (std::size_t l = 0; l < pointmaping[temp_p2.y][temp_p2.x].size(); l++)
              {
                Point2f test;
                test = pointmaping[temp_p2.y][temp_p2.x].back();
                result_points[i].push(pointmaping[temp_p2.y][temp_p2.x].back());
                pointmaping[temp_p2.y][temp_p2.x].pop_back();
              }
              count = 0;
            }
          }
          break;
        }
        else
        {
          count += 1;
        }
      }
    }
    // fit the line
    double dis_mine, distance;
    //     LOG(INFO)<<"-------------";
    for (int l = 0; l < 2; l++)
    {
      //         LOG(INFO)<<fir_lasPoint[l].x<<" "<<fir_lasPoint[l].y;
      dis_mine = line_img_.rows;

      for (std::size_t n = 0; n < pro_point.size(); n++)
      {
        distance =
            abs(sqrt((pro_point[n].x - fir_lasPoint[l].x) * (pro_point[n].x - fir_lasPoint[l].x) +
                     (pro_point[n].y - fir_lasPoint[l].y) * (pro_point[n].y - fir_lasPoint[l].y)));
        if (distance < dis_mine)
        {
          dis_mine = distance;
          //             LOG(INFO)<<"dis-mine:"<<dis_mine;
          seleEndPoint = pro_point[n];
        }
      }
      //         LOG(INFO)<<"seleEndPoint:"<<seleEndPoint.x<<"
      //         "<<seleEndPoint.y;
      //         LOG(INFO)<<"fir_lasPoint:"<<fir_lasPoint[l].x<<"
      //         "<<fir_lasPoint[l].y;
      double k =
          double(fir_lasPoint[l].y - seleEndPoint.y) / double(fir_lasPoint[l].x - seleEndPoint.x);
      //           double b =
      //           double(fir_lasPoint[l].y-double(k*fir_lasPoint[l].x));
      //           LOG(INFO)<<"k:"<<k<<"b:"<<b;
      Point extraPoint;
      int k_max = 0, delta_x = 0, delta_y = 0;
      if (abs(k) < 1)
      {
        k_max = abs(seleEndPoint.x - fir_lasPoint[l].x);
        delta_x = (seleEndPoint.x - fir_lasPoint[l].x) / abs(seleEndPoint.x - fir_lasPoint[l].x);
        delta_y = 0;
      }
      else if (abs(k) >= 1)
      {
        k_max = abs(seleEndPoint.y - fir_lasPoint[l].y);
        //             LOG(INFO)<<"k_max"<<k_max;
        delta_y = (seleEndPoint.y - fir_lasPoint[l].y) / abs(seleEndPoint.y - fir_lasPoint[l].y);
        delta_x = 0;
      }
      for (int m = 0; m < k_max; m++)
      {
        if (dis_mine > 5)
        {
          break;
        }
        extraPoint.x = fir_lasPoint[l].x + delta_x * m;
        extraPoint.y = fir_lasPoint[l].y + delta_y * m;
        //           LOG(INFO)<<"extraPoint:"<<extraPoint.x<<" "<<extraPoint.y;
        //           LOG(INFO)<<extraPoint.x<<" "<<extraPoint.y;
        read5Point(extraPoint.y, extraPoint.x);
        for (int k = 0; k < 5; k++)
        {
          if (int(temp_target[k]) == 128)
          {
            Point temp_p2 = target_p[k];
            //               result_points[i].push(temp_p2);
            //                 int a = temp_p2.x;
            //                 int b = temp_p2.y;
            for (std::size_t l = 0; l < pointmaping[temp_p2.y][temp_p2.x].size(); l++)
            {
              Point2f test;
              test = pointmaping[temp_p2.y][temp_p2.x].back();
              result_points[i].push(pointmaping[temp_p2.y][temp_p2.x].back());
              pointmaping[temp_p2.y][temp_p2.x].pop_back();
            }
          }
        }
      }
    }
    fir_lasPoint[0] = Point(0, 0);
    fir_lasPoint[1] = Point(0, 0);
    dis_mine = line_img_.rows;

    lenth_all.push_back(lenth_line);

    lenth_line = 0;
  }
  // get the rect point of the cluster
  vector<vector<Point>> contours;
  findContours(clusterPic, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
  vector<RotatedRect> box(contours.size());
  LOG(INFO) << startPoint[0].x << " " << startPoint[0].y;
  LOG(INFO) << startPoint[1].x << " " << startPoint[1].y;
  return result_points;
}
void lineFit::savePointsToTxt(std::vector<std::queue<Point2f>> clustered_points)
// void lineFit::savePointsToTxt(std::vector<std::queue<linePoint>>
// clustered_points)
{
  int k = 0;
  for (auto points : clustered_points)
  {
    ;
    if (!points.empty())
    {
      ofstream outFile(map_path_ + "line_clustered_points" + to_string(k++) + ".txt",
                       std::ios::out);

      //     ofstream outFile(map_path_ + "line_clustered_points"+
      //     to_string(k++) + ".txt", std::ios::out);
      while (!points.empty())
      {
        auto point = points.front();
        outFile << point.x << " " << point.y << " " << 0 << endl;
        //         outFile << (max_box_[1]  - point.y) / resolution_ <<" " <<
        //         (max_box_[0]  - point.x) / resolution_ <<" " << 0 <<endl;
        points.pop();
      }
      outFile.close();
    }
  }
}

void lineFit::savePointsToTxt(std::map<int, std::vector<Eigen::Vector3d>> points_in_map)
{
  for (auto points : points_in_map)
  {
    ofstream outFile(map_path_ + "origin_points_in_map" + to_string(points.first) + ".txt",
                     std::ios::out);
    for (auto point : points.second)
    {
      outFile << point(0) << " " << point(1) << " " << point(2) << endl;
    }
    outFile.close();
  }
}

void lineFit::savePointsToTxt(const vector<Eigen::Vector2d> &points)
{
  ofstream outFile(map_path_ + "final_strip_points_in_map.txt", std::ios::out);
  for (auto point : points)
  {
    outFile << point(0) << " " << point(1) << endl;
  }
  outFile.close();
}

void lineFit::getCrossPoint()
{
  Point2f p1, p2, p3, p4;

  p1.x = temp.up_x;
  p1.y = temp.up_y;
  p2.x = temp.down_x;
  p2.y = temp.down_y;
  p3.x = temp2.up_x;
  p3.y = temp2.up_y;
  p4.x = temp2.down_x;
  p4.y = temp2.down_y;

  double k1 = (p1.y - p2.y) / (p1.x - p2.x);
  double k2 = (p3.y - p4.y) / (p3.x - p4.x);
  double b1 = p1.y - k1 * p1.x;
  double b2 = p3.y - k2 * p3.x;

  p_cross.x = (b2 - b1) / (k1 - k2);
  p_cross.y = (b2 * k1 - b1 * k2) / (k1 - k2);

  float dis3;
  dis3 = sqrt((p3.x - p_cross.x) * (p3.x - p_cross.x) + (p3.y - p_cross.y) * (p3.y - p_cross.y));
  //   LOG(INFO)<<"dis1:"<<dis3;
  if (dis3 > 0.2)
  {
    p_cross.x = 0;
    p_cross.y = 0;
  }
  //   Point p11,p22,p33,p44,p55;
  //   p11.x=(max_box_[1]  - temp.up_y) / resolution_;p11.y=(max_box_[0]  -
  //   temp.up_x) / resolution_; p22.x=(max_box_[1]  - temp.down_y) /
  //   resolution_;p22.y=(max_box_[0]  - temp.down_x) / resolution_;
  //   p33.x=(max_box_[1]  - temp2.up_y) / resolution_;p33.y=(max_box_[0]  -
  //   temp2.up_x) / resolution_; p44.x=(max_box_[1]  - temp2.down_y) /
  //   resolution_;p44.y=(max_box_[0]  - temp2.down_x) / resolution_;
  //   p55.x=(max_box_[1]  - p_cross.y) / resolution_;p55.y=(max_box_[0]  -
  //   p_cross.x) / resolution_; LOG(INFO)<<p11.x<<","<<p11.y<<"
  //   "<<p22.x<<","<<p22.y; LOG(INFO)<<p33.x<<","<<p33.y<<"
  //   "<<p44.x<<","<<p44.y; LOG(INFO)<<p55.x<<","<<p55.y; LOG(INFO);
  //   LOG(INFO)<<"p_cross:"<<p_cross.x<<" "<<p_cross.y;
}

int lineFit::func_nc8(int *b)
//端点的连通性检测
{
  int n_odd[4] = {1, 3, 5, 7}; //四邻域
  int i, j, sum, d[10];

  for (i = 0; i <= 9; i++)
  {
    j = i;
    if (i == 9) j = 1;
    if (abs(*(b + j)) == 1)
    {
      d[i] = 1;
    }
    else
    {
      d[i] = 0;
    }
  }
  sum = 0;
  for (i = 0; i < 4; i++)
  {
    j = n_odd[i];
    sum = sum + d[j] - d[j] * d[j + 1] * d[j + 2];
  }
  return (sum);
}

void lineFit::cvHilditchThin(cv::Mat &src, cv::Mat &dst)
{
  if (src.type() != CV_8UC1)
  {
    printf("只能处理二值或灰度图像\n");
    return;
  }
  //非原地操作时候，copy src到dst
  if (dst.data != src.data)
  {
    src.copyTo(dst);
  }

  // 8邻域的偏移量
  int offset[9][2] = {{0, 0}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}};
  //四邻域的偏移量
  int n_odd[4] = {1, 3, 5, 7};
  int px, py;
  int b[9];         // 3*3格子的灰度信息
  int condition[6]; // 1-6个条件是否满足
  int counter;      //移去像素的数量
  int i, x, y, copy, sum;

  uchar *img;
  int width, height;
  width = dst.cols;
  height = dst.rows;
  img = dst.data;
  int step = dst.step;
  uchar BLACK = 0, WHITE = 255, GRAY = 128;
  do
  {
    counter = 0;

    for (y = 0; y < height; y++)
    {
      for (x = 0; x < width; x++)
      {
        //前面标记为删除的像素，我们置其相应邻域值为-1
        for (i = 0; i < 9; i++)
        {
          b[i] = 0;
          px = x + offset[i][0];
          py = y + offset[i][1];
          if (px >= 0 && px < width && py >= 0 && py < height)
          {
            // printf("%d\n", img[py*step+px]);
            //                       LOG(INFO)<<"img[py*step+px]"<<img[py*step+px];
            if (img[py * step + px] == WHITE)
            //                       if (img[py*step+px] == Scalar(0))
            {
              b[i] = 1;
            }
            else if (img[py * step + px] == GRAY)
            //                       else if (int(img[py*step+px])  != 0)
            {
              b[i] = -1;
            }
          }
        }
        for (i = 0; i < 6; i++)
        {
          condition[i] = 0;
        }

        //条件1，是前景点
        if (b[0] == 1) condition[0] = 1;

        //条件2，是边界点
        sum = 0;
        for (i = 0; i < 4; i++)
        {
          sum = sum + 1 - abs(b[n_odd[i]]);
        }
        if (sum >= 1) condition[1] = 1;

        //条件3， 端点不能删除
        sum = 0;
        for (i = 1; i <= 8; i++)
        {
          sum = sum + abs(b[i]);
        }
        if (sum >= 2) condition[2] = 1;

        //条件4， 孤立点不能删除
        sum = 0;
        for (i = 1; i <= 8; i++)
        {
          if (b[i] == 1) sum++;
        }
        if (sum >= 1) condition[3] = 1;

        //条件5， 连通性检测
        if (func_nc8(b) == 1) condition[4] = 1;

        //条件6，宽度为2的骨架只能删除1边
        sum = 0;
        for (i = 1; i <= 8; i++)
        {
          if (b[i] != -1)
          {
            sum++;
          }
          else
          {
            copy = b[i];
            b[i] = 0;
            if (func_nc8(b) == 1) sum++;
            b[i] = copy;
          }
        }
        if (sum == 8) condition[5] = 1;

        if (condition[0] && condition[1] && condition[2] && condition[3] && condition[4] &&
            condition[5])
        {
          img[y * step + x] =
              GRAY; //可以删除，置位GRAY，GRAY是删除标记，但该信息对后面像素的判断有用
                    //                     img[y*step+x] = 255;
          counter++;
          // printf("----------------------------------------------\n");
          // PrintMat(dst);
        }
      }
    }

    if (counter != 0)
    {
      for (y = 0; y < height; y++)
      {
        for (x = 0; x < width; x++)
        {
          //                       if (img[y*step+x] != 0)
          if (img[y * step + x] == GRAY) img[y * step + x] = BLACK;
          //                         img[y*step+x] = 255;
        }
      }
    }

  } while (counter != 0);
}

void lineFit::read8Point(int i, int j)
{
  temp_skeleton[0] = skeletonPic.at<uchar>(i, j);
  temp_skeleton[1] = skeletonPic.at<uchar>(i, j - 1);
  temp_skeleton[2] = skeletonPic.at<uchar>(i + 1, j);
  temp_skeleton[3] = skeletonPic.at<uchar>(i, j + 1);
  temp_skeleton[4] = skeletonPic.at<uchar>(i - 1, j);
  temp_skeleton[5] = skeletonPic.at<uchar>(i - 1, j - 1);
  temp_skeleton[6] = skeletonPic.at<uchar>(i + 1, j - 1);
  temp_skeleton[7] = skeletonPic.at<uchar>(i + 1, j + 1);
  temp_skeleton[8] = skeletonPic.at<uchar>(i - 1, j + 1);

  skeleton_p[0] = Point(j, i);
  skeleton_p[1] = Point(j - 1, i);
  skeleton_p[2] = Point(j, i + 1);
  skeleton_p[3] = Point(j + 1, i);
  skeleton_p[4] = Point(j, i - 1);
  skeleton_p[5] = Point(j - 1, i - 1);
  skeleton_p[6] = Point(j - 1, i + 1);
  skeleton_p[7] = Point(j + 1, i + 1);
  skeleton_p[8] = Point(j + 1, i - 1);
}

void lineFit::read5Point(int i, int j)
{
  temp_target[0] = contourPic.at<uchar>(i, j);
  temp_target[1] = contourPic.at<uchar>(i, j - 1);
  temp_target[2] = contourPic.at<uchar>(i - 1, j);
  temp_target[3] = contourPic.at<uchar>(i + 1, j);
  temp_target[4] = contourPic.at<uchar>(i, j + 1);

  target_p[0] = Point(j, i);
  target_p[1] = Point(j - 1, i);
  target_p[2] = Point(j, i - 1);
  target_p[3] = Point(j, i + 1);
  target_p[4] = Point(j + 1, i);
}

void lineFit::read3Point_x(int i, int j)
{
  temp_target[0] = contourPic.at<uchar>(i, j);
  //       temp_target[1] = contourPic.at<uchar>(i,j-1);
  temp_target[1] = contourPic.at<uchar>(i - 1, j);
  temp_target[2] = contourPic.at<uchar>(i + 1, j);
  //       temp_target[4] = contourPic.at<uchar>(i,j+1);

  target_p[0] = Point(j, i);
  //       target_p[1] = Point(j-1,i);
  target_p[1] = Point(j, i - 1);
  target_p[2] = Point(j, i + 1);
  /*      target_p[4] = Point(j+1,i); */
}

void lineFit::read3Point_y(int i, int j)
{
  temp_target[0] = contourPic.at<uchar>(i, j);
  temp_target[1] = contourPic.at<uchar>(i, j - 1);
  //       temp_target[2] = contourPic.at<uchar>(i-1,j);
  //       temp_target[3] = contourPic.at<uchar>(i+1,j);
  temp_target[2] = contourPic.at<uchar>(i, j + 1);

  target_p[0] = Point(j, i);
  target_p[1] = Point(j - 1, i);
  //       target_p[2] = Point(j,i-1);
  //       target_p[3] = Point(j,i+1);
  target_p[2] = Point(j + 1, i);
}
} // namespace map_manager