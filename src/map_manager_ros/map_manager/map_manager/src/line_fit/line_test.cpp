#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/mapping/2d/probability_grid_range_data_inserter_2d.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/submaps.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/timestamped_transform.h>
#include <cartographer/transform/transform.h>
#include <cartographer_ros/node.h>
#include <cartographer_ros/node_options.h>
#include <map_manager_msgs/LinePoints.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>

#include "line_fit/line_fit.h"

using namespace std;
using namespace cartographer;
using namespace cartographer::mapping;
ros::Subscriber strip_detect_result_sub_;
ros::ServiceServer srv_;
string map_path_;
std::map<ros::Time, std::map<int, std::vector<Eigen::Vector3d>>> points_with_stamp_;

void HandleStripDetectResult(const map_manager_msgs::LinePoints::ConstPtr &msg)
{
  std::map<int, std::vector<Eigen::Vector3d>> points;
  for (auto it : msg->left_points_in_base)
  {
    points[0].push_back(Eigen::Vector3d(it.position.x, it.position.y, it.position.z));
  }
  for (auto it : msg->right_points_in_base)
  {
    points[1].push_back(Eigen::Vector3d(it.position.x, it.position.y, it.position.z));
  }
  points_with_stamp_[msg->header.stamp] = points;
}

std::map<ros::Time, cartographer::transform::Rigid3d> readFromPbstream(std::string filename)
{
  std::vector<cartographer::mapping::TrajectoryNode::Data> node_datas;
  mapping::proto::ProbabilityGridRangeDataInserterOptions2D options;
  options.set_hit_probability(0.55);
  options.set_miss_probability(0.49);
  options.set_insert_free_space(true);
  mapping::RangeDataInserterInterface *range_data_inserter =
      new mapping::ProbabilityGridRangeDataInserter2D(options);
  io::ProtoStreamReader stream(filename);
  io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  MapById<NodeId, transform::Rigid3d> node_poses;
  std::map<ros::Time, cartographer::transform::Rigid3d> node_poses_with_times;
  for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
       pose_graph_proto.trajectory())
  {
    node_datas.resize(trajectory_proto.node().size());
    for (const cartographer::mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
      common::Time time = common::FromUniversal(node_proto.timestamp());
      ros::Time ros_time = cartographer_ros::ToRos(time);
      node_poses_with_times[ros_time] = global_pose;
    }
  }
  return node_poses_with_times;
}

bool HandleFinishedPoints(std_srvs::Empty::Request &request, std_srvs::Empty::Response &response)
{
  tf2_ros::Buffer tfBuffer_(ros::Duration(20.));
  tf2_ros::TransformListener tfListener_(tfBuffer_);

  transform::Rigid3d base2tracking;
  geometry_msgs::TransformStamped base2tracking_tf;
  ros::Rate rate(100);

  while (1)
  {
    try
    {
      base2tracking_tf = tfBuffer_.lookupTransform("laser_link", "base_footprint", ros::Time(0));
      Eigen::Vector3d translation(base2tracking_tf.transform.translation.x,
                                  base2tracking_tf.transform.translation.y,
                                  base2tracking_tf.transform.translation.z);

      Eigen::Quaterniond rotation(
          base2tracking_tf.transform.rotation.w, base2tracking_tf.transform.rotation.x,
          base2tracking_tf.transform.rotation.y, base2tracking_tf.transform.rotation.z);
      base2tracking = transform::Rigid3d(translation, rotation);
      break;
    }
    catch (tf::TransformException ex)
    {
      rate.sleep();
      LOG(WARNING) << ex.what();
    }
  }

  std::map<ros::Time, cartographer::transform::Rigid3d> node_poses_with_times =
      readFromPbstream(map_path_ + "map.pbstream");
  std::map<int, std::vector<Eigen::Vector3d>> points_in_map;
  for (auto &points : points_with_stamp_)
  {
    common::Time cur_t = cartographer_ros::FromRos(points.first);
    map<ros::Time, cartographer::transform::Rigid3d>::iterator it1 = node_poses_with_times.begin();
    map<ros::Time, cartographer::transform::Rigid3d>::iterator it2 = node_poses_with_times.begin();
    ++it2;
    for (; it2 != node_poses_with_times.end(); ++it1, ++it2)
    {
      ros::Time t1 = it1->first;
      ros::Time t2 = it2->first;
      common::Time t11 = cartographer_ros::FromRos(t1);
      common::Time t22 = cartographer_ros::FromRos(t2);
      if (cur_t > t11 && cur_t < t22)
      {
        cartographer::transform::Rigid3d transform =
            cartographer::transform::Interpolate(
                cartographer::transform::TimestampedTransform{t11, it1->second},
                cartographer::transform::TimestampedTransform{t22, it2->second}, cur_t)
                .transform;
        for (int i = 0; i < 2; i++)
        {
          for (auto &point : points.second[i])
          {
            points_in_map[i].push_back(transform * base2tracking * point);
          }
        }
        break;
      }
    }
  }

  lineFit lin_fit(map_path_);

  lin_fit.linDraw(points_in_map);
}
// int main(int argc, char** argv)
// {
//   LOG(INFO)<<"1";
//   google::InitGoogleLogging(argv[0]);
//   google::SetStderrLogging(google::GLOG_INFO);
//
//   FLAGS_colorlogtostderr =true;
//   map_path_ = "/home/sail/Desktop/test_line1/";
//   ros::init(argc, argv, "line_test");
//   ros::NodeHandle n;
//   line_detect_result_sub_ = n.subscribe( "/base_point", 1,
//   HandleLineDetectResult); srv_ = n.advertiseService("finish_detect",
//   HandleFinishedPoints); ros::spin(); google::ShutdownGoogleLogging(); return
//   1;
//
// }

int main(int argc, char **argv)
{
  ros::init(argc, argv, "line_test");
  ros::NodeHandle n;
  Eigen::Vector3d point;
  float ch;
  std::map<int, std::vector<Eigen::Vector3d>> points_in_map;
  //   vector<linePoint> points_in_map;
  vector<float> everypoints0;
  vector<float> everypoints1;
  string map_name;
  ::ros::param::get("/line_test/map_name", map_name);
  map_path_ = std::string(getenv("HOME")) + "/map/" + map_name + "/";
  LOG(INFO) << "map path: " << map_path_;
  std::string point_map0 = map_path_ + "origin_points_in_map0.txt";
  std::string point_map1 = map_path_ + "origin_points_in_map1.txt";

  ifstream inf;
  inf.open(point_map0);
  if (inf)
  {
    LOG(INFO) << "point_map0";
    for (int i = 0; !inf.eof(); i++)
    {
      inf >> ch;
      //       LOG(INFO)<<ch;
      everypoints0.push_back(ch);
    }
    inf.close();
  }

  else
  {
    LOG(INFO) << "point_map0.txt open failed!";
    return 0;
  }
  LOG(INFO) << "everypoints0.size()" << everypoints0.size();
  everypoints0.pop_back();
  ifstream inf2;

  inf2.open(point_map1);
  if (inf2)
  {
    LOG(INFO) << "point_map1";
    for (int i = 0; !inf2.eof(); i++)
    {
      inf2 >> ch;
      //       LOG(INFO)<<ch;
      everypoints1.push_back(ch);
    }
    inf2.close();
  }
  else
  {
    LOG(INFO) << "point_map1.txt open failed!";
    return 0;
  }

  for (int j = 0; j < int(everypoints0.size() / 3); j++)
  {
    float x_one = everypoints0[3 * j];
    float y_two = everypoints0[3 * j + 1];

    point[0] = x_one;
    point[1] = y_two;
    point[2] = 0;

    points_in_map[0].push_back(point);
  }

  for (int j = 0; j < int(everypoints1.size() / 3); j++)
  {
    float x_one = everypoints1[3 * j];
    float y_two = everypoints1[3 * j + 1];

    point[0] = x_one;
    point[1] = y_two;
    point[2] = 0;

    points_in_map[1].push_back(point);
  }
  LOG(INFO) << "points_in_map.size" << points_in_map.size();

  map_manager::lineFit lin_fit(map_path_);
  lin_fit.linDraw(points_in_map);
}
