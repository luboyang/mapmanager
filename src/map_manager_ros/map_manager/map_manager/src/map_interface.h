/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPINTERFACE_H
#define MAPINTERFACE_H
#include <absl/synchronization/mutex.h>
#include <cartographer/io/image.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/sensor/range_data.h>
#include <cartographer/transform/rigid_transform.h>
#include <cartographer_ros_msgs/SubmapImages.h>
#include <cartographer_ros_msgs/SubmapList.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Pose.h>
#include <glog/logging.h>
#include <ros/ros.h>
#include <sys/wait.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <vtr_msgs/GetReferenceInfo.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <mutex>
#include <opencv2/opencv.hpp>
#include <string>
#include <thread>
#include <vector>

namespace map_manager
{
class MapInterface
{
  public:
  enum MAP_FORMART
  {
    SHOW_MAP,
    SAVE_MAP,
    PUB_MAP
  };
  struct NodeData
  {
    cartographer::sensor::RangeData range_data;
    Eigen::VectorXf rotational_scan_matcher_histogram_in_gravity;
    Eigen::Quaterniond local_from_gravity_aligned;
    cartographer::transform::Rigid3d global_pose;
  };
  struct ProbabilityGridInfo
  {
    int id;
    cartographer::transform::Rigid3d global_pose;
    cartographer::transform::Rigid3d local_pose;
    int num_range_data;
    bool finished;
    double resolution;
    double max[2];
    int num_cells[2];
  };
  struct LandmarkInfo
  {
    std::string ns;
    std::string id;
    int visible;
    cartographer::transform::Rigid3d pose;
    Eigen::Vector2d translation_in_image;
    float translation_weight;
    float rotation_weight;
    float pole_radius;
  };

  public:
  MapInterface();
  virtual ~MapInterface();
  void getPath(const std::string &map_name, std::string &full_path);
  void createPath(const std::string &path);
  std::string adjustMapPixelSingle(std::string pngDirPath, int size, const bool is_saved_map);
  std::string adjustMapPixel(const std::string &file_path, const int &size,
                             const bool is_saved_map = false);
  cartographer::io::PaintSubmapSlicesResult PaintSubmapsSlices(
      const std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> &submaps,
      const double resolution);
  cartographer::io::PaintSubmapSlicesResult
  PaintSubmapSlice(::cartographer::io::SubmapSlice &submap, const double resolution);

  // public tools
  double GetYaw(const Eigen::Quaterniond &rotation);
  double GetYaw(geometry_msgs::Pose p);
  Eigen::Isometry3d ToIsometry(geometry_msgs::Pose p);
  geometry_msgs::Pose ToGeometryPoseMsg(const double &x, const double &y, const double &yaw);

  geometry_msgs::Point ToGeometryMsgPoint(const Eigen::Vector3d &vector3d);
  geometry_msgs::Pose ToGeometryMsgPose(const cartographer::transform::Rigid3d &rigid3d);
  geometry_msgs::Pose ToGeometryPoseMsg(const geometry_msgs::Transform &transform);
  Eigen::Quaterniond ToEigen(const geometry_msgs::Quaternion &quaternion);
  Eigen::Affine3d ToEigen(const ::cartographer::transform::Rigid3d &rigid3);
  Eigen::Isometry3d ToEigenIsometry3d(const ::cartographer::transform::Rigid3d &rigid3);
  cartographer::transform::Rigid3d ToRigid3d(const geometry_msgs::Pose &pose);
  cv::Mat grey16ToRgba(const cv::Mat &image);
  cv::Mat grey8ToRgba(const cv::Mat &image);
  void bash(std::string cmd);
  // mapping
  const cv::Mat getMatMap(const cartographer::io::PaintSubmapSlicesResult &painted_slices,
                          MAP_FORMART format = SHOW_MAP);
  void interpretateSubmapList(
      const cartographer_ros_msgs::SubmapList::ConstPtr &msg,
      std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> &submap_slices,
      std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry>
          &submap_images,
      ros::ServiceClient *submap_query_client);
  int mergeTransparentImgs(cv::Mat &dst, cv::Mat &scr, double scale);
  Eigen::AlignedBox2d getBox(Eigen::Isometry3d pose, Eigen::Vector2d sides,
                             const double &resolution);
  virtual void setMode(const int &mode){};
  virtual void clear(){};
  bool referenceInfoSrv(::vtr_msgs::GetReferenceInfo::Request &request,
                        ::vtr_msgs::GetReferenceInfo::Response &response);
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  protected:
  ros::NodeHandle nh_;

  private:
  ros::ServiceServer reference_info_srv_;
};

// class ScopedRosLogSink : public ::google::LogSink {
//  public:
//   ScopedRosLogSink();
//   ~ScopedRosLogSink() override;
//
//   void send(::google::LogSeverity severity, const char* filename,
//             const char* base_filename, int line, const struct std::tm*
//             tm_time, const char* message, size_t message_len) override;
//
//   void WaitTillSent() override;
//
//  private:
//   bool will_die_;
// };
} // namespace map_manager
#endif // MAPINTERFACE_H
