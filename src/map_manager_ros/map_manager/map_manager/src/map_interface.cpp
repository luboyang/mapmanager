/*
 * Copyright 2018 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "map_interface.h"

#include <cartographer_ros/submap.h>
#include <cartographer_ros_msgs/ReferenceLocationFromMap.h>
#include <sensor_msgs/PointCloud.h>

#include <cstdlib>

#include "map_manager_ulitity.h"
using namespace std;
using namespace cv;
using namespace cartographer_ros;
using ::cartographer::io::PaintSubmapSlicesResult;
using ::cartographer::io::SubmapSlice;
using ::cartographer::mapping::SubmapId;

namespace map_manager
{

const int kPaddingPixel = 0;
using UniqueCairoSurfacePtr = std::unique_ptr<cairo_surface_t, void (*)(cairo_surface_t *)>;
using UniqueCairoPtr = std::unique_ptr<cairo_t, void (*)(cairo_t *)>;
constexpr cairo_format_t kCairoFormat = CAIRO_FORMAT_ARGB32;

inline UniqueCairoSurfacePtr MakeUniqueCairoSurfacePtr(cairo_surface_t *surface)
{
  return UniqueCairoSurfacePtr(surface, cairo_surface_destroy);
}
inline UniqueCairoPtr MakeUniqueCairoPtr(cairo_t *surface)
{
  return UniqueCairoPtr(surface, cairo_destroy);
}
inline Eigen::Affine3d ToEigen_mapping(const ::cartographer::transform::Rigid3d &rigid3)
{
  return Eigen::Translation3d(rigid3.translation()) * rigid3.rotation();
}

void CairoPaintSubmapSlice(const double scale, const SubmapSlice &submap, cairo_t *cr,
                           std::function<void(const SubmapSlice &)> draw_callback)
{
  cairo_scale(cr, scale, scale);
  const auto &submap_slice = submap;
  if (submap_slice.surface == nullptr)
  {
    return;
  }
  const Eigen::Matrix4d homo = ToEigen_mapping(submap_slice.slice_pose).matrix();

  cairo_save(cr);
  cairo_matrix_t matrix;
  cairo_matrix_init(&matrix, homo(1, 0), homo(0, 0), -homo(1, 1), -homo(0, 1), homo(0, 3),
                    -homo(1, 3));
  cairo_transform(cr, &matrix);

  const double submap_resolution = submap_slice.resolution;
  cairo_scale(cr, submap_resolution, submap_resolution);

  // Invokes caller's callback to utilize slice data in global cooridnate
  // frame. e.g. finds bounding box, paints slices.
  draw_callback(submap_slice);
  cairo_restore(cr);
}
void CairoPaintSubmapsSlices(
    const double scale, const std::map<::cartographer::mapping::SubmapId, SubmapSlice> &submaps,
    cairo_t *cr, std::function<void(const SubmapSlice &)> draw_callback)
{
  cairo_pattern_set_filter(cairo_get_source(cr), CAIRO_FILTER_NEAREST);
  cairo_scale(cr, scale, scale);
  for (auto &pair : submaps)
  {
    const auto &submap_slice = pair.second;
    if (submap_slice.surface == nullptr)
    {
      return;
    }
    const Eigen::Matrix4d homo =
        ToEigen_mapping(submap_slice.pose * submap_slice.slice_pose).matrix();

    cairo_save(cr);
    cairo_matrix_t matrix;
    cairo_matrix_init(&matrix, homo(1, 0), homo(0, 0), -homo(1, 1), -homo(0, 1), homo(0, 3),
                      -homo(1, 3));
    cairo_transform(cr, &matrix);

    const double submap_resolution = submap_slice.resolution;
    cairo_scale(cr, submap_resolution, submap_resolution);

    // Invokes caller's callback to utilize slice data in global cooridnate
    // frame. e.g. finds bounding box, paints slices.
    draw_callback(submap_slice);
    cairo_restore(cr);
  }
}

enum EXTREME_VALUE
{
  MIN,
  MAX
};

inline double getExtremeValue(const vector<double> &values, const EXTREME_VALUE &type)
{
  if (type == MIN)
  {
    double min_value = values[0];
    for (auto &it : values)
    {
      if (min_value > it)
      {
        min_value = it;
      }
    }
    return min_value;
  }
  else
  {
    double max_value = values[0];
    for (auto &it : values)
    {
      if (max_value < it)
      {
        max_value = it;
      }
    }
    return max_value;
  }
}

MapInterface::MapInterface() : tfBuffer_(ros::Duration(20.)), tfListener_(tfBuffer_)
{
  reference_info_srv_ =
      nh_.advertiseService("/reference_info_server", &MapInterface::referenceInfoSrv, this);
}

MapInterface::~MapInterface()
{
  reference_info_srv_.shutdown();
  LOG(WARNING) << "shutdown reference info srv.";
}

void MapInterface::getPath(const string &map_name, string &full_path)
{
  std::string home_path;

  if (const char *env_p = std::getenv("HOME"))
  {
    string temp = "/root";
    home_path = env_p;
    if (home_path.compare(0, 4, temp, 0, 4) == 0)
    {
      LOG(WARNING) << "Origin home path is " << home_path;
      home_path = "/home/jz";
    }
  }
  else
  {
    LOG(WARNING) << "Can not use 'getenv' to get home path!";
    home_path = "/home/jz";
  }
  full_path = home_path + "/map/" + map_name + "/";
}

void MapInterface::createPath(const string &path)
{
  string system_command = "rm -r -f " + path;
  LOG(INFO) << system_command;
  int status = system(system_command.c_str());
  checkSystemStatus(system_command, status);
  assert(status != -1);

  system_command = "mkdir " + path;
  LOG(INFO) << system_command;
  status = system(system_command.c_str());
  checkSystemStatus(system_command, status);
  assert(status != -1);
}

std::string MapInterface::adjustMapPixel(const std::string &file_path, const int &size,
                                         const bool is_saved_map)
{
  if (size != 3 && size != 5 && size != 0)
  {
    return "Nothing will be done! Because flag is not  0, 3 or 5 !";
  }

  boost::property_tree::ptree pt;
  try
  {
    boost::property_tree::read_xml(file_path + "map.xml", pt,
                                   boost::property_tree::xml_parser::trim_whitespace);
  }
  catch (std::exception &e)
  {
  }
  if (pt.empty())
  {
    const std::string error_string = "Load " + file_path + "map.xml failed!";
    ROS_FATAL("%s", error_string.c_str());
    return error_string;
  }

  int node_count = pt.get<int>("map.property.node_count");
  std::string submap_filepath = file_path + "frames/";

  for (int i = 0; i < node_count; i++)
  {
    auto err = adjustMapPixelSingle(submap_filepath + std::to_string(i), size, is_saved_map);
    if (!err.empty()) return err;
  }
  if (is_saved_map) return "";
  pt.put("map.property.pixel_size", size);
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(file_path + "map.xml", pt, std::locale(), setting);
  nh_.setParam("mapPixelSize", size);
  //   if (!is_mapping)
  //   {
  //     createMapMergerFromDisk(file_path);
  //     compressedMapImage_ = createCompressedMapImage(map_merger_);
  //   }
  return "";
}

string MapInterface::adjustMapPixelSingle(string pngDirPath, int size, const bool is_saved_map)
{
  auto png_filepath = pngDirPath;
  std::vector<int> params;
  params.push_back(CV_IMWRITE_PNG_COMPRESSION);
  params.push_back(0);

  std::string origin_path = png_filepath + "/modified_images/erode_0_pixels/";
  Mat tmp_mat = imread(origin_path + "/probability_grid.png", CV_LOAD_IMAGE_UNCHANGED);
  if (tmp_mat.data == nullptr)
  {
    bash("mkdir -p " + origin_path);
    bash("cp " + png_filepath + "/probability_grid.png" + " " + origin_path +
         "/probability_grid_16bit.png");
    Mat tmp_mat16 = imread(origin_path + "/probability_grid_16bit.png", CV_LOAD_IMAGE_UNCHANGED);
    Mat map_adjusted = image16ToImage8(tmp_mat16);
    imwrite(origin_path + "probability_grid_8bit.png", map_adjusted, params);
  }
  if (size == 0)
  {
    bash("cp " + origin_path + "/probability_grid_16bit.png" + " " + png_filepath +
         "/probability_grid.png");
    return "";
  }
  Mat cellMat = imread(origin_path + "/probability_grid_16bit.png", CV_LOAD_IMAGE_UNCHANGED);
  if (cellMat.data == nullptr)
  {
    return "Load " + origin_path + "/probability_grid_16bit.png failed!";
  }

  Mat org1 = cellMat.clone();
  std::vector<cv::Point> zero_points;
  for (int i = 0; i < org1.rows; i++)
    for (int j = 0; j < org1.cols; j++)
    {
      int value = org1.at<uint16_t>(i, j);
      if (value == 0)
      {
        zero_points.push_back(cv::Point(i, j));
        org1.at<uint16_t>(i, j) = 255 * 128;
      }
    }
  Mat result(cellMat.cols, cellMat.rows, CV_16UC1);

  Mat kernel = getStructuringElement(cv::MORPH_RECT, Size(size, size));
  erode(org1, result, kernel);
  GaussianBlur(result, result, Size(size, size), 1);

  for (auto &pt : zero_points)
    result.at<uint16_t>(pt.x, pt.y) = 0;

  if (is_saved_map)
  {
    std::string tmp_path =
        png_filepath + "/modified_images/erode_" + std::to_string(size) + "_pixels/";
    bash("mkdir -p " + tmp_path);
    imwrite(tmp_path + "probability_grid_16bit.png", result, params);

    Mat map_adjusted = image16ToImage8(result);
    imwrite(tmp_path + "probability_grid_8bit.png", map_adjusted, params);
  }
  else
    imwrite(png_filepath + "/probability_grid.png", result, params);
  return "";
}

PaintSubmapSlicesResult MapInterface::PaintSubmapSlice(SubmapSlice &submap, const double resolution)
{
  Eigen::AlignedBox2f bounding_box;
  {
    auto surface = MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, 1, 1));
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    const auto update_bounding_box = [&bounding_box, &cr](double x, double y)
    {
      cairo_user_to_device(cr.get(), &x, &y);
      bounding_box.extend(Eigen::Vector2f(x, y));
    };

    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&update_bounding_box](const SubmapSlice &submap_slice)
                          {
                            update_bounding_box(0, 0);
                            update_bounding_box(submap_slice.width, 0);
                            update_bounding_box(0, submap_slice.height);
                            update_bounding_box(submap_slice.width, submap_slice.height);
                          });
  }

  const Eigen::Array2i size(std::ceil(bounding_box.sizes().x()) + 2 * kPaddingPixel,
                            std::ceil(bounding_box.sizes().y()) + 2 * kPaddingPixel);
  const Eigen::Array2f origin(-bounding_box.min().x() + kPaddingPixel,
                              -bounding_box.min().y() + kPaddingPixel);

  auto surface =
      MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, size.x(), size.y()));
  {
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    cairo_set_source_rgba(cr.get(), 0.5, 0.0, 0.0, 1.);
    cairo_paint(cr.get());
    cairo_translate(cr.get(), origin.x(), origin.y());
    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&cr](const SubmapSlice &submap_slice)
                          {
                            cairo_set_source_surface(cr.get(), submap_slice.surface.get(), 0., 0.);
                            //                              cairo_set_antialias
                            //                              (cr.get(),
                            //                              CAIRO_ANTIALIAS_NONE);
                            //                              cairo_pattern_set_filter(cairo_get_source(cr.get()),CAIRO_FILTER_NEAREST);
                            cairo_paint(cr.get());
                          });
    cairo_surface_flush(surface.get());
  }
  return PaintSubmapSlicesResult(std::move(surface), origin);
}
PaintSubmapSlicesResult MapInterface::PaintSubmapsSlices(
    const std::map<::cartographer::mapping::SubmapId, SubmapSlice> &submaps,
    const double resolution)
{
  Eigen::AlignedBox2f bounding_box;
  {
    auto surface = MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, 1, 1));
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));

    const auto update_bounding_box = [&bounding_box, &cr](double x, double y)
    {
      cairo_user_to_device(cr.get(), &x, &y);
      bounding_box.extend(Eigen::Vector2f(x, y));
    };

    CairoPaintSubmapsSlices(1. / resolution, submaps, cr.get(),
                            [&update_bounding_box](const SubmapSlice &submap_slice)
                            {
                              update_bounding_box(0, 0);
                              update_bounding_box(submap_slice.width, 0);
                              update_bounding_box(0, submap_slice.height);
                              update_bounding_box(submap_slice.width, submap_slice.height);
                            });
  }

  const Eigen::Array2i size(std::ceil(bounding_box.sizes().x()) + 2 * kPaddingPixel,
                            std::ceil(bounding_box.sizes().y()) + 2 * kPaddingPixel);
  const Eigen::Array2f origin(-bounding_box.min().x() + kPaddingPixel,
                              -bounding_box.min().y() + kPaddingPixel);

  auto surface =
      MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, size.x(), size.y()));

  {
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    cairo_set_source_rgba(cr.get(), 0.5, 0.0, 0.0, 1.);
    cairo_paint(cr.get());
    cairo_translate(cr.get(), origin.x(), origin.y());
    CairoPaintSubmapsSlices(1. / resolution, submaps, cr.get(),
                            [&cr](const SubmapSlice &submap_slice)
                            {
                              cairo_set_source_surface(cr.get(), submap_slice.surface.get(), 0.,
                                                       0.);
                              //                              cairo_set_antialias
                              //                              (cr.get(),
                              //                              CAIRO_ANTIALIAS_NONE);
                              //                              cairo_pattern_set_filter(cairo_get_source(cr.get()),CAIRO_FILTER_NEAREST);
                              cairo_paint(cr.get());
                            });
    cairo_surface_flush(surface.get());
  }
  return PaintSubmapSlicesResult(std::move(surface), origin);
}

double MapInterface::GetYaw(const Eigen::Quaterniond &rotation)
{
  const Eigen::Matrix<double, 3, 1> direction = rotation * Eigen::Matrix<double, 3, 1>::UnitX();
  return atan2(direction.y(), direction.x());
}

double MapInterface::GetYaw(geometry_msgs::Pose p)
{
  Eigen::Isometry3d pose;
  Eigen::Quaterniond q =
      Eigen::Quaterniond(p.orientation.w, p.orientation.x, p.orientation.y, p.orientation.z);
  double yaw = GetYaw(q);
  return yaw;
}

Eigen::Isometry3d MapInterface::ToIsometry(geometry_msgs::Pose p)
{
  Eigen::Isometry3d pose;
  Eigen::Quaterniond q =
      Eigen::Quaterniond(p.orientation.w, p.orientation.x, p.orientation.y, p.orientation.z);
  Eigen::Matrix3d rotMat = q.matrix();
  pose.setIdentity();
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      pose(i, j) = rotMat(i, j);
  pose(0, 3) = p.position.x;
  pose(1, 3) = p.position.y;
  pose(2, 3) = p.position.z;
  return pose;
}

geometry_msgs::Pose MapInterface::ToGeometryPoseMsg(const double &x, const double &y,
                                                    const double &yaw)
{
  geometry_msgs::Pose pose;
  pose.position.x = x;
  pose.position.y = y;
  pose.position.z = 0;
  pose.orientation.w = cos(yaw / 2);
  pose.orientation.x = 0;
  pose.orientation.y = 0;
  pose.orientation.z = sin(yaw / 2);
  return pose;
}

geometry_msgs::Pose MapInterface::ToGeometryPoseMsg(const geometry_msgs::Transform &transform)
{
  geometry_msgs::Pose pose;
  pose.position.x = transform.translation.x;
  pose.position.y = transform.translation.y;
  pose.position.z = transform.translation.z;

  pose.orientation.x = transform.rotation.x;
  pose.orientation.y = transform.rotation.y;
  pose.orientation.z = transform.rotation.z;
  pose.orientation.w = transform.rotation.w;
  return pose;
}

geometry_msgs::Point MapInterface::ToGeometryMsgPoint(const Eigen::Vector3d &vector3d)
{
  geometry_msgs::Point point;
  point.x = vector3d.x();
  point.y = vector3d.y();
  point.z = vector3d.z();
  return point;
}

geometry_msgs::Pose MapInterface::ToGeometryMsgPose(const cartographer::transform::Rigid3d &rigid3d)
{
  geometry_msgs::Pose pose;
  pose.position = ToGeometryMsgPoint(rigid3d.translation());
  pose.orientation.w = rigid3d.rotation().w();
  pose.orientation.x = rigid3d.rotation().x();
  pose.orientation.y = rigid3d.rotation().y();
  pose.orientation.z = rigid3d.rotation().z();
  return pose;
}

Eigen::Quaterniond MapInterface::ToEigen(const geometry_msgs::Quaternion &quaternion)
{
  return Eigen::Quaterniond(quaternion.w, quaternion.x, quaternion.y, quaternion.z);
}
Eigen::Affine3d MapInterface::ToEigen(const ::cartographer::transform::Rigid3d &rigid3)
{
  return Eigen::Translation3d(rigid3.translation()) * rigid3.rotation();
}

Eigen::Isometry3d MapInterface::ToEigenIsometry3d(const cartographer::transform::Rigid3d &rigid3)
{
  return Eigen::Translation3d(rigid3.translation()) * rigid3.rotation();
  // 	Eigen::Isometry3d pose;
  // 	Eigen::Quaterniond q = Eigen::Quaterniond(p.orientation.w,
  // p.orientation.x,p.orientation.y, p.orientation.z);
  //   Eigen::Matrix3d rotMat = q.matrix();
  //   pose.setIdentity();
  //   for (int i = 0; i < 3; i++)
  //     for (int j = 0; j < 3; j++)
  //       pose(i, j) = rotMat(i, j);
  //   pose(0, 3) = p.position.x;
  //   pose(1, 3) = p.position.y;
  //   pose(2, 3) = p.position.y;
  // 	return pose;
}

cartographer::transform::Rigid3d MapInterface::ToRigid3d(const geometry_msgs::Pose &pose)
{
  return cartographer::transform::Rigid3d({pose.position.x, pose.position.y, pose.position.z},
                                          ToEigen(pose.orientation));
}
void MapInterface::bash(std::string cmd)
{
  LOG(INFO) << cmd;
  auto status = system(cmd.c_str());
  checkSystemStatus(cmd, status);
  assert(status != -1);
}

Mat MapInterface::grey16ToRgba(const Mat &image)
{
  Mat map_adjusted = Mat::zeros(image.rows, image.cols, CV_8UC4);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uint16_t>(i, j);
      if (value == 0)
      {
        map_adjusted.at<Vec4b>(i, j)[0] = 128;
        map_adjusted.at<Vec4b>(i, j)[1] = 128;
        map_adjusted.at<Vec4b>(i, j)[2] = 128;
        map_adjusted.at<Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value /
        // 128;
        map_adjusted.at<Vec4b>(i, j)[0] = value / 128.;
        map_adjusted.at<Vec4b>(i, j)[1] = value / 128.;
        map_adjusted.at<Vec4b>(i, j)[2] = value / 128.;

        const int delta = 128 - value / 128;
        // 				const int delta = 128 - int(value /
        // 256);
        if (delta < 0)
        {
          map_adjusted.at<Vec4b>(i, j)[0] = 255;
          map_adjusted.at<Vec4b>(i, j)[1] = 255;
          map_adjusted.at<Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta :
        // 0; 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        map_adjusted.at<Vec4b>(i, j)[3] = 255 * sqrt(sqrt(alpha / 128.));
      }
    }
  }
  return map_adjusted;
}

Mat MapInterface::grey8ToRgba(const Mat &image)
{
  Mat map_adjusted = Mat::zeros(image.rows, image.cols, CV_8UC4);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uchar>(i, j);
      if (value == 128)
      {
        map_adjusted.at<Vec4b>(i, j)[0] = 128;
        map_adjusted.at<Vec4b>(i, j)[1] = 128;
        map_adjusted.at<Vec4b>(i, j)[2] = 128;
        map_adjusted.at<Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value /
        // 128;
        map_adjusted.at<Vec4b>(i, j)[0] = value;
        map_adjusted.at<Vec4b>(i, j)[1] = value;
        map_adjusted.at<Vec4b>(i, j)[2] = value;

        const int delta = 128 - value;
        // 				const int delta = 128 - int(value /
        // 256);
        if (delta < 0)
        {
          map_adjusted.at<Vec4b>(i, j)[0] = 255;
          map_adjusted.at<Vec4b>(i, j)[1] = 255;
          map_adjusted.at<Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta :
        // 0; 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        map_adjusted.at<Vec4b>(i, j)[3] = 255 * sqrt(sqrt(alpha / 128.));
      }
    }
  }
  return map_adjusted;
}

// mapping
using namespace cartographer::io;
const Mat MapInterface::getMatMap(const PaintSubmapSlicesResult &painted_slices, MAP_FORMART format)
{
  const int width = cairo_image_surface_get_width(painted_slices.surface.get());
  const int height = cairo_image_surface_get_height(painted_slices.surface.get());
  const uint32_t *pixel_data =
      reinterpret_cast<uint32_t *>(cairo_image_surface_get_data(painted_slices.surface.get()));
  cv::Mat pub_img(height, width, CV_8UC4);
  cv::Mat show_img(height, width, CV_8UC4);
  cv::Mat save_img(height, width, CV_16UC1);
  for (int y = height - 1; y >= 0; --y)
  {
    for (int x = 0; x < width; ++x)
    {
      const uint32_t packed = pixel_data[y * width + x];
      const unsigned char color = packed >> 16;
      const unsigned char observed = packed >> 8;
      const int delta = 128 - int(color);
      // 			const unsigned char alpha = delta > 0 ? delta :
      // -delta; 			LOG(INFO) << int(alpha) <<"  "; 			LOG(INFO) << int(color) ;
      const int value =
          observed == 0 ? -1 : ::cartographer::common::RoundToInt((1. - color / 255.) * 100.);
      CHECK_LE(-1, value);
      CHECK_GE(100, value);
      // 			if(observed!=0)
      // 			if (color == 128)
      // 				img.at<uint16_t>(y, x) = 0;
      // 			else
      if (format == PUB_MAP)
      {
        if (color == 128)
        {
          pub_img.at<Vec4b>(y, x)[0] = 128;
          pub_img.at<Vec4b>(y, x)[1] = 128;
          pub_img.at<Vec4b>(y, x)[2] = 128;
          pub_img.at<Vec4b>(y, x)[3] = 0;
        }
        else
        {
          pub_img.at<Vec4b>(y, x)[0] = color;
          pub_img.at<Vec4b>(y, x)[1] = color;
          pub_img.at<Vec4b>(y, x)[2] = color;
          // 					pub_img.at<Vec4b>(y, x)[3] =
          // alpha; 				const int delta = 128 - int(value / 256);
          if (delta < 0)
          {
            pub_img.at<Vec4b>(y, x)[0] = 255;
            pub_img.at<Vec4b>(y, x)[1] = 255;
            pub_img.at<Vec4b>(y, x)[2] = 255;
          }
          // 					const unsigned char alpha1 = delta > 0 ? delta :
          // -delta; 				const unsigned char value1 = delta > 0 ? delta : 0;
          const double alpha1 = delta > 0 ? delta : -delta;
          pub_img.at<Vec4b>(y, x)[3] = 255 * sqrt(sqrt(alpha1 / 128.));
          // 					pub_img.at<Vec4b>(y, x)[3] = -255*
          // log(1-alpha/128)
        }
      }
      else if (format == SAVE_MAP)
      {
        if (color == 128)
          save_img.at<uint16_t>(y, x) = 0;
        else
          save_img.at<uint16_t>(y, x) = color * 128;
      }
      else
      {
        show_img.at<Vec4b>(y, x)[0] = color;
        show_img.at<Vec4b>(y, x)[1] = color;
        show_img.at<Vec4b>(y, x)[2] = color;
        show_img.at<Vec4b>(y, x)[3] = 255;
        // 				show_img.at<uint8_t>(y, x) = color;
      }
    }
  }
  if (format == PUB_MAP)
  {
    Mat pub_img_flip;
    transpose(pub_img, pub_img_flip);
    cv::flip(pub_img_flip, pub_img_flip, 0);
    return pub_img_flip.clone();
  }
  else if (format == SAVE_MAP)
  {
    Mat save_img_flip;
    transpose(save_img, save_img_flip);
    cv::flip(save_img_flip, save_img_flip, 0);
    return save_img_flip.clone();
  }
  else
  {
    Mat show_img_flip;
    transpose(show_img, show_img_flip);
    cv::flip(show_img_flip, show_img_flip, 0);
    return show_img_flip.clone();
  }
}
void MapInterface::interpretateSubmapList(
    const cartographer_ros_msgs::SubmapList::ConstPtr &msg,
    map<cartographer::mapping::SubmapId, SubmapSlice> &submap_slices,
    std::map<cartographer::mapping::SubmapId, cartographer_ros_msgs::SubmapImageEntry>
        &submap_images,
    ros::ServiceClient *submap_query_client)
{
  std::set<cartographer::mapping::SubmapId> submap_ids_to_delete;
  for (const auto &pair : submap_slices)
  {
    submap_ids_to_delete.insert(pair.first);
  }
  unsigned int i = 0;
  for (const auto &submap_msg : msg->submap)
  {
    const cartographer::mapping::SubmapId id{submap_msg.trajectory_id, submap_msg.submap_index};
    submap_ids_to_delete.erase(id);

    if (submap_images.find(id) == submap_images.end())
    {
      cartographer_ros_msgs::SubmapImageEntry submap_image;
      submap_image.trajectory_id = submap_msg.trajectory_id;
      submap_image.submap_index = submap_msg.submap_index;
      submap_images[id] = submap_image;
    }

    i++;
    SubmapSlice &submap_slice = submap_slices[id];
    submap_slice.pose = ToRigid3d(submap_msg.pose);
    submap_slice.metadata_version = submap_msg.submap_version;
    // 		cartographer_ros_msgs::SubmapImageEntry& submap_pair=
    // submap_images_msg_.submap_images[i]; 		cartographer::transform::Rigid3d
    // pose_in_map = submap_slice.pose*submap_slice.slice_pose;
    // 		submap_pair.pose_in_map = ToGeometryMsgPose(pose_in_map);
    // 		submap_pair.resolution = submap_slice.resolution;
    if (submap_slice.surface != nullptr && submap_slice.version == submap_msg.submap_version)
    {
      continue;
    }
    // 		string str = submap_query_client_.getService();
    auto fetched_textures = cartographer_ros::FetchSubmapTextures(id, submap_query_client);
    if (fetched_textures == nullptr)
    {
      continue;
    }
    CHECK(!fetched_textures->textures.empty());
    submap_slice.version = fetched_textures->version;

    // We use the first texture only. By convention this is the highest
    // resolution texture and that is the one we want to use to construct the
    // map for ROS.
    const auto fetched_texture = fetched_textures->textures.begin();
    submap_slice.width = fetched_texture->width;
    submap_slice.height = fetched_texture->height;
    submap_slice.slice_pose = fetched_texture->slice_pose;
    submap_slice.resolution = fetched_texture->resolution;
    submap_slice.cairo_data.clear();

    submap_slice.surface = ::cartographer::io::DrawTexture(
        fetched_texture->pixels.intensity, fetched_texture->pixels.alpha, fetched_texture->width,
        fetched_texture->height, &submap_slice.cairo_data);
  }

  // Delete all submaps that didn't appear in the message.
  for (const auto &id : submap_ids_to_delete)
  {
    submap_slices.erase(id);
    submap_images.erase(id);
  }
}

int MapInterface::mergeTransparentImgs(cv::Mat &dst, cv::Mat &scr, double scale)
{
  if (dst.channels() != 3 || scr.channels() != 4)
  {
    return true;
  }
  if (scale < 0.01) return false;
  std::vector<cv::Mat> scr_channels;
  std::vector<cv::Mat> dstt_channels;
  split(scr, scr_channels);
  split(dst, dstt_channels);
  CV_Assert(scr_channels.size() == 4 && dstt_channels.size() == 3);

  if (scale < 1)
  {
    scr_channels[3] *= scale;
    scale = 1;
  }
  for (int i = 0; i < 3; i++)
  {
    dstt_channels[i] = dstt_channels[i].mul(255.0 / scale - scr_channels[3], scale / 255.0);
    dstt_channels[i] += scr_channels[i].mul(scr_channels[3], scale / 255.0);
  }
  merge(dstt_channels, dst);
  return true;
}

Eigen::AlignedBox2d MapInterface::getBox(Eigen::Isometry3d pose, Eigen::Vector2d sides,
                                         const double &resolution)
{
  Eigen::AlignedBox2d box;
  double width, heigth;
  width = -sides[0] * 1.0;
  heigth = -sides[1] * 1.0;
  double inv_resolution = 1 / resolution;
  pose(0, 3) *= inv_resolution * 1.0;
  ;
  pose(1, 3) *= inv_resolution * 1.0;
  ;

  Eigen::Vector3d point0, point1, point2, point3;
  /*
 0        1
 ---------
 |       |
 |       |
 ---------
 2        3
 */
  point0 = Eigen::Vector3d(0, 0, 0);
  point1 = Eigen::Vector3d(0, width, 0);
  point2 = Eigen::Vector3d(heigth, 0, 0);
  point3 = Eigen::Vector3d(heigth, width, 0);

  point0 = pose * point0;
  point1 = pose * point1;
  point2 = pose * point2;
  point3 = pose * point3;

  vector<double> set_x, set_y;
  set_x.push_back(point0(0));
  set_x.push_back(point1(0));
  set_x.push_back(point2(0));
  set_x.push_back(point3(0));
  set_y.push_back(point0(1));
  set_y.push_back(point1(1));
  set_y.push_back(point2(1));
  set_y.push_back(point3(1));
  double min_x, min_y, max_x, max_y;
  min_x = getExtremeValue(set_y, MIN);
  min_y = getExtremeValue(set_x, MIN);
  max_x = getExtremeValue(set_y, MAX);
  max_y = getExtremeValue(set_x, MAX);

  box = Eigen::AlignedBox2d(Eigen::Vector2d(min_x, min_y), Eigen::Vector2d(max_x, max_y));
  return box;
}

bool MapInterface::referenceInfoSrv(vtr_msgs::GetReferenceInfo::Request &request,
                                    vtr_msgs::GetReferenceInfo::Response &response)
{
  LOG(INFO) << "Getting reference info!";
  geometry_msgs::Pose fm_base2map, fm_base2ref;
  int fm_ref_id;

  string srv_type = request.json_string;
  if (srv_type == "fm_mapping")
  {
    std::string result_string = "";

    ros::ServiceClient client = nh_.serviceClient<cartographer_ros_msgs::ReferenceLocationFromMap>(
        "/fm_odometry/reference_pose_server");
    cartographer_ros_msgs::ReferenceLocationFromMap query;

    if (client.call(query))
    {
      result_string = query.response.error_message;
      if (result_string != "")
      {
        response.error_message = result_string;
        return true;
      }
    }
    else
    {
      response.error_message = result_string;
      return true;
    }

    fm_base2map = query.response.worldPose[0];
    fm_base2ref = query.response.referencePose[0];
    fm_ref_id = query.response.reference_id[0];

    LOG(INFO) << "Fm Teaching Succeed!";
  }
  else if (srv_type == "fm_localization" || srv_type == "laser_localization")
  {
    string prefix = "fm_";
    string srv_prefix = "/fm_localiser/";
    if (srv_type != "fm_localization")
    {
      prefix = "";
      srv_prefix = "/vtr/";
    }
    LOG(INFO) << "srv_type: " << srv_type << "\tprefix: " << prefix;
    geometry_msgs::TransformStamped base2map_tf, base2ref_tf;
    try
    {
      base2map_tf =
          tfBuffer_.lookupTransform(prefix + "map", prefix + "base_footprint", ros::Time(0));
      base2ref_tf = tfBuffer_.lookupTransform(prefix + "reference_frame", prefix + "base_footprint",
                                              ros::Time(0));
      fm_base2map = ToGeometryPoseMsg(base2map_tf.transform);
      fm_base2ref = ToGeometryPoseMsg(base2ref_tf.transform);
    }
    catch (tf::TransformException ex)
    {
      response.error_message = "Can not get tf!";
      LOG(WARNING) << response.error_message;
      return true;
    }
    ros::ServiceClient client =
        nh_.serviceClient<vtr_msgs::GetReferenceInfo>(srv_prefix + "get_reference_id_server");
    vtr_msgs::GetReferenceInfo query;
    fm_ref_id = -1;
    if (client.call(query))
    {
      fm_ref_id = query.response.reference_id;
    }
  }
  else
  {
    response.error_message = "type error!";
    LOG(WARNING) << response.error_message;
    return true;
  }

  response.base2map = fm_base2map;
  response.base2reference = fm_base2ref;
  response.reference_id = fm_ref_id;
  if (fm_ref_id < 0)
  {
    response.error_message = "reference_id error!";
    LOG(WARNING) << response.error_message;
  }
  else
    response.error_message = "";
  return true;
}

} // namespace map_manager
// const char* GetBasename(const char* filepath) {
//   const char* base = std::strrchr(filepath, '/');
//   return base ? (base + 1) : filepath;
// }

// ScopedRosLogSink::ScopedRosLogSink() : will_die_(false) { AddLogSink(this); }
// ScopedRosLogSink::~ScopedRosLogSink() { RemoveLogSink(this); }
//
// void ScopedRosLogSink::send(const ::google::LogSeverity severity,
//                             const char* const filename,
//                             const char* const base_filename, const int line,
//                             const struct std::tm* const tm_time,
//                             const char* const message,
//                             const size_t message_len) {
//   const std::string message_string = ::google::LogSink::ToString(
//       severity, GetBasename(filename), line, tm_time, message, message_len);
//   switch (severity) {
//     case ::google::GLOG_INFO:
//       ROS_INFO_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_WARNING:
//       ROS_WARN_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_ERROR:
//       ROS_ERROR_STREAM(message_string);
//       break;
//
//     case ::google::GLOG_FATAL:
//       ROS_FATAL_STREAM(message_string);
//       will_die_ = true;
//       break;
//   }
// }
//
// void ScopedRosLogSink::WaitTillSent() {
//   if (will_die_) {
//     // Give ROS some time to actually publish our message.
//     std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//   }
// }
