/*
 * Copyright 2023 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef TOROSMSG_H
#define TOROSMSG_H
#include <cartographer_ros_msgs/SubmapImagesServer.h>
#include <cartographer_ros_msgs/LandmarkNewEntry.h>
#include <map_manager_msgs/IdRemap.h>
#include <Eigen/Eigen>
class ToRosMsg
{
public:
  ToRosMsg();
  ~ToRosMsg();
  bool loadSubmaps(
    const std::string& map_path,
    std::vector<cartographer_ros_msgs::SubmapImageEntry>& submap_images,
    std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>>& localInGlobal_poses,
    std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>>& posesInGlobal,
    std::vector<double>& maxes,
    int size_of_map[2]);
  bool loadLandmarks(
    const std::string& map_path,
    std::vector<cartographer_ros_msgs::LandmarkNewEntry>& landmark_list);
  bool updateLandmarks(
    const std::string& map_path, 
    const std::vector<cartographer_ros_msgs::LandmarkNewEntry>& landmark_list);
  bool modifyLandmarksId(
    const std::string& map_path, 
    std::vector<map_manager_msgs::IdRemap>& ids_remap);
private:
  
  
};

#endif // TOROSMSG_H
