/*
 * Copyright 2023 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "to_ros_msg.h"
#include "services/load_submaps_server_service.h"
#include "services/load_landmarks_server_service.h"
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Pose.h>
#include "utils/utils_cv.hpp"
ToRosMsg::ToRosMsg()
{

}

ToRosMsg::~ToRosMsg()
{

}
inline geometry_msgs::Pose ToGeometryPose(const Rigid3d& pose)
{
  geometry_msgs::Pose geo_pose;
  geo_pose.position.x = pose.translation().x();
  geo_pose.position.y = pose.translation().y();
  geo_pose.position.z = pose.translation().z();
  geo_pose.orientation.x = pose.rotation().x();
  geo_pose.orientation.y = pose.rotation().y();
  geo_pose.orientation.z = pose.rotation().z();
  geo_pose.orientation.w = pose.rotation().w();
  return geo_pose;
}
inline cartographer_ros_msgs::SubmapImageEntry ToSubmapEntry(
  const output_data_type::SubmapImageEntry& data)
{
  cartographer_ros_msgs::SubmapImageEntry entry;
  sensor_msgs::CompressedImagePtr msg_ptr =  
    cv_bridge::CvImage(std_msgs::Header(),
                       "bgra8",
                       cvutils::Grey16ToRgba(data.image)).toCompressedImageMsg(cv_bridge::PNG);
  entry.image = *msg_ptr;
  entry.resolution = data.resolution;
  entry.submap_index = data.submap_index;
  entry.submap_version = data.submap_version;
  entry.trajectory_id = data.trajectory_id;
  entry.pose_in_map = ToGeometryPose(data.pose_in_map);
  return entry;
}


bool ToRosMsg::loadSubmaps(
  const std::string& map_path,
  std::vector<cartographer_ros_msgs::SubmapImageEntry>& submap_entries, 
  std::vector< Eigen::Isometry3d, Eigen::aligned_allocator< Eigen::Isometry3d > >& localInGlobal_poses, std::vector< Eigen::Isometry3d, Eigen::aligned_allocator< Eigen::Isometry3d > >& posesInGlobal,
  std::vector< double >& maxes, int size_of_map[2])
{
  std::vector<output_data_type::SubmapImageEntry> submap_images;
  output_data_type::MapPosesInfo map_poses_info;
  if(!LoadSubmapsServerService(map_path).GetSubmapImages(submap_images, map_poses_info).IsOk())
  {
    return false;
  }
  for(const output_data_type::SubmapImageEntry& it : submap_images)
  {
    submap_entries.push_back(ToSubmapEntry(it));
  }
  localInGlobal_poses = map_poses_info.local_in_global_poses;
  posesInGlobal = map_poses_info.poses_in_global;
  maxes = map_poses_info.maxes;
  size_of_map[0] = map_poses_info.size_of_map[0];
  size_of_map[1] = map_poses_info.size_of_map[1];
  return true;
}


bool ToRosMsg::loadLandmarks(
  const std::string& map_path, 
  std::vector< cartographer_ros_msgs::LandmarkNewEntry >& landmark_list)
{
  std::vector<file_data_type::LandmarkInfo> landmark_infos;
  if(!LoadLandmarksService(map_path).LoadLandmarksData(landmark_infos).IsOk())
  {
    return false;
  }
  landmark_list.reserve(landmark_infos.size());
  for(const file_data_type::LandmarkInfo mark_info:landmark_infos)
  {
    if(!mark_info.visible)
      continue;
    cartographer_ros_msgs::LandmarkNewEntry entry;
    entry.ns = mark_info.ns;
    entry.id = mark_info.id;
    entry.pole_radius = mark_info.pole_radius;
    entry.rotation_weight = mark_info.rotation_weight;
    entry.translation_weight = mark_info.translation_weight;
    entry.visible = mark_info.visible;
    entry.tracking_from_landmark_transform = ToGeometryPose(mark_info.transform);
    landmark_list.emplace_back(std::move(entry));
  }
  return true;
}

bool ToRosMsg::updateLandmarks(
  const std::string& map_path, 
  const std::vector< cartographer_ros_msgs::LandmarkNewEntry >& landmark_list)
{
  std::vector<file_data_type::LandmarkInfo> landmark_infos;
  for(const cartographer_ros_msgs::LandmarkNewEntry& entry : landmark_list)
  {
    file_data_type::LandmarkInfo mark;
    mark.ns = entry.ns;
    mark.id = entry.id;
    landmark_infos.push_back(mark);
  }
  return LoadLandmarksService(map_path).UpDateLandmarks(landmark_infos).IsOk();
}

bool ToRosMsg::modifyLandmarksId(
  const std::string& map_path, 
  std::vector< map_manager_msgs::IdRemap >& ids_remap)
{
  std::vector<output_data_type::IdRemap> ids;
  for(const map_manager_msgs::IdRemap id : ids_remap)
  {
    output_data_type::IdRemap remap_id;
    remap_id.ns = id.ns;
    remap_id.origin_id = id.origin_id;
    remap_id.new_id = id.new_id;
    ids.push_back(remap_id);
  }
  return LoadLandmarksService(map_path).ModifyLandmarksId(ids).IsOk();
}








