/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef GHOSTINGDETECTION_H
#define GHOSTINGDETECTION_H
#include <ros/ros.h>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <mutex>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <stack>
#include <vector>

using namespace std;
using namespace cv;
using namespace cv::line_descriptor;
struct Line
{
  double x1;
  double y1;
  double x2;
  double y2;
};

class GhostingDetection
{
  public:
  GhostingDetection();
  ~GhostingDetection();
  cv::Mat ghostDetection(const std::string &path,
                         std::vector<std::pair<Line, Line>> &ghostingLines);

  private:
  double Point_distance(const Point2d &P, const Point2d &A, const Point2d &B);
  bool judge(const Line &l1, const Line &l2);
  cv::Mat SkeletonExtraction(const std::string &path);
  void gammaTransformation(const cv::Mat &matInput, cv::Mat &matOutput, const float &fGamma,
                           const float &fC);
  void ZhangSuen_Thin(const cv::Mat &src, cv::Mat &dst);
};

#endif // GHOSTINGDETECTION_H
