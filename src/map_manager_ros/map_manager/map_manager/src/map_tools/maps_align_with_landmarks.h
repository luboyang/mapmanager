/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAPSALIGNWITHLANDMARKS_H
#define MAPSALIGNWITHLANDMARKS_H

#include <cartographer/io/image.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer/sensor/point_cloud.h>
#include <cartographer/transform/rigid_transform.h>
#include <cartographer/transform/transform.h>

namespace cartographer
{
struct PbstreamInfos
{
  std::vector<cartographer::mapping::TrajectoryNode::Data> node_id_to_node;
  std::vector<cartographer::transform::Rigid3d> node_poses;
  std::vector<cartographer::sensor::FixedFramePoseData> fixed_poses_data;
  std::vector<cartographer::sensor::ImuData> imu_datas;
  std::vector<cartographer::sensor::OdometryData> odom_datas;
  std::vector<cartographer::sensor::LandmarkData> landmark_datas;
  std::vector<cartographer::mapping::PoseGraphInterface::Constraint> constraints;
};
struct PbstreamPosesInfos
{
  mapping::MapById<mapping::NodeId, transform::Rigid3d> node_poses;
  mapping::MapById<mapping::NodeId, mapping::TrajectoryNode::Data> node_datas;
  mapping::MapById<mapping::SubmapId, transform::Rigid3d> submap_poses;
  std::map<std::string, transform::Rigid3d> landmark_poses;
  std::vector<std::pair<int, cartographer::sensor::OdometryData>> odom_datas;
  std::vector<std::pair<int, cartographer::sensor::LandmarkData>> landmark_datas;
  std::vector<mapping::PoseGraphInterface::Constraint> constraints;
};
class MapsAlignWithLandmarks
{

  public:
  MapsAlignWithLandmarks(const std::string &path1, const std::string &path2, bool frozen = true);
  ~MapsAlignWithLandmarks();

  private:
  void readPosesFromPbstream(const std::string &path, const int &trajectory_id,
                             PbstreamPosesInfos &infos, int &cur_trajectory_id);
  void loadLandmarksFromXml(const std::string &fileName, const std::string &con_ns,
                            std::map<std::string, transform::Rigid3d> &pose_list);
  transform::Rigid2d
  getRelativePoseFromLandmarks(const std::map<std::string, transform::Rigid3d> &mark_poses1,
                               const std::map<std::string, transform::Rigid3d> &mark_poses2);
  std::unique_ptr<transform::Rigid3d>
  InterpolateOdometry(const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
                      const int trajectory_id, const common::Time time) const;

  std::unique_ptr<transform::Rigid3d>
  CalculateOdometryBetweenNodes(const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
                                const int trajectory_id,
                                const mapping::optimization::NodeSpec2D &first_node_data,
                                const mapping::optimization::NodeSpec2D &second_node_data) const;

  void Solve(const std::vector<mapping::PoseGraphInterface::Constraint> &constraints,
             //     const std::map<int, PoseGraphInterface::TrajectoryState>&
             //         trajectories_state,
             const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
             const std::map<std::string, mapping::PoseGraphInterface::LandmarkNode> &landmark_nodes,
             mapping::MapById<mapping::NodeId, mapping::optimization::NodeSpec2D> &node_data_,
             mapping::MapById<mapping::SubmapId, mapping::optimization::SubmapSpec2D> &submap_data_,
             std::map<std::string, transform::Rigid3d> &landmark_data_, const bool &frozen);

  void saveCorrectedPbstream(
      const std::string &path, const int &trajectory_id,
      const mapping::MapById<mapping::NodeId, mapping::optimization::NodeSpec2D> &node_data_,
      const mapping::MapById<mapping::SubmapId, mapping::optimization::SubmapSpec2D> &submap_data_,
      const std::map<std::string, transform::Rigid3d> &landmark_data_,
      const std::map<std::string, std::string> &landmarks_id_remap);

  mapping::optimization::proto::OptimizationProblemOptions options_;

  std::map<std::string, transform::Rigid3d> landmarks_in_xml_;
  std::map<std::string, std::string> landmarks_id_remap_;
};
} // namespace cartographer
#endif // MAPSALIGNWITHLANDMARKS_H
