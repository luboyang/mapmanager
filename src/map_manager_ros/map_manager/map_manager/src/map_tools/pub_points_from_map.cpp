/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "pub_points_from_map.h"

#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/io/proto_stream_interface.h>
#include <glog/logging.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <chrono>

#include "map_tools/pub_points_from_map.h"
#include "opencv2/opencv.hpp"
PubPointsFromMap::PubPointsFromMap(const ros::NodeHandle &nh) : nh_(nh)
{
  pub_ = nh_.advertise<sensor_msgs::PointCloud>("map_to_points", 2);
  no_marks_trajectory_pub_ =
      nh_.advertise<sensor_msgs::PointCloud>("no_marks_trajectory_points", 2);
  trajectory_pub_ = nh_.advertise<sensor_msgs::PointCloud>("trajectory_points", 2);
  wall_timer_ = nh_.createWallTimer(::ros::WallDuration(0.2), &PubPointsFromMap::pubTopic, this);
  scan_pub_ = nh_.advertise<sensor_msgs::LaserScan>("cur_scan", 2);

  // getPointsFormMap("/home/cyy/map/2d_cc81/");
}

PubPointsFromMap::~PubPointsFromMap() {}

void PubPointsFromMap::getPoseInMap(const std::string &fileName, const std::string &con_ns,
                                    std::vector<LandmarkPose> &pose_list)
{
  boost::property_tree::ptree pt;
  try
  {
    boost::property_tree::xml_parser::read_xml(fileName, pt);
  }
  catch (std::exception &e)
  {
    LOG(WARNING) << e.what();
    LOG(WARNING) << "Can't find or open landmark.xml file and it will not use "
                    "lasermark. ";
    return;
  }
  LOG(WARNING) << "fileName: " << fileName;
  std::string con_ns2 = con_ns + "_4x4";
  std::string xml, xml_id;
  for (auto &m : pt)
  {
    if (m.first == "landmark")
    {
      std::string ns = m.second.get("ns", " ");
      std::string is_visible_tmp = m.second.get("visible", " ");
      xml_id = m.second.get("id", " ");
      LOG(WARNING) << is_visible_tmp << ": " << xml_id;
      int is_visible = std::stoi(is_visible_tmp);
      if (ns != con_ns && ns != con_ns2) continue;
      xml_id = m.second.get("id", " ");
      xml = m.second.get("transform", " ");
      std::string pole_radius_str = m.second.get("pole_radius", "-1");
      float value[7];
      std::sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
                  &value[4], &value[5], &value[6]);
      LandmarkPose pose1;
      pose1.id = std::stoi(xml_id); // id string 2 int
                                    //       pose1.position =
                                    //       Eigen::Vector2d(value[0],value[1]);
      cartographer::transform::Rigid3f pose({value[0], value[1], value[2]},
                                            {value[6], value[3], value[4], value[5]});
      pose1.pose = pose;
      pose1.pole_radius = std::stof(pole_radius_str);
      pose_list.push_back(pose1);
    }
  }
}

void PubPointsFromMap::loadDataXml(const std::string &fileName, DataInfo &data_info)
{
  boost::property_tree::ptree pt;
  boost::property_tree::read_xml(fileName, pt);

  if (pt.empty())
  {
    ROS_FATAL("Load %s/data.xml failed!", fileName.c_str());
    return;
  };
  std::string data;
  float pose[7];
  Eigen::Quaternionf q1, q2;
  Eigen::Matrix3f rotMat;

  int id = pt.get<int>("id");
  data_info.submap_id = id;
  data = pt.get<std::string>("pose");
  std::sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3],
              &pose[4], &pose[5], &pose[6]);
  q1 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);

  data_info.global_pose = cartographer::transform::Rigid3f({pose[0], pose[1], pose[2]}, q1);
  data.clear();

  data = pt.get<std::string>("local_pose");
  std::sscanf(data.c_str(), "%f %f %f %f %f %f %f", &pose[0], &pose[1], &pose[2], &pose[3],
              &pose[4], &pose[5], &pose[6]);
  q2 = Eigen::Quaternionf(pose[6], pose[3], pose[4], pose[5]);
  data_info.global_pose = cartographer::transform::Rigid3f({pose[0], pose[1], pose[2]}, q2);
  data.clear();

  data_info.resolution = pt.get<double>("probability_grid.resolution");
  LOG(INFO) << data_info.resolution;
  data = pt.get<std::string>("probability_grid.max");
  LOG(INFO) << data;
  std::sscanf(data.c_str(), "%f %f", &data_info.max[0], &data_info.max[1]);
  LOG(INFO) << data_info.max[0] << ", " << data_info.max[1];
}

void PubPointsFromMap::readPbstream(const std::string &path, DataInPbstream &data)
{
  cartographer::io::ProtoStreamReader stream(path);
  cartographer::io::ProtoStreamDeserializer deserializer(&stream);

  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();

  for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
       pose_graph_proto.trajectory())
  {
    for (const cartographer::mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      cartographer::transform::Rigid3d global_pose =
          cartographer::transform::ToRigid3(node_proto.pose());
      data.nodes_pose.Insert(
          cartographer::mapping::NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
          global_pose);
    }
  }
}

void PubPointsFromMap::getPointsFormMap(
    const std::string &path, /*const DataInPbstream& data_in_pbstream,*/
    std::vector<cartographer::mapping::NodeId> &node_id)
{
  cv::Mat image = cv::imread(path + "frames/0/probability_grid.png", CV_LOAD_IMAGE_UNCHANGED);
  // load landmarks
  std::vector<LandmarkPose> pose_list;
  getPoseInMap(path + "landmark.xml", "Lasermarks", pose_list);
  LOG(INFO) << pose_list.size();
  // load data.xml
  DataInfo data_info;
  loadDataXml(path + "frames/0/data.xml", data_info);
  LOG(INFO) << data_info.max;
  bool flag = true;
  std::vector<Eigen::Vector3i> valid_grids;
  for (const LandmarkPose &it : pose_list)
  {
    LOG(INFO) << it.pose;
    Eigen::Vector2i image_pos;
    image_pos(1) = (data_info.max[0] - it.pose.translation().x()) / data_info.resolution;
    image_pos(0) = (data_info.max[1] - it.pose.translation().y()) / data_info.resolution;
    //     world2imageLoc({data_info.max[0],
    //     data_info.max[1]},data_info.resolution, {it.pose.translation().x(),
    //     it.pose.translation().y()}, image_pos);
    valid_grids.push_back({image_pos.x(), image_pos.y(), 1});
    if (flag) LOG(INFO) << valid_grids.back().transpose();
    flag = false;
  }
  flag = true;
  LOG(INFO) << valid_grids.size();
  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uint16_t>(i, j);
      if (value == 0 || value > 2000) continue;
      valid_grids.push_back({j, i, 0});
      if (flag) LOG(INFO) << valid_grids.back().transpose();
      flag = false;
    }
  }
  LOG(INFO) << valid_grids.size();

  std::vector<Eigen::Vector3f> valid_points_in_map;
  flag = true;
  LOG(INFO) << data_info.max[0] << ", " << data_info.max[1];
  LOG(INFO) << data_info.resolution;
  cartographer::sensor::PointCloud map_cloud;
  map_cloud.reserve(valid_grids.size());
  for (auto it : valid_grids)
  {
    Eigen::Vector2d world_pos;
    world_pos[0] = data_info.max[0] - it.y() * data_info.resolution;
    world_pos[1] = data_info.max[1] - it.x() * data_info.resolution;
    //     image2worldLoc({data_info.max[1],
    //     data_info.max[0]},data_info.resolution, {it.x(), it.y()}, world_pos);
    Eigen::Vector3f tmp(world_pos.x(), world_pos.y(), it.z());
    if (flag)
    {
      LOG(INFO) << data_info.max[0] << ", " << data_info.max[1];
      LOG(INFO) << data_info.resolution;
      LOG(INFO) << it.x() << ", " << it.y();
      LOG(INFO) << tmp.transpose();
    }

    flag = false;
    geometry_msgs::Point32 p;

    p.x = tmp.x();
    p.y = tmp.y();
    p.z = tmp.z();
    pub_msg.points.push_back(p);
    map_cloud.push_back({tmp, it.z()});
  }

  pub_msg.header.frame_id = "map";

  LOG(INFO) << pub_msg.points.size();

  DataInPbstream data_in_pbstream;
  readPbstream(path + "map.pbstream", data_in_pbstream);

  std::vector<std::vector<cartographer::mapping::NodeId>> no_marks_nodes;
  std::vector<cartographer::mapping::NodeId> no_marks_node;
  for (const auto &it : data_in_pbstream.nodes_pose)
  {
    cartographer::transform::Rigid3f cur_global_pose = it.data.cast<float>();
    {
      geometry_msgs::Point32 p;
      p.x = cur_global_pose.translation().x();
      p.y = cur_global_pose.translation().y();
      p.z = cur_global_pose.translation().z();
      trajectory_points_.points.push_back(p);
    }

    cartographer::sensor::PointCloud cur_cloud =
        cartographer::sensor::TransformPointCloud(map_cloud, cur_global_pose.inverse());
    sensor_msgs::LaserScan output_;
    float angle_min, angle_max, angle_increment, range_min, range_max;
    angle_min = -3.1415926;
    angle_max = 3.1415926;
    angle_increment = 0.2 / 180 * 3.1415926;
    range_min = 0.1;
    range_max = 10;

    int range_size = ceil((angle_max - angle_min) / angle_increment) + 1; //向上取整
    output_.angle_min = angle_min;
    output_.angle_max = angle_max;
    output_.angle_increment = angle_increment;
    output_.time_increment = 0;
    output_.scan_time = 0;
    output_.range_min = range_min;
    output_.range_max = range_max;

    output_.ranges.assign(range_size, std::numeric_limits<float>::infinity());
    output_.intensities.assign(range_size, 0.0);
    output_.header.frame_id = "base_footprint";
    for (const auto &it2 : cur_cloud)
    {
      float range = it2.position.norm();
      if (range > range_max || range < range_min) continue;
      float angle = atan2(it2.position.y(), it2.position.x());
      int index = floor((angle - angle_min) / angle_increment); //向下取整
      if (index >= range_size || index < 0)
      {
        continue;
      }

      if (range < output_.ranges[index])
      {
        if (output_.ranges[index] != std::numeric_limits<float>::infinity())
        {
          if (output_.intensities[index] == 1)
          {
            if (fabs(range - output_.ranges[index]) < 0.3) continue;
          }
        }
        output_.ranges[index] = range;
        output_.intensities[index] = it2.intensity;
      }
    }
    int cnt = 0;
    scans.push(output_);
    node_poses_.push(it.data);
    //     scan_pub_.publish(output_);
    //     sleep(1);
    for (auto it3 : output_.intensities)
      if (it3 == 1) cnt++;
    if (cnt < 3)
      no_marks_node.push_back(it.id);
    else
    {
      if (no_marks_node.size() > 5) no_marks_nodes.push_back(no_marks_node);
      no_marks_node.clear();
    }
  }
  if (no_marks_node.size() > 5) no_marks_nodes.push_back(no_marks_node);

  for (auto it4 : no_marks_nodes)
  {
    for (auto it5 : it4)
    {
      cartographer::transform::Rigid3d cur_pose = data_in_pbstream.nodes_pose.at(it5);
      geometry_msgs::Point32 p;
      p.x = cur_pose.translation().x();
      p.y = cur_pose.translation().y();
      p.z = cur_pose.translation().z();
      no_marks_trajectory_points_.points.push_back(p);
      node_id.push_back(it5);
      // LOG(INFO) << "id1 :" << it5.trajectory_id << "id2: " << it5.node_index;
    }
  }
  // for(auto it : node_id){
  //   LOG(INFO) << "id" << it;
  // }
}

void PubPointsFromMap::pubTopic(const ros::WallTimerEvent &unused_timer_event)
{
  if (!pub_msg.points.empty() && !no_marks_trajectory_points_.points.empty())
  {
    pub_msg.header.stamp = ros::Time::now();
    pub_.publish(pub_msg);
    no_marks_trajectory_points_.header = pub_msg.header;
    no_marks_trajectory_pub_.publish(no_marks_trajectory_points_);
    trajectory_points_.header = pub_msg.header;
    trajectory_pub_.publish(trajectory_points_);

    if (scans.empty()) return;
    sensor_msgs::LaserScan scan_msg = scans.front();
    scans.pop();
    scan_msg.header.stamp = ros::Time::now();
    scan_pub_.publish(scan_msg);
    geometry_msgs::TransformStamped base2map_tf;
    base2map_tf.header = pub_msg.header;
    base2map_tf.child_frame_id = "base_footprint";
    cartographer::transform::Rigid3d cur_pose = node_poses_.front();
    node_poses_.pop();
    base2map_tf.transform.translation.x = cur_pose.translation().x();
    base2map_tf.transform.translation.y = cur_pose.translation().y();
    base2map_tf.transform.translation.z = cur_pose.translation().z();
    base2map_tf.transform.rotation.x = cur_pose.rotation().x();
    base2map_tf.transform.rotation.y = cur_pose.rotation().y();
    base2map_tf.transform.rotation.z = cur_pose.rotation().z();
    base2map_tf.transform.rotation.w = cur_pose.rotation().w();
    tf_broadcaster_.sendTransform(base2map_tf);
  }
}

// int main(int argc, char** argv)
// {
//   ros::init(argc, argv, "points_from_map");
//   ros::NodeHandle n;
//   PubPointsFromMap p(n);
//   ros::spin();
//   return 0;
// }