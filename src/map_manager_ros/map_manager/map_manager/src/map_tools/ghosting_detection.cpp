#include <ros/package.h>
#include <tf2/convert.h>

#include <Eigen/Geometry>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "algorithm"
#include "cmath"
#include "iostream"
#include "mutex"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/line_descriptor.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/opencv_modules.hpp"
#include "ros/ros.h"
#include "stack"
#include "vector"
// #include "ghosting_detection.h"
#include "ghosting_detection.h"
#include "map_tools/ghosting_detection.h"

using namespace cv;
using namespace cv::line_descriptor;

GhostingDetection::GhostingDetection() {}

GhostingDetection::~GhostingDetection() {}

void GhostingDetection::ZhangSuen_Thin(const cv::Mat &src, cv::Mat &dst)
{
  if (src.type() != CV_8UC1)
  {
    cout << "只处理二值化图像，请先二值化图像" << endl;
    return;
  }

  src.copyTo(dst);
  int width = src.cols;
  int height = src.rows;
  int step = dst.step;
  bool ifEnd;
  int p1, p2, p3, p4, p5, p6, p7, p8; //八邻域
  std::vector<uchar *> flag;          //用于标记待删除的点
  uchar *img = dst.data;

  while (true)
  {
    ifEnd = false;
    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        uchar *p = img + i * step + j;
        if (*p == 0) continue; //如果不是前景点,跳过
        //判断八邻域像素点的值(要考虑边界的情况),若为前景点(白色255),则为1;反之为0
        p1 = p[(i == 0) ? 0 : -step] > 0 ? 1 : 0;
        p2 = p[(i == 0 || j == width - 1) ? 0 : -step + 1] > 0 ? 1 : 0;
        p3 = p[(j == width - 1) ? 0 : 1] > 0 ? 1 : 0;
        p4 = p[(i == height - 1 || j == width - 1) ? 0 : step + 1] > 0 ? 1 : 0;
        p5 = p[(i == height - 1) ? 0 : step] > 0 ? 1 : 0;
        p6 = p[(i == height - 1 || j == 0) ? 0 : step - 1] > 0 ? 1 : 0;
        p7 = p[(j == 0) ? 0 : -1] > 0 ? 1 : 0;
        p8 = p[(i == 0 || j == 0) ? 0 : -step - 1] > 0 ? 1 : 0;
        if ((p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8) >= 2 &&
            (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8) <= 6) //条件1
        {
          //条件2的计数
          int count = 0;
          if (p1 == 0 && p2 == 1) ++count;
          if (p2 == 0 && p3 == 1) ++count;
          if (p3 == 0 && p4 == 1) ++count;
          if (p4 == 0 && p5 == 1) ++count;
          if (p5 == 0 && p6 == 1) ++count;
          if (p6 == 0 && p7 == 1) ++count;
          if (p7 == 0 && p8 == 1) ++count;
          if (p8 == 0 && p1 == 1) ++count;
          if (count == 1 && p1 * p3 * p5 == 0 && p3 * p5 * p7 == 0)
          {                    //条件2、3、4
            flag.push_back(p); //将当前像素添加到待删除数组中
          }
        }
      }
    }
    //将标记的点删除
    for (std::vector<uchar *>::iterator i = flag.begin(); i != flag.end(); ++i)
    {
      **i = 0;
      ifEnd = true;
    }
    flag.clear(); //清空待删除数组

    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        uchar *p = img + i * step + j;
        if (*p == 0) continue;
        p1 = p[(i == 0) ? 0 : -step] > 0 ? 1 : 0;
        p2 = p[(i == 0 || j == width - 1) ? 0 : -step + 1] > 0 ? 1 : 0;
        p3 = p[(j == width - 1) ? 0 : 1] > 0 ? 1 : 0;
        p4 = p[(i == height - 1 || j == width - 1) ? 0 : step + 1] > 0 ? 1 : 0;
        p5 = p[(i == height - 1) ? 0 : step] > 0 ? 1 : 0;
        p6 = p[(i == height - 1 || j == 0) ? 0 : step - 1] > 0 ? 1 : 0;
        p7 = p[(j == 0) ? 0 : -1] > 0 ? 1 : 0;
        p8 = p[(i == 0 || j == 0) ? 0 : -step - 1] > 0 ? 1 : 0;
        if ((p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8) >= 2 &&
            (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8) <= 6)
        {
          int count = 0;
          if (p1 == 0 && p2 == 1) ++count;
          if (p2 == 0 && p3 == 1) ++count;
          if (p3 == 0 && p4 == 1) ++count;
          if (p4 == 0 && p5 == 1) ++count;
          if (p5 == 0 && p6 == 1) ++count;
          if (p6 == 0 && p7 == 1) ++count;
          if (p7 == 0 && p8 == 1) ++count;
          if (p8 == 0 && p1 == 1) ++count;
          if (count == 1 && p1 * p3 * p7 == 0 && p1 * p5 * p7 == 0)
          {
            flag.push_back(p);
          }
        }
      }
    }
    //将标记的点删除
    for (std::vector<uchar *>::iterator i = flag.begin(); i != flag.end(); ++i)
    {
      **i = 0;
      ifEnd = true;
    }
    flag.clear();

    if (!ifEnd) break; //若没有可以删除的像素点，则退出循环
  }
}

void GhostingDetection::gammaTransformation(const cv::Mat &matInput, cv::Mat &matOutput,
                                            const float &fGamma, const float &fC)
{
  assert(matInput.elemSize() == 1);
  //构造输出图像
  matOutput = cv::Mat::zeros(matInput.rows, matInput.cols, matInput.type());

  //循环中尽量避免除法
  float fNormalFactor = 1.0f / 255.0f;
  //构造查询表
  std::vector<unsigned char> lookUp(256);
  for (size_t m = 0; m < lookUp.size(); m++)
  {
    // gamma变换
    float fOutput = std::pow(m * fNormalFactor, fGamma) * fC;
    //数值溢出判断
    fOutput = fOutput > 1.0f ? 1.0f : fOutput;
    //输出
    lookUp[m] = static_cast<unsigned char>(fOutput * 255.0f);
  }

  for (size_t r = 0; r < matInput.rows; r++)
  {
    unsigned char *pInput = matInput.data + r * matInput.step[0];
    unsigned char *pOutput = matOutput.data + r * matOutput.step[0];
    for (size_t c = 0; c < matInput.cols; c++)
    {
      //查表gamma变换
      pOutput[c] = lookUp[pInput[c]];
    }
  }
}

cv::Mat GhostingDetection::SkeletonExtraction(const string &path)
{
  cv::Mat src = cv::imread(path);

  if (src.empty())
  {
    std::cout << "error!" << std::endl;
  }
  Mat medianimg, medianimg1;

  cv::Mat gray_binary, gray;
  if (src.type() != CV_8UC1)
  {
    cv::cvtColor(src, gray, CV_BGR2GRAY);
    // imwrite(string(getenv("HOME")) + "/map/result1/" + "gray.bmp" , gray);
  }
  else
    gray = src;

  cv::Mat dst, result4, result5, edge, img1;
  Mat dest1, thin;
  // imwrite(string(getenv("HOME")) + "/map/result1/" + "tubai.bmp" , gray);

  gammaTransformation(gray, result5, 0.4, 1.0);

  // imwrite(string(getenv("HOME")) + "/map/result1/" +
  // "gammaTransformation.bmp" , result5);
  threshold(result5, gray_binary, 180, 255,
            CV_THRESH_BINARY_INV); // THRESH_BINARY | THRESH_OTSU自动确定阈值
                                   // cv::THRESH_BINARY_INV
  medianBlur(gray_binary, medianimg1, 1);

  // imwrite(string(getenv("HOME")) + "/map/result1/" + "gray_binary2.bmp" ,
  // gray_binary); imwrite(string(getenv("HOME")) + "/map/result1/" +
  // "medianimg1.bmp" , medianimg1);

  Mat result_binary;
  cv::Mat result4_binary, result4_gray, dst_r, thin2;
  if (medianimg1.type() != CV_8UC1)
  {
    cv::cvtColor(medianimg1, result4_gray, CV_BGR2GRAY);
  }
  else
    result4_gray = medianimg1;

  // imwrite(string(getenv("HOME")) + "/map/result1/" + "dst1111.bmp" ,
  // result4_gray);
  ZhangSuen_Thin(result4_gray, dst);

  Mat thin1;

  Mat ker = getStructuringElement(cv::MORPH_CROSS, Size(1, 1), Point(-1, -1)); //定义闭运算算子
  dilate(dst, thin1, ker, Point(-1, -1));

  // imwrite(string(getenv("HOME")) + "/map/result1/" + "dst.bmp" , dst);

  Mat labels, stats, centroids;
  ;
  //连通域去除
  int num = connectedComponentsWithStats(thin1, labels, stats, centroids, 8, CV_32S);
  cout << num << endl;
  std::vector<Vec3b> colors(num);
  colors[0] = Vec3b(0, 0, 0);
  for (size_t i = 1; i < num; i++)
  {
    if (stats.at<int>(i, CC_STAT_AREA) > 20)
      colors[i] = Vec3b(255, 255, 255); //给连通域添加颜色
    else
      colors[i] = Vec3b(0, 0, 0);
  }
  // Mat aimArea = Mat::zeros(dst.size(), dst.type());
  Mat aimArea = Mat::zeros(src.size(), src.type());
  for (size_t row = 0; row < dst.rows; row++)
  {
    for (size_t col = 0; col < dst.cols; col++)
    {
      //获取原图中每个点的label
      size_t label = labels.at<uint32_t>(row, col);
      aimArea.at<Vec3b>(row, col) = colors[label];
    }
  }

  // imwrite(string(getenv("HOME")) + "/map/result1/" + "aimArea.bmp" ,
  // aimArea);
  return aimArea;
}

bool GhostingDetection::judge(const Line &l1, const Line &l2)
{
  if ((max(l1.x1, l1.x2) >= min(l2.x1, l2.x2) &&
       min(l1.x1, l1.x2) <= max(l2.x1, l2.x2)) && //判断x轴投影
      (max(l1.y1, l1.y2) >= min(l2.y1, l2.y2) &&
       min(l1.y1, l1.y2) <= max(l2.y1, l2.y2)) //判断y轴投影
  )
  {
    if (((l2.x1 - l1.x1) * (l1.y2 - l1.y1) - (l2.y1 - l1.y1) * (l1.x2 - l1.x1)) * //判断B是否跨过A
                ((l2.x2 - l1.x1) * (l1.y2 - l1.y1) - (l2.y2 - l1.y1) * (l1.x2 - l1.x1)) <=
            0 &&
        ((l1.x1 - l2.x1) * (l2.y2 - l2.y1) - (l1.y1 - l2.y1) * (l2.x2 - l2.x1)) * //判断A是否跨过B
                ((l1.x2 - l2.x1) * (l2.y2 - l2.y1) - (l1.y2 - l2.y1) * (l2.x2 - l2.x1)) <=
            0)
    {
      return 1;
    }
    else
      return 0;
  }
  else
    return 0;
}

/*
求点到线段的距离
使用方法 distance （P ，A ，B） P为待测点，AB为线段起始点
*/
double GhostingDetection::Point_distance(const Point2d &P, const Point2d &A, const Point2d &B)
{
  //计算r |AB| |AP| |BP| |PC|

  double ab = sqrt(pow((B.x - A.x), 2) + pow((B.y - A.y), 2)); // |AB|
  double ap = sqrt(pow((P.x - A.x), 2) + pow((P.y - A.y), 2)); // |AP|
  double bp = sqrt(pow((P.x - B.x), 2) + pow((P.y - B.y), 2)); // |BP|
  double r = 0;
  if (ab > 0)
  {
    r = ((P.x - A.x) * (B.x - A.x) + (P.y - A.y) * (B.y - A.y)) / pow(ab, 2);
  } // r
  else
  {
    cout << "no lines" << endl;
  }

  // double distance = 0;
  double distance = 0;
  if (ab > 0)
  {
    if (r >= 1)
      distance = bp;
    else if (r > 0 && r < 1)
      distance = sqrt(pow(ap, 2) - r * r * pow(ab, 2));
    else
      distance = ap;
  }

  //显示结果,可以带入点进行测试
  /*cout<<"|r|"<<r<<endl;
  cout<<"|AP|"<<ap<<endl;
  cout<<"|BP|"<<bp<<endl;
  cout<<"|AB|"<<ab<<endl;
  cout<<"distance is : "<<distance<<endl;
  */
  return distance;
}

cv::Mat GhostingDetection::ghostDetection(const std::string &path,
                                          std::vector<std::pair<Line, Line>> &ghostingLines)
{
  std::vector<string> p;
  std::vector<Eigen::Vector2f> resultP;

  Mat dst, cdst, cdstP;
  std::string sp = path + "/map.png";
  cv::Mat src = SkeletonExtraction(sp);

  if (src.type() != CV_8UC1)
  {
    cv::cvtColor(src, src, CV_BGR2GRAY);
  }
  else
    src = src;
  cv::Mat img = cv::imread(sp);
  // Check if image is loaded fine
  if (src.empty())
  {
    printf(" Error opening image\n");
    printf(" image load error \n");
    return src;
  }

  threshold(src, dst, 180, 255, CV_THRESH_BINARY);

  cvtColor(dst, cdst, CV_GRAY2BGR);
  cdstP = cdst.clone();

  std::vector<Vec4i> linesP; // will hold the results of the detection

  HoughLinesP(dst, linesP, 1, CV_PI / 180, 35, 4,
              1); // runs the actual detection

  int size = linesP.size();
  cout << "size:" << size << endl;

  std::vector<Point2i> e;

  for (size_t i = 0; i < linesP.size(); i++) // Draw the lines
  {
    Vec4i l = linesP[i];
    line(cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 1, LINE_AA);

    //定义一个向量容器
    Point2i ep;

    ep.x = l[0] - l[2];
    ep.y = l[1] - l[3];

    e.push_back(ep);
  }
  // imwrite(string(getenv("HOME")) + "/map/result1/" + "dst(end).bmp" , cdstP);
  int count = 0;
  for (int i = 0; i < size; i++)
  {
    int eix = e[i].x;
    int eiy = e[i].y;

    Point2d A, B;       //参照线段
    A.x = linesP[i][0]; //线段起点 x
    A.y = linesP[i][1]; //线段起点 y
    B.x = linesP[i][2]; //线段终点 x
    B.y = linesP[i][3]; //线段终点 y

    for (int j = 0; j < size; j++)
    {
      if (i == j) continue;
      int ejx = e[j].x;
      int ejy = e[j].y;
      float t = ((eix) * (ejx) + (eiy) * (ejy)) /
                (sqrt(pow(eix, 2) + pow(eiy, 2)) * sqrt(pow(ejx, 2) + pow(ejy, 2))); //向量的叉积

      float edi = sqrt(pow(eix, 2) + pow(eiy, 2));
      float edj = sqrt(pow(ejx, 2) + pow(ejy, 2));
      if (t > 0.98 && t < 1.02)
      {
        Point2d P1, P2;      //待测线段
        P1.x = linesP[j][0]; //待测点p1 x
        P1.y = linesP[j][1]; //待测点p1 y
        P2.x = linesP[j][2]; //待测点p2 x
        P2.y = linesP[j][3]; //待测点p2 y
        float d1, d2;
        if (edi < edj)
        {
          d1 = Point_distance(A, P1, P2); //求距离
          d2 = Point_distance(B, P1, P2);
        }
        else
        {
          d1 = Point_distance(P1, A, B); //求距离
          d2 = Point_distance(P2, A, B);
        }
        //求距离
        float d = d1 > d2 ? d1 : d2;
        Line l1, l2;
        l1.x1 = linesP[i][0];
        l1.y1 = linesP[i][1];
        l1.x2 = linesP[i][2];
        l1.y2 = linesP[i][3];

        l2.x1 = linesP[j][0];
        l2.y1 = linesP[j][1];
        l2.x2 = linesP[j][2];
        l2.y2 = linesP[j][3];
        if (d1 < 5 && d2 < 5 && d1 > 1.3 && d2 > 1.3)
        {
          if (edi >= 8 && edj >= 8)
          {
            if (judge(l1, l2) == 1)
            {
              continue;
            }
            else
            {
              if (abs(edi - edj) < 50)
              {
                count++;
                ghostingLines.emplace_back(std::make_pair(l1, l2));
                line(img, Point(linesP[i][0], linesP[i][1]), Point(linesP[i][2], linesP[i][3]),
                     Scalar(255, 0, 0), 1, LINE_AA);
                line(img, Point(linesP[j][0], linesP[j][1]), Point(linesP[j][2], linesP[j][3]),
                     Scalar(0, 255, 0), 1, LINE_AA);
              }
            }
          }
        }
      }
    }
  }
  cout << "count:" << count << endl;
  imwrite(std::string(getenv("HOME")) + "/src.png", img);
  return img;
}