/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef PBSTREAMCONVERTOR_H
#define PBSTREAMCONVERTOR_H
#include <cartographer/common/thread_pool.h>
#include <cartographer/io/internal/mapping_state_serialization.h>
#include <cartographer/io/proto_stream.h>
#include <cartographer/io/proto_stream_interface.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/2d/probability_grid_range_data_inserter_2d.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/optimization/optimization_problem_2d.h>
#include <cartographer/mapping/proto/map_builder_options.pb.h>
#include <cartographer/mapping/proto/serialization.pb.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/transform/transform.h>

#include <Eigen/Eigen>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

#include "2d/map_mapping.h"
#include "utility/kd_tree.h"
#include "utility/point_optimization_ceres.h"
namespace map_manager
{
namespace pbstream_convertor
{
class PbstreamConvertor
{
  public:
  PbstreamConvertor(const cartographer::mapping::proto::MapBuilderOptions &options);
  ~PbstreamConvertor();
  void setFrezon(const bool &is_frezon);
  bool handlePbstream(const std::string &origin_pbstream_path, const std::string &new_map_name);
  void updatePoints(std::vector<Pose3d> &nav_points);

  private:
  std::map<int, int> LoadState(cartographer::io::ProtoStreamReaderInterface *const reader,
                               bool load_frozen_state);

  void preHandle();
  void clusteringSubmaps();
  void mergeSubmaps(const std::vector<cartographer::mapping::SubmapId> &ids,
                    const cartographer::mapping::SubmapId &new_submap_id);
  int AddTrajectoryForDeserialization(
      const cartographer::mapping::proto::TrajectoryBuilderOptionsWithSensorIds
          &options_with_sensor_ids_proto);
  cv::Point2f findNearestCenterNode(const cv::Point2f &center_point,
                                    std::vector<cartographer::mapping::SubmapId> ids);
  void getImage(std::unique_ptr<cartographer::mapping::Submap2D> merge_submap,
                const cartographer::transform::Rigid3f &global_pose);
  void saveMapFolder(const std::string &new_map_name);

  void updateCornerPoints(const std::string &origin_full_path, const std::string &full_path);
  std::unique_ptr<cartographer::mapping::PoseGraph> origin_pose_graph_, new_pose_graph_;
  const cartographer::mapping::proto::MapBuilderOptions options_;
  cartographer::common::ThreadPool thread_pool1_, thread_pool2_;
  std::vector<std::unique_ptr<cartographer::mapping::TrajectoryBuilderInterface>>
      trajectory_builders_;
  std::vector<cartographer::mapping::proto::TrajectoryBuilderOptionsWithSensorIds>
      all_trajectory_builder_options_;

  std::vector<int> num_range_data_;

  std::map<int, std::vector<cartographer::mapping::NodeId>> nodes_sort_by_trajectory_;
  std::map<int, std::vector<cartographer::mapping::SubmapId>> clustering_submaps_;
  std::map<int, cv::Point2f> clustering_submaps_poses_;
  std::map<cartographer::mapping::SubmapId, std::vector<cartographer::mapping::TrajectoryNode>>
      submap_with_nodes_;
  std::map<cartographer::mapping::NodeId, cartographer::mapping::TrajectoryNode> nodes_;
  std::map<cartographer::mapping::SubmapId, std::vector<cartographer::mapping::NodeId>>
      new_constraints_;
  std::map<cartographer::mapping::SubmapId, std::vector<cartographer::mapping::NodeId>>
      intra_constraints_, inter_constraints_;
  //   absl::Mutex mutex_;
  float interval_of_submap_;
  bool debug_flag_;
  bool is_frezon_;
  cartographer::mapping::scan_matching::CeresScanMatcher2D ceres_scan_matcher_;
  std::unique_ptr<kd_tree::KDTree> kd_tree_;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::TrajectoryNode>
      new_nodes_;
  std::vector<cartographer::mapping::PoseGraph::Constraint> constraints_;
};
} // namespace pbstream_convertor
} // namespace map_manager
#endif // PBSTREAMCONVERTOR_H
