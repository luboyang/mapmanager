/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef PUBPOINTSFROMMAP_H
#define PUBPOINTSFROMMAP_H
#include <cartographer/mapping/id.h>
#include <cartographer/transform/transform.h>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_broadcaster.h>

#include <Eigen/Eigen>
#include <queue>

struct LandmarkPose
{
  int id; // id string 2 int
  cartographer::transform::Rigid3f pose;
  float pole_radius;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

struct DataInfo
{
  int submap_id; // id string 2 int
  cartographer::transform::Rigid3f global_pose;
  cartographer::transform::Rigid3f local_pose;
  float max[2];
  float resolution;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

struct DataInPbstream
{
  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      nodes_pose;
  //   cartographer::mapping::MapById<cartographer::mapping::NodeId,
  //   cartographer::transform::Rigid3d> nodes_pose;
};
class PubPointsFromMap
{
  public:
  PubPointsFromMap(const ros::NodeHandle &nh);
  ~PubPointsFromMap();
  void getPointsFormMap(const std::string &path,
                        std::vector<cartographer::mapping::NodeId>
                            &node_id); /*const DataInPbstream& data_in_pbstream,*/
  private:
  void getPoseInMap(const std::string &fileName, const std::string &con_ns,
                    std::vector<LandmarkPose> &pose_list);
  void loadDataXml(const std::string &fileName, DataInfo &data_info);
  void pubTopic(const ::ros::WallTimerEvent &unused_timer_event);
  void readPbstream(const std::string &path, DataInPbstream &data);
  ros::NodeHandle nh_;
  ros::Publisher pub_;
  ros::Publisher trajectory_pub_;
  ros::Publisher no_marks_trajectory_pub_;
  ros::Publisher scan_pub_;
  ros::WallTimer wall_timer_;
  sensor_msgs::PointCloud pub_msg;
  sensor_msgs::PointCloud no_marks_trajectory_points_;
  sensor_msgs::PointCloud trajectory_points_;
  std::queue<sensor_msgs::LaserScan> scans;
  std::queue<cartographer::transform::Rigid3d> node_poses_;
  tf2_ros::TransformBroadcaster tf_broadcaster_;
};

#endif // PUBPOINTSFROMMAP_H
