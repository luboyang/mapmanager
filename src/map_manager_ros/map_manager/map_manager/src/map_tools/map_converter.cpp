/*
 * Copyright 2023 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "map_converter.h"

#include <cartographer/mapping/3d/submap_3d.h>
#include <pcl/io/pcd_io.h>

#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "../3d/map_mapping_3d.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
using namespace cartographer;
using namespace cartographer::mapping;
using cartographer::mapping::proto::SerializedData;
using namespace std;
namespace map_manager
{
typedef pcl::PointXYZ PointT;
MapConverter::MapConverter(const std::string &map_name)
{
  PbstreamInfos infos;
  string path = string(getenv("HOME")) + "/map/" + map_name;
  string new_map_name = map_name + "_convertor";
  readPosesFromPbstream(path, infos);
  string filepath = string(getenv("HOME")) + "/map/" + new_map_name;
  std::shared_ptr<const Submap3D> submap_ptr =
      std::make_shared<const Submap3D>(infos.submap_id_to_submap.begin()->data.submap_3d());

  int num_range_data = submap_ptr->num_range_data() / 2;
  std::vector<std::vector<cartographer::mapping::TrajectoryNode>> point_clouds;
  sensor::PointCloud all_cloud;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr all_cloud_with_rgb(new pcl::PointCloud<pcl::PointXYZRGB>);
  uint submap_cnt = 0;
  std::vector<transform::Rigid3d> submap_pose;
  std::map<SubmapId, int> submap_submapId;
  std::ofstream outFile;
  int status;
  std::string system_command = "mkdir -p " + filepath;
  status = system(system_command.c_str());
  outFile.open(filepath + "/node_poses.txt", std::ios::out);
  LOG(WARNING) << "num_range_data:" << num_range_data;

  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  cartographer::mapping::proto::RangeDataInserterOptions3D range_data_inserter_options;
  {
    range_data_inserter_options.set_hit_probability(0.8);
    range_data_inserter_options.set_miss_probability(0.3);
    range_data_inserter_options.set_num_free_space_voxels(2);
  }
  const Eigen::VectorXf initial_rotational_scan_matcher_histogram = Eigen::VectorXf::Zero(120);
  for (const auto &it : infos.node_datas)
  {
    if (point_clouds.empty() || point_clouds.front().size() % num_range_data == 0)
    {
      if (point_clouds.size() == 2)
      {
        std::string pcd_folder = filepath + "/frames/" + std::to_string(submap_cnt) + "/";

        system_command = "mkdir -p " + pcd_folder;
        LOG(INFO) << system_command << std::endl;
        status = system(system_command.c_str());
        checkSystemStatus(system_command, status);
        CHECK(status != -1);

        std::vector<cartographer::mapping::TrajectoryNode> &pts = point_clouds.front();

        pcl::PointCloud<PointT>::Ptr tmp(new pcl::PointCloud<PointT>);
        transform::Rigid3d submap2global = pts[num_range_data].global_pose;

        std::unique_ptr<cartographer::mapping::Submap3D> merge_submap_ptr(new mapping::Submap3D(
            0.2, 1.0, transform::Rigid3d::Identity(), initial_rotational_scan_matcher_histogram));

        sensor::PointCloud submap_cloud;
        sensor::PointCloud surf_submap_cloud;
        cartographer::mapping::RangeDataInserter3D range_data_inserter(range_data_inserter_options);
        for (const cartographer::mapping::TrajectoryNode &node : pts)
        {
          transform::Rigid3d node2submap = submap2global.inverse() * node.global_pose;
          sensor::PointCloud pts_in_submap = sensor::TransformPointCloud(
              node.constant_data->low_resolution_point_cloud, node2submap.cast<float>());
          submap_cloud.insert(submap_cloud.begin(), pts_in_submap.begin(), pts_in_submap.end());

          sensor::PointCloud pts_in_submap_high = sensor::TransformPointCloud(
              node.constant_data->high_resolution_point_cloud, node2submap.cast<float>());
          surf_submap_cloud.insert(surf_submap_cloud.begin(), pts_in_submap_high.begin(),
                                   pts_in_submap_high.end());

          merge_submap_ptr->InsertData(node2submap.translation().cast<float>(), pts_in_submap_high,
                                       pts_in_submap, range_data_inserter,
                                       node.constant_data->gravity_alignment,
                                       node.constant_data->rotational_scan_matcher_histogram);
        }

        {
          io::ProtoStreamWriter writer2(pcd_folder + "submap.pbstream");
          //             proto::PoseGraph pose;
          SerializedData proto2;
          auto *const submap_proto2 = proto2.mutable_submap();
          *submap_proto2 = merge_submap_ptr->ToProto(true);
          submap_proto2->mutable_submap_id()->set_trajectory_id(0);
          submap_proto2->mutable_submap_id()->set_submap_index(submap_cnt);
          writer2.WriteProto(proto2);
          writer2.Close();
        }

        LOG(WARNING) << "submap_cloud size:" << submap_cloud.size();
        sensor::PointCloud submap_cloud2 = sensor::VoxelFilter(0.05).Filter(submap_cloud);
        LOG(WARNING) << "submap_cloud2 size:" << submap_cloud2.size();
        tmp->points.reserve(submap_cloud2.size());
        for (const sensor::RangefinderPoint &p : submap_cloud2)
        {
          PointT p2;
          p2.x = p.position.x();
          p2.y = p.position.y();
          p2.z = p.position.z();
          //             p2.intensity = p.intensity;
          tmp->points.push_back(p2);
        }
        if (tmp->points.size() > 0)
        {
          tmp->width = tmp->points.size();
          tmp->height = 1;
          pcl::io::savePCDFileBinaryCompressed(pcd_folder + "corner_submap.pcd", *tmp);
        }
        LOG(WARNING) << "save corner_submap.pcd done!";
        pcl::PointCloud<PointT>::Ptr tmp2(new pcl::PointCloud<PointT>);
        sensor::PointCloud surf_submap_cloud2 = sensor::VoxelFilter(0.05).Filter(surf_submap_cloud);
        tmp2->points.reserve(surf_submap_cloud2.size());
        for (const sensor::RangefinderPoint &p : surf_submap_cloud2)
        {
          PointT p2;
          p2.x = p.position.x();
          p2.y = p.position.y();
          p2.z = p.position.z();
          //             p2.intensity = p.intensity;
          tmp2->points.push_back(p2);
        }
        if (tmp2->points.size() > 0)
        {
          tmp2->width = tmp2->points.size();
          tmp2->height = 1;

          pcl::io::savePCDFileBinaryCompressed(pcd_folder + "surf_submap.pcd", *tmp2);
        }
        LOG(WARNING) << "save surf_submap.pcd done!";
        boost::property_tree::ptree p_map;
        std::string data;
        //   NodeId tmp_node_id{0,0};
        transform::Rigid3d::Vector translation;
        transform::Rigid3d::Quaternion quaternion;
        translation = submap2global.translation();
        quaternion = submap2global.rotation();
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(translation[i]) + " ";
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(quaternion.coeffs()[i]) + " ";
        data = data + std::to_string(quaternion.coeffs()[3]);
        p_map.put("id", submap_cnt);
        p_map.put("submap_global_pose", data);
        p_map.put("pose", data);
        data.clear();
        boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
        boost::property_tree::write_xml(pcd_folder + "data.xml", p_map, std::locale(), setting);
        submap_pose.push_back(submap2global);
        SubmapId cur_submap_id(0, submap_cnt);
        submap_submapId[cur_submap_id] = submap_cnt;
        ++submap_cnt;

        point_clouds.erase(point_clouds.begin());
      }
      point_clouds.push_back({});
    }
    for (std::vector<cartographer::mapping::TrajectoryNode> &pts : point_clouds)
    {
      pts.push_back(
          {std::make_shared<mapping::TrajectoryNode::Data>(mapping::TrajectoryNode::Data(it.data)),
           infos.node_poses.at(it.id)});
    }
    sensor::PointCloud pts_in_global = sensor::TransformPointCloud(
        it.data.high_resolution_point_cloud, infos.node_poses.at(it.id).cast<float>());
    all_cloud.insert(all_cloud.end(), pts_in_global.begin(), pts_in_global.end());
    outFile << it.data.time << " " << infos.node_poses.at(it.id).translation().x() << " "
            << infos.node_poses.at(it.id).translation().y() << " "
            << infos.node_poses.at(it.id).translation().z() << " "
            << infos.node_poses.at(it.id).rotation().w() << " "
            << infos.node_poses.at(it.id).rotation().x() << " "
            << infos.node_poses.at(it.id).rotation().y() << " "
            << infos.node_poses.at(it.id).rotation().z() << std::endl;
  }
  outFile.close();
  if (all_cloud.size() > 0)
  {
    pcl::PointCloud<PointT>::Ptr tmp(new pcl::PointCloud<PointT>);
    sensor::PointCloud all_cloud2 = sensor::VoxelFilter(0.2).Filter(all_cloud);
    tmp->points.reserve(all_cloud2.size());
    for (const sensor::RangefinderPoint &p : all_cloud2)
    {
      PointT p2;
      p2.x = p.position.x();
      p2.y = p.position.y();
      p2.z = p.position.z();
      //         p2.intensity = p.intensity;
      tmp->points.push_back(p2);
    }
    tmp->width = tmp->points.size();
    tmp->height = 1;
    //       pcl::io::savePCDFileBinaryCompressed(filepath +
    //       "/pcd_frames/map.pcd", *tmp);
    pcl::io::savePCDFileBinaryCompressed(filepath + "map.pcd", *tmp);
    all_cloud_with_rgb->points.reserve(all_cloud.size());
    for (const sensor::RangefinderPoint &p : all_cloud)
    {
      if (p.intensity > 0)
      {
        pcl::PointXYZRGB p2;
        p2.x = p.position.x();
        p2.y = p.position.y();
        p2.z = p.position.z();
        int ab1 = p.intensity;
        p2.b = ab1 >> 16;
        int ab2 = (ab1 - 256 * 256 * p2.b);
        p2.g = ab2 >> 8;
        p2.r = ab2 - 256 * p2.g;
        all_cloud_with_rgb->points.push_back(p2);
      }
    }
    if (!all_cloud_with_rgb->empty())
    {
      all_cloud_with_rgb->resize(all_cloud_with_rgb->size());
      all_cloud_with_rgb->width = all_cloud_with_rgb->points.size();
      all_cloud_with_rgb->height = 1;
      //         pcl::io::savePCDFileBinaryCompressed(filepath +
      //         "/pcd_frames/map_rgb.pcd", *all_cloud_with_rgb);
      pcl::io::savePCDFileBinaryCompressed(filepath + "map_rgb.pcd", *all_cloud_with_rgb);
    }
  }
  {
    std::vector<std::set<int>> connections;
    std::map<NodeId, int> node_submapId;
    std::vector<PoseGraphInterface::Constraint> constraints = infos.constraints;
    // LOG(INFO)<<constraints.size()<<std::endl;
    connections.resize(submap_cnt);
    // determine which submap each node(intra) belongs to
    for (uint constraints_id = 0; constraints_id < constraints.size() - 2; constraints_id++)
    {
      mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
      if (constraint.tag == mapping::PoseGraph::Constraint::INTRA_SUBMAP &&
          submap_submapId.find(constraint.submap_id) != submap_submapId.end())
      {
        node_submapId.insert(
            std::pair<NodeId, int>(constraint.node_id, submap_submapId[constraint.submap_id]));
      }
    }
    // add adjacent connections
    for (uint i = 0; i < submap_cnt - 1; i++)
    {
      connections[i].insert(i + 1);
      connections[i + 1].insert(i);
    }
    //    LOG(INFO) << "submap_cnt:" << submap_cnt << std::endl;
    //    LOG(INFO) << "constraints.size():" << constraints.size() << std::endl;
    // add connections induced by nodes(inter)
    for (uint constraints_id = 0; constraints_id < constraints.size() - 2; constraints_id++)
    {
      mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
      if (constraint.tag == mapping::PoseGraph::Constraint::INTER_SUBMAP &&
          submap_submapId.find(constraint.submap_id) != submap_submapId.end() &&
          node_submapId.find(constraint.node_id) != node_submapId.end())
      {
        //        LOG(INFO) << "constraint.submap_id:" << constraint.submap_id
        //        << std::endl; LOG(INFO) << "constraint.node_id:" <<
        //        constraint.node_id << std::endl; LOG(INFO) <<
        //        "submap_submapId[constraint.submap_id]:"
        //                  << submap_submapId[constraint.submap_id] <<
        //                  std::endl;
        //        LOG(INFO) << "node_submapId[constraint.node_id]:"
        //                  << node_submapId[constraint.node_id] << std::endl;
        connections[submap_submapId[constraint.submap_id]].insert(
            node_submapId[constraint.node_id]);
        connections[node_submapId[constraint.node_id]].insert(
            submap_submapId[constraint.submap_id]);
      }
    }
    transform::Rigid3d::Vector translation;
    transform::Rigid3d::Quaternion quaternion;
    std::string data;
    boost::property_tree::ptree p_top;
    boost::property_tree::ptree p_map;
    boost::property_tree::ptree p_node_list;
    for (uint i = 0; i < submap_cnt; i++)
    {
      boost::property_tree::ptree p_node;

      p_node.put("id", i);

      boost::property_tree::ptree p_neighbour_list;
      for (std::set<int>::iterator p = connections[i].begin(); p != connections[i].end(); ++p)
      {
        boost::property_tree::ptree p_neighbour;
        uint connected_id = *p;

        CHECK(connected_id < submap_cnt);

        p_neighbour.put("id", connected_id);
        transform::Rigid3d neighbour2current = submap_pose[i].inverse() * submap_pose[connected_id];
        translation = neighbour2current.translation();
        quaternion = neighbour2current.rotation();
        for (int i = 0; i < 3; i++)
          data = data + std::to_string(translation[i]) + " ";
        data = data + std::to_string(quaternion.w()) + " ";
        data = data + std::to_string(quaternion.x()) + " ";
        data = data + std::to_string(quaternion.y()) + " ";
        data = data + std::to_string(quaternion.z());
        p_neighbour.put("transform", data);
        data.clear();
        p_neighbour.put("reachable", true);

        p_neighbour_list.add_child("neighbour", p_neighbour);
      }

      p_node.add_child("neighbour_list", p_neighbour_list);

      p_node_list.add_child("node", p_node);
    }
    p_map.add_child("node_list", p_node_list);
    boost::property_tree::ptree p_property;
    p_property.put("node_count", submap_cnt);

    p_map.add_child("property", p_property);

    p_top.add_child("map", p_map);
    //       LOG(INFO) << "save  "  << filepath <<
    //       "/pcd_frames/map.xml"<<std::endl;
    LOG(INFO) << "save  " << filepath << "/map.xml" << std::endl;
    boost::property_tree::write_xml(filepath + "/map.xml", p_top, std::locale(), setting);
  }
  map_manager::MapMapping3D m3;
  m3.saveShowImage(filepath + "/");
  cout << "done!" << endl;
}

MapConverter::~MapConverter() {}

} // namespace map_manager

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  FLAGS_colorlogtostderr = true;
  google::SetStderrLogging(google::GLOG_INFO);
  ros::init(argc, argv, "map_converter_node");
  string map_name = argv[1];
  map_manager::MapConverter c(map_name);

  return 0;
}