#include <cartographer/io/proto_stream_deserializer.h>
#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/proto/2d/local_trajectory_builder_options_2d.pb.h>
#include <cartographer/mapping/proto/2d/submaps_options_2d.pb.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer_ros/msg_conversion.h>
#include <cartographer_ros/node.h>
#include <cartographer_ros/node_options.h>
#include <cartographer_ros/time_conversion.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>

#include "map_manager/3d/map_localization_3d.h"
#include "map_manager/3d/map_mapping_3d.h"
#include "map_manger/map_tools/pbstream_convertor.h"
using namespace cartographer;
using namespace cartographer::mapping;
using namespace cartographer::sensor;
using namespace cartographer_ros;
using namespace std;
sensor_msgs::PointCloud2 point_cloud2_msg;
sensor_msgs::Imu imu_msg;
ros::Publisher imu_pub, point_pub;
ros::Subscriber merge_point_cloud_sub;
sensor::PointCloud all_point_cloud;
std::unique_ptr<cartographer::mapping::Submap3D> merge_3dmap_;
ros::ServiceServer srv_;
// cartographer::mapping::RangeDataInserter3D range_data_inserter_;
class RangeDataInsert
{
  public:
  RangeDataInsert(
      const cartographer::mapping::proto::RangeDataInserterOptions3D range_data_inserter_options)
      : range_data_inserter(range_data_inserter_options)
  {
  }
  cartographer::mapping::RangeDataInserter3D range_data_inserter;

  private:
};
RangeDataInsert *range_data_inserter_;
void handlePointCloud2(const sensor_msgs::PointCloud2Ptr &msg)
{
  // 	double high_resolution,low_resolution;
  // 	high_resolution = 0.05;
  // 	low_resolution = 0.1;
  // 	MapInterface::NodeData nodes_data;
  // 	cartographer::mapping::TrajectoryNode::Data node_data;
  // 	std::shared_ptr<Submap3D> submap_(new Submap3D(high_resolution,
  // low_resolution, 							nodes_data[local_id].global_pose,
  // 							initial_rotational_scan_matcher_histogram));
  if (point_cloud2_msg.point_step == 0) point_cloud2_msg = *msg;
}
void handleImu(const sensor_msgs::ImuPtr &msg)
{
  if (imu_msg.linear_acceleration.z == 0)
  // 		imu_msg = *msg;
  {
    imu_msg = *msg;
    imu_msg.angular_velocity.x = 0;
    imu_msg.angular_velocity.y = 0;
    imu_msg.angular_velocity.z = 0;
    // 		imu_msg.orientation.w = 1;
  }
}
void pubTopic(const ::ros::WallTimerEvent &unused_timer_event)
{
  if (point_cloud2_msg.point_step != 0 && imu_msg.linear_acceleration.z != 0)
  {
    imu_msg.header.stamp = ros::Time::now();
    point_cloud2_msg.header.stamp = ros::Time::now();
    imu_pub.publish(imu_msg);
    point_pub.publish(point_cloud2_msg);
  }
}
sensor_msgs::PointCloud2 PreparePointCloud2Message1(const cartographer::common::Time timestamp,
                                                    const std::string &frame_id,
                                                    const int num_points)
{
  sensor_msgs::PointCloud2 msg;
  msg.header.stamp = ToRos(timestamp);
  msg.header.frame_id = frame_id;
  msg.height = 1;
  msg.width = num_points;
  msg.fields.resize(4);
  msg.fields[0].name = "x";
  msg.fields[0].offset = 0;
  msg.fields[0].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[0].count = 1;
  msg.fields[1].name = "y";
  msg.fields[1].offset = 4;
  msg.fields[1].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[1].count = 1;
  msg.fields[2].name = "z";
  msg.fields[2].offset = 8;
  msg.fields[2].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[2].count = 1;
  msg.fields[3].name = "i";
  msg.fields[3].offset = 12;
  msg.fields[3].datatype = sensor_msgs::PointField::FLOAT32;
  msg.fields[3].count = 1;
  msg.is_bigendian = false;
  msg.point_step = 20;
  msg.row_step = 20 * msg.width;
  msg.is_dense = true;
  msg.data.resize(20 * num_points);
  return msg;
}

sensor_msgs::PointCloud2
ToPointCloud2Message1(const ::cartographer::common::Time timestamp, const std::string &frame_id,
                      const ::cartographer::sensor::PointCloud &point_cloud)
{
  auto msg = PreparePointCloud2Message1(timestamp, frame_id, point_cloud.size());
  ::ros::serialization::OStream stream(msg.data.data(), msg.data.size());
  for (const cartographer::sensor::RangefinderPoint &point : point_cloud)
  {
    stream.next(point.position.x());
    stream.next(point.position.y());
    stream.next(point.position.z());
    stream.next(point.intensity);
    stream.next(1);
  }
  // LOG(INFO) <<"intensity:" <<  point_cloud[0].intensity ;
  return msg;
}

void saveToPbstream(std::unique_ptr<cartographer::mapping::Submap3D> &merge_submap)
{
  std::string folderstring = std::string(std::getenv("HOME")) + "/map/test666/frames/0/";
  std::string system_command = "mkdir -p " + folderstring;
  LOG(INFO) << system_command;
  int status = system(system_command.c_str());
  CHECK(status != -1);

  io::ProtoStreamWriter writer(folderstring + "submap.pbstream");
  cartographer::mapping::proto::PoseGraph pose;
  cartographer::mapping::proto::SerializedData proto1;
  auto *const submap_proto = proto1.mutable_submap();
  *submap_proto = merge_submap->ToProto(
      /*include_probability_grid_data=*/true);
  submap_proto->mutable_submap_id()->set_trajectory_id(0);
  submap_proto->mutable_submap_id()->set_submap_index(0);
  writer.WriteProto(proto1);
  writer.Close();
  LOG(INFO) << "write proto done!";
}

vector<cartographer::mapping::TrajectoryNode::Data> node_datas_;
void mergeSubmaps(const std::string &path)
{
  //   std::unique_ptr<cartographer::mapping::Submap3D> merge_submap;
  //   cartographer::mapping::proto::RangeDataInserterOptions3D
  //   range_data_inserter_options;
  //   {
  //     range_data_inserter_options.set_hit_probability(0.8);
  //     range_data_inserter_options.set_miss_probability(0.2);
  //     range_data_inserter_options.set_num_free_space_voxels(2);
  //   }
  //   cartographer::mapping::RangeDataInserter3D
  //   range_data_inserter(range_data_inserter_options);

  const Eigen::VectorXf initial_rotational_scan_matcher_histogram = Eigen::VectorXf::Zero(120);
  std::unique_ptr<cartographer::mapping::Submap3D> merge_submap;

  io::ProtoStreamReader stream(path);
  io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  MapById<NodeId, transform::Rigid3d> node_poses;

  for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
       pose_graph_proto.trajectory())
  {
    node_datas_.resize(trajectory_proto.node().size());
    //     LOG(INFO) << node_datas_.size();
    for (const cartographer::mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
      //       ros::Time time_stamp =
      //       ToRos(common::FromUniversal(node_proto.timestamp()));
    }
  }
  cartographer::mapping::proto::SerializedData proto_tmp;
  bool tmp_flag = true;
  int k = 0;
  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      cartographer::mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      cartographer::mapping::proto::TrajectoryNodeData proto_node_data =
          *proto_node->mutable_node_data();

      cartographer::mapping::TrajectoryNode::Data node_data =
          cartographer::mapping::FromProto(proto_node_data);
      //        proto_node->node_id().trajectory_id();
      //        proto_node->node_id().node_index();
      NodeId id(proto_node->node_id().trajectory_id(), proto_node->node_id().node_index());
      transform::Rigid3f node_global_pose = node_poses.at(id).cast<float>();
      Eigen::Vector3f origin_in_tracking(0, 0, 0);
      Eigen::Vector3f origin_in_map = node_global_pose * origin_in_tracking;
      transform::Rigid3d local_pose = node_data.local_pose;
      transform::Rigid3d local2global = node_poses.at(id) * local_pose.inverse();

      {
        TrajectoryNode::Data data;
        data.local_pose = node_poses.at(id);
        data.rotational_scan_matcher_histogram = node_data.rotational_scan_matcher_histogram;
        data.gravity_alignment = local2global.rotation() * node_data.gravity_alignment;
        data.time = node_data.time;
        node_datas_[k++] = (data);
        //           LOG(INFO) << ToRos(data.time);
      }
      continue;
      //         LOG(WARNING) << node_poses.at(id) * local_pose.inverse();
      sensor::PointCloud high_resolution_point_cloud_in_tracking =
          node_data.high_resolution_point_cloud;
      sensor::PointCloud low_resolution_point_cloud_in_tracking =
          node_data.low_resolution_point_cloud;

      sensor::PointCloud high_resolution_point_cloud_in_map =
          sensor::TransformPointCloud(high_resolution_point_cloud_in_tracking, node_global_pose);

      sensor::PointCloud low_resolution_point_cloud_in_map =
          sensor::TransformPointCloud(low_resolution_point_cloud_in_tracking, node_global_pose);
      if (tmp_flag)
      {
        tmp_flag = false;
        // // //           cartographer::mapping::Submap2D tmp2d_submap;
        std::unique_ptr<cartographer::mapping::Submap3D> tmp_submap(new mapping::Submap3D(
            0.2, 1.0, transform::Rigid3d::Identity(), initial_rotational_scan_matcher_histogram));
        merge_submap = std::move(tmp_submap);

        //           Eigen::Quaterniond q = Eigen::Quaterniond::Identity();
        Eigen::Quaterniond q = local2global.rotation() * node_data.gravity_alignment;
        merge_submap->InsertData(origin_in_map, high_resolution_point_cloud_in_map,
                                 low_resolution_point_cloud_in_map,
                                 range_data_inserter_->range_data_inserter, q,
                                 node_data.rotational_scan_matcher_histogram);
      }
      else
      {
        //           Eigen::Quaterniond q = Eigen::Quaterniond::Identity();
        Eigen::Quaterniond q = local2global.rotation() * node_data.gravity_alignment;
        merge_submap->InsertData(origin_in_map, high_resolution_point_cloud_in_map,
                                 low_resolution_point_cloud_in_map,
                                 range_data_inserter_->range_data_inserter, q,
                                 node_data.rotational_scan_matcher_histogram);
      }
    }

    default:
      break;
    }
  }

  return;
  saveToPbstream(merge_submap);
}

void merge2DSubmaps(const std::string &path)
{
  std::unique_ptr<cartographer::mapping::Submap3D> merge_submap;

  io::ProtoStreamReader stream(path);
  io::ProtoStreamDeserializer deserializer(&stream);
  cartographer::mapping::proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  MapById<NodeId, transform::Rigid3d> node_poses;
  for (const cartographer::mapping::proto::Trajectory &trajectory_proto :
       pose_graph_proto.trajectory())
  {
    node_datas_.resize(trajectory_proto.node().size());
    //     LOG(INFO) << node_datas_.size();
    for (const cartographer::mapping::proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      transform::Rigid3d global_pose = transform::ToRigid3(node_proto.pose());
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        global_pose);
      //       ros::Time time_stamp =
      //       ToRos(common::FromUniversal(node_proto.timestamp()));
    }
  }
  cartographer::mapping::proto::SerializedData proto_tmp;
  //   bool tmp_flag = true;
  //   proto::SubmapsOptions2D option;
  //   proto::GridOptions2D grid_option;
  //   grid_option.set_grid_type(1);
  //   grid_option.set_resolution(0.05);
  //
  //   proto::ProbabilityGridRangeDataInserterOptions2D range_data_option;
  //   range_data_option.set_hit_probability(0.55);
  //   range_data_option.set_miss_probability(0.49);
  //   range_data_option.set_insert_free_space(true);
  //
  //   proto::RangeDataInserterOptions inserter_option;
  //   inserter_option.set_range_data_inserter_type(1);
  //   inserter_option.set_allocated_probability_grid_range_data_inserter_options_2d(&range_data_option);
  //   option.set_allocated_grid_options_2d(&grid_option);
  //   option.set_allocated_range_data_inserter_options(&inserter_option);
  //   option.set_little_submap_num_range_data(1e9);
  //   option.set_num_range_data(1e9);
  //   option.set_submap_local_pub_freq(0.001);
  //   mapping::ActiveSubmaps2D submap_2d(option);
  sensor::PointCloud all_point_cloud;
  while (deserializer.ReadNextSerializedData(&proto_tmp))
  {
    switch (proto_tmp.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      cartographer::mapping::proto::Node *proto_node = proto_tmp.mutable_node();
      cartographer::mapping::proto::TrajectoryNodeData proto_node_data =
          *proto_node->mutable_node_data();
      cartographer::mapping::TrajectoryNode::Data node_data =
          cartographer::mapping::FromProto(proto_node_data);
      sensor::PointCloud temp_point_cloud = node_data.filtered_gravity_aligned_point_cloud;
      //         sensor::RangeData temp_range_data =
      //         sensor::RangeData{Eigen::Vector3f::Zero(), temp_point_cloud,
      //         {}};
      NodeId id(proto_node->node_id().trajectory_id(), proto_node->node_id().node_index());
      transform::Rigid3f node_global_pose = node_poses.at(id).cast<float>();
      sensor::PointCloud point_cloud_in_map =
          TransformPointCloud(temp_point_cloud, node_global_pose);
      all_point_cloud.insert(all_point_cloud.end(), point_cloud_in_map.begin(),
                             point_cloud_in_map.end());
    }
    break;
    default:
      break;
    }
  }
  // PointCloud = std::vector<RangefinderPoint>;
  //  struct RangefinderPoint {
  //   public:
  //   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  //
  //   Eigen::Vector3f position;
  //   float intensity;
  // };
}
void readSubmap3DFromPbstream(const std::string &path, std::shared_ptr<Submap3D> &submap_ptr)
{
  cartographer::io::ProtoStreamReader stream(path);
  cartographer::mapping::proto::SerializedData proto;
  submap_ptr = nullptr;
  while (stream.ReadProto(&proto))
  {
    if (proto.data_case() == cartographer::mapping::proto::SerializedData::kSubmap)
    {
      submap_ptr = std::make_shared<cartographer::mapping::Submap3D>(proto.submap().submap_3d());
    }
  }
}
sensor::PointCloud submap3D2PointCloud(std::shared_ptr<Submap3D> submap_ptr)
{
  const HybridGrid &high_grid = submap_ptr->high_resolution_hybrid_grid();
  sensor::PointCloud point_cloud;
  HybridGrid::Iterator it = high_grid.begin();

  while (it != high_grid.end())
  {
    Eigen::Array3i temp = it.GetCellIndex();
    RangefinderPoint point;
    point.position = high_grid.GetCenterOfCell(temp);
    point.intensity = 0;
    point_cloud.push_back(point);
    it.Next();
  }
  LOG(INFO) << point_cloud.size();
  return point_cloud;
}
sensor::PointCloud ToSensorPointCloud(const sensor_msgs::PointCloud2::ConstPtr &msg)
{
  sensor::PointCloudWithIntensities point_cloud1;
  common::Time time;
  std::tie(point_cloud1, time) = ToPointCloudWithIntensities(*msg);
  sensor::PointCloud point_cloud;
  for (auto &it : point_cloud1.points)
  {
    point_cloud.push_back({it.position, it.intensity});
  }
  return point_cloud;
}

void handleMergePointCloud(const sensor_msgs::PointCloud2::ConstPtr &msg)
{
  LOG(INFO) << node_datas_.size();
  cartographer::mapping::TrajectoryNode::Data data;
  if (node_datas_.size() <= 1)
  {
    saveToPbstream(merge_3dmap_);
    exit(1);
  }
  data = node_datas_[0];
  ros::Time save_time = ToRos(data.time);
  ros::Time msg_time = msg->header.stamp;
  while (save_time < msg_time)
  {
    node_datas_.erase(node_datas_.begin());
    data = node_datas_[0];
    save_time = ToRos(data.time);
  }
  if (save_time != msg_time)
  {
    return;
  }
  else
  {
    sensor::PointCloud tmp1 = ToSensorPointCloud(msg);
    sensor::PointCloud tmp = sensor::VoxelFilter(0.2).Filter(tmp1);

    sensor::RangeData range_data_origin_high{{0., 0., 0.}, tmp, {}};

    sensor::RangeData range_data_high =
        sensor::TransformRangeData(range_data_origin_high, data.local_pose.cast<float>());
    //     sensor::PointCloud tmp2 =
    //     sensor::VoxelFilter(2).Filter(range_data_high.returns);
    //     all_point_cloud.insert(all_point_cloud.end(),tmp2.begin(),tmp2.end());
    merge_3dmap_->InsertData(range_data_high, range_data_inserter_->range_data_inserter, 50,
                             data.gravity_alignment, data.rotational_scan_matcher_histogram);
  }
  point_cloud2_msg = ToPointCloud2Message1(data.time, "map", all_point_cloud);
  point_pub.publish(point_cloud2_msg);
  LOG(INFO) << "save_time: " << save_time << " \t msg_time: " << msg_time;
}

bool shutDown(std_srvs::Empty::Request &request, std_srvs::Empty::Response &response)
{
  merge_point_cloud_sub.shutdown();
  saveToPbstream(merge_3dmap_);
  return true;
}
int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  google::SetStderrLogging(google::GLOG_INFO);
  FLAGS_colorlogtostderr = true;
  // 	ScopedRosLogSink ros_log_sink;
  ros::init(argc, argv, "map_manager_tool_node");

  ros::NodeHandle n;

  cartographer_ros::NodeOptions node_options;
  cartographer_ros::TrajectoryOptions trajectory_options;
  std::tie(node_options, trajectory_options) = cartographer_ros::LoadOptions(
      std::string(std::getenv("HOME")) +
          "/jz_project/cartographer_ws/install_isolated/share/cartographer_ros/"
          "configuration_files",
      "backpack_2d_jz_mapping.lua");
  //   pbstream_convertor::PbstreamConvertor
  //   pbstream(node_options.map_builder_options);

  {
    std::unique_ptr<pbstream_convertor::PbstreamConvertor> pbstream(
        new pbstream_convertor::PbstreamConvertor(node_options.map_builder_options));
    pbstream->setFrezon(false);
    if (pbstream->handlePbstream("0428", "new_map1"))
      LOG(INFO) << "save merged pbstream successfully !";
    else
      LOG(WARNING) << "save merged pbstream failed! ";
    LOG(INFO) << "save merged pbstream successfully too!";
  }
  return 1;
  //   std::shared_ptr<Submap3D> submap_ptr;
  //   readSubmap3DFromPbstream("/home/cyy/map/inroom/frames/0/submap.pbstream",submap_ptr);
  //   submap3D2PointCloud(submap_ptr);
  LOG(INFO) << "get poses!";
  cartographer::mapping::proto::RangeDataInserterOptions3D range_data_inserter_options;
  {
    range_data_inserter_options.set_hit_probability(0.8);
    range_data_inserter_options.set_miss_probability(0.2);
    range_data_inserter_options.set_num_free_space_voxels(0);
  }
  //   cartographer::mapping::RangeDataInserter3D
  //   range_data_inserter(range_data_inserter_options);
  range_data_inserter_ = new RangeDataInsert(range_data_inserter_options);
  const Eigen::VectorXf initial_rotational_scan_matcher_histogram = Eigen::VectorXf::Zero(120);

  std::unique_ptr<cartographer::mapping::Submap3D> tmp_submap(new mapping::Submap3D(
      0.2, 1.0, transform::Rigid3d::Identity(), initial_rotational_scan_matcher_histogram));
  merge_3dmap_ = std::move(tmp_submap);
  mergeSubmaps(std::string(std::getenv("HOME")) + "/map/sdpx3/map.pbstream");
  LOG(INFO) << "got poses!";
  merge_point_cloud_sub = n.subscribe("/merge_point_cloud", 1000, handleMergePointCloud);

  srv_ = n.advertiseService("/shutdown_map_tool", shutDown);
  //  MapMapping3D map(n);

  // 	ros::WallTimer wall_timer_ =
  // n.createWallTimer(::ros::WallDuration(0.2),pubTopic); 	ros::Subscriber
  // imu_sub = n.subscribe("mynteye/imu/data_raw",3,handleImu); 	ros::Subscriber
  // point_sub = n.subscribe("LSD_SLAM/cloud",3,handlePointCloud2); 	imu_pub =
  // n.advertise<sensor_msgs::Imu>("/mynteye/imu/data_raw1", 1);
  point_pub = n.advertise<sensor_msgs::PointCloud2>("/point_tmp", 1);
  ros::spin();

  return 1;
}