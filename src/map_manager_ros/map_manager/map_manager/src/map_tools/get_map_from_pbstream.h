/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef GENERATEMAPFROMPBSTREAM_H
#define GENERATEMAPFROMPBSTREAM_H

#include "cartographer_ros_msgs/SaveMapServerRequest.h"
#include "cartographer_ros_msgs/SubmapQuery.h"
#include "map_manager_ulitity.h"
namespace map_manager
{
class GetMapFromPbstream
{
  public:
  GetMapFromPbstream(const std::string &pbstream_path, const std::string &save_path);
  ~GetMapFromPbstream();

  void collectMsg(const PbstreamInfos &infos, cartographer_ros_msgs::SaveMapServerRequest &request);

  void saveMapXml(const std::string &full_path,
                  const std::map<cartographer::mapping::SubmapId,
                                 std::vector<cartographer::mapping::SubmapId>> &constraints,
                  std::map<cartographer::mapping::SubmapId, int> id_num,
                  std::map<int, cartographer_ros_msgs::SubmapID> num_id);
  void saveToXml(const std::string &path, const int &id, const ProbabilityGridInfo &info);
  void saveLandmarkXml(const std::string &full_path, const std::vector<LandmarkInfo> &landmarks);
  void saveMap(const std::string &full_path, const float &resolution, const PbstreamInfos &infos,
               cartographer_ros_msgs::SaveMapServerRequest &request);
};
} // namespace map_manager
#endif // GENERATEMAPFROMPBSTREAM_H
