/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef MAP_MANAGER_MERGE_MAP_H_
#define MAP_MANAGER_MERGE_MAP_H_

#include <cartographer/io/image.h>
#include <cartographer/io/submap_painter.h>
#include <cartographer/mapping/2d/probability_grid.h>
#include <cartographer/mapping/id.h>
#include <cartographer/mapping/internal/2d/pose_graph_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/ceres_scan_matcher_2d.h>
#include <cartographer/mapping/internal/2d/scan_matching/fast_correlative_scan_matcher_2d.h>
#include <cartographer/mapping/internal/optimization/ceres_pose.h>
#include <cartographer/mapping/internal/optimization/cost_functions/landmark_jz_cost_function_2d.h>
#include <cartographer/mapping/pose_graph.h>
#include <cartographer/mapping/trajectory_builder_interface.h>
#include <cartographer/mapping/value_conversion_tables.h>
#include <cartographer/sensor/internal/voxel_filter.h>
#include <cartographer/sensor/point_cloud.h>
#include <cartographer/transform/rigid_transform.h>
#include <cartographer/transform/transform.h>

#include "utility/kd_tree.h"

namespace map_manager
{
class MergeMap
{
  public:
  MergeMap();
  ~MergeMap();
  void start(const std::string &origin_path, const std::string &new_path,
             const cartographer::transform::Rigid3d &init_pose);
  void load(const std::string &origin_path, const std::string &new_path);
  void save(const std::string &new_map_path);
  void crop(const std::vector<std::vector<Eigen::Vector2f>> &rect_points, const bool flag);
  cv::Mat getResultImage() { return merge_map_; };

  private:
  struct SubmapScanMatcher
  {
    const cartographer::mapping::Grid2D *probability_grid;
    std::unique_ptr<cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D>
        fast_correlative_scan_matcher;
  };
  struct AreaTransform
  {
    Eigen::Matrix2f area_R_map;
    Eigen::Vector2f area_t_map;
    Eigen::Vector2f max_area;
  };
  struct SubmapInfo
  {
    Eigen::AlignedBox2i known_cells_box;
    double max[2];
    cv::Mat cells_mat;
  };

  void saveLandmarkToXML(const std::string &path,
                         std::map<std::string, cartographer::transform::Rigid3d> landmarks_);

  void loadSubamp(const std::string &path);

  void loadPbstream(const std::string &path);
  bool match(const cartographer::sensor::PointCloud &node_point_cloud,
             const cartographer::transform::Rigid2d &initial_pose,
             cartographer::transform::Rigid2d &pose_estimate);
  std::unique_ptr<cartographer::transform::Rigid3d> InterpolateOdometry(
      const cartographer::sensor::MapByTime<cartographer::sensor::OdometryData> &odometry_data_,
      const int trajectory_id, const cartographer::common::Time time) const;
  std::unique_ptr<cartographer::transform::Rigid3d> CalculateOdometryBetweenNodes(
      const cartographer::sensor::MapByTime<cartographer::sensor::OdometryData> &odometry_data_,
      const int trajectory_id,
      const cartographer::mapping::optimization::NodeSpec2D &first_node_data,
      const cartographer::mapping::optimization::NodeSpec2D &second_node_data) const;
  void
  Solve(const std::vector<cartographer::mapping::PoseGraphInterface::Constraint> &constraints,
        const cartographer::sensor::MapByTime<cartographer::sensor::OdometryData> &odometry_data_,
        cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                       cartographer::mapping::optimization::NodeSpec2D> &node_data_,
        cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                       cartographer::mapping::optimization::SubmapSpec2D>
            &submap_data_);
  void SolveOld(
      const std::vector<cartographer::mapping::PoseGraphInterface::Constraint> &constraints,
      const cartographer::sensor::MapByTime<cartographer::sensor::OdometryData> &odometry_data_,
      cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                     cartographer::mapping::optimization::NodeSpec2D> &node_data_,
      cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                     cartographer::mapping::optimization::SubmapSpec2D>
          &submap_data_);

  SubmapInfo mergeCurTrajectory(cartographer::mapping::Grid2D *probability_grid);
  void poseGraph();
  std::vector<AreaTransform>
  getTrajectoryArea(const std::vector<std::vector<Eigen::Vector2f>> &rect_points);

  void updatePoints(std::map<std::string, cartographer::transform::Rigid3d> &nav_points);
  void AddLandmarkCostFunctions(
      const std::map<std::string, cartographer::mapping::PoseGraphInterface::LandmarkNode>
          &landmark_nodes,
      const cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                           cartographer::mapping::optimization::NodeSpec2D>
          &node_data,
      cartographer::mapping::MapById<cartographer::mapping::NodeId, std::array<double, 3>> *C_nodes,
      std::map<std::string, cartographer::mapping::optimization::CeresPose> *C_landmarks,
      ceres::Problem *problem, double huber_scale);
  cartographer::sensor::AdaptiveVoxelFilter *adaptive_voxel_filter_;
  cartographer::mapping::Grid2D *submap_probability_grid_;
  SubmapScanMatcher submap_scan_matcher_l_;
  std::shared_ptr<cartographer::mapping::scan_matching::CeresScanMatcher2D> ceres_scan_matcher_;
  cartographer::mapping::ValueConversionTables conversion_tables_;

  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      node_poses_;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::TrajectoryNode::Data>
      node_constant_datas_;
  std::vector<cartographer::mapping::PoseGraph::Constraint> constraints_;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId, cartographer::transform::Rigid3d>
      submap_poses_;
  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> save_submap_slices_;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                 cartographer::mapping::proto::Submap>
      submap_id_to_submap_;
  std::unique_ptr<kd_tree::KDTree> kd_tree_;
  std::unique_ptr<kd_tree::KDTree> new_nodes_kd_tree_;
  std::vector<cartographer::sensor::RangeData> trajectory_ranges_;
  cv::Mat probability_grid_;
  float maxes_[2];
  SubmapInfo origin_map_info_, merge_map_info_, new_submap_info_;

  std::string origin_map_path_, new_map_path_;

  std::unique_ptr<cartographer::mapping::PoseGraph> origin_pose_graph_;
  std::vector<std::unique_ptr<cartographer::mapping::TrajectoryBuilderInterface>>
      trajectory_builders_;
  std::vector<cartographer::mapping::proto::TrajectoryBuilderOptionsWithSensorIds>
      all_trajectory_builder_options_;
  int new_trajectory_id_;
  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::mapping::proto::Node>
      new_node_id_to_node_;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::proto::Submap>
      new_submap_id_to_submap_;
  cartographer::mapping::SubmapId aligned_submap_id_;
  cartographer::transform::Rigid3d aligned_submap_pose_;
  std::vector<cartographer::sensor::ImuData> imu_datas_;
  std::vector<cartographer::sensor::OdometryData> odom_datas_;
  std::vector<cartographer::sensor::LandmarkData> landmark_datas_;
  std::vector<cartographer::sensor::FixedFramePoseData> fixed_frame_datas_;
  cv::Mat merge_map_;
  std::string origin_path1_, new_path_;

  std::map<std::string, cartographer::transform::Rigid3d> origin_landmarks_;
  std::map<std::string, cartographer::transform::Rigid3d> new_landmarks_;
  std::map<std::string, cartographer::transform::Rigid3d> new_landmarks_ori_;
  std::map<std::string, cartographer::transform::Rigid3d> save_landmarks_;
  std::map<std::string, cartographer::transform::Rigid3d> save_landmarks_ori_;

  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      new_origin_node_poses_;

  std::map<std::string /* landmark ID */, cartographer::mapping::PoseGraphInterface::LandmarkNode>
      landmark_nodes;
  cartographer::mapping::MapById<cartographer::mapping::NodeId,
                                 cartographer::mapping::optimization::NodeSpec2D>
      node_data_;
  cartographer::mapping::MapById<cartographer::mapping::SubmapId,
                                 cartographer::mapping::optimization::SubmapSpec2D>
      submap_data_;
  cartographer::mapping::optimization::proto::OptimizationProblemOptions options_;

  //------------------------
  std::map<std::string, cartographer::transform::Rigid3d> landmark_data_;
  //------------------------

  // merge params
  bool debug_mode_;
  int ceres_solver_options_max_num_iterations_;
  bool ceres_solver_options_use_nonmonotonic_steps_;
  int ceres_solver_options_num_threads_;
  float match_ratio_;
  float odometry_rotation_weight_;
  float huber_scale_;
  float local_slam_pose_translation_weight_;
  float local_slam_pose_rotation_weight_;
  float landmark_translation_weight_ratio_;
  float landmark_rotation_weight_ratio_;
  bool log_solver_summary_;
  bool use_online_imu_extrinsics_in_3d_;

  float intra_matcher_tranlation_weight_;
  float intra_matcher_rotation_weight_;
  float same_inter_matcher_tranlation_weight_;
  float same_inter_matcher_rotation_weight_;
  float diff_inter_matcher_translation_weight_;
  float diff_inter_matcher_rotation_weight_;

  int csm_linear_search_window_;
  float csm_angular_search_window_;
  int csm_branch_and_bound_depth_;
  float csm_min_score_;

  // save map params
  float hit_probability_;
  float miss_probability_;
  bool insert_free_space_;

  float map_resolution_;
};
} // namespace map_manager
#endif // MAP_MANAGER_MERGE_MAP_H_
