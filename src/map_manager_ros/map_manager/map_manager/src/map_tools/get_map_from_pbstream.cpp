/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "get_map_from_pbstream.h"

#include <cartographer/io/submap_painter.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "cartographer_ros/msg_conversion.h"
#include "cartographer_ros/ros_map.h"
#include "cartographer_ros/time_conversion.h"
#include "cartographer_ros_msgs/SaveSubmap.h"
#include "cartographer_ros_msgs/StatusCode.h"
using namespace std;
using namespace cartographer;
using namespace cartographer::mapping;
namespace map_manager
{
GetMapFromPbstream::GetMapFromPbstream(const std::string &pbstream_path,
                                       const std::string &save_path)
{
  PbstreamInfos infos;
  readPosesFromPbstream(pbstream_path, infos);
  cartographer_ros_msgs::SaveMapServerRequest request;
  collectMsg(infos, request);
  saveMap(save_path, 0.05, infos, request);
}

GetMapFromPbstream::~GetMapFromPbstream() {}

void GetMapFromPbstream::collectMsg(const PbstreamInfos &infos,
                                    cartographer_ros_msgs::SaveMapServerRequest &request)
{
  uint submap_cnt = 0;
  std::map<cartographer::mapping::SubmapId, int> submap_submapId;
  std::map<int, cartographer::mapping::SubmapId> submapId_submap;
  LOG(INFO) << "GetAllSubmapData";
  for (const auto &submap_id_data : infos.submap_id_to_submap)
  {
    cartographer::mapping::SubmapId submap_id = submap_id_data.id;
    submap_submapId.insert(std::pair<cartographer::mapping::SubmapId, int>(submap_id, submap_cnt));
    submapId_submap.insert(std::pair<int, cartographer::mapping::SubmapId>(submap_cnt, submap_id));
    submap_cnt++;
  }
  std::vector<std::set<int>> connections;
  std::map<cartographer::mapping::NodeId, int> node_submapId;
  LOG(INFO) << "constraints";
  std::vector<cartographer::mapping::PoseGraphInterface::Constraint> constraints =
      infos.constraints;
  // LOG(INFO)<<constraints.size()<<std::endl;
  connections.resize(submap_cnt);

  // determine which submap each node(intra) belongs to
  for (uint constraints_id = 0; constraints_id < constraints.size(); constraints_id++)
  {
    cartographer::mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
    if (constraint.tag == cartographer::mapping::PoseGraph::Constraint::INTRA_SUBMAP &&
        submap_submapId.find(constraint.submap_id) != submap_submapId.end())
    {
      node_submapId.insert(std::pair<cartographer::mapping::NodeId, int>(
          constraint.node_id, submap_submapId[constraint.submap_id]));
    }
  }

  // add adjacent connections
  for (uint i = 0; i < submap_cnt - 1; i++)
  {
    connections[i].insert(i + 1);
    connections[i + 1].insert(i);
  }

  // add connections induced by nodes(inter)
  for (uint constraints_id = 0; constraints_id < constraints.size(); constraints_id++)
  {
    cartographer::mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
    if (constraint.tag == cartographer::mapping::PoseGraph::Constraint::INTER_SUBMAP &&
        submap_submapId.find(constraint.submap_id) != submap_submapId.end() &&
        node_submapId.find(constraint.node_id) != node_submapId.end())
    {
      connections[submap_submapId[constraint.submap_id]].insert(node_submapId[constraint.node_id]);
      connections[node_submapId[constraint.node_id]].insert(submap_submapId[constraint.submap_id]);
    }
  }

  // std::vector<cartographer::mapping::PoseGraph::Constraint> constraints =
  // map_builder_->pose_graph()->constraints();
  std::map<cartographer::mapping::SubmapId, std::set<cartographer::mapping::SubmapId>>
      classfied_constraints, node_constraints;
  for (uint i = 0; i < connections.size(); i++)
  {
    cartographer::mapping::SubmapId id1 = submapId_submap.find(i)->second;
    for (auto id : connections[i])
    {
      cartographer::mapping::SubmapId id2 = submapId_submap.find(id)->second;
      classfied_constraints[id1].insert(id2);
    }
  }

  {
    LOG(INFO) << "GetAllSubmapData";

    ValueConversionTables conversion_tables_;
    for (const auto &submap_id_data : infos.submap_id_to_submap)
    {
      const std::shared_ptr<const Submap2D> submap_ptr =
          std::make_shared<const Submap2D>(submap_id_data.data.submap_2d(), &conversion_tables_);
      const cartographer::transform::Rigid3d submap_pose = infos.submap_poses.at(submap_id_data.id);
      //       ::cartographer::mapping::PoseGraph::SubmapData submapData =
      //       submap_id_data.data;
      cartographer_ros_msgs::SaveSubmap submap_info;
      submap_info.id.trajectory_id = submap_id_data.id.trajectory_id;
      submap_info.id.submap_index = submap_id_data.id.submap_index;

      submap_info.pose_in_map.position.x = submap_pose.translation().x();
      submap_info.pose_in_map.position.y = submap_pose.translation().y();
      submap_info.pose_in_map.position.z = submap_pose.translation().z();

      submap_info.pose_in_map.orientation.x = submap_pose.rotation().x();
      submap_info.pose_in_map.orientation.y = submap_pose.rotation().y();
      submap_info.pose_in_map.orientation.z = submap_pose.rotation().z();
      submap_info.pose_in_map.orientation.w = submap_pose.rotation().w();

      submap_info.pose_in_local.position.x = submap_ptr->local_pose().translation().x();
      submap_info.pose_in_local.position.y = submap_ptr->local_pose().translation().y();
      submap_info.pose_in_local.position.z = submap_ptr->local_pose().translation().z();

      submap_info.pose_in_local.orientation.x = submap_ptr->local_pose().rotation().x();
      submap_info.pose_in_local.orientation.y = submap_ptr->local_pose().rotation().y();
      submap_info.pose_in_local.orientation.z = submap_ptr->local_pose().rotation().z();
      submap_info.pose_in_local.orientation.w = submap_ptr->local_pose().rotation().w();

      submap_info.num_cells[0] = submap_ptr->grid()->limits().cell_limits().num_x_cells;
      submap_info.num_cells[1] = submap_ptr->grid()->limits().cell_limits().num_y_cells;
      submap_info.max[0] = submap_ptr->grid()->limits().max()[0];
      submap_info.max[1] = submap_ptr->grid()->limits().max()[1];
      for (auto &constraint : classfied_constraints[submap_id_data.id])
      {
        cartographer_ros_msgs::SubmapID temp;
        temp.trajectory_id = constraint.trajectory_id;
        temp.submap_index = constraint.submap_index;
        submap_info.constraints.push_back(temp);
      }
      cartographer_ros_msgs::SubmapQuery query;
      query.request.trajectory_id = submap_id_data.id.trajectory_id;
      query.request.submap_index = submap_id_data.id.submap_index;

      // HandleSubmapQuery
      {
        cartographer::mapping::proto::SubmapQuery::Response response_proto;
        submap_ptr->ToResponseProto(submap_pose, &response_proto);
        query.response.submap_version = response_proto.submap_version();
        for (const auto &texture_proto : response_proto.textures())
        {
          query.response.textures.emplace_back();
          auto &texture = query.response.textures.back();
          texture.cells.insert(texture.cells.begin(), texture_proto.cells().begin(),
                               texture_proto.cells().end());
          texture.width = texture_proto.width();
          texture.height = texture_proto.height();
          texture.resolution = texture_proto.resolution();
          texture.slice_pose = cartographer_ros::ToGeometryMsgPose(
              cartographer::transform::ToRigid3(texture_proto.slice_pose()));
        }

        query.response.status.message = "Success.";
        query.response.status.code = cartographer_ros_msgs::StatusCode::OK;
      }
      submap_info.submap_version = query.response.submap_version;
      submap_info.textures = query.response.textures;
      request.submaps.push_back(submap_info);
    }
  }

  {
    std::string data;
    LOG(INFO) << "GetLandmarkInfos";
    std::set<string> landmarks;
    for (const sensor::LandmarkData &landmark : infos.landmark_datas)
    {
      if (landmark.landmark_observations.empty())
      {
        LOG(WARNING) << "landmark would not be saved.";
        continue;
      }
      std::string ns;
      std::string id_s = landmark.landmark_observations[0].id;
      int id_i = std::stoi(id_s);
      if (id_i >= 5000000)
      {
        id_i = id_i - 5000000;
        ns = "Scenes";
      }
      else if (id_i >= 1000000)
      {
        id_i = id_i - 1000000;
        ns = "Lasermarks";
      }
      else if (id_i >= 44032)
        ns = "Visualmarks_4x4";
      else
        ns = "Visualmarks";
      cartographer_ros_msgs::LandmarkNewEntry entry;
      entry.ns = ns;
      entry.id = std::to_string(id_i);
      entry.visible = 1;

      entry.pole_radius = -1.;
      if (!landmark.landmark_observations[0].observation_points.empty())
        entry.pole_radius = landmark.landmark_observations[0].pole_radius;
      if (infos.landmark_poses.find(id_s) == infos.landmark_poses.end()) continue;
      if (landmarks.count(id_s) != 0) continue;
      landmarks.insert(id_s);
      cartographer::transform::Rigid3d landmark_global_pose = infos.landmark_poses.at(id_s);
      entry.tracking_from_landmark_transform.position.x = landmark_global_pose.translation().x();
      entry.tracking_from_landmark_transform.position.y = landmark_global_pose.translation().y();
      entry.tracking_from_landmark_transform.position.z = landmark_global_pose.translation().z();

      entry.tracking_from_landmark_transform.orientation.x = landmark_global_pose.rotation().x();
      entry.tracking_from_landmark_transform.orientation.y = landmark_global_pose.rotation().y();
      entry.tracking_from_landmark_transform.orientation.z = landmark_global_pose.rotation().z();
      entry.tracking_from_landmark_transform.orientation.w = landmark_global_pose.rotation().w();
      request.landmarks.push_back(entry);
    }
  }

  {
    LOG(INFO) << "GetTrajectoryNodes";
    for (auto node_pose : infos.node_datas)
    {
      geometry_msgs::PoseStamped msg;
      msg.header.stamp = cartographer_ros::ToRos(node_pose.data.time);
      cartographer::transform::Rigid3d node_global_pose = infos.node_poses.at(node_pose.id);
      msg.pose.position.x = node_global_pose.translation().x();
      msg.pose.position.y = node_global_pose.translation().y();
      msg.pose.position.z = node_global_pose.translation().z();
      msg.pose.orientation.w = node_global_pose.rotation().w();
      msg.pose.orientation.x = node_global_pose.rotation().x();
      msg.pose.orientation.y = node_global_pose.rotation().y();
      msg.pose.orientation.z = node_global_pose.rotation().z();
      request.nodes_poses_with_stamp.push_back(msg);
    }
  }
}

void GetMapFromPbstream::saveMapXml(const string &full_path,
                                    const map<SubmapId, vector<SubmapId>> &constraints,
                                    std::map<cartographer::mapping::SubmapId, int> id_num,
                                    std::map<int, cartographer_ros_msgs::SubmapID> num_id)
{
  ::cartographer::transform::Rigid3d::Vector translation;
  ::cartographer::transform::Rigid3d::Quaternion quaternion;
  std::string data;
  boost::property_tree::ptree p_top;
  boost::property_tree::ptree p_map;
  boost::property_tree::ptree p_node_list;
  int submap_cnt = constraints.size();
  for (auto &constraint : constraints)
  {
    SubmapId id = constraint.first;
    if (id_num.find(id) == id_num.end())
    {
      continue;
    }
    boost::property_tree::ptree p_node;
    p_node.put("id", id_num.find(id)->second);

    boost::property_tree::ptree p_neighbour_list;
    std::set<int> connected_ids;
    for (auto &pair : constraint.second)
    {
      std::map<cartographer::mapping::SubmapId, int>::iterator it;
      it = id_num.find(pair);
      connected_ids.insert(it->second);
    }
    for (auto &pair : connected_ids)
    {
      boost::property_tree::ptree p_neighbour;
      p_neighbour.put("id", pair);
      //       LOG(WARNING) << pair <<", " << num_id.find(pair)->second;
      cartographer_ros_msgs::SubmapID msg_id = num_id.find(pair)->second;
      SubmapId connect_id(msg_id.trajectory_id, msg_id.submap_index);

      ::cartographer::transform::Rigid3d neighbour2current =
          cartographer::transform::Rigid3d::Identity();
      translation = neighbour2current.translation();
      quaternion = neighbour2current.rotation();
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      data = data + std::to_string(quaternion.w()) + " ";
      data = data + std::to_string(quaternion.x()) + " ";
      data = data + std::to_string(quaternion.y()) + " ";
      data = data + std::to_string(quaternion.z());
      p_neighbour.put("transform", data);
      data.clear();
      p_neighbour.put("reachable", true);

      p_neighbour_list.add_child("neighbour", p_neighbour);
    }

    p_node.add_child("neighbour_list", p_neighbour_list);

    p_node_list.add_child("node", p_node);
  }
  p_map.add_child("node_list", p_node_list);
  boost::property_tree::ptree p_property;
  p_property.put("node_count", submap_cnt);

  p_map.add_child("property", p_property);

  p_top.add_child("map", p_map);
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(full_path + "/map.xml", p_top, std::locale(), setting);
}

void GetMapFromPbstream::saveLandmarkXml(const string &full_path,
                                         const vector<LandmarkInfo> &landmarks)
{
  std::string data;
  boost::property_tree::ptree p_landmark_info;
  boost::property_tree::ptree p_neighbour_list;

  for (const auto &landmark : landmarks)
  {
    cartographer::transform::Rigid3d::Vector translation;
    cartographer::transform::Rigid3d::Quaternion quaternion;
    p_landmark_info.put("ns", landmark.ns);
    p_landmark_info.put("id", landmark.id);
    p_landmark_info.put("pole_radius", landmark.pole_radius);
    p_landmark_info.put("visible", landmark.visible);
    cartographer::transform::Rigid3d landmark_pose = landmark.pose;
    translation = landmark_pose.translation();
    quaternion = landmark_pose.rotation();
    for (int i = 0; i < 3; i++)
      data = data + std::to_string(translation[i]) + " ";
    for (int i = 0; i < 4; i++)
      data = data + std::to_string(quaternion.coeffs()[i]) + " ";
    p_landmark_info.put("transform", data);
    data.clear();
    data = std::to_string(landmark.translation_in_image(0)) + " " +
           std::to_string(landmark.translation_in_image(1));
    p_landmark_info.put("translation_in_image", data);
    p_neighbour_list.add_child("landmark", p_landmark_info);
    data.clear();
  }
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  boost::property_tree::write_xml(full_path + "/landmark.xml", p_neighbour_list, std::locale(),
                                  setting);
}

void GetMapFromPbstream::saveToXml(const string &path, const int &id,
                                   const ProbabilityGridInfo &info)
{
  boost::property_tree::ptree p_map;
  boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
  std::string folderstring_submap = path + std::to_string(id) + "/";
  string system_command = "mkdir -p " + folderstring_submap;
  LOG(INFO) << system_command;
  int status = system(system_command.c_str());
  checkSystemStatus(system_command, status);
  CHECK(status != -1);

  string str = "/.";
  const char *compare_reslut;
  compare_reslut = strstr(path.c_str(), str.c_str());

  p_map.put("id", id);

  cartographer::transform::Rigid3d global_pose = info.global_pose;
  Eigen::Vector3d translation = global_pose.translation();
  Eigen::Quaterniond quaternion = global_pose.rotation();
  string data;
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(translation[i]) + " ";
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(quaternion.coeffs()[i]) + " ";
  data = data + std::to_string(quaternion.coeffs()[3]);
  p_map.put("pose", data);
  data.clear();

  cartographer::transform::Rigid3d local_pose = info.local_pose;
  translation = local_pose.translation();
  quaternion = local_pose.rotation();
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(translation[i]) + " ";
  for (int i = 0; i < 3; i++)
    data = data + std::to_string(quaternion.coeffs()[i]) + " ";
  data = data + std::to_string(quaternion.coeffs()[3]);
  p_map.put("local_pose", data);
  data.clear();

  p_map.put("num_range_data", info.num_range_data);

  p_map.put("finished", info.finished);

  boost::property_tree::ptree p_probability_grid;
  uint num_x_cells, num_y_cells;

  p_probability_grid.put("resolution", info.resolution);
  data = std::to_string(info.max[0]) + " " + std::to_string(info.max[1]);
  p_probability_grid.put("max", data);
  data.clear();
  num_x_cells = info.num_cells[0];
  num_y_cells = info.num_cells[1];
  data = std::to_string(num_x_cells) + " " + std::to_string(num_y_cells);
  p_probability_grid.put("num_cells", data);
  data.clear();
  data = std::to_string(0) + " " + std::to_string(0) + " " + std::to_string(num_x_cells - 1) + " " +
         std::to_string(num_y_cells - 1);
  p_probability_grid.put("known_cells_box", data);
  p_map.add_child("probability_grid", p_probability_grid);
  boost::property_tree::write_xml(folderstring_submap + "data.xml", p_map, std::locale(), setting);
}

void GetMapFromPbstream::saveMap(const std::string &full_path, const float &resolution,
                                 const PbstreamInfos &infos,
                                 cartographer_ros_msgs::SaveMapServerRequest &request)
{
  Eigen::Vector2d max_box_;
  // save map with nodes
  {
    constexpr int kInitialSubmapSize = 20;
    mapping::ValueConversionTables conversion_tables;
    std::unique_ptr<ProbabilityGrid> origin_merge_grid(new ProbabilityGrid(
        mapping::MapLimits(resolution,
                           Eigen::Vector2d(0., 0.) +
                               0.5 * kInitialSubmapSize * resolution * Eigen::Vector2d::Ones(),
                           mapping::CellLimits(kInitialSubmapSize, kInitialSubmapSize)),
        &conversion_tables));
    mapping::proto::ProbabilityGridRangeDataInserterOptions2D options;
    options.set_hit_probability(0.55);
    options.set_miss_probability(0.49);
    options.set_insert_free_space(true);
    std::shared_ptr<mapping::RangeDataInserterInterface> origin_range_data_inserter(
        new mapping::ProbabilityGridRangeDataInserter2D(options));
    for (const auto &it : infos.node_datas)
    {
      transform::Rigid3d gravity2map =
          infos.node_poses.at(it.id) *
          transform::Rigid3d::Rotation(it.data.gravity_alignment.inverse());
      Eigen::Vector2f origin(gravity2map.translation().x(), gravity2map.translation().y());
      sensor::PointCloud points_in_global = sensor::TransformPointCloud(
          it.data.filtered_gravity_aligned_point_cloud, gravity2map.cast<float>());
      sensor::RangeData range{{origin(0), origin(1), 0}, points_in_global, {}};
      origin_range_data_inserter->Insert(range, origin_merge_grid.get());
    }
    auto tmp_grid = origin_merge_grid->ComputeCroppedGrid();
    double max[2];
    max[0] = tmp_grid->limits().max()[0];
    max[1] = tmp_grid->limits().max()[1];
    double resolution = tmp_grid->limits().resolution();
    const std::vector<uint16_t> &cells = tmp_grid->cells();
    int num_x_cells = tmp_grid->limits().cell_limits().num_x_cells;
    int num_y_cells = tmp_grid->limits().cell_limits().num_y_cells;
    cv::Mat cells_mat = cv::Mat::zeros(num_y_cells, num_x_cells, CV_16UC1);
    for (int i = 0; i < num_y_cells; i++)
      for (int j = 0; j < num_x_cells; j++)
      {
        uint16_t value = cells[i * num_x_cells + j];
        cells_mat.at<uint16_t>(i, j) = value;
      }
    ProbabilityGridInfo merge_info;
    merge_info.resolution = resolution;
    merge_info.num_cells[0] = num_x_cells;
    merge_info.num_cells[1] = num_y_cells;
    merge_info.max[0] = max[0];
    merge_info.max[1] = max[1];
    merge_info.num_range_data = infos.node_datas.size();
    merge_info.finished = true;
    saveToXml(full_path + "/frames/", 0, merge_info);
    string img_name = full_path + "/frames/0/probability_grid.png";
    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(0);
    cv::imwrite(img_name, cells_mat, compression_params);
    cv::Mat show_img = image16ToImage8(cells_mat);
    cv::imwrite(full_path + "/map.png", show_img, compression_params);
    max_box_ = Eigen::Vector2d(max[0], max[1]);
    {
      boost::property_tree::ptree p_map, p_top;
      std::string data;
      const int width = show_img.cols;
      const int height = show_img.rows;
      data = to_string(height);
      p_map.put("width", data);
      data.clear();
      data = to_string(width);
      p_map.put("height", data);
      data.clear();
      data = to_string(resolution);
      p_map.put("resolution", data);
      data.clear();
      double x = max[0] / resolution;
      double y = max[1] / resolution;
      cartographer::transform::Rigid3d::Vector translation(y, x, 0);
      cartographer::transform::Rigid3d::Quaternion quaternion(0, -sqrt(2) / 2, sqrt(2) / 2, 0);
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(translation[i]) + " ";
      for (int i = 0; i < 3; i++)
        data = data + std::to_string(quaternion.coeffs()[i]) + " ";
      data = data + std::to_string(quaternion.coeffs()[3]);
      p_map.put("pose", data);
      data.clear();
      p_top.add_child("mapPng", p_map);

      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      boost::property_tree::write_xml(full_path + "/map_data.xml", p_top, std::locale(), setting);
    }
  }
  std::map<SubmapId, std::vector<SubmapId>> constraints;
  int num = 0;
  std::map<cartographer::mapping::SubmapId, int> id_num1;
  std::map<int, cartographer_ros_msgs::SubmapID> num_id1;
  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> save_submap_slices_;
  std::vector<cartographer::transform::Rigid3d> local_poses_;
  for (auto &it : request.submaps)
  {
    cartographer_ros_msgs::SaveSubmap submap;
    submap = it;
    cartographer::mapping::SubmapId id(submap.id.trajectory_id, submap.id.submap_index);
    cartographer_ros_msgs::SubmapID msg_id;
    msg_id.trajectory_id = id.trajectory_id;
    msg_id.submap_index = id.submap_index;
    ::cartographer::io::SubmapSlice &slice = save_submap_slices_[id];
    slice.pose = cartographer_ros::ToRigid3d(submap.pose_in_map);
    {
      auto fetched_textures = absl::make_unique<::cartographer::io::SubmapTextures>();
      fetched_textures->version = submap.submap_version;
      for (const auto &texture : submap.textures)
      {
        const std::string compressed_cells(texture.cells.begin(), texture.cells.end());
        fetched_textures->textures.emplace_back(::cartographer::io::SubmapTexture{
            ::cartographer::io::UnpackTextureData(compressed_cells, texture.width, texture.height),
            texture.width, texture.height, texture.resolution,
            cartographer_ros::ToRigid3d(texture.slice_pose)});
      }
      slice.version = fetched_textures->version;
      const auto fetched_texture = fetched_textures->textures.begin();
      slice.width = fetched_texture->width;
      slice.height = fetched_texture->height;
      slice.slice_pose = fetched_texture->slice_pose;
      slice.resolution = fetched_texture->resolution;
      slice.cairo_data.clear();

      slice.surface = ::cartographer::io::DrawTexture(
          fetched_texture->pixels.intensity, fetched_texture->pixels.alpha, fetched_texture->width,
          fetched_texture->height, &slice.cairo_data);
    }

    num_id1[num] = msg_id;
    id_num1[id] = num;
    num++;
    for (auto &constraint : submap.constraints)
    {
      constraints[id].push_back(SubmapId(constraint.trajectory_id, constraint.submap_index));
    }
    local_poses_.push_back(cartographer_ros::ToRigid3d(submap.pose_in_local));
  }
  LOG(INFO) << "Save show map done!";

  // map_server
  {
    auto result = io::PaintSubmapSlices(save_submap_slices_, resolution);
    string system_command = "mkdir -p " + full_path + "/map_server";
    LOG(INFO) << system_command;
    int status = system(system_command.c_str());
    checkSystemStatus(system_command, status);
    CHECK(status != -1);
    ::cartographer::io::StreamFileWriter pgm_writer(full_path + "/map_server/map.pgm");
    ::cartographer::io::Image image(std::move(result.surface));
    image.Rotate90DegreesClockwise();
    image.Rotate90DegreesClockwise();
    image.Rotate90DegreesClockwise();
    cartographer_ros::WritePgm(image, resolution, &pgm_writer);
    LOG(INFO) << result.origin.x() * resolution << ", " << result.origin.y() * resolution << ", "
              << image.height() * resolution << ", " << image.width() * resolution;
    const Eigen::Vector2d origin(-result.origin.x() * resolution, result.origin.y() * resolution);
    ::cartographer::io::StreamFileWriter yaml_writer(full_path + "/map_server/map.yaml");
    const std::string output =
        "image: " + pgm_writer.GetFilename() + "\n" + "resolution: " + std::to_string(resolution) +
        "\n" + "origin: [" + std::to_string(origin.x()) + ", " + std::to_string(origin.y()) + ", " +
        std::to_string(-M_PI / 2) + "]\nnegate: 0\noccupied_thresh: 0.65\nfree_thresh: 0.196\n";
    yaml_writer.Write(output.data(), output.size());
  }
  LOG(INFO) << "Save map_server_files done!";

  LOG(INFO) << "Start save full_path map.xml!";
  {
    std::map<SubmapId, std::vector<SubmapId>> constraints1;
    std::map<cartographer::mapping::SubmapId, int> id_num;
    std::map<int, cartographer_ros_msgs::SubmapID> num_id;
    for (int i = 0; i < 1; i++)
    {
      SubmapId id_temp(0, i);
      cartographer_ros_msgs::SubmapID msg_id;
      msg_id.trajectory_id = id_temp.trajectory_id;
      msg_id.submap_index = id_temp.submap_index;
      num_id[i] = msg_id;
      id_num[id_temp] = i;
      constraints1[id_temp].push_back(id_temp);
    }
    saveMapXml(full_path, constraints1, id_num, num_id);
  }

  LOG(INFO) << "Start save landmarks!";
  std::vector<LandmarkInfo> landmarks;
  //   double resolution = 0.05;
  for (auto &it : request.landmarks)
  {
    LandmarkInfo landmark;
    landmark.ns = it.ns;
    landmark.id = it.id;
    landmark.pole_radius = it.pole_radius;
    landmark.pose = cartographer_ros::ToRigid3d(it.tracking_from_landmark_transform);
    landmark.visible = it.visible;
    landmark.rotation_weight = it.rotation_weight;
    landmark.translation_weight = it.translation_weight;
    landmark.translation_in_image(1) = (max_box_[0] - landmark.pose.translation().x()) / resolution;
    landmark.translation_in_image(0) = (max_box_[1] - landmark.pose.translation().y()) / resolution;
    landmarks.push_back(landmark);
  }

  if (landmarks.size() != 0) saveLandmarkXml(full_path, landmarks);

  LOG(INFO) << "Start save map pixel image!";
  adjustMapPixel(full_path + "/", 0, true);
  adjustMapPixel(full_path + "/", 3, true);
  adjustMapPixel(full_path + "/", 5, true);
  autoSetIgnoreArea(full_path + "/");
  LOG(INFO) << "Save done!";
}

} // namespace map_manager
