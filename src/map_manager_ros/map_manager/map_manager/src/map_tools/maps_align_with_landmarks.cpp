/*
 * Copyright 2022 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "maps_align_with_landmarks.h"

#include <ros/package.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <opencv2/opencv.hpp>

#include "../utility/point_optimization_ceres.h"
#include "cartographer/common/ceres_solver_options.h"
#include "cartographer/io/internal/mapping_state_serialization.h"
#include "cartographer/io/proto_stream.h"
#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer/mapping/internal/optimization/cost_functions/spa_cost_function_2d.h"
#include "cartographer_ros/node_options.h"
#include "get_map_from_pbstream.h"
using namespace std;
using namespace cartographer::mapping;
namespace cartographer
{
MapsAlignWithLandmarks::MapsAlignWithLandmarks(const std::string &path1, const std::string &path2,
                                               bool frozen)
{
  options_.mutable_ceres_solver_options()->set_max_num_iterations(10);
  options_.mutable_ceres_solver_options()->set_use_nonmonotonic_steps(false);
  options_.mutable_ceres_solver_options()->set_num_threads(2);
  options_.set_odometry_rotation_weight(1e1);
  options_.set_huber_scale(1e1);
  options_.set_local_slam_pose_translation_weight(1e5);
  options_.set_local_slam_pose_rotation_weight(1e5);
  options_.set_log_solver_summary(false);
  options_.set_use_online_imu_extrinsics_in_3d(false);
  PbstreamPosesInfos infos1, infos2;
  int first_max_trajectory_id = 0;
  int second_max_trajectory_id = 0;
  readPosesFromPbstream(path1, 0, infos1, first_max_trajectory_id);
  readPosesFromPbstream(path2, first_max_trajectory_id + 1, infos2, second_max_trajectory_id);
  loadLandmarksFromXml(path2, "Lasermarks", landmarks_in_xml_);
  // remap landmark id
  {
    if (landmarks_in_xml_.empty())
    {
      LOG(WARNING) << "landmark.xml is empty.";
      return;
    }
    for (auto it : infos2.landmark_poses)
    {
      string id = it.first;
      float dist = 1e9;
      string id_in_xml = "";
      for (auto landmark_in_xml : landmarks_in_xml_)
      {
        transform::Rigid3d landmark_in_xml_pose = landmark_in_xml.second;
        transform::Rigid3d relative_pose = it.second.inverse() * landmark_in_xml_pose;
        if (relative_pose.translation().norm() < 1e-2 && relative_pose.translation().norm() < dist)
        {
          dist = relative_pose.translation().norm();
          id_in_xml = landmark_in_xml.first;
        }
      }
      if (id_in_xml != "")
      {
        landmarks_id_remap_[id] = id_in_xml;
        LOG(INFO) << "origin id " << id << " --> xml_id " << id_in_xml;
      }
    }
  }

  transform::Rigid2d cur2ref =
      getRelativePoseFromLandmarks(infos1.landmark_poses, infos2.landmark_poses);
  LOG(INFO) << cur2ref;
  //   cur2ref = cur2ref.inverse();
  MapById<NodeId, optimization::NodeSpec2D> node_data_;
  MapById<SubmapId, optimization::SubmapSpec2D> submap_data_;
  sensor::MapByTime<sensor::OdometryData> odometry_data_;
  std::map<std::string, transform::Rigid3d> landmark_data_;
  std::map<std::string, PoseGraphInterface::LandmarkNode> landmark_nodes;
  for (const auto &it : infos1.node_datas)
  {
    optimization::NodeSpec2D node_data{it.data.time, transform::Project2D(it.data.local_pose),
                                       transform::Project2D(infos1.node_poses.at(it.id)),
                                       it.data.gravity_alignment};
    node_data_.Insert(it.id, node_data);
  }
  LOG(INFO) << node_data_.size();
  for (const auto &it : infos2.node_datas)
  {
    optimization::NodeSpec2D node_data{it.data.time, transform::Project2D(it.data.local_pose),
                                       cur2ref * transform::Project2D(infos2.node_poses.at(it.id)),
                                       it.data.gravity_alignment};
    node_data_.Insert(it.id, node_data);
  }
  LOG(INFO) << node_data_.size();
  for (const auto &it : infos1.submap_poses)
  {
    submap_data_.Insert(it.id, optimization::SubmapSpec2D{transform::Project2D(it.data)});
  }
  LOG(INFO) << submap_data_.size();
  for (const auto &it : infos2.submap_poses)
  {
    submap_data_.Insert(it.id, optimization::SubmapSpec2D{cur2ref * transform::Project2D(it.data)});
  }
  LOG(INFO) << submap_data_.size();
  for (const auto &it : infos1.odom_datas)
    odometry_data_.Append(it.first, it.second);
  for (const auto &it : infos2.odom_datas)
    odometry_data_.Append(it.first, it.second);

  for (const auto &landmark_data : infos1.landmark_datas)
  {
    for (const sensor::LandmarkObservation &observation :
         landmark_data.second.landmark_observations)
    {
      landmark_nodes[observation.id].landmark_observations.emplace_back(
          PoseGraphInterface::LandmarkNode::LandmarkObservation{
              landmark_data.first, landmark_data.second.time,
              observation.landmark_to_tracking_transform, observation.tranking_to_local_transform,
              //                         observation.translation_weight,
              //                         observation.rotation_weight,
              1e3, 1e-9, observation.observation_points, observation.pole_radius});
    }
  }
  LOG(INFO) << landmark_nodes.size();
  for (const auto &landmark_data : infos2.landmark_datas)
  {
    for (const sensor::LandmarkObservation &observation :
         landmark_data.second.landmark_observations)
    {
      if (landmarks_id_remap_.find(observation.id) == landmarks_id_remap_.end()) continue;
      landmark_nodes[landmarks_id_remap_[observation.id]].landmark_observations.emplace_back(
          PoseGraphInterface::LandmarkNode::LandmarkObservation{
              landmark_data.first, landmark_data.second.time,
              observation.landmark_to_tracking_transform, observation.tranking_to_local_transform,
              //                         observation.translation_weight,
              //                         observation.rotation_weight,
              1e3, 1e-9, observation.observation_points, observation.pole_radius});
    }
  }
  {
    sensor::PointCloud all_cloud;
    cv::Mat debug_img(1000, 1000, CV_8UC3, cv::Scalar(255, 255, 255));
    for (auto it : infos1.node_datas)
    {
      optimization::NodeSpec2D spec_2d = node_data_.at(it.id);
      transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(spec_2d.gravity_alignment);
      transform::Rigid3d correct_pose =
          transform::Embed3D(spec_2d.global_pose_2d) * gravity_alignment.inverse();
      sensor::PointCloud cur_cloud = it.data.filtered_gravity_aligned_point_cloud;
      sensor::PointCloud points_in_map =
          sensor::TransformPointCloud(cur_cloud, correct_pose.cast<float>());
      all_cloud.insert(all_cloud.end(), points_in_map.begin(), points_in_map.end());
    }
    sensor::PointCloud filtered_cloud = sensor::VoxelFilter(0.05).Filter(all_cloud);
    for (const sensor::RangefinderPoint &p : filtered_cloud)
    {
      Eigen::Vector3f p2 = p.position;
      float img_y = p2.x() / 0.1 + 500;
      float img_x = p2.y() / 0.1 + 500;
      cv::circle(debug_img, {img_x, img_y}, 0.5, cv::Scalar(0, 255, 0));
    }
    all_cloud.clear();
    filtered_cloud.clear();
    for (auto it : infos2.node_datas)
    {
      optimization::NodeSpec2D spec_2d = node_data_.at(it.id);
      transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(spec_2d.gravity_alignment);
      transform::Rigid3d correct_pose =
          transform::Embed3D(spec_2d.global_pose_2d) * gravity_alignment.inverse();
      sensor::PointCloud cur_cloud = it.data.filtered_gravity_aligned_point_cloud;
      sensor::PointCloud points_in_map =
          sensor::TransformPointCloud(cur_cloud, correct_pose.cast<float>());
      all_cloud.insert(all_cloud.end(), points_in_map.begin(), points_in_map.end());
    }
    filtered_cloud = sensor::VoxelFilter(0.05).Filter(all_cloud);

    {
      for (const sensor::RangefinderPoint &p : filtered_cloud)
      {
        Eigen::Vector3f p2 = p.position;
        float img_y = p2.x() / 0.1 + 500;
        float img_x = p2.y() / 0.1 + 500;
        cv::circle(debug_img, {img_x, img_y}, 0.5, cv::Scalar(255, 0, 0));
      }
      //       cv::imshow("debug",debug_img);
      //       cv::waitKey(0);
      cv::imwrite(string(getenv("HOME")) + "/merge_init.png", debug_img);
    }
  }

  LOG(INFO) << landmark_nodes.size();

  vector<PoseGraphInterface::Constraint> all_constraints;
  all_constraints.insert(all_constraints.end(), infos1.constraints.begin(),
                         infos1.constraints.end());
  all_constraints.insert(all_constraints.end(), infos2.constraints.begin(),
                         infos2.constraints.end());
  Solve(all_constraints, odometry_data_, landmark_nodes, node_data_, submap_data_, landmark_data_,
        frozen);

  // save map (from pbstream)
  // 1. save pbstream
  saveCorrectedPbstream(path1, 0, node_data_, submap_data_, landmark_data_, landmarks_id_remap_);
  saveCorrectedPbstream(path2, first_max_trajectory_id + 1, node_data_, submap_data_,
                        landmark_data_, landmarks_id_remap_);

  map_manager::GetMapFromPbstream g1(path1 + "_remap", path1 + "_remap");
  map_manager::GetMapFromPbstream g2(path2 + "_remap", path2 + "_remap");
  // draw points in image
  {
    sensor::PointCloud all_cloud;
    cv::Mat debug_img(1000, 1000, CV_8UC3, cv::Scalar(255, 255, 255));
    for (auto it : infos1.node_datas)
    {
      optimization::NodeSpec2D spec_2d = node_data_.at(it.id);
      transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(spec_2d.gravity_alignment);
      transform::Rigid3d correct_pose =
          transform::Embed3D(spec_2d.global_pose_2d) * gravity_alignment.inverse();
      sensor::PointCloud cur_cloud = it.data.filtered_gravity_aligned_point_cloud;
      sensor::PointCloud points_in_map =
          sensor::TransformPointCloud(cur_cloud, correct_pose.cast<float>());
      all_cloud.insert(all_cloud.end(), points_in_map.begin(), points_in_map.end());
    }
    sensor::PointCloud filtered_cloud = sensor::VoxelFilter(0.05).Filter(all_cloud);
    for (const sensor::RangefinderPoint &p : filtered_cloud)
    {
      Eigen::Vector3f p2 = p.position;
      float img_y = p2.x() / 0.1 + 500;
      float img_x = p2.y() / 0.1 + 500;
      cv::circle(debug_img, {img_x, img_y}, 0.5, cv::Scalar(0, 255, 0));
    }
    all_cloud.clear();
    filtered_cloud.clear();
    for (auto it : infos2.node_datas)
    {
      optimization::NodeSpec2D spec_2d = node_data_.at(it.id);
      transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(spec_2d.gravity_alignment);
      transform::Rigid3d correct_pose =
          transform::Embed3D(spec_2d.global_pose_2d) * gravity_alignment.inverse();
      sensor::PointCloud cur_cloud = it.data.filtered_gravity_aligned_point_cloud;
      sensor::PointCloud points_in_map =
          sensor::TransformPointCloud(cur_cloud, correct_pose.cast<float>());
      all_cloud.insert(all_cloud.end(), points_in_map.begin(), points_in_map.end());
    }
    filtered_cloud = sensor::VoxelFilter(0.05).Filter(all_cloud);

    {
      for (const sensor::RangefinderPoint &p : filtered_cloud)
      {
        Eigen::Vector3f p2 = p.position;
        float img_y = p2.x() / 0.1 + 500;
        float img_x = p2.y() / 0.1 + 500;
        cv::circle(debug_img, {img_x, img_y}, 0.5, cv::Scalar(255, 0, 0));
      }
      //       cv::imshow("debug",debug_img);
      //       cv::waitKey(0);
      cv::imwrite(string(getenv("HOME")) + "/merge.png", debug_img);
    }
  }
}

MapsAlignWithLandmarks::~MapsAlignWithLandmarks() {}

transform::Rigid2d MapsAlignWithLandmarks::getRelativePoseFromLandmarks(
    const map<string, transform::Rigid3d> &mark_poses1,
    const map<string, transform::Rigid3d> &mark_poses2)
{
  //   std::vector<cv::Point2f> src_points;
  //   std::vector<cv::Point2f> ref_points;
  //   for(auto it :mark_poses2)
  //   {
  //     if(landmarks_id_remap_.find(it.first) == landmarks_id_remap_.end())
  //       continue;
  //     src_points.push_back({it.second.translation().x(),
  //     it.second.translation().y()}); string id =
  //     landmarks_id_remap_[it.first]; transform::Rigid3d ref_pose =
  //     mark_poses1.at(id); ref_points.push_back({ref_pose.translation().x(),
  //     ref_pose.translation().y()});
  //   }
  //   cv::Mat homo = cv::findHomography(src_points, ref_points);
  //   LOG(INFO) << "homo: " << homo;
  //   transform::Rigid2d relative_pose({homo.at<double>(0,2),
  //   homo.at<double>(1,2)}, atan2(homo.at<double>(1,0),homo.at<double>(0,0)));
  //   LOG(INFO) <<"relative_pose: " << relative_pose;

  ceres::Problem problem;
  ceres::LossFunction *loss_function = NULL;
  double point_angle = 0;
  Eigen::Vector2d opt_p(0, 0);
  for (auto it : mark_poses2)
  {
    if (landmarks_id_remap_.find(it.first) == landmarks_id_remap_.end()) continue;
    //     src_points.push_back({it.second.translation().x(),
    //     it.second.translation().y()});
    string id = landmarks_id_remap_[it.first];
    transform::Rigid3d ref_pose = mark_poses1.at(id);
    //     ref_points.push_back({ref_pose.translation().x(),
    //     ref_pose.translation().y()});
    ceres::CostFunction *cost_function =
        costFunc2D::Create(ref_pose.translation().x(), ref_pose.translation().y(),
                           it.second.translation().x(), it.second.translation().y());
    problem.AddResidualBlock(cost_function, loss_function, &(opt_p(0)), &(opt_p(1)), &point_angle);
  }
  ceres::Solver::Options options;
  options.max_num_iterations = 10;
  options.num_linear_solver_threads = 2;
  options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;

  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  transform::Rigid2d relative_pose({opt_p(0), opt_p(1)}, point_angle);
  LOG(INFO) << "relative_pose: " << relative_pose;
  return relative_pose;
}

void MapsAlignWithLandmarks::readPosesFromPbstream(const std::string &path, const int &added_id,
                                                   PbstreamPosesInfos &infos,
                                                   int &cur_trajectory_id)
{
  io::ProtoStreamReader stream(path + "/map.pbstream");
  io::ProtoStreamDeserializer deserializer(&stream);
  proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();

  std::map<int, int> trajectory_remapping;
  // TODO consider muti trajectory
  //   trajectory_remapping[0] = trajectory_id;
  LOG(INFO) << added_id;

  LOG(INFO) << "Insert node poses..";

  std::map<NodeId, transform::Rigid3d> origin_node_poses;
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    trajectory_remapping[trajectory_proto.trajectory_id()] =
        trajectory_proto.trajectory_id() + added_id;
    if (trajectory_proto.trajectory_id() + added_id > cur_trajectory_id)
      cur_trajectory_id = trajectory_proto.trajectory_id() + added_id;
    for (const proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      infos.node_poses.Insert(NodeId{trajectory_remapping.at(trajectory_proto.trajectory_id()),
                                     node_proto.node_index()},
                              transform::ToRigid3(node_proto.pose()));
    }
  }
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Submap &submap_proto : trajectory_proto.submap())
    {
      infos.submap_poses.Insert(SubmapId{trajectory_remapping.at(trajectory_proto.trajectory_id()),
                                         submap_proto.submap_index()},
                                transform::ToRigid3(submap_proto.pose()));
    }
  }

  for (const auto &landmark : pose_graph_proto.landmark_poses())
  {
    infos.landmark_poses[landmark.landmark_id()] = transform::ToRigid3(landmark.global_pose());
  }

  cartographer::mapping::proto::SerializedData proto;
  LOG(INFO) << "deserializer pbstream..";
  std::vector<sensor::ImuData> imu_datas;
  std::vector<sensor::OdometryData> odom_datas;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kPoseGraph:
      LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
                    "stream likely corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kAllTrajectoryBuilderOptions:
      LOG(ERROR) << "Found multiple serialized "
                    "`AllTrajectoryBuilderOptions`. Serialized stream likely "
                    "corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kSubmap:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      proto.mutable_node()->mutable_node_id()->set_trajectory_id(
          trajectory_remapping.at(proto.node().node_id().trajectory_id()));
      const NodeId node_id(proto.node().node_id().trajectory_id(),
                           proto.node().node_id().node_index());
      infos.node_datas.Insert(node_id, FromProto(proto.node().node_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kImuData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kOdometryData:
    {
      infos.odom_datas.push_back(
          std::make_pair(trajectory_remapping.at(proto.odometry_data().trajectory_id()),
                         sensor::FromProto(proto.odometry_data().odometry_data())));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kFixedFramePoseData:
    {
      break;
    }
    case cartographer::mapping::proto::SerializedData::kLandmarkData:
    {
      infos.landmark_datas.push_back(
          std::make_pair(trajectory_remapping.at(proto.landmark_data().trajectory_id()),
                         sensor::FromProto(proto.landmark_data().landmark_data())));
      break;
    }
    default:
      break;
    }
  }
  std::vector<mapping::PoseGraphInterface::Constraint> constraints =
      FromProto(pose_graph_proto.constraint());
  for (mapping::PoseGraphInterface::Constraint &constarint : constraints)
  {
    constarint.node_id.trajectory_id += added_id;
    constarint.submap_id.trajectory_id += added_id;
  }
  infos.constraints = constraints;
}

void MapsAlignWithLandmarks::loadLandmarksFromXml(
    const std::string &fileName, const std::string &con_ns,
    std::map<std::string, transform::Rigid3d> &pose_list)
{
  boost::property_tree::ptree pt;
  try
  {
    boost::property_tree::xml_parser::read_xml(fileName + "/landmark.xml", pt);
  }
  catch (std::exception &e)
  {
    LOG(WARNING) << e.what();
    LOG(WARNING) << "Can't find or open landmark.xml file and it will not use "
                    "lasermark. ";
    return;
  }
  string con_ns2 = con_ns + "_4x4";
  string xml, xml_id;
  for (auto &m : pt)
  {
    if (m.first == "landmark")
    {
      string ns = m.second.get("ns", " ");
      string is_visible_tmp = m.second.get("visible", " ");
      int is_visible = stoi(is_visible_tmp);
      if ((ns != con_ns && ns != con_ns2) || is_visible == 0) continue;
      xml_id = m.second.get("id", " ");
      xml = m.second.get("transform", " ");
      float value[7];
      sscanf(xml.c_str(), "%f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
             &value[4], &value[5], &value[6]);
      int id = stoi(xml_id);
      transform::Rigid3d landmark_pose({value[0], value[1], value[2]},
                                       {value[6], value[3], value[4], value[5]});

      if ("Lasermarks" == ns) id += 1000000;
      pose_list[std::to_string(id)] = landmark_pose;
    }
  }
  LOG(INFO) << "landmark size: " << pose_list.size();
}

std::array<double, 3> FromPose(const transform::Rigid2d &pose)
{
  return {{pose.translation().x(), pose.translation().y(), pose.normalized_angle()}};
}

// Converts a pose as represented for Ceres back to an transform::Rigid2d pose.
transform::Rigid2d ToPose(const std::array<double, 3> &values)
{
  return transform::Rigid2d({values[0], values[1]}, values[2]);
}

void AddLandmarkCostFunctions(
    const std::map<std::string, PoseGraphInterface::LandmarkNode> &landmark_nodes,
    const MapById<NodeId, optimization::NodeSpec2D> &node_data,
    MapById<NodeId, std::array<double, 3>> *C_nodes,
    std::map<std::string, optimization::CeresPose> *C_landmarks, ceres::Problem *problem,
    double huber_scale)
{
  for (const auto &landmark_node : landmark_nodes)
  {
    for (const auto &observation : landmark_node.second.landmark_observations)
    {
      const std::string &landmark_id = landmark_node.first;
      const auto &begin_of_trajectory = node_data.BeginOfTrajectory(observation.trajectory_id);
      // The landmark observation was made before the trajectory was created.
      if (observation.time < begin_of_trajectory->data.time)
      {
        continue;
      }
      // Find the trajectory nodes before and after the landmark observation.
      auto next = node_data.lower_bound(observation.trajectory_id, observation.time);
      // The landmark observation was made, but the next trajectory node has
      // not been added yet.
      if (next == node_data.EndOfTrajectory(observation.trajectory_id))
      {
        continue;
      }
      if (next == begin_of_trajectory)
      {
        next = std::next(next);
      }
      auto prev = std::prev(next);
      // Add parameter blocks for the landmark ID if they were not added before.
      std::array<double, 3> *prev_node_pose = &C_nodes->at(prev->id);
      //       std::array<double, 3>* next_node_pose = &C_nodes->at(next->id);
      transform::Rigid2d bpre_to_local_2d = node_data.at(prev->id).local_pose_2d;
      transform::Rigid3d bpre_to_local_3d =
          transform::Embed3D(bpre_to_local_2d) *
          transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
      transform::Rigid2d bpre_to_local = transform::Project2D(bpre_to_local_3d);

      transform::Rigid3d base_to_local = observation.tracking_to_local_transform;
      transform::Rigid2d base_to_local_2d = transform::Project2D(base_to_local);
      transform::Rigid2d bpre_to_base = base_to_local_2d.inverse() * bpre_to_local;
      const std::array<double, 3> base2bpre_array = FromPose(bpre_to_base.inverse());
      if (!C_landmarks->count(landmark_id))
      {
        transform::Rigid3d starting_point_tmp;
        if (landmark_node.second.global_landmark_pose.has_value())
        {
          starting_point_tmp = landmark_node.second.global_landmark_pose.value();
        }
        else
        {
          transform::Rigid3d bpre_to_local_3d =
              transform::Embed3D(node_data.at(prev->id).local_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d bpre_to_global =
              transform::Embed3D(node_data.at(prev->id).global_pose_2d) *
              transform::Rigid3d::Rotation(node_data.at(prev->id).gravity_alignment);
          transform::Rigid3d mark_to_base = observation.landmark_to_tracking_transform;

          //                 LandmarkNode::LandmarkObservation observation;
          if (!observation.observation_points.empty())
          {
            ceres::Problem problem;
            double center_x = observation.landmark_to_tracking_transform.translation().x();
            double center_y = observation.landmark_to_tracking_transform.translation().y();
            for (int i = 0; i < observation.observation_points.size(); i++)
            {
              double xx = observation.observation_points[i].x();
              double yy = observation.observation_points[i].y();
              ceres::CostFunction *cost =
                  new ceres::AutoDiffCostFunction<optimization::CircleCost2D, 1, 1, 1>(
                      new optimization::CircleCost2D(xx, yy, observation.pole_radius));
              problem.AddResidualBlock(cost, nullptr, &center_x, &center_y);
            }
            // Build and solve the problem.
            ceres::Solver::Options options;
            options.max_num_iterations = 500;
            options.linear_solver_type = ceres::DENSE_QR;
            options.num_linear_solver_threads = 2;
            ceres::Solver::Summary summary;
            ceres::Solve(options, &problem, &summary);
            mark_to_base = transform::Rigid3d(Eigen::Vector3d(center_x, center_y, 0.),
                                              Eigen::Quaterniond::Identity());
          }

          starting_point_tmp =
              bpre_to_global * bpre_to_local_3d.inverse() * base_to_local * mark_to_base;
        }
        const transform::Rigid3d starting_point = starting_point_tmp;

        C_landmarks->emplace(landmark_id,
                             optimization::CeresPose(
                                 starting_point, nullptr /* translation_parametrization */,
                                 absl::make_unique<ceres::QuaternionParameterization>(), problem));
        // Set landmark constant if it is frozen.
        if (landmark_node.second.frozen)
        {
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).translation());
          problem->SetParameterBlockConstant(C_landmarks->at(landmark_id).rotation());
        }
      }
      if (observation.observation_points.empty())
        problem->AddResidualBlock(
            optimization::LandmarkJzCostFunction2D::CreateAutoDiffCostFunction(
                observation, prev->data, base2bpre_array),
            new ceres::HuberLoss(huber_scale), prev_node_pose->data(),
            C_landmarks->at(landmark_id).rotation(), C_landmarks->at(landmark_id).translation());
      else
      {
        //         LandmarkNode::LandmarkObservation observation;
        //         const Eigen::Quaterniond prev_node_gravity_alignment=
        //         prev->data.gravity_alignment; transform::Rigid3d
        //         prev2gravity(Eigen::Vector3d(0,0,0),
        //         prev_node_gravity_alignment); auto tmp =
        //         transform::Project2D(prev2gravity); transform::Rigid2d
        //         prev2map = ToPose(*prev_node_pose) * tmp; transform::Rigid2d
        //         base2map = prev2map * ToPose(base2bpre_array); const
        //         Eigen::Vector2d
        //         mark_in_map(C_landmarks->at(landmark_id).translation()[0],
        //                                           C_landmarks->at(landmark_id).translation()[1]);
        for (Eigen::Vector3f point : observation.observation_points)
        {
          //           Eigen::Vector2d /*p2*/(point.x(), point.y());
          problem->AddResidualBlock(
              optimization::LandmarkPoleLikeFunction2D::CreateAutoDiffCostFunction(
                  point, observation.pole_radius, prev->data, base2bpre_array, 1.0),
              new ceres::HuberLoss(huber_scale), prev_node_pose->data(),
              C_landmarks->at(landmark_id).translation());
        }
      }

      //      problem->AddResidualBlock(
      //           LandmarkJzCostFunction2D::CreateAutoDiffCostFunction(
      //               observation, next->data, base2bnext_array),
      //           new ceres::HuberLoss(huber_scale), next_node_pose->data(),
      //           C_landmarks->at(landmark_id).rotation(),
      //           C_landmarks->at(landmark_id).translation());
    }
  }
}

std::unique_ptr<transform::Rigid3d> MapsAlignWithLandmarks::InterpolateOdometry(
    const sensor::MapByTime<sensor::OdometryData> &odometry_data_, const int trajectory_id,
    const common::Time time) const
{
  const auto it = odometry_data_.lower_bound(trajectory_id, time);
  if (it == odometry_data_.EndOfTrajectory(trajectory_id))
  {
    return nullptr;
  }
  if (it == odometry_data_.BeginOfTrajectory(trajectory_id))
  {
    if (it->time == time)
    {
      return absl::make_unique<transform::Rigid3d>(it->pose);
    }
    return nullptr;
  }
  const auto prev_it = std::prev(it);
  return absl::make_unique<transform::Rigid3d>(
      transform::Interpolate(transform::TimestampedTransform{prev_it->time, prev_it->pose},
                             transform::TimestampedTransform{it->time, it->pose}, time)
          .transform);
}

std::unique_ptr<transform::Rigid3d> MapsAlignWithLandmarks::CalculateOdometryBetweenNodes(
    const sensor::MapByTime<sensor::OdometryData> &odometry_data_, const int trajectory_id,
    const optimization::NodeSpec2D &first_node_data,
    const optimization::NodeSpec2D &second_node_data) const
{
  if (odometry_data_.HasTrajectory(trajectory_id))
  {
    const std::unique_ptr<transform::Rigid3d> first_node_odometry =
        InterpolateOdometry(odometry_data_, trajectory_id, first_node_data.time);
    const std::unique_ptr<transform::Rigid3d> second_node_odometry =
        InterpolateOdometry(odometry_data_, trajectory_id, second_node_data.time);
    if (first_node_odometry != nullptr && second_node_odometry != nullptr)
    {
      transform::Rigid3d relative_odometry =
          transform::Rigid3d::Rotation(first_node_data.gravity_alignment) *
          first_node_odometry->inverse() * (*second_node_odometry) *
          transform::Rigid3d::Rotation(second_node_data.gravity_alignment.inverse());
      return absl::make_unique<transform::Rigid3d>(relative_odometry);
    }
  }
  return nullptr;
}

void MapsAlignWithLandmarks::Solve(
    const std::vector<PoseGraphInterface::Constraint> &constraints,
    //     const std::map<int, PoseGraphInterface::TrajectoryState>&
    //         trajectories_state,
    const sensor::MapByTime<sensor::OdometryData> &odometry_data_,
    const std::map<std::string, PoseGraphInterface::LandmarkNode> &landmark_nodes,
    MapById<NodeId, optimization::NodeSpec2D> &node_data_,
    MapById<SubmapId, optimization::SubmapSpec2D> &submap_data_,
    std::map<std::string, transform::Rigid3d> &landmark_data_, const bool &frozen)
{
  std::set<int> frozen_trajectories;
  //   for (const auto& it : trajectories_state)
  //   {
  //     if (it.second == PoseGraphInterface::TrajectoryState::FROZEN)
  //     {
  //       frozen_trajectories.insert(it.first);
  //     }
  //   }

  ceres::Problem::Options problem_options;
  ceres::Problem problem(problem_options);

  // Set the starting point.
  // TODO(hrapp): Move ceres data into SubmapSpec.
  MapById<SubmapId, std::array<double, 3>> C_submaps;
  MapById<NodeId, std::array<double, 3>> C_nodes;
  std::map<std::string, optimization::CeresPose> C_landmarks;
  {
    bool first_submap = true;
    for (const auto &submap_id_data : submap_data_)
    {
      //       const bool frozen =
      //           frozen_trajectories.count(submap_id_data.id.trajectory_id) !=
      //           0;
      C_submaps.Insert(submap_id_data.id, FromPose(submap_id_data.data.global_pose));
      problem.AddParameterBlock(C_submaps.at(submap_id_data.id).data(), 3);
      if ((first_submap && submap_id_data.id.trajectory_id == 0))
      {
        first_submap = false;
        // Fix the pose of the first submap or all submaps of a frozen
        // trajectory.
        problem.SetParameterBlockConstant(C_submaps.at(submap_id_data.id).data());
      }
    }
    //     first_submap = true;
    for (const auto &node_id_data : node_data_)
    {
      //       const bool frozen =
      //           frozen_trajectories.count(node_id_data.id.trajectory_id) !=
      //           0;
      C_nodes.Insert(node_id_data.id, FromPose(node_id_data.data.global_pose_2d));
      problem.AddParameterBlock(C_nodes.at(node_id_data.id).data(), 3);
      if (frozen && node_id_data.id.trajectory_id == 0)
      {
        problem.SetParameterBlockConstant(C_nodes.at(node_id_data.id).data());
      }
    }
  }

  // Add cost functions for intra- and inter-submap constraints.
  for (const PoseGraphInterface::Constraint &constraint : constraints)
  {
    problem.AddResidualBlock(optimization::CreateAutoDiffSpaCostFunction(constraint.pose),
                             // Loop closure constraints should have a loss function.
                             constraint.tag == PoseGraphInterface::Constraint::INTER_SUBMAP
                                 ? new ceres::HuberLoss(options_.huber_scale())
                                 : nullptr,
                             C_submaps.at(constraint.submap_id).data(),
                             C_nodes.at(constraint.node_id).data());
  }

  // Add cost functions for landmarks.
  AddLandmarkCostFunctions(landmark_nodes, node_data_, &C_nodes, &C_landmarks, &problem,
                           options_.huber_scale());

  // Add penalties for violating odometry or changes between consecutive nodes
  // if odometry is not available.
  for (auto node_it = node_data_.begin(); node_it != node_data_.end();)
  {
    const int trajectory_id = node_it->id.trajectory_id;
    const auto trajectory_end = node_data_.EndOfTrajectory(trajectory_id);
    if (frozen_trajectories.count(trajectory_id) != 0)
    {
      node_it = trajectory_end;
      continue;
    }

    auto prev_node_it = node_it;
    for (++node_it; node_it != trajectory_end; ++node_it)
    {
      const NodeId first_node_id = prev_node_it->id;
      const optimization::NodeSpec2D &first_node_data = prev_node_it->data;
      prev_node_it = node_it;
      const NodeId second_node_id = node_it->id;
      const optimization::NodeSpec2D &second_node_data = node_it->data;

      if (second_node_id.node_index != first_node_id.node_index + 1)
      {
        continue;
      }

      // Add a relative pose constraint based on the odometry (if available).
      std::unique_ptr<transform::Rigid3d> relative_odometry = CalculateOdometryBetweenNodes(
          odometry_data_, trajectory_id, first_node_data, second_node_data);
      if (relative_odometry != nullptr)
      {
        problem.AddResidualBlock(
            optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
                *relative_odometry, options_.odometry_translation_weight(),
                options_.odometry_rotation_weight()}),
            nullptr /* loss function */, C_nodes.at(first_node_id).data(),
            C_nodes.at(second_node_id).data());
      }

      // Add a relative pose constraint based on consecutive local SLAM poses.
      const transform::Rigid3d relative_local_slam_pose = transform::Embed3D(
          first_node_data.local_pose_2d.inverse() * second_node_data.local_pose_2d);
      problem.AddResidualBlock(
          optimization::CreateAutoDiffSpaCostFunction(PoseGraphInterface::Constraint::Pose{
              relative_local_slam_pose, options_.local_slam_pose_translation_weight(),
              options_.local_slam_pose_rotation_weight()}),
          nullptr /* loss function */, C_nodes.at(first_node_id).data(),
          C_nodes.at(second_node_id).data());
    }
  }

  // Solve.
  ceres::Solver::Summary summary;
  ceres::Solve(common::CreateCeresSolverOptions(options_.ceres_solver_options()), &problem,
               &summary);
  if (options_.log_solver_summary())
  {
    LOG(INFO) << summary.FullReport();
  }

  // Store the result.
  for (const auto &C_submap_id_data : C_submaps)
  {
    submap_data_.at(C_submap_id_data.id).global_pose = ToPose(C_submap_id_data.data);
  }
  for (const auto &C_node_id_data : C_nodes)
  {
    node_data_.at(C_node_id_data.id).global_pose_2d = ToPose(C_node_id_data.data);
  }
  for (const auto &C_landmark : C_landmarks)
  {
    landmark_data_[C_landmark.first] = C_landmark.second.ToRigid();
  }

  LOG(WARNING) << "End of Solve.";
}

void MapsAlignWithLandmarks::saveCorrectedPbstream(
    const std::string &path, const int &trajectory_id,
    const MapById<NodeId, optimization::NodeSpec2D> &node_data_,
    const MapById<SubmapId, optimization::SubmapSpec2D> &submap_data_,
    const map<string, transform::Rigid3d> &landmark_data_,
    const std::map<std::string, std::string> &landmarks_id_remap)
{
  cartographer_ros::NodeOptions node_options;
  cartographer_ros::TrajectoryOptions trajectory_options;
  std::string config_path = ::ros::package::getPath("cartographer_ros") + "/configuration_files";
  std::tie(node_options, trajectory_options) =
      cartographer_ros::LoadOptions(config_path, "backpack_2d_jz_mapping.lua");
  cartographer::common::ThreadPool thread_pool1_(2);
  const cartographer::mapping::proto::MapBuilderOptions options(node_options.map_builder_options);
  std::unique_ptr<cartographer::mapping::PoseGraph> pose_graph =
      absl::make_unique<cartographer::mapping::PoseGraph2D>(
          options.pose_graph_options(),
          absl::make_unique<optimization::OptimizationProblem2D>(
              options.pose_graph_options().optimization_problem_options()),
          &thread_pool1_);

  std::string origin_pbstream = path + "/map.pbstream";
  cartographer::io::ProtoStreamReader stream(origin_pbstream);
  io::ProtoStreamDeserializer deserializer(&stream);
  proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();

  std::map<int, int> trajectory_remapping;
  std::vector<std::unique_ptr<cartographer::mapping::TrajectoryBuilderInterface>>
      trajectory_builders_;
  std::vector<cartographer::mapping::proto::TrajectoryBuilderOptionsWithSensorIds>
      all_trajectory_builder_options_;
  for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
  {
    auto &trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
    const auto &options_with_sensor_ids_proto =
        all_builder_options_proto.options_with_sensor_ids(i);

    const int new_trajectory_id = trajectory_builders_.size();
    trajectory_builders_.emplace_back();
    all_trajectory_builder_options_.push_back(options_with_sensor_ids_proto);

    CHECK(trajectory_remapping.emplace(trajectory_proto.trajectory_id(), new_trajectory_id).second)
        << "Duplicate trajectory ID: " << trajectory_proto.trajectory_id();
    trajectory_proto.set_trajectory_id(new_trajectory_id);
    if (true)
    {
      pose_graph->FreezeTrajectory(new_trajectory_id);
    }
  }
  LOG(INFO) << "init done..";
  for (const auto &landmark : pose_graph_proto.landmark_poses())
  {
    std::string landmark_id = landmark.landmark_id();
    if (trajectory_id > 0)
    {
      if (landmarks_id_remap.find(landmark_id) == landmarks_id_remap_.end()) continue;

      landmark_id = landmarks_id_remap_.at(landmark_id);
    }
    if (landmark_data_.find(landmark_id) == landmark_data_.end()) continue;
    pose_graph->SetLandmarkPose(landmark_id, landmark_data_.at(landmark_id), true);
  }
  LOG(INFO) << "add landmark pose done.";
  MapById<SubmapId, mapping::proto::Submap> submap_id_to_submap;
  MapById<NodeId, mapping::proto::Node> node_id_to_node;
  cartographer::mapping::proto::SerializedData proto;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case cartographer::mapping::proto::SerializedData::kPoseGraph:
      LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
                    "stream likely corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kAllTrajectoryBuilderOptions:
      LOG(ERROR) << "Found multiple serialized "
                    "`AllTrajectoryBuilderOptions`. Serialized stream likely "
                    "corrupt!.";
      break;
    case cartographer::mapping::proto::SerializedData::kSubmap:
    {
      submap_id_to_submap.Insert(SubmapId{proto.submap().submap_id().trajectory_id(),
                                          proto.submap().submap_id().submap_index()},
                                 proto.submap());
      break;
    }
    case cartographer::mapping::proto::SerializedData::kNode:
    {
      int cur_trajectory_id = proto.node().node_id().trajectory_id();
      cur_trajectory_id += trajectory_id;
      const NodeId node_id(proto.node().node_id().trajectory_id(),
                           proto.node().node_id().node_index());
      const transform::Rigid3d &node_pose =
          transform::Embed3D(node_data_.at({cur_trajectory_id, node_id.node_index}).global_pose_2d);
      pose_graph->AddNodeFromProto(node_pose, proto.node());
      node_id_to_node.Insert(node_id, proto.node());
      break;
    }
    case cartographer::mapping::proto::SerializedData::kTrajectoryData:
    {
      proto.mutable_trajectory_data()->set_trajectory_id(
          trajectory_remapping.at(proto.trajectory_data().trajectory_id()));
      pose_graph->SetTrajectoryDataFromProto(proto.trajectory_data());
      // no use
      break;
    }
    case cartographer::mapping::proto::SerializedData::kImuData:
    {
      //         if (load_frozen_state) break;
      pose_graph->AddImuData(trajectory_remapping.at(proto.imu_data().trajectory_id()),
                             sensor::FromProto(proto.imu_data().imu_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kOdometryData:
    {
      pose_graph->AddOdometryData(trajectory_remapping.at(proto.odometry_data().trajectory_id()),
                                  sensor::FromProto(proto.odometry_data().odometry_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kFixedFramePoseData:
    {
      pose_graph->AddFixedFramePoseData(
          trajectory_remapping.at(proto.fixed_frame_pose_data().trajectory_id()),
          sensor::FromProto(proto.fixed_frame_pose_data().fixed_frame_pose_data()));
      break;
    }
    case cartographer::mapping::proto::SerializedData::kLandmarkData:
    {
      sensor::LandmarkData landmark_data = sensor::FromProto(proto.landmark_data().landmark_data());
      if (trajectory_id > 0)
      {
        if (landmark_data.landmark_observations.empty()) break;

        for (sensor::LandmarkObservation &observation : landmark_data.landmark_observations)
        {
          string landmark_id = observation.id;
          if (landmarks_id_remap.find(landmark_id) == landmarks_id_remap_.end()) continue;
          observation.id = landmarks_id_remap.at(landmark_id);
        }
        pose_graph->AddLandmarkData(trajectory_remapping.at(proto.landmark_data().trajectory_id()),
                                    landmark_data);
      }
      else
        pose_graph->AddLandmarkData(trajectory_remapping.at(proto.landmark_data().trajectory_id()),
                                    landmark_data);
      break;
    }

    default:
      LOG(WARNING) << "Skipping unknown message type in stream: " << proto.GetTypeName();
    }
  }
  LOG(INFO) << "load origin pbstream done.";
  for (const auto &submap_id_submap : submap_id_to_submap)
  {
    int cur_trajectory_id = submap_id_submap.id.trajectory_id;
    cur_trajectory_id += trajectory_id;
    pose_graph->AddSubmapFromProto(
        transform::Embed3D(
            submap_data_.at({cur_trajectory_id, submap_id_submap.id.submap_index}).global_pose),
        submap_id_submap.data);
  }

  pose_graph->AddSerializedConstraints(FromProto(pose_graph_proto.constraint()));
  LOG(INFO) << "add constraints done.";
  std::string new_path = path + "_remap";
  std::string system_command = "mkdir -p " + new_path;
  LOG(INFO) << system_command << std::endl;
  auto status = system(system_command.c_str());

  io::ProtoStreamWriter writer(new_path + "/map.pbstream");
  io::WritePbStream(*pose_graph, all_trajectory_builder_options_, &writer, false);
  writer.Close();
  LOG(INFO) << "write done.";
}

} // namespace cartographer

// int main(int argc, char** argv)
// {
//   string path1 = string(getenv("HOME")) + "/map/" + string(argv[1]);
//   string path2 = string(getenv("HOME")) + "/map/" + string(argv[2]);
//   bool frozen = false;
//   if(argc == 4)
//     if(string(argv[3]) == "true")
//       frozen = true;
//   cartographer::MapsAlignWithLandmarks map(path1, path2,frozen);
//   return  0;
// }