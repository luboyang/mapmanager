/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "pbstream_convertor.h"

#include <cartographer/mapping/internal/constraints/constraint_builder_2d.h>
#include <cartographer_ros_msgs/StatusCode.h>

#include "cartographer/io/proto_stream_deserializer.h"
#include "cartographer_ros/node.h"
#include "cartographer_ros/node_options.h"
#include "cartographer_ros_msgs/SubmapQuery.h"
#include "math.h"

using namespace std;
using namespace cv;
using namespace cartographer;
// using namespace cartographer::common;
using namespace cartographer::mapping;
using namespace cartographer::io;

using mapping::proto::SerializedData;

using mapping::MapById;
using mapping::NodeId;
using mapping::PoseGraphInterface;
using mapping::SubmapId;
using mapping::TrajectoryNode;
using mapping::proto::SerializedData;

namespace map_manager
{
namespace pbstream_convertor
{

static constexpr int kMappingStateSerializationFormatVersion = 2;
static constexpr int kFormatVersionWithoutSubmapHistograms = 1;

const int kPaddingPixel = 0;
using UniqueCairoSurfacePtr = std::unique_ptr<cairo_surface_t, void (*)(cairo_surface_t *)>;
using UniqueCairoPtr = std::unique_ptr<cairo_t, void (*)(cairo_t *)>;
constexpr cairo_format_t kCairoFormat = CAIRO_FORMAT_ARGB32;

inline UniqueCairoSurfacePtr MakeUniqueCairoSurfacePtr(cairo_surface_t *surface)
{
  return UniqueCairoSurfacePtr(surface, cairo_surface_destroy);
}
inline UniqueCairoPtr MakeUniqueCairoPtr(cairo_t *surface)
{
  return UniqueCairoPtr(surface, cairo_destroy);
}
inline Eigen::Affine3d ToEigen_mapping(const ::cartographer::transform::Rigid3d &rigid3)
{
  return Eigen::Translation3d(rigid3.translation()) * rigid3.rotation();
}

double inline GetYaw(const Eigen::Quaterniond &rotation)
{
  const Eigen::Matrix<double, 3, 1> direction = rotation * Eigen::Matrix<double, 3, 1>::UnitX();
  return atan2(direction.y(), direction.x());
}

bool inline judgeFileExist(const std::string &file_name)
{
  bool existed_flag = false;
  if (FILE *file = fopen(file_name.c_str(), "r"))
  {
    fclose(file);
    existed_flag = true;
  }
  else
  {
    LOG(INFO) << "\033[33m" << file_name << " is not existed!\033[37m";
    existed_flag = false;
  }
  return existed_flag;
}
void CairoPaintSubmapSlice(const double scale, const SubmapSlice &submap, cairo_t *cr,
                           std::function<void(const SubmapSlice &)> draw_callback)
{
  cairo_scale(cr, scale, scale);
  const auto &submap_slice = submap;
  if (submap_slice.surface == nullptr)
  {
    return;
  }
  const Eigen::Matrix4d homo = ToEigen_mapping(submap_slice.slice_pose).matrix();

  cairo_save(cr);
  cairo_matrix_t matrix;
  cairo_matrix_init(&matrix, homo(1, 0), homo(0, 0), -homo(1, 1), -homo(0, 1), homo(0, 3),
                    -homo(1, 3));
  cairo_transform(cr, &matrix);

  const double submap_resolution = submap_slice.resolution;
  cairo_scale(cr, submap_resolution, submap_resolution);

  // Invokes caller's callback to utilize slice data in global cooridnate
  // frame. e.g. finds bounding box, paints slices.
  draw_callback(submap_slice);
  cairo_restore(cr);
}

PaintSubmapSlicesResult PaintSubmapSlice(SubmapSlice &submap, const double resolution)
{
  Eigen::AlignedBox2f bounding_box;
  {
    auto surface = MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, 1, 1));
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    const auto update_bounding_box = [&bounding_box, &cr](double x, double y)
    {
      cairo_user_to_device(cr.get(), &x, &y);
      bounding_box.extend(Eigen::Vector2f(x, y));
    };

    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&update_bounding_box](const SubmapSlice &submap_slice)
                          {
                            update_bounding_box(0, 0);
                            update_bounding_box(submap_slice.width, 0);
                            update_bounding_box(0, submap_slice.height);
                            update_bounding_box(submap_slice.width, submap_slice.height);
                          });
  }

  const Eigen::Array2i size(std::ceil(bounding_box.sizes().x()) + 2 * kPaddingPixel,
                            std::ceil(bounding_box.sizes().y()) + 2 * kPaddingPixel);
  const Eigen::Array2f origin(-bounding_box.min().x() + kPaddingPixel,
                              -bounding_box.min().y() + kPaddingPixel);

  auto surface =
      MakeUniqueCairoSurfacePtr(cairo_image_surface_create(kCairoFormat, size.x(), size.y()));
  {
    auto cr = MakeUniqueCairoPtr(cairo_create(surface.get()));
    cairo_set_source_rgba(cr.get(), 0.5, 0.0, 0.0, 1.);
    cairo_paint(cr.get());
    cairo_translate(cr.get(), origin.x(), origin.y());
    CairoPaintSubmapSlice(1. / resolution, submap, cr.get(),
                          [&cr](const SubmapSlice &submap_slice)
                          {
                            cairo_set_source_surface(cr.get(), submap_slice.surface.get(), 0., 0.);
                            //                              cairo_set_antialias
                            //                              (cr.get(),
                            //                              CAIRO_ANTIALIAS_NONE);
                            //                              cairo_pattern_set_filter(cairo_get_source(cr.get()),CAIRO_FILTER_NEAREST);
                            cairo_paint(cr.get());
                          });
    cairo_surface_flush(surface.get());
  }
  return PaintSubmapSlicesResult(std::move(surface), origin);
}

std::unique_ptr<mapping::GridInterface>
CreateGrid(const Eigen::Vector2f &origin, mapping::ValueConversionTables *conversion_tables)
{
  constexpr int kInitialSubmapSize = 100;
  float resolution = 0.05;
  return absl::make_unique<mapping::ProbabilityGrid>(
      mapping::MapLimits(resolution,
                         origin.cast<double>() +
                             0.5 * kInitialSubmapSize * resolution * Eigen::Vector2d::Ones(),
                         mapping::CellLimits(kInitialSubmapSize, kInitialSubmapSize)),
      conversion_tables);
}

PbstreamConvertor::PbstreamConvertor(const cartographer::mapping::proto::MapBuilderOptions &options)
    : options_(options), thread_pool1_(1), thread_pool2_(1),
      ceres_scan_matcher_(
          options.pose_graph_options().constraint_builder_options().ceres_scan_matcher_options())
{
  //   cartographer::mapping::proto::MapBuilderOptions options;
  interval_of_submap_ = 3.0;
  ros::param::get("~interval_of_submap", interval_of_submap_);
  LOG(INFO) << "pbstream merger: interval_of_submap is " << interval_of_submap_;
  debug_flag_ = false;
  ros::param::get("~pbstream_convertor_debug", debug_flag_);
  LOG(INFO) << "pbstream merger: pbstream_convertor_debug is " << debug_flag_;

  origin_pose_graph_ = absl::make_unique<cartographer::mapping::PoseGraph2D>(
      options_.pose_graph_options(),
      absl::make_unique<optimization::OptimizationProblem2D>(
          options_.pose_graph_options().optimization_problem_options()),
      &thread_pool1_);

  new_pose_graph_ = absl::make_unique<cartographer::mapping::PoseGraph2D>(
      options_.pose_graph_options(),
      absl::make_unique<optimization::OptimizationProblem2D>(
          options_.pose_graph_options().optimization_problem_options()),
      &thread_pool2_);
  is_frezon_ = true;
  //   exit(1);
}

PbstreamConvertor::~PbstreamConvertor()
{
  auto tmp = std::move(origin_pose_graph_);
  auto tmp2 = std::move(new_pose_graph_);
  origin_pose_graph_ = nullptr;
  new_pose_graph_ = nullptr;
}

int PbstreamConvertor::AddTrajectoryForDeserialization(
    const proto::TrajectoryBuilderOptionsWithSensorIds &options_with_sensor_ids_proto)
{
  const int trajectory_id = trajectory_builders_.size();
  trajectory_builders_.emplace_back();
  all_trajectory_builder_options_.push_back(options_with_sensor_ids_proto);
  //   CHECK_EQ(trajectory_builders_.size(),
  //   all_trajectory_builder_options_.size());
  return trajectory_id;
}

std::map<int, int> PbstreamConvertor::LoadState(io::ProtoStreamReaderInterface *const reader,
                                                bool load_frozen_state)
{
  //   absl::MutexLock lock(&mutex_);
  LOG(INFO) << "start loading ... ";

  io::ProtoStreamDeserializer deserializer(reader);

  // Create a copy of the pose_graph_proto, such that we can re-write the
  // trajectory ids.
  proto::PoseGraph pose_graph_proto = deserializer.pose_graph();
  const auto &all_builder_options_proto = deserializer.all_trajectory_builder_options();

  std::map<int, int> trajectory_remapping;
  //   LOG(INFO) << "trajectory size: " <<pose_graph_proto.trajectory_size() ;
  for (int i = 0; i < pose_graph_proto.trajectory_size(); ++i)
  {
    auto &trajectory_proto = *pose_graph_proto.mutable_trajectory(i);
    const auto &options_with_sensor_ids_proto =
        all_builder_options_proto.options_with_sensor_ids(i);
    const int new_trajectory_id = AddTrajectoryForDeserialization(options_with_sensor_ids_proto);
    //     LOG(INFO) << "new_trajectory_id:" <<new_trajectory_id ;
    CHECK(trajectory_remapping.emplace(trajectory_proto.trajectory_id(), new_trajectory_id).second)
        << "Duplicate trajectory ID: " << trajectory_proto.trajectory_id();
    trajectory_proto.set_trajectory_id(new_trajectory_id);
    if (load_frozen_state)
    {
      origin_pose_graph_->FreezeTrajectory(new_trajectory_id);
    }
  }

  // no frezon
  //   new_pose_graph_->FreezeTrajectory(0);

  // Apply the calculated remapping to constraints in the pose graph proto.
  for (auto &constraint_proto : *pose_graph_proto.mutable_constraint())
  {
    //     LOG(INFO) << "constraint_proto.submap_id().trajectory_id():
    //     "<<constraint_proto.submap_id().trajectory_id() ;
    constraint_proto.mutable_submap_id()->set_trajectory_id(
        trajectory_remapping.at(constraint_proto.submap_id().trajectory_id()));

    //     LOG(INFO) << "constraint_proto.node_id().trajectory_id():
    //     "<<constraint_proto.node_id().trajectory_id() ;
    constraint_proto.mutable_node_id()->set_trajectory_id(
        trajectory_remapping.at(constraint_proto.node_id().trajectory_id()));
  }

  cartographer::mapping::MapById<SubmapId, transform::Rigid3d> submap_poses;

  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Submap &submap_proto : trajectory_proto.submap())
    {
      submap_poses.Insert(SubmapId{trajectory_proto.trajectory_id(), submap_proto.submap_index()},
                          transform::ToRigid3(submap_proto.pose()));
    }
  }
  cartographer::mapping::MapById<cartographer::mapping::NodeId, cartographer::transform::Rigid3d>
      node_poses;
  for (const proto::Trajectory &trajectory_proto : pose_graph_proto.trajectory())
  {
    for (const proto::Trajectory::Node &node_proto : trajectory_proto.node())
    {
      node_poses.Insert(NodeId{trajectory_proto.trajectory_id(), node_proto.node_index()},
                        transform::ToRigid3(node_proto.pose()));
    }
  }

  // Set global poses of landmarks.
  for (const auto &landmark : pose_graph_proto.landmark_poses())
  {
    origin_pose_graph_->SetLandmarkPose(landmark.landmark_id(),
                                        transform::ToRigid3(landmark.global_pose()), true);
    new_pose_graph_->SetLandmarkPose(landmark.landmark_id(),
                                     transform::ToRigid3(landmark.global_pose()), true);
  }

  MapById<SubmapId, mapping::proto::Submap> submap_id_to_submap;
  MapById<NodeId, mapping::proto::Node> node_id_to_node;
  SerializedData proto;
  while (deserializer.ReadNextSerializedData(&proto))
  {
    switch (proto.data_case())
    {
    case SerializedData::kPoseGraph:
      LOG(ERROR) << "Found multiple serialized `PoseGraph`. Serialized "
                    "stream likely corrupt!.";
      break;
    case SerializedData::kAllTrajectoryBuilderOptions:
      LOG(ERROR) << "Found multiple serialized "
                    "`AllTrajectoryBuilderOptions`. Serialized stream likely "
                    "corrupt!.";
      break;
    case SerializedData::kSubmap:
    {
      proto.mutable_submap()->mutable_submap_id()->set_trajectory_id(
          trajectory_remapping.at(proto.submap().submap_id().trajectory_id()));
      submap_id_to_submap.Insert(SubmapId{proto.submap().submap_id().trajectory_id(),
                                          proto.submap().submap_id().submap_index()},
                                 proto.submap());
      break;
    }
    case SerializedData::kNode:
    {
      proto.mutable_node()->mutable_node_id()->set_trajectory_id(
          trajectory_remapping.at(proto.node().node_id().trajectory_id()));
      const NodeId node_id(proto.node().node_id().trajectory_id(),
                           proto.node().node_id().node_index());
      const transform::Rigid3d &node_pose = node_poses.at(node_id);
      origin_pose_graph_->AddNodeFromProto(node_pose, proto.node());
      node_id_to_node.Insert(node_id, proto.node());
      break;
    }
    case SerializedData::kTrajectoryData:
    {
      proto.mutable_trajectory_data()->set_trajectory_id(
          trajectory_remapping.at(proto.trajectory_data().trajectory_id()));
      origin_pose_graph_->SetTrajectoryDataFromProto(proto.trajectory_data());
      // no use
      break;
    }
    case SerializedData::kImuData:
    {
      //         if (load_frozen_state) break;
      origin_pose_graph_->AddImuData(trajectory_remapping.at(proto.imu_data().trajectory_id()),
                                     sensor::FromProto(proto.imu_data().imu_data()));
      new_pose_graph_->AddImuData(trajectory_remapping.at(0),
                                  sensor::FromProto(proto.imu_data().imu_data()));
      break;
    }
    case SerializedData::kOdometryData:
    {
      //         if (load_frozen_state) break;
      origin_pose_graph_->AddOdometryData(
          trajectory_remapping.at(proto.odometry_data().trajectory_id()),
          sensor::FromProto(proto.odometry_data().odometry_data()));
      new_pose_graph_->AddOdometryData(trajectory_remapping.at(0),
                                       sensor::FromProto(proto.odometry_data().odometry_data()));
      break;
    }
    case SerializedData::kFixedFramePoseData:
    {
      //         if (load_frozen_state) break;
      origin_pose_graph_->AddFixedFramePoseData(
          trajectory_remapping.at(proto.fixed_frame_pose_data().trajectory_id()),
          sensor::FromProto(proto.fixed_frame_pose_data().fixed_frame_pose_data()));
      new_pose_graph_->AddFixedFramePoseData(
          trajectory_remapping.at(0),
          sensor::FromProto(proto.fixed_frame_pose_data().fixed_frame_pose_data()));
      break;
    }
    case SerializedData::kLandmarkData:
    {
      //         if (load_frozen_state) break;
      origin_pose_graph_->AddLandmarkData(
          trajectory_remapping.at(proto.landmark_data().trajectory_id()),
          sensor::FromProto(proto.landmark_data().landmark_data()));
      new_pose_graph_->AddLandmarkData(trajectory_remapping.at(0),
                                       sensor::FromProto(proto.landmark_data().landmark_data()));
      break;
    }
    default:
      LOG(WARNING) << "Skipping unknown message type in stream: " << proto.GetTypeName();
    }
  }

  // TODO(schwoere): Remove backwards compatibility once the pbstream format
  // version 2 is established.
  //   if (deserializer.header().format_version() ==
  //       io::kFormatVersionWithoutSubmapHistograms) {
  //     submap_id_to_submap =
  //         cartographer::io::MigrateSubmapFormatVersion1ToVersion2(
  //             submap_id_to_submap, node_id_to_node, pose_graph_proto);
  //   }
  for (const auto &submap_id_submap : submap_id_to_submap)
  {
    origin_pose_graph_->AddSubmapFromProto(submap_poses.at(submap_id_submap.id),
                                           submap_id_submap.data);
    //     LOG(INFO) << "submap id: " << submap_id_submap.id << "\t
    //     submap_pose:" <<
    //     submap_poses.at(submap_id_submap.id).translation().transpose() ;
  }

  if (0)
  {
    // Add information about which nodes belong to which submap.
    // Required for 3D pure localization.
    for (const proto::PoseGraph::Constraint &constraint_proto : pose_graph_proto.constraint())
    {
      origin_pose_graph_->AddNodeToSubmap(NodeId{constraint_proto.node_id().trajectory_id(),
                                                 constraint_proto.node_id().node_index()},
                                          SubmapId{constraint_proto.submap_id().trajectory_id(),
                                                   constraint_proto.submap_id().submap_index()});
    }
  }
  else
  {
    // When loading unfrozen trajectories, 'AddSerializedConstraints' will
    // take care of adding information about which nodes belong to which
    // submap.
    origin_pose_graph_->AddSerializedConstraints(FromProto(pose_graph_proto.constraint()));
  }
  if (!is_frezon_) origin_pose_graph_->RunFinalOptimization();
  CHECK(reader->eof());
  LOG(INFO) << "Load done!";
  return trajectory_remapping;
}

Point2f PbstreamConvertor::findNearestCenterNode(const Point2f &center_point, vector<SubmapId> ids)
{
  double dist = 1e9;
  Point2f nearest_node(0, 0);
  //   LOG(INFO) << "center_point:" << center_point.x <<"," <<center_point.y;
  LOG(INFO) << "nodes size: ";
  for (SubmapId submap_id : ids)
  {
    for (NodeId node_id : intra_constraints_[submap_id])
    {
      transform::Rigid3d node_pose = nodes_.at(node_id).global_pose;
      double cur_dist = (node_pose.translation().x() - center_point.x) *
                            (node_pose.translation().x() - center_point.x) +
                        (node_pose.translation().y() - center_point.y) *
                            (node_pose.translation().y() - center_point.y);
      if (cur_dist < dist)
      {
        dist = cur_dist;
        nearest_node.x = node_pose.translation().x();
        nearest_node.y = node_pose.translation().y();
      }
    }
    LOG(INFO) << intra_constraints_[submap_id].size() << "\t";
  }
  LOG(INFO);
  LOG(INFO) << "nearest_node:" << nearest_node.x << "," << nearest_node.y << "\t" << dist;
  return nearest_node;
}

void PbstreamConvertor::clusteringSubmaps()
{
  //   absl::MutexLock lock(&mutex_);
  LOG(INFO) << "start clustering submaps ... ";

  map<SubmapId, Point2f> submap_poses;
  for (const auto &submap_nodes : submap_with_nodes_)
  {
    Mat points_temp(submap_nodes.second.size(), 1, CV_32FC2), labels_temp;
    int clusterCount = 1;
    Mat centers(clusterCount, 1, points_temp.type());
    int i = 0;
    for (const auto &node : submap_nodes.second)
    {
      points_temp.at<Vec2f>(i)[0] = node.global_pose.translation()(0);
      points_temp.at<Vec2f>(i)[1] = node.global_pose.translation()(1);
      i++;
    }
    kmeans(points_temp, clusterCount, labels_temp,
           TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 10, 1.0), 3, KMEANS_PP_CENTERS,
           centers);

    submap_poses[submap_nodes.first] = Point2f(centers.at<Vec2f>(0)[0], centers.at<Vec2f>(0)[1]);
  }

  LOG(INFO) << "submap_poses size: " << submap_poses.size();
  Mat points(submap_poses.size(), 1, CV_32FC2), labels;
  //   MapById<SubmapId,transform::Rigid3d>::ConstIterator it =
  //   submap_poses_.begin(); int i = 0;
  vector<SubmapId> submap_ids;
  std::map<string, vector<SubmapId>> clustering_temp;
  //   submap_ids.resize(submap_poses_.size());
  std::map<string, int> id_with_tables;
  int k = 0;
  float max_x = -1e9, max_y = -1e9, min_x = 1e9, min_y = 1e9;
  for (const auto &it : submap_poses)
  {
    LOG(INFO) << it.second.x << ", " << it.second.y;
    if (max_x < it.second.x) max_x = it.second.x;
    if (max_y < it.second.y) max_y = it.second.y;
    if (min_x > it.second.x) min_x = it.second.x;
    if (min_y > it.second.y) min_y = it.second.y;
  }
  float max_dist = 0;
  if (max_x - min_x > max_y - min_y)
    max_dist = max_x - min_x;
  else
    max_dist = max_y - min_y;

  if (max_dist <= interval_of_submap_ + 0.5)
  {
    interval_of_submap_ = max_dist / 3 * 2;
  }
  LOG(WARNING) << "max_dist:" << max_dist << "\tinterval_of_submap: " << interval_of_submap_;
  for (const auto &it : submap_poses)
  {
    //     float x = it.second.x;
    //     float y = it.second.y;
    int x = std::round(it.second.x / interval_of_submap_);
    int y = std::round(it.second.y / interval_of_submap_);
    string id_temp = to_string(x) + " " + to_string(y);
    if (clustering_temp.find(id_temp) == clustering_temp.end())
    {
      LOG(WARNING) << "id_temp:" << id_temp << "\t" << it.first;
      clustering_submaps_poses_[k] = it.second;
      id_with_tables[id_temp] = k;
      //       id_with_tables.insert(std::make_pair(id_temp, k));
      k = k + 1;
      waitKey(10);
    }
    LOG(WARNING) << id_with_tables.at(id_temp);
    clustering_submaps_[id_with_tables[id_temp]].push_back(it.first);
    clustering_temp[id_temp].push_back(it.first);
  }
  LOG(WARNING) << clustering_temp.size();
  Mat img(1000, 1000, CV_8UC3, Scalar(255, 255, 255));
  Scalar colorTab[clustering_temp.size()];
  RNG rng(12435);
  for (std::size_t n = 0; n < clustering_temp.size(); n++)
  {
    colorTab[n] = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
  };

  LOG(WARNING) << clustering_submaps_.size();

  for (auto it1 : clustering_submaps_)
  {
    LOG(INFO) << "sort_id: " << it1.first;
    for (auto it2 : it1.second)
      LOG(INFO) << it2;
    clustering_submaps_poses_[it1.first] =
        findNearestCenterNode(clustering_submaps_poses_[it1.first], it1.second);
  }

  if (debug_flag_)
  {
    imshow("temp1", img);
    waitKey(10);
  }
  LOG(INFO) << "clustering submaps done! ";
}

void PbstreamConvertor::preHandle()
{
  //   absl::MutexLock lock(&mutex_);
  LOG(INFO) << "start prehandling nodes and submaps ... ";
  num_range_data_.resize(trajectory_builders_.size());
  const MapById<SubmapId, PoseGraphInterface::SubmapData> &submaps_data =
      origin_pose_graph_->GetAllSubmapData();
  const auto nodes_temp = origin_pose_graph_->GetTrajectoryNodes();
  std::map<NodeId, int> temp_nodeids;
  std::map<int, int> trajectory_with_size;
  for (PoseGraphInterface::Constraint constraint : origin_pose_graph_->constraints())
  {
    if (constraint.tag == PoseGraphInterface::Constraint::INTRA_SUBMAP)
    {
      int update_node_index = constraint.node_id.node_index;
      temp_nodeids[constraint.node_id] = update_node_index;
    }
  }

  for (const auto &it : nodes_temp)
  {
    if (trajectory_with_size.find(it.id.trajectory_id) == trajectory_with_size.end())
      trajectory_with_size[it.id.trajectory_id] = it.id.node_index;
    else
    {
      if (trajectory_with_size[it.id.trajectory_id] < it.id.node_index)
        trajectory_with_size[it.id.trajectory_id] = it.id.node_index;
    }
    nodes_sort_by_trajectory_[it.id.trajectory_id].push_back(it.id);
  }
  int trajectory_nodes_size[nodes_sort_by_trajectory_.size()];

  for (std::size_t i = 0; i < nodes_sort_by_trajectory_.size(); i++)
  {
    LOG(INFO) << "trajectory_with_size: " << trajectory_with_size[i]
              << "\t nodes_sort_by_trajectory_[i]" << nodes_sort_by_trajectory_[i].size();
    if (trajectory_with_size[i] > nodes_sort_by_trajectory_[i].size())
      trajectory_nodes_size[i] = trajectory_with_size[i] + 2;
    else
      trajectory_nodes_size[i] = nodes_sort_by_trajectory_[i].size() + 2;
    LOG(INFO) << "trajectory_nodes_size: " << trajectory_nodes_size[i];
  }

  std::map<int, std::vector<NodeId>> trajectory_with_unfinished_nodes;
  for (auto nodes_with_trajectory : nodes_sort_by_trajectory_)
  {
    for (auto ori_node_id : nodes_with_trajectory.second)
    {
      if (temp_nodeids.find(ori_node_id) == temp_nodeids.end())
      {
        int update_node_index = ori_node_id.node_index;
        for (int i = 0; i < ori_node_id.trajectory_id; i++)
          update_node_index += trajectory_nodes_size[i];
        mapping::NodeId node_id(0, update_node_index);
        trajectory_with_unfinished_nodes[ori_node_id.trajectory_id].push_back(node_id);
      }
    }
  }
  LOG(INFO) << "trajectory_with_unfinished_nodes: " << trajectory_with_unfinished_nodes[0].size();
  kd_tree::poseVec poses;
  for (const auto &it : nodes_temp)
  {
    //     if(temp_nodeids.find(it.id) == temp_nodeids.end())
    //       continue;
    int update_node_index = it.id.node_index;
    for (int i = 0; i < it.id.trajectory_id; i++)
      update_node_index += trajectory_nodes_size[i];
    mapping::NodeId node_id(0, update_node_index);
    nodes_[node_id] = nodes_temp.at(it.id);
    poses.push_back({it.data.global_pose.translation(), it.data.global_pose.rotation(),
                     Eigen::Vector2i(0, update_node_index)});
  }
  LOG(INFO) << "poses size: " << poses.size();
  kd_tree_.reset(new kd_tree::KDTree(poses));
  // for old merged map
  //   for(int i =0; i< num_range_data_.size(); i++)
  //   {
  //     proto::TrajectoryBuilderOptionsWithSensorIds options =
  //       all_trajectory_builder_options_[i];
  //     num_range_data_[i] =
  //       options.trajectory_builder_options().trajectory_builder_2d_options().submaps_options().num_range_data();
  //     int j =0, node_cnt = 0;
  //     int max_cnt = nodes_sort_by_trajectory_[i].size() / num_range_data_[i]
  //     - 1; for(const auto& node_id:nodes_sort_by_trajectory_[i] )
  //     {
  //       if(node_cnt < num_range_data_[i])
  //       {
  //         submap_with_nodes_[SubmapId(i,j)].push_back(nodes.at(node_id));
  //       }
  //       else if(j < max_cnt)
  //       {
  //         submap_with_nodes_[SubmapId(i,j)].push_back(nodes.at(node_id));
  //         if(j+1 < max_cnt)
  //           submap_with_nodes_[SubmapId(i,j+1)].push_back(nodes.at(node_id));
  //       }
  //       else
  //         break;
  //       if(node_cnt%num_range_data_[i] == 0 || node_cnt ==
  //       num_range_data_[i])
  //         ++j;
  //       ++node_cnt;
  //     }
  //     LOG(WARNING) << num_range_data_[i] << "\t cnt: " << node_cnt << "\t" <<
  //     j;
  //   }

  const vector<PoseGraphInterface::Constraint> &constraints = origin_pose_graph_->constraints();
  LOG(INFO) << "constraints size: " << constraints.size();
  std::map<int, std::set<SubmapId>> trajectory_with_submaps_size;
  for (PoseGraphInterface::Constraint constraint : origin_pose_graph_->constraints())
  {
    if (constraint.tag == PoseGraphInterface::Constraint::INTRA_SUBMAP)
    {
      int update_node_index = constraint.node_id.node_index;
      for (int i = 0; i < constraint.node_id.trajectory_id; i++)
        update_node_index += trajectory_nodes_size[i];
      constraint.node_id.trajectory_id = 0;
      constraint.node_id.node_index = update_node_index;
      submap_with_nodes_[constraint.submap_id].push_back(nodes_.at(constraint.node_id));
      intra_constraints_[constraint.submap_id].push_back(constraint.node_id);
      trajectory_with_submaps_size[constraint.submap_id.trajectory_id].insert(constraint.submap_id);
    }
    else if (constraint.tag == PoseGraphInterface::Constraint::INTER_SUBMAP)
    {
      int update_node_index = constraint.node_id.node_index;
      for (int i = 0; i < constraint.node_id.trajectory_id; i++)
        update_node_index += trajectory_nodes_size[i];
      constraint.node_id.trajectory_id = 0;
      constraint.node_id.node_index = update_node_index;
      inter_constraints_[constraint.submap_id].push_back(constraint.node_id);
    }
  }
  transform::Rigid3d last_node_pose_inv;
  if (1)
  {
    int j = -1;
    int k = -1;
    LOG(INFO) << nodes_temp.size();
    bool flag = true;
    for (std::size_t i = 0; i < 2 * nodes_temp.size(); i++)
    {
      if (i % 150 == 0)
      {
        j++;
        k++;
      }

      mapping::NodeId node_id(0, i);
      if (nodes_temp.find(node_id) != nodes_temp.end())
      {
        if ((nodes_.at(node_id).global_pose * last_node_pose_inv).translation().norm() > 1)
        {
          j++;
          k = 0;
          submap_with_nodes_[cartographer::mapping::SubmapId(0, j)].push_back(nodes_.at(node_id));
          intra_constraints_[cartographer::mapping::SubmapId(0, j)].push_back(node_id);
          last_node_pose_inv = nodes_.at(node_id).global_pose.inverse();
          continue;
        }
        submap_with_nodes_[cartographer::mapping::SubmapId(0, j)].push_back(nodes_.at(node_id));
        intra_constraints_[cartographer::mapping::SubmapId(0, j)].push_back(node_id);
        if (j != 0 && k != 0)
        {
          submap_with_nodes_[cartographer::mapping::SubmapId(0, j - 1)].push_back(
              nodes_.at(node_id));
          intra_constraints_[cartographer::mapping::SubmapId(0, j - 1)].push_back(node_id);
          flag = true;
        }
        last_node_pose_inv = nodes_.at(node_id).global_pose.inverse();
      }
      else
      {
        if (flag == true)
        {
          j++;
          flag = false;
          k = 0;
        }
      }
    }
  }
  LOG(WARNING) << "intra_constraints_ size:" << intra_constraints_.size();
  LOG(WARNING) << "inter_constraints_ size:" << inter_constraints_.size();
  LOG(WARNING) << "trajectory_with_submaps size:" << trajectory_with_submaps_size.size();
  for (auto it : trajectory_with_submaps_size) // add unfinished nodes to submap
  {
    SubmapId sub_id(it.first, it.second.size());
    for (auto it2 : trajectory_with_unfinished_nodes[it.first])
    {
      submap_with_nodes_[sub_id].push_back(nodes_.at(it2));
      intra_constraints_[sub_id].push_back(it2);
    }
  }

  LOG(WARNING) << "new intra_constraints_ size:" << intra_constraints_.size();
  {
    // add node with new node_id
    for (const auto &node_id_data : nodes_temp)
    {
      SerializedData proto;
      auto *const node_proto = proto.mutable_node();
      int update_node_index = node_id_data.id.node_index;
      for (int i = 0; i < node_id_data.id.trajectory_id; i++)
        update_node_index += trajectory_nodes_size[i];

      const NodeId node_id(0, update_node_index);
      const transform::Rigid3d &node_pose = nodes_.at(node_id).global_pose;
      node_proto->mutable_node_id()->set_trajectory_id(0);
      node_proto->mutable_node_id()->set_node_index(update_node_index);
      *node_proto->mutable_node_data() = ToProto(*node_id_data.data.constant_data);
      new_pose_graph_->AddNodeFromProto(node_pose, proto.node());
    }
  }

  //   for(const auto& constrait :constraints)
  //   {
  //     int origin_node_index = constrait.node_id.node_index;
  //     int update_node_index = origin_node_index;
  //     for(int i =0; i < constrait.node_id.trajectory_id; i++)
  //       update_node_index += trajectory_nodes_size[i];
  //     new_constraints_[constrait.submap_id].push_back(NodeId(0,update_node_index));
  //   }

  LOG(INFO) << "prehandle done !";
}

void PbstreamConvertor::mergeSubmaps(const vector<SubmapId> &ids, const SubmapId &new_submap_id)
{
  //   absl::MutexLock lock(&mutex_);
  mapping::ValueConversionTables conversion_tables;
  mapping::proto::ProbabilityGridRangeDataInserterOptions2D options;
  options.set_hit_probability(0.66);
  options.set_miss_probability(0.49);
  options.set_insert_free_space(true);
  mapping::RangeDataInserterInterface *range_data_inserter =
      new mapping::ProbabilityGridRangeDataInserter2D(options);

  std::unique_ptr<mapping::Submap2D> merge_submap;
  bool initialised = false;
  //  avoid repetition
  set<common::Time> avoid_repetition_nodes;
  set<NodeId> constraint_nodes;

  std::set<NodeId> temp_node_ids;
  //   float min_score =
  //   options_.pose_graph_options().constraint_builder_options().min_score();
  double translation_weight =
      options_.pose_graph_options().constraint_builder_options().loop_closure_translation_weight();
  double rotation_weight =
      options_.pose_graph_options().constraint_builder_options().loop_closure_rotation_weight();

  for (const auto &submap_id : ids)
  {
    for (const auto &node : submap_with_nodes_[submap_id])
    {
      //       if(avoid_repetition_nodes.emplace(node.time()).second)
      //         continue;
      sensor::PointCloud points = node.constant_data->filtered_gravity_aligned_point_cloud;
      const transform::Rigid3d gravity_alignment =
          transform::Rigid3d::Rotation(node.constant_data->gravity_alignment);
      transform::Rigid3f global_pose =
          node.global_pose.cast<float>() * gravity_alignment.inverse().cast<float>();
      //       transform::Rigid3f global_pose = node.global_pose.cast<float>();
      transform::Rigid2f global_pose_2d = transform::Project2D(global_pose);
      sensor::PointCloud points_in_global = sensor::TransformPointCloud(points, global_pose);
      Eigen::Vector2f origin;
      origin = global_pose_2d.translation();
      sensor::RangeData rangedata{{origin(0), origin(1), 0}, points_in_global, {}};
      if (!initialised)
      {
        origin = Eigen::Vector2f(clustering_submaps_poses_[new_submap_id.submap_index].x,
                                 clustering_submaps_poses_[new_submap_id.submap_index].y);
        LOG(WARNING) << new_submap_id << " origin:" << origin.transpose();
        //         if(new_submap_id.submap_index == 0)
        //           origin= Eigen::Vector2f(0,0);
        merge_submap.reset(
            new mapping::Submap2D(node.time(), origin, // Eigen::Vector2f(0,0)
                                  std::unique_ptr<mapping::Grid2D>(static_cast<mapping::Grid2D *>(
                                      CreateGrid(origin, // Eigen::Vector2f(0,0)
                                                 &conversion_tables)
                                          .release())),
                                  &conversion_tables));
        initialised = true;
      }
      merge_submap->InsertRangeData(rangedata, range_data_inserter);
    }

    for (auto node_id : new_constraints_[submap_id])
      constraint_nodes.insert(node_id);
  }
  merge_submap->Finish();

  proto::Submap submap_proto = merge_submap->ToProto(true);
  proto::SubmapId *proto_submap_id = submap_proto.mutable_submap_id();

  proto_submap_id->set_trajectory_id(new_submap_id.trajectory_id);
  proto_submap_id->set_submap_index(new_submap_id.submap_index);
  new_pose_graph_->AddSubmapFromProto(merge_submap->local_pose(), submap_proto);

  for (const auto &submap_id : ids)
  {
    for (NodeId node_id : intra_constraints_[submap_id])
    {
      if (temp_node_ids.emplace(node_id).second)
      {
        cartographer::mapping::TrajectoryNode node = nodes_.at(node_id);
        const transform::Rigid3d gravity_alignment =
            transform::Rigid3d::Rotation(node.constant_data->gravity_alignment);
        transform::Rigid2d node_pose_2d =
            transform::Project2D<double>(node.global_pose * gravity_alignment.inverse());
        //         float score = 0.;
        if (!is_frezon_ && 1)
        {
          //           std::unique_ptr<scan_matching::FastCorrelativeScanMatcher2D>
          //           fast_correlative_scan_matcher =
          //             absl::make_unique<scan_matching::FastCorrelativeScanMatcher2D>(
          //                   *(merge_submap->grid()),
          //                   options_.pose_graph_options().constraint_builder_options().fast_correlative_scan_matcher_options());
          //           fast_correlative_scan_matcher->Match(node_pose_2d,
          //                                               node.constant_data->filtered_gravity_aligned_point_cloud,
          //                                               min_score,
          //                                               &score,
          //                                               &node_pose_2d);
          //           if(score <= min_score)
          //            {
          //              LOG(WARNING) << node_id <<" "<< new_submap_id <<" "<<
          //              merge_submap->local_pose()<< " " <<node_pose_2d;
          //              continue;
          //            }
          ceres::Solver::Summary unused_summary;
          ceres_scan_matcher_.Match(node_pose_2d.translation(), node_pose_2d,
                                    node.constant_data->filtered_gravity_aligned_point_cloud,
                                    *(merge_submap->grid()), &node_pose_2d, &unused_summary);
        }

        transform::Rigid3d node_pose = transform::Embed3D<double>(node_pose_2d) * gravity_alignment;
        constraints_.push_back(PoseGraph::Constraint{
            new_submap_id,
            node_id,
            {merge_submap->local_pose().inverse() * node_pose, translation_weight, rotation_weight},
            PoseGraphInterface::Constraint::INTRA_SUBMAP});
      }
    }
    LOG(WARNING) << "constraints size:" << constraints_.size();
    for (NodeId node_id : inter_constraints_[submap_id])
    {
      if (temp_node_ids.emplace(node_id).second)
      {
        cartographer::mapping::TrajectoryNode node = nodes_.at(node_id);

        const transform::Rigid3d gravity_alignment =
            transform::Rigid3d::Rotation(node.constant_data->gravity_alignment);
        transform::Rigid2d node_pose_2d =
            transform::Project2D<double>(node.global_pose * gravity_alignment.inverse());
        //         float score = 0.;
        if (!is_frezon_ && 1)
        {
          //           std::unique_ptr<scan_matching::FastCorrelativeScanMatcher2D>
          //           fast_correlative_scan_matcher =
          //           absl::make_unique<scan_matching::FastCorrelativeScanMatcher2D>(
          //                 *(merge_submap->grid()),
          //                 options_.pose_graph_options().constraint_builder_options().fast_correlative_scan_matcher_options());
          //           fast_correlative_scan_matcher->Match(node_pose_2d,
          //                                               node.constant_data->filtered_gravity_aligned_point_cloud,
          //                                               min_score,
          //                                               &score,
          //                                               &node_pose_2d);
          //            if(score <= min_score)
          //            {
          //              LOG(WARNING) << node_id <<" "<< new_submap_id <<" "<<
          //              transform::Project2D<double>(node.global_pose *
          //              gravity_alignment.inverse()) << " " <<node_pose_2d;
          //              continue;
          //            }

          ceres::Solver::Summary unused_summary;
          ceres_scan_matcher_.Match(node_pose_2d.translation(), node_pose_2d,
                                    node.constant_data->filtered_gravity_aligned_point_cloud,
                                    *(merge_submap->grid()), &node_pose_2d, &unused_summary);

          if (0)
            LOG(WARNING) << node_id << " " << new_submap_id << " "
                         << transform::Project2D<double>(node.global_pose *
                                                         gravity_alignment.inverse())
                         << " " << node_pose_2d;
        }
        transform::Rigid3d node_pose = transform::Embed3D<double>(node_pose_2d) * gravity_alignment;
        constraints_.push_back(PoseGraph::Constraint{
            new_submap_id,
            node_id,
            {merge_submap->local_pose().inverse() * node_pose, translation_weight, rotation_weight},
            PoseGraphInterface::Constraint::INTER_SUBMAP});
      }
    }

    if (0)
    {
      transform::Rigid3d submap_pose_inv = merge_submap->local_pose().inverse();
      for (auto it : nodes_)
      {
        mapping::NodeId node_id = it.first;
        cartographer::mapping::TrajectoryNode node = it.second;
        //         transform::Rigid3d node_pose = node.global_pose;
        const transform::Rigid3d gravity_alignment =
            transform::Rigid3d::Rotation(node.constant_data->gravity_alignment);
        transform::Rigid2d node_pose_2d =
            transform::Project2D<double>(node.global_pose * gravity_alignment.inverse());

        if (temp_node_ids.emplace(node_id).second &&
            (submap_pose_inv * node.global_pose * gravity_alignment.inverse())
                    .translation()
                    .norm() < 10)
        {
          //           const transform::Rigid3d gravity_alignment =
          //               transform::Rigid3d::Rotation(node.constant_data->gravity_alignment);
          //           transform::Rigid2d node_pose_2d =
          //           transform::Project2D<double>(node.global_pose *
          //           gravity_alignment.inverse()); float score = 0.;
          //
          //           std::unique_ptr<scan_matching::FastCorrelativeScanMatcher2D>
          //           fast_correlative_scan_matcher =
          //           absl::make_unique<scan_matching::FastCorrelativeScanMatcher2D>(
          //                 *(merge_submap->grid()),
          //                 options_.pose_graph_options().constraint_builder_options().fast_correlative_scan_matcher_options());
          //           fast_correlative_scan_matcher->Match(node_pose_2d,
          //                                               node.constant_data->filtered_gravity_aligned_point_cloud,
          //                                               min_score,
          //                                               &score,
          //                                               &node_pose_2d);
          //            if(score <= min_score)
          //            {
          //              LOG(WARNING) << node_id <<" "<< new_submap_id <<" "<<
          //              transform::Project2D<double>(node.global_pose *
          //              gravity_alignment.inverse()) << " " <<node_pose_2d;
          //              continue;
          //            }
          //            ceres::Solver::Summary unused_summary;
          //           ceres_scan_matcher_.Match(node_pose_2d.translation(),
          //           node_pose_2d,
          //                                     node.constant_data->filtered_gravity_aligned_point_cloud,
          //                                     *(merge_submap->grid()),
          //                                     &node_pose_2d,
          //                                     &unused_summary);
          transform::Rigid3d node_pose =
              transform::Embed3D<double>(node_pose_2d) * gravity_alignment;
          constraints_.push_back(
              PoseGraph::Constraint{new_submap_id,
                                    node_id,
                                    {merge_submap->local_pose().inverse() * node_pose,
                                     translation_weight, rotation_weight},
                                    PoseGraphInterface::Constraint::INTER_SUBMAP});
        }
      }
    }
    LOG(WARNING) << "constraints size:" << constraints_.size();
  }

  if (debug_flag_)
    getImage(std::move(merge_submap),
             transform::Rigid3f(
                 Eigen::Vector3f(clustering_submaps_poses_[new_submap_id.submap_index].x,
                                 clustering_submaps_poses_[new_submap_id.submap_index].y, 0),
                 Eigen::Quaternionf::Identity()));
}

void PbstreamConvertor::getImage(std::unique_ptr<mapping::Submap2D> merge_submap,
                                 const transform::Rigid3f &global_pose)
{
  std::map<::cartographer::mapping::SubmapId, ::cartographer::io::SubmapSlice> submap_slices;
  SubmapSlice &submap_slice = submap_slices[SubmapId(0, 0)];
  //    string str = submap_query_client_.getService();
  cartographer_ros_msgs::SubmapQuery::Response response_query;
  proto::SubmapQuery::Response response_proto;
  merge_submap->ToResponseProto(global_pose.cast<double>(), &response_proto);
  for (const auto &texture_proto : response_proto.textures())
  {
    response_query.textures.emplace_back();
    auto &texture = response_query.textures.back();
    texture.cells.insert(texture.cells.begin(), texture_proto.cells().begin(),
                         texture_proto.cells().end());
    texture.width = texture_proto.width();
    texture.height = texture_proto.height();
    texture.resolution = texture_proto.resolution();
    LOG(INFO) << "resolution: " << texture_proto.resolution();
  }
  auto response = absl::make_unique<::cartographer::io::SubmapTextures>();
  for (const auto &texture : response_query.textures)
  {
    const std::string compressed_cells(texture.cells.begin(), texture.cells.end());
    response->textures.emplace_back(::cartographer::io::SubmapTexture{
        ::cartographer::io::UnpackTextureData(compressed_cells, texture.width, texture.height),
        texture.width, texture.height, texture.resolution, global_pose.cast<double>()});
  }
  submap_slice.version = response->version;

  // We use the first texture only. By convention this is the highest
  // resolution texture and that is the one we want to use to construct the
  // map for ROS.
  const auto fetched_texture = response->textures.begin();
  submap_slice.width = fetched_texture->width;
  submap_slice.height = fetched_texture->height;
  submap_slice.slice_pose = fetched_texture->slice_pose;
  submap_slice.resolution = fetched_texture->resolution;
  submap_slice.cairo_data.clear();

  submap_slice.surface = ::cartographer::io::DrawTexture(
      fetched_texture->pixels.intensity, fetched_texture->pixels.alpha, fetched_texture->width,
      fetched_texture->height, &submap_slice.cairo_data);

  PaintSubmapSlicesResult result = PaintSubmapSlice(submap_slice, 0.05);
  const int width = cairo_image_surface_get_width(result.surface.get());
  const int height = cairo_image_surface_get_height(result.surface.get());
  const uint32_t *pixel_data =
      reinterpret_cast<uint32_t *>(cairo_image_surface_get_data(result.surface.get()));
  cv::Mat pub_img(height, width, CV_8UC4);
  cv::Mat show_img(height, width, CV_8UC4);
  for (int y = height - 1; y >= 0; --y)
  {
    for (int x = 0; x < width; ++x)
    {
      const uint32_t packed = pixel_data[y * width + x];
      const unsigned char color = packed >> 16;
      const unsigned char observed = packed >> 8;
      //       const int delta =
      //         128 - int(color);
      //       const unsigned char alpha = delta > 0 ? delta : -delta;
      const int value =
          observed == 0 ? -1 : ::cartographer::common::RoundToInt((1. - color / 255.) * 100.);
      CHECK_LE(-1, value);
      CHECK_GE(100, value);
      show_img.at<Vec4b>(y, x)[0] = color;
      show_img.at<Vec4b>(y, x)[1] = color;
      show_img.at<Vec4b>(y, x)[2] = color;
      show_img.at<Vec4b>(y, x)[3] = 255;
      //       if (color == 128)
      //       {
      //         pub_img.at<Vec4b>(y, x)[0] = 128;
      //         pub_img.at<Vec4b>(y, x)[1] = 128;
      //         pub_img.at<Vec4b>(y, x)[2] = 128;
      //         pub_img.at<Vec4b>(y, x)[3] = 0;
      //       }
      //
      //       if(color == 128)
      //           save_img.at<uint16_t>(y, x) = 0;
      //         else
      //           save_img.at<uint16_t>(y, x) = color * 128;
    }
  }

  imshow("show_img", show_img);
  waitKey(10);
}

bool PbstreamConvertor::handlePbstream(const std::string &origin_map_name,
                                       const std::string &new_map_name)
{
  std::string map_path = std::string(getenv("HOME")) + "/map/";
  std::string origin_pbstream = map_path + origin_map_name + "/map.pbstream";
  if (FILE *file = fopen(origin_pbstream.c_str(), "r"))
  {
    fclose(file);
  }
  else
  {
    LOG(ERROR) << origin_pbstream + " is not existed!";
    return false;
  }

  cartographer::io::ProtoStreamReader stream(origin_pbstream);
  LoadState(&stream, true);
  preHandle();
  clusteringSubmaps();

  LOG(INFO) << "start merging submaps ...";
  for (auto submap_ids : clustering_submaps_)
  {
    mergeSubmaps(submap_ids.second, SubmapId(0, submap_ids.first));
  }
  new_pose_graph_->AddSerializedConstraints(constraints_);
  LOG(INFO) << "merge submaps done !";

  for (const auto &submap_id_data : new_pose_graph_->GetAllSubmapData())
  {
    LOG(WARNING) << submap_id_data.data.pose;
  }
  if (!is_frezon_)
  {
    LOG(WARNING) << "Run Final Optimization!";
    new_pose_graph_->RunFinalOptimization();
  }
  for (const auto &submap_id_data : new_pose_graph_->GetAllSubmapData())
  {
    LOG(WARNING) << submap_id_data.data.pose;
  }
  std::string full_path = map_path + new_map_name;
  std::string hide_path = map_path + "." + new_map_name;
  {
    std::string system_command = "rm -r -f " + full_path;
    LOG(INFO) << system_command;
    int status = system(system_command.c_str());
    system_command = "mkdir " + full_path;
    LOG(INFO) << system_command;
    status = system(system_command.c_str());

    system_command = "rm -r -f " + hide_path;
    LOG(INFO) << system_command;
    status = system(system_command.c_str());
    system_command = "mkdir " + hide_path;
    LOG(INFO) << system_command;
    status = system(system_command.c_str());
  }

  LOG(INFO) << "save_pbstream_path: " << full_path;
  io::ProtoStreamWriter writer(full_path + "/map.pbstream");
  while (all_trajectory_builder_options_.size() > 1)
    all_trajectory_builder_options_.pop_back();

  LOG(INFO) << "start writing pbstream ... ";
  io::WritePbStream(*new_pose_graph_, all_trajectory_builder_options_, &writer, false);
  LOG(INFO) << "Writing pbstream done! ";
  saveMapFolder(new_map_name);
  new_nodes_ = new_pose_graph_->GetTrajectoryNodes();
  updateCornerPoints(map_path + origin_map_name, full_path);
  LOG(INFO) << "done!";
  return true;
}

void PbstreamConvertor::setFrezon(const bool &is_frezon) { is_frezon_ = is_frezon; }

void PbstreamConvertor::saveMapFolder(const std::string &new_map_name)
{
  cartographer_ros_msgs::SaveMapServer srv;
  uint submap_cnt = 0;
  std::map<cartographer::mapping::SubmapId, int> submap_submapId;
  std::map<int, cartographer::mapping::SubmapId> submapId_submap;
  for (const auto &submap_id_data : new_pose_graph_->GetAllSubmapData())
  {
    cartographer::mapping::SubmapId submap_id = submap_id_data.id;
    submap_submapId.insert(std::pair<cartographer::mapping::SubmapId, int>(submap_id, submap_cnt));
    submapId_submap.insert(std::pair<int, cartographer::mapping::SubmapId>(submap_cnt, submap_id));
    submap_cnt++;
  }
  std::vector<std::set<int>> connections;
  std::map<cartographer::mapping::NodeId, int> node_submapId;
  std::vector<cartographer::mapping::PoseGraph::Constraint> constraints =
      new_pose_graph_->constraints();
  // LOG(INFO)<<constraints.size();
  connections.resize(submap_cnt);

  // determine which submap each node(intra) belongs to
  for (uint constraints_id = 0; constraints_id < constraints.size(); constraints_id++)
  {
    cartographer::mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
    if (constraint.tag == cartographer::mapping::PoseGraph::Constraint::INTRA_SUBMAP &&
        submap_submapId.find(constraint.submap_id) != submap_submapId.end())
    {
      node_submapId.insert(std::pair<cartographer::mapping::NodeId, int>(
          constraint.node_id, submap_submapId[constraint.submap_id]));
    }
  }

  // add adjacent connections
  for (uint i = 0; i < submap_cnt - 1; i++)
  {
    connections[i].insert(i + 1);
    connections[i + 1].insert(i);
  }

  // add connections induced by nodes(inter)
  for (uint constraints_id = 0; constraints_id < constraints.size(); constraints_id++)
  {
    cartographer::mapping::PoseGraph::Constraint constraint = constraints[constraints_id];
    if (constraint.tag == cartographer::mapping::PoseGraph::Constraint::INTER_SUBMAP &&
        submap_submapId.find(constraint.submap_id) != submap_submapId.end() &&
        node_submapId.find(constraint.node_id) != node_submapId.end())
    {
      connections[submap_submapId[constraint.submap_id]].insert(node_submapId[constraint.node_id]);
      connections[node_submapId[constraint.node_id]].insert(submap_submapId[constraint.submap_id]);
    }
  }

  std::map<cartographer::mapping::SubmapId, std::set<cartographer::mapping::SubmapId>>
      classfied_constraints, node_constraints;
  for (uint i = 0; i < connections.size(); i++)
  {
    cartographer::mapping::SubmapId id1 = submapId_submap.find(i)->second;
    for (auto id : connections[i])
    {
      cartographer::mapping::SubmapId id2 = submapId_submap.find(id)->second;
      classfied_constraints[id1].insert(id2);
    }
  }

  {
    for (const auto &submap_id_data : new_pose_graph_->GetAllSubmapData())
    {
      ::cartographer::mapping::PoseGraph::SubmapData submapData = submap_id_data.data;
      const ::cartographer::mapping::Submap2D *submap;
      submap = (const cartographer::mapping::Submap2D *)submapData.submap.get();
      cartographer_ros_msgs::SaveSubmap submap_info;
      submap_info.id.trajectory_id = submap_id_data.id.trajectory_id;
      submap_info.id.submap_index = submap_id_data.id.submap_index;
      //       submap_info.pose_in_map = submapData.pose;
      submap_info.pose_in_map.position.x = submapData.pose.translation().x();
      submap_info.pose_in_map.position.y = submapData.pose.translation().y();
      submap_info.pose_in_map.position.z = submapData.pose.translation().z();

      submap_info.pose_in_map.orientation.x = submapData.pose.rotation().x();
      submap_info.pose_in_map.orientation.y = submapData.pose.rotation().y();
      submap_info.pose_in_map.orientation.z = submapData.pose.rotation().z();
      submap_info.pose_in_map.orientation.w = submapData.pose.rotation().w();

      //      submap_info.pose_in_local = submap->local_pose();
      submap_info.pose_in_local.position.x = submap->local_pose().translation().x();
      submap_info.pose_in_local.position.y = submap->local_pose().translation().y();
      submap_info.pose_in_local.position.z = submap->local_pose().translation().z();

      submap_info.pose_in_local.orientation.x = submap->local_pose().rotation().x();
      submap_info.pose_in_local.orientation.y = submap->local_pose().rotation().y();
      submap_info.pose_in_local.orientation.z = submap->local_pose().rotation().z();
      submap_info.pose_in_local.orientation.w = submap->local_pose().rotation().w();

      submap_info.num_cells[0] = submap->grid()->limits().cell_limits().num_x_cells;
      submap_info.num_cells[1] = submap->grid()->limits().cell_limits().num_y_cells;
      submap_info.max[0] = submap->grid()->limits().max()[0];
      submap_info.max[1] = submap->grid()->limits().max()[1];
      cartographer::mapping::SubmapId temp_id(submap_id_data.id.trajectory_id,
                                              submap_id_data.id.submap_index);

      for (auto &constraint : classfied_constraints[temp_id])
      {
        cartographer_ros_msgs::SubmapID temp;
        temp.trajectory_id = constraint.trajectory_id;
        temp.submap_index = constraint.submap_index;
        submap_info.constraints.push_back(temp);
      }
      cartographer_ros_msgs::SubmapQuery query;
      query.request.trajectory_id = submap_id_data.id.trajectory_id;
      query.request.submap_index = submap_id_data.id.submap_index;
      {
        cartographer::mapping::SubmapId submap_id{query.request.trajectory_id,
                                                  query.request.submap_index};
        cartographer::mapping::proto::SubmapQuery::Response response_proto;
        submapData.submap->ToResponseProto(submapData.pose, &response_proto);
        query.response.submap_version = response_proto.submap_version();
        for (const auto &texture_proto : response_proto.textures())
        {
          query.response.textures.emplace_back();
          auto &texture = query.response.textures.back();
          texture.cells.insert(texture.cells.begin(), texture_proto.cells().begin(),
                               texture_proto.cells().end());
          texture.width = texture_proto.width();
          texture.height = texture_proto.height();
          texture.resolution = texture_proto.resolution();

          transform::Rigid3d temp = cartographer::transform::ToRigid3(texture_proto.slice_pose());
          geometry_msgs::Pose pose;
          pose.position.x = temp.translation().x();
          pose.position.y = temp.translation().y();
          pose.position.z = temp.translation().z();
          pose.orientation.w = temp.rotation().w();
          pose.orientation.x = temp.rotation().x();
          pose.orientation.y = temp.rotation().y();
          pose.orientation.z = temp.rotation().z();
          texture.slice_pose = pose;
          LOG(WARNING) << "texture.slice_pose:" << temp;
        }

        query.response.status.message = "Success.";
        query.response.status.code = cartographer_ros_msgs::StatusCode::OK;
      }
      submap_info.submap_version = query.response.submap_version;
      submap_info.textures = query.response.textures;
      srv.request.submaps.push_back(submap_info);
      LOG(WARNING) << "pose:" << submapData.pose;
      LOG(WARNING) << "local_pose:" << submap->local_pose();
    }
  }

  {
    std::string data;
    for (const auto &landmark : new_pose_graph_->GetLandmarkPoses())
    {
      std::string ns;
      std::string id_s = landmark.first;
      int id_i = std::stoi(id_s);
      if (id_i >= 1000000)
      {
        id_i = id_i - 1000000;
        ns = "Lasermarks";
      }
      else
        ns = "Visualmarks";
      cartographer_ros_msgs::LandmarkNewEntry entry;
      entry.ns = ns;
      entry.id = std::to_string(id_i);
      entry.visible = 1;
      entry.tracking_from_landmark_transform.position.x = landmark.second.translation().x();
      entry.tracking_from_landmark_transform.position.y = landmark.second.translation().y();
      entry.tracking_from_landmark_transform.position.z = landmark.second.translation().z();

      entry.tracking_from_landmark_transform.orientation.x = landmark.second.rotation().x();
      entry.tracking_from_landmark_transform.orientation.y = landmark.second.rotation().y();
      entry.tracking_from_landmark_transform.orientation.z = landmark.second.rotation().z();
      entry.tracking_from_landmark_transform.orientation.w = landmark.second.rotation().w();
      srv.request.landmarks.push_back(entry);
    }
  }

  {
    srv.request.map_name = new_map_name;
    MapMapping temp;
    if (temp.saveMapServer(srv.request, srv.response))
      LOG(INFO) << "Save Map Success!";
    else
      LOG(WARNING) << "Save Map Failed!";
  }
}

void PbstreamConvertor::updatePoints(std::vector<Pose3d> &nav_points)
{
  for (Pose3d &point : nav_points)
  {
    kd_tree::pose pose{point.p, Eigen::Quaterniond::Identity(), Eigen::Vector2i(0, 0)};

    kd_tree::pose result = kd_tree_->nearest_point(pose);
    mapping::NodeId node_id(result.id(0), result.id(1));
    transform::Rigid3d origin_nearest_pose = nodes_.at(node_id).global_pose;
    double nearest_dist = sqrt((point.p.x() - origin_nearest_pose.translation().x()) *
                                   (point.p.x() - origin_nearest_pose.translation().x()) +
                               (point.p.y() - origin_nearest_pose.translation().y()) *
                                   (point.p.y() - origin_nearest_pose.translation().y()));

    kd_tree::pointIndexArr near_nodes = kd_tree_->neighborhood(pose, nearest_dist + 2);
    if (near_nodes.size() < 2)
    {
      transform::Rigid3d new_pose = new_nodes_.at(node_id).global_pose;
      transform::Rigid3d point_pose(point.p, point.q);
      transform::Rigid3d new_point = new_pose * origin_nearest_pose.inverse() * point_pose;
      point.p = new_point.translation();
      point.q = new_point.rotation();
      continue;
    }

    Problem problem;
    ceres::LossFunction *loss_function = NULL;
    double point_angle = GetYaw(point.q);
    //     LOG(INFO) << "\033[32m" ;
    for (kd_tree::pointIndex node : near_nodes)
    {
      mapping::NodeId node_id(node.first.id(0), node.first.id(1));
      transform::Rigid3d origin_pose = nodes_.at(node_id).global_pose;
      transform::Rigid3d new_pose = new_nodes_.at(node_id).global_pose;
      double dist = sqrt((point.p.x() - origin_pose.translation().x()) *
                             (point.p.x() - origin_pose.translation().x()) +
                         (point.p.y() - origin_pose.translation().y()) *
                             (point.p.y() - origin_pose.translation().y()));
      transform::Rigid3d point_pose(point.p, point.q);
      const transform::Rigid3d point_in_global = new_pose * origin_pose.inverse() * point_pose;

      const double temp_yaw = GetYaw(point_in_global.rotation());
      ceres::CostFunction *cost_function =
          pointConstraint2D::Create(point_in_global.translation().x(),
                                    point_in_global.translation().y(), temp_yaw, 1.0 / dist);
      problem.AddResidualBlock(cost_function, loss_function, &(point.p(0)), &(point.p(1)),
                               &point_angle);
    }
    //     LOG(INFO) << "\033[37m"  ;
    ceres::Solver::Options options;
    options.max_num_iterations = 10;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    point.q = Eigen::AngleAxis<double>(point_angle, Eigen::Matrix<double, 3, 1>::UnitZ());
  }
}

void PbstreamConvertor::updateCornerPoints(const string &origin_full_path, const string &full_path)
{
  std::vector<Pose3d> corner_points_poses;
  {
    boost::property_tree::ptree pt;
    if (!judgeFileExist(origin_full_path + "/corner_points.xml")) return;
    boost::property_tree::xml_parser::read_xml(origin_full_path + "/corner_points.xml", pt);

    string xml;
    if (pt.empty())
    {
      LOG(WARNING) << "corner_points.xml is empty!";
      return;
    }

    for (auto &mark : pt)
    {
      if (mark.first == "corner_points")
      {
        int points_size = stoi(mark.second.get("points_size", ""));
        CHECK(points_size == 4);

        xml = mark.second.get("points", " ");
        float value[8];
        sscanf(xml.c_str(), "%f %f %f %f %f %f %f %f", &value[0], &value[1], &value[2], &value[3],
               &value[4], &value[5], &value[6], &value[7]);
        // TODO new msg will be here

        for (int i = 0; i < points_size; i++)
        {
          Eigen::Vector3d corner_point(value[i * 2], value[i * 2 + 1], 0);
          Pose3d temp;
          temp.p = corner_point;
          temp.q.setIdentity();
          corner_points_poses.push_back(temp);
        }
      }
    }
  }

  updatePoints(corner_points_poses);

  std::vector<Eigen::Vector3d> corner_points;
  for (auto &it : corner_points_poses)
    corner_points.push_back(it.p);

  int size_of_map_[2];
  double maxes_[2];
  double resolution;
  {
    boost::property_tree::ptree pt1;
    string submap_filepath = full_path + "/frames/0";
    boost::property_tree::read_xml(submap_filepath + "/data.xml", pt1);

    if (pt1.empty())
    {
      ROS_FATAL("Load %s/data.xml failed!", submap_filepath.c_str());
      return;
    }
    std::string data;

    resolution = pt1.get<double>("probability_grid.resolution");
    data = pt1.get<std::string>("probability_grid.max");
    sscanf(data.c_str(), "%lf %lf", &maxes_[0], &maxes_[1]);
    data = pt1.get<std::string>("probability_grid.num_cells");
    sscanf(data.c_str(), "%d %d", &size_of_map_[0], &size_of_map_[1]);
  }

  {
    boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
    Mat mask = Mat(Size(size_of_map_[0], size_of_map_[1]), CV_8UC1, Scalar(255));
    std::string data;
    boost::property_tree::ptree p_info;
    boost::property_tree::ptree p_list;
    int cnt = 0;
    for (std::size_t i = 0; i < corner_points.size() / 4; i++)
    {
      vector<vector<Point>> contours;
      vector<Point> contour;
      for (std::size_t j = i * 4; j < i * 4 + 4; j++)
      {
        Eigen::Vector2d point_in_map(corner_points[j](0), corner_points[j](1));
        Eigen::Vector2i imageLoc;
        imageLoc[0] = (maxes_[1] - point_in_map(1)) / resolution;
        imageLoc[1] = (maxes_[0] - point_in_map(0)) / resolution;
        Point p(imageLoc(0), imageLoc(1));
        contour.push_back(p);
        data = data + std::to_string(corner_points[j](0)) + " ";
        if (corner_points[j](0) == corner_points.back().x() &&
            corner_points[j](1) == corner_points.back().y())
          data = data + std::to_string(corner_points[j](1));
        else
          data = data + std::to_string(corner_points[j](1)) + " ";
        cnt++;
      }

      p_info.put("points_size", cnt);
      p_info.put("points", data);
      p_list.add_child("corner_points", p_info);
      data.clear();
      cnt = 0;
      contours.push_back(contour);
      cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
    }
    boost::property_tree::write_xml(full_path + "/corner_points.xml", p_list, std::locale(),
                                    setting);

    imwrite(full_path + "/bool_image.png", mask);
  }
}

} // namespace pbstream_convertor
} // namespace map_manager
