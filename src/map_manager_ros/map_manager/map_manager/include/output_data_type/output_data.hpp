#ifndef OUTPUT_DATA_TYPE_OUTPUT_DATA_HPP_
#define OUTPUT_DATA_TYPE_OUTPUT_DATA_HPP_
#include <opencv2/opencv.hpp>
#include "../transform.h"
namespace output_data_type{
struct SubmapImageEntry
{
  int32_t trajectory_id;
  int32_t submap_index;
  int32_t submap_version;
  double resolution;
  Rigid3d pose_in_map;
  cv::Mat image;
  SubmapImageEntry(){};
  SubmapImageEntry(int32_t newTrajectoryId, int32_t newSubmapIndex, int32_t newSubmapVersion,
                   double newResolution, Rigid3d &&newPoseInMap,
                   cv::Mat &&newImage)
      : trajectory_id(newTrajectoryId), submap_index(newSubmapIndex), submap_version(newSubmapVersion),
        resolution(newResolution), pose_in_map(newPoseInMap), image(std::move(newImage)){};
  SubmapImageEntry(SubmapImageEntry &&submapImageEntry)
  {
    trajectory_id = submapImageEntry.trajectory_id;
    submap_index = submapImageEntry.submap_index;
    submap_version = submapImageEntry.submap_version;
    resolution = submapImageEntry.resolution;
    pose_in_map = std::move(submapImageEntry.pose_in_map);
    image = std::move(submapImageEntry.image);
  }
  SubmapImageEntry &operator=(SubmapImageEntry &&submapImageEntry)
  {
    trajectory_id = submapImageEntry.trajectory_id;
    submap_index = submapImageEntry.submap_index;
    submap_version = submapImageEntry.submap_version;
    resolution = submapImageEntry.resolution;
    pose_in_map = std::move(submapImageEntry.pose_in_map);
    image = std::move(submapImageEntry.image);
  }
};

struct MapPosesInfo
{
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> local_in_global_poses;
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d>> poses_in_global;
  std::vector<double> maxes;
  std::vector<int> size_of_map;
};

struct MapData
{
  int width;
  int height;
  float resolution;
  Rigid3d transform;
};

struct IdRemap
{
  std::string ns;
  int origin_id;
  int new_id;
};


}
#endif
