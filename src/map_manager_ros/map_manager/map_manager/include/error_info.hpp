#ifndef ERROR_INFO_
#define ERROR_INFO_
#include <string>
namespace common
{
// 错误类型枚举
enum class ErrorType : int8_t
{
  kOk = 0,     // 正常
  kError,      // 一般错误
  kSystemError // 系统错误
};

struct ErrorInfo
{
  int retCode;          // 0正常，非0异常
  ErrorType errType;    // 错误类型枚举
  std::string errorMsg; // 错误信息
  ErrorInfo() : retCode(0), errType(ErrorType::kOk) {}

  ErrorInfo(const int &errCode, const std::string &errMsg, const ErrorType &errType)
      : retCode(errCode), errType(errType), errorMsg(errMsg)
  {
  }

  bool IsOk() const { return errType == ErrorType::kOk; }
};
} // namespace common

#endif
