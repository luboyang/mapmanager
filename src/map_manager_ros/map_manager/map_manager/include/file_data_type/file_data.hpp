#ifndef FILE_DATA_TYPE_FILE_DATA_HPP_
#define FILE_DATA_TYPE_FILE_DATA_HPP_

#include "../transform.h"
namespace file_data_type
{
struct SubmapData
{
  bool finished;
  int id;
  int numRangeData;
  double resolution;
  double max[2];
  int numCells[2];
  int knownCellsBox[4];
  Rigid3d pose;
  Rigid3d localPose;

  SubmapData() : finished(true), id(-1), numRangeData(0), resolution(0.05)
  {
    memset(max, 0, sizeof(max));
    memset(numCells, 0, sizeof(numCells));
    memset(knownCellsBox, 0, sizeof(knownCellsBox));
  }

  SubmapData(bool newFinished, int newId, int numRangeData, float newResolution, double *newMax,
                int *newNumCells, int *newKnownCellsBox, Rigid3d newPose,
                Rigid3d newLocalPose)
      : finished(newFinished), id(newId), numRangeData(numRangeData), resolution(newResolution),
        pose(newPose), localPose(newLocalPose)
  {
    memcpy(max, newMax, sizeof(max));
    memcpy(numCells, newNumCells, sizeof(numCells));
    memcpy(knownCellsBox, newKnownCellsBox, sizeof(knownCellsBox));
  }
};
/*
struct LandmarkData
{
  int visible;
  int id;
  double translationInImage[2];
  std::string ns;
  Rigid3d transform;
  LandmarkData() : visible(0), id(-1),transform(Rigid3d::Identity())
  {
    memset(translationInImage, 0, sizeof(translationInImage));
  }

  LandmarkData(int newVisible, int newId, double newTranslationInImage[2], const char *newNs,
               Rigid3d newTransform)
      : visible{newVisible}, id{newId}, ns{newNs}, transform{newTransform}
  {
    memcpy(translationInImage, newTranslationInImage, sizeof(translationInImage));
  }
};*/

struct NeighbourInMapData
{
  bool reachable;
  int id;
  Rigid3d transform;

//   NeighbourInMapData() : reachable(false), id(-1), transform(Rigid3d::Identity()){}
//   NeighbourInMapData(bool newReachable, int newId, Rigid3d newTransform)
//       : reachable(newReachable), id(newId), transform(newTransform){};
};

struct MapData
{
  int id;
  std::vector<NeighbourInMapData> neighbourList;

//   MapData() : id(0){};
//   MapData(int newId, std::vector<NeighbourInMapData> newNeighbourList)
//       : id(newId), neighbourList(newNeighbourList){};
};

struct LandmarkInfo
{
  std::string ns;
  std::string id;
  int visible;
  Rigid3d transform;
  Eigen::Vector2d translation_in_image;
  float translation_weight;
  float rotation_weight;
  float pole_radius;
};


}


#endif


