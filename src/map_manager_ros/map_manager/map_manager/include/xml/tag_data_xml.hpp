#ifndef TAG_DATA_XML
#define TAG_DATA_XML
#include <vector>
#include <string>
#include <geometry_msgs/Pose.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <utility/system/utils_system.hpp>
#include <utility/string/utils_string.hpp>
#include <common/data.hpp>
namespace tagdataxml
{
    class TagDataXml
    {
    public:
        TagDataXml(){};
        TagDataXml(data::TagDataNode newNode) : node_(newNode){};
        ~TagDataXml(){};

        bool SetTagDataXml(const std::string &path)
        {
            if(node_.id < 0)
            {
                printf("Node is empty!");
                return false;
            }
            boost::property_tree::ptree pTagData;
            pTagData.put("id", node_.id);
            pTagData.put("tag_id", node_.tagId);
            std::string poseStr;
            stringutils::PoseXmlToString(node_.pose, poseStr);
            pTagData.put("pose", poseStr);

            std::string folderString = path + "/frames/" + std::to_string(node_.id);
            std::string systemCommand = "mkdir -p " + folderString;
            std::string systemCommandErrorMsg;
            if (!systemutils::CheckSystemStatus(systemCommand,systemCommandErrorMsg))
            {
                printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
                return false;
            }

            boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
            boost::property_tree::write_xml(folderString + "/data.xml", pTagData,
                                            std::locale(), setting);
            return true;
        };

        bool GetTagDataXml(const std::string &path)
        {
            boost::property_tree::ptree tagDataXmlPt;
            boost::property_tree::xml_parser::read_xml(path + "/data.xml", tagDataXmlPt,
                                               boost::property_tree::xml_parser::trim_whitespace);
            if(tagDataXmlPt.empty())
            {
                printf("Tag data.xml is empty! Path is %s", path.c_str());
                return false;
            }
            node_.id = std::atoi(tagDataXmlPt.get("id", "").c_str());
            node_.tagId=std::atoi(tagDataXmlPt.get("tag_id", "").c_str());
            std::string poseStr = tagDataXmlPt.get("pose", "");
            stringutils::PoseStringToXml(poseStr, node_.pose);
            return true;
        };

        data::TagDataNode GetNode()
        {
            return node_;
        }
    private:
        data::TagDataNode node_;
    };
};
#endif