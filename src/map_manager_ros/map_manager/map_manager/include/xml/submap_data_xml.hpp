#ifndef LASER_DATA_XML
#define LASER_DATA_XML
#include <vector>
#include <string>
#include "file_data_type/file_data.hpp"
#include "utils/utils_system.hpp"
#include "utils/utils_string.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "tinyxml2/tinyxml2.h"
namespace data_xml
{
  class MapXml
  {
  public:
    MapXml(){};
    MapXml(std::vector<file_data_type::MapData> nodeList)
      : nodeCount_(nodeList.size()),nodeList_(nodeList){};
    ~MapXml(){};
    
    bool SetMapXml(const std::string &path)
    {
      if(0 == nodeCount_)
      {
        printf("NodeList is empty!");
        return false;
      }
      boost::property_tree::ptree pTop;
      boost::property_tree::ptree pMap;
      boost::property_tree::ptree pNodeList;
      boost::property_tree::ptree pNode;
      boost::property_tree::ptree pNeighbourList;
      boost::property_tree::ptree pNeighbour;
      boost::property_tree::ptree pProperty;
      std::string transformStr;
      for (const auto &node : nodeList_)
      {
        pNode.put("id", node.id);
        for (const auto &neighbour : node.neighbourList)
        {
          pNeighbour.put("id", neighbour.id);
          pNeighbour.put("reachable", neighbour.reachable);
          stringutils::PoseXmlToString(neighbour.transform, transformStr);
          pNeighbour.put("transform", transformStr);
          pNeighbourList.add_child("neighbour", pNeighbour);
          pNeighbour.clear();
        }
        pNode.add_child("neighbour_list", pNeighbourList);
        pNodeList.add_child("node", pNode);
        pNode.clear();
        pNeighbourList.clear();
      }
      pMap.add_child("node_list", pNodeList);
      pProperty.put("node_count", nodeCount_);
      pMap.add_child("property", pProperty);
      pTop.add_child("map", pMap);
      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      std::string systemCommand = "mkdir -p " + path;
      std::string systemCommandErrorMsg;
      if (!systemutils::CheckSystemStatus(systemCommand,systemCommandErrorMsg))
      {
        printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
        return false;
      }
      boost::property_tree::write_xml(path + "/map.xml", pTop, std::locale(), setting);
      return true;
    };
    
    bool GetMapXml(const std::string &path)
    {
      boost::property_tree::ptree mapXmlPt;
      boost::property_tree::xml_parser::read_xml(path + "/map.xml", mapXmlPt,
                                                 boost::property_tree::xml_parser::trim_whitespace);
      if (mapXmlPt.empty())
      {
        printf("Map.xml is empty! Path is %s", path.c_str());
        return false;
      }
      
      file_data_type::MapData tmpNode;
      file_data_type::NeighbourInMapData neighbourInNode;
      std::string tmpPoseStr;
      boost::property_tree::ptree pNeighbourList;
      boost::property_tree::ptree pNodeList = mapXmlPt.get_child("map.node_list");
      nodeList_.clear();
      nodeList_.reserve(pNodeList.size());
      for (auto &node : pNodeList)
      {
        tmpNode.id = std::stoi(node.second.get("id", "").c_str());
        pNeighbourList = node.second.get_child("neighbour_list");
        for (auto &neighbour : pNeighbourList)
        {
          neighbourInNode.id = std::atoi(neighbour.second.get("id", "").c_str());
          tmpPoseStr = neighbour.second.get("transform", " ");
          stringutils::PoseStringToXml(tmpPoseStr, neighbourInNode.transform);
          tmpNode.neighbourList.emplace_back(std::move(neighbourInNode));
        }
        nodeList_.emplace_back(std::move(tmpNode));
      }
      this->nodeCount_=this->nodeList_.size();
      return true;
    };
    
    int GetNodeCount()
    {
      return nodeCount_;
    }
    
    std::vector<file_data_type::MapData> GetNodeList()
    {
      return nodeList_;
    }
  private:
    int nodeCount_;
    std::vector<file_data_type::MapData> nodeList_;
  };
  
  class SubmapDataXml
  {
  public:
    SubmapDataXml(){};
    SubmapDataXml(file_data_type::SubmapData newNode) : node_(newNode){};
    ~SubmapDataXml(){};
    
    bool SetLaserDataXml(const std::string &path)
    {
      boost::property_tree::ptree pLaserData;
      boost::property_tree::ptree pProbabilityGrid;
      std::string folderStringSubmap = path + "/frames/" + std::to_string(node_.id);
      std::string systemCommand = "mkdir -p " + folderStringSubmap;
      std::string systemCommandErrorMsg;
      if (!systemutils::CheckSystemStatus(systemCommand,systemCommandErrorMsg))
      {
        printf("System command error! Error msg is %s", systemCommandErrorMsg.c_str());
        return false;
      }
      pLaserData.put("id", node_.id);
      std::string poseStr;
      stringutils::PoseXmlToString(node_.pose, poseStr);
      pLaserData.put("pose", poseStr);
      std::string localPoseStr;
      stringutils::PoseXmlToString(node_.localPose, localPoseStr);
      pLaserData.put("local_pose", localPoseStr);
      pLaserData.put("num_range_data", node_.numRangeData);
      pLaserData.put("finished", node_.finished);
      pProbabilityGrid.put("resolution",node_.resolution);
      std::string maxStr = std::to_string(node_.max[0]) + " " +
      std::to_string(node_.max[1]);
      pProbabilityGrid.put("max", maxStr);
      std::string numCellsStr = std::to_string(node_.numCells[0]) + " " +
      std::to_string(node_.numCells[1]);
      pProbabilityGrid.put("num_cells", numCellsStr);
      std::string knownCellsBoxStr = std::to_string(node_.knownCellsBox[0]) + " " +
      std::to_string(node_.knownCellsBox[1]) + " " +
      std::to_string(node_.knownCellsBox[2]) + " " +
      std::to_string(node_.knownCellsBox[3]);
      pProbabilityGrid.put("known_cells_box", knownCellsBoxStr);
      pLaserData.add_child("probability_grid", pProbabilityGrid);
      boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
      boost::property_tree::write_xml(folderStringSubmap + "/data.xml", pLaserData,
                                      std::locale(), setting);
      return true;
    };
    
    bool GetLaserDataXml(const std::string &path)
    {
      boost::property_tree::ptree laserDataXmlPt;
      boost::property_tree::xml_parser::read_xml(path + "/data.xml", laserDataXmlPt,
                                                 boost::property_tree::xml_parser::trim_whitespace);
      if(laserDataXmlPt.empty())
      {
        printf("Laser data.xml is empty! Path is %s", path.c_str());
        return false;
      }
      node_.id = std::atoi(laserDataXmlPt.get("id", "").c_str());
      node_.finished = laserDataXmlPt.get<bool>("finished");
      node_.numRangeData = std::atoi(laserDataXmlPt.get("num_range_data", "").c_str());
      std::string poseStr = laserDataXmlPt.get("pose", "");
      stringutils::PoseStringToXml(poseStr, node_.pose);
      std::string localPoseStr = laserDataXmlPt.get("local_pose", "");
      stringutils::PoseStringToXml(localPoseStr, node_.localPose);
      std::string maxStr = laserDataXmlPt.get<std::string>("probability_grid.max");
      sscanf(maxStr.c_str(), "%lf %lf", &node_.max[0], &node_.max[1]);
      std::string numCellsStr = laserDataXmlPt.get<std::string>("probability_grid.num_cells");
      sscanf(numCellsStr.c_str(), "%d %d", &node_.numCells[0], &node_.numCells[1]);
      std::string knownCellsBoxStr = laserDataXmlPt.get<std::string>("probability_grid.known_cells_box");
      sscanf(knownCellsBoxStr.c_str(), "%d %d %d %d", &node_.knownCellsBox[0], &node_.knownCellsBox[1],
             &node_.knownCellsBox[2], &node_.knownCellsBox[3]);
      node_.resolution = laserDataXmlPt.get<double>("probability_grid.resolution");
      
      return true;
    };
    
    file_data_type::SubmapData GetNode()
    {
      return node_;
    }
  private:
    file_data_type::SubmapData node_;
  };

  
  
};

namespace tiny_xml
{

};

#endif