#ifndef TAG_GLOBAL_POSES_XML
#define TAG_GLOBAL_POSES_XML
#include <vector>
#include <string>
#include <geometry_msgs/Pose.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <utility/string/utils_string.hpp>
#include <common/data.hpp>
namespace tagglobalposesxml
{
    class TagGlobalPosesXml
    {
    public:
        TagGlobalPosesXml(){};
        TagGlobalPosesXml(std::vector<data::TagGlobalPoseNode> nodeList) : nodeList_(nodeList){};
        ~TagGlobalPosesXml(){};

        bool SetTagGlobalPosesXml(const std::string &path)
        {
            if(0 == nodeList_.size())
            {
                printf("NodeList is empty!");
                return false;
            }
            boost::property_tree::ptree pTagGlobalPoses;
            boost::property_tree::ptree pGlobalPoseMap;
            std::string poseStr;
            for(const auto &tag:nodeList_)
            {
                pGlobalPoseMap.put("ns", tag.ns);
                pGlobalPoseMap.put("tag_id", tag.tagId);
                pGlobalPoseMap.put("id", tag.id);
                pGlobalPoseMap.put("visible", tag.visible);
                stringutils::PoseXmlToString(tag.globalPose, poseStr);
                pGlobalPoseMap.put("global_pose", poseStr);
                pTagGlobalPoses.add_child("tag", pGlobalPoseMap);
            }
            boost::property_tree::xml_writer_settings<std::string> setting(' ', 2);
            boost::property_tree::write_xml(path + "/tag_global_poses.xml", pTagGlobalPoses, std::locale(),
                                            setting);
            return true;
        };

        bool GetTagGlobalPosesXml(const std::string &path)
        {
            boost::property_tree::ptree TagGlobalPosesXmlPt;
            boost::property_tree::xml_parser::read_xml(path + "/tag_global_poses.xml", TagGlobalPosesXmlPt,
                                               boost::property_tree::xml_parser::trim_whitespace);
            if(TagGlobalPosesXmlPt.empty())
            {
                printf("tag_global_poses.xml is empty! Path is %s", path.c_str());
                return false;
            }
            data::TagGlobalPoseNode tmpNode;
            std::string poseStr;
            nodeList_.clear();
            nodeList_.reserve(TagGlobalPosesXmlPt.size());
            for (const auto &tag : TagGlobalPosesXmlPt)
            {
                if(tag.first=="tag")
                {
                    int visible = std::stoi(tag.second.get("visible", "").c_str());
                    if (!visible)
                    {
                        continue;
                    }
                    tmpNode.id = std::stoi(tag.second.get("id", "").c_str());
                    tmpNode.tagId = std::stoi(tag.second.get("tag_id", "").c_str());
                    tmpNode.ns = tag.second.get("ns", "");
                    poseStr = tag.second.get("global_pose", "");
                    stringutils::PoseStringToXml(poseStr, tmpNode.globalPose);
                    nodeList_.emplace_back(std::move(tmpNode));
                }
            }
            return true;
        };

        std::vector<data::TagGlobalPoseNode> GetNodeList()
        {
            return nodeList_;
        }
    private:
        std::vector<data::TagGlobalPoseNode> nodeList_;
    };
};
#endif