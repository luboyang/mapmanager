#ifndef MAP_XML
#define MAP_XML
#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "utils/utils_system.hpp"
#include "utils/utils_string.hpp"
#include "file_data_type/file_data.hpp"
#endif