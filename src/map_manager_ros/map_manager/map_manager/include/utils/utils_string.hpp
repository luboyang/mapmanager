#ifndef UTILS_STRING
#define UTILS_STRING
#include "transform.h"
#include <string>
#include <vector>

namespace stringutils
{
typedef std::uint64_t hash_t;
constexpr hash_t prime = 0x100000001B3ull;
constexpr hash_t basis = 0xCBF29CE484222325ull;

inline void PoseXmlToString(const Rigid3d &pose, std::string &poseStr, const bool flag = false)
{
  if(flag)
    poseStr = std::to_string(pose.translation().x()) + " " + std::to_string(pose.translation().y()) + " " +
            std::to_string(pose.translation().z())  + " " + std::to_string(pose.rotation().w()) + " " +
            std::to_string(pose.rotation().x()) + " " + std::to_string(pose.rotation().y()) + " " +
            std::to_string(pose.rotation().z());
  else
    poseStr = std::to_string(pose.translation().x()) + " " + std::to_string(pose.translation().y()) + " " +
              std::to_string(pose.translation().z())  + " " +
              std::to_string(pose.rotation().x()) + " " + std::to_string(pose.rotation().y()) + " " +
              std::to_string(pose.rotation().z())+ " " + std::to_string(pose.rotation().w());
}

inline void PoseStringToXml(const std::string &poseStr, Rigid3d &pose, const bool flag = false)
{
  double x,y,z,qx,qy,qz,qw;
  if(flag)
    std::sscanf(poseStr.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &x, &y, &z ,&qw, &qx, &qy, &qz);
  else
    std::sscanf(poseStr.c_str(), "%lf %lf %lf %lf %lf %lf %lf", &x, &y, &z, &qx, &qy, &qz,&qw);
  pose = Rigid3d({x,y,z},{qw,qx,qy,qz});
}

inline void SplitString() { return; }

inline hash_t hash_(char const *str)
{
  hash_t ret{basis};

  while (*str)
  {
    ret ^= *str;
    ret *= prime;
    str++;
  }

  return ret;
}
inline constexpr hash_t hash_compile_time(char const *str, hash_t last_value = basis)
{
  return *str ? hash_compile_time(str + 1, (*str ^ last_value) * prime) : last_value;
}
// switch string
inline constexpr unsigned long long operator"" _hash(char const *p, size_t)
{
  return hash_compile_time(p);
}
} // namespace stringutils
#endif