#ifndef UTILS_SYSTEM
#define UTILS_SYSTEM
#include <cstring>
#include <string>

namespace systemutils
{
inline void GetPath(const std::string &mapName, std::string &fullPath)
{
  std::string homePath;

  if (const char *envPath = std::getenv("HOME"))
  {
    std::string temp = "/root";
    homePath = envPath;
    if (homePath.compare(0, 4, temp, 0, 4) == 0)
    {
      homePath = "/home/jz";
    }
  }
  else
  {
    homePath = "/home/jz";
  }
  fullPath = homePath + "/map/" + mapName + "/";
}

inline bool JudgeFileExist(const std::string &fileName)
{
  if (FILE *file = fopen(fileName.c_str(), "r"))
  {
    fclose(file);
    return true;
  }
  else
  {
    return false;
  }
}

inline bool CheckSystemStatus(const std::string &cmdString, std::string &errorMsgs)
{
  int status = system(cmdString.c_str());
  if (status < 0)
  {
    // 这里务必要把errno信息输出或记入Log
    errorMsgs = "cmd:" + cmdString + "\nerror:" + strerror(errno);
    return false;
  }

#ifndef _WIN32
  if (WIFEXITED(status))
  {
    // 取得cmdString执行结果
    errorMsgs = "normal termination, exit status = " + WEXITSTATUS(status);
  }
  else if (WIFSIGNALED(status))
  {
    // 如果cmdString被信号中断，取得信号值
    errorMsgs = "abnormal termination,signal number = " + WTERMSIG(status);
  }
  else if (WIFSTOPPED(status))
  {
    // 如果cmdString被信号暂停执行，取得信号值
    errorMsgs = "process stopped, signal number = " + WSTOPSIG(status);
  }
#endif
  return true;
}

inline bool CreatePath(const std::string &path, std::string &errorMsg)
{
  std::string systemCommand = "rm -r -f " + path;
  // 判断cmd执行情况
  if (!CheckSystemStatus(systemCommand, errorMsg))
  {
    printf("CreatePath cmd : %s, errormsg : %s", systemCommand.c_str(), errorMsg.c_str());
    return false;
  }

  systemCommand = "mkdir " + path;
  if (!CheckSystemStatus(systemCommand, errorMsg))
  {
    printf("CreatePath cmd : %s, errormsg : %s", systemCommand.c_str(), errorMsg.c_str());
    return false;
  }

  return true;
}
} // namespace systemutils
#endif