#ifndef UTILS_CV
#define UTILS_CV
#include <opencv2/opencv.hpp>
#include <vector>
namespace cvutils
{
enum MAP_FORMART
{
  SHOW_MAP,
  SAVE_MAP,
  PUB_MAP
};

inline cv::Mat Grey16ToRgba(const cv::Mat &image)
{
  cv::Mat mapAdjusted = cv::Mat::zeros(image.rows, image.cols, CV_8UC4);
  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uint16_t>(i, j);
      if (value == 0)
      {
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value / 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = value / 128.;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = value / 128.;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = value / 128.;

        const int delta = 128 - value / 128;
        // 				const int delta = 128 - int(value / 256);
        if (delta < 0)
        {
          mapAdjusted.at<cv::Vec4b>(i, j)[0] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[1] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta : 0;
        // 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 255 * sqrt(sqrt(alpha / 128.));
      }
    }
  }
  return mapAdjusted;
}

inline cv::Mat Grey8ToRgba(const cv::Mat &image)
{
  cv::Mat mapAdjusted = cv::Mat::zeros(image.rows, image.cols, CV_8UC4);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int value = image.at<uchar>(i, j);
      if (value == 128)
      {
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 0;
      }
      else
      {
        // 				map_adjusted.at<uchar>(i, j) = value / 128;
        mapAdjusted.at<cv::Vec4b>(i, j)[0] = value;
        mapAdjusted.at<cv::Vec4b>(i, j)[1] = value;
        mapAdjusted.at<cv::Vec4b>(i, j)[2] = value;

        const int delta = 128 - value;
        // 				const int delta = 128 - int(value / 256);
        if (delta < 0)
        {
          mapAdjusted.at<cv::Vec4b>(i, j)[0] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[1] = 255;
          mapAdjusted.at<cv::Vec4b>(i, j)[2] = 255;
        }
        const double alpha = delta > 0 ? delta : -delta;
        // 				const unsigned char value1 = delta > 0 ? delta : 0;
        // 				map_adjusted.at<Vec4b>(i, j)[3] = alpha*2-1;
        mapAdjusted.at<cv::Vec4b>(i, j)[3] = 255 * sqrt(sqrt(alpha / 128.));
      }
    }
  }
  return mapAdjusted;
}

inline cv::Mat Image16ToImage8(const cv::Mat &image16)
{
  cv::Mat image8(image16.rows, image16.cols, CV_8UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      int value = image16.at<uint16_t>(i, j);
      if (value == 0)
        image8.at<uchar>(i, j) = 128;
      else
        image8.at<uchar>(i, j) = value / 128;
    }

  return image8;
}

inline cv::Mat Image8ToImage16(const cv::Mat &image8)
{
  cv::Mat image16(image8.rows, image8.cols, CV_16UC1);
  for (int i = 0; i < image16.rows; i++)
    for (int j = 0; j < image16.cols; j++)
    {
      const unsigned char color = image8.at<uchar>(i, j);
      if (color == 128)
        image16.at<uint16_t>(i, j) = 0;
      else
        image16.at<uint16_t>(i, j) = color * 128;
    }
  return image16;
}
inline void AutoSetIgnoreArea(const std::string &path, const int &distance_threshold = 3)
{
  // auto set ignore area
  cv::Mat img = cv::imread(path + "map.png", CV_LOAD_IMAGE_UNCHANGED);
  cv::Mat gray_img;
  if (img.type() != CV_8UC1)
  {
    cvtColor(img, gray_img, CV_BGR2GRAY);
  }
  else
  {
    gray_img = img;
  }
  cv::Mat new_bool_image(gray_img.rows, gray_img.cols, CV_8UC1, cv::Scalar(255));
  cv::Mat binary_img;
  threshold(gray_img, binary_img, 100, 255, CV_THRESH_BINARY);
  cv::Mat dstImage;
  cv::distanceTransform(binary_img, dstImage, CV_DIST_L2, CV_DIST_MASK_PRECISE);
  //   Mat element1 = getStructuringElement(cv::MorphShapes::MORPH_RECT, Size(5, 5));
  //   erode(binary_img, dstImage, element1);
  cv::Mat output(dstImage.rows, dstImage.cols, CV_8UC1, cv::Scalar(0));
  for (int i = 0; i < dstImage.rows; i++)
  {
    for (int j = 0; j < dstImage.cols; j++)
    {
      // if(dstImage.at<uchar>(i,j) != 0)
      if (dstImage.at<float>(i, j) > distance_threshold)
      {
        new_bool_image.at<uchar>(i, j) = 0;
      }
    }
  }
  imwrite(path + "bool_image_auto.png", new_bool_image);
}

inline void UpdateModifyArea(const cv::Mat &modify_area_img, const std::string &path)
{
  std::string img_full_path = path + "frames/0/probability_grid_origin.png";
  std::string img_update_path = path + "frames/0/probability_grid.png";
  cv::Mat org = cv::imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
  for (int i = 0; i < modify_area_img.rows; i++)
    for (int j = 0; j < modify_area_img.cols; j++)
    {
      if (modify_area_img.at<uint8_t>(i, j) == 127 || modify_area_img.at<uint8_t>(i, j) == 128)
      {
        continue;
      }
      else if (modify_area_img.at<uint8_t>(i, j) == 255)
        org.at<uint16_t>(i, j) = 32765;
      else
      {
        if (modify_area_img.at<uint8_t>(i, j) == 0)
          org.at<uint16_t>(i, j) = 1;
        else
          org.at<uint16_t>(i, j) = 128 * modify_area_img.at<uint8_t>(i, j);
      }
    }
  imwrite(path + "modify_area.png", modify_area_img);
  // LOG(INFO) << "imwrite modify area.png done!";
  imwrite(img_update_path, org);
  // LOG(INFO) << "update probability_grid.png done!";
  cv::Mat img_8 = Image16ToImage8(org);
  imwrite(path + "map.png", img_8);
  // LOG(INFO) << "update map.png done!";
}
} // namespace cvutils
#endif