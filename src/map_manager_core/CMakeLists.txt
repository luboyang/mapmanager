cmake_minimum_required (VERSION 3.8)

add_subdirectory(editor)
add_subdirectory(mapping)
add_subdirectory(localization)

