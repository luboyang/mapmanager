#ifndef LOAD_LANDMARKS_SERVER_SERVICE
#define LOAD_LANDMARKS_SERVER_SERVICE
#include <vector>
#include "xml/xml_handle.h"
#include "common/datatype/output_data_type/output_data.hpp"
#include "common/datatype/file_data_type/file_data.hpp"
#include "common/error_info.hpp"
class LoadLandmarksService
{
  public:
	  LoadLandmarksService() {};
  common::ErrorInfo Init(const std::string& filePath);
  common::ErrorInfo LoadLandmarksData(std::vector<file_data_type::LandmarkInfo> &markerList);
  common::ErrorInfo UpDateLandmarks(const std::vector<file_data_type::LandmarkInfo> &newLandmarkList);
  common::ErrorInfo ModifyLandmarksId(const std::vector<output_data_type::IdRemap> &idList);

  private:
  std::string filePath_;
};

#endif