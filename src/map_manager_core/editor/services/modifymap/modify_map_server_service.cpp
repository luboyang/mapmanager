#include "modify_map_server_service.h"

#include "utility/system/utils_system.hpp"
#include "utility/string/utils_string.hpp"
#include "utility/cv/utils_cv.hpp"
#include "xml/xml_handle.h"
using namespace common;



ErrorInfo ModifyMapServerService::Init(const std::string& mapPath, const std::vector<double>& maxes,
    const double& resolution)
{
	if (mapPath.empty())
	{
		return common::ErrorInfo(-1, "mapPath is empty", common::ErrorType::kError);
	}
    
    if (maxes.size() < 1)
    {
		return common::ErrorInfo(-1, "Maxes is invalid", common::ErrorType::kError);
    }

    mapPath_ = mapPath;
    maxes_ = maxes;
    resolution_ = resolution;

    return ErrorInfo();
}

ErrorInfo ModifyMapServerService::ModifyMap(
    const std::vector<output_data_type::SubmapImageEntry> &modifiedMapList)
{
  cv::Mat modify_area_img;
  if (!systemutils::JudgeFileExist(mapPath_ + "/modify_area.png"))
  {
    cv::Mat map_img = cv::imread(mapPath_ + "/map.png");
    modify_area_img = cv::Mat(map_img.rows, map_img.cols, CV_8UC1, cv::Scalar(127));
  }
  else
  {
    modify_area_img = cv::imread(mapPath_ + "/modify_area.png", CV_LOAD_IMAGE_UNCHANGED);
  }

  tiny_xml::MapPngXml map_png_xml;
  if (!map_png_xml.GetXml(mapPath_))
  {
    return ErrorInfo(-1, "Load " + mapPath_ + "/map_data.xml failed!", ErrorType::kError);
  }
  auto mapDataXmlNode = map_png_xml.GetNode();
  float Max_box[2] = {static_cast<float>(mapDataXmlNode.pose.translation().y()), static_cast<float>(mapDataXmlNode.pose.translation().x())};
  float map_resolution = mapDataXmlNode.resolution;

  for (const auto &submap : modifiedMapList)
  {
    int id = submap.submap_index;
    std::string img_full_path = mapPath_ + "/frames/" + std::to_string(id) + "/probability_grid.png";
    std::string img_origin_path =
        mapPath_ + "/frames/" + std::to_string(id) + "/probability_grid_origin.png";

    cv::Mat org = cv::imread(img_origin_path, CV_LOAD_IMAGE_UNCHANGED);
    if (org.empty())
    {
      org = cv::imread(img_full_path, CV_LOAD_IMAGE_UNCHANGED);
      imwrite(img_origin_path, org);
    }

    cv::Mat img = submap.image;
    // assert(org.cols == img.cols && org.rows == img.rows);
    if (org.cols != img.cols || org.rows != img.rows)
    {
      return ErrorInfo(-1, "error", ErrorType::kError);
    }

    std::string dataXmlPath = mapPath_ + "/frames/" + std::to_string(id);
    tiny_xml::SubmapDataXml submap_data_xml;
    if (!submap_data_xml.GetXml(dataXmlPath))
    {
      return ErrorInfo(-1, "Load " + dataXmlPath + "/data.xml failed!", ErrorType::kError);
    }
    auto submap_data = submap_data_xml.GetNode();

    Rigid3d submap_global_pose =submap_data.pose;
    float submap_resolution = static_cast<float>(submap_data.resolution);
    double submap_box[2];
    submap_box[0] = submap_data.max[0];
    submap_box[1] = submap_data.max[1];
    try {
        for (int i = 0; i < img.rows; i++)
        {
            for (int j = 0; j < img.cols; j++)
            {
                int img_delta = img.at<cv::Vec4b>(i, j)[0];
                int img_alpha = img.at<cv::Vec4b>(i, j)[3];
                if (img_alpha > 0)
                {
                    Eigen::Vector3d point_in_submap(submap_box[0] - (i + 0.5) * submap_resolution,
                        submap_box[1] - (j + 0.5) * submap_resolution, 0);
                    Eigen::Vector3d point_in_map = submap_global_pose * point_in_submap;
                    int map_i = static_cast<int>(Max_box[0] - point_in_map[0] / map_resolution);
                    int map_j = static_cast<int>(Max_box[1] - point_in_map[1] / map_resolution);
                    if (map_i > 0 && map_i < modify_area_img.rows && map_j > 0 &&
                        map_j < modify_area_img.cols)
                    {
                        modify_area_img.at<uint8_t>(map_i, map_j) = img_delta;
                        if (img_delta == 0)
                        {
                            modify_area_img.at<uint8_t>(map_i, map_j) = 255 - img_alpha;
                        }
                    }
                }
            }
        }
    }
    catch (cv::Exception& e)
    {
        std::cout << e.what() << std::endl;
    }

    cvutils::UpdateModifyArea(modify_area_img, mapPath_);
  }
  cvutils::AutoSetIgnoreArea(mapPath_);
  UpdateIgnoreArea();
  return ErrorInfo();
}

ErrorInfo ModifyMapServerService::UpdateIgnoreArea()
{
  if (!systemutils::JudgeFileExist(mapPath_ + "/corner_points.xml"))
  {
    return ErrorInfo(-1, "no " + mapPath_ + "/corner_points.xml file!", ErrorType::kError);
  }

  tiny_xml::BoolImageCornerPointsXml cornerPointsXml;
  if (!cornerPointsXml.GetXml(mapPath_))
  {
    return ErrorInfo(-1, "Get corner_points.xml failed!", ErrorType::kError);
  }
  auto cornerPointsNodeList = cornerPointsXml.GetNodeList();
  cv::Mat mask = cv::imread(mapPath_ + "/bool_image_auto.png", CV_LOAD_IMAGE_UNCHANGED);
  std::vector<cv::Point> contour;
  std::vector<std::vector<cv::Point>> contours;
  for (const auto &corberPointsNode : cornerPointsNodeList)
  {
    contour.clear();
    contours.clear();
    int pointSize = corberPointsNode.points_size;
    for (int i = 0; i < pointSize; i++)
    {
      Eigen::Vector2i imageLoc;
      World2imageLoc(Eigen::Vector2d(maxes_[0], maxes_[1]), resolution_,
                     Eigen::Vector2d(corberPointsNode.points[2*i], 
                                     corberPointsNode.points[2*i + 1]), 
                     imageLoc);
      cv::Point p(imageLoc(0), imageLoc(1));
      contour.emplace_back(std::move(p));
    }
    contours.emplace_back(std::move(contour));
  }
  cv::drawContours(mask, contours, -1, cv::Scalar::all(0), CV_FILLED);
  imwrite(mapPath_ + "/bool_image.png", mask);
  return ErrorInfo();
}
