#ifndef MODIFY_MAP_SERVER
#define MODIFY_MAP_SERVER
#include "common/datatype/file_data_type/file_data.hpp"
#include "common/datatype/output_data_type/output_data.hpp"
#include "common/error_info.hpp"
#include <string>
class ModifyMapServerService
{
  public:
      ModifyMapServerService() {};
      ~ModifyMapServerService() {};
 /* ModifyMapServerService(const std::string &mapPath, const std::vector<double> &maxes,
                         const double &resolution)
      : mapPath_(mapPath), maxes_(maxes), resolution_(resolution){};*/
      common::ErrorInfo Init(const std::string& mapPath, const std::vector<double>& maxes,
          const double& resolution);
  common::ErrorInfo ModifyMap(const std::vector<output_data_type::SubmapImageEntry> &modifiedMapList);

  private:
  common::ErrorInfo UpdateIgnoreArea();
  std::string mapPath_;
  std::vector<double> maxes_;
  double resolution_;
};

#endif