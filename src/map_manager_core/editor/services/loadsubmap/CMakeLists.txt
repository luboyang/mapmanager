cmake_minimum_required (VERSION 3.8)

set(TARGET_NAME loadsubmap)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_ROOT_PATH}/lib/${TARGET_NAME})

set(OpenCV_DIR "${VCPKG_INSTALLED_DIR}/x86-windows/share/opencv3")

find_package(OpenCV REQUIRED)
include_directories(${PROJECT_INC_DIR} ${THIRD_PARTY_INC_DIR} ${EIGEN3_INCLUDE_DIR})
aux_source_directory(. compile_list)
add_library(${TARGET_NAME} STATIC ${compile_list})
target_link_libraries(${TARGET_NAME}  PUBLIC ${OpenCV_LIBS} xml_handle)