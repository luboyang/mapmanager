#ifndef LOAD_SUBMAPS_SERVER_SERVICE_
#define LOAD_SUBMAPS_SERVER_SERVICE_
#include "xml/xml_handle.h"
#include "common/datatype/output_data_type/output_data.hpp"
#include "common/error_info.hpp"

class LoadSubmapsServerService
{
public:
  LoadSubmapsServerService() {};
  common::ErrorInfo Init(const std::string& fullPath);

  common::ErrorInfo GetSubmapImages(std::vector<output_data_type::SubmapImageEntry> &submap_images,
                            output_data_type::MapPosesInfo &map_poses_info);

private:
  common::ErrorInfo LoadOldMap(const std::string &filePath,
                       output_data_type::SubmapImageEntry &submapImage,
                       output_data_type::MapPosesInfo &map_poses_info);
  std::string fullPath_;
};

#endif // LOAD_SUBMAPS_SERVER_SERVICE_