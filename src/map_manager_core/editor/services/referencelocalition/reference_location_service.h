/*
 * Copyright 2023 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef REFERENCELOCATIONSERVICE_H
#define REFERENCELOCATIONSERVICE_H
#include "common/datatype/output_data_type/output_data.hpp"
#include "common/error_info.hpp"
class ReferenceLocationService
{
public:
  ReferenceLocationService();
  ~ReferenceLocationService();
  common::ErrorInfo getReferenceInfos(
    const std::vector<Eigen::Isometry3d,Eigen::aligned_allocator<Eigen::Isometry3d>>& posesInGlobal_,
    const std::vector<Rigid3d>& request_poses, 
    std::vector<output_data_type::ReferenceLocationData>& ref_infos);
};

#endif // REFERENCELOCATIONSERVICE_H
