/*
 * Copyright 2023 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include "reference_location_service.h"

ReferenceLocationService::ReferenceLocationService()
{

}

ReferenceLocationService::~ReferenceLocationService()
{

}

common::ErrorInfo ReferenceLocationService::getReferenceInfos(
  const std::vector<Eigen::Isometry3d,Eigen::aligned_allocator<Eigen::Isometry3d>>& posesInGlobal_,
  const std::vector< Rigid3d >& request_poses, 
  std::vector<output_data_type::ReferenceLocationData>& ref_infos)
{
  //transfrom to rigid3d
  std::vector<Rigid3d> poses_in_global;
  for (const auto& it : posesInGlobal_)
  {
      poses_in_global.push_back(FromIsometry(it));
  }
  for (uint cnt = 0; cnt < request_poses.size(); cnt++)
  {
    Eigen::Vector2d worldLoc;
    Rigid3d pose = request_poses[cnt];
    output_data_type::ReferenceLocationData ref_info;
    ref_info.world_pose = Rigid3d({pose.translation().x(), pose.translation().y(), 0}, pose.rotation());
    ref_info.reference_id = -1;
    Rigid3d nearestPose;
    getNearestPose(poses_in_global,
        Eigen::Vector2d(pose.translation().x(), pose.translation().y()),
        nearestPose, ref_info.reference_id);
    if (ref_info.reference_id == -1)
    {
        return common::ErrorInfo(-1, "Invalid map with " + std::to_string(posesInGlobal_.size()) + " poses inside!", common::ErrorType::kError);
    }
    ref_info.reference_pose = nearestPose.inverse() * ref_info.world_pose;
    ref_infos.push_back(ref_info);
  }

  return common::ErrorInfo();
}
